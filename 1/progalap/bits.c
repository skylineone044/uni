#include <stdio.h>
#include <stdlib.h>

int array2d() {
    // dynamic array malloc free dinamikus tomb
    // 2. modszer
    // tomb ami tele van tombokre mutato pointerekkel
    // nehez lefoglalni es felszabaditani de konnyu indexelni
    int sorok = 10;
    int oszlopok = 20;

    // crate 2d array
    int **t2 = (int **)malloc(sorok * oszlopok * sizeof(int *));
    for (int i = 0; i < sorok; i++) {
        t2[i] = malloc(oszlopok * sizeof(int));
    }

    // feltolteni egyre nagyobb elemekkel
    int szamlalo = 0;
    for (int i = 0; i < sorok; i++) {
        for (int j = 0; j < oszlopok; j++) {
            t2[i][j] =  szamlalo;
            printf("%3d ", t2[i][j]);
            szamlalo++;
        }
        printf("\n");
    }
    printf("\n");

    // free 2d array
    for (int i = 0; i < sorok; i++) {
        free(t2[i]);
    }
    free(t2);
    return 0;
}

int array1d_as_2d() {
    // 1. modszer
    // 1 dimenzios tombkent, es atszamitjuk az indexet
    // konnyu letrehiznui es felszabaditani, nehez indexelni
    // i*oszlopok+j
    int sorok = 10;
    int oszlopok = 20;
    int *t1 = (int *)malloc(sizeof(int) * sorok * oszlopok);

    // feltolteni egyre nagyobb elemekkel
    int szamlalo = 0;
    for (int i = 0; i < sorok; i++) {
        for (int j = 0; j < oszlopok; j++) {
            t1[i * oszlopok + j] = szamlalo;
            szamlalo++;
            printf("%3d ", t1[i * oszlopok + j]);
        }
        printf("\n");
    }
    printf("\n");

    // free it 
    free(t1);

}

// reverse string sztring megfordit rev str
void string_megfordit(char *szoveg) {
    int len = 0;
    for (; szoveg[len] != '\0'; len++) {
    }
    for (int i = 0; i < len / 2; i++) {
        char tmp = szoveg[i];
        szoveg[i] = szoveg[len - i - 1];
        szoveg[len - i - 1] = tmp;
    }
}
double absz(double x) {
    if (x < 0) {
        return -x;
    } else {
        return x;
    }
}
int nearlyEqual(double a, double b, double error) {
    if (absz(a-b) <= error) {
        return 1;
    } else {
        return 0;
    }
}


void mybubblesort(int list[], int len) {
    // ez egy megfixalt verzioja a regi bubble sort funkcionak, atalakitva hogy
    // menjen intekkel
    // int bubblesort int array list int bubble sort
    for (int i = 0; i < len; i++) {
        for (int j = 0; j < len - 1; ++j) {
            int value = list[j];
            int value_after = list[j + 1];

            if (value <= value_after) {
                // right order, keep them
            } else {
                // wrong order, switch them
                int placeholder = list[j];
                list[j] = list[j + 1];
                list[j + 1] = placeholder;

            }
        }
    }
}

int main() {
    array2d();
    array1d_as_2d();

    char hello[] = {'H', 'E', 'L', 'L', 'O', ' ', 'T', 'H', 'E', 'R', 'E', ' ',  'G', 'E', 'N', 'E', 'R', 'A', 'L', ' ', 'K', 'E', 'N', 'o', 'b', 'i', '\0'};
    printf("%s\n", hello);
    string_megfordit(hello);
    printf("%s\n\n", hello);

    float a = 0.069;
    double b = 0.0691;
    printf("%f == %lf: %s\n\n", a, b, (nearlyEqual(a, b, 0.001) == 1 ? "igen" : "nem"));
    return 0;
}
