// make coc-clang display long warinings in popus instead of the vim command line
#include <stdio.h>

int main() {
    double forint, euro, arfolyam;
    arfolyam = 357.806;
    printf("Hany forintot valtsunk?\n> ");
    scanf("%lf", &forint);
    euro = forint / arfolyam;
    printf("%lf HUF = %lf EUR\n", forint, euro);

    return 0;
}
