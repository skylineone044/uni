/*
 * Szegedi Tudományegyetem
 * Informatikai Tanszékcsoport
 * Szoftverfejlesztés Tanszék
 *
 * Programozás Alapjai
 */

#include <stdio.h>

int main() {
    int elso = 1;
    int masodik = 2;
    int harmadik = 3;
    printf("L06: e= %d; m= %d; h= %d;\n", elso, masodik, harmadik);
    printf("L08: e= %d; m= %d; h= %d;\n", elso, masodik, harmadik);
    {
        printf("L11: e= %d; m= %d; h= %d;\n", elso, masodik, harmadik);
    }
    printf("L13: e= %d; m= %d; h= %d;\n", elso, masodik, harmadik);
    {
        printf("L16: e= %d; m= %d; h= %d;\n", elso, masodik, harmadik);
    }
    printf("L18: e= %d; m= %d; h= %d;\n", elso, masodik, harmadik);
    return 0;
}
