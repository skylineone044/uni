/*
 * Szegedi Tudományegyetem
 * Informatikai Tanszékcsoport
 * Szoftverfejlesztés Tanszék
 *
 * Programozás Alapjai
 */

#include <stdio.h>
#include <stdlib.h>

struct ido {
    int hrs;
    int min;
    int sec;
    int ms;
};

int beolvas(struct ido *t) {
    return (scanf("%d:%d:%d.%d", &t->hrs, &t->min, &t->sec, &t->ms) == 4);
}

void kiir(char prefix, struct ido t) {
    if (t.hrs > 0) {
        printf("%c%2d:%02d:%02d.%03d\n", prefix, t.hrs, t.min, t.sec, t.ms);
        return;
    }
    if (t.min > 0) {
        printf("%c%5d:%02d.%03d\n", prefix, t.min, t.sec, t.ms);
        return;
    }
    printf("%c%8d.%03d\n", prefix, t.sec, t.ms);
}

struct ido kulonbseg(struct ido start, struct ido stop) {
    start.ms = (((stop.hrs - start.hrs) * 60 + (stop.min - start.min)) * 60 +
                (stop.sec - start.sec)) * 1000 + (stop.ms - start.ms);
    start.hrs = start.ms / 3600000;
    start.ms %= 3600000;
    start.min = start.ms / 60000;
    start.ms %= 60000;
    start.sec = start.ms / 1000;
    start.ms %= 1000;
    return start;
}

int main() {
    struct ido prev, curr;
    beolvas(&prev);
    kiir(' ', prev);
    while (beolvas(&curr)) {
        prev = kulonbseg(prev, curr);
        kiir('+', prev);
        prev = curr;
    }
}
