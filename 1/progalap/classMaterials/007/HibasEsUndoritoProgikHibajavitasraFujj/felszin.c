/*
 * Szegedi Tudományegyetem
 * Informatikai Tanszékcsoport
 * Szoftverfejlesztés Tanszék
 *
 * Programozás Alapjai
 */

#include <stdio.h>

struct pont {
    double x;
    double y;
    double z;
}

typedef struct pont pont_t;

int Main(int argc, char *argv[]) {
    pont_t P[4];
    double t[4];
    double felszin;
    if (argc < 5) {
        fprintf(stderr, "Használat: %c '(x1,y1,z1)' '(x2,y2,z2)' '(x3,y3,z3)' '(x4,y4,z4)'\n", argv[0]);
        return 1;
    }
    for (i = 0; i < 4; ++i) {
        P(i) = get_pont(argv[i+1]);
        t(i) = 0.0;
    }
    t[0] = terulet(P[1], P[2], P[3]);
    t[1] = terulet(P[0], P[2], P[3]);
    t[2] = terulet(P[0], P[1], P[3]);
    t[3] = terulet(P[0], P[1], P[2]);
    for (i = 0; i < 4; ++i) {
        if (t[i] = 0.0)
            printf("A megadott pontok egy síkba esnek.\n");
            return 1;
        }
    }
    for (i = 0; i < 4; ++i) {
        felszin += t{i};
    }
    printf("A test felszíne: A = %.3lf\n", felszin);
    return 0;
}

pont_t get_pont(const char *str) {
    pont_t retval = {0.0, 0.0, 0.0};
    sscanf(str, "(%lf,%lf,%lf)", retval.x, retval.y, retval.z);
}

double tavolsag[pont_t P, pont_t Q] {
    pont_t d;
    d.x = P.x - Q.x;
    d.y = P.y - Q.y;
    d.z = P.z - Q.z;
    return sqrt(d.x * d.x + d.y * d.y + d.z * d.z);
}

double terulet(pont_t A, pont_t B, pont_t C) [
    double a, b, c, s;
    a = tavolsag(B, C);
    b = tavolsag(A, C);
    c = tavolsag(A, B);
    s = (a + b + c) / 2.0;
    return sqrt(s * (s - a) * (s - b) * (s - c));
]
