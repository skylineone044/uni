#include <stdio.h>

// faktorialsis fuggvegy for ciklussal
int factorialis_for(int x) {
    int eredmeny = x;
    for (int i = x - 1; i > 1; i--) {
        eredmeny *= i;
    }
    return eredmeny;
}

// faktorialis fuggveny rekurzivan
int factorialis_rec(int x) {
    if (x < 2) {
        return x;
    }
    return x * factorialis_rec(x - 1);
}

void negtivjaig(int szam) {
    for (int i = szam; i >= -szam; i--) {
        printf("%d ", i);
    }
    printf("\n");
}

void negtivjaig_rec(int szam) {
    printf("%d ", szam);
    if (szam == 0) {
        return;
    }
    negtivjaig_rec(szam - 1);
    printf("%d ", -szam);
}

// sorozatok
/* szmmtani sorozatok */
// 7 13 19 25 31 37...
int szamtani_sorozat_osszeg(int elso_tag, int masodik_tag, int mettol,
                            int meddig) {
    int osszeg = 0;
    int inkremens = masodik_tag - elso_tag;
    for (int i = 1; i <= meddig; i++) {
        if (i >= mettol) {
            osszeg += elso_tag;
        }
        elso_tag += inkremens;
        /* elso_tag += inkremens; */
    }
    return osszeg;
}

int main() {
    int szam = 6;
    printf("factorialis: %d\n", factorialis_for(szam));
    printf("factorialis: %d\n", factorialis_rec(szam));
    negtivjaig(szam);
    negtivjaig_rec(szam); printf("\n");
    printf("%d \n", szamtani_sorozat_osszeg(7, 13, 4, 7));
    return 0;
}
