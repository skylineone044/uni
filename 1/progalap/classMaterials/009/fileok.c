#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    char nev[100];
    int szint;
    double ero;
    double pontok;
} Harcos;

void harcoskiir(Harcos kit) {
    printf("%s ereje %lf szitje %d\n", kit.nev, kit.ero, kit.szint);
}

void mybubblesort(Harcos list[], int len) {
    // ez egy megfixalt verzioja a regi bubble sort funkcionak, atalakitva hogy
    // menjen harcosokkal
    for (int i = 0; i < len; i++) {
        for (int j = 0; j < len - 1; ++j) {
            double value = list[j].pontok;
            double value_after = list[j + 1].pontok;

            if (value <= value_after) {
                // right order, keep them
            } else {
                // wrong order, switch them
                Harcos placeholder = list[j];
                list[j] = list[j + 1];
                list[j + 1] = placeholder;
            }
        }
    }
}
int main() {

    FILE* file_be;
    file_be = fopen("harcosok.be", "r");
    
    char szoveg[1000];
    fscanf(file_be, "%s", szoveg);
    printf("%s\n", szoveg);



    int harcosokszama = 100;
    Harcos csata[harcosokszama];
    int i = 0;
    while (1) {
        Harcos ujHArcos;
        printf("A harcos neve: ");
        scanf("%s", ujHArcos.nev);
        if (strcmp(ujHArcos.nev, "vege") == 0) {
            break;
        }
        printf("A harcos ereje: ");
        scanf("%lf", &ujHArcos.ero);
        printf("A harcos szitje: ");
        scanf("%d", &ujHArcos.szint);
        csata[i] = ujHArcos;
        i++;
    }

    for (int j = 0; j < i; ++j) {
        harcoskiir(csata[j]);
    }

    for (int j = 0; j < i; ++j) {
        csata[j].pontok = csata[j].ero * csata[j].szint;
    }
    mybubblesort(csata, i);
    printf("Nyertes: %s\n", csata[i - 1].nev);

    fclose(file_be);

    return 0;
}
