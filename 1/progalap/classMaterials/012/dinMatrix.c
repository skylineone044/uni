#include <stdio.h>
#include <stdlib.h>

int main() {
    // 1. modszer
    // 1 dimenzios tombkent, es atszamitjuk az indexet
    // konnyu letrehiznui es felszabaditani, nehez indexelni
    // i*oszlopok+j
    int sorok = 9;
    int oszlopok = 7;
    int *t1 = (int *)malloc(sizeof(int) * sorok * oszlopok);

    int szamlalo = 0;
    for (int i = 0; i < sorok; i++) {
        for (int j = 0; j < oszlopok; j++) {
            t1[i * oszlopok + j] = szamlalo;
            szamlalo++;
            printf("%3d ", t1[i * oszlopok + j]);
        }
        printf("\n");
    }
    /* t1[1] = 5000; */
    // mekkora  legnagyobb oszlop osszege
    int legnagyobbosszeg = 0;
    int legnagyobb_id = 0;
    for (int i = 0; i < oszlopok; i++) {
        int osszeg = 0;
        for (int j = 0; j < sorok; j++) {
            osszeg += t1[j * oszlopok + i];
        }
        if (osszeg > legnagyobbosszeg) {
            legnagyobbosszeg = osszeg;
            legnagyobb_id = i;
        }
    }
    printf("Legnagyobb: %d. = %d\n", legnagyobb_id + 1, legnagyobbosszeg);

    free(t1);

    // dynamic array malloc free dinamikus tomb
    // 2. modszer
    // tomb ami tele van tombokre mutato pointerekkel
    // nehez lefoglalni es felszabaditani de konnyu indexelni
    int **t2 = (int **)malloc(sorok * oszlopok * sizeof(int *));
    for (int i = 0; i < sorok; i++) {
        t2[i] = malloc(oszlopok * sizeof(int));
    }

    szamlalo = 190;
    for (int i = 0; i < sorok; i++) {
        for (int j = 0; j < oszlopok; j++) {
            t2[i][j] = szamlalo;
            szamlalo--;
        }
    }

    // mekkora  legnagyobb oszlop osszege
    for (int i = 0; i < oszlopok; i++) {
        for (int j = 0; j < sorok; j++) {
            printf("%3d ", t2[j][i]);
        }
        printf("\n");
    }

    // free it all
    for (int i = 0; i < sorok; i++) {
        free(t2[i]);
    }
    free(t2);

    return 0;
}
