#include <stdio.h>
#include <stdlib.h>

int **beolvas(char *file, int *sorok, int *oszlopok, int *max_intenzitas) {
    int van_kommet = 0;
    FILE *be_pgm = fopen(file, "r");
    fscanf(be_pgm, "P2");
    char temp[100];
    fscanf(be_pgm, " %99[^\n]", temp);
    /* printf("%s\n", temp); */
    if (temp[0] == '#') {
        van_kommet = 1;
    } else {
        van_kommet = 0;
    }
    fclose(be_pgm);
    be_pgm = fopen(file, "r");
    fscanf(be_pgm, "P2");
    if (van_kommet) {
        fscanf(be_pgm, " %99[^\n]", temp);
    }
    fscanf(be_pgm, "%d %d\n%d\n", sorok, oszlopok, max_intenzitas);
    /* printf("%d %d\n%d\n", *sorok, *oszlopok, *max_intenzitas); */

    int **kep = (int **)malloc(*sorok * *oszlopok * sizeof(int *));
    for (int i = 0; i < *oszlopok; i++) {
        kep[i] = malloc(*sorok * sizeof(int));
    }

    // fill up 2d array
    for (int i = 0; i < *oszlopok; i++) {
        for (int j = 0; j < *sorok; j++) {
            fscanf(be_pgm, "%d", &kep[i][j]);
        }
    }
    for (int i = 0; i < *oszlopok; i++) {
        for (int j = 0; j < *sorok; j++) {
            /* printf("%d ", kep[i][j]); */
        }
        /* printf("\n"); */
    }

    fclose(be_pgm);

    return kep;
}

int kiir(char *file, int *sorok, int *oszlopok, int *max_intenzitas,
         int *kep[]) {
    FILE *ki_pgm = fopen(file, "w");
    fprintf(ki_pgm, "P2\n%d %d\n%d\n", *sorok, *oszlopok, *max_intenzitas);
    for (int i = 0; i < *oszlopok; i++) {
        for (int j = 0; j < *sorok; j++) {
            fprintf(ki_pgm, "%d ", kep[i][j]);
        }
        fprintf(ki_pgm, "\n");
    }
    return 0;
}

int inv_kiir(char *file, int *sorok, int *oszlopok, int *max_intenzitas,
             int *kep[]) {
    FILE *ki_pgm = fopen(file, "w");
    fprintf(ki_pgm, "P2\n%d %d\n%d\n", *sorok, *oszlopok, *max_intenzitas);
    for (int i = 0; i < *oszlopok; i++) {
        for (int j = 0; j < *sorok; j++) {
            fprintf(ki_pgm, "%d ", *max_intenzitas - kep[i][j]);
        }
        fprintf(ki_pgm, "\n");
    }
    return 0;
}
int kiir_bw(char *file, int *sorok, int *oszlopok, int *max_intenzitas,
            int *kep[]) {
    FILE *ki_pgm = fopen(file, "w");
    fprintf(ki_pgm, "P2\n%d %d\n%d\n", *sorok, *oszlopok, *max_intenzitas);
    for (int i = 0; i < *oszlopok; i++) {
        for (int j = 0; j < *sorok; j++) {
            kep[i][j] = (kep[i][j] >= (*max_intenzitas / 2) ? 255 : 0);
            fprintf(ki_pgm, "%d ", kep[i][j]);
        }
        fprintf(ki_pgm, "\n");
    }
    return 0;
}

int kiir_decode(char *file, int *sorok, int *oszlopok, int *max_intenzitas,
                int *kep[]) {
    FILE *ki_pgm = fopen(file, "w");
    fprintf(ki_pgm, "P2\n%d %d\n%d\n", *sorok, *oszlopok, *max_intenzitas);
    for (int i = 0; i < *oszlopok; i++) {
        for (int j = 0; j < *sorok; j++) {
            if (kep[i][j] % 2 == 0) {
                kep[i][j] = 255;
            } else {
                kep[i][j] = 0;
            }
        }
    }
    for (int i = 0; i < *oszlopok; i++) {
        for (int j = 0; j < *sorok; j++) {
            fprintf(ki_pgm, "%d ", kep[i][j]);
        }
        fprintf(ki_pgm, "\n");
    }
    return 0;
}

int kiir_encode(char *file, int *sorok, int *oszlopok, int *max_intenzitas,
                int *kep[], int *uzenet[]) {
    FILE *ki_pgm = fopen(file, "w");
    fprintf(ki_pgm, "P2\n%d %d\n%d\n", *sorok, *oszlopok, *max_intenzitas);
    for (int i = 0; i < *oszlopok; i++) {
        for (int j = 0; j < *sorok; j++) {
            if (kep[i][j] % 2 == 0) {
                kep[i][j] -= 1;
            }
        }
    }

    for (int i = 0; i < *oszlopok; i++) {
        for (int j = 0; j < *sorok; j++) {
            if (uzenet[i][j] != 0) {
                kep[i][j] += 1;
            }
        }
    }
    for (int i = 0; i < *oszlopok; i++) {
        for (int j = 0; j < *sorok; j++) {
            fprintf(ki_pgm, "%d ", kep[i][j]);
        }
        fprintf(ki_pgm, "\n");
    }
    return 0;
}

int main(int argc, char *argv[]) {
    // beolvas
    int sorok, oszlopok, max_intenzitas;
    int **kep = beolvas(argv[1], &sorok, &oszlopok, &max_intenzitas);
    int soroku, oszlopoku, max_intenzitasu;
    int **uzenet = beolvas(argv[3], &soroku, &oszlopoku, &max_intenzitasu);
    // kiir
    /* kiir(argv[2], &sorok, &oszlopok, &max_intenzitas, kep); */
    /* inv_kiir(argv[2], &sorok, &oszlopok, &max_intenzitas, kep); */
    /* kiir_bw(argv[2], &sorok, &oszlopok, &max_intenzitas, kep); */
    /* kiir_decode(argv[2], &sorok, &oszlopok, &max_intenzitas, kep); */
    kiir_encode(argv[2], &sorok, &oszlopok, &max_intenzitas, kep, uzenet);

    // invert kiir

    // free it all
    for (int i = 0; i < sorok; i++) {
        free(kep[i]);
    }
    free(kep);
    for (int i = 0; i < sorok; i++) {
        free(uzenet[i]);
    }
    free(uzenet);

    /* int **kep =(int**); */

    return 0;
}
