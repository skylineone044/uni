#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    for (int i = 0; i < argc; i++) {
        printf("%d. %s\n", i, argv[i]);
    }

    int oszeg = 0;
    for (int i = 1; i < argc; i++) {
        // text to int szovegbol szam
        oszeg += atoi(argv[i]);
    }
    printf("osszeg: %d\n", oszeg);

    return 0;
}
