#include <stdio.h>
#include <stdlib.h>

typedef void (*muvelettipus)(int *);

void kiir(int *elem) { printf("%d ", *elem); }

void bejar(muvelettipus muvelet, int *t, int meret) {
    for (int i = 0; i < meret; i++) {
        (*muvelet)(&t[i]);
        kiir(&t[i]);
    }
    printf("\n");
}

void veletlen(int *elem) { *elem = rand() % 10; }

void duplaz(int *elem) { *elem *= 2; }

void negyyet(int *elem) { *elem *= *elem; }

int main() {
    printf("tomb meret: ");
    int meret;
    scanf("%d", &meret);

    int *t = (int *)malloc(sizeof(int) * meret);

    bejar(veletlen, t, meret);
    bejar(duplaz, t, meret);
    bejar(negyyet, t, meret);
    free(t);
    return 0;
}
