#include <stdio.h>
#include <stdlib.h>

int main() {
    FILE *be_txt = fopen("be.txt", "r");
    int szelesseg, magassag;
    int pos_x, pos_y;
    fscanf(be_txt, "%d %d", &szelesseg, &magassag);
    fscanf(be_txt, "%d %d", &pos_y, &pos_x);
    /* pos_x++; */
    /* pos_y++; */

    int *tabla = (int *)malloc(sizeof(int) * szelesseg * magassag);

    for (int i = 0; i < magassag; i++) {
        for (int j = 0; j < szelesseg; j++) {
            fscanf(be_txt, "%d", &tabla[i * szelesseg + j]);
        }
    }
    tabla[pos_y * szelesseg + pos_x] = 9;
    for (int i = 0; i < magassag; i++) {
        for (int j = 0; j < szelesseg; j++) {
            printf("%d ", tabla[i * szelesseg + j]);
        }
        printf("\n");
    }

    int szmalalo = 0;
    int pos2_y = pos_y;
    int pos2_x = pos_x;
    while (pos2_x > 0 && pos2_y > 0) {
        printf("bal fel\n");
        pos2_x--;
        pos2_y--;
        szmalalo++;
        if (pos2_x > 0 && pos2_y > 0) {
            if (tabla[pos2_y * szelesseg + pos2_x]) {
                break;
            }
        }
    }
    pos2_y = pos_y;
    pos2_x = pos_x;
    while (pos2_x < szelesseg && pos2_y < magassag) {
        printf("jobb le\n");
        pos2_x++;
        pos2_y++;
        szmalalo++;
        if (pos2_x < szelesseg && pos2_y < magassag) {
            if (tabla[pos2_y * szelesseg + pos2_x]) {
                break;
            }
        }
    }
    pos2_y = pos_y;
    pos2_x = pos_x;
    while (pos2_x > 0 && pos2_y < magassag) {
        printf("bal le\n");
        pos2_x--;
        pos2_y++;
        szmalalo++;
        if (pos2_x > 0 && pos2_y < magassag) {
            if (tabla[pos2_y * szelesseg + pos2_x]) {
                break;
            }
        }
    }
    pos2_y = pos_y;
    pos2_x = pos_x;
    while (pos2_x < szelesseg && pos2_y > 0) {
        printf("jobb fel\n");
        pos2_x++;
        pos2_y--;
        szmalalo++;
        if (pos2_x < szelesseg && pos2_y > 0) {
            if (tabla[pos2_y * szelesseg + pos2_x]) {
                break;
            }
        }
    }
    printf("eredmeny: %d\n", szmalalo);
}
