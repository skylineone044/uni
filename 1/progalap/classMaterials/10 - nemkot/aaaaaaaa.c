#include <stdio.h>

#define SOROK 5
#define OSZLOPOK 8

int main() {
    // 2d tomb - matrix
    // eloszor a sor, aztan az oszlop indexek
    int matrix[SOROK][OSZLOPOK];

    int szamlalo = 0;
    for (int i = 0; i < SOROK; i++) {        // sorok
        for (int j = 0; j < OSZLOPOK; j++) { // oszlopok
            matrix[i][j] = szamlalo;
            szamlalo++;
        }
    }

    // matrix kiiratasa
    for (int i = 0; i < SOROK; i++) {
        for (int j = 0; j < OSZLOPOK; j++) {
            printf("%2d ", matrix[i][j]);
        }
        printf("\n");
    }

    // toltsuk fel a matrixot ugy, hogy csak 0-k es 1-esek legyenek benne,
    // de a kozvetlenul egymas mellettiek vagy alattiak mind legyenek
    // kulonbozok. A bal felso elem 1 legyen!
    // 1 0 1 0...
    // 0 1 0 1...
    // 1 0 1 0...
    for (int i = 0; i < SOROK; i++) {
        int x = 0;
        if (i % 2 == 0)
            x = 1;
        for (int j = 0; j < OSZLOPOK; j++) {
            matrix[i][j] = x;
            x = !x; // ha 0 volt, egy lesz, ha 1 volt, 0 lesz
        }
    }

    // matrix kiiratasa
    for (int i = 0; i < SOROK; i++) {
        for (int j = 0; j < OSZLOPOK; j++) {
            printf("%2d ", matrix[i][j]);
        }
        printf("\n");
    }

    return 0;
}
