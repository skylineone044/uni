#include <stdio.h>
#include <stdlib.h>

int main() {
    int *a = malloc(sizeof(int));
    *a = 55;
    printf("A aaa: %d, cime %p\n", *a, a);
    free(a);
    printf("A aaa: %d, cime %p\n", *a, a);

    int *b = malloc(sizeof(int));
    *b = 55;
    printf("A aaa: %d, cime %p\n", *b, b);
    free(b);
    printf("A aaa: %d, cime %p\n", *b, b);

    int *dint = malloc(10 * sizeof(int));
    int szam = 0;
    for (int i = 0; i < 10; i++) {
        dint[i] = i;
    }
    for (int i = 0; i < 10; i++) {
        printf("%d ", dint[i]);
    }
    free(dint);

    return 0;
}
