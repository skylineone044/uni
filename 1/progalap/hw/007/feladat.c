#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* this is such a mess, but it seems to work
 * I'm sorry future me
 *                         -past me */

typedef struct {
    char nev;
    float uj_korido;
    int kopas_kezdete;
    float kopas_merteke;
    int elhasznalodas_kezdete;
    float elhasznalodas_merteke;
} Gumi;

typedef struct {
    char ujtipus;
    int kortol; // ettol a kortol ervenyes az ujtipus -tipusu gumi
} gumiCsere;

void nullazo(gumiCsere tomb[], int len) {
    for (int i = 0; i < len; i++) {
        tomb[i].kortol = 0;
        tomb[i].ujtipus = 0;
    }
}

void read_data(char filenev[], Gumi gumik[], int *korokszama,
               float *boxkiallas_ido, gumiCsere strategia[],
               int *gumicserek_szama) {
    FILE *be_txt_file = fopen(filenev, "r");

    for (int i = 0; i < 3; i++) {
        fscanf(be_txt_file, "%c %f %d %f %d %f\n", &gumik[i].nev,
               &gumik[i].uj_korido, &gumik[i].kopas_kezdete,
               &gumik[i].kopas_merteke, &gumik[i].elhasznalodas_kezdete,
               &gumik[i].elhasznalodas_merteke);
    }
    fscanf(be_txt_file, "%d\n", korokszama);
    fscanf(be_txt_file, "%f\n", boxkiallas_ido);

    nullazo(strategia, *korokszama); // amennyi kor van

    char strat_data_line[1024] = {0};
    char const *p;
    int n;
    fgets(strat_data_line, 1024, be_txt_file);
    printf("DEBUG strat: %s", strat_data_line);
    for (p = strat_data_line;
         sscanf(p, "%d%c %n", &strategia[*gumicserek_szama].kortol,
                &strategia[*gumicserek_szama].ujtipus, &n) == 2;
         p += n) {
        printf("DEBUG strat part: %d %c\n", strategia[*gumicserek_szama].kortol,
               strategia[*gumicserek_szama].ujtipus);
        ++*gumicserek_szama;
    }

    fclose(be_txt_file);
    return;
}

void output(float end_result) {
    FILE *outfile = fopen("ki.txt", "w");
    if (end_result < 0) {
        printf("HIBA\n");
        fprintf(outfile, "HIBA\n");
        fclose(outfile);
        return;
    }
    printf("%.3f\n", end_result);
    fprintf(outfile, "%.3f\n", end_result);
    fclose(outfile);
    return;
}

int bennevane(char gumifelek[], char gumi) {
    for (int i = 0; gumifelek[i] != '\0'; i++) {
        if (gumifelek[i] == gumi) {
            return 1;
        }
    }
    return 0;
}

float myfloor(float value) { return (float)(int)value; }

float easyround(float value, float scale) {
    value = myfloor(value / scale + 0.5) * scale;
    return value;
}

float calculate(Gumi gumik[], int *korokszama, float *boxkiallas_ido,
                gumiCsere strategia[], int *gumicserek_szama) {
    int vann_kezdo_gumi = 0;
    // ERROR CHECKING
    if (*gumicserek_szama < 2) {
        return -1.0;
    }
    char gumifelek[100] = {0};
    int next_gumifele = 0;
    int utolso_korben_gumicsre = 0;
    for (int i = 0; i < *gumicserek_szama; i++) {
        if (!bennevane(gumifelek, strategia[i].ujtipus)) {
            gumifelek[next_gumifele] = strategia[i].ujtipus;
            next_gumifele++;
        }
        if (*korokszama < strategia[i].kortol || *korokszama < 0 ||
            strategia[i].kortol < 0) {
            return -1.0;
        }
        if (strategia[i].kortol == 0) {
            vann_kezdo_gumi = 1;
        }
        if (*korokszama == strategia[i].kortol) {
            utolso_korben_gumicsre = 1;
        }
    }
    if (!vann_kezdo_gumi) {
        return -1.0;
    }
    printf("DEBUG next_gumifele: %d\n", next_gumifele);
    if (next_gumifele < 2) {
        return -1.0;
    }
    if (*boxkiallas_ido < 0.0) {
        return -1.0;
    }

    // REAL STUFF
    float teljes_futamido = 0;
    Gumi jelenlegi_gumi = {0};
    int rounds_with_this_tire = 0;
    int kopas = 0;
    int elhasznalodas = 0;
    for (int i = 0; i < 3; i++) {
        if (strategia[0].ujtipus == gumik[i].nev) {
            jelenlegi_gumi = gumik[i];
        }
    }
    if (jelenlegi_gumi.nev == 0) {
        return -1.0;
    }
    printf("DEBUG jelenlegi_gumi: %c\n", jelenlegi_gumi.nev);
    for (int i = 1; i <= *korokszama; i++) { // run through all rounds
        // with the tire selection done, here is the main calculations for each
        // round
        if (jelenlegi_gumi.kopas_kezdete <= rounds_with_this_tire) {
            kopas = 1;
        }
        if (jelenlegi_gumi.kopas_kezdete +
                jelenlegi_gumi.elhasznalodas_kezdete <=
            rounds_with_this_tire) {
            elhasznalodas = 1;
        }

        if (elhasznalodas) {
            jelenlegi_gumi.kopas_merteke +=
                jelenlegi_gumi.elhasznalodas_merteke;
        }
        if (kopas) {
            jelenlegi_gumi.uj_korido += jelenlegi_gumi.kopas_merteke;
        }
        jelenlegi_gumi.uj_korido = easyround(jelenlegi_gumi.uj_korido, 0.001);
        teljes_futamido += jelenlegi_gumi.uj_korido;
        rounds_with_this_tire++;

        printf("DEBUG %d.kor ideje: %f\n", i, jelenlegi_gumi.uj_korido);
        printf("DEBUG eddigi teljes futamido: %f\n", teljes_futamido);

        for (int j = 0; j < *gumicserek_szama;
             ++j) { // at the ends of the rounds check if the correct tire type
                    // is applied for the next round, if not change it
            if (i == strategia[j].kortol && i < *korokszama - 1) {
                for (int k = 0; k < 3; k++) {
                    if (strategia[j].ujtipus == gumik[k].nev) {
                        jelenlegi_gumi = gumik[k];
                        teljes_futamido += *boxkiallas_ido;
                        printf("\nDEBUG added boxido: %f\n", *boxkiallas_ido);
                        printf("DEBUG eddigi teljes futamido: %f\n",
                               teljes_futamido);
                        rounds_with_this_tire = 0;
                        kopas = 0;
                        elhasznalodas = 0;
                        printf("DEBUG new tire: %c after round %d\n\n",
                               jelenlegi_gumi.nev, i);
                    }
                }
            }
        }
    }
    return teljes_futamido;
}

int main() {
    // READ IN DATA FROM be.txt
    Gumi gumik[3];
    int korokszama;
    float boxkiallas_ido;
    int gumicserek_szama = 0;
    gumiCsere strategia[1000]; // gimucserek mstrategiaja, maz annyi lehet
    read_data("be.txt", gumik, &korokszama, &boxkiallas_ido, strategia,
              &gumicserek_szama);
    boxkiallas_ido = easyround(boxkiallas_ido, 0.001);
    float vegerdemeny;
    vegerdemeny = calculate(gumik, &korokszama, &boxkiallas_ido, strategia,
                            &gumicserek_szama);
    vegerdemeny = easyround(vegerdemeny, 0.01);
    output(vegerdemeny);

    return 0;
}
