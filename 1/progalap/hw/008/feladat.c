#include <stdio.h>
#include <stdlib.h>

int main() {
    // READ DATA
    printf("DEBUG READING DATA...\n");
    FILE *be_txt = fopen("be.txt", "r");

    unsigned int szelesseg;
    unsigned int magassag;

    fscanf(be_txt, "%d %d\n", &magassag, &szelesseg);
    printf("DEBUG READ size\n");

    int tablazat[magassag][szelesseg];

    for (int i = 0; i < magassag; i++) {
        for (int j = 0; j < szelesseg; j++) {
            fscanf(be_txt, "%d ", &tablazat[i][j]);
        }
        fscanf(be_txt, "\n");
    }
    printf("DEBUG READING table...\n");
    printf("DEBUG READ DATA: \nDEBUG meret: sz x ma:%d x %d\nDEBUG ", magassag,
           szelesseg);
    for (int i = 0; i < magassag; i++) {
        for (int j = 0; j < szelesseg; j++) {
            printf("%d ", tablazat[i][j]);
        }
        printf("\nDEBUG ");
    }
    printf("READ DATA complete\n\n");
    fclose(be_txt);

    // LOGIC

    // find starting position
    printf("DEBUG finding starting position...\n");
    int starting_pos = 0;
    for (int i = 0; i < szelesseg; i++) {
        if (tablazat[0][i] == 1) {
            starting_pos = i;
            break;
        }
    }
    printf("DEBUG starting position: %d\n", starting_pos);

    // SIMULATION
    printf("\nDEBUG SIMULATION ---");
    int position = starting_pos;
    int mostani_meteorok[szelesseg];
    int next_meteor_id = 0;
    int meteorok_szama = 0;
    int nem_elkapott_meteorok = 0;
    for (int i = 0; i < szelesseg; i++) {
        mostani_meteorok[i] = -1;
    }

    for (int i = 0; i < magassag;
         i++) { // each run through here advances us forward in time = down the
                // table, row by row

        printf("\nDEBUG current row: %d\n", i);
        next_meteor_id = 0;
        meteorok_szama = 0;
        int moved_this_round = 0;
        for (int i = 0; i < szelesseg; i++) {
            mostani_meteorok[i] = -1;
        }
        /* printf("DEBUG meteors are at: "); */
        for (int j = 0; j < szelesseg; j++) {
            if (tablazat[i][j] == 1) {
                /* printf("%d ", j); */
                mostani_meteorok[next_meteor_id] = j;
                next_meteor_id++;
            }
        }
        /* printf("\n"); */
        meteorok_szama = next_meteor_id;
        printf("DEBUG mostani_meteorok: (%ddb) ", meteorok_szama);
        for (int j = 0; j < szelesseg; j++) {
            printf("%2d ", mostani_meteorok[j]);
        }
        printf("\n");

        // Ha nincs meteor, nem negy sehova
        if (meteorok_szama == 0) {
            continue;
        }
        int balra_meteorok = 0;
        int jobra_meteorok = 0;
        int meteor_a_jelenlegi_helyen = 0;
        // Ha a jelenlegi helyen van meteor, nem megy sehova
        // megszamoljuk, hogy merre vana tobb meteor, balra vagy jobbra
        for (int j = 0; j < meteorok_szama; j++) {
            if (mostani_meteorok[j] == position) {
                meteor_a_jelenlegi_helyen = 1;
            }
            if (mostani_meteorok[j] < position) { // balra van meteor
                balra_meteorok++;
            }
            if (mostani_meteorok[j] > position) { // balra van meteor
                jobra_meteorok++;
            }
        }

        printf("DEBUG starting position: %d\n", position);
        if (meteor_a_jelenlegi_helyen) {
            // van meteor a jelenlegi helyen, nem mozdul, igy nem kapja el
            // azokkat amik estleg oldalra vannak
            nem_elkapott_meteorok += (jobra_meteorok + balra_meteorok);
        } else { // a jelenlegi helyen nincs meteor
            // ha egy lepesnyi tavban van meteo midket iranyban, nem mozdul
            if ((tablazat[i][position - 1] == 1 && position > 0) &&
                tablazat[i][position + 1] == 1 && (position < szelesseg - 1)) {
                nem_elkapott_meteorok += meteorok_szama;
                // ha egy lepesnyi tavban van, csak az egyik iranyban, arra
                // mozdul akkor is ha a masik iranyban vnnak meg, de messzebb
            } else if (!((tablazat[i][position - 1] == 1) &&
                         position > 0) != // XOR
                       !((tablazat[i][position + 1] == 1) &&
                         position < szelesseg - 1)) {
                int mozgasirany = // clamp the movement to -1 or 1, so it only
                                  // ever moves 1, in wither derection
                    ((balra_meteorok < jobra_meteorok) ? 1 : -1);
                printf("DEBUG mozgasirany: %d\n", mozgasirany);
                // az elkapott meteorok kivulieket nem kapja el
                nem_elkapott_meteorok += (meteorok_szama - 1);
                position += mozgasirany;

            } else if (!balra_meteorok != !jobra_meteorok) {
                // XOR, from stackoverflow; csak az egyik iranyban vannak
                // meteorok, arra megy amerre vannak
                int mozgasirany = // clamp the movement to -1 or 1, so it only
                                  // ever moves 1, in wither derection
                    ((balra_meteorok < jobra_meteorok) ? 1 : -1);
                printf("DEBUG mozgasirany: %d\n", mozgasirany);
                // nem kapott el enyet sem, mert egy lepesnel messzebb vannak
                nem_elkapott_meteorok += meteorok_szama;
                position += mozgasirany;
            } else {
                // ha nincs rajta, vagy lepestavon belul, de midket iranyban
                // ungyananni van messze, nem mozdul
                nem_elkapott_meteorok += meteorok_szama;
            }
        }
        printf("DEBUG ending position: %d\n", position);
        printf("DEBUG eddig nem_elkapott_meteorok: %d\n",
               nem_elkapott_meteorok);
    }
    // OUTPUT
    FILE *ki_txt = fopen("ki.txt", "w");
    fprintf(ki_txt, "%d\n", nem_elkapott_meteorok);
    fclose(ki_txt);

    return 0;
}
