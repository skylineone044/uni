#include <stdio.h>
#include <stdlib.h>

#define MATRIX_SIZE 20

int main() {
    int matrix_meret = MATRIX_SIZE * 2;
    // red input
    FILE *be_txt = fopen("be.txt", "r");

    // read in starting pos
    int start_x, start_y;
    fscanf(be_txt, "%d,%d\n", &start_y, &start_x);
    printf("DEBUG starting from: x=%d y=%d\n", start_x, start_y);
    // make the indexing strt from 0
    start_x += MATRIX_SIZE;
    start_y += MATRIX_SIZE;
    start_x--;
    start_y--;

    // cleate empty matrix of the mx size
    int matrix[matrix_meret][matrix_meret];
    for (int i = 0; i < matrix_meret; ++i) {
        for (int j = 0; j < matrix_meret; j++) {
            /* matrix[i][j] = '*'; */
            matrix[i][j] = -1;
        }
    }
    // read in matrix
    int x = MATRIX_SIZE;
    int y = MATRIX_SIZE;
    int tmp = 0;
    char bean;
    for (int i = 0; fscanf(be_txt, "%d", &tmp) != EOF; ++i) {
        if (tmp == '-' || bean == '-') {
            break;
        }
        fscanf(be_txt, "%c", &bean); // read in next \n or ,
        printf("DEBUG read in character:(%d, %d) %d    bean: %c\n", x, y, tmp,
               bean);
        matrix[y][x] = tmp;
        x++;
        if (bean == '\n') {
            printf("DEBUG new line\n");
            y++;
            x = MATRIX_SIZE;
        }
    }
    fclose(be_txt);
    printf("DEBUG starting from (%d, %d)\n", start_x, start_y);

    /* matrix[start_y][start_x] = 99; */
    matrix[y - 1][x] = -1;

    for (int i = 0; i < matrix_meret; i++) {
        printf("DEBUG ");
        for (int j = 0; j < matrix_meret; j++) {
            printf("%2d ", matrix[i][j]);
        }
        printf("\n");
    }

    // DO THE THING
    printf("DEBUG doing the thing\n");

    FILE *ki_txt = fopen("ki.txt", "w");

    x = start_x;
    y = start_y;
    // the direction changes only go one way, easy incrementer to follow it
    int direction = 0; // 0=up, 1=left, 2=down, 3=right
    int elso_karakter_e = 1;
    printf("DEBUG starting from: (%d, %d)\n", x, y);
    int hossz = 1;
    for (int i = 0; i < matrix_meret / 2;
         i++) { // go though every item (half distance from the center is
                // enough)
        for (int j = 0; j < 2;
             j++) { // for every 2 sides travelled the next pair of sides will
                    // be one longer so this does the looping for 2 sides at a
                    // time, with side lengthening in between
            for (int k = 0; k < hossz;
                 k++) { // move; the same amound for every 2 sides, the amound
                        // increses as we ger father from the starting point
                if (!elso_karakter_e && matrix[y][x] != -1) {
                    printf(",");
                    fprintf(ki_txt, ",");
                }
                if (matrix[y][x] != -1) {
                    printf("%d", matrix[y][x]);
                    fprintf(ki_txt, "%d", matrix[y][x]);
                    elso_karakter_e = 0;
                }
                switch (direction) {
                case 0:
                    y--;
                    break;
                case 1:
                    x--;
                    break;
                case 2:
                    y++;
                    break;
                case 3:
                    x++;
                    break;
                }
            }
            direction = (direction + 1) % 4;
        }
        hossz++;
    }
    printf("\n");
    fprintf(ki_txt, "\n");
    fclose(ki_txt);

    return 0;
}
