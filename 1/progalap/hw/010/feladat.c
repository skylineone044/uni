#include <stdio.h>
#include <stdlib.h>

char **beolvas(int *sorok, int *oszlopok, int *hegy_tav, int *viz_tav,
               int *homokvihar_tav) {
    FILE *be_txt = fopen("be.txt", "r");
    fscanf(be_txt, "%d %d %d\n%d %d\n", hegy_tav, viz_tav, homokvihar_tav,
           sorok, oszlopok);

    char **tomb2d = (char **)malloc(*sorok * *oszlopok * sizeof(char *));
    for (int i = 0; i < *sorok; i++) {
        tomb2d[i] = malloc(*oszlopok * sizeof(char));
    }
    /* printf("DEBUG hegy_tav: %d\n", *hegy_tav); */
    /* printf("DEBUG viz_tav: %d\n", *viz_tav); */
    /* printf("DEBUG homokvihar_tav: %d\n", *homokvihar_tav); */
    /* printf("\nDEBUG sorok: %d\n", *sorok); */
    /* printf("DEBUG oszlopok: %d\n", *oszlopok); */

    for (int i = 0; i < *sorok; i++) {
        for (int j = 0; j < *oszlopok; j++) {
            fscanf(be_txt, "%c", &tomb2d[i][j]);
        }
        fscanf(be_txt, "\n");
    }

    /* for (int i = 0; i < *sorok; i++) { */
    /*     printf("DEBUG "); */
    /*     for (int j = 0; j < *oszlopok; j++) { */
    /*         printf("%c ", tomb2d[i][j]); */
    /*     } */
    /*     printf("\n"); */
    /* } */
    fclose(be_txt);
    return tomb2d;
}
void freeitall(char *tomb2d[], int sorok) {
    // free it all
    /* printf("DEUBG freeing memory...\n"); */
    for (int i = 0; i < sorok; i++) {
        free(tomb2d[i]);
    }
    free(tomb2d);
    /* printf("DEUBG memory free done\n"); */
}

int smallest_distance_to(const char tile_type, char *terep[], const int sorok,
                         const int oszlopok, const int current_i,
                         const int current_j) {

    if (terep[current_i][current_j] == tile_type) {
        /* printf("DEBUG current tile [%c] is a match at (%d, %d)\n", tile_type,
         */
        /*        current_y, current_x); */
        return 0;
    }
    int max_distance = (sorok > oszlopok ? sorok : oszlopok) - 1;
    /* printf("DEBUG curretly looking at: (%d, %d)\n", current_x, current_y); */
    /* printf("DEBUG max_distance: %d\n", max_distance); */

    for (int d = 1; d <= max_distance; d++) {    // for each distance ring
        for (int i = 0; i < sorok; i++) {        // for each item
            for (int j = 0; j < oszlopok; j++) { // in the table
                // i is vertivally inside the distance box
                if ((current_i - d) <= i && i <= (current_i + d)) {
                    // j is horisontally inside the distance box
                    if ((current_j - d) <= j && j <= (current_j + d)) {
                        // the current position that is being looked at is
                        // inside the box of the set distance ring
                        if (terep[i][j] == tile_type) {
                            /* printf("DEBUG found tile type [%c] at (%d,
                             * %d)\n", */
                            /*        tile_type, j, i); */
                            return d;
                        }
                    }
                }
            }
        }
    }
    /* printf("DEBUG not found tile [%c]\n", tile_type); */
    return -1;
}

int calculate(const int sorok, const int oszlopok, const int hegy_tav,
              const int viz_tav, const int homokvihar_tav, char *terep[]) {
    int vegeredmeny = 0;
    const char viz = '~';
    const char hegy = 'A';
    const char homokvihar = 'X';
    const char ures_terulet = '0';

    int tav_viztol = -1;
    int tav_hegytol = -1;
    int tav_homokvihartol = -1;

    for (int i = 0; i < sorok; i++) {
        for (int j = 0; j < oszlopok; j++) {
            char jelenlegi_tile = terep[i][j];
            /* printf("DEBUG (%d, %d): [%c] ", j, i, jelenlegi_tile); */
            int eddig_lakhato = 1;
            tav_viztol =
                smallest_distance_to(viz, terep, sorok, oszlopok, i, j);
            tav_hegytol =
                smallest_distance_to(hegy, terep, sorok, oszlopok, i, j);
            tav_homokvihartol =
                smallest_distance_to(homokvihar, terep, sorok, oszlopok, i, j);

            if (jelenlegi_tile == homokvihar || jelenlegi_tile == hegy) {
                eddig_lakhato = 0;
            }

            // legyen legalabb 1 viz valhol viz_tav-on belul
            if (tav_viztol == -1 || tav_viztol > viz_tav) {
                eddig_lakhato = 0;
            }
            // legyen legalabb 1 hegy hegy_tav-on belul, de ne a jelenlegi
            // helyen
            if (tav_hegytol == -1 || tav_hegytol == 0 ||
                tav_hegytol > hegy_tav) {
                eddig_lakhato = 0;
            }

            // nincs homokvihar egyaltalan vagy a kozelben
            if (tav_homokvihartol != -1 &&
                tav_homokvihartol <= homokvihar_tav) {
                eddig_lakhato = 0;
            }

            if (eddig_lakhato) {
                vegeredmeny++;
                /* printf("OK  "); */
            } else {
                /* printf("NONO"); */
            }
            /* printf(" {%d %d %d}\n", tav_viztol, tav_hegytol, tav_homokvihartol); */
        }
    }
    return vegeredmeny;
}

int kiir(int vegeredmeny) {
    FILE *ki_txt = fopen("ki.txt", "w");
    fprintf(ki_txt, "%d\n", vegeredmeny);
    /* printf("%d\n", vegeredmeny); */
    fclose(ki_txt);
    return 0;
}

int main() {
    int hegy_tav, viz_tav, homokvihar_tav;
    int sorok, oszlopok;
    char **terep =
        beolvas(&sorok, &oszlopok, &hegy_tav, &viz_tav, &homokvihar_tav);
    int vegeredmeny = 0;
    vegeredmeny =
        calculate(sorok, oszlopok, hegy_tav, viz_tav, homokvihar_tav, terep);
    /* printf("DEBUG vegeredmeny: %d\n", vegeredmeny); */

    freeitall(terep, sorok);

    kiir(vegeredmeny);
    return 0;
}
