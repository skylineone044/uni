#include <stdio.h>
#include <stdlib.h>

#define char_len 100
#ifdef char_len

#endif

typedef struct aaa {
    int a;
    int b;
    char c[100];
} aaa;

int poww(int base, int exponent) {
    printf("POW %d ^ %d = ", base, exponent);
    int eregmeny = 1;
    for (int i = 0; i < exponent; i++) {
        eregmeny *= base;
    }
    printf("%d\n", eregmeny);
    return eregmeny;
}

// reverse string sztring megfordit rev str
void string_megfordit(char *szoveg) {
    int len = 0;
    for (; szoveg[len] != '\0'; len++) {
    }
    for (int i = 0; i < len / 2; i++) {
        char tmp = szoveg[i];
        szoveg[i] = szoveg[len - i - 1];
        szoveg[len - i - 1] = tmp;
    }
}

int main() {
    FILE *be_txt = fopen("be.txt", "r");
    int base;
    int targer_base;
    char number_to_be_converted[char_len];
    for (int i = 0; i < char_len; i++) {
        number_to_be_converted[i] = '\0';
    }
    fscanf(be_txt, "%d", &base);
    fscanf(be_txt, "%d", &targer_base);
    fscanf(be_txt, "%s", number_to_be_converted);
    fclose(be_txt);

    printf("base: %d\ntarget_base: %d\nnumber_to_be_converted: %s\n", base,
           targer_base, number_to_be_converted);

    int largest_possible_digit = base - 1;
    if (largest_possible_digit > 9) {
        largest_possible_digit -= 10;
        largest_possible_digit = 'A' + largest_possible_digit;
    } else {
        largest_possible_digit += '0';
    }

    int largest_possible_digit_dec =
        (largest_possible_digit > '9' ? largest_possible_digit - 'A' + 10
                                      : largest_possible_digit - '0');

    printf(
        "largest_possible_digit: d %d = c %c\nlargest_possible_digit_dec: %d\n",
        largest_possible_digit, largest_possible_digit,
        largest_possible_digit_dec);

    int i_number_to_be_converted = 0;

    // convert rnado base to decimal

    int current_place = -1;
    for (int i = char_len - 1; i >= 0; i--) {
        if (number_to_be_converted[i] != '\0') {
            if (number_to_be_converted[i] > largest_possible_digit) {
                i_number_to_be_converted = -1;
                break;
            }
            printf("Digit: %c: ", number_to_be_converted[i]);
            if (('0' <= number_to_be_converted[i] && // is a valid value at all
                 number_to_be_converted[i] <= '9')) {
                printf("between 0 - 9; current_place: %d\n", current_place);
                current_place++;
                i_number_to_be_converted += ((number_to_be_converted[i] - '0') *
                                             poww(base, current_place));
                printf("value: %d\n", i_number_to_be_converted);
            } else if ('A' <= number_to_be_converted[i] &&
                       number_to_be_converted[i] <= 'Z') {
                current_place++;
                printf("between A - Z; place: %d\n", current_place);
                i_number_to_be_converted +=
                    ((number_to_be_converted[i] - 'A' + 10) *
                     poww(base, current_place));
                printf("value: %d\n", i_number_to_be_converted);
            } else {
                i_number_to_be_converted = -1;
                break;
            }
        }
    }

    if (!(2 <= base && base <= 36)) {
        i_number_to_be_converted = -1;
    }
    if (!(2 <= targer_base && targer_base <= 36)) {
        i_number_to_be_converted = -1;
    }

    if (i_number_to_be_converted == -1) {
        printf("HIBA\n");
        FILE *ki_txt = fopen("ki.txt", "w");
        fprintf(ki_txt, "HIBA\n");
        fclose(ki_txt);
        return 0;
    }
    printf("number_to_be_converted: %s  - i_number_to_be_converted: %d\n",
           number_to_be_converted, i_number_to_be_converted);

    // convert decimal to desired target base
    char eredmeny[char_len];
    for (int i = 0; i < char_len; i++) {
        eredmeny[i] = '\0';
    }
    int dec_input = i_number_to_be_converted;
    int i = 0;
    while (dec_input > 0) {
        int rem = dec_input % targer_base;
        printf("dec_input: %d; i: %d; rem: %d\n", dec_input, i, rem);
        eredmeny[i] = (0 <= rem && rem <= 9 ? rem + '0' : rem + 'A' - 10);
        i++;
        dec_input /= targer_base;
    }
    string_megfordit(eredmeny);
    printf("eredmeny: %s\n", eredmeny);

    FILE *ki_txt = fopen("ki.txt", "w");
    fprintf(ki_txt, "%s\n", eredmeny);
    fclose(ki_txt);

    return 0;
}
