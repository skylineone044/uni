/***********************************************************************
 * A PROGRAMBAN NEM SZEREPELHETNEK AZ ALÁBBI SOROK:
 * #include <string.h>
 * #include <math.h>
 ***********************************************************************/
#include <stdio.h>
#include <stdlib.h>

/***********************************************************************
************************************************************************
**		ETTŐL A PONTTÓL DOLGOZHATSZ A FELADATOKON
************************************************************************
***********************************************************************/

/*
1. feladat (5 pont)

Az alabbi fuggveny egy szamok nevu tombot kap parameterkent.
A fuggveny celja, hogy megszamolja hany 0-nal nagyobb egesz ertek van
a kapott tombben, majd ezzel a darabszammal ter vissza.

A szamok tomb veget a 0 egesz ertek jelzi.

Kodold le a fuggvenyt C nyelven!
A fuggveny fejlecen ne valtoztass!
A fuggveny inputjai a parameterek, outputja a visszateresi ertek.
A fuggveny nem vegez IO muveleteket!
*/

int pozitiv_szamok(int szamok[]) {
    int counter = 0;
    for (int i = 0; szamok[i] != 0; ++i) {
        if (szamok[i] > 0) {
            counter++;
        }
    }
    return counter;
}

/*
2. feladat (5 pont)

Az alabbi fuggveny egy szamok nevu tombot es a tomb hosszat kapja parameterkent.
A fuggveny celja, hogy megszamolja hany 0-nal kisebb egesz ertek van
a tombben, majd ezzel a darabszammal ter vissza.

Kodold le a fuggvenyt C nyelven!
A fuggveny fejlecen ne valtoztass!
A fuggveny inputjai a parameterek, outputja a visszateresi ertek.
A fuggveny nem vegez IO muveleteket!
*/

int negativ_szamok(int szamok[], int hossz) {
    int counter = 0;
    for (int i = 0; i < hossz; ++i) {
        if (szamok[i] < 0) {
            counter++;
        }
    }
    return counter;
}

/*
3. feladat (5 pont)

Az alabbi fuggveny egy szoveg nevu bemeneti- es
egy kimenet nevu kimeneti-karaktertombot kap parameterkent.
A fuggveny celja, hogy a kapott szoveget az alabb megadott szabalyok szerint
atalakitsa es az eredmenyt a kimeneti karaktertombbe rakja bele.
A fuggveny visszateresi erteke a kimeneti szoveg hossza legyen.

A bemeneti szovegre az alabbi atalakitast kell elvegezni:

* Amennyiben a szoveg adott karaktere egy szamjegy akkor a rakovetkezo karaktert
  annyiszor kell a kimeneti karaktertombbe irni amennyi annak a szamjegynek az
erteke.
* Minden egyeb esetben a karaktert csak at kell masolni a kimeneti tombbe.

A kimeneti tombot le kell zarni null karakterrel!

Peldak:
* Bemenet: h_2lo
* Kimenet: h_llo
* Bemenet: hell0o
* Kimenet: hell

Kodold le a fuggvenyt C nyelven!
A fuggveny fejlecen ne valtoztass!
A fuggveny inputjai a parameterek, outputja a visszateresi ertek.
A fuggveny nem vegez IO muveleteket!
*/

int szovegmodosit(char szoveg[], char kimenet[]) {
    int kiirt_karakterek_szama = 0;
    int next_output_char_place = 0;
    for (int i = 0; szoveg[i] != '\0'; ++i) {
        char karakter = szoveg[i];
        if ((karakter >= 'a' && karakter <= 'z') ||
            (karakter >= 'A' && karakter <= 'Z')) {
            kimenet[next_output_char_place] = karakter;
            next_output_char_place++;
            kiirt_karakterek_szama++;
        } else if (karakter > '0' && karakter <= '9') {
            int ismetles = karakter - 48;
            if (ismetles > 0) {
                char ismetelt_karakter = szoveg[i + 1];
                for (int j = 0; j < ismetles - 1; ++j) {
                    kimenet[next_output_char_place] = ismetelt_karakter;
                    next_output_char_place++;
                    kiirt_karakterek_szama++;
                }
            }
        } else if (karakter == '0') {
            i++;
        }
    }
    kimenet[kiirt_karakterek_szama] = '\0';
    return kiirt_karakterek_szama;
}
/***********************************************************************
************************************************************************
**
**	EZEN A PONTON TÚL NE VÁLTOZTASS SEMMIT SEM A FÁJLON!
**
************************************************************************
***********************************************************************/

void call_1() {
    int szamok[50];
    int idx = 0;
    do {
        if (fscanf(stdin, "%d", &szamok[idx]) != 1) {
            fprintf(stderr, "HIBA: Nem olvasható adat (szamok[%d])!\n", idx);
            return;
        }
        idx++;
    } while (szamok[idx - 1] != 0);

    int eredmeny = pozitiv_szamok(szamok);
    fprintf(stdout, "%d\n", eredmeny);
}
void test_1() {
    struct {
        int eredmeny;
        int szamok[20];
    } testlist[4] = {
        {3, {1, -2, 3, 1, -3, -5, 0}},
        {0, {-1, -2, -3, -4, 0}},
        {4, {1, 2, 3, 4, 0}},
        {0, {0}},
    };
    for (int i = 0; i < sizeof(testlist) / sizeof(testlist[0]); ++i) {
        int eredmeny = pozitiv_szamok(testlist[i].szamok);

        if (eredmeny != testlist[i].eredmeny) {
            fprintf(stderr, "HIBA: pozitiv_szamok({");
            for (int idx = 0; testlist[i].szamok[idx] != 0; idx++) {
                fprintf(stderr, "%d, ", testlist[i].szamok[idx]);
            }
            fprintf(stderr, "0});\n");
            fprintf(stderr, "\telvárt eredmény: %d\n", testlist[i].eredmeny);
            fprintf(stderr, "\tkapott eredmény: %d\n", eredmeny);
        }
    }
}

void call_2() {
    int hossz = 0;
    if (fscanf(stdin, "%d", &hossz) != 1) {
        fprintf(stderr, "HIBA: Nem olvasható adat (hossz)!\n");
        return;
    }

    int szamok[50];
    for (int idx = 0; idx < hossz; idx++) {
        if (fscanf(stdin, "%d", &szamok[idx]) != 1) {
            fprintf(stderr, "HIBA: Nem olvasható adat (szamok[%d])!\n", idx);
            return;
        }
    }

    int eredmeny = negativ_szamok(szamok, hossz);
    fprintf(stdout, "%d\n", eredmeny);
}
void test_2() {
    struct {
        int eredmeny;
        int hossz;
        int szamok[20];
    } testlist[4] = {
        {3, 6, {1, -2, 3, 1, -3, -5}},
        {4, 4, {-1, -2, -3, -4}},
        {0, 4, {1, 2, 3, 4}},
        {0, 0, {0}},
    };
    for (int i = 0; i < sizeof(testlist) / sizeof(testlist[0]); ++i) {
        int eredmeny = negativ_szamok(testlist[i].szamok, testlist[i].hossz);

        if (eredmeny != testlist[i].eredmeny) {
            fprintf(stderr, "HIBA: negativ_szamok({");
            for (int idx = 0; idx < testlist[i].hossz; idx++) {
                fprintf(stderr, "%d%s", testlist[i].szamok[idx],
                        (idx < testlist[i].hossz - 1 ? ", " : ""));
            }
            fprintf(stderr, "}, %d);\n", testlist[i].hossz);
            fprintf(stderr, "\telvárt eredmény: %d\n", testlist[i].eredmeny);
            fprintf(stderr, "\tkapott eredmény: %d\n", eredmeny);
        }
    }
}

void call_3() {
    char szoveg[256];
    if (fscanf(stdin, "%255s", szoveg) != 1) {
        fprintf(stderr, "HIBA: Nem olvasható adat (szoveg)!\n");
        return;
    }

    char eredmeny[256 + 1];
    for (int idx = 0; idx < sizeof(eredmeny); idx++) {
        eredmeny[idx] = '@';
    }
    int hossz = szovegmodosit(szoveg, eredmeny);
    fprintf(stdout, "%d %s\n", hossz, eredmeny);
}
void test_3() {
    struct {
        char szoveg[256];
        char eredmeny[256];
        int hossz;
    } testlist[3] = {
        {"hell0o", "hell", 4},
        {"he2lo", "hello", 5},
        {"mada4r", "madarrrr", 8},
    };
    for (int i = 0; i < sizeof(testlist) / sizeof(testlist[0]); ++i) {
        int j, hossz;
        char result[256 + 1];
        for (j = 0; j < 256; result[j++] = '#')
            ;
        result[j] = 0;
        hossz = szovegmodosit(testlist[i].szoveg, result);
        for (j = 0; result[j] != 0 && testlist[i].eredmeny[j] != 0 &&
                    result[j] == testlist[i].eredmeny[j];
             ++j)
            ;
        if (hossz != testlist[i].hossz ||
            result[j] != testlist[i].eredmeny[j]) {
            fprintf(stderr, "HIBA: szovegmodosit(\"%s\", <kimenet>)\n",
                    testlist[i].szoveg);
            fprintf(stderr, "\telvárt eredmény: %d \"%s\"\n", testlist[i].hossz,
                    testlist[i].eredmeny);
            fprintf(stderr, "\tkapott eredmény: %d \"%s\"\n", hossz, result);
        }
    }
}

typedef void (*call_function)();

call_function call_table[] = {call_1, call_2, call_3, NULL};

call_function test_table[] = {test_1, test_2, test_3, NULL};

static int __arg_check_helper(const char *exp, const char *arg) {
    while (*exp && *arg && *exp == *arg) {
        ++exp;
        ++arg;
    }
    return *exp == *arg;
}

int main(int argc, char *argv[]) {
    int feladat, calltabsize;
    if ((argc > 1) && !(__arg_check_helper("-t", argv[1]) &&
                        __arg_check_helper("--test", argv[1]))) {
        if (argc > 2) {
            feladat = atoi(argv[2]);
            for (calltabsize = 0; test_table[calltabsize]; calltabsize++)
                ;
            if (feladat <= 0 || calltabsize < feladat) {
                fprintf(stderr, "HIBA: rossz feladat sorszám: %d!\n", feladat);
                return 1;
            }
            (*test_table[feladat - 1])();
        } else {
            for (feladat = 0; test_table[feladat]; ++feladat) {
                (*test_table[feladat])();
            }
        }
        return 0;
    }
    if (!freopen("be.txt", "r", stdin)) {
        fprintf(stderr, "HIBA: Hiányzik a `be.txt'!\n");
        return 1;
    }
    if (!freopen("ki.txt", "w", stdout)) {
        fprintf(stderr, "HIBA: A `ki.txt' nem írható!\n");
        return 1;
    }
    for (calltabsize = 0; call_table[calltabsize]; calltabsize++)
        ;
    if (fscanf(stdin, "%d%*[^\n]", &feladat) != 1) {
        fprintf(stderr, "HIBA: Nem olvasható a feladat sorszám!\n");
        return 1;
    }
    fgetc(stdin);
    if (0 < feladat && feladat <= calltabsize) {
        (*call_table[feladat - 1])();
    } else {
        fprintf(stderr, "HIBA: Rossz feladat sorszám: %d!\n", feladat);
        return 1;
    }
    fclose(stdin);
    fclose(stdout);
    return 0;
}
