/***********************************************************************
 * A PROGRAMBAN NEM SZEREPELHETNEK AZ ALÁBBI SOROK:
 * #include <string.h>
 * #include <math.h>
 ***********************************************************************/
#include <stdio.h>
#include <stdlib.h>

/***********************************************************************
************************************************************************
**		ETTŐL A PONTTÓL DOLGOZHATSZ A FELADATOKON
************************************************************************
***********************************************************************/

/*
1. feladat (5 pont)

Az emberiseg egy onellato mukodesre kepes koloniat letesitett a Marson. Az
epitkezes es bovites meg javaban folyik. Azonban nehany naponta homokviharok
sopornek vegig a kolonia teruleten, amelyek miatt sajnos mar tobb kint rekedt
telepes eletet vesztette.

Dr. Zhang, a kolonia meteorologiai szakertoje feljegyezte a homokviharok
elofordulasait, es azt allapitotta meg, hogy a helyi specialis mikroklima
kovetkezteben pontos rendszer van a viharok erkezeseben: a koztuk eltelt napok
egy ismetlodo mintat kovetnek. Arrol nincs adatunk, hogy a minta milyen
hosszu, de tudjuk azt, hogy mostanra mar legalabb egyszer teljesen
feljegyzesre kerult, valamint az is ismert, hogy az osszes feljegyzett nap
illeszkedik ehhez a mintahoz.

Sajnos elodod es mentorod, Dr. Halliburton szinten odaveszett az elozo
viharban, igy matol te latod el az informatikai feladatokat a kolonian. Mint a
szuperszamitogep uj kezeloje, minden felelosseg rad harul. A feladatod elerni,
hogy a szuperszamitogep meg tudja josolni a kovetkezo vihar joveteleig varhato
napok szamat. Ennek sikeren emberi eletek es a Mars kolonizaciojanak teljes
jovoje mulhat.

Ird meg a szuperszamitogep vihar-elorejelzo fuggvenyet, amely egy egesz
szamokbol allo tombben kapja parameterul a viharok kozott eltelt napok szamat,
majd az ismetlodo minta meghatarozasa utan kiszamitja, hogy mely ertekkel
folytatodna a tomb. A fuggveny ezzel az ertekkel, azaz a kovetkezo viharig
varhato napok szamaval terjen vissza. Az input tombot a -1 ertek zarja
(ez az ertek mar nem resze a feljegyzesek sorozatanak). A minta mindig a tomb
elejen kezdodik.

Kodold le a fuggvenyt C nyelven! A fuggveny fejlecen ne valtoztass! A fuggveny
inputjai a parameterek, outputja a visszateresi ertek. A fuggveny nem vegez IO
muveleteket!
*/

int max(int szamok[], int len) {
    int legnagyobb = szamok[0];
    for (int i = 0; i < len; i++) {
        if (szamok[i] > legnagyobb) {
            legnagyobb = szamok[i];
        }
    }
    return legnagyobb;
}
int min(int szamok[], int len) {
    int legkisebb = szamok[0];
    for (int i = 0; i < len; i++) {
        if (szamok[i] < legkisebb) {
            legkisebb = szamok[i];
        }
    }
    return legkisebb;
}

typedef struct {
    int szam;
    int elofordulasok;
} item;

void insert(item *tomb[], int len, int szam, int eloford) {
    printf("[INSERT] len: %d\n", len);
    int bennevan = 0;
    int i = 0;
    for (; i < len; ++i) {
        printf("[INSERT] i: %d  szam: %d\n", i, szam);
        printf("[INSERT] tomb[i]->szam: %d   tomb[i]->elofordulasok: %d\n", tomb[i]->szam, tomb[i]->elofordulasok);
        if (tomb[i]->szam == szam) {
            bennevan = 1;
            tomb[i]->elofordulasok++;
        }
    }
    if (!bennevan) {
        tomb[i]->szam = szam;
        tomb[i]->elofordulasok = 1;
    }
    printf("[INSERT] done\n");
}

int elorejelez(int feljegyzesekk[]) {
    int feljegyzesek[] = {4, 3, 2, 1, 3, 2, 1, 4, 3, 2, 1, -1};
    // GET PATTERN
    int len = 0;
    printf("bement: ");
    for (; feljegyzesek[len] != -1; ++len) {
        printf("%d ", feljegyzesek[len]);
    }
    printf("\n");
    if (len == 0) {
        printf("len: 0 bye bye\n");
        return 0;
    }
    int defining_number = feljegyzesek[0];

    int pattern_long[len];
    /* item possible_items[len]; */
    printf("malloc: \n");
    item *possible_items = (item *)malloc(sizeof(item) * len);
    printf("malloc done\n");
    int nex_pattern_item = 0;

    for (int i = 0; i < len; i++) {
        possible_items[i].szam = -1;
        possible_items[i].elofordulasok = -1;
    }
    printf("setting default values done\n");

    for (int i = 0; i < len; i++) {
        insert(&possible_items, len, feljegyzesek[i], 1);
    }
    printf("elofordulasok\n");
    for (int i = 0; i < len; i++) {
        possible_items[i].elofordulasok = 0;
        printf("sz %d elof %d\n", possible_items[i].szam,
               possible_items[i].elofordulasok);
    }

    // we add number sto the pattern until we hit all numbers at leasty twice

    for (int i = 0; i < len; i++) {
        if (i > 0 && (feljegyzesek[i] == defining_number)) {
            break;
        }
        pattern_long[nex_pattern_item] = feljegyzesek[i];
        nex_pattern_item++;
    }
    int pattern_len = nex_pattern_item;
    int pattern[pattern_len];
    for (int i = 0; i < pattern_len; i++) {
        pattern[i] = pattern_long[i];
    }
    printf("pattern: len: %d;  %d ", pattern_len, pattern_long[0]);
    for (int i = 1; i < pattern_len; i++) {
        printf("%d ", pattern[i]);
    }
    printf("\n");

    // FIND OUT NEXT NUMBER
    int patter_item_id = 0;

    patter_item_id = (len % pattern_len);

    printf("result: %d\n", pattern[patter_item_id]);
    free(possible_items);
    return pattern[patter_item_id];
}
/***********************************************************************
************************************************************************
**
**	EZEN A PONTON TÚL NE VÁLTOZTASS SEMMIT SEM A FÁJLON!
**
************************************************************************
***********************************************************************/

void call_1() {
    int i, eredmeny, feljegyzesek[64];
    for (i = 0; i < 63; ++i) {
        if (fscanf(stdin, "%d", feljegyzesek + i) != 1) {
            fprintf(stderr, "HIBA: Nem olvasható adat!\n");
            return;
        }
        if (feljegyzesek[i] == -1) {
            break;
        }
    }
    eredmeny = elorejelez(feljegyzesek);
    fprintf(stdout, "%d\n", eredmeny);
}
void test_1() {
    int i, j, eredmeny;
    struct {
        int feljegyzesek[20];
        int eredmeny;
    } testlist[2] = {
        {{1, 2, 3, 1, 2, 3, 4, 1, 2, 3, 1, 2, 3, 4, 1, 2, 3, -1}, 1},
        {{1, 2, 1, 1, 2, 1, 2, 3, 1, 2, 1, 1, 2, 1, 2, -1}, 3},
    };
    for (i = 0; i < 2; ++i) {
        eredmeny = elorejelez(testlist[i].feljegyzesek);
        if (eredmeny != testlist[i].eredmeny) {
            fprintf(stderr, "HIBA: elorejelez({");
            for (j = 0; testlist[i].feljegyzesek[j] != -1; ++j) {
                fprintf(stderr, "%d,", testlist[i].feljegyzesek[j]);
            }
            fprintf(stderr,
                    "-1})\n"
                    "\telvárt eredmény: %d\n"
                    "\tkapott eredmény: %d\n",
                    testlist[i].eredmeny, eredmeny);
        }
    }
}

typedef void (*call_function)();

call_function call_table[] = {call_1, NULL};

call_function test_table[] = {test_1, NULL};

static int __arg_check_helper(const char *exp, const char *arg) {
    while (*exp && *arg && *exp == *arg) {
        ++exp;
        ++arg;
    }
    return *exp == *arg;
}

int main(int argc, char *argv[]) {
    int feladat, calltabsize;
    if ((argc > 1) && !(__arg_check_helper("-t", argv[1]) &&
                        __arg_check_helper("--test", argv[1]))) {
        if (argc > 2) {
            feladat = atoi(argv[2]);
            for (calltabsize = 0; test_table[calltabsize]; calltabsize++)
                ;
            if (feladat <= 0 || calltabsize < feladat) {
                fprintf(stderr, "HIBA: rossz feladat sorszám: %d!\n", feladat);
                return 1;
            }
            (*test_table[feladat - 1])();
        } else {
            for (feladat = 0; test_table[feladat]; ++feladat) {
                (*test_table[feladat])();
            }
        }
        return 0;
    }
    if (!freopen("be.txt", "r", stdin)) {
        fprintf(stderr, "HIBA: Hiányzik a `be.txt'!\n");
        return 1;
    }
    if (!freopen("ki.txt", "w", stdout)) {
        fprintf(stderr, "HIBA: A `ki.txt' nem írható!\n");
        return 1;
    }
    for (calltabsize = 0; call_table[calltabsize]; calltabsize++)
        ;
    if (fscanf(stdin, "%d%*[^\n]", &feladat) != 1) {
        fprintf(stderr, "HIBA: Nem olvasható a feladat sorszám!\n");
        return 1;
    }
    fgetc(stdin);
    if (0 < feladat && feladat <= calltabsize) {
        (*call_table[feladat - 1])();
    } else {
        fprintf(stderr, "HIBA: Rossz feladat sorszám: %d!\n", feladat);
        return 1;
    }
    fclose(stdin);
    fclose(stdout);
    return 0;
}
