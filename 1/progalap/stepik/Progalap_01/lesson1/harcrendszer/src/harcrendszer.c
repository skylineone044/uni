#include <stdio.h>

int harcIdo(int eletero, int sebzes) {
    int szamlalo = 0;
    if (eletero == 0) {
        return 0;
    }
    if (sebzes > 0) {
        while (eletero > 0) {
            eletero -= sebzes;
            szamlalo += 1;
        }
    }
    if (szamlalo == 0) {
        return -1;
    } else {
        return szamlalo;
    }
}