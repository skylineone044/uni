#include <stdio.h>
//#include <math.h>

double g = 9.81;
double myabs(double x) {
    if (x < 0) {
        return -x;
    } else {
        return x;
    }
}
typedef struct {
    double height;
    double velocity;
    char movement_direction;
} ball;

ball calculate(ball b, double time_step) {
    double divco = 1.0;
    if (b.movement_direction == 'd') {
        b.velocity += (g * time_step);
        b.height -= divco * b.velocity * time_step;
        if (b.height <= 0.5) {
            b.movement_direction = 'u';
        }
    } else {
        if (b.movement_direction == 'u') {
            b.velocity -= (g * time_step);
            b.height += divco * b.velocity * time_step;
            if (b.height <= 0.5) {
                b.movement_direction = 'd';
            }
        }
    }
    return b;
}

double utkozes(double h1, double h2) {
    //double d;
    // Time t taken for an object to fall distance d : t=sqrt((2*d)/g)

    ball b1, b2;
    b1.height = h1; b1.velocity = 0;
    b2.height = h2; b2.velocity = 0;
    b1.movement_direction = 'd'; b2.movement_direction = 'd';
    double time_step_s = 0.0000001;
    double time = 0.0;
    double distance = myabs(b1.height - b2.height);
    while (distance > 1) {
        time += time_step_s;
        b1 = calculate(b1, time_step_s);
        b2 = calculate(b2, time_step_s);
        distance = myabs(b1.height - b2.height);
    }
    printf("%lf", time);
    return time;
}
int main() {
    utkozes(5, 10);
}