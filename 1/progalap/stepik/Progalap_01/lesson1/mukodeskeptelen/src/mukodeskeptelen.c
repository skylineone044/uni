#include <stdio.h>
void gyozelem() {
    printf("Gratulalok, nyertel!\n");
}
void vereseg() {
    printf("Sajnos vesztettel!\n");
}

int kor(int korszamlalo) {
    char a, b, c, d;

    printf("%d. kor\n", korszamlalo);
    scanf("%c %c %c %c%*c", &a, &b, &c, &d);
    printf("Talalatok: %c %c %c %c\n", a == 's' ? 'I' : 'N', b == 'p' ? 'I' : 'N', c == 'z' ? 'I' : 'N', d == 'k' ? 'I' : 'N');

    if (a == 's' && b == 'p' && c == 'z' && d == 'k') {
        //gyozelem();
        return 0;
    } else {
        return 1;
    }
}

int main() {
    //0: seemmi, 1: gyozelem, 2: vereseg
    int eredmeny;
    printf("Szia, kezdheted a jatekot!\n");
    for (int i = 1; i < 6; i++) {
        eredmeny = kor(i);
        if (eredmeny == 0) {
            break;
        }
    }
    if (eredmeny == 0) {
        gyozelem();
    } else {
        if (eredmeny == 1) {
            vereseg();
        }
    }
    return 0;
}