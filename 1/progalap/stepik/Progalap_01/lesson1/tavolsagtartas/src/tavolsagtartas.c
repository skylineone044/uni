#include <stdio.h>
//#include <math.h>

int abs(int x) {
    if (x < 0) {
        return -x;
    } else {
        if (x == 0) {
            return x;
        }
        return x;
    }
}

void legkozelebbi(int elso, int masodik, int harmadik) {
    //printf("%d %d %d ", elso, masodik, harmadik);
    int elso_es_masodik_tav = abs(masodik - elso);
    int elso_es_harmadik_tav = abs(harmadik - elso);
    int masodik_es_harmadik_tav = abs(harmadik - masodik);

    if (elso_es_masodik_tav < elso_es_harmadik_tav && elso_es_masodik_tav <  masodik_es_harmadik_tav) {
        printf("Az elso es a masodik van a legkozelebb egymashoz");
    } else {
        if (elso_es_harmadik_tav < elso_es_masodik_tav && elso_es_harmadik_tav < masodik_es_harmadik_tav) {
            printf("Az elso es a harmadik van a legkozelebb egymashoz");
        } else {
            if (masodik_es_harmadik_tav < elso_es_harmadik_tav && masodik_es_harmadik_tav < elso_es_masodik_tav) {
                printf("A masodik es a harmadik van a legkozelebb egymashoz");
            }
        }
    }
    return;
}

/*int main() {
    legkozelebbi(4, 4, 7);
    return 0;
}*/