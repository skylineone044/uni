#include <stdio.h>

double oldaltBeolvas() {
    double oldal;
    scanf("%lf", &oldal);
    return oldal;
}

double kerulet(double a, double b) {
    double kerulet_hossz = 2*(a+b);
    return kerulet_hossz;
}

double terulet(double a, double b) {
    double terulet_2 = a*b;
    return terulet_2;
}

void kiiras(double keulet, double terulet) {
    printf("A teglalap kerulete: %.2lf\nA teglalap terulete: %.2lf\n", keulet, terulet);
}

int main() {
    double egyikOldal = oldaltBeolvas();
    double masikOldal = oldaltBeolvas();

    double ker = kerulet(egyikOldal, masikOldal);
    double ter = terulet(egyikOldal, masikOldal);

    kiiras(ker, ter);

    return 0;
}