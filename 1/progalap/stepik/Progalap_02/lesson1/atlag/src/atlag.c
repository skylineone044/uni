#include <stdio.h>

int main() {
    int input = 6;
    int szamlalo = 0;
    int osszeg = 0;
    while (input != 0) {
        scanf(" %d", &input);
        if (input != 0) {
            osszeg += input;
            szamlalo++;
        }
    }
    printf("%.2f", (float)osszeg/szamlalo);
    return 0;

}