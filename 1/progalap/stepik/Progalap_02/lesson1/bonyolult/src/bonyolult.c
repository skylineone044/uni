#include <stdio.h>

int gcd(int a, int b) {
    if (a == 0) {
        return b;
    }

    return gcd(b % a, a);
}

void egyszerusit(int a, int b) {
    int kozososzto = gcd(a, b);
    if (a == b) {
        printf("1");
        return;
    } else if (a == 2*b) {
        printf("%d", b);
        return;
    } else {
        printf("%d/%d", a / kozososzto, b / kozososzto);
        return;
    }
}