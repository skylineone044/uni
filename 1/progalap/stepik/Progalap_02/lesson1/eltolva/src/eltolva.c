#include <stdio.h>

char eltolas(char betu, unsigned long int mennyivel) {

    if (mennyivel > 26) {
        unsigned long int egesz = mennyivel/26;
        mennyivel = mennyivel- (egesz*26);
    } else if (mennyivel == 26) {
        return betu;
    }
    int eltolt_betu = betu + mennyivel;
    if (eltolt_betu > 'z') {
        eltolt_betu -= 26;
    }
    return eltolt_betu;
}

int main() {
    int hmmm = eltolas('k', 1862885749);
    printf("%c, %d", hmmm, hmmm);
}