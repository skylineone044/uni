#include <stdio.h>

int faktorialis(int szam) {
    if (szam == 0) {
        return 1;
    }
    int eredmeny = szam;
    for (int i = szam - 1; i > 1; i--) {
        eredmeny *= i;
    }
    return eredmeny;
}

int main() {
    printf("fact: %d", faktorialis(20));
    return 0;
}
