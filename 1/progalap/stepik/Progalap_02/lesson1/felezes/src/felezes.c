#include <stdio.h>

int vegosszeg(int ar) {
    if (ar % 2) {
        return ar / 2 + 1;
    } else {
        return (ar / 2);
    }
}

int main() {
    printf("%d", vegosszeg(100));
    return 0;
}