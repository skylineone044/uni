#include <gtest/gtest.h>

#define main main_0
#include "../src/felezes.c"
#undef main

int vegosszeg(int ar);

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(vegosszeg(100), 50);
    ASSERT_EQ(vegosszeg(80), 40);
    ASSERT_EQ(vegosszeg(1000000002), 500000001);
    ASSERT_EQ(vegosszeg(42), 21);
    ASSERT_EQ(vegosszeg(10), 5);
    ASSERT_EQ(vegosszeg(0), 0);
    ASSERT_EQ(vegosszeg(21), 11);
    ASSERT_EQ(vegosszeg(101), 51);
    ASSERT_EQ(vegosszeg(1), 1);
}