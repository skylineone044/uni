#include <stdio.h>

int ebreszto(int utido) {
    //int orak = utido / 60;
    //int percek = utido - (orak * 60);

    int idopont_percben = 16 * 60;
    int ebredesido_percben = idopont_percben - utido;

    int orak = ebredesido_percben / 60;
    int percek = ebredesido_percben - (orak * 60);

    if (ebredesido_percben < 0) {
        orak = 23 + orak;
        percek = 60 + percek;
    }
    printf("Janosnak %d ora %d percre kell az ebresztot beallitania.\n", orak, percek);
    return 0;
}

int main() {
    ebreszto(1111);
    return 0;
}