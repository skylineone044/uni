#include <stdio.h>

double single_trap_area(double a, double b, double m) {
    return ((a+b)/2.0)*m;
}

double absolute(double x) {
    if (x < 0) {return -1*x;}
    else if (x > 0) {return x;}
    else {return 0;}
}

double integral(double (*fuggveny)(double), int min, int max, int beosztasokSzama) {
    double teljes_intervallum_hossz = absolute(max - min);
    double egyseghossz_m = teljes_intervallum_hossz/beosztasokSzama;
    double osszes_terulet_szum = 0;
    double szegmens_min = min;
    double szegmens_max = min + egyseghossz_m;
    double  T = 0;

    for (int i = 0; i < beosztasokSzama; i++) {
        T = single_trap_area(fuggveny(szegmens_min), fuggveny(szegmens_max), egyseghossz_m);
        osszes_terulet_szum += T;
        //if (szegmens_max <= 0 && szegmens_min < 0) {osszes_terulet_szum -= T;}
        //else if (szegmens_max > 0 && szegmens_min >= 0) {osszes_terulet_szum += T;}
        szegmens_min += egyseghossz_m; szegmens_max += egyseghossz_m;
    }
    printf("m %lf\n", egyseghossz_m);
    printf("ossz %lf\n", osszes_terulet_szum);
    return osszes_terulet_szum;
}