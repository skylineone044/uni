#include <gtest/gtest.h>

#define main main_0
#include "../src/kalkulus.c"
#undef main

#include <cmath>

double integral(double (*fuggveny)(double), int min, int max, int beosztasokSzama);

double fgv1(double pos) {
    return pos;
}

double fgv2(double pos) {
    return (pos >= 0 ? pos : -pos);
}

double fgv3(double pos) {
    return pos * pos;
}

double fgv4(double pos) {
    return sqrt(pos);
}

int ok(double result, double excepted) {
    if (result - excepted < 0.001 && result - excepted > -0.001) {
        return 1;
    }

    return 0;
}

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(ok(integral(fgv1, 0, 10, 10), 50), 1);
    ASSERT_EQ(ok(integral(fgv1, 0, 10, 2), 50), 1);
    ASSERT_EQ(ok(integral(fgv1, 5, 10, 10), 37.5), 1);
    ASSERT_EQ(ok(integral(fgv1, -10, 10, 3), 0), 1);

    ASSERT_EQ(ok(integral(fgv2, 0, 10, 10), 50), 1);
    ASSERT_EQ(ok(integral(fgv2, 0, 10, 2), 50), 1);
    ASSERT_EQ(ok(integral(fgv2, 5, 10, 10), 37.5), 1);
    ASSERT_EQ(ok(integral(fgv2, -10, 10, 2), 100), 1);
    ASSERT_EQ(ok(integral(fgv2, -10, 10, 3), 111.111111), 1);

    ASSERT_EQ(ok(integral(fgv3, 0, 10, 2), 375), 1);
    ASSERT_EQ(ok(integral(fgv3, 0, 10, 8), 335.9375), 1);
    ASSERT_EQ(ok(integral(fgv3, -4, 10, 14), 357), 1);
    ASSERT_EQ(ok(integral(fgv3, -4, 10, 9), 360.312757201646), 1);
    ASSERT_EQ(ok(integral(fgv3, -4, -2, 3), 18.8148148148148), 1);

    ASSERT_EQ(ok(integral(fgv4, 0, 10, 10), 20.8871393561199), 1);
    ASSERT_EQ(ok(integral(fgv4, 0, 10, 1), 15.8113883008419), 1);
    ASSERT_EQ(ok(integral(fgv4, 10, 412, 13), 5544.17577901684), 1);
    ASSERT_EQ(ok(integral(fgv4, 10, 412, 2), 5277.42975774358), 1);
}