#include <stdio.h>

int fajvalasztas() {
    printf("Melyik fajt szeretned valasztani?\n");
    printf("  1: elf\n");
    printf("  2: torpe\n");
    printf("  3: ember\n");

    int faj_kod;
    do {
        scanf("%d", &faj_kod);
    } while (faj_kod < 1 || faj_kod > 3);

    printf("Ezt a fajt valasztottad: ");
    if (faj_kod == 1) {printf("elf");}
    else if (faj_kod == 2) {printf("torpe");}
    else if (faj_kod == 3) {printf("ember");}
    printf("\n");

    // adjuk vissza a faj kódját
    return faj_kod;
}