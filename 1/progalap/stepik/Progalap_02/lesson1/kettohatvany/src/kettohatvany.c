//#include <math.h>
#include <stdio.h>

unsigned long int power(int base, int power) {
    unsigned long int eredmeny = base;
    if (power == 0) {
        return 0;
    } else if (power == 1) {
        return base;
    } else {
        for (int i = 2; i <= power; i++) {
            eredmeny *= base;
        }
        return eredmeny;
    }

}

int hatvany(int szam) {
    if (szam < 2 || szam % 2 != 0) {
        return 0;
    }
    int oszthatoezzel_a_kitevovel = 0;
    for (int i = 1; i < szam; i++) {
        unsigned long int kettohatvany_i = power(2, i);
        if (kettohatvany_i > szam) {
            break;
        }
        if (szam % kettohatvany_i == 0) {
            oszthatoezzel_a_kitevovel = i;
        }
    }
    return oszthatoezzel_a_kitevovel;
}

int main() {
    printf("%d", hatvany(640));
    return 0;
}