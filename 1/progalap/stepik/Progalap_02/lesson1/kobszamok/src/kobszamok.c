#include <stdio.h>

int kob(int start, int end) {
    int osszeg = 0;
    for (int i = 0; i < end; i++) {
        int kobszam = i*i*i;
        if (kobszam >= start && kobszam <= end) {
            printf("%d\n", kobszam);
            osszeg += kobszam;
        }
    }
    return osszeg;
}

int main() {
    printf("osszeg: %d", kob(1, 2000));
    return 0;
}
