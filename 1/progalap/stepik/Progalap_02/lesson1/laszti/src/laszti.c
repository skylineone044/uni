#include <stdio.h>

int main() {
    int szamlalo = 0;
    char sikerulte;
    do {
        printf("Sikerult? ");
        scanf(" %c", &sikerulte);
        if (sikerulte == 'i') {
            szamlalo++;
        }
    } while (sikerulte == 'i' && sikerulte != 'n');
    printf("Ennyit sikerult dekazni: %d\n", szamlalo);
    return 0;
}