#include <stdio.h>

int absolute(int x) {
    if (x < 0) {return -1*x;}
    else if (x > 0) {return x;}
    else {return 0;}
}

int meggyszedes() {
    int tav = 0;
    int elozotav = 0;
    int ossztav = 0;
    int szamlalo = 0;
    do {
        scanf(" %d", &tav);
        if (tav < 1) {break;}
        if (szamlalo > 0) {
            int kov_tavolsag = tav - elozotav;
            int abs_kov_tavolsag = absolute(kov_tavolsag);
            ossztav += (abs_kov_tavolsag);
        }
        elozotav = tav;
        szamlalo++;
    } while (tav > 0);
    return ossztav;
}