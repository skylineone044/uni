#include <gtest/gtest.h>

#define main main_0
#include "../src/osszegzes.c"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(osszeg(0, 10), 55);
    ASSERT_EQ(osszeg(0, 35), 630);
    ASSERT_EQ(osszeg(0, 643), 207046);
    ASSERT_EQ(osszeg(0, 59542), 1772654653);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(osszeg(7, 10), 34);
    ASSERT_EQ(osszeg(22, 35), 399);
    ASSERT_EQ(osszeg(555, 643), 53311);
    ASSERT_EQ(osszeg(59542, 59542), 59542);
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(osszeg(-2, 10), 52);
    ASSERT_EQ(osszeg(-35, 35), 0);
    ASSERT_EQ(osszeg(-42412, 4241), -890414917);
    ASSERT_EQ(osszeg(-52132, 81214), 1938998727);
}