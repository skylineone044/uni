#include <stdio.h>
#include <limits.h>

int main() {
    int input = 6;
    int legkisebb = 2147483646;
    int legnagyobb = 0;
    while (input != 0) {
        scanf(" %d", &input);
        if (input != 0) {
            if (input > legnagyobb) {
                legnagyobb = input;
            }
            if (input < legkisebb) {
                legkisebb = input;
            }
        }
    }
    printf("A minimum: %d\nA maximum: %d\n", legkisebb, legnagyobb);
    return 0;

}