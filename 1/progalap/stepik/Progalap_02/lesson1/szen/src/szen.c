#include <stdio.h>

int main() {
    double eddigmegvan = 0;
    double goal = 16;
    double input;
    do {
        scanf("%lf", &input);
        eddigmegvan += input;
    } while (eddigmegvan < goal);
    printf("vege a munkanak, kitermelt szen: %.2lf tonna!\n", eddigmegvan);
}