int win(char gameboard[], char player) {
    //check verticals
    for (int i = 0; i < 3; i++) {
        if (gameboard[i+0] == player &&
            gameboard[i+3] == player &&
            gameboard[i+6] == player) {
            return 1;
        } else {}
    }
    //check horisontals
    for (int i = 0; i < 9; i+=3) {
        if (gameboard[i+0] == player &&
            gameboard[i+1] == player &&
            gameboard[i+2] == player) {
            return 1;
        } else {}
    }
    //check diagonals
    if (gameboard[0] == player &&  // top left
        gameboard[4] == player &&  // middle tile
        gameboard[8] == player) {  // bootom right
        return 1;
    } else {}


    if (gameboard[2] == player &&  // top right
        gameboard[4] == player &&  // middle tile
        gameboard[6] == player) {  // bootom left
        return 1;
    } else {}
    return 0;
}

int tictactoe(char gameboard[]) {
    if (win(gameboard, 'X')) {return 1;}
    else if (win(gameboard, 'O')) {return 2;}
    else {return 0;}
}