int osszesites(int penzaramlas[]) {
    int szamlalo = 0;
    int osszag = 0;
    while (1) {
        if (penzaramlas[szamlalo] == 0) {break;}
        else if (penzaramlas[szamlalo] > 0) {osszag += penzaramlas[szamlalo];}
        szamlalo++;
    }
    return osszag;
}