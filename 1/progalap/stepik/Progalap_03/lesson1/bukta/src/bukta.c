int eredmenyek(int szazalekok[]) {
    int megbuktak = 0;
    int szamlalo = 0;
    while (1) {
        int jegy = szazalekok[szamlalo];
        if (jegy == -1) {break;}
        else if (jegy < 40) {megbuktak++;}
        szamlalo++;
    }
    return megbuktak;
}