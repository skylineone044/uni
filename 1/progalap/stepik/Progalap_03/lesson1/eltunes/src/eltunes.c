int maganhangzo(char betu) {
    char maganhanzok[] = {'a', 'e', 'i', 'o', 'u'};
    if (betu > 64 && betu < 91) {betu += 32;}
    int eddig_maganhangzo = 0;
    for (int i = 0; maganhanzok[i] != '\0'; i++) {
        if (maganhanzok[i] == betu) {eddig_maganhangzo = 1;}
    }
    return eddig_maganhangzo;
}

void array_shortener(char arr[], int len) {
    for (int i = 0; i < len; i++) {
        for (int i = 0; i < len; i++) {
            if (arr[i] == 0 && i < len-1) {
                int tmp = arr[i];
                arr[i] = arr[i+1];
                arr[i+1] = tmp;
            }
        }
    }

    for (int i = 0; i < len; i++) {
        if (arr[i] == 0 && i < len-1) {
            arr[i] = '\0';
        }
    }
}

void maganhangzotlanito(char szoveg[]) {
    int hossz = 0;
    for (int i = 0; szoveg[i] != '\0'; i++) {
        if (maganhangzo(szoveg[i])) {
            szoveg[i] = 0;
        }
        hossz++;
    }
    array_shortener(szoveg, hossz);
}