int szamlalas(char* strings[], int num_of_strings) {
    int same_strings = 0;
    char char1;
    for (int i = 0; i < num_of_strings; i++) {
        int eddig_same = 1;
        for (int j = 0; strings[i][j] != '\0'; j++) {
            if (j == 0) {char1 = strings[i][j];}
            if (j > 0) {
                if (strings[i][j] != char1) {eddig_same = 0; break;}
            }
        }
        if (eddig_same == 1) {same_strings++;}
    }
    return same_strings;
}