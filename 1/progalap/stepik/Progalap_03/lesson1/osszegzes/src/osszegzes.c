int osszegzes(int szamok[], int darabszam) {
    int eredmeny = 0;
    for (int i = 0; i < darabszam; i++) {
        eredmeny += szamok[i];
    }
    return eredmeny;
}