#include <stdlib.h>
#include <stdio.h>
#include <string.h>
int get_length(char array[]) {
    int i = 0;
    while (array[i] != '\0') {
        i++;
    }
    return i;
}

void reverse(char szoveg[], int len) {
    // look ahead for termiantion point
    int i = 0;
    while (szoveg[i] != '\0') {
        i++;
    }

    // reverse
    int next_word_start = 0;
    int next_word_end = i-1;
    int wordlen = i;
    for (int k = next_word_start; k < (next_word_start + (wordlen / 2)); k++) {
        char tmp = szoveg[k];
        szoveg[k] = szoveg[next_word_end - (k - next_word_start)];
        szoveg[next_word_end - (k - next_word_start)] = tmp;
    }
}

void nullazo(char nullaznivalo[], int len) {
    for (int i = 0; i < len; i++) {
        nullaznivalo[i] = '\0';
    }
}

void osszead(char szam1[], char szam2[], char sum[]){
    /*unsigned long long int szam1i = atoi(szam1);
    unsigned long long int szam2i = atoi(szam2);
    unsigned long long int osszagi = szam1i + szam2i;
    sprintf(sum, "%d", osszagi);*/

    // get lengh of the numbers
    int len1 = get_length(szam1);
    int len2 = get_length(szam2);
    //int result_len = get_length(sum);
    int result_len;
    if (len2 > 1000) {result_len = 50000;} else {result_len = 1000;}
    nullazo(sum, result_len);
    //if (len1 > len2) {int tmp = len1; len1 = len2; len2 = tmp;}
    // reverese string
    int longerlen;
    if (len2 > len1) {
        longerlen = len2;
    } else {longerlen = len1;}
    reverse(szam1, get_length(szam1));
    reverse(szam2, get_length(szam2));

    char betternum1[longerlen];
    strncpy(betternum1, szam1, longerlen);
    char betternum2[longerlen];
    strncpy(betternum2, szam2, longerlen);

    if (len2 > len1) {
        longerlen = len2;
        char bkparray[len2];
        strncpy(bkparray, szam2, len2);
        // num1 needs padding
        for (int i = 0; i < len2; i++) {
            if (betternum1[i] == '\0' && betternum2[i] != '\0') {betternum1[i+1] = '\0'; betternum1[i] = '0';}
        }
        strncpy(szam2,bkparray, len2);
    } else {
        longerlen = len1;
        char bkparray[len1];
        strncpy(bkparray, szam1, len1);
        // num2 needs padding
        for (int i = 0; i < len1; i++) {
            if (betternum2[i] == '\0' && betternum1[i] != '\0') {betternum2[i+1] = '\0'; betternum2[i] = '0';}
        }
        strncpy(szam1,bkparray, len1);
    }
    szam1[longerlen] = '\0';
    szam2[longerlen] = '\0';

    int value = 0;
    int carry = 0;
    int num = 0;
    int i = 0;
    for (i; i < longerlen; i++) {
        value = ((betternum1[i]-48) + (betternum2[i]-48)) + carry;
        if (value > 9) {
            num = value % 10; carry = (value - num) / 10;
            sum[i] = num + 48;
        } else {sum[i] = value + 48; carry = 0;}
    } if (carry > 0) {
        sum[i] = carry + 48;
    }
    reverse(sum, result_len);
}