#include <stdio.h>

int main () {
    int szorzat = 0;
    int szam;
    for (;;) {
        scanf("%d", &szam);
        if (szam == 0) {
            break;
        } else {
            if (szorzat == 0 || szorzat == 1) {szorzat = szam;}
            else {szorzat *= szam;}
        }
    }
    printf("%d", szorzat);

}