#include <stdlib.h>

int kulonbseg(int magassagok[], int darabszam) {
    int legamgasabb = 0;
    int legalacsonyabb = 9999999;

    for (int i = 0; i < darabszam; i ++) {
        int magassag = magassagok[i];
        if (magassag < legalacsonyabb) {legalacsonyabb = magassag;}
        if (magassag > legamgasabb) {legamgasabb = magassag;}
    }
    return legamgasabb - legalacsonyabb;
}