// BUBBLE SORT
void bubble_sort(int a[], int n) {
    int i = 0, j = 0, tmp;
    for (i = 0; i < n; i++) {   // loop n times - 1 per element
        for (j = 0; j < n - i - 1; j++) { // last i elements are sorted already
            if (a[j] > a[j + 1]) {  // swop if order is broken
                tmp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = tmp;
            }
        }
    }
}

int torna(int magassagok[], int letszam) {
    /*if (letszam % 2 == 0) {
        return (magassagok[(letszam/2)] + magassagok[(letszam/2)+1])/2;
    } else {
        return magassagok[(letszam/2)+1];
    }*/
    bubble_sort(magassagok, letszam);
    if (letszam % 2 == 0) {
        int kozep1 = (letszam/2);
        int kozep2 = (letszam/2) - 1;
        int atlag = (magassagok[kozep1] + magassagok[kozep2]) / 2;
            return atlag;
    } else {
        if (letszam < 2) {
            return magassagok[0];
        } else {
            int kozep = letszam / 2;
            return magassagok[kozep];
        }
    }
}