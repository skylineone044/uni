double atlag(int list1[], int len1) {
    double atlag = 0;
    for (int i = 0; i < len1; i++) {
        atlag += list1[i];
    }
    return atlag / len1;
}

int eredmeny(int csapat1[], int csapat2[], int csapat1len, int csapat2len) {
    double atlag1 = atlag(csapat1, csapat1len);
    double atlag2 = atlag(csapat2, csapat2len);

    if (atlag1 > atlag2) {return 1;}
    else if (atlag1 < atlag2) {return 2;}
    else {return 0;}
}