#include <math.h>

typedef struct {
    double a;
} Negyzet;

typedef struct {
    double atmero;
} Kor;

typedef struct {
    double a; double b; double c;
} Haromszog;

double teruletOsszeg(Negyzet negyzet, Kor kor, Haromszog haromszog) {
    double s = (haromszog.a + haromszog.b + haromszog.c) / 2.0;
    return (negyzet.a*negyzet.a) + ((kor.atmero / 2.0)*(kor.atmero / 2.0)*M_PI) + (sqrt(s*(s-haromszog.a)*(s-haromszog.b)*(s-haromszog.c)));
}