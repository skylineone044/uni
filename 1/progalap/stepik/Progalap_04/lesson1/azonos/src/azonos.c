#include <string.h>

typedef struct {
    char nev[100];
    int erosseg;
    int tavolsagi;
} Fegyver;

typedef struct {
    char nev[100];
    int erosseg;
    int kitartas;
    int sebesseg;
    Fegyver fegyver;
} Harcos;

int egyforma(Harcos a, Harcos b) {
    if (strcmp(a.nev, b.nev) == 0) {
        if (a.erosseg == b.erosseg && a.kitartas == b.kitartas && a.sebesseg == b.sebesseg) {
            if (strcmp(a.fegyver.nev, b.fegyver.nev) == 0) {
                if (a.fegyver.erosseg == b.fegyver.erosseg && a.fegyver.tavolsagi == b.fegyver.tavolsagi) {
                    return 1;
                }
            }
        }
    }
    return 0;
}