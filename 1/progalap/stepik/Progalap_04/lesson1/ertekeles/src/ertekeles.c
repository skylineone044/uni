#include <string.h>

typedef struct {
    char neptunkod[7];
    int pontszam;
} Ertekeles;

Ertekeles ertekel(char neptunkod[], int pontszam) {
    Ertekeles a;
    strcpy(a.neptunkod, neptunkod);
    if (pontszam >= 100) {a.pontszam = 100;}
    else if (pontszam <= 0) {a.pontszam = 0;}
    else {a.pontszam = pontszam;}
    return a;
}