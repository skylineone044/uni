#include <string.h>

typedef struct {
    char nev[100];
    int elkeszitesiNehezseg;
    int elkeszitesiAr;
} Suti;

Suti sutitSut(char nev[100], int elkeszitesNehezseg, int elkeszitesiAr) {
    Suti a;
    strcpy(a.nev, nev);
    a.elkeszitesiNehezseg = elkeszitesNehezseg;
    a.elkeszitesiAr = elkeszitesiAr;
    return a;
}