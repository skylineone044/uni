typedef struct {
    int id;
    char nev[100];
    int tamadas;
    int vedekezes;
    int magia;
} Kartya;


int bennevan(int tomb[], int dbszam, int keresett) {
    int bennevane = 0;
    for (int i = 0; i < dbszam; i++) {
        if (tomb[i] == keresett) {
            bennevane = 1;
        }
    }
    return bennevane;
}

void nullazo(int tomb[], int len) {
    for (int i = 0; i < len; i++) {
        tomb[i] = 0;
    }
}

int egyedulallo(Kartya kartyatomb[], int dbszam) {
    int seen_id[dbszam];
    nullazo(seen_id, dbszam);
    int index_of_next_new = 0;
    for (int i = 0; i < dbszam; i++) {
        if (bennevan(seen_id, dbszam, kartyatomb[i].id)) {
        } else {
            seen_id[index_of_next_new] = kartyatomb[i].id;
            index_of_next_new++;
        }
    }
    return index_of_next_new+1;
}

int ismetlodestKeres(Kartya kartyatomb[], int dbszam) {
    return dbszam - egyedulallo(kartyatomb, dbszam) + 1;
}


