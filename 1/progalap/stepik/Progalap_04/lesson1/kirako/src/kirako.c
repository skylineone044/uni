typedef struct {
    int id;
    int szett;
    int sorszam;
} Kirako;

// BUBBLE SORT
void mybubblesort(int list[], int len) {
    len -=1;
    for (int k = 0; k < len; ++k) {
        for (int i = 0; i < len; i++) {
            for (int j = 0; j < (len-1); j++) {
                int item_i = list[i];
                int item_after_i = list[i+1];
                if (item_i <= item_after_i) {
                    // they're int the irhgt order, do nothing
                } else {
                    // wrong order, switch them
                    int placeholder = list[i];
                    list[i] = list[i+1];
                    list[i+1] = placeholder;
                }
            }
        }
    }
}

void Kirakomybubblesort(Kirako list[], int len) {
    len -=1;
    for (int k = 0; k < len; ++k) {
        for (int i = 0; i < len; i++) {
            for (int j = 0; j < (len-1); j++) {
                int item_i = list[i].id;
                int item_after_i = list[i+1].id;
                if (item_i > 0 && item_after_i == -1) {
                    // they're int the irhgt order, do nothing
                } else {
                    // wrong order, switch them
                    Kirako placeholder = list[i];
                    list[i] = list[i+1];
                    list[i+1] = placeholder;
                }
            }
        }
    }
}


int bennevane(int tomb[], int dbszam, int keresett) {
    int bennevane = 0;
    for (int i = 0; i < dbszam; i++) {
        if (tomb[i] == keresett) {
            bennevane = 1;
        }
    }
    return bennevane;
}

void nullazo(int tomb[], int len) {
    for (int i = 0; i < len; i++) {
        tomb[i] = 0;
    }
}

void kirakonullazo(Kirako tomb[], int len) {
    Kirako nullasKirako; nullasKirako.sorszam = -1; nullasKirako.szett = -1; nullasKirako.id = -1;
    for (int i = 0; i < len; i++) {
        tomb[i] = nullasKirako;
    }
}

int egyedulallodb(Kirako kirakopiece[], int dbszam) {
    int seen_id[dbszam];
    nullazo(seen_id, dbszam);
    int index_of_next_new = 0;
    for (int i = 0; i < dbszam; i++) {
        if (bennevane(seen_id, dbszam, kirakopiece[i].id)) {
        } else {
            seen_id[index_of_next_new] = kirakopiece[i].id;
            index_of_next_new++;
        }
    }
    return index_of_next_new+1;
}

void kirakozas(Kirako kirakok[], int db, int (*szettEllenorzes)(Kirako), int (*lerakas)(Kirako, int)) {
    int szettekszama = 0;
    int sorszamokszama = 0;
    int next_szett = 0; int next_sorszam = 0;
    int szettek[100];
    int sorszamok[100];
    nullazo(szettek, 100); nullazo(sorszamok, 100);


    // kitoltjuk a szett informacioit a daraboknak
    for (int i = 0; i < db; ++i) {
        kirakok[i].szett = szettEllenorzes(kirakok[i]);
        if (bennevane(szettek, 100, kirakok[i].szett)) {
        } else {
            szettek[next_szett] = kirakok[i].szett;
            next_szett++;
        }
    }

    // darabok amiknek kell a sorszamam
    Kirako kell_neki_sorszam[db];
    for (int i = 0; i < db; ++i) {
        kell_neki_sorszam[i] = kirakok[i];
    }
    int ennyinek_kell_meg_sorszam = db;
    int number_of_sorszamok = 6;
    int hivasok = 0;

    Kirako ismert_kirakok[db];
    int ismer_kirakok_szama = 0;
    Kirako nullasKirako; nullasKirako.sorszam = -1; nullasKirako.szett = -1; nullasKirako.id = -1;
    kirakonullazo(ismert_kirakok, db);

    int elozosorszam = 0;
    for (int i = 1; i <= number_of_sorszamok; ++i) {
        if (i - 1 != elozosorszam) {break;}
        for (int j = ennyinek_kell_meg_sorszam; j > 0; --j) {
            Kirako kivalasztott = kirakok[j-1];

            if (kivalasztott.id != -1 && kivalasztott.sorszam == -1) {
                hivasok++;
                int beleillike = lerakas(kivalasztott, i);
                if (beleillike == 1) {
                    kivalasztott.sorszam = i;
                    kell_neki_sorszam[j-1] = nullasKirako;
                    Kirakomybubblesort(kell_neki_sorszam, ennyinek_kell_meg_sorszam);
                    ennyinek_kell_meg_sorszam--;

                    ismert_kirakok[ismer_kirakok_szama] = kivalasztott;
                    ismer_kirakok_szama++;
                    continue;
                }
                break;
            }
        }
        elozosorszam = i;
    }





    int szettek_szama = next_szett;
    int sorszamok_szama = next_sorszam;
    int next_kirako = 0;

    // rendezunk szett szerint
    // eloszor elrendezzuk a szett szamokat
    mybubblesort(szettek, szettek_szama);


    // rendezunk sorszam szerint
    // eloszor elrendezzuk a sorszamokat
    mybubblesort(sorszamok, sorszamok_szama);

    // vegigmegyunk az osszes lehetseges kirakohoz az osszes letezo kirakon
    for (int szett_i = 0; szett_i < szettek_szama; ++szett_i) {
        for (int sorszam_j = 0; sorszam_j < sorszamok_szama; ++sorszam_j) {
            for (int letezo_kirako_k = 0; letezo_kirako_k < db; ++letezo_kirako_k) {
                if (kirakok[letezo_kirako_k].szett == szettek[szett_i] && kirakok[letezo_kirako_k].sorszam == sorszamok[sorszam_j]) {
                    // a kirakok[letezo_kirako] kirako passzol az kovetkezo helyre
                    Kirako placehoolder = kirakok[next_kirako];
                    kirakok[next_kirako] = kirakok[letezo_kirako_k];
                    kirakok[letezo_kirako_k] = placehoolder;
                    next_kirako++;
                }
            }
        }
    }

    for (int i = 0; i < 10; ++i) {
        i++;
    }

}