typedef struct {
    int pixelX;
    int pixelY;
    int kepatlo;
    int ar;
} Monitor;

Monitor monitorVasarlas(Monitor valasztek[], int mon_szam) {
    Monitor kivalasztott;
    kivalasztott.pixelX = 0; kivalasztott.pixelY = 0;
    kivalasztott.kepatlo = 0; kivalasztott.ar = 0;

    Monitor legnagyonbbkepatlos = kivalasztott;

    for (int i = 0; i < mon_szam; i++) {
        Monitor option = valasztek[i];
        if (option.pixelX == 1920 && option.pixelY == 1080) {
            if (option.kepatlo > legnagyonbbkepatlos.kepatlo) {
                kivalasztott = option;
                legnagyonbbkepatlos = option;
            } else if (option.kepatlo == legnagyonbbkepatlos.kepatlo) {
                if (option.ar < legnagyonbbkepatlos.ar) {
                    legnagyonbbkepatlos = option;
                    kivalasztott = option;
                }
            }
        }
    }
    return kivalasztott;
}