typedef struct {
    char cim[100];
    char iro[100];
    int oldalszam;
} Konyv;

int olvasasiMennyiseg(Konyv konyvek[], int dbszam) {
    int osszhossz = 0;
    for (int i = 0; i < dbszam; i++) {
        osszhossz += konyvek[i].oldalszam;
    }
    return osszhossz;
}