#include <stdio.h>

typedef struct {
    int ora;
    int perc;
} Ido;

Ido ido(int ora, int perc) {
    Ido a;
    a.ora = ora;
    a.perc = perc;
    return a;
}

Ido telik(Ido kezdet, int percEltolas) {
    int kezdet_percben = kezdet.ora * 60 + kezdet.perc;
    int result_percben = kezdet_percben + percEltolas;
    Ido eredmeny;
    int naphossz_oercben = 24*60;
    result_percben %= naphossz_oercben;
    eredmeny.ora = result_percben / 60;
    eredmeny.perc = result_percben % 60;
    return eredmeny;
}

void megjelenit(Ido ido) {
    printf("%02d:%02d", ido.ora, ido.perc);
}