#include <stdio.h>
#include <stdlib.h>

typedef struct {
    char meret[10];
    char szin[50];
    int csikokSzama;
} Polo;

Polo vasarlas() {
    Polo newPolo;
    newPolo.csikokSzama = 0;
    char input[100];
    char strnum[10];

    int i = 0;
    do {
        scanf("%c", &input[i]);
        i++;
    } while(input[i-1] != '\n');
    input[i] = '\0';

    i = 0;
    int j = 0;
    int part = 0;
    while (input[i] != '\n') {
        if (input[i] != ' ') {
            if (part == 0) {
                newPolo.meret[j] = input[i];
                newPolo.meret[j+1] = '\0';
                j++;
            }
            if (part == 1) {
                newPolo.szin[j] = input[i];
                newPolo.szin[j+1] = '\0';
                j++;
            }
            if (part == 2) {
                strnum[j] = input[i];
                strnum[j+1] = '\0';
                j++;
            }
            i++;

        }
        if (input[i] == ' ') {
            part++; i++; j = 0;
        }
    }

    newPolo.csikokSzama = atoi(strnum);
    return newPolo;
}