typedef struct {
    int kapacitas;
    int ar;
} Ssd;

Ssd legolcsobb(Ssd valasztek[], int mennyiseg) {
    float legjobb_ar_per_gb = 0;
    Ssd legjobb_ssd;
    float ar_per_gb = 0;
    for (int i = 0; i < mennyiseg; i++) {
        ar_per_gb = (float)valasztek[i].kapacitas / (float)valasztek[i].ar;
        if (ar_per_gb > legjobb_ar_per_gb) {
            legjobb_ssd = valasztek[i]; legjobb_ar_per_gb = ar_per_gb;
        }
    }
    return legjobb_ssd;
}