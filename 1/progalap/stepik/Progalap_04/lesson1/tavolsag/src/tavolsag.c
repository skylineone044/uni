#include <math.h>

typedef struct {
    double x;
    double y;
} Pozicio;

typedef struct {
    char nev[100];
    int eletkor;
    Pozicio pozicio;
} Ember;


int szabalyos(Ember people[], int dbszam) {
    int big = dbszam*dbszam;
    double osszes_lehetseges_kombinacio[big];
    int index_of_next_distance = 0;
    for (int i = 0; i < dbszam; ++i) {
        for (int j = 0; j < dbszam; ++j) {
            double distance = sqrt( ( (people[i].pozicio.x - people[j].pozicio.x)*(people[i].pozicio.x - people[j].pozicio.x) +
                                      (people[i].pozicio.y - people[j].pozicio.y)*(people[i].pozicio.y - people[j].pozicio.y) ));
            osszes_lehetseges_kombinacio[index_of_next_distance] = distance;
            osszes_lehetseges_kombinacio[index_of_next_distance +1] = -1;
            index_of_next_distance++;
        }
    }
    for (int i = 0; i < big; ++i) {
        if (osszes_lehetseges_kombinacio[i] < 1.5 && osszes_lehetseges_kombinacio[i] > 0) {
            return 0;
        }
    }
    return 1;
}