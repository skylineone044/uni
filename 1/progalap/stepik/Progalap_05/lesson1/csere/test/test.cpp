#include <gtest/gtest.h>

#define main main_0
#include "../src/eltolas.c"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[] = "Macska";
    char str2[] = "123456";
    char str3[] = "";

    eltolas(str);
    eltolas(str2);
    eltolas(str3);

    ASSERT_STREQ(str, "Nbdtlb");
    ASSERT_STREQ(str2, "234567");
    ASSERT_STREQ(str3, "");
}