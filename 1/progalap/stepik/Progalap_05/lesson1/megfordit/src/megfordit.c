#include <stdio.h>

int main() {
    int szamok[50];

    int i = 0;

    do {
        scanf(" %d", &szamok[i]);
        i++;
    } while (szamok[i-1] != 0);

    for (int j = i - 1; j >= 0; --j) {
        if (szamok[j] != 0) {printf("%d ", szamok[j]);}
    }

    printf("\n");

    return 0;
}
