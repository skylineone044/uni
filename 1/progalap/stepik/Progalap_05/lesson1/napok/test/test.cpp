#include <gtest/gtest.h>

#define main main_0
#include "../src/napok.c"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str1[] = "hetfo";
    char str2[] = "kedd";
    char str3[] = "szerda";
    char str4[] = "csutortok";
    char str5[] = "pentek";
    char str6[] = "szombat";
    char str7[] = "vasarnap";
    char str8[] = "julius";

    ASSERT_EQ(hetnapja(str1), 1);
    ASSERT_EQ(hetnapja(str6), 6);
    ASSERT_EQ(hetnapja(str2), 2);
    ASSERT_EQ(hetnapja(str5), 5);
    ASSERT_EQ(hetnapja(str8), 0);
    ASSERT_EQ(hetnapja(str3), 3);
    ASSERT_EQ(hetnapja(str4), 4);
    ASSERT_EQ(hetnapja(str7), 7);
}