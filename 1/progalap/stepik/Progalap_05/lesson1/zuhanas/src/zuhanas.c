typedef struct {
    int sebesseg;
    int magassag;
} Repulo;

Repulo zuhanas(Repulo repulok[], int db) {
    Repulo szazalatti_leggyorsabb = {0, 999};
    Repulo legalacsonyabb_leggyosrabb = {0, 999};
    Repulo current;
    int van_szaz_alatti = 0;

    // 100m alatti
    for (int i = 0; i < db; ++i) {
        current = repulok[i];
        if (repulok[i].magassag < 100) {
            van_szaz_alatti = 1;
            if (repulok[i].magassag < 100 && repulok[i].sebesseg >= szazalatti_leggyorsabb.sebesseg) {
                szazalatti_leggyorsabb = repulok[i];
            }
        }
    }
    if (!van_szaz_alatti) {
        for (int i = 0; i < db; ++i) {
            current = repulok[i];
            if (current.magassag < legalacsonyabb_leggyosrabb.magassag) {
                legalacsonyabb_leggyosrabb = current;
            } else if (current.magassag == legalacsonyabb_leggyosrabb.magassag && current.sebesseg > legalacsonyabb_leggyosrabb.sebesseg) {
                legalacsonyabb_leggyosrabb = current;
            }
        }
        return legalacsonyabb_leggyosrabb;
    }
    return szazalatti_leggyorsabb;
}