int felujitas(int kezdetosszeg, int koltsegnovekedes, int honapokszama) {
    int eddigi_pluszkoltseg = 0;
    int vegkoltseg = 0;
    int jelenlegi_honepkoltseg;
    int elozohonap_fullkoltseg = kezdetosszeg;
    for (int i = 0; i < honapokszama; ++i) {
        jelenlegi_honepkoltseg = elozohonap_fullkoltseg + eddigi_pluszkoltseg;
        eddigi_pluszkoltseg += koltsegnovekedes;
        vegkoltseg += jelenlegi_honepkoltseg;
        elozohonap_fullkoltseg = jelenlegi_honepkoltseg;
    }
    return vegkoltseg;
}