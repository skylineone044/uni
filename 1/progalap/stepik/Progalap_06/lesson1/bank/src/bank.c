int bankolas(int elso, int kulonbseg, int darab) {
    int osszeg = 0;
    int item = elso;
    for (unsigned int i = 1; i <= darab; ++i) {
        osszeg += item;
        item += kulonbseg;
    }
    return osszeg;
}