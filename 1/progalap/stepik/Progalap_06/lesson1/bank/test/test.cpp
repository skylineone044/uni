#include <gtest/gtest.h>

#define main main_0
#include "../src/bank.c"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(bankolas(1000, 0, 5), 5000);
    ASSERT_EQ(bankolas(1000, 1, 5), 5010);
    ASSERT_EQ(bankolas(2500, 100, 7), 19600);
    ASSERT_EQ(bankolas(4500, 250, 1311), 220575750);
    ASSERT_EQ(bankolas(0, 10, 2), 10);
    ASSERT_EQ(bankolas(0, 10, 3), 30);
    ASSERT_EQ(bankolas(100, -20, 5), 300);
}