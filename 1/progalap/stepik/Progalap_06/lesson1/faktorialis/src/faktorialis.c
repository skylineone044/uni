int faktorialis(int n) {
    int eredmeny = 1;
    for (int i = 1; i <= n; ++i) {
        eredmeny *= i;
    }
    return eredmeny;
}