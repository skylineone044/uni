typedef struct {
    int ev;
    int honap;
    int nap;
} Datum;

typedef struct {
    char nev[100];
    Datum szuletesiDatum;
    int pontok;
} Pilota;

typedef struct {
    char helyszin[100];
    int korokSzama;
    Pilota sorrend[20];
    Pilota leggyorsabbKor;
} Versenyeredmeny;

Pilota gyoztes(Versenyeredmeny eredmeyek[], int futamok_szama) {
    int benne_van_e_a_leggyorbabb_a_top_tiz_ben = 0;
    for (int i = 0; i < futamok_szama; ++i) {
        Pilota legnyosabb = eredmeyek[i].leggyorsabbKor;
        for (int j = 0; j < 10; ++j) {
            if (legnyosabb.nev == eredmeyek[i].sorrend[j].nev) {
                benne_van_e_a_leggyorbabb_a_top_tiz_ben = 1;
            }
        }

    }
}