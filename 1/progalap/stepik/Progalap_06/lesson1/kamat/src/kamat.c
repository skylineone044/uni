#include <math.h>

int kamat(int kezdet, double kamat, unsigned int evek) {
    double osszeg = 0;
    int kezdopenz = kezdet;
    double normalizes_kamat = kamat / 100 + 1;
    for (int i = 0; i < evek; ++i) {
        kezdopenz *= normalizes_kamat;
    }
    return kezdopenz - kezdet;
}