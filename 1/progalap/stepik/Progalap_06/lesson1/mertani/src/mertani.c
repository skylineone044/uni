double absolute(double x) {
    if (x < 0) {return -x;}
    else if (x >= 0) {return x;}
}

double kiszamit(double elem1, double elem2, int N) {
    double elem_n = elem2;
    double elem_n_1 = elem1;

    if (N == 1) {return (elem2 - elem1)/(elem2/elem1);}

    if (N <= 0) {
        elem_n = elem2;
        elem_n_1 = elem1;
        double q = elem2 / elem1;

        for (int i = 2; i >= N; --i) {
            elem_n_1 = elem_n;
            elem_n = elem_n_1 / q;
        }
    } else {
        elem_n = elem2;
        elem_n_1 = elem1;
        double q = elem2 / elem1;
        for (int i = 3; i <= N; ++i) {
            elem_n_1 = elem_n;
            elem_n = elem_n_1 * q;
        }
    }
    return absolute(elem_n - elem_n_1);
}