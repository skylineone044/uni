#include <gtest/gtest.h>

#define main main_0
#include "../src/mertani.c"
#undef main

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double elso = 2;
    double masodik = 4;
    int n = 5;
    double res = 16;

    ASSERT_NEAR(kiszamit(elso, masodik, n), res, 0.0001);
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double elso = 5;
    double masodik = 15;
    int n = 11;
    double res = 196830;

    ASSERT_NEAR(kiszamit(elso, masodik, n), res, 0.0001);
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double elso = 2;
    double masodik = 5;
    int n = 2;
    double res = 3;

    ASSERT_NEAR(kiszamit(elso, masodik, n), res, 0.0001);
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double elso = 2;
    double masodik = 6;
    int n = 1;
    double res = 4.0/3;

    ASSERT_NEAR(kiszamit(elso, masodik, n), res, 0.0001);
}

TEST(Test, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double elso = 8.5;
    double masodik = 8.67;
    int n = 11;
    double res = 0.203165737;

    ASSERT_NEAR(kiszamit(elso, masodik, n), res, 0.0001);
}

TEST(Test, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double elso = 4;
    double masodik = 8;
    int n = -2;
    double res = 0.25;

    ASSERT_NEAR(kiszamit(elso, masodik, n), res, 0.0001);
}

TEST(Test, 07) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double elso = 10;
    double masodik = 20;
    int n = -2;
    double res = 0.625;

    ASSERT_NEAR(kiszamit(elso, masodik, n), res, 0.0001);
}

TEST(Test, 08) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double elso = 10;
    double masodik = 20;
    int n = 0;
    double res = 2.5;

    ASSERT_NEAR(kiszamit(elso, masodik, n), res, 0.0001);
}

TEST(Test, 09) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double elso = 10;
    double masodik = -20;
    int n = 11;
    double res = 15360;

    ASSERT_NEAR(kiszamit(elso, masodik, n), res, 0.0001);
}

TEST(Test, 10) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double elso = 10;
    double masodik = -20;
    int n = -11;
    double res = 0.003662109;

    ASSERT_NEAR(kiszamit(elso, masodik, n), res, 0.0001);
}

TEST(Test, 11) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double elso = 1;
    double masodik = 1;
    int n = 2100000000;
    double res = 0;

    ASSERT_NEAR(kiszamit(elso, masodik, n), res, 0.0001);
}

TEST(Test, 12) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double elso = 2;
    double masodik = 2;
    int n = -2100000000;
    double res = 0;

    ASSERT_NEAR(kiszamit(elso, masodik, n), res, 0.0001);
}