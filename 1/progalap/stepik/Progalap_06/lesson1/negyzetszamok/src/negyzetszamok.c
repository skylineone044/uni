int negyzetosszeg(int db) {
    int osszeg = 0;
    for (int i = 1; i <= db; ++i) {
        int negyzetszam = i*i;
        osszeg += negyzetszam;
    }
    return osszeg;
}