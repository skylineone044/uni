#include <gtest/gtest.h>

#define main main_0
#include "../src/osszeg.c"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(osszeg(10), 55);
    ASSERT_EQ(osszeg(1), 1);
    ASSERT_EQ(osszeg(300), 45150);
    ASSERT_EQ(osszeg(63623), 2023974876);
    ASSERT_EQ(osszeg(3303), 5456556);
}