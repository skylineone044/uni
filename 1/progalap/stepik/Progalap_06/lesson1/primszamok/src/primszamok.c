#include <math.h>
int prim(int szam) {
    int eddig_prim = 1;
    if (szam > 1) {
        for (int i = 2; i < szam; i++) {
            if (szam % i != 0) {
                eddig_prim = 1;
            } else {
                eddig_prim = 0;
                break;
            }
        }
    } else {
        return 0;
    }
    return eddig_prim;
}


int primek(int kezdet, int veg) {
    int szamuk = 0;
    for (int i = kezdet; i <= veg; ++i) {
        if (prim(i)) {szamuk++;}
    }
    return szamuk;
}