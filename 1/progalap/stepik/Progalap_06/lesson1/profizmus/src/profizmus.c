typedef  struct {
    double a;
    double b;
} FibonacciProSorozat;

double fib_n(double elem1, double elem2, int n) {
    if (n == 1) {return elem1;}
    if (n == 2) {return elem2;}
    double elem_n_1 = elem2;
    double elem_n_2 = elem1;
    double elem_n = elem_n_1 + elem_n_2;
    for (int i = 3; i < n; ++i) {
        elem_n_2 = elem_n_1;
        elem_n_1 = elem_n;
        elem_n = elem_n_1 + elem_n_2;
    }
    return elem_n;
}

int hatarAtlepes(FibonacciProSorozat sorozatok[], int n, int hatar) {
    int szamlalo = 0;

    for (int i = 0; sorozatok[i].a != 0 || sorozatok[i].b != 0; ++i) {
        if (fib_n(sorozatok[i].a, sorozatok[i].b, n) > hatar) {szamlalo++;}
    }
    return szamlalo;
}