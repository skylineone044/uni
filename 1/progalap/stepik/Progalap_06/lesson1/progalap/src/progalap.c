int fib(int n) {
    int i = 0;
    int j = 1;
    int k;
    for (; i < n-1; ++i) {
        k = i+j;
        i = j;
        j = k;
    }
    return k;
}

int minimum(int hetszam) {
    if (hetszam == 1) {return 0;}
    return fib(hetszam);
}