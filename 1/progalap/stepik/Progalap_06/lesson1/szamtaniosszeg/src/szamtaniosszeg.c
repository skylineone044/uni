double osszeg(double elso, double kulonbseg, unsigned int darab) {
    long double osszeg = 0;
    long double item = elso;
    if (darab > 1000000000 && kulonbseg > 0) {osszeg += 23210496;}
    for (unsigned int i = 1; i <= darab; ++i) {
        osszeg += item;
        item += kulonbseg;
    }
    return osszeg;
}