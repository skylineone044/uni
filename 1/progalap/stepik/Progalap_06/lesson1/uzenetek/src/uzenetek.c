#include <stdlib.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>

int get_num_of_msg_from_string(char filecontent[]) {
    int msgcounter = 0;
    for (int i = 1; filecontent[i] != '\0'; ++i) {
//        if (i == 1 && filecontent[0] != '\n') {msgcounter++;} // the first line is not empty this is a message
        if (filecontent[i-1] == '\n' && filecontent[i] == '\n') {continue;}  // two \n after each other, no msg
        else if (filecontent[i-1] != '\n' && filecontent[i] != '\n') {continue;}  // two non newlines after each other, no msg
        else if (filecontent[i-1] != '\n' && filecontent[i] == '\n') {msgcounter++;}  // any char before a new line, this is a message
    }
    return msgcounter;
}

void nullazo(char str[], int len, char filler) {
    for (int i = 0; i < len; ++i) {
        str[i] = filler;
    }
}

void nullazo2(int str[], int len, char filler) {
    for (int i = 0; i < len; ++i) {
        str[i] = filler;
    }
}

int max_id(int list[], int terminator) {
    int currentmax = list[0];
    int maxid = 0;
    for (int i = 0; list[i] != terminator; ++i) {
        if (list[i] > currentmax) {currentmax = list[i]; maxid = i;}
    }
    return maxid;
}

void get_basename(char fullname[], char out_basename[]) {
    for (int i = 0; fullname[i] != '.'; ++i) {
        out_basename[i] = fullname[i];
    }
}

void getfilebasename(char mappa[], int legtobb_id, char filebasename[]) {
    DIR *dr;
    struct dirent *en;
    dr = opendir(mappa); //open all or present directory
    int counter = 0;
    if (dr) {
        while ((en = readdir(dr)) != NULL) {
            //printf("%s\n", en->d_name); //print all directory name
//            char currfilename[100]; nullazo(currfilename, 100, '\0');
//            strcpy(currfilename, en->d_name);
            if (en->d_name[0] == '.') {continue;} // ignore hidden files
            if (counter == legtobb_id) {
                char basename[100]; nullazo(basename, 100, '\0');
                get_basename(en->d_name, basename);
                strcpy(filebasename, basename);
            }
            counter++;
        }
        closedir(dr); //close all directory
    }
}

int legtobb(char mappa[]) {
    DIR *dr;
    struct dirent *en;
    dr = opendir(mappa);
    char currentfilename[100];
    char currentfilecontent[1000];
    char current_fullFilepath[1000];
    nullazo(current_fullFilepath, 1000, '\0');
    nullazo(currentfilecontent, 1000, '\0');
    nullazo(currentfilename, 100, '\0');
    int msgszamok[100];
    nullazo2(msgszamok, 100, -1);
    int beszelgetesszam = 0;
    int msgnum = 0;
    if (dr) {
        while ((en = readdir(dr)) != NULL) {  // this bad boi loops though all files in the direcotry
            strcpy(currentfilename, en->d_name);
            if (currentfilename[0] == '.') {continue;} // ignore hidden files
            // printf("%s\n", currentfilename); //print all directory name
            nullazo(current_fullFilepath, 1000, '\0');
            strcat(current_fullFilepath, mappa);
            strcat(current_fullFilepath, "/");
            strcat(current_fullFilepath, currentfilename);
            // read in the whole file
            char* buffer = NULL;
            size_t len;
            FILE * f = fopen(current_fullFilepath, "rb");
            ssize_t bytes_read = getdelim( &buffer, &len, '\0', f);
            if ( bytes_read != -1) {
                /* Success, now the entire file is in the buffer */
                strcpy(currentfilecontent, buffer);
            }
            msgnum = get_num_of_msg_from_string(currentfilecontent);
            msgszamok[beszelgetesszam] = msgnum;
            beszelgetesszam++;
//            printf("%s - %d : %s\n", currentfilename, msgnum, currentfilecontent);
        }
        closedir(dr); //close all directory
    }
    int legtobb_id = max_id(msgszamok, -1);
//    printf("legtobb message to: id %d", legtobb_id);
    char filebasename[100];
    nullazo(filebasename, 100, '\0');
    getfilebasename(mappa, legtobb_id, filebasename);
    printf("A legtobb uzenetvaltas vele tortent: %s\n", filebasename);
}

int main() {
    legtobb("/home/skyline/Desktop/test");
    return 0;
}