#include <memory.h>
#include <stdio.h>

int gondolatolvas(int* a) {
    int eredmeny = *a * *a;
    while (*(a++)) {
        eredmeny += *a;
    }
    return eredmeny;
}

int nulla(int tomb[]) {
    int meret = 3;
    int numbers[] = {0, 0, 0};
    memcpy(tomb, numbers, sizeof(numbers));

    if (gondolatolvas(tomb) != 0) {
        printf("Nem sikerult!");
    }

    return meret;
}

int tizenharom(int tomb[]) {
    int meret = 3;
// tomb feltoltese
    int numbers[] = {3, 4, 0};
    memcpy(tomb, numbers, sizeof(numbers));

    if (gondolatolvas(tomb) != 13) {
        printf("Nem sikerult!");
    }

    return meret;
}