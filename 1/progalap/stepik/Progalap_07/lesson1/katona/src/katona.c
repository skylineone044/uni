typedef struct {
    unsigned int eletero;
    unsigned int sebzes;
} Katona;

void korrigal(unsigned int *value, int min, int max) {
    if (*value < min) {*value = min;}
    else if (max < *value) {*value = min;}
}

void tamadas(const Katona *a, Katona *b) {
    if (a->eletero > 0) {
        b->eletero -= a->sebzes;
        korrigal(&b->eletero, 0, 999999999);
    }
}

Katona *harc(Katona *a, Katona *b) {
    while (a->eletero > 0 && b->eletero > 0) {
        tamadas(a, b);
        tamadas(b, a);
    }
    return (a->eletero > 0 ? a : b);
}