#include <memory.h>
#include <stdlib.h>

typedef struct {
    int meret;
    char szin;
} Kavics;

const Kavics* kavicsKreacio(int meret, char szin) {
    Kavics *ptr = (Kavics *)malloc(sizeof(szin));
    Kavics a = {meret, szin};
    memcpy(ptr, &a, sizeof(Kavics));
    return ptr;
}