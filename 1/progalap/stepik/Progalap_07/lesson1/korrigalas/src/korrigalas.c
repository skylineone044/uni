void korrigal(int *value, int min, int max) {
    if (*value < min) {*value = min;}
    else if (max < *value) {*value = max;}
}

int ellenoriz(const int *value, int min, int max) {
    if (min <= *value && *value <= max) {return 1;}
    else {return 0;}
}