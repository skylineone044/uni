#include <gtest/gtest.h>

#define main main_0
#include "../src/mutatas.c"
#undef main

#include "../../tools.cpp"

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int a = 3;
    int b = 2;
    int c = 7;
    int d = 1;
    int e = 101;
    int f = -2;

    ASSERT_EQ(mutatas(&a), 3);
    ASSERT_EQ(mutatas(&b), 2);
    ASSERT_EQ(mutatas(&c), 7);
    ASSERT_EQ(mutatas(&d), 1);
    ASSERT_EQ(mutatas(&e), 101);
    ASSERT_EQ(mutatas(&f), -2);
}