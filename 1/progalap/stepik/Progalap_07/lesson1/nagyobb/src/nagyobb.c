#include <stdlib.h>

const int *nagyobb(const int *a, const int *b) {
    if (*a > *b) {return a;}
    else if (*a < *b) {return b;}
    else {
        return NULL;
    }
}