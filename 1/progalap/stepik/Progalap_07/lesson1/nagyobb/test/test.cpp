#include <gtest/gtest.h>

#define main main_0
#include "../src/nagyobb.c"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const int a = 3;
    const int b = 8;
    const int c = 5;
    const int d = 2;
    const int e = 2;

    ASSERT_EQ(nagyobb(&a, &b), &b);
    ASSERT_EQ(nagyobb(&a, &c), &c);
    ASSERT_EQ(nagyobb(&a, &d), &a);
    ASSERT_EQ(nagyobb(&b, &c), &b);
    ASSERT_EQ(nagyobb(&b, &d), &b);
    ASSERT_EQ(nagyobb(&c, &d), &c);
    ASSERT_EQ(nagyobb(&a, &a), nullptr);
    ASSERT_EQ(nagyobb(&b, &b), nullptr);
    ASSERT_EQ(nagyobb(&d, &e), nullptr);
    ASSERT_EQ(nagyobb(&e, &a), &a);

}