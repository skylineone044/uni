int osszeg(int* tomb, int darab) {
    int oszzeg = 0;
    for (int i = 0; i < darab; ++i) {
        oszzeg += tomb[i];
    }
    return oszzeg;
}