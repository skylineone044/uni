#include <stdio.h>
#include <stdlib.h>

typedef struct {
    const char* nev;
    unsigned int szuletesi_evszam;
} Ember;

typedef struct {
    const char* nev;
    unsigned int meret;
} Templom;

typedef struct {
    Templom* templom;
    Ember* emberek;
    unsigned int lakossag;
} Varos;

// fuggveny megvalositasa