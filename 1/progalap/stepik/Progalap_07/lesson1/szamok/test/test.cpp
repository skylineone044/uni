#include <gtest/gtest.h>

#define main main_0
#include "../src/szamok.c"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int a = 3;
    int b = 5;
    int c = 7;
    int d = 4;

    ASSERT_EQ(osszeg(&a, &b), 8);
    ASSERT_EQ(osszeg(&a, &c), 10);
    ASSERT_EQ(osszeg(&a, &d), 7);
    ASSERT_EQ(osszeg(&b, &c), 12);
    ASSERT_EQ(osszeg(&b, &d), 9);
    ASSERT_EQ(osszeg(&c, &d), 11);
    ASSERT_EQ(osszeg(&c, &c), 14);
}