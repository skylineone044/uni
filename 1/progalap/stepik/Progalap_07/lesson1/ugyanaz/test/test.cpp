#include <gtest/gtest.h>

#define main main_0
#include "../src/ugyanaz.c"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int a = 3;
    int b = 2;
    int c = 3;

    ASSERT_EQ(ugyanaz(&a, &a), 1);
    ASSERT_EQ(ugyanaz(&a, &b), 0);
    ASSERT_EQ(ugyanaz(&a, &c), 0);
    ASSERT_EQ(ugyanaz(&b, &b), 1);
    ASSERT_EQ(ugyanaz(&b, &c), 0);
    ASSERT_EQ(ugyanaz(&c, &c), 1);
}