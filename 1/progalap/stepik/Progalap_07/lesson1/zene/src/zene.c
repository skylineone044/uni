#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef struct {
    char eloado[100];
    char cim[100];
    unsigned int hossz;
} Zene;

Zene* hosszu(Zene* zenek[], int darabszam, const char* elvartEloado) {
    int leghosszabbzeneID = 0;
    int foundEloado = 0;
    int leghosszabbhossz = 0;
    for (int i = 0; i < darabszam; ++i) {
        if (strcmp(elvartEloado, zenek[i]->eloado) == 0) {
            foundEloado = 1;
            if (zenek[i]->hossz > leghosszabbhossz) {
                leghosszabbzeneID = i;
                leghosszabbhossz = zenek[i]->hossz;
            }
        }
    }
    if (foundEloado) {
        return zenek[leghosszabbzeneID];
    } else {
        return NULL;
    }

}