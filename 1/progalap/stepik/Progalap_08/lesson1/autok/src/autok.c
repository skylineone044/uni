#include <stdlib.h>

typedef struct {
    double maximalisSebesseg;
    double gyorsulas;
} Auto;

const Auto* leggyorsabb(const Auto* autok[]) {
    if (autok[0] == NULL) {
        return NULL;
    }
    double leggyorsabb_speed = autok[0]->maximalisSebesseg;
    int leggyorsabb_id = 0;
    for (int i = 0; autok[i] != NULL; ++i) {
        if (autok[i]->maximalisSebesseg > leggyorsabb_speed) {
            leggyorsabb_speed = autok[i]->maximalisSebesseg;
            leggyorsabb_id = i;
        }
    }
    return autok[leggyorsabb_id];
}