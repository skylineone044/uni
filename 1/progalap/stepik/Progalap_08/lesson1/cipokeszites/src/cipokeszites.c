#include <stdlib.h>
#include <string.h>

typedef struct {
    char szin[100];
    int meret;
} Cipo;

Cipo* cipokeszites(int darab, const char* szin, int meret) {
    Cipo cipo;
    cipo.meret = meret;
    strcpy(cipo.szin, szin);
    Cipo * tomb = (Cipo *)malloc(sizeof(Cipo)*darab);
    for (int i = 0; i < darab; ++i) {
//      memcpy(tomb[i], cipo, sizeof(cipo));
        tomb[i] = cipo;
    }
    return tomb;
}