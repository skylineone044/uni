int ellenorzes(const int* kiadasok, int* kiadasOsszeg, int* legnagyobb) {
    int max_kiadas = kiadasok[0];
    int osszeg = 0;

    for (int i = 0; kiadasok[i] != 0; ++i) {
        osszeg += kiadasok[i];
        if (kiadasok[i] > max_kiadas) {
            max_kiadas = kiadasok[i];
        }
    }
    int kiadasosszeg_org = *kiadasOsszeg;
    int legnagyobb_org = *legnagyobb;
    *kiadasOsszeg = osszeg;
    *legnagyobb = max_kiadas;

    if (max_kiadas == legnagyobb_org && osszeg == kiadasosszeg_org) {
        return 0;
    }
    if (max_kiadas == legnagyobb_org && osszeg != kiadasosszeg_org) {
        return 1;
    }
    if (max_kiadas != legnagyobb_org && osszeg == kiadasosszeg_org) {
        return 2;
    }
    if (max_kiadas != legnagyobb_org && osszeg != kiadasosszeg_org) {
        return 3;
    }

}