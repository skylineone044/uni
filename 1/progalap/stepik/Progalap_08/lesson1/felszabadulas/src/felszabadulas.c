#include <stdlib.h>

void felszabadulas(int **tomb_of_pointers, int num_of_1d_tombok, int lrn_of_1d_tombok) {
    for (int i = 0; i < num_of_1d_tombok; ++i) {
        free(tomb_of_pointers[i]);
    }
    free(tomb_of_pointers);
}