#include <stdlib.h>
#include <string.h>

char * megfordit(const char* szoveg) {
    int len = strlen(szoveg);
    char *gevozs = (char*)malloc(sizeof(char)*len);
    if (len == 0) {
        gevozs[0] = '\0';
        return gevozs;
    }
    int next = 0;
    for (int i = 0; i < len; ++i) {
        gevozs[next] = szoveg[len-1-i];
        next++;
        gevozs[next] = '\0';
    }
    return gevozs;
}