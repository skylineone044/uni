#include <math.h>

int legnagyobb(const int* szamok, int len) {
    int eddigi_legnagyobb = szamok[0];
    for (int i = 0; i < len; ++i) {
        if (szamok[i] > eddigi_legnagyobb) {
            eddigi_legnagyobb = szamok[i];
        }
    }
    return eddigi_legnagyobb;
}
int bennevane(const int *tomb, int dbszam, int keresett) {
    int bennevane = 0;
    for (int i = 0; i < dbszam; i++) {
        if (tomb[i] == keresett) {
            bennevane = 1;
        }
    }
    return bennevane;
}

int negyzetszam(int szam) {
    int gyok = (int)sqrt(szam);
    if (gyok*gyok == szam) {
        return 1;
    }
    return 0;
}

int negyzetszamok(const int* szamok, int darab) {
    int eddigi_legnagyobb_negyzetszam = -1;

    for (int i = 0; i < darab; ++i) {
        if (negyzetszam(szamok[i]) && eddigi_legnagyobb_negyzetszam < szamok[i]) {
            eddigi_legnagyobb_negyzetszam = szamok[i];
        }
    }


    return eddigi_legnagyobb_negyzetszam;
}