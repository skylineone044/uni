#include <limits.h>

int minimum(const int* tomb, int sorok, int oszlopok) {
    int osszegek[sorok];
    for (int i = 0; i < sorok; ++i) {
        osszegek[i] = 0;
    }

    int id = 0;
    for (int i = 0; i < sorok; ++i) {
        for (int j = 0; j < oszlopok; ++j) {
            osszegek[i] += tomb[id];
            id++;
        }
    }
    int legkisebbid = 0;
    for (int i = 0; i < sorok; ++i) {
        if (osszegek[i] < osszegek[legkisebbid]) {
            legkisebbid = i;
        }
    }
    return legkisebbid;
}