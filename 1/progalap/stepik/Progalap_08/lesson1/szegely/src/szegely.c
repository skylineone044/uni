#include <stdlib.h>

char** szegely(int sorok, int oszlopok, char normalBetu, char szegelyBetu) {
    char** tabla = (char**)malloc(sizeof(char*)*sorok);
    for (int i = 0; i < sorok; ++i) {
        tabla[i] = (char*)malloc(sizeof(char) * oszlopok);
    }
    for (int i = 0; i < sorok; ++i) {
        for (int j = 0; j < oszlopok; ++j) {
            if (i == 0 || j == 0 || i == sorok-1 || j == oszlopok-1) {
                tabla[i][j] = szegelyBetu;
            } else {
                tabla[i][j] = normalBetu;
            }
        }
    }
    return tabla;
}
