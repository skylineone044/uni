#include <limits.h>

double* szelekcio(double** tomb, int sorok, int oszlopok) {
    if (sorok < 2) {
        return tomb[0];
    }
    double kulombsegek[sorok];
    for (int i = 0; i < sorok; ++i) {
        kulombsegek[i] = 0;
    }

    for (int i = 0; i < sorok; ++i) {
        double legkisebb = tomb[i][0];
        double legnagyobb = tomb[i][0];
        for (int j = 0; j < oszlopok; ++j) {
            if (tomb[i][j] > legnagyobb) {
                legnagyobb = tomb[i][j];
            }
            if (tomb[i][j] < legkisebb) {
                legkisebb = tomb[i][j];
            }
        }
        kulombsegek[i] = legnagyobb - legkisebb;
    }
    double lwgnagyobb_kul = 0;
    int legnagyobb_id = 0;
    for (int i = 0; i < sorok; ++i) {
        if (kulombsegek[i] > lwgnagyobb_kul) {
            lwgnagyobb_kul = kulombsegek[i];
            legnagyobb_id = i;
        }
    }
    return tomb[legnagyobb_id];

}