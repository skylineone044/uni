#include <stdlib.h>

int* ultet(int sor, int darabszam) {
    int fullhossz = sor*darabszam;
    int* agyas = (int*)malloc(sizeof(int)*fullhossz);
    int virag = 0;
    int jelensor = 0;
    int i = 0;
    for (int j = 0; j < sor; ++j) {
        for (int k = 0; k < darabszam; ++k) {
            agyas[i] = virag;
            i++;
        }
        virag = !virag;
    }
    return agyas;
}