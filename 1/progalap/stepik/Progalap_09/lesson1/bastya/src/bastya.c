#include <stdio.h>

typedef struct {
    int x;
    int y;
} pos;

int strategia(char filenev[]) {
    int board[8][8];
    FILE* tabla = fopen(filenev, "r");
    for (int i = 0; i < 8; ++i) {
        for (int j = 0; j < 8; ++j) {
            fscanf(tabla, "%d", &board[i][j]);
        }
    }
    fclose(tabla);
    pos bastya;
    for (int i = 0; i < 8; ++i) {
        for (int j = 0; j < 8; ++j) {
            printf("%d ", board[i][j]);
            if (board[i][j] == 2) {
                bastya.x = j;
                bastya.y = i;
            }
        }
        printf("\n");
    }
    printf("bastya: %d %d\n", bastya.x, bastya.y);

    int counter = 0;
    pos searchpos = bastya;
    // right
    for (int i = searchpos.x; i < 8; ++i) {
        if (board[searchpos.y][i] == 1) {
            counter++;
            printf("found:r %d %d\n", i, searchpos.y);
            break;
        }
    }
    // left
    for (int i = searchpos.x; i >= 0; --i) {
        if (board[searchpos.y][i] == 1) {
            counter++;
            printf("found:l %d %d\n", i, searchpos.y);
            break;
        }
    }
    // up
    for (int i = searchpos.y; i >= 0; --i) {
        if (board[i][searchpos.x] == 1) {
            counter++;
            printf("found:u %d %d\n", searchpos.x, i);
            break;
        }
    }
    // down
    for (int i = searchpos.y; i < 8; ++i) {
        if (board[i][searchpos.x] == 1) {
            counter++;
            printf("found:u %d %d\n", searchpos.x, i);
            break;
        }
    }
    return counter;
}