#include <stdio.h>
#include <stdlib.h>

int* histogram(const char* fajlnev) {
    FILE* file = fopen(fajlnev, "r");
    int szelesseg, magassag;
    int max_value;
    if (file != NULL) {
        fscanf(file, "P2%d%d%d", &szelesseg, &magassag, &max_value);
    } else {
        return NULL;
    }
    int* hiszt = (int*)malloc(sizeof(int)*max_value);
    for (int i = 0; i < max_value; ++i) {
        hiszt[i] = 0;
    }

    int valuse;
    for (int i = 0; i < szelesseg * magassag; ++i) {
        fscanf(file, "%d", &valuse);
        //printf("val: %d\n", valuse);
        hiszt[valuse]++;
    }

    fclose(file);
    return hiszt;
}