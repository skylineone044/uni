#include <stdlib.h>

typedef struct {
    int ar;
    int tomeg;
} Csoki;

int legolcsobb(const Csoki* tomb[], int dbszam) {
    float legkisebb_atlag = 99999;
    int id = 0;
    int j = 0;
    for (int i = 0; i < dbszam; ++i) {
        int currentcounter = 0;
        float jelenatlag = 0;
        for (; tomb[j] != NULL; ++j) {
             jelenatlag += (float)tomb[j]->ar / (float)tomb[j]->tomeg;
             currentcounter++;
        }
        j++;
        float atlag = jelenatlag / (float)currentcounter;
        if (legkisebb_atlag > atlag) {
            legkisebb_atlag = atlag;
            id = i;
        }
    }
    return id;
}