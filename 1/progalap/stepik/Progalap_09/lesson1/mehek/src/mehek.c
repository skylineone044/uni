#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct {
    int sorszam;
    double hatotav;
} DoromboloMeh;

double tav(int x, int y, int i, int j) {
    // calculate distance between 2 points: (x, y) and (i, j)
    return sqrt((x-i)*(x-i) + (y-j)*(y-j));
}

int checktav(int x, int y, DoromboloMeh **mehek[], int sorok, int oszlopok) {
    // megnezi hogy a jelenlegi pozicio benne van e legalabb egy hatokorben
    for (int i = 0; i < sorok; ++i) {
        for (int j = 0; j < oszlopok; ++j) {
            if (mehek[i][j] != NULL && i != x && j != y) {
                if (tav(x, y, i, j) <= mehek[i][j]->hatotav) {
                    return 1;
                }
            }
        }
    }
    return 0;
}

int dorombolas(DoromboloMeh **mehek[], int sorok, int oszlopok) {
    int eredmeny = 0;

    for (int i = 0; i < sorok; ++i) {
        for (int j = 0; j < oszlopok; ++j) {
            if (mehek[i][j] != NULL) {
//                double jelen_hatotav = mehek[i][j]->hatotav;
//                printf("tav: %lf\n", jelen_hatotav);
                eredmeny += checktav(i, j, mehek, sorok, oszlopok); // megszamolja mennyinek a hatokoreben van benne
            }
        }
    }
    return eredmeny*2;
}