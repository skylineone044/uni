#include <stdlib.h>

const char* nagyobb(const char a[], const char b[]) {
    long int aa = (long int)strtol(a, NULL, 16);
    long int bb = (long int)strtol(b, NULL, 16);
    if (aa > bb) {
        return a;
    } else if (bb > aa) {
        return b;
    } else {
        return NULL;
    }
}