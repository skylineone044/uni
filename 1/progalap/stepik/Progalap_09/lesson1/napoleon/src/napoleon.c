void kodol(char* szoveg) {
    for (int i = 0; szoveg[i] != '\0'; ++i) {
        if (('a' <= szoveg[i] && szoveg[i] <= 'z') || ('A' <= szoveg[i] && szoveg[i] <= 'Z')) {
            if (szoveg[i] == 'z') {
                szoveg[i] = 'a';
            } else if (szoveg[i] == 'Z') {
                szoveg[i] = 'A';
            } else {
                szoveg[i]++;
            }
        }
    }
}

void dekodol(char* szoveg) {
    for (int i = 0; szoveg[i] != '\0'; ++i) {
        if (('a' <= szoveg[i] && szoveg[i] <= 'z') || ('A' <= szoveg[i] && szoveg[i] <= 'Z')) {
            if (szoveg[i] == 'a') {
                szoveg[i] = 'z';
            } else if (szoveg[i] == 'A') {
                szoveg[i] = 'Z';
            } else {
                szoveg[i]--;
            }
        }
    }
}