#include <stdio.h>

int pixelszam(const char* fajlnev) {
    FILE* file = fopen(fajlnev, "r");
    if (file != NULL) {
        int szelesseg, magassag;
        fscanf(file, "P2\n%d %d\n", &szelesseg, &magassag);
        return szelesseg*magassag;
    }
    return 0;
}