#include <gtest/gtest.h>

#define main main_0
#include "../src/pixelek.c"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int a = pixelszam("../../../lesson1/pixelek/macska.pgm");
    int b = pixelszam("macska.pgm");
    int c = pixelszam("../../../lesson1/pixelek/tik.pgm");
    ASSERT_EQ(a, 749000);
    ASSERT_EQ(b, 0);
    ASSERT_EQ(c, 336960);
}