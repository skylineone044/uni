#include <stdio.h>

void rajzol(const char* fajlnev, unsigned int szinekSzama, unsigned int meret) {
    FILE*outfile = fopen(fajlnev, "w");
    fprintf(outfile, "P2\n%d %d\n%d\n", meret, meret, szinekSzama);
    printf("P2\n%d %d\n%d\n", meret, meret, szinekSzama);
    unsigned int next_szin = szinekSzama;
    unsigned int cellameret = meret / 8;
//    next_szin = (next_szin == szinekSzama ? 0 : szinekSzama);
    for (int i = 0; i < meret / cellameret; ++i) {
        for (int j = 0; j < meret; ++j) {
            for (int k = 0; k < cellameret; ++k) {
                fprintf(outfile, " %d", next_szin);
                printf(" %d", next_szin);
            }
            if (meret % cellameret == 0) {
                next_szin = (next_szin == szinekSzama ? 0 : szinekSzama);
            }
        }
        next_szin = (next_szin == szinekSzama ? 0 : szinekSzama);
        fprintf(outfile, "\n");
        printf("\n");
    }
    fclose(outfile);
}