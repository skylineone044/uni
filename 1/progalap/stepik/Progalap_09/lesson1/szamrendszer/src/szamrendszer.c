int alap(const char szoveg[]) {
    char largest_char = '0';

    for (int i = 0; szoveg[i] != '\0'; ++i) {
        if (szoveg[i] > largest_char) {
            largest_char = szoveg[i];
        }
    }
    if ('0' <= largest_char && largest_char <= '9') {
        return largest_char - '0'+1;
    } else if ('A' <= largest_char && largest_char <= 'Z') {
        return largest_char - 'A' + 11;
    }
}