#include <gtest/gtest.h>

#define main main_0
#include "../src/szamrendszer.c"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const char* a = "90";
    ASSERT_EQ(alap(a), 10);
    ASSERT_EQ(alap("101011001"), 2);
    ASSERT_EQ(alap("12"), 3);
    ASSERT_EQ(alap("414561"), 7);
    ASSERT_EQ(alap("222222"), 3);
    ASSERT_EQ(alap("AAAA"), 11);
    ASSERT_EQ(alap("FAFA41"), 16);
    ASSERT_EQ(alap("GAGA"), 17);
    ASSERT_EQ(alap("KA41A"), 21);
    ASSERT_EQ(alap("NANA"), 24);
    ASSERT_EQ(alap("QKA"), 27);
    ASSERT_EQ(alap("WWW"), 33);
    ASSERT_EQ(alap("ZOO"), 36);
    ASSERT_EQ(alap("UWU"), 33);
    ASSERT_EQ(alap("C0S0I0G0A"), 29);
}