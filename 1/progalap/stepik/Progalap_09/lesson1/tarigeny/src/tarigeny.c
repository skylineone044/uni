#include <math.h>

int tarigeny(unsigned int szam) {
    if (szam == 0) {
        return 1;
    }
    return (int)log2(szam)+1;
}