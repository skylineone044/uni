#include <gtest/gtest.h>

#define main main_0
#include "../src/tarigeny.c"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(tarigeny(1), 1);
    ASSERT_EQ(tarigeny(0), 1);
    ASSERT_EQ(tarigeny(4), 3);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(tarigeny(13), 4);
    ASSERT_EQ(tarigeny(7), 3);
    ASSERT_EQ(tarigeny(8), 4);
    ASSERT_EQ(tarigeny(33), 6);
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(tarigeny(71), 7);
    ASSERT_EQ(tarigeny(120), 7);
    ASSERT_EQ(tarigeny(224), 8);
    ASSERT_EQ(tarigeny(5125), 13);
    ASSERT_EQ(tarigeny(11111), 14);
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(tarigeny(412414122), 29);
    ASSERT_EQ(tarigeny(2412414122), 32);
}