#include <stdio.h>
#include <stdlib.h>

int* titkositas(const int* eredeti, int meret) {
    int *titkostomb = (int*)malloc(sizeof(int)*meret);
    titkostomb[0] = eredeti[0];
    for (int i = 1; i < meret; ++i) {
//        printf("TITKOSIT %d + %d = ", eredeti[i-1], eredeti[i]);
        titkostomb[i] = eredeti[i-1] + eredeti[i];
//        printf("%d\n", titkostomb[i]);
    }
    return titkostomb;
}

int* megfejtes(const int* titkositott, int meret) {
    int *dekodolttomb = (int*)malloc(sizeof(int)*meret);
    dekodolttomb[0] = titkositott[0];
    int diff = titkositott[1] - titkositott[0];

    for (int i = 1; i < meret; ++i) {
        printf("diff: %d\n", diff);
        dekodolttomb[i] = diff;
        diff = titkositott[i+1] - diff;
    }
    return dekodolttomb;
}