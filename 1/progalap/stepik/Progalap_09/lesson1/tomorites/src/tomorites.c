#include <stdlib.h>
#include <string.h>
#include <stdio.h>

char* tomorites(char** szovegek) {
    int dbszam = 0;
    for (; szovegek[dbszam] != NULL; ++dbszam) {
        for (int i = 0; szovegek[dbszam][i] != '\0'; ++i) {
            if ('A' <= szovegek[dbszam][i] && szovegek[dbszam][i] <= 'Z') {
                return NULL;
            }
        }
    }
    int lens[dbszam];
    int longest_len = 0;
    int longest_len_id  = 0;
    for (int i = 0; i < dbszam; ++i) {
        lens[i] = 0;
        for (int j = 0; szovegek[i][j] != '\0'; ++j) {
            lens[i]++;
        }
        if (lens[i] > longest_len) {
            longest_len = lens[i];
            longest_len_id  = i;
        }
    }
    char* output = (char*)malloc(sizeof(char)*longest_len+1);
    for (int i = 0; i < longest_len; ++i) {
        output[i] = 'a';
        output[i+1] = '\0';
    }

    for (int i = 0; i < longest_len; ++i) {
        for (int j = 0; j < dbszam; ++j) {
            if (i <= lens[j] && szovegek[j][i] != '\0') {
                int offset = (szovegek[j][i] - 'a');
                output[i] += offset;
                if (!('a' <= output[i] && output[i] <= 'z')) {
                    return NULL;
                }
                printf("added: %d\n", offset);
            }
        }
    }



    return output;
}