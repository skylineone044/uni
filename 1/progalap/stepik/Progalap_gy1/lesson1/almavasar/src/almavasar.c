int vasarlas(double alapAr, double penz) {
    int almakszama = 0;
    while (penz > 0) {
        penz -= alapAr;
        almakszama++;
        alapAr *= 1.1;
    }
    return almakszama-1;
}