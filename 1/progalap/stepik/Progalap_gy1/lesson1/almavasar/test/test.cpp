#include <gtest/gtest.h>
#include "../src/almavasar.c"

TEST(SumTest, Simple) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(vasarlas(10, 100), 7);
    ASSERT_EQ(vasarlas(2.5, 250), 25);
    ASSERT_EQ(vasarlas(0.1, 9.84), 25);
    ASSERT_EQ(vasarlas(0.1, 9.83), 24);
    ASSERT_EQ(vasarlas(15, 7317), 40);
    ASSERT_EQ(vasarlas(1.3, 1.2), 0);
}