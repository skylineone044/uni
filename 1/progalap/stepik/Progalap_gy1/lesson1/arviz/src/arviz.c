double atlag(int erdemjegyek[], int jegySzam) {  // "strive ot be lazy" -some person on the internet
    int osszeg = 0;
    for (int i = 0; i < jegySzam; ++i) {
        osszeg += erdemjegyek[i];
    }
    return (double )osszeg / (double )jegySzam;
}

void arviz(int epuletek[], int darabszam) {
    double atlagmegassag = atlag(epuletek, darabszam);
    for (int i = 0; i < darabszam; ++i) {
        if (epuletek[i] < atlagmegassag) {
            epuletek[i] = 0;
        }
    }
}
