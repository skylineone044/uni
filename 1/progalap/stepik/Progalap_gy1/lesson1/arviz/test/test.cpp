#include <gtest/gtest.h>
#include "../src/arviz.c"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t1[] = {5, 4, 3, 8, 6, 4};
    int t1m[] = {5, 0, 0, 8, 6, 0};

    int t2[] = {5, 2, 1, 5, 1, 2};
    int t2m[] = {5, 0, 0, 5, 0, 0};

    int t3[] = {2, 4, 2, 4, 1, 5, 3, 3, 3};
    int t3m[] = {0, 4, 0, 4, 0, 5, 3, 3, 3};

    int t4[] = {5, 5};
    int t4m[] = {5, 5};

    int t5[] = {4};
    int t5m[] = {4};

    arviz(t1, 6);
    arviz(t2, 6);
    arviz(t3, 9);
    arviz(t4, 2);
    arviz(t5, 1);

    for (int i = 0; i < 6; i++) ASSERT_EQ(t1[i], t1m[i]);
    for (int i = 0; i < 6; i++) ASSERT_EQ(t2[i], t2m[i]);
    for (int i = 0; i < 9; i++) ASSERT_EQ(t3[i], t3m[i]);
    for (int i = 0; i < 2; i++) ASSERT_EQ(t4[i], t4m[i]);
    for (int i = 0; i < 1; i++) ASSERT_EQ(t5[i], t5m[i]);
}