
// C double atlag tomb darabszam double average array lenght
double atlag(int erdemjegyek[], int jegySzam) {
    int osszeg = 0;
    for (int i = 0; i < jegySzam; ++i) {
        osszeg += erdemjegyek[i];
    }
    return (double )osszeg / (double )jegySzam;
}