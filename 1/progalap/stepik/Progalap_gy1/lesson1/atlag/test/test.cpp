#include <gtest/gtest.h>
#include "../src/atlag.c"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t1[] = {1, 4, 3, 2, 5, 2, 1, 1};
    int t2[] = {5, 5, 5, 5, 5};
    int t3[] = {4, 1, 2, 3, 1};
    int t4[] = {4, 2, 3};
    int t5[] = {5};
    int t6[] = {2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

    ASSERT_DOUBLE_EQ(atlag(t1, 8), 2.375);
    ASSERT_DOUBLE_EQ(atlag(t2, 5), 5);
    ASSERT_DOUBLE_EQ(atlag(t3, 5), 2.2);
    ASSERT_DOUBLE_EQ(atlag(t4, 3), 3);
    ASSERT_DOUBLE_EQ(atlag(t5, 1), 5);
    ASSERT_DOUBLE_EQ(atlag(t6, 14), 15.0/14);
}