int butorTerulet(int butorterulet, int butorszam) {
    return butorszam * butorterulet;
}

int oszlopSzam(int szekekszama, int szekek_oszloponkent) {
    return szekekszama / szekek_oszloponkent + (szekekszama % szekek_oszloponkent == 0 ? 0 : 1);
}

int szekLab(int szekekszama) {
    return 4 * szekekszama;
}