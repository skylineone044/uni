#include <gtest/gtest.h>
#include "../src/butor.c"

int butorTerulet(int meret, int darab);
int oszlopSzam(int darab, int oszlopKorlat);
int szekLab(int szekSzam);


TEST(butorTest, butorTerulet) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(butorTerulet(1, 2), 2);
    ASSERT_EQ(butorTerulet(7, 2), 14);
    ASSERT_EQ(butorTerulet(3, 8), 24);
    ASSERT_EQ(butorTerulet(5, 5), 25);
    ASSERT_EQ(butorTerulet(1, 1), 1);
}

TEST(butorTest, oszlopSzam) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(oszlopSzam(10, 4), 3);
    ASSERT_EQ(oszlopSzam(10, 2), 5);
    ASSERT_EQ(oszlopSzam(10, 1), 10);
    ASSERT_EQ(oszlopSzam(311, 7), 45);
    ASSERT_EQ(oszlopSzam(52323, 411), 128);
    ASSERT_EQ(oszlopSzam(4, 5), 1);
}

TEST(butorTest, szekLab) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(szekLab(0), 0);
    ASSERT_EQ(szekLab(72), 288);
    ASSERT_EQ(szekLab(30), 120);
    ASSERT_EQ(szekLab(5), 20);
    ASSERT_EQ(szekLab(41212), 164848);
}