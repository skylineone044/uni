#include <gtest/gtest.h>
#include "../src/darabszam.c"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char s1[] = "macska";
    char s2[] = "egerek";
    char s3[] = "nyihahamintegylo";
    char s4[] = "Noemi neni arulja a viragokat, Leen meglocsolja";
    char s5[] = "Ontozik a viragokat";
    char s6[] = "+++-+-+--+---,,,-..,;**,*";

    ASSERT_EQ(osszeszamlal(s1, 'a'), 2);
    ASSERT_EQ(osszeszamlal(s2, 'e'), 3);
    ASSERT_EQ(osszeszamlal(s3, 'u'), 0);
    ASSERT_EQ(osszeszamlal(s4, 'N'), 1);
    ASSERT_EQ(osszeszamlal(s5, 'Q'), 0);
    ASSERT_EQ(osszeszamlal(s6, '*'), 3);
}