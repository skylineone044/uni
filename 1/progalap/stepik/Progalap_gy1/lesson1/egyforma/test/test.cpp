#include <gtest/gtest.h>
#include "../src/egyforma.c"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t1[] = {5, 2, 1, 2, 1, 2, 3, 2};
    int t2[] = {4, 4, 4, 1};
    int t3[] = {1, 4, 4, 4};
    int t4[] = {1, 4, 1, 4};
    int t5[] = {7};
    int t6[] = {7, 7, 7};
    int* t7 = (int*) malloc(0);

    ASSERT_EQ(egyforma(t1, 8), 0);
    ASSERT_EQ(egyforma(t2, 4), 0);
    ASSERT_EQ(egyforma(t3, 4), 0);
    ASSERT_EQ(egyforma(t4, 4), 0);
    ASSERT_EQ(egyforma(t5, 1), 1);
    ASSERT_EQ(egyforma(t6, 3), 1);
    ASSERT_EQ(egyforma(t7, 0), 1);

    free(t7);
}