#include <limits.h>

typedef struct {
    char nev[100];
    int eletkor;
    int szarnyMeret;
} Fonix;

int fonixSzemle(Fonix fonixek[], int darab) {
    int min = INT_MAX;
    int max = INT_MIN;

    for (int i = 0; i < darab; i++) {
        if (fonixek[i].szarnyMeret < min) {
            min = fonixek[i].szarnyMeret;
        }

        if (fonixek[i].szarnyMeret > max) {
            max = fonixek[i].szarnyMeret;
        }
    }

    if (min == INT_MAX) {
        return 0;
    }

    return max - min;
}