#include <gtest/gtest.h>
#include "../src/fonix.c"
#include <climits>

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Fonix f1[] = {
            {"nev1", 30, 13},
            {"nev2", 2, 17},
            {"nev3", 5, 22},
            {"nev4", 3, 15},
            {"nev5", 8, 24},
    };

    Fonix f2[] = {
            {"nev21", 33, 10},
            {"nev22", 22, 40},
            {"nev23", 44, 2},
            {"nev24", 11, 2},
    };

    Fonix f3[] = {
            {"nev31", 66, 40},
    };

    Fonix f4[] = {
            {"nev41", 44, INT_MAX - 5},
            {"nev42", 54, INT_MAX - 7},
            {"nev43", 73, INT_MAX - 10}
    };

    ASSERT_EQ(fonixSzemle(f1, 5), 11);
    ASSERT_EQ(fonixSzemle(f2, 4), 38);
    ASSERT_EQ(fonixSzemle(f3, 1), 0);
    ASSERT_EQ(fonixSzemle(f4, 3), 5);
}