#include <gtest/gtest.h>
#include "../src/fuggvenyek.c"

TEST(FuggvenyTeszt, harom) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(harom(), 3);
}

TEST(FuggvenyTeszt, echo) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    for (int i = 0; i < 10; i++) {
        int r = rand() % 10000;
        ASSERT_EQ(echo(r), r);
    }
}

TEST(FuggvenyTeszt, semmi) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    semmi();
}
