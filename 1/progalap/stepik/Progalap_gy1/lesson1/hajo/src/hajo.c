typedef struct {
    int eletkor;
    int nem;
} Ember;

typedef struct {
    Ember utasok[100];
    int emberekSzama;
} Hajo;

double atlag(Ember erdemjegyek[], int jegySzam) {
    int osszeg = 0;
    for (int i = 0; i < jegySzam; ++i) {
        osszeg += erdemjegyek[i].eletkor;
    }
    return (double )osszeg / (double )jegySzam;
}

Hajo utazas(Hajo egyik, Hajo masik) {
    double atlag_egyik = atlag(egyik.utasok, egyik.emberekSzama);
    double atlag_masik = atlag(masik.utasok, masik.emberekSzama);

    return (atlag_egyik > atlag_masik ? egyik : masik);
}