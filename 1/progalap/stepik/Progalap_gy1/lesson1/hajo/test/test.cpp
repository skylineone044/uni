#include <gtest/gtest.h>
#include "../src/hajo.c"

int egyforma(Hajo h1, Hajo h2) {
    if (h1.emberekSzama != h2.emberekSzama) {
        return 0;
    }

    for (int i = 0; i < h1.emberekSzama; i++) {
        if (h1.utasok[i].eletkor != h2.utasok[i].eletkor || h1.utasok[i].nem != h2.utasok[i].nem) {
            return 0;
        }
    }

    return 1;
}

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Ember e1[] = {
            {6, 1},
            {2, 4},
            {7, 4},
            {3, 5},
            {3, 3}
    };

    Ember e2[] = {
            {3, 3},
            {5, 1},
            {2, 3}
    };

    Ember e3[] = {
            {4, 2},
            {2, 1},
            {4, 2},
            {5, 3},
            {3, 2}
    };

    Ember e4[] = {
            {3, 2}
    };

    Hajo h1; for (int i = 0; i < 5; i++) h1.utasok[i] = e1[i]; h1.emberekSzama = 5; // 4.2
    Hajo h2; for (int i = 0; i < 3; i++) h2.utasok[i] = e2[i]; h2.emberekSzama = 3; // 3.3
    Hajo h3; for (int i = 0; i < 5; i++) h3.utasok[i] = e3[i]; h3.emberekSzama = 5; // 3.6
    Hajo h4; for (int i = 0; i < 1; i++) h4.utasok[i] = e4[i]; h4.emberekSzama = 1; // 3

    Hajo res1 = utazas(h1, h2);
    Hajo res2 = utazas(h1, h3);
    Hajo res3 = utazas(h1, h4);
    Hajo res4 = utazas(h2, h3);
    Hajo res5 = utazas(h2, h4);
    Hajo res6 = utazas(h3, h4);

    ASSERT_EQ(egyforma(res1, h1), 1);
    ASSERT_EQ(egyforma(res2, h1), 1);
    ASSERT_EQ(egyforma(res3, h1), 1);
    ASSERT_EQ(egyforma(res4, h3), 1);
    ASSERT_EQ(egyforma(res5, h2), 1);
    ASSERT_EQ(egyforma(res6, h3), 1);
}

