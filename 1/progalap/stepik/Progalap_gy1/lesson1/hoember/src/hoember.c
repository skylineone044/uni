char nagyobb(int karolyHoembere, int adelHoembere, int emeseHoembere) {
    int legnagyobb = 0;
    int legnagyobb_id = 0;
    int hoemberek[3] = {karolyHoembere, adelHoembere, emeseHoembere};
    int i = 0;
    for (; i < 3; ++i) {
        if (hoemberek[i] > legnagyobb) {legnagyobb = hoemberek[i]; legnagyobb_id = i;}
    }
    if (legnagyobb_id == 0) {return 'k';}
    if (legnagyobb_id == 1) {return 'a';}
    if (legnagyobb_id == 2) {return 'e';}
}