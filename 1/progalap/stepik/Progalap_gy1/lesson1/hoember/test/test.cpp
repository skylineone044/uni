#include <gtest/gtest.h>
#include "../src/hoember.c"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(nagyobb(7, 3, 2), 'k');
    ASSERT_EQ(nagyobb(7, 2, 4), 'k');
    ASSERT_EQ(nagyobb(2, 7, 5), 'a');
    ASSERT_EQ(nagyobb(5, 9, 2), 'a');
    ASSERT_EQ(nagyobb(7, 9, 12), 'e');
    ASSERT_EQ(nagyobb(8, 1, 2200), 'e');
}