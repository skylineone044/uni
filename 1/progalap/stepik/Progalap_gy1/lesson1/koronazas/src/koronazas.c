#include <string.h>

typedef struct {
    char nev[255];
    int kor;
    int pontszam;
} Ember;

typedef struct {
    char nev[255];
    Ember uralkodo;
    Ember lakosok[100];
    int terulet;
    int lakosokSzama;
} Orszag;

Orszag osszeolvadas(Orszag o1, Orszag o2) {
    Orszag o3;
    strcpy(o3.nev, o1.nev);
    o3.uralkodo = o1.uralkodo;
    o3.terulet = o1.terulet + o2.terulet;
    o3.lakosokSzama = o1.lakosokSzama + o2.lakosokSzama;

    int j = 0;
    for (int i = 0; i < o3.lakosokSzama; ++i) {
        if (i < o1.lakosokSzama) {
            o3.lakosok[i] = o1.lakosok[i];
        } else {
            o3.lakosok[i] = o2.lakosok[j]; j++;
        }
    }
    return o3;
}
