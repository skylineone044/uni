#include <gtest/gtest.h>
#include <string.h>
#include "../src/koronazas.c"

Orszag createOrszag(char nev[], Ember uralkodo, Ember emberek[], int terulet, int lakosok) {
    Orszag o;

    strcpy(o.nev, nev);
    o.uralkodo = uralkodo;
    o.terulet = terulet;
    o.lakosokSzama = lakosok;

    for (int i = 0; i < lakosok; i++) {
        o.lakosok[i] = emberek[i];
    }

    return o;
}

Orszag asd() {
    Orszag a;
    return a;
}

int egyforma(Orszag o1, Orszag o2) {
    if (strcmp(o1.nev, o2.nev) != 0) {
        printf("A");
        return 0;
    }

    if (o1.terulet != o2.terulet) {
        printf("B");
        return 0;
    }

    if (o1.lakosokSzama != o2.lakosokSzama) {
        printf("C");
        return 0;
    }

    if (o1.uralkodo.pontszam != o2.uralkodo.pontszam) {
        printf("D");
        return 0;
    }

    if (o1.uralkodo.kor != o2.uralkodo.kor) {
        printf("E");
        return 0;
    }

    if (strcmp(o1.uralkodo.nev, o2.uralkodo.nev) != 0) {
        printf("F");
        return 0;
    }

    for (int i = 0; i < o1.lakosokSzama; i++) {
        printf("%d", i);
        if (o1.lakosok[i].kor != o2.lakosok[i].kor) {
            printf("G");
            return 0;
        }

        if (o1.lakosok[i].pontszam != o2.lakosok[i].pontszam) {
            printf("H");
            return 0;
        }

        if (strcmp(o1.lakosok[i].nev, o2.lakosok[i].nev) != 0) {
            printf("I");
            return 0;
        }
    }

    return 1;
}

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char nev1[] = "Kina";
    char nev2[] = "Japan";
    char nev3[] = "Oroszorszag";
    char nev4[] = "Szingapur";

    Ember e1[] = {
            {"Akos", 5, 7},
            {"Judit", 4, 4},
            {"Jutka", 2, 5},
            {"Marika", 8, 23}
    };

    Ember e2[] = {
            {"Eszter", 8, 2},
            {"Andras", 3, 4},
            {"Peter", 2, 1}
    };

    Ember e3[] = {
            {"Otto", 7, 2},
            {"Gyorgy", 10, 2}
    };

    Ember e4[] = {
            {"Gyula", 10, 10}
    };

    Ember e1m[] = {
            {"Akos", 5, 7},
            {"Judit", 4, 4},
            {"Jutka", 2, 5},
            {"Marika", 8, 23},
            {"Eszter", 8, 2},
            {"Andras", 3, 4},
            {"Peter", 2, 1}
    };

    Ember e2m[] = {
            {"Otto", 7, 2},
            {"Gyorgy", 10, 2},
            {"Gyula", 10, 10}
    };

    Ember u1 = {"Akos", 5, 7};
    Ember u2 = {"Fanni", 7, 9};

    Orszag o1 = createOrszag(nev1, u1, e1, 51, 4);
    Orszag o2 = createOrszag(nev2, u1, e2, 21, 3);
    Orszag o3 = createOrszag(nev3, u2, e3, 754, 2);
    Orszag o4 = createOrszag(nev4, u2, e4, 322, 1);

    Orszag o1m = createOrszag(nev1, u1, e1m, 72, 7);
    Orszag o2m = createOrszag(nev3, u2, e2m, 1076, 3);

    Orszag res1 = osszeolvadas(o1, o2);
    Orszag res2 = osszeolvadas(o3, o4);

    ASSERT_EQ(egyforma(res1, o1m), 1);
    ASSERT_EQ(egyforma(res2, o2m), 1);
}