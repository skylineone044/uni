char leggyakrabbi(char szoveg[]) {
    char abc[26] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    int nagybetu = 0;
    int betuid = 0;
    for (int i = 0; szoveg[i] != '\0'; ++i) {
        if (szoveg[i] >= 'a' && szoveg[i] <= 'z' || szoveg[i] >= 'A' && szoveg[i] <= 'Z') {
            if (szoveg[i] >= 'A' && szoveg[i] <= 'Z') {nagybetu = 1;}
            else {nagybetu = 0;}
            betuid = (nagybetu ? szoveg[i]-'A': szoveg[i]-'a');
            abc[betuid]++;
        }
    }
    int leggyakoribb = 0;
    int leggyakoribb_id = 0;
    for (int i = 0; i < 26; ++i) {
        if (abc[i] > leggyakoribb) {leggyakoribb = abc[i]; leggyakoribb_id = i;}
    }
    return leggyakoribb_id + 'a';
}