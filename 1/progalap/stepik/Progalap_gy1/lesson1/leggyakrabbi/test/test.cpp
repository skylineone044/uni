#include <gtest/gtest.h>
#include "../src/leggyakrabbi.c"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char s1[] = "macska";
    char s2[] = "a a b c d e f";
    char s3[] = "LlLlaaa";
    char s4[] = "LLLLavamksd";
    char s5[] = "q";
    char s6[] = "++++------++++----24141231289416419284612842141689246178s52352";

    ASSERT_EQ(leggyakrabbi(s1), 'a');
    ASSERT_EQ(leggyakrabbi(s2), 'a');
    ASSERT_EQ(leggyakrabbi(s3), 'l');
    ASSERT_EQ(leggyakrabbi(s4), 'l');
    ASSERT_EQ(leggyakrabbi(s5), 'q');
    ASSERT_EQ(leggyakrabbi(s6), 's');
}