#include <math.h>

int linearis(double f(int x), int min, int max) {
    double elso_valtozas = roundf((f(min) - f(min+1)) * 100) / 100;
    double valtozas;
    if (max - min <= 1) {return 1;}
    for (int i = min+1; i < max; ++i) {
        valtozas = roundf((f(i) - f(i+1)) * 100) / 100;
        if (valtozas != elso_valtozas) {
            return 0;
        }
    }

    return 1;
}