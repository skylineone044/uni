#include <gtest/gtest.h>

#include "../src/linearis.c"

double fgv1(int x) {
    return x;
}

double fgv2(int x) {
    return 2*x;
}

double fgv3(int x) {
    return 0.45*x;
}

double fgv4(int x) {
    return 0.22*x + 0.1;
}

double fgv5(int x) {
    return x*x;
}

double fgv6(int x) {
    return x*x-x;
}

double fgv7(int x) {
    return abs(x);
}

double fgv8(int x) {
    return abs(x) - x;
}

double fgv9(int x) {
    return 52.4;
}

double fgv10(int x) {
    if (x >= 4990 && x <= 5000) {
        return x*2;
    }

    return x;
}

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(linearis(fgv1, 0, 10), 1);
    ASSERT_EQ(linearis(fgv1, -5, 5), 1);
    ASSERT_EQ(linearis(fgv1, 0, 0), 1);
    ASSERT_EQ(linearis(fgv1, 3, 4), 1);
    ASSERT_EQ(linearis(fgv2, -15, 10), 1);
    ASSERT_EQ(linearis(fgv3, -100, 1), 1);
    ASSERT_EQ(linearis(fgv4, -20, 30), 1);

    ASSERT_EQ(linearis(fgv5, 3, 3), 1);
    ASSERT_EQ(linearis(fgv5, 2, 3), 1);
    ASSERT_EQ(linearis(fgv5, 2, 4), 0);

    ASSERT_EQ(linearis(fgv6, 10, 20), 0);
    ASSERT_EQ(linearis(fgv6, -1, 1), 0);
    ASSERT_EQ(linearis(fgv6, 0, 1), 1);

    ASSERT_EQ(linearis(fgv7, 0, 100), 1);
    ASSERT_EQ(linearis(fgv7, -100, -50), 1);
    ASSERT_EQ(linearis(fgv7, -100, 50), 0);

    ASSERT_EQ(linearis(fgv8, -12, -4), 1);
    ASSERT_EQ(linearis(fgv8, 0, 3), 1);
    ASSERT_EQ(linearis(fgv8, -1, 2), 0);

    ASSERT_EQ(linearis(fgv9, -10, -3), 1);
    ASSERT_EQ(linearis(fgv9, 4, 7), 1);
    ASSERT_EQ(linearis(fgv10, -400, 3130), 1);

    ASSERT_EQ(linearis(fgv10, 4990, 5000), 1);
    ASSERT_EQ(linearis(fgv10, 0, 4989), 1);
    ASSERT_EQ(linearis(fgv10, 5001, 5500), 1);

    ASSERT_EQ(linearis(fgv10, 0, 4990), 0);
    ASSERT_EQ(linearis(fgv10, 0, 4991), 0);
    ASSERT_EQ(linearis(fgv10, 5000, 7000), 0);
    ASSERT_EQ(linearis(fgv10, -100000, 4990), 0);
    ASSERT_EQ(linearis(fgv10, -100, 100000), 0);
}