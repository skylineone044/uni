#include <gtest/gtest.h>
#include <cstring>
#include "../src/macska.c"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Macska m;
    strcpy(m.nev, "cirmi");
    strcpy(m.szin, "fekete");
    m.magassag = 3.4;
    m.nyavogasiMertek = -4;

    ASSERT_STREQ(m.nev, "cirmi");
    ASSERT_STREQ(m.szin, "fekete");
    ASSERT_EQ(m.magassag, 3.4);
    ASSERT_EQ(m.nyavogasiMertek, -4);
}