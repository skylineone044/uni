#include <gtest/gtest.h>
#include "../src/masolas.c"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t1[] = {4, 2, 6, 4, 8, 5, -1};
    int t1m[] = {0, 0, 0, 0, 0, 0, 0};

    int t2[] = {4, 6, 3, 2, -1};
    int t2m[] = {0, 0, 0, 0, 0};

    int t3[] = {7, 7, -1};
    int t3m[] = {0, 0, 0};

    int t4[] = {4, 2, 6, -1};
    int t4m[] = {0, 0, 0, 0};

    int t5[] = {-1};
    int t5m[] = {0};

    masolas(t1, t1m);
    masolas(t2, t2m);
    masolas(t3, t3m);
    masolas(t4, t4m);
    masolas(t5, t5m);

    for (int i = 0; i < 7; i++) ASSERT_EQ(t1[i], t1m[i]);
    for (int i = 0; i < 5; i++) ASSERT_EQ(t2[i], t2m[i]);
    for (int i = 0; i < 3; i++) ASSERT_EQ(t3[i], t3m[i]);
    for (int i = 0; i < 4; i++) ASSERT_EQ(t4[i], t4m[i]);
    for (int i = 0; i < 1; i++) ASSERT_EQ(t5[i], t5m[i]);
}