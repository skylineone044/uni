#include <gtest/gtest.h>
#include "../src/maxpont.c"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t1[] = {4, 2, 4, 2, 0};
    int t2[] = {5, 5, 5, 5, 5};
    int t3[] = {100, 75, 4, 23, 53, 66};
    int t4[] = {3, 2, 3, -1, -5, -51};
    int t5[] = {2, 2, 2, 3, 2};
    int t6[] = {2, 1, 1, 1, 1, 1, 1, 1};
    int t7[] = {3};
    int* t8 = (int*) malloc(0);

    ASSERT_EQ(zh(t1, 5, 5), 0);
    ASSERT_EQ(zh(t2, 5, 5), 5);
    ASSERT_EQ(zh(t3, 6, 100), 1);
    ASSERT_EQ(zh(t4, 6, 3), 2);
    ASSERT_EQ(zh(t5, 5, 3), 1);
    ASSERT_EQ(zh(t6, 8, 2), 1);
    ASSERT_EQ(zh(t7, 1, 5), 0);
    ASSERT_EQ(zh(t8, 0, 1), 0);

    free(t8);
}