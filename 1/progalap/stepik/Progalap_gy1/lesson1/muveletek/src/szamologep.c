double osszead(double a, double b) {
    return a + b;
}

double kivon(double a, double b) {
    return a - b;
}

double szoroz(double a, double b) {
    return a * b;
}

double oszt(double a, double b) {
    if (b == 0) {return -1;}
    return a / b;
}