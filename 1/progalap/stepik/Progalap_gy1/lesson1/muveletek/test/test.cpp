#include <gtest/gtest.h>
#include "../src/szamologep.c"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_DOUBLE_EQ(osszead(1, 2), 3);
    ASSERT_DOUBLE_EQ(osszead(7.2, 4), 11.2);
    ASSERT_DOUBLE_EQ(osszead(-1, 7), 6);
    ASSERT_DOUBLE_EQ(kivon(7.5, 5.5), 2);
    ASSERT_DOUBLE_EQ(kivon(3, 6), -3);
    ASSERT_DOUBLE_EQ(szoroz(8.5, 2), 17);
    ASSERT_DOUBLE_EQ(szoroz(1, 0), 0);
    ASSERT_DOUBLE_EQ(oszt(7, 2), 3.5);
    ASSERT_DOUBLE_EQ(oszt(2.5, 1.25), 2);
    ASSERT_DOUBLE_EQ(oszt(4, 0), -1);
}