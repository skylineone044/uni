int noveny(int alapTerulet, int varosTerulet) {
    int percek = 0;
    while (alapTerulet < varosTerulet) {
        percek++;
        alapTerulet *= 2;
    }
    return percek;
}
