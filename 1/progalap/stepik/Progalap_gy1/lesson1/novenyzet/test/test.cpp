#include <gtest/gtest.h>
#include "../src/novenyzet.c"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(noveny(15, 15358), 10);
    ASSERT_EQ(noveny(15, 15360), 10);
    ASSERT_EQ(noveny(15, 15361), 11);
    ASSERT_EQ(noveny(2, 7), 2);
    ASSERT_EQ(noveny(93, 5950), 6);
    ASSERT_EQ(noveny(93, 6094849), 17);
    ASSERT_EQ(noveny(10000, 5119999), 9);
}