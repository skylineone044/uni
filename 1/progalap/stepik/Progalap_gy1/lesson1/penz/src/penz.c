int zsebpenz(int felretettOsszegek[], int honapSzam) {
    int osszeg =0;
    for (int i = 0; i < honapSzam; ++i) {
        osszeg += felretettOsszegek[i];
    }
    return osszeg;
}
