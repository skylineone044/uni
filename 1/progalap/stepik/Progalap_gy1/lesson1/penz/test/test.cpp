#include <gtest/gtest.h>
#include "../src/penz.c"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t1[] = {4, 6, 3, 2, 1};
    int t2[] = {7, 5, 4, 9, 9};
    int t3[] = {13, 512, 51, 10};
    int t4[] = {7343};
    int t5[] = {734, 41, 123, 6346, 754};
    int* t6 = (int*) malloc(0);

    ASSERT_EQ(zsebpenz(t1, 5), 16);
    ASSERT_EQ(zsebpenz(t2, 5), 34);
    ASSERT_EQ(zsebpenz(t3, 4), 586);
    ASSERT_EQ(zsebpenz(t4, 1), 7343);
    ASSERT_EQ(zsebpenz(t5, 5),  7998);
    ASSERT_EQ(zsebpenz(t6, 0), 0);

    free(t6);
}