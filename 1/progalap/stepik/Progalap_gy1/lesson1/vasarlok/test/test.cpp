#include <gtest/gtest.h>
#include "../src/vasarlok.c"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t1[] = {5, 2, 3, 2, 1, 0, 3, -1};
    int t2[] = {5, 2, 1, -1};
    int t3[] = {5, 5, -1};
    int t4[] = {5, 2, -1};
    int t5[] = {5, -1};
    int t6[] = {-1};

    ASSERT_EQ(sorbanAllas(t1), 7);
    ASSERT_EQ(sorbanAllas(t2), 3);
    ASSERT_EQ(sorbanAllas(t3), 2);
    ASSERT_EQ(sorbanAllas(t4), 2);
    ASSERT_EQ(sorbanAllas(t5), 1);
    ASSERT_EQ(sorbanAllas(t6), 0);
}