#include <gtest/gtest.h>

#define main main_0
#include "../../tools.cpp"
#undef main

namespace elso {
    #include "../src/debug.c"

    TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
        char str[300];
        IO("", int res = osszeg(5, 7), str)
        ASSERT_STREQ(str, "");
        ASSERT_EQ(res, 12);
    }

    TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
        char str[300];
        IO("", int res = osszeg(62, -40), str)
        ASSERT_STREQ(str, "");
        ASSERT_EQ(res, 22);
    }

}

namespace masodik {
#define DEBUG
#include "../src/debug.c"

    TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
        char str[300];
        IO("", int res = osszeg(5, 7), str)
        ASSERT_STREQ(str, "a ket parameter erteke:5 7\na res erteke: 0\naz osszeg: 12\n");
        ASSERT_EQ(res, 12);
    }

    TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
        char str[300];
        IO("", int res = osszeg(2, -4), str)
        ASSERT_STREQ(str, "a ket parameter erteke:2 -4\na res erteke: 0\naz osszeg: -2\n");
        ASSERT_EQ(res, -2);
    }

}