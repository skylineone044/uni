#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    if (argc != 2) {
        return 1;
    }
    FILE* be_file = fopen(argv[1], "r");
    if (be_file == NULL) {
        return 2;
    }
    int legnagyobb_pont = -1;
    char legtobb_pontos_id[100];
    for (int i = 0; i < 100; ++i) {
        legtobb_pontos_id[i] = '\0';
    }
    int pont;
    char id[100];
    for (int i = 0; i < 100; ++i) {
        id[i] = '\0';
    }

    while (fscanf(be_file, "%s %d\n", id, &pont) != EOF) {
        if (pont > legnagyobb_pont) {
            legnagyobb_pont = pont;
            strcpy(legtobb_pontos_id, id);
        }
    }
    printf("%s", legtobb_pontos_id);
    return 0;
}