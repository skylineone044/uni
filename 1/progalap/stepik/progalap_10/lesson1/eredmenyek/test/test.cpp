#include <gtest/gtest.h>

#define main main_0
#include "../src/eredmenyek.c"
#undef main

#include "../../tools.cpp"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char elso[] = "./eredmenyek";
    char fajlnev[] = "pontszamok.txt";
    writeContentToFile(fajlnev, "ABCDEF 7\nVSJNAK 30\nAAAAAA 5\n");
    char legjobb[] = "VSJNAK";

    char* params[] = {elso, fajlnev, NULL};
    char str[100];
    IO("", main_0(2, params), str)
    ASSERT_STREQ(str, legjobb);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char elso[] = "./eredmenyek";
    char fajlnev[] = "eredmenyek.txt";
    writeContentToFile(fajlnev, "KMKMKM 7\nVNXKNX 5\nQQQQQQ 10\nBCVCVC 9\nANKNBA 0\nANBKCX 6\n");
    char legjobb[] = "QQQQQQ";

    char* params[] = {elso, fajlnev, NULL};
    char str[100];
    IO("", main_0(2, params), str)
    ASSERT_STREQ(str, legjobb);
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char elso[] = "./eredmenyek";
    char fajlnev[] = "tehen.txt";
    writeContentToFile(fajlnev, "AVNKSN 31241\nAKNAVK 9000\n");
    char legjobb[] = "AVNKSN";

    char* params[] = {elso, fajlnev, NULL};
    char str[100];
    IO("", main_0(2, params), str)
    ASSERT_STREQ(str, legjobb);
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char elso[] = "./eredmenyek";
    char fajlnev[] = "cica.txt";
    writeContentToFile(fajlnev, "MMMMMM 0\n");
    char legjobb[] = "MMMMMM";

    char* params[] = {elso, fajlnev, NULL};
    char str[100];
    IO("", main_0(2, params), str)
    ASSERT_STREQ(str, legjobb);
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char elso[] = "./eredmenyek";
    char fajlnev[] = "eredmenyek.txt";
    writeContentToFile(fajlnev, "MMMMMM 0\n");

    char* params[] = {elso, fajlnev, NULL};
    char str[100];
    IO("", int res = main_0(2, params), str)
    ASSERT_EQ(res, 0);
}

TEST(Teszt, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char elso[] = "./eredmenyek";
    char fajlnev[] = "eredmenyek.txt";
    writeContentToFile(fajlnev, "MMMMMM 0\n");

    char* params[] = {elso, fajlnev, fajlnev, NULL};
    char str[100];
    IO("", int res = main_0(3, params), str)
    ASSERT_NE(res, 0);
}

TEST(Teszt, 07) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char elso[] = "./eredmenyek";
    char fajlnev[] = "eredmenyek.txt";
    writeContentToFile(fajlnev, "MMMMMM 0\n");
    char falsefilename[] = "jkassvnkjn.txt";

    char* params[] = {elso, falsefilename, NULL};
    char str[100];
    IO("", int res = main_0(2, params), str)
    ASSERT_NE(res, 0);
}

TEST(Teszt, 08) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char elso[] = "./eredmenyek";
    char fajlnev[] = "eredmenyek.txt";
    writeContentToFile(fajlnev, "MMMMMM 0\n");

    char* params[] = {elso, NULL};
    char str[100];
    IO("", int res = main_0(1, params), str)
    ASSERT_NE(res, 0);
}