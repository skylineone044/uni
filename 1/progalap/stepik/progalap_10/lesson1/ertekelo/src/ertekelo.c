#include <string.h>
#include <stdio.h>

int main(int argc, char* argv[]) {
    char cmdstr[100];
    sprintf(cmdstr, "sh -c \"gcc %s 2>&1\"", argv[1]);
    FILE *out_txt = popen(cmdstr, "r");
    int len = 10000;
    char buffer[len];
    for (int i = 0; i < len; ++i) {
        if (fscanf(out_txt, "%c", &buffer[i]) == EOF) {
            break;
        }
    }
    buffer[len-1] = '\0';
    if (strstr(buffer, "error") != NULL) {
        printf("HELYTELEN");
    } else {
        printf("HELYES");
    }
    fclose(out_txt);
    return 0;
}