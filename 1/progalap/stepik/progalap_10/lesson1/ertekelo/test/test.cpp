#include <gtest/gtest.h>

#define main main_0
#include "../src/ertekelo.c"
#undef main

#include "../../tools.cpp"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char filename[] = "program.c";
    writeContentToFile(filename, "int main() { return 0; }");

    char elso[] = "./ertekelo";
    char* params[] = {elso, filename, NULL};

    char str[20];
    IO("", main_0(2, params), str);

    ASSERT_STREQ(str, "HELYES");
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char filename[] = "program.c";
    writeContentToFile(filename, "int main() { return \"cica\"; }");

    char elso[] = "./ertekelo";
    char* params[] = {elso, filename, NULL};

    char str[20];
    IO("", main_0(2, params), str);

    ASSERT_STREQ(str, "HELYES");
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char filename[] = "program.c";
    writeContentToFile(filename, "int main() { 5; return \"cica\"; }");

    char elso[] = "./ertekelo";
    char* params[] = {elso, filename, NULL};

    char str[20];
    IO("", main_0(2, params), str);

    ASSERT_STREQ(str, "HELYES");
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char filename[] = "cicak.c";
    writeContentToFile(filename, "int main() { return 0 }");

    char elso[] = "./ertekelo";
    char* params[] = {elso, filename, NULL};

    char str[20];
    IO("", main_0(2, params), str);

    ASSERT_STREQ(str, "HELYTELEN");
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char filename[] = "kutyak.c";
    writeContentToFile(filename, "int main() { return valami(); } int valami() { return 4; }");

    char elso[] = "./ertekelo";
    char* params[] = {elso, filename, NULL};

    char str[20];
    IO("", main_0(2, params), str);

    ASSERT_STREQ(str, "HELYES");
}

TEST(Teszt, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char filename[] = "kutyak.c";
    writeContentToFile(filename, "int 5a; int main() {}");

    char elso[] = "./ertekelo";
    char* params[] = {elso, filename, NULL};

    char str[20];
    IO("", main_0(2, params), str);

    ASSERT_STREQ(str, "HELYTELEN");
}

TEST(Teszt, 07) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char filename[] = "lovak.c";
    writeContentToFile(filename, "int main() { int a = b; return 0; }");

    char elso[] = "./ertekelo";
    char* params[] = {elso, filename, NULL};

    char str[20];
    IO("", main_0(2, params), str);

    ASSERT_STREQ(str, "HELYTELEN");
}

TEST(Teszt, 08) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char filename[] = "lovak.c";
    writeContentToFile(filename, "int main() { printf(\"%d\", 5, 6); return 0; }");

    char elso[] = "./ertekelo";
    char* params[] = {elso, filename, NULL};

    char str[20];
    IO("", main_0(2, params), str);

    ASSERT_STREQ(str, "HELYES");
}

TEST(Teszt, 09) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char filename[] = "tehenek.c";
    writeContentToFile(filename, "int main() { int masik() } }");

    char elso[] = "./ertekelo";
    char* params[] = {elso, filename, NULL};

    char str[20];
    IO("", main_0(2, params), str);

    ASSERT_STREQ(str, "HELYTELEN");
}

TEST(Teszt, 10) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char filename[] = "tehenek.c";
    writeContentToFile(filename, "class A {}");

    char elso[] = "./ertekelo";
    char* params[] = {elso, filename, NULL};

    char str[20];
    IO("", main_0(2, params), str);

    ASSERT_STREQ(str, "HELYTELEN");
}