#include <gtest/gtest.h>

#define main main_0
#include "../src/fozes.c"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Konyha k;
    k.talakSzama = 130;
    k.receptSzam = 20;

    k.tejMennyiseg = 3;
    ASSERT_DOUBLE_EQ(k.tejMennyiseg, 3);
    ASSERT_EQ(k.talakSzama, 0);
    ASSERT_EQ(k.receptSzam, 0);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Konyha k;
    k.talakSzama = 20;
    k.tejMennyiseg = 3;
    k.receptSzam = 21;
    ASSERT_EQ(k.talakSzama, 21);
    ASSERT_EQ(k.receptSzam, 21);
}