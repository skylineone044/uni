#include <gtest/gtest.h>

#define main main_0
#include "../../tools.cpp"
#undef main

namespace elso {
    #include "../src/fuggvenynev.c"

    TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
        char str[100];
        IO("", teszt(), str);
        ASSERT_STREQ(str, "teszt");
    }
}

namespace masodik {
#define teszt masik
#include "../src/fuggvenynev.c"
#undef teszt

    TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
        char str[100];
        IO("", masik(), str);
        ASSERT_STREQ(str, "masik");
    }
}

namespace harmadik {
#define teszt cicus
#include "../src/fuggvenynev.c"
#undef teszt

    TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
        char str[100];
        IO("", cicus(), str);
        ASSERT_STREQ(str, "cicus");
    }
}

namespace negyedik {
#define teszt kiscicakadobozbanalszanak
#include "../src/fuggvenynev.c"
#undef teszt

    TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
        char str[100];
        IO("", kiscicakadobozbanalszanak(), str);
        ASSERT_STREQ(str, "kiscicakadobozbanalszanak");
    }
}