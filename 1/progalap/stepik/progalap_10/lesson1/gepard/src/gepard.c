#include <time.h>

int gyorsabb(int f1(int a), int f2(int a)) {
    int szam = 1073676287;
    clock_t start = clock();
    f1(szam);
    clock_t end = clock();
    double t1 = (double)(end - start);
    start = clock();
    f2(szam);
    end = clock();
    double t2 = (double)(end - start);

return (t1 < t2 ? 1 : 2);


}