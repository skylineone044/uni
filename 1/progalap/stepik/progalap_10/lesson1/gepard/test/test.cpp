#include <gtest/gtest.h>
#include <cmath>

#define main main_0
#include "../src/gepard.c"
#undef main

int f1(int ertek) {
    int prim;

    for (int i = 2; i < ertek; i++) {
        if (ertek % i == 0) {
            prim = 0;
        }
    }

    return prim;
}

int f2(int ertek) {
    for (int i = 2; i < ertek; i++) {
        if (ertek % i == 0) {
            return 0;
        }
    }

    return 1;
}

int f3(int ertek) {
    for (int i = 2; i <= ertek / 2; i++) {
        if (ertek % i == 0) {
            return 0;
        }
    }

    return 1;
}

int f4(int ertek) {
    for (int i = 2; i <= sqrt(ertek); i++) {
        if (ertek % i == 0) {
            return 0;
        }
    }

    return 1;
}

int f5(int ertek) {
    if (ertek == 2) {
        return 1;
    }

    if (ertek % 2 == 0) {
        return 0;
    }

    for (int i = 3; i <= sqrt(ertek); i+=2) {
        if (ertek % i == 0) {
            return 0;
        }
    }

    return 1;
}

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(gyorsabb(f1, f2), 2);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(gyorsabb(f2, f3), 2);
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(gyorsabb(f4, f3), 1);
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(gyorsabb(f5, f4), 1);
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(gyorsabb(f2, f5), 2);
}

TEST(Teszt, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(gyorsabb(f5, f1), 1);
}

TEST(Teszt, 07) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(gyorsabb(f5, f2), 1);
}