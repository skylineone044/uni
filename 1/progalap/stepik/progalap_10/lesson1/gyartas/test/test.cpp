#include <gtest/gtest.h>

#define main main_0
#include "../src/gyartas.c"
#undef main

int s1() { return 1; }
int s2() { return 3; }

int f1(int a) { return 2*a; }
int f2(int a) { return a*a; }
int f3(int a) { return -a; }
int f4(int a) { return a+5; }
int f5(int a) { return a*a-a; }

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Elem e1; e1.fuggveny = reinterpret_cast<int(*)(int)>(&s1);
    Elem e2; e2.fuggveny = reinterpret_cast<int(*)(int)>(&f1);
    Elem e3; e3.fuggveny = reinterpret_cast<int(*)(int)>(&f2);
    Elem e4; e4.fuggveny = reinterpret_cast<int(*)(int)>(&f3);
    Elem e5; e5.fuggveny = reinterpret_cast<int(*)(int)>(&f4);

    e1.kovetkezo = &e2;
    e2.kovetkezo = &e3;
    e3.kovetkezo = &e4;
    e4.kovetkezo = &e5;
    e5.kovetkezo = NULL;

    int res = kiszamit(&e1);
    ASSERT_EQ(res, 1);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Elem e1; e1.fuggveny = reinterpret_cast<int(*)(int)>(&s2);
    Elem e2; e2.fuggveny = reinterpret_cast<int(*)(int)>(&f1);
    Elem e3; e3.fuggveny = reinterpret_cast<int(*)(int)>(&f3);
    Elem e4; e4.fuggveny = reinterpret_cast<int(*)(int)>(&f3);
    Elem e5; e5.fuggveny = reinterpret_cast<int(*)(int)>(&f5);

    e1.kovetkezo = &e2;
    e2.kovetkezo = &e3;
    e3.kovetkezo = &e4;
    e4.kovetkezo = &e5;
    e5.kovetkezo = NULL;

    int res = kiszamit(&e1);
    ASSERT_EQ(res, 30);
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Elem e1; e1.fuggveny = reinterpret_cast<int(*)(int)>(&s1);
    Elem e2; e2.fuggveny = reinterpret_cast<int(*)(int)>(&f5);
    Elem e3; e3.fuggveny = reinterpret_cast<int(*)(int)>(&f5);
    Elem e4; e4.fuggveny = reinterpret_cast<int(*)(int)>(&f5);
    Elem e5; e5.fuggveny = reinterpret_cast<int(*)(int)>(&f5);

    e1.kovetkezo = &e2;
    e2.kovetkezo = &e3;
    e3.kovetkezo = &e4;
    e4.kovetkezo = &e5;
    e5.kovetkezo = NULL;

    int res = kiszamit(&e1);
    ASSERT_EQ(res, 0);
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Elem e1; e1.fuggveny = reinterpret_cast<int(*)(int)>(&s2);
    Elem e2; e2.fuggveny = reinterpret_cast<int(*)(int)>(&f2);
    Elem e3; e3.fuggveny = reinterpret_cast<int(*)(int)>(&f4);

    e1.kovetkezo = &e2;
    e2.kovetkezo = &e3;
    e3.kovetkezo = NULL;

    int res = kiszamit(&e1);
    ASSERT_EQ(res, 14);
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Elem e1; e1.fuggveny = reinterpret_cast<int(*)(int)>(&s2);

    e1.kovetkezo = NULL;

    int res = kiszamit(&e1);
    ASSERT_EQ(res, 3);
}