#include <gtest/gtest.h>

#define main main_0
#include "../src/hiba.c"
#undef main

#include "../../tools.cpp"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int db = 3;

    char param0[] = "./hiba";
    char param1[] = "Mariska";
    char param2[] = "50";

    char* params[] = {param0, param1, param2, NULL};

    char str[100];
    IO("", main_0(db, params), str);

    ASSERT_STREQ(str, "Sikeres parameterezes\n");
    ASSERT_STREQ(error_string, "");
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int db = 3;

    char param0[] = "./hiba";
    char param1[] = "Bela";
    char param2[] = "25";

    char* params[] = {param0, param1, param2, NULL};

    char str[100];
    IO("", main_0(db, params), str);

    ASSERT_STREQ(str, "Sikeres parameterezes\n");
    ASSERT_STREQ(error_string, "");
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int db = 2;

    char param0[] = "./hiba";
    char param1[] = "Endre40";

    char* params[] = {param0, param1, NULL};

    char str[100];
    IO("", main_0(db, params), str)

    ASSERT_STREQ(error_string, "Hasznalat: nev eredmeny\n");
    ASSERT_STREQ(str, "");
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int db = 4;

    char param0[] = "./hiba";
    char param1[] = "Mariska";
    char param2[] = "50";
    char param3[] = "70";

    char* params[] = {param0, param1, param2, param3, NULL};

    char str[100];
    IO("", main_0(db, params), str);

    ASSERT_STREQ(error_string, "Hasznalat: nev eredmeny\n");
    ASSERT_STREQ(str, "");
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int db = 1;

    char param0[] = "./hiba";

    char* params[] = {param0, NULL};

    char str[100];
    IO("", main_0(db, params), str);

    ASSERT_STREQ(error_string, "Hasznalat: nev eredmeny\n");
    ASSERT_STREQ(str, "");
}

TEST(Teszt, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int db = 1;

    char param0[] = "./hiba";

    char* params[] = {param0, NULL};

    char str[100];
    IO("", int res = main_0(db, params), str);

    ASSERT_NE(res, 0);
}

TEST(Teszt, 07) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int db = 3;

    char param0[] = "./hiba";
    char param1[] = "asd";
    char param2[] = "jo";

    char* params[] = {param0, param1, param2, NULL};

    char str[100];
    IO("", int res = main_0(db, params), str);

    ASSERT_EQ(res, 0);
}