#include <gtest/gtest.h>

#define main main_0
#include "../src/hossz.c"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char szoveg[] = "Macska";
    int h = 6;

    ASSERT_EQ(hossz(szoveg, h), h);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char szoveg[] = "Tehen Lo Kecske";
    int h = 15;

    ASSERT_EQ(hossz(szoveg, h), h);
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char szoveg[] = "Vizilo Es A Medve";
    int h = 17;

    ASSERT_EQ(hossz(szoveg, h), h);
}