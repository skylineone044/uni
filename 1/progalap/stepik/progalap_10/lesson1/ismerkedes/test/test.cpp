#include <gtest/gtest.h>

#define main main_0
#include "../src/ismerkedes.c"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char n1[] = "Janos Pal";
    char n2[] = "Nagy Jozsef";
    char n3[] = "Kis Bela";
    char n4[] = "Kis Anna";
    char n5[] = "Virag Virag";
    char n6[] = "Nagy Emma";

    char f1[] = "matematikus";
    char f2[] = "lakatos";
    char f3[] = "tanar";
    char f4[] = "pap";
    char f5[] = "asztalos";
    char f6[] = "utcasepro asszisztens";
    char f7[] = "portas";
    char f8[] = "postas";

    char* ff1[] = {f1, f2, f3, f5, NULL}; // mat
    char* ff2[] = {f4, f7, NULL};
    char* ff3[] = {f1, f4, NULL}; // mat
    char* ff4[] = {f7, f8, NULL};
    char* ff5[] = {f7, f6, f3, f2, f5, f8, NULL};
    char* ff6[] = {f7, f6, f1, f2, f5, f8, NULL}; // mat

    Ember e1; e1.foglalkozasok = ff1; e1.nev = n1; e1.eletkor = 30; e1.nem = 1; // ?
    Ember e2; e2.foglalkozasok = ff2; e2.nev = n2; e2.eletkor = 47; e2.nem = 1;
    Ember e3; e3.foglalkozasok = ff3; e3.nev = n3; e3.eletkor = 18; e3.nem = 0; // ?
    Ember e4; e4.foglalkozasok = ff4; e4.nev = n4; e4.eletkor = 44; e4.nem = 0;
    Ember e5; e5.foglalkozasok = ff5; e5.nev = n5; e5.eletkor = 47; e5.nem = 2;
    Ember e6; e6.foglalkozasok = ff6; e6.nev = n6; e6.eletkor = 18; e6.nem = 0; // ?

    Ember emberek[] = {e1, e2, e3, e4, e5, e6, NULL};

    informacioNullazas();
    int ertek = 47;
    informaciotHozzaad('c', 'e', &ertek);
    char* res = mianeve(emberek, 6);
    ASSERT_EQ(res, nullptr);

    informacioNullazas();
    int ertek2 = 23;
    informaciotHozzaad('a', 'e', &ertek2);
    res = mianeve(emberek, 6);
    ASSERT_EQ(res, nullptr);

    char s1[] = "Nagy";
    informaciotHozzaad('d', 'n', s1);
    res = mianeve(emberek, 6);
    ASSERT_STREQ(res, "Nagy Emma");

    informacioNullazas();

    int ertek3 = 29;
    informaciotHozzaad('b', 'e', &ertek3);
    res = mianeve(emberek, 6);
    ASSERT_STREQ(res, "Janos Pal");

    informacioNullazas();

    int ertek4 = 0;
    informaciotHozzaad('c', 'g', &ertek4);
    res = mianeve(emberek, 6);
    ASSERT_EQ(res, nullptr);

    char s2[] = "a";
    informaciotHozzaad('d', 'n', s2);
    res = mianeve(emberek, 6);
    ASSERT_EQ(res, nullptr);

    char s3[] = "asztalos";
    informaciotHozzaad('d', 'f', s3);
    res = mianeve(emberek, 6);
    ASSERT_STREQ(res, "Nagy Emma");

    informacioNullazas();

    char s4[] = "asztalo";
    informaciotHozzaad('d', 'f', s4);
    res = mianeve(emberek, 6);
    ASSERT_EQ(res, nullptr);

    int ertek5 = 1;
    informaciotHozzaad('c', 'g', &ertek5);
    res = mianeve(emberek, 6);
    ASSERT_EQ(res, nullptr);

    informacioNullazas();

    int ertek7 = 19;
    informaciotHozzaad('a', 'e', &ertek7);
    res = mianeve(emberek, 6);
    ASSERT_EQ(res, nullptr);

    int ertek8 = 17;
    informaciotHozzaad('b', 'e', &ertek8);
    res = mianeve(emberek, 6);
    ASSERT_EQ(res, nullptr);

    char s5[] = "lakatos";
    informaciotHozzaad('d', 'f', &s5);
    res = mianeve(emberek, 6);
    ASSERT_STREQ(res, "Nagy Emma");

    informacioNullazas();

    char s6[] = "pap";
    informaciotHozzaad('d', 'f', &s6);
    res = mianeve(emberek, 6);
    ASSERT_STREQ(res, "Kis Bela");

    informacioNullazas();
    int ertek9 = 25;
    informaciotHozzaad('b', 'e', &ertek9);
    res = mianeve(emberek, 6);
    ASSERT_STREQ(res, "Janos Pal");

    int ertek10 = 11;
    informaciotHozzaad('b', 'e', &ertek10);
    res = mianeve(emberek, 6);
    ASSERT_STREQ(res, "Janos Pal");

    int ertek11 = 40;
    informaciotHozzaad('a', 'e', &ertek11);
    res = mianeve(emberek, 6);
    ASSERT_STREQ(res, "Janos Pal");

    int ertek12 = 29;
    informaciotHozzaad('a', 'e', &ertek12);
    res = mianeve(emberek, 6);
    ASSERT_EQ(res, nullptr);

    for (int i = 0; i < 10000000; i++) {
        informaciotHozzaad('a', 'e', &ertek12);
    }

}