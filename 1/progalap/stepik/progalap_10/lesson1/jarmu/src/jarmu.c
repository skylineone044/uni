#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    char* marka;
    int ajtokSzama;
    int maximalisSebesseg;
} Szemelygepkocsi;

typedef struct {
    int maximalisMagassag;
    int maximalisUtasokSzama;
    int gyorsulas;
} Repulo;

typedef struct {
    char* szin;
    int kivilagitva;
} Bicikli;

enum Tipus {SZEMELYGEPKOCSI, REPULO, BICIKLI};

typedef struct {
    enum Tipus tipus;
    union {
        Szemelygepkocsi szemelygepkocsi;
        Repulo repulo;
        Bicikli bicikli;
    }jarmu;
} Jarmu;


void kiir(Jarmu jarmu) {
    if (jarmu.tipus == SZEMELYGEPKOCSI) {
        printf("[Szemelygepkocsi] %s;%d;%d",
               jarmu.jarmu.szemelygepkocsi.marka,
               jarmu.jarmu.szemelygepkocsi.ajtokSzama,
               jarmu.jarmu.szemelygepkocsi.maximalisSebesseg);
    } else if (jarmu.tipus == REPULO) {
        printf("[Repulo] %d;%d;%d",
               jarmu.jarmu.repulo.maximalisMagassag,
               jarmu.jarmu.repulo.maximalisUtasokSzama,
               jarmu.jarmu.repulo.gyorsulas);
    } else if (jarmu.tipus == BICIKLI) {
        printf("[Bicikli] %s;%s",
               jarmu.jarmu.bicikli.szin,
               (jarmu.jarmu.bicikli.kivilagitva == 1 ? "ki van vilagitva" : "nincs kivilagitva"));
    }
}

Jarmu letrehoz(enum Tipus tipus) {
    Jarmu jarmu;
    if (tipus == SZEMELYGEPKOCSI) {
        jarmu.tipus = SZEMELYGEPKOCSI;
        Szemelygepkocsi szemelygepkocsi;
        szemelygepkocsi.marka = (char*)malloc(sizeof(char)*20);
        scanf("%s %d %d",
               szemelygepkocsi.marka,
               &szemelygepkocsi.ajtokSzama,
               &szemelygepkocsi.maximalisSebesseg);
        jarmu.jarmu.szemelygepkocsi = szemelygepkocsi;
    } else if (tipus == REPULO) {
        jarmu.tipus = REPULO;
        Repulo repulo;
        scanf("%d %d %d",
               &repulo.maximalisMagassag,
               &repulo.maximalisUtasokSzama,
               &repulo.gyorsulas);
        jarmu.jarmu.repulo = repulo;
    } else if (tipus == BICIKLI) {
        jarmu.tipus = BICIKLI;
        Bicikli bicikli;
        char kivilagitva[30];
        char *szin = (char*)malloc(sizeof(char)*30);
        scanf("%s %s",
               szin,
               kivilagitva);
        bicikli.szin = szin;
        bicikli.kivilagitva = (strcmp(kivilagitva, "igen") ? 0 : 1);
        jarmu.jarmu.bicikli = bicikli;
    }
    return jarmu;
}