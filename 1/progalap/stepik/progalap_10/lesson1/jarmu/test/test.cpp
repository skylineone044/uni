#include <gtest/gtest.h>

#define main main_0
#include "../src/jarmu.c"
#undef main

#include "../../tools.cpp"

TEST(Szemelygepkocsi, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Szemelygepkocsi s;
    s.ajtokSzama = 7;
    s.maximalisSebesseg = 5;
    char marka[100] = "asd";
    s.marka = marka;

    ASSERT_EQ(s.ajtokSzama, 7);
    ASSERT_EQ(s.maximalisSebesseg, 5);
    ASSERT_STREQ(s.marka, "asd");
}

TEST(Repulo, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Repulo r;
    r.gyorsulas = 50;
    r.maximalisUtasokSzama = 3;
    r.maximalisMagassag = 400;

    ASSERT_EQ(r.gyorsulas, 50);
    ASSERT_EQ(r.maximalisUtasokSzama, 3);
    ASSERT_EQ(r.maximalisMagassag, 400);
}

TEST(Bicikli, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Bicikli b;
    b.kivilagitva = 7;
    char s[] = "piros";
    b.szin = s;

    ASSERT_EQ(b.kivilagitva, 7);
    ASSERT_STREQ(b.szin, "piros");
}

TEST(Tipus, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Tipus t1 = SZEMELYGEPKOCSI;
    t1 = REPULO;
    t1 = BICIKLI;
}

TEST(Jarmu, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Jarmu j1;
    j1.tipus = BICIKLI;
    Bicikli b;
    j1.jarmu.bicikli = b;

    j1.tipus = REPULO;
    Repulo r;
    j1.jarmu.repulo = r;

    j1.tipus = SZEMELYGEPKOCSI;
    Szemelygepkocsi s;
    j1.jarmu.szemelygepkocsi = s;

    int bbb = sizeof(Bicikli);
    int rrr = sizeof(Repulo);
    int sss = sizeof(Szemelygepkocsi);
    int max = bbb > rrr ? sss > bbb ? sss : bbb : sss > rrr ? sss : rrr;

    ASSERT_LT(sizeof(j1.jarmu), bbb+rrr+sss);
    ASSERT_EQ(sizeof(j1.jarmu), max);
}

TEST(kiir, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Jarmu j;
    j.tipus = BICIKLI;
    Bicikli b;
    char c[] = "pirosas";
    b.szin = c;
    b.kivilagitva = 1;
    j.jarmu.bicikli = b;

    char str[1000];
    IO("", kiir(j), str);
    ASSERT_STREQ(str, "[Bicikli] pirosas;ki van vilagitva");
}

TEST(kiir, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Jarmu j;
    j.tipus = REPULO;
    Repulo r;
    r.maximalisMagassag = 540;
    r.maximalisUtasokSzama = 4;
    r.gyorsulas = 64;
    j.jarmu.repulo = r;

    char str[1000];
    IO("", kiir(j), str);
    ASSERT_STREQ(str, "[Repulo] 540;4;64");
}

TEST(kiir, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Jarmu j;
    j.tipus = SZEMELYGEPKOCSI;
    Szemelygepkocsi s;
    char c[] = "asm";
    s.marka = c;
    s.maximalisSebesseg = 67;
    s.ajtokSzama = 4;
    j.jarmu.szemelygepkocsi = s;

    char str[1000];
    IO("", kiir(j), str);
    ASSERT_STREQ(str, "[Szemelygepkocsi] asm;4;67");
}

TEST(kiir, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Jarmu j;
    j.tipus = BICIKLI;
    Bicikli b;
    char c[] = "piros";
    b.szin = c;
    b.kivilagitva = 0;
    j.jarmu.bicikli = b;

    char str[1000];
    IO("", kiir(j), str);
    ASSERT_STREQ(str, "[Bicikli] piros;nincs kivilagitva");
}

TEST(kiir, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Jarmu j;
    j.tipus = REPULO;
    Repulo r;
    r.maximalisMagassag = 54;
    r.maximalisUtasokSzama = 40;
    r.gyorsulas = 1;
    j.jarmu.repulo = r;

    char str[1000];
    IO("", kiir(j), str);
    ASSERT_STREQ(str, "[Repulo] 54;40;1");
}

TEST(kiir, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Jarmu j;
    j.tipus = SZEMELYGEPKOCSI;
    Szemelygepkocsi s;
    char c[] = "cpp";
    s.marka = c;
    s.maximalisSebesseg = 4;
    s.ajtokSzama = 24;
    j.jarmu.szemelygepkocsi = s;

    char str[1000];
    IO("", kiir(j), str);
    ASSERT_STREQ(str, "[Szemelygepkocsi] cpp;24;4");
}

TEST(letrehoz, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("gyors 7 300", Jarmu ret = letrehoz(SZEMELYGEPKOCSI), str);
    ASSERT_EQ(ret.tipus, SZEMELYGEPKOCSI);
    ASSERT_STREQ(ret.jarmu.szemelygepkocsi.marka, "gyors");
    ASSERT_EQ(ret.jarmu.szemelygepkocsi.ajtokSzama, 7);
    ASSERT_EQ(ret.jarmu.szemelygepkocsi.maximalisSebesseg, 300);
}

TEST(letrehoz, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("lassu 4 320", Jarmu ret = letrehoz(SZEMELYGEPKOCSI), str);
    ASSERT_EQ(ret.tipus, SZEMELYGEPKOCSI);
    ASSERT_STREQ(ret.jarmu.szemelygepkocsi.marka, "lassu");
    ASSERT_EQ(ret.jarmu.szemelygepkocsi.ajtokSzama, 4);
    ASSERT_EQ(ret.jarmu.szemelygepkocsi.maximalisSebesseg, 320);
}

TEST(letrehoz, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("piros igen", Jarmu ret = letrehoz(BICIKLI), str);
    ASSERT_EQ(ret.tipus, BICIKLI);
    ASSERT_STREQ(ret.jarmu.bicikli.szin, "piros");
    ASSERT_EQ(ret.jarmu.bicikli.kivilagitva, 1);
}

TEST(letrehoz, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("kek nem", Jarmu ret = letrehoz(BICIKLI), str);
    ASSERT_EQ(ret.tipus, BICIKLI);
    ASSERT_STREQ(ret.jarmu.bicikli.szin, "kek");
    ASSERT_EQ(ret.jarmu.bicikli.kivilagitva, 0);
}

TEST(letrehoz, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("3 5 7", Jarmu ret = letrehoz(REPULO), str);
    ASSERT_EQ(ret.tipus, REPULO);
    ASSERT_EQ(ret.jarmu.repulo.maximalisMagassag, 3);
    ASSERT_EQ(ret.jarmu.repulo.maximalisUtasokSzama, 5);
    ASSERT_EQ(ret.jarmu.repulo.gyorsulas, 7);
}

TEST(letrehoz, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("420 20 111", Jarmu ret = letrehoz(REPULO), str);
    ASSERT_EQ(ret.tipus, REPULO);
    ASSERT_EQ(ret.jarmu.repulo.maximalisMagassag, 420);
    ASSERT_EQ(ret.jarmu.repulo.maximalisUtasokSzama, 20);
    ASSERT_EQ(ret.jarmu.repulo.gyorsulas, 111);
}