enum JatekAllapot{IN_MENU = 0, IN_GAME, PAUSED};

int loves(int Allapot) {
    if (Allapot != IN_GAME) {
        return 0;
    }
    return 1;
}

void esc(enum JatekAllapot *Allapot) {
    if (*Allapot == IN_GAME) {
        *Allapot = PAUSED;
    } else if (*Allapot == PAUSED){
        *Allapot = IN_GAME;
    }
}

void start(enum JatekAllapot *Allapot) {
    if (*Allapot == IN_MENU) {
        *Allapot = IN_GAME;
    }
}