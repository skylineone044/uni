#include <gtest/gtest.h>

#define main main_0
#include "../src/jatek.c"
#undef main

TEST(loves, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    JatekAllapot a = IN_MENU;
    ASSERT_EQ(loves(a), 0);

    JatekAllapot b = IN_GAME;
    ASSERT_EQ(loves(b), 1);

    JatekAllapot c = PAUSED;
    ASSERT_EQ(loves(c), 0);
}

TEST(esc, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    JatekAllapot a = IN_MENU;
    esc(&a);
    ASSERT_EQ(a, IN_MENU);

    JatekAllapot b = IN_GAME;
    esc(&b);
    ASSERT_EQ(b, PAUSED);

    JatekAllapot c = PAUSED;
    esc(&c);
    ASSERT_EQ(c, IN_GAME);
}

TEST(start, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    JatekAllapot a = IN_MENU;
    start(&a);
    ASSERT_EQ(a, IN_GAME);

    JatekAllapot b = IN_GAME;
    start(&b);
    ASSERT_EQ(b, IN_GAME);

    JatekAllapot c = PAUSED;
    start(&c);
    ASSERT_EQ(c, PAUSED);
}