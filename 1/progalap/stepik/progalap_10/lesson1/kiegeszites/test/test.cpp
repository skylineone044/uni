#include <gtest/gtest.h>

#define main main_0
#include "../src/kiegeszites.c"
#undef main

#include "../../tools.cpp"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
#ifndef print
    FAIL();
#endif

    char str[100];
    IO("", print("cica"), str);
    ASSERT_STREQ(str, "cica [line: 15]\n");
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", print("a kutya elment setalni, de nem ment 500 meternel messzebb"), str);
    ASSERT_STREQ(str, "a kutya elment setalni, de nem ment 500 meternel messzebb [line: 21]\n");
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", print(""), str);
    ASSERT_STREQ(str, " [line: 27]\n");
}