#include <gtest/gtest.h>

#define main main_0
#include "../src/kiiro.h"
#include "../src/kiiro.h"
#include "../src/kiiro.h"
#include "../src/kiiro.h"
#include "../src/kiiro.h"
#include "../src/kiiro.h"
#include "../src/kiiro.h"
#undef main

#include "../../tools.cpp"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[50];
    IO("", kiir(50), str);
    ASSERT_STREQ(str, "50");
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[50];
    IO("", kiir(33), str);
    ASSERT_STREQ(str, "33");
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[50];
    IO("", kiir(515421), str);
    ASSERT_STREQ(str, "515421");
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[50];
    IO("", kiir(1), str);
    ASSERT_STREQ(str, "1");
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[50];
    IO("", kiir(-42), str);
    ASSERT_STREQ(str, "-42");
}