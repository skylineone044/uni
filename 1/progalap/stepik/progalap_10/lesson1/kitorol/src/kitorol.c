#include <stdlib.h>
#include <string.h>

int in(char keresett, const char tomb[]) {
    for (int i = 0; tomb[i] != '\0'; ++i) {
        if (tomb[i] == keresett) {
            return 1;
        }
    }
    return 0;
}

char *kitorol(const char eredeti[], const char torlendo[]) {
    unsigned long len = strlen(eredeti);
    char* eredmeny = (char*)malloc(sizeof(char)*(len));
    for (int i = 0; i <= len; ++i) {
        eredmeny[i] = '\0';
    }
    int next = 0;
    for (int i = 0; i < len; ++i) {
        if (!in(eredeti[i], torlendo)) {
            eredmeny[next] = eredeti[i];
            next++;
        }
    }
    return eredmeny;
}