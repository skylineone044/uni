#include <gtest/gtest.h>

#define main main_0
#include "../src/kitorol.c"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const char* str = "macska";
    const char* torlendo = "a";
    const char* elvart = "mcsk";

    char* res = kitorol(str, torlendo);
    ASSERT_STREQ(res, elvart);
    free(res);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const char* str = "kigyouborka";
    const char* torlendo = "uborka";
    const char* elvart = "igy";

    char* res = kitorol(str, torlendo);
    ASSERT_STREQ(res, elvart);
    free(res);
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const char* str = "a kis kamion az ut szelen megallt";
    const char* torlendo = "amk @ LON";
    const char* elvart = "isionzutszelenegllt";

    char* res = kitorol(str, torlendo);
    ASSERT_STREQ(res, elvart);
    free(res);
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const char* str = "zebra";
    const char* torlendo = "ASVOSMABINVKSDNBANADFKVNABDFJBADNFKV NSDFVBKASNDVSDNASDINBSINVISNVIDSN";
    const char* elvart = "zebra";

    char* res = kitorol(str, torlendo);
    ASSERT_STREQ(res, elvart);
    free(res);
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const char* str = "a";
    const char* torlendo = "ASVOSMABINVKSDNBANADFKVNABDFJBADNFKV NSDFVBKASNDVSDNASDINBSINVISNVIDSN";
    const char* elvart = "a";

    char* res = kitorol(str, torlendo);
    ASSERT_STREQ(res, elvart);
    free(res);
}

TEST(Teszt, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const char* str = "kicsi";
    const char* torlendo = "ikes ige";
    const char* elvart = "c";

    char* res = kitorol(str, torlendo);
    ASSERT_STREQ(res, elvart);
    free(res);
}

TEST(Teszt, 07) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const char* str = "kicsi";
    const char* torlendo = "ikes igem nincs nekem";
    const char* elvart = "";

    char* res = kitorol(str, torlendo);
    ASSERT_STREQ(res, elvart);
    free(res);
}