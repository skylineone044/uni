#include <gtest/gtest.h>

#define main main_0
#include "../src/konstans.c"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(ertek, 7);
    duplaz();
    ASSERT_EQ(ertek, 14);
    duplaz();
    ASSERT_EQ(ertek, 28);
    duplaz();
    ASSERT_EQ(ertek, 56);
    duplaz();
    ASSERT_EQ(ertek, 112);
    duplaz();
}