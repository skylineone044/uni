#include <stdlib.h>

typedef struct Elem Elem;

typedef struct Elem {
    int ertek;
    Elem* elozo;
    Elem* kovetkezo;
} Elem;

int listaosszeg(Elem *listaelem) {
    int osszeg = 0;
    while (listaelem->kovetkezo != NULL) {
        osszeg += listaelem->ertek;
        listaelem = listaelem->kovetkezo;
    }
    osszeg += listaelem->ertek;
    return osszeg;
}

void beszur(Elem *ezutan, Elem *ezt) {
    ezt->elozo = ezutan;
    ezt->kovetkezo = ezutan->kovetkezo;
    if (ezutan->kovetkezo != NULL) {
        ezutan->kovetkezo->elozo = ezt;
    } else {
        ezutan->kovetkezo = ezt;
    }
    ezutan->kovetkezo = ezt;
}

int maximum(Elem *kivalsztott) {
    int legnagyobb = kivalsztott->ertek;
    while (kivalsztott->elozo != NULL) {
        if (kivalsztott->ertek > legnagyobb) {
            legnagyobb = kivalsztott->ertek;
        }
        kivalsztott = kivalsztott->elozo;
    }
    while (kivalsztott->kovetkezo != NULL) {
        if (kivalsztott->ertek > legnagyobb) {
            legnagyobb = kivalsztott->ertek;
        }
        kivalsztott = kivalsztott->kovetkezo;
    }
    return legnagyobb;
}

Elem* torol(Elem *elem, int torolnivslo_ertek) {
    if (elem->elozo == NULL && elem->kovetkezo == NULL && elem->ertek == torolnivslo_ertek) {
        return NULL;
    }
    while (elem->kovetkezo != NULL) {
        if (elem->ertek == torolnivslo_ertek) {
                if (elem->elozo == NULL && elem->kovetkezo == NULL) {
                    return NULL;
                } else if (elem->elozo == NULL && elem->kovetkezo != NULL) {
                    elem->kovetkezo->elozo = NULL;
                } else if (elem->elozo != NULL && elem->kovetkezo == NULL) {
                    elem->elozo->kovetkezo = NULL;
                } else if (elem->elozo != NULL && elem->kovetkezo != NULL) {
                    elem->elozo->kovetkezo = elem->kovetkezo;
                    elem->kovetkezo->elozo = elem->elozo;
                }
        }
        elem = elem->kovetkezo;
    }
    if (elem->elozo == NULL && elem->kovetkezo == NULL) {
        return NULL;
    }
    if (elem->kovetkezo == NULL && elem->ertek == torolnivslo_ertek) {
        elem->elozo->kovetkezo = NULL;
        elem = elem->elozo;
    }

    while (elem->elozo != NULL) {
        elem = elem->elozo;
    }
    return elem;
}