#include <gtest/gtest.h>

#define main main_0
#include "../src/lanc.c"
#undef main

TEST(Elem, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Elem e1;
    Elem e2;

    e1.ertek = 2;
    e2.ertek = 5;
    e1.kovetkezo = &e2;
    e2.elozo = &e1;
    e2.kovetkezo = NULL;
    e1.elozo = NULL;
}

TEST(listaosszeg, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Elem e1; e1.ertek = 4;
    Elem e2; e2.ertek = 7;
    Elem e3; e3.ertek = 5;
    Elem e4; e4.ertek = 3;
    Elem e5; e5.ertek = 8;

    e1.elozo = NULL; e1.kovetkezo = &e2;
    e2.elozo = &e1; e2.kovetkezo = &e3;
    e3.elozo = &e2; e3.kovetkezo = &e4;
    e4.elozo = &e3; e4.kovetkezo = &e5;
    e5.elozo = &e4; e5.kovetkezo = NULL;

    int res = listaosszeg(&e1);
    ASSERT_EQ(res, 27);
}

TEST(listaosszeg, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Elem e1; e1.ertek = 3;
    Elem e2; e2.ertek = 0;
    Elem e3; e3.ertek = 2;
    Elem e4; e4.ertek = -1;
    Elem e5; e5.ertek = 4;
    Elem e6; e6.ertek = 8;
    Elem e7; e7.ertek = 11;
    Elem e8; e8.ertek = 20;
    Elem e9; e9.ertek = -4;

    e1.elozo = NULL; e1.kovetkezo = &e2;
    e2.elozo = &e1; e2.kovetkezo = &e3;
    e3.elozo = &e2; e3.kovetkezo = &e4;
    e4.elozo = &e3; e4.kovetkezo = &e5;
    e5.elozo = &e4; e5.kovetkezo = &e6;
    e6.elozo = &e5; e6.kovetkezo = &e7;
    e7.elozo = &e6; e7.kovetkezo = &e8;
    e8.elozo = &e7; e8.kovetkezo = &e9;
    e9.elozo = &e8; e9.kovetkezo = NULL;

    int res = listaosszeg(&e1);
    ASSERT_EQ(res, 43);
}

TEST(listaosszeg, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Elem e1; e1.ertek = 3;

    e1.elozo = NULL; e1.kovetkezo = NULL;

    int res = listaosszeg(&e1);
    ASSERT_EQ(res, 3);
}

TEST(beszur, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Elem e1; e1.ertek = 3;
    Elem e2; e2.ertek = 0;
    Elem e3; e3.ertek = 2;
    Elem e4; e4.ertek = -1;
    Elem e5; e5.ertek = 4;
    Elem e6; e6.ertek = 8;
    Elem e7; e7.ertek = 11;
    Elem e8; e8.ertek = 20;
    Elem e9; e9.ertek = -4;

    e1.elozo = NULL; e1.kovetkezo = &e2;
    e2.elozo = &e1; e2.kovetkezo = &e3;
    e3.elozo = &e2; e3.kovetkezo = &e4;
    e4.elozo = &e3; e4.kovetkezo = &e5;
    e5.elozo = &e4; e5.kovetkezo = &e6;
    e6.elozo = &e5; e6.kovetkezo = &e7;
    e7.elozo = &e6; e7.kovetkezo = &e8;
    e8.elozo = &e7; e8.kovetkezo = &e9;
    e9.elozo = &e8; e9.kovetkezo = NULL;

    Elem uj; uj.ertek = 777; uj.elozo = NULL; uj.kovetkezo = NULL;
    beszur(&e3, &uj);

    ASSERT_EQ(e2.kovetkezo, &e3);
    ASSERT_EQ(e3.elozo, &e2);

    ASSERT_EQ(e3.kovetkezo, &uj);
    ASSERT_EQ(uj.elozo, &e3);
    ASSERT_EQ(uj.kovetkezo, &e4);
    ASSERT_EQ(e4.elozo, &uj);

    ASSERT_EQ(uj.ertek, 777);
}

TEST(beszur, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Elem e1; e1.ertek = 3;
    Elem e2; e2.ertek = 0;
    Elem e3; e3.ertek = 2;

    e1.elozo = NULL; e1.kovetkezo = &e2;
    e2.elozo = &e1; e2.kovetkezo = &e3;
    e3.elozo = &e2; e3.kovetkezo = NULL;

    Elem uj; uj.ertek = 10; uj.elozo = NULL; uj.kovetkezo = NULL;
    beszur(&e3, &uj);

    ASSERT_EQ(e2.kovetkezo, &e3);
    ASSERT_EQ(e3.elozo, &e2);

    ASSERT_EQ(e3.kovetkezo, &uj);
    ASSERT_EQ(uj.elozo, &e3);
    ASSERT_EQ(uj.kovetkezo, nullptr);

    ASSERT_EQ(uj.ertek, 10);
}

TEST(beszur, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Elem e1; e1.ertek = 3;
    Elem e2; e2.ertek = 0;
    Elem e3; e3.ertek = 2;

    e1.elozo = NULL; e1.kovetkezo = &e2;
    e2.elozo = &e1; e2.kovetkezo = &e3;
    e3.elozo = &e2; e3.kovetkezo = NULL;

    Elem uj; uj.ertek = 100; uj.elozo = NULL; uj.kovetkezo = NULL;
    beszur(&e1, &uj);

    ASSERT_EQ(e1.kovetkezo, &uj);
    ASSERT_EQ(uj.elozo, &e1);
    ASSERT_EQ(uj.kovetkezo, &e2);
    ASSERT_EQ(e2.elozo, &uj);

    ASSERT_EQ(uj.ertek, 100);
}

TEST(beszur, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Elem e1; e1.ertek = 3;

    e1.elozo = NULL; e1.kovetkezo = NULL;

    Elem uj; uj.ertek = 1; uj.elozo = NULL; uj.kovetkezo = NULL;
    beszur(&e1, &uj);

    ASSERT_EQ(e1.kovetkezo, &uj);
    ASSERT_EQ(uj.elozo, &e1);
    ASSERT_EQ(uj.kovetkezo, nullptr);

    ASSERT_EQ(uj.ertek, 1);
}

TEST(maximum, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Elem e1; e1.ertek = 3;
    Elem e2; e2.ertek = 0;
    Elem e3; e3.ertek = 2;
    Elem e4; e4.ertek = -1;
    Elem e5; e5.ertek = 4;
    Elem e6; e6.ertek = 8;
    Elem e7; e7.ertek = 11;
    Elem e8; e8.ertek = 20;
    Elem e9; e9.ertek = -4;

    e1.elozo = NULL; e1.kovetkezo = &e2;
    e2.elozo = &e1; e2.kovetkezo = &e3;
    e3.elozo = &e2; e3.kovetkezo = &e4;
    e4.elozo = &e3; e4.kovetkezo = &e5;
    e5.elozo = &e4; e5.kovetkezo = &e6;
    e6.elozo = &e5; e6.kovetkezo = &e7;
    e7.elozo = &e6; e7.kovetkezo = &e8;
    e8.elozo = &e7; e8.kovetkezo = &e9;
    e9.elozo = &e8; e9.kovetkezo = NULL;

    int max = maximum(&e1);
    ASSERT_EQ(max, 20);

    max = maximum(&e5);
    ASSERT_EQ(max, 20);

    max = maximum(&e9);
    ASSERT_EQ(max, 20);

    max = maximum(&e8);
    ASSERT_EQ(max, 20);
}

TEST(maximum, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Elem e1; e1.ertek = 3;
    Elem e2; e2.ertek = 0;
    Elem e3; e3.ertek = 2;
    Elem e4; e4.ertek = -1;

    e1.elozo = NULL; e1.kovetkezo = &e2;
    e2.elozo = &e1; e2.kovetkezo = &e3;
    e3.elozo = &e2; e3.kovetkezo = &e4;
    e4.elozo = &e3; e4.kovetkezo = NULL;

    int max = maximum(&e1);
    ASSERT_EQ(max, 3);

    max = maximum(&e3);
    ASSERT_EQ(max, 3);

    max = maximum(&e2);
    ASSERT_EQ(max, 3);

    max = maximum(&e4);
    ASSERT_EQ(max, 3);
}

TEST(maximum, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Elem e1; e1.ertek = 5;

    e1.elozo = NULL; e1.kovetkezo = NULL;

    int max = maximum(&e1);
    ASSERT_EQ(max, 5);
}

TEST(torol, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Elem e1; e1.ertek = 3;
    Elem e2; e2.ertek = 0;
    Elem e3; e3.ertek = 2;
    Elem e4; e4.ertek = 5;
    Elem e5; e5.ertek = 4;
    Elem e6; e6.ertek = -8;
    Elem e7; e7.ertek = 5;
    Elem e8; e8.ertek = 5;
    Elem e9; e9.ertek = 4;

    e1.elozo = NULL; e1.kovetkezo = &e2;
    e2.elozo = &e1; e2.kovetkezo = &e3;
    e3.elozo = &e2; e3.kovetkezo = &e4;
    e4.elozo = &e3; e4.kovetkezo = &e5;
    e5.elozo = &e4; e5.kovetkezo = &e6;
    e6.elozo = &e5; e6.kovetkezo = &e7;
    e7.elozo = &e6; e7.kovetkezo = &e8;
    e8.elozo = &e7; e8.kovetkezo = &e9;
    e9.elozo = &e8; e9.kovetkezo = NULL;

    Elem* res = torol(&e1, 5);

    ASSERT_EQ(res, &e1);

    ASSERT_EQ(e1.kovetkezo, &e2);
    ASSERT_EQ(e1.elozo, nullptr);

    ASSERT_EQ(e2.elozo, &e1);
    ASSERT_EQ(e2.kovetkezo, &e3);

    ASSERT_EQ(e3.elozo, &e2);
    ASSERT_EQ(e3.kovetkezo, &e5);

    ASSERT_EQ(e5.elozo, &e3);
    ASSERT_EQ(e5.kovetkezo, &e6);

    ASSERT_EQ(e6.elozo, &e5);
    ASSERT_EQ(e6.kovetkezo, &e9);

    ASSERT_EQ(e9.elozo, &e6);
    ASSERT_EQ(e9.kovetkezo, nullptr);
}

TEST(torol, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Elem e1; e1.ertek = 3;
    Elem e2; e2.ertek = 0;
    Elem e3; e3.ertek = 2;
    Elem e4; e4.ertek = 70;

    e1.elozo = NULL; e1.kovetkezo = &e2;
    e2.elozo = &e1; e2.kovetkezo = &e3;
    e3.elozo = &e2; e3.kovetkezo = &e4;
    e4.elozo = &e3; e4.kovetkezo = NULL;

    Elem* res = torol(&e1, 70);

    ASSERT_EQ(res, &e1);

    ASSERT_EQ(e1.kovetkezo, &e2);
    ASSERT_EQ(e1.elozo, nullptr);

    ASSERT_EQ(e2.elozo, &e1);
    ASSERT_EQ(e2.kovetkezo, &e3);

    ASSERT_EQ(e3.elozo, &e2);
    ASSERT_EQ(e3.kovetkezo, nullptr);
}

TEST(torol, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Elem e1; e1.ertek = 3;
    Elem e2; e2.ertek = 0;
    Elem e3; e3.ertek = 2;
    Elem e4; e4.ertek = 70;

    e1.elozo = NULL; e1.kovetkezo = &e2;
    e2.elozo = &e1; e2.kovetkezo = &e3;
    e3.elozo = &e2; e3.kovetkezo = &e4;
    e4.elozo = &e3; e4.kovetkezo = NULL;

    Elem* res = torol(&e1, 3);

    ASSERT_EQ(res, &e2);

    ASSERT_EQ(e2.elozo, nullptr);
    ASSERT_EQ(e2.kovetkezo, &e3);

    ASSERT_EQ(e3.elozo, &e2);
    ASSERT_EQ(e3.kovetkezo, &e4);

    ASSERT_EQ(e4.elozo, &e3);
    ASSERT_EQ(e4.kovetkezo, nullptr);
}

TEST(torol, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Elem e1; e1.ertek = 3;
    Elem e2; e2.ertek = 3;
    Elem e3; e3.ertek = 3;
    Elem e4; e4.ertek = 3;

    e1.elozo = NULL; e1.kovetkezo = &e2;
    e2.elozo = &e1; e2.kovetkezo = &e3;
    e3.elozo = &e2; e3.kovetkezo = &e4;
    e4.elozo = &e3; e4.kovetkezo = NULL;

    Elem* res = torol(&e1, 3);

    ASSERT_EQ(res, nullptr);
}

TEST(torol, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Elem e1; e1.ertek = 11;

    e1.elozo = NULL; e1.kovetkezo = NULL;

    Elem* res = torol(&e1, 11);

    ASSERT_EQ(res, nullptr);
}