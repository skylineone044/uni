#include <stdlib.h>

typedef  double (*func_p)(double z);

func_p legnagyobb(double(*tomb[])(double x), double szam) {
    if (tomb[0] == NULL) {
        return NULL;
    }
    double legnagyobb = tomb[0](szam);
    int legnagyobb_id = 0;
    for (int i = 1; tomb[i] != NULL; i++) {
        if (tomb[i](szam) > legnagyobb) {
            legnagyobb = tomb[i](szam);
            legnagyobb_id = i;
        }
    }
    return tomb[legnagyobb_id];
}