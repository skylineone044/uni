#include <gtest/gtest.h>

#define main main_0
#include "../src/legnagyobb.c"
#undef main

double fgv1(double a) { return a; }
double fgv2(double a) { return 3*a; }
double fgv3(double a) { return a*a; }
double fgv4(double a) { return a*a*a; }
double fgv5(double a) { return -a; }
double fgv6(double a) { return a - 10; }
double fgv7(double a) { return 10; }

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double(*fuggvenyek[])(double) = {&fgv1, &fgv2, NULL};
    double ertek = 7;
    double (*elvart)(double) = &fgv2;
    ASSERT_EQ(legnagyobb(fuggvenyek, ertek), elvart);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double(*fuggvenyek[])(double) = {&fgv1, &fgv2, NULL};
    double ertek = -2;
    double (*elvart)(double) = &fgv1;
    ASSERT_EQ(legnagyobb(fuggvenyek, ertek), elvart);
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double(*fuggvenyek[])(double) = {&fgv1, &fgv2, &fgv3, &fgv4, NULL};
    double ertek = 8;
    double (*elvart)(double) = &fgv4;
    ASSERT_EQ(legnagyobb(fuggvenyek, ertek), elvart);
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double(*fuggvenyek[])(double) = {&fgv1, &fgv2, &fgv3, &fgv4, NULL};
    double ertek = 1;
    double (*elvart)(double) = &fgv2;
    ASSERT_EQ(legnagyobb(fuggvenyek, ertek), elvart);
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double(*fuggvenyek[])(double) = {&fgv1, &fgv5, &fgv7, NULL};
    double ertek = 0;
    double (*elvart)(double) = &fgv7;
    ASSERT_EQ(legnagyobb(fuggvenyek, ertek), elvart);
}

TEST(Teszt, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double(*fuggvenyek[])(double) = {&fgv1, &fgv5, &fgv7, NULL};
    double ertek = 12;
    double (*elvart)(double) = &fgv1;
    ASSERT_EQ(legnagyobb(fuggvenyek, ertek), elvart);
}

TEST(Teszt, 07) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double(*fuggvenyek[])(double) = {&fgv1, &fgv5, &fgv7, NULL};
    double ertek = 7;
    double (*elvart)(double) = &fgv7;
    ASSERT_EQ(legnagyobb(fuggvenyek, ertek), elvart);
}

TEST(Teszt, 08) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double(*fuggvenyek[])(double) = {&fgv6, NULL};
    double ertek = 7;
    double (*elvart)(double) = &fgv6;
    ASSERT_EQ(legnagyobb(fuggvenyek, ertek), elvart);
}

TEST(Teszt, 09) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double(*fuggvenyek[])(double) = {NULL};
    double ertek = 1300;
    double (*elvart)(double) = NULL;
    ASSERT_EQ(legnagyobb(fuggvenyek, ertek), elvart);
}

TEST(Teszt, 10) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double(*fuggvenyek[])(double) = {&fgv1, &fgv2, &fgv3, &fgv4, &fgv5, &fgv6, &fgv7, NULL};
    double ertek = 5;
    double (*elvart)(double) = &fgv4;
    ASSERT_EQ(legnagyobb(fuggvenyek, ertek), elvart);
}

TEST(Teszt, 11) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double(*fuggvenyek[])(double) = {&fgv1, &fgv2, &fgv3, &fgv4, &fgv5, &fgv6, &fgv7, NULL};
    double ertek = 1;
    double (*elvart)(double) = &fgv7;
    ASSERT_EQ(legnagyobb(fuggvenyek, ertek), elvart);
}

TEST(Teszt, 12) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double(*fuggvenyek[])(double) = {&fgv1, &fgv2, &fgv3, &fgv4, &fgv5, &fgv6, &fgv7, NULL};
    double ertek = -2;
    double (*elvart)(double) = &fgv7;
    ASSERT_EQ(legnagyobb(fuggvenyek, ertek), elvart);
}

TEST(Teszt, 13) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double(*fuggvenyek[])(double) = {&fgv1, &fgv2, &fgv3, &fgv4, &fgv5, &fgv6, &fgv7, NULL};
    double ertek = -4;
    double (*elvart)(double) = &fgv3;
    ASSERT_EQ(legnagyobb(fuggvenyek, ertek), elvart);
}