#include <gtest/gtest.h>

#define main main_0
#include "../src/macska.c"
#undef main

#include "../../tools.cpp"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int db = 5;

    char param0[] = "./macska";
    char param1[] = "macska";
    char param2[] = "lo";
    char param3[] = "macska";
    char param4[] = "macska";

    char* szovegek[] = {param0, param1, param2, param3, param4, NULL};
    char str[1000];
    IO("", main_0(db, szovegek), str)

    ASSERT_STREQ(str, "A macskak darabszama: 3\n");
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int db = 3;

    char param0[] = "./macska";
    char param1[] = "macska";
    char param2[] = "macska";

    char* szovegek[] = {param0, param1, param2, NULL};
    char str[1000];
    IO("", main_0(db, szovegek), str)

    ASSERT_STREQ(str, "A macskak darabszama: 2\n");
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int db = 5;

    char param0[] = "./macska";
    char param1[] = "macskak";
    char param2[] = "lo";
    char param3[] = "amacska";
    char param4[] = "MACSKA";

    char* szovegek[] = {param0, param1, param2, param3, param4, NULL};
    char str[1000];
    IO("", main_0(db, szovegek), str)

    ASSERT_STREQ(str, "A macskak darabszama: 0\n");
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int db = 1;

    char param0[] = "./macska";

    char* szovegek[] = {param0, NULL};
    char str[1000];
    IO("", main_0(db, szovegek), str)

    ASSERT_STREQ(str, "A macskak darabszama: 0\n");
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int db = 1;

    char param0[] = "./macska";

    char* szovegek[] = {param0, NULL};
    char str[1000];
    IO("", int res = main_0(db, szovegek), str)

    ASSERT_EQ(res, 0);
}