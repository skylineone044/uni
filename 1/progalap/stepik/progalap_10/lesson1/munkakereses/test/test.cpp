#include <gtest/gtest.h>

#define main main_0
#include "../src/munkakereses.c"
#undef main

char v1[] = "virag";
char v2[] = "leander";
char v3[] = "rozsa";
char v4[] = "leander";
char v5[] = "medvevirag";
char v6[] = "tulipan";
char v7[] = "tulielf";
char v8[] = "leander";

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char n1[] = "Cica kft";
    char* vv1[] = {v1, v2, v3, v4, NULL};
    Munkahely m1; m1.viragok = vv1; m1.nev = n1; // 4 | 2

    char n2[] = "Kutya bt";
    char* vv2[] = {v1, v3, v5, v6, v7, v8, NULL};
    Munkahely m2; m2.viragok = vv2; m2.nev = n2; // 6 | 1

    char n3[] = "masik";
    char* vv3[] = {v1, v4, v8, NULL};
    Munkahely m3; m3.viragok = vv3; m3.nev = n3; // 3 | 3

    char n4[] = "kovetkezo";
    char* vv4[] = {v1, v3, v5, v8, NULL};
    Munkahely m4; m4.viragok = vv4; m4.nev = n4; // 4 | 1

    char n5[] = "utolso";
    char* vv5[] = {v2, v4, v5, NULL};
    Munkahely m5; m5.viragok = vv5; m5.nev = n5; // 3 | 2

    char n6[] = "last";
    char* vv6[] = {v8, NULL};
    Munkahely m6; m6.viragok = vv6; m6.nev = n6; // 1 | 1

    char n7[] = "nemtudom";
    char* vv7[] = {NULL};
    Munkahely m7; m7.viragok = vv7; m7.nev = n7; // 0 | 0

    char n8[] = "kesz";
    Munkahely m8; m8.viragok = NULL; m8.nev = n8; // 0 | 0

    Munkahely* res;

    Munkahely* mm1[] = {&m1, &m2, &m3, NULL};
    res = munkavalasztas(mm1);
    ASSERT_EQ(res, &m2);

    Munkahely* mm2[] = {&m2, &m5, &m6, &m8, NULL};
    res = munkavalasztas(mm2);
    ASSERT_EQ(res, &m2);

    Munkahely* mm3[] = {&m3, &m4, &m8, NULL};
    res = munkavalasztas(mm3);
    ASSERT_EQ(res, &m4);

    Munkahely* mm4[] = {&m5, NULL};
    res = munkavalasztas(mm4);
    ASSERT_EQ(res, &m5);

    Munkahely* mm5[] = {&m8, NULL};
    res = munkavalasztas(mm5);
    ASSERT_EQ(res, nullptr);

    Munkahely* mm6[] = {&m8, &m7, NULL};
    res = munkavalasztas(mm6);
    ASSERT_EQ(res, nullptr);

    Munkahely* mm7[] = {&m3, &m5, NULL};
    res = munkavalasztas(mm7);
    ASSERT_EQ(res, &m3);

    Munkahely* mm8[] = {&m5, &m3, NULL};
    res = munkavalasztas(mm8);
    ASSERT_EQ(res, &m5);
}