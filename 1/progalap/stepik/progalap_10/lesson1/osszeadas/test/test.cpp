#include <gtest/gtest.h>

#define main main_0
#include "../src/osszeadas.c"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
#ifndef osszeadas
    FAIL();
#endif

    unsigned int a = 5;
    unsigned int b = 4;

    ASSERT_EQ(osszeadas(3, 5), 8);
    ASSERT_EQ(osszeadas(2+3, 4), 9);
    ASSERT_EQ(3 * osszeadas(2, 7), 27);
    ASSERT_EQ(osszeadas(3+2, 3*3)*2, 28);
    ASSERT_EQ(osszeadas(a & b, 2), 6);

}