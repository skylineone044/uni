#include <stdio.h>
#include <string.h>
void osszefuz(const int szamok[], char eredmeny[]) {
    if (szamok[0] != -1) {
        sprintf(eredmeny, "%d", szamok[0]);
    } else {
        eredmeny[0] = '\0';
        return;
    }
    for (int i = 1; szamok[i] != -1; i++) {
        sprintf(eredmeny + strlen(eredmeny), ";%d", szamok[i]);
    }
}