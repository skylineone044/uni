#include <gtest/gtest.h>

#define main main_0
#include "../src/osszefuzes.c"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int szamok[] = {3, 6, 3, 2, 3, 30, -1};
    char str[200];
    osszefuz(szamok, str);
    ASSERT_STREQ(str, "3;6;3;2;3;30");
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int szamok[] = {4, 2, -1};
    char str[200];
    osszefuz(szamok, str);
    ASSERT_STREQ(str, "4;2");
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int szamok[] = {5, -1};
    char str[200];
    osszefuz(szamok, str);
    ASSERT_STREQ(str, "5");
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int szamok[] = {-1};
    char str[200];
    osszefuz(szamok, str);
    ASSERT_STREQ(str, "");
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int szamok[] = {-4, 6, 50, 212, 0, 2, -1, 4};
    char str[200];
    osszefuz(szamok, str);
    ASSERT_STREQ(str, "-4;6;50;212;0;2");
}