#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
    int osszeg = 0;
    for (int i = 1; i < argc; ++i) {
        osszeg += atoi(argv[i]);
    }
    printf("%d", osszeg);
    return 0;
}