#include <gtest/gtest.h>

#define main main_0
#include "../src/osszeg.c"
#undef main

#include "../../tools.cpp"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char param0[] = "./osszeg";
    char param1[] = "14";
    char param2[] = "30";
    char param3[] = "2";

    char* params[] = {param0, param1, param2, param3, NULL};

    char res[100];
    IO("", main_0(4, params), res);

    ASSERT_STREQ(res, "46");
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char param0[] = "./osszeg";
    char param1[] = "40004";
    char param2[] = "42";
    char param3[] = "0";
    char param4[] = "2";

    char* params[] = {param0, param1, param2, param3, param4, NULL};

    char res[100];
    IO("", main_0(5, params), res);

    ASSERT_STREQ(res, "40048");
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char param0[] = "./osszeg";
    char param1[] = "310";
    char param2[] = "2";
    char param3[] = "-5";
    char param4[] = "7";

    char* params[] = {param0, param1, param2, param3, param4, NULL};

    char res[100];
    IO("", main_0(5, params), res);

    ASSERT_STREQ(res, "314");
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char param0[] = "./osszeg";
    char param1[] = "-2";

    char* params[] = {param0, param1, NULL};

    char res[100];
    IO("", main_0(2, params), res);

    ASSERT_STREQ(res, "-2");
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char param0[] = "./osszeg";

    char* params[] = {param0, NULL};

    char res[100];
    IO("", main_0(1, params), res);

    ASSERT_STREQ(res, "0");
}

TEST(Teszt, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char param0[] = "./osszeg";
    char param1[] = "44";

    char* params[] = {param0, param1, NULL};

    char res[100];
    IO("", int r = main_0(2, params), res);

    ASSERT_EQ(r, 0);
}