#include <gtest/gtest.h>

#define main main_0
#include "../src/printf.c"
#undef main

#include "../../tools.cpp"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", int res = pprintf("macska"), str)
    ASSERT_STREQ(str, "macska");
    ASSERT_EQ(res, 6);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", int res = pprintf("egy ket lo"), str)
    ASSERT_STREQ(str, "egy ket lo");
    ASSERT_EQ(res, 10);
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", int res = pprintf("%d", 5), str)
    ASSERT_STREQ(str, "5");
    ASSERT_EQ(res, 1);
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", int res = pprintf("%d\n", 54), str)
    ASSERT_STREQ(str, "54\n");
    ASSERT_EQ(res, 3);
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", int res = pprintf("%lf\n", 2.2), str)
    ASSERT_STREQ(str, "2.200000\n");
    ASSERT_EQ(res, 9);
}

TEST(Teszt, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", int res = pprintf("Ez a betum: %c", 'a'), str)
    ASSERT_STREQ(str, "Ez a betum: a");
    ASSERT_EQ(res, 13);
}

TEST(Teszt, 07) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", int res = pprintf("ok: %x", 16), str)
    ASSERT_STREQ(str, "ok: 10");
    ASSERT_EQ(res, 6);
}

TEST(Teszt, 08) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", int res = pprintf("ok: %x", 15), str)
    ASSERT_STREQ(str, "ok: f");
    ASSERT_EQ(res, 5);
}

TEST(Teszt, 09) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", int res = pprintf("ok: %X", 15), str)
    ASSERT_STREQ(str, "ok: F");
    ASSERT_EQ(res, 5);
}

TEST(Teszt, 10) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", int res = pprintf("nem jo: %i", 404), str)
    ASSERT_STREQ(str, "nem jo: 404");
    ASSERT_EQ(res, 11);
}

TEST(Teszt, 11) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", int res = pprintf("nem jo: %hd", 404), str)
    ASSERT_STREQ(str, "nem jo: 404");
    ASSERT_EQ(res, 11);
}

TEST(Teszt, 12) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", int res = pprintf("nem jo: %ld", 404), str)
    ASSERT_STREQ(str, "nem jo: 404");
    ASSERT_EQ(res, 11);
}

TEST(Teszt, 13) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", int res = pprintf("nem jo: %u", -1), str)
    ASSERT_STREQ(str, "nem jo: 4294967295");
    ASSERT_EQ(res, 18);
}

TEST(Teszt, 14) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", int res = pprintf("%o lesz", 41), str)
    ASSERT_STREQ(str, "51 lesz");
    ASSERT_EQ(res, 7);
}

TEST(Teszt, 15) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", int res = pprintf("%ho", 41), str)
    ASSERT_STREQ(str, "51");
    ASSERT_EQ(res, 2);
}

TEST(Teszt, 16) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", int res = pprintf("%hu", 41), str)
    ASSERT_STREQ(str, "41");
    ASSERT_EQ(res, 2);
}

TEST(Teszt, 17) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", int res = pprintf("melyik a jo? %s?", "cica"), str)
    ASSERT_STREQ(str, "melyik a jo? cica?");
    ASSERT_EQ(res, 18);
}

TEST(Teszt, 18) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    int k;
    IO("", int res = pprintf("melyik a jo? %nez", &k), str)
    ASSERT_STREQ(str, "melyik a jo? ez");
    ASSERT_EQ(res, 15);
    ASSERT_EQ(k, 13);
}

TEST(Teszt, 19) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", int res = pprintf("%d+%d=%d", 5, 3, 8), str)
    ASSERT_STREQ(str, "5+3=8");
    ASSERT_EQ(res, 5);
}

TEST(Teszt, 20) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", int res = pprintf("%s darab %s", "ot", "paradicsom"), str)
    ASSERT_STREQ(str, "ot darab paradicsom");
    ASSERT_EQ(res, 19);
}

TEST(Teszt, 21) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", int res = pprintf("%d macska talalhato az %s.", 3, "udvar kozepen"), str)
    ASSERT_STREQ(str, "3 macska talalhato az udvar kozepen.");
    ASSERT_EQ(res, 36);
}

TEST(Teszt, 22) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", int res = pprintf("%c%c%c%%azeredmeny\taa", '3', '2', '5'), str)
    ASSERT_STREQ(str, "325%azeredmeny\taa");
    ASSERT_EQ(res, 17);
}

TEST(Teszt, 23) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", int res = pprintf(""), str)
    ASSERT_STREQ(str, "");
    ASSERT_EQ(res, 0);
}

TEST(Teszt, 24) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    char nev[] = "cirmi"; Macska m; m.nev = nev; m.eletkor = 5;
    IO("", int res = pprintf("%m", m), str)
    ASSERT_STREQ(str, "{cirmi;5}");
    ASSERT_EQ(res, 9);
}

TEST(Teszt, 25) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    char nev[] = "garfield"; Macska m; m.nev = nev; m.eletkor = 22;
    IO("", int res = pprintf("%d macska van: %m, %m", 8, m, m), str)
    ASSERT_STREQ(str, "8 macska van: {garfield;22}, {garfield;22}");
    ASSERT_EQ(res, 42);
}

TEST(Teszt, 26) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    char nev[] = "garfield"; Macska m; m.nev = nev; m.eletkor = 22;
    IO("", int res = pprintf("%.2f%m", 5.5, m), str)
    ASSERT_STREQ(str, "5.50{garfield;22}");
    ASSERT_EQ(res, 17);
}

TEST(Teszt, 27) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    char nev[] = "garfield"; Macska m; m.nev = nev; m.eletkor = 22;
    IO("", int res = pprintf("%03d%m", 4, m), str)
    ASSERT_STREQ(str, "004{garfield;22}");
    ASSERT_EQ(res, 16);
}

TEST(Teszt, 28) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    char nev[] = "ggg"; Macska m; m.nev = nev; m.eletkor = 3;
    IO("", int res = pprintf("%08.2f%m", 4.152, m), str)
    ASSERT_STREQ(str, "00004.15{ggg;3}");
    ASSERT_EQ(res, 15);
}

TEST(Teszt, 29) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    char nev[] = "ggg"; Macska m; m.nev = nev; m.eletkor = 3;
    char nev2[] = "an"; Macska m2; m2.nev = nev2; m2.eletkor = 12;
    char nev3[] = "en"; Macska m3; m3.nev = nev3; m3.eletkor = 61;
    IO("", int res = pprintf("%%%m%m%m%%%%", m, m2, m3), str)
    ASSERT_STREQ(str, "%{ggg;3}{an;12}{en;61}%%");
    ASSERT_EQ(res, 24);
}

TEST(Teszt, 30) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    char nev[] = "ggg"; Macska m; m.nev = nev; m.eletkor = 3;
    char nev2[] = "an"; Macska m2; m2.nev = nev2; m2.eletkor = 12;
    char nev3[] = "en"; Macska m3; m3.nev = nev3; m3.eletkor = 61;
    IO("", int res = pprintf("%%%m%m%+d%m%%%%", m, m2, 5, m3), str)
    ASSERT_STREQ(str, "%{ggg;3}{an;12}+5{en;61}%%");
    ASSERT_EQ(res, 26);
}