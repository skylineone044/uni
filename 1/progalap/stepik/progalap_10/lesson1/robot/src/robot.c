#include <stdio.h>
#include <string.h>
#include <stdlib.h>

enum Irany{BALRA, JOBBRA, FEL, LE};

typedef struct Robot Robot;

struct Robot{
    int x;
    int y;
    void (*lepes)(Robot *robot, enum Irany irany);
    char* (*toString)(Robot *robot);
};

void lep(Robot *robot, enum Irany irany) {
    switch (irany) {
        case BALRA:
            (robot->x)--;
            break;
        case JOBBRA:
            (robot->x)++;
            break;
        case FEL:
            (robot->y)++;
            break;
        case LE:
            (robot->y)--;
            break;
        default:
            break;
    }
}

char *szovegesit(Robot *robot) {
    char* szoveg = (char*)malloc(sizeof(char) * 100);
    for (int i = 0; i < 100; ++i) {
        szoveg[i] = '\0';
    }
    sprintf(szoveg, "(%d;%d)", robot->x, robot->y);
    return szoveg;
}
Robot* robototLetrehoz() {
   Robot* robot = (Robot*)malloc(sizeof(Robot));
   robot->x = 0;
   robot->y = 0;
   robot->lepes = lep;
   robot->toString = szovegesit;
    return robot;
}