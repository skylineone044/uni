#include <gtest/gtest.h>

#define main main_0
#include "../src/robot.c"
#undef main

TEST(lep, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Robot r;

    r.x = 2; r.y = 3;
    lep(&r, BALRA);
    ASSERT_EQ(r.x, 1);
    ASSERT_EQ(r.y, 3);

    lep(&r, FEL);
    ASSERT_EQ(r.x, 1);
    ASSERT_EQ(r.y, 4);

    lep(&r, JOBBRA);
    ASSERT_EQ(r.x, 2);
    ASSERT_EQ(r.y, 4);

    lep(&r, FEL);
    ASSERT_EQ(r.x, 2);
    ASSERT_EQ(r.y, 5);

    lep(&r, LE);
    ASSERT_EQ(r.x, 2);
    ASSERT_EQ(r.y, 4);

    lep(&r, BALRA);
    lep(&r, BALRA);
    lep(&r, BALRA);
    lep(&r, BALRA);
    ASSERT_EQ(r.x, -2);
    ASSERT_EQ(r.y, 4);
}

TEST(szovegesit, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Robot r;

    r.x = 3; r.y = 5;
    char* res = szovegesit(&r);
    ASSERT_STREQ(res, "(3;5)");
    free(res);

    r.x = -3; r.y = 3;
    res = szovegesit(&r);
    ASSERT_STREQ(res, "(-3;3)");
    free(res);

    r.x = 130; r.y = -42112;
    res = szovegesit(&r);
    ASSERT_STREQ(res, "(130;-42112)");
    free(res);
}

TEST(robototLetrehoz, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Robot* r1 = robototLetrehoz();
    ASSERT_EQ(r1->x, 0);
    ASSERT_EQ(r1->y, 0);

    char* res = r1->toString(r1);
    ASSERT_STREQ(res, "(0;0)");
    free(res);

    r1->lepes(r1, JOBBRA);
    r1->lepes(r1, LE);
    res = r1->toString(r1);
    ASSERT_STREQ(res, "(1;-1)");
    free(res);
}