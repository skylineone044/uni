#include <stdarg.h>
#include <limits.h>

double absz(double x) {
    if (x < 0) {
        return -x;
    } else {
        return x;
    }
}
int neralyequal(double a, double b, double error) {
    if (absz(a-b) <= error) {
        return 1;
    } else {
        return 0;
    }
}

int sorozat(int tipus, ...) {
    va_list arguments;
    if (!(0 <= tipus && tipus <= 1)) {
        return -2;
    }
    int size = 100;
    va_start(arguments, size);
    double double_array[size];
    if (tipus == 0) {
        for (int i = 0;i < size; ++i) {
            double_array[i] = va_arg(arguments, int);
            if (double_array[i] == INT_MAX) {
                break;
            }
        }
    } else if (tipus == 1) {
        for (int i = 0;i < size; ++i) {
            double_array[i] = va_arg(arguments, double);
            if (double_array[i] == INT_MAX) {
                break;
            }
        }
    }
    double q = double_array[1] / double_array[0];
    double d = double_array[1] - double_array[0];
    int len = 0;
    int szmatani = 1;
    int mertani = 1;
    for (; double_array[len] != INT_MAX; len++);
    len--;
    for (int i = len; 0 < i; i--) {
        if (!neralyequal(double_array[i] / double_array[i-1], q, 0.0001)) {
            mertani = 0;
        }
        if (!neralyequal(double_array[i] - double_array[i-1],  d, 0.0001)) {
            szmatani = 0;
        }
    }
    va_end(arguments);
    if (mertani && szmatani) {return 2;}
    else if (mertani && !szmatani) {return 1;}
    else if (!mertani && szmatani) {return 0;}
    else {
        return -1;
    }

}