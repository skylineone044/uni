#include <gtest/gtest.h>
#include <climits>

#define main main_0
#include "../src/sorozat.c"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(sorozat(0, 5, 10, 20, 40, 80, INT_MAX), 1);
    ASSERT_EQ(sorozat(0, 20, 10, 5, INT_MAX), 1);
    ASSERT_EQ(sorozat(0, 32, 16, 8, 4, 2, 1, INT_MAX), 1);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(sorozat(0, 2, 4, 6, 8, 10, INT_MAX), 0);
    ASSERT_EQ(sorozat(0, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, INT_MAX), 0);
    ASSERT_EQ(sorozat(0, 5, 3, 1, -1, -3, -5, -7, INT_MAX), 0);
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(sorozat(0, 2, 4, INT_MAX), 2);
    ASSERT_EQ(sorozat(0, 5, 10, INT_MAX), 2);
    ASSERT_EQ(sorozat(0, 3, INT_MAX), 2);
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(sorozat(1, 0.3, 0.9, 2.7, 8.1, (double)INT_MAX), 1);
    ASSERT_EQ(sorozat(1, 42.0, 16.8, 6.72, 2.688, (double)INT_MAX), 1);
    ASSERT_EQ(sorozat(1, 2.0, 4.0, 8.0, 16.0, (double)INT_MAX), 1);
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(sorozat(1, 2.0, 4.0, 6.0, 8.0, 10.0, (double)INT_MAX), 0);
    ASSERT_EQ(sorozat(1, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.6, 2.8, 3.0, 3.2, (double)INT_MAX), 0);
    ASSERT_EQ(sorozat(1, 1.5, 0.7, -0.1, -0.9, (double)INT_MAX), 0);
}

TEST(Teszt, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(sorozat(1, 3.3, 5.2, (double)INT_MAX), 2);
    ASSERT_EQ(sorozat(1, 5.0, 10.0, (double)INT_MAX), 2);
    ASSERT_EQ(sorozat(1, 1.512, (double)INT_MAX), 2);
}

TEST(Teszt, 07) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(sorozat(3, 5, 2, 6, INT_MAX), -2);
    ASSERT_EQ(sorozat(2, 5, 3, 2, INT_MAX), -2);
    ASSERT_EQ(sorozat(-1, 4.4, 2.1, 5.2, "cica", INT_MAX), -2);
}

TEST(Teszt, 08) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(sorozat(0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 65, 70, 75, 80, INT_MAX), -1);
    ASSERT_EQ(sorozat(0, 3, 6, 12, 24, 48, 48, 96, INT_MAX), -1);
    ASSERT_EQ(sorozat(1, 2.2, 4.2, 6.2, 8.2, 10.2, 14.2, (double)INT_MAX), -1);
    ASSERT_EQ(sorozat(1, 4.2, 8.4, 16.8, 33.61, (double)INT_MAX), -1);
}