#include <gtest/gtest.h>

#define main main_0
#include "../src/tomb.c"
#undef main

#include "../../tools.cpp"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
#ifndef print_array
    FAIL();
#endif

    int szamok[] = {3, 5, 2};
    char str[100];
    IO("", print_array(szamok, 3), str)
    ASSERT_STREQ(str, "{3, 5, 2}");
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int szamok[] = {3, 5, 2, 5, 5, 0, 1, -1, 4, 2};
    char str[100];
    IO("", print_array(szamok, 10), str)
    ASSERT_STREQ(str, "{3, 5, 2, 5, 5, 0, 1, -1, 4, 2}");
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int szamok[] = {-3, -5, 5};
    char str[100];
    IO("", print_array(szamok, 3), str)
    ASSERT_STREQ(str, "{-3, -5, 5}");
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int szamok[] = {3};
    char str[100];
    IO("", print_array(szamok, 1), str)
    ASSERT_STREQ(str, "{3}");
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int szamok[] = {500};
    char str[100];
    IO("", print_array(szamok, 0), str)
    ASSERT_STREQ(str, "{}");
}

TEST(Teszt, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int szamok[] = {1, 1, 1, 1, 1, 1, 1, 1};
    char str[100];
    IO("", print_array(szamok, 5), str)
    ASSERT_STREQ(str, "{1, 1, 1, 1, 1}");
}

TEST(Teszt, 07) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int i = 2;
    int szamok[] = {1, 1, 1, 1, 1, 1, 1, 1};
    char str[100];
    IO("", print_array(szamok, i), str)
    ASSERT_STREQ(str, "{1, 1}");
}