#ifndef viragok
#define viragok

typedef struct {
    char szin;
    int meret;
} Virag;

void novekszik(Virag* virag, int szam);
void elpusztult(Virag* virag);
void kiviragzik(Virag* virag, char szin);

#endif
