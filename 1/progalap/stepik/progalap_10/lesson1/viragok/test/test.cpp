#include <gtest/gtest.h>

#define main main_0
#include "../src/viragok.h"
#include "../src/viragok.h"
#include "../src/viragok.h"
#include "../src/viragok.h"
#undef main

void novekszik(Virag* v, int i) {
    v->meret += i;
}

void elpusztul(Virag* v) {
    v->szin = 's';
    v->meret = 0;
}

void kiviragzik(Virag* v, char c) {
    v->szin = c;
}

TEST(Struktura, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Virag v;
    v.szin = 'p';
    v.meret = 614;

    ASSERT_EQ(v.szin, 'p');
    ASSERT_EQ(v.meret, 614);
}

TEST(novekszik, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Virag v = {'p', 2};
    novekszik(&v, 4);
    ASSERT_EQ(v.szin, 'p');
    ASSERT_EQ(v.meret, 6);
}

TEST(elpusztul, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Virag v = {'k', 5};
    elpusztul(&v);
    ASSERT_EQ(v.szin, 's');
    ASSERT_EQ(v.meret, 0);
}

TEST(kiviragzik, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Virag v = {'k', 3};
    kiviragzik(&v, 'r');
    ASSERT_EQ(v.meret, 3);
    ASSERT_EQ(v.szin, 'r');
}