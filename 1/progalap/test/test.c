#include <stdio.h>

void mybubblesort(int list[], int len) {
    // ez egy megfixalt verzioja a regi bubble sort funkcionak, atalakitva hogy
    // menjen intekkel
    for (int i = 0; i < len; i++) {
        for (int j = 0; j < len - 1; ++j) {
            int value = list[j];
            int value_after = list[j + 1];

            if (value <= value_after) {
                // right order, keep them
            } else {
                // wrong order, switch them
                int placeholder = list[j];
                list[j] = list[j + 1];
                list[j + 1] = placeholder;

            }
        }
    }
}
int egyszam(int tomb[], int meret) {
    mybubblesort(tomb, meret);

    int eddigi_legkisebb_egyeduli = -1;

    if (tomb[0] < tomb[1]) {
        return  tomb[0];
    }

    for (int i = meret-2; i > 0; --i) {
        if (tomb[i-1] < tomb[i] && tomb[i] < tomb[i+1]) {
            eddigi_legkisebb_egyeduli = tomb[i];
        }
    }
    if (eddigi_legkisebb_egyeduli == -1 && tomb[meret-1] != tomb[meret-2]) {
        return tomb[meret-1];
    }
    return eddigi_legkisebb_egyeduli;
}

int main() {
    int tomb1[] = {5, 4, 3, 2, 1};
    printf("%d\n", egyszam(tomb1, 5));
    int tomb2[] = {1, 2, 3, 4, 5};
    printf("%d\n", egyszam(tomb2, 5));
    int tomb3[] = {1, 2, 3, 4, 3, 2, 1};
    printf("%d\n", egyszam(tomb3, 7));
}
