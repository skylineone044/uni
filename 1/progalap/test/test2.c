#include <stdio.h>
// revisit 3 6 17 18 20 25
#define m(A) ((A) * (A))

int main() {
    /* int **p; */
    /* **p ***p; */
    /* *p + *p; */
    /* *p **p; */
    /* *p + p; */
    /* p *p; */

    int x = 2;
    int y = 3;
    printf("%d\n", 2*m(x + y));
    signed char cc = -4;
    printf("%x\n", cc);
    return 0;
}
