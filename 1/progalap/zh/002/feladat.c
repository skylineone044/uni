/***********************************************************************
* A PROGRAMBAN NEM SZEREPELHETNEK AZ ALÁBBI SOROK:
* #include <string.h>
* #include <math.h>
***********************************************************************/
#include <stdio.h>
#include <stdlib.h>

/***********************************************************************
************************************************************************
**		ETTŐL A PONTTÓL DOLGOZHATSZ A FELADATOKON
************************************************************************
***********************************************************************/

/*
1. feladat (2 pont)

A feladat egy sorrend fuggveny megirasa, amely ket egesz erteket
kap parameterul, a visszateresi erteke pedig 1 vagy -1, illetve
0 attol fuggoen, hogy a ket parameter kozul az elso (a) vagy
a masodik (b) a nagyobb, illetve egyenloek.

Kodold le C nyelven a fuggvenyt! A fuggveny fejlecen ne valtoztass!
A fuggveny inputjai a parameterek, outputja a visszateresi ertek.
A fuggveny nem vegez IO muveleteket!
*/

int sorrend(int a, int b) {
    if (a > b) {return 1;}
    else if (a < b) {return -1;}
    else {return 0;}
}

/*
2. feladat (3 pont)

A feladat meghatarozni ket egesz szam kozotti zart intervallumba eso
negyzetszamok osszeget. A fuggveny ket parametere sorban az
intervallum also (a) es felso (b) vegpontja.
Visszateresi erteke az intervallumba eso negyzetszamok osszege. A
vegpontok meg az intervallum reszei.

A feladat a math.h hasznalata nelkul megoldhato egy ciklussal,
amely 0-tol indul, es addig tart amig a ciklusvaltozo negyzete nagyobb
nem lesz b-nel. A ciklusmagban ellenorizzuk, hogy a ciklusvaltozo
negyzete nagyobb-egyenlo-e mint a, es ha igen, akkor noveljuk a
negyzetszamok osszeget tarolo valtozo erteket a ciklusvaltozo negyzetevel.
A fuggveny ezzel az eredetileg 0-ra inicializalt valtozoval ter vissza.
(Nulla darab ertek osszege 0.)

Kodold le C nyelven a fuggvenyt! A fuggveny fejlecen ne valtoztass!
A fuggveny inputjai a parameterek, outputja a visszateresi ertek.
A fuggveny nem vegez IO muveleteket!
*/

int negyzetosszeg(int a, int b) {
    int osszeg = 0;
    int i = 0;
    for (i; i <= b; i++) {
        int negyezetszam = i*i;
        if (negyezetszam >= a && negyezetszam <= b) {
            osszeg += negyezetszam;
        }
    }
    return osszeg;
}
/***********************************************************************
************************************************************************
**
**	EZEN A PONTON TÚL NE VÁLTOZTASS SEMMIT SEM A FÁJLON!
**
************************************************************************
***********************************************************************/

void call_1()
{
    int a, b, eredmeny;
    if(fscanf(stdin, "%d %d", &a, &b)!=2) {
        fprintf(stderr, "HIBA: Nem olvasható adat!\n");
        return;
    }
    eredmeny=sorrend(a, b);
    fprintf(stdout, "%d\n", eredmeny);
}
void test_1()
{
    int eredmeny, i;
    struct {int a; int b; int eredmeny;} testlist[2] = {{-3, 1, -1}, {3, 1, 1}};
    for (i = 0; i < 2; ++i) {
        eredmeny = sorrend(testlist[i].a, testlist[i].b);
        if (eredmeny != testlist[i].eredmeny) {
            fprintf(stderr, "HIBA: sorrend(%d, %d)\n"
                            "\telvárt eredmény: %d\n"
                            "\tkapott eredmény: %d\n",
                testlist[i].a, testlist[i].b, testlist[i].eredmeny, eredmeny);
        }
    }
}

void call_2()
{
	int a, b, eredmeny;
	if(fscanf(stdin, "%d %d", &a, &b)!=2) {
		fprintf(stderr, "HIBA: Nem olvasható adat!\n");
		return;
	}
	eredmeny=negyzetosszeg(a, b);
	fprintf(stdout, "%d\n", eredmeny);
}
void test_2()
{
	int eredmeny, i;
	struct {int a; int b; int eredmeny;} testlist[2] = {{100, 400, 2585}, {5, 20, 25}};
	for (i = 0; i < 2; ++i) {
		eredmeny = negyzetosszeg(testlist[i].a, testlist[i].b);
		if (eredmeny != testlist[i].eredmeny) {
			fprintf(stderr, "HIBA: negyzetosszeg(%d, %d)\n"
							"\telvárt eredmény: %d\n"
							"\tkapott eredmény: %d\n",
				testlist[i].a, testlist[i].b, testlist[i].eredmeny, eredmeny);
		}
	}
}

typedef void (*call_function)();

call_function call_table[] = {
	call_1,
	call_2,
	NULL
};


call_function test_table[] = {
	test_1,
	test_2,
	NULL
};

static int __arg_check_helper(const char * exp, const char * arg) {
	while (*exp && *arg && *exp == *arg) {
		++exp;
		++arg;
	}
	return *exp == *arg;
}

int main(int argc, char *argv[])
{
	int feladat, calltabsize;
	if ((argc > 1) && !(__arg_check_helper("-t", argv[1]) && __arg_check_helper("--test", argv[1]))) {
		if (argc > 2) {
			feladat = atoi(argv[2]);
			for (calltabsize=0; test_table[calltabsize]; calltabsize++);
			if (feladat <= 0 || calltabsize < feladat) {
				fprintf(stderr, "HIBA: rossz feladat sorszám: %d!\n", feladat);
				return 1;
			}
			(*test_table[feladat-1])();
		} else {
			for (feladat=0; test_table[feladat]; ++feladat) {
				(*test_table[feladat])();
			}
		}
		return 0;
	}
	if (!freopen("be.txt", "r", stdin)) {
		fprintf(stderr, "HIBA: Hiányzik a `be.txt'!\n");
		return 1;
	}
	if (!freopen("ki.txt", "w", stdout)) {
		fprintf(stderr, "HIBA: A `ki.txt' nem írható!\n");
		return 1;
	}
	for (calltabsize=0; call_table[calltabsize]; calltabsize++);
	if (fscanf(stdin, "%d%*[^\n]", &feladat)!=1) {
		fprintf(stderr, "HIBA: Nem olvasható a feladat sorszám!\n");
		return 1;
	}
	fgetc(stdin);
	if (0<feladat && feladat<=calltabsize) {
		(*call_table[feladat-1])();
	} else {
		fprintf(stderr, "HIBA: Rossz feladat sorszám: %d!\n", feladat);
		return 1;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
