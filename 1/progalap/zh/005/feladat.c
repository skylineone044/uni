/***********************************************************************
 * A PROGRAMBAN NEM SZEREPELHETNEK AZ ALÁBBI SOROK:
 * #include <string.h>
 * #include <math.h>
 ***********************************************************************/
#include <stdio.h>
#include <stdlib.h>

/***********************************************************************
************************************************************************
**		ETTŐL A PONTTÓL DOLGOZHATSZ A FELADATOKON
************************************************************************
***********************************************************************/

/*
1. feladat (5 pont)

Irj egy intervallum nevu strukturat, ami az
a es b nevu int tipusu mezoiben
egy intervallum also illetve felso vegpontjat tarolja.
Irj egy osszead nevu fuggvenyt, ami ket ilyen
intervallumot kap parameterul, es es a ket intervallum osszegevel
ter vissza.
Az [a,b] es [c,d] intervallumok osszege: [a+c,b+d].
Mindket parameter intervallumra igaz, hogy 0 <= a <= b.
*/
/****************************************************
 * A kovetkezo sor programba illesztese eseten      *
 * (// torlese a #define elol) a feladat nem okoz   *
 * forditasi hibat, de pontot sem fog erni.         *
 ****************************************************/
//#define KIHAGY_1
#if !(defined(KIHAGY_1) || defined(KIHAGY_MIND))

typedef struct {
    int a;
    int b;
} intervallum;

intervallum osszead(intervallum i1, intervallum i2) {
    intervallum newinterv;
    newinterv.a = i1.a + i2.a;
    newinterv.b = i1.b + i2.b;
    return newinterv;
}

#endif

/*
2. feladat (5 pont)

Egyszamjatekot jatszunk. Ennek lenyege, hogy sokan valasztanak maguknak egy-egy
pozitiv egesz szamot, es az nyer, aki a legkisebb olyat valasztotta, amit senki
mas. A feladat megirni azt a fuggvenyt, ami egy tomben tarolt pozitiv egesz
szamok kozul kivalasztja a legkisebbet, amely egyedul szerepel a tombben. A
fuggveny parameterei maga a tomb es annak a merete,  visszateresi erteke pedig
a legkisebb egyszam, vagy -1, ha nincs olyan, amit csak egyvalaki tippelt.
A jatekosok nagyobb szamot annal, mint ahanyan jatszanak nem mondanak.
*/

void mybubblesort(int list[], int len) {
    // ez egy megfixalt verzioja a regi bubble sort funkcionak, atalakitva hogy
    // menjen intekkel
    // int bubblesort int array list int bubble sort
    for (int i = 0; i < len; i++) {
        for (int j = 0; j < len - 1; ++j) {
            int value = list[j];
            int value_after = list[j + 1];

            if (value <= value_after) {
                // right order, keep them
            } else {
                // wrong order, switch them
                int placeholder = list[j];
                list[j] = list[j + 1];
                list[j + 1] = placeholder;
            }
        }
    }
}
int egyszam(int tomb[], int meret) {
    mybubblesort(tomb, meret);

    int eddigi_legkisebb_egyeduli = -1;

    if (tomb[0] < tomb[1]) { // a legkisebb elem egyedul van, tuti ez a megoldas
        return tomb[0];
    }

    for (int i = meret - 2; i > 0; --i) { // vegig megyunk a timbon
                                          // a nagyobbaktol a kisebbek fele
        if (tomb[i - 1] < tomb[i] &&
            tomb[i] < tomb[i + 1]) { // ha 3 elembol a kozepso egyedul van, az
                                     // egy lehetseges megoldas, es a legutolso
                                     // felulirasa a eddigi_legkisebb_egyeduli
                                     // -nek a megoldas, mert az a legkisebb
            eddigi_legkisebb_egyeduli = tomb[i];
        }
    }

    // ha nincs 3 asaval veve egyeduli, (nem valtozott -1 rol az
    // eddigi_legkisebb_egyeduli) es az utolso elem nem ugyan az mint az utolso
    // elotti  -> a legnagyobb elem van csak egyedul -> ez a megoldas
    if (eddigi_legkisebb_egyeduli == -1 && tomb[meret - 1] != tomb[meret - 2]) {
        return tomb[meret - 1];
    }
    return eddigi_legkisebb_egyeduli;
}

/***********************************************************************
************************************************************************
**
**	EZEN A PONTON TÚL NE VÁLTOZTASS SEMMIT SEM A FÁJLON!
**
************************************************************************
***********************************************************************/

void call_1() {
#if !(defined(KIHAGY_1) || defined(KIHAGY_MIND))
    int v;
    intervallum a, b, x;
    if (fscanf(stdin, "%d", &v) != 1) {
        fprintf(stderr, "HIBA: Nem olvasható adat!\n");
        return;
    }
    switch (v) {
    case 1:
        if (fscanf(stdin, "%d", &v) != 1) {
            fprintf(stderr, "HIBA: Nem olvasható adat!\n");
            return;
        }
        fprintf(stdout, "fordul%d\n", v);
        break;
    default:
        if (fscanf(stdin, "%d %d", &a.a, &a.b) != 2) {
            fprintf(stderr, "HIBA: Nem olvasható adat!\n");
            return;
        }
        if (a.a > a.b) {
            fprintf(stderr, "HIBA: Hibás input [%d,%d]!\n", a.a, a.b);
            return;
        }
        if (fscanf(stdin, "%d %d", &b.a, &b.b) != 2) {
            fprintf(stderr, "HIBA: Nem olvasható adat!\n");
            return;
        }
        if (b.a > b.b) {
            fprintf(stderr, "HIBA: Hibás input [%d,%d]!\n", b.a, b.b);
            return;
        }
        x = osszead(a, b);
        fprintf(stdout, "[%d,%d]\n", x.a, x.b);
        break;
    }
#endif
}
void test_1() {
#if !(defined(KIHAGY_1) || defined(KIHAGY_MIND))
    int i;
    intervallum eredmeny;
    struct {
        intervallum a, b, eredmeny;
    } testlist[2] = {{{.a = 3, .b = 10}, {.a = 0, .b = 7}, {.a = 3, .b = 17}},
                     {{.a = 2, .b = 5}, {.a = 3, .b = 14}, {.a = 5, .b = 19}}};
    for (i = 0; i < 2; ++i) {
        eredmeny = osszead(testlist[i].a, testlist[i].b);
        if (eredmeny.a != testlist[i].eredmeny.a ||
            eredmeny.b != testlist[i].eredmeny.b) {
            fprintf(stderr,
                    "HIBA: osszead([%d,%d], [%d,%d])\n"
                    "\telvárt eredmény: [%d,%d]\n"
                    "\tkapott eredmény: [%d,%d]\n",
                    testlist[i].a.a, testlist[i].a.b, testlist[i].b.a,
                    testlist[i].b.b, testlist[i].eredmeny.a,
                    testlist[i].eredmeny.b, eredmeny.a, eredmeny.b);
        }
    }
#endif
}

void call_2() {
    int i = 0, db, tippek[128], eredmeny;
    if (fscanf(stdin, "%d", &db) != 1) {
        fprintf(stderr, "HIBA: Nem olvasható adat!\n");
        return;
    }
    for (i = 0; i < db; i++) {
        if (fscanf(stdin, "%d", tippek + i) != 1) {
            fprintf(stderr, "HIBA: Nem olvasható adat!\n");
            return;
        }
    }
    eredmeny = egyszam(tippek, db);
    fprintf(stdout, "%d\n", eredmeny);
}
void test_2() {
    int i, eredmeny;
    struct {
        int tippek[128];
        int db;
        int eredmeny;
    } testlist[2] = {{{5, 5}, 2, -1}, {{1, 2, 3, 4, 3, 2, 1}, 7, 4}};
    for (i = 0; i < 2; ++i) {
        eredmeny = egyszam(testlist[i].tippek, testlist[i].db);
        if (eredmeny != testlist[i].eredmeny) {
            fprintf(stderr,
                    "HIBA: egyszam({...}, %d)\n"
                    "\telvárt eredmény: %d\n"
                    "\tkapott eredmény: %d\n",
                    testlist[i].db, testlist[i].eredmeny, eredmeny);
        }
    }
}

typedef void (*call_function)();

call_function call_table[] = {call_1, call_2, NULL};

call_function test_table[] = {test_1, test_2, NULL};

static int __arg_check_helper(const char *exp, const char *arg) {
    while (*exp && *arg && *exp == *arg) {
        ++exp;
        ++arg;
    }
    return *exp == *arg;
}

int main(int argc, char *argv[]) {
    int feladat, calltabsize;
    if ((argc > 1) && !(__arg_check_helper("-t", argv[1]) &&
                        __arg_check_helper("--test", argv[1]))) {
        if (argc > 2) {
            feladat = atoi(argv[2]);
            for (calltabsize = 0; test_table[calltabsize]; calltabsize++)
                ;
            if (feladat <= 0 || calltabsize < feladat) {
                fprintf(stderr, "HIBA: rossz feladat sorszám: %d!\n", feladat);
                return 1;
            }
            (*test_table[feladat - 1])();
        } else {
            for (feladat = 0; test_table[feladat]; ++feladat) {
                (*test_table[feladat])();
            }
        }
        return 0;
    }
    if (!freopen("be.txt", "r", stdin)) {
        fprintf(stderr, "HIBA: Hiányzik a `be.txt'!\n");
        return 1;
    }
    if (!freopen("ki.txt", "w", stdout)) {
        fprintf(stderr, "HIBA: A `ki.txt' nem írható!\n");
        return 1;
    }
    for (calltabsize = 0; call_table[calltabsize]; calltabsize++)
        ;
    if (fscanf(stdin, "%d%*[^\n]", &feladat) != 1) {
        fprintf(stderr, "HIBA: Nem olvasható a feladat sorszám!\n");
        return 1;
    }
    fgetc(stdin);
    if (0 < feladat && feladat <= calltabsize) {
        (*call_table[feladat - 1])();
    } else {
        fprintf(stderr, "HIBA: Rossz feladat sorszám: %d!\n", feladat);
        return 1;
    }
    fclose(stdin);
    fclose(stdout);
    return 0;
}
