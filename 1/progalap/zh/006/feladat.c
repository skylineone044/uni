/***********************************************************************
* A PROGRAMBAN NEM SZEREPELHETNEK AZ ALÁBBI SOROK:
* #include <string.h>
* #include <math.h>
***********************************************************************/
#include <stdio.h>
#include <stdlib.h>

/***********************************************************************
************************************************************************
**		ETTŐL A PONTTÓL DOLGOZHATSZ A FELADATOKON
************************************************************************
***********************************************************************/

/*
1. feladat (5 pont)

A feladat meghatarozni egy szam valodi osztoinak szamat. A fuggveny egyetlen
parametere a kerdeses szam, amelyrol meg kell mondani, hogy 1-en es onmagan
kivul hany darab osztoja van. A visszateresi ertek a valodi osztok szama.

Kodold le C nyelven a fuggvenyt! A fuggveny fejlecen ne valtoztass!
A fuggveny inputjai a parameterek, outputja a visszateresi ertek.
A fuggveny nem vegez IO muveleteket!
*/

int osztokszama(int n) {
    int szamlalo = 0;

    for (int i = 2; i < n; i++) {
        if (n % i == 0) {
            szamlalo++;
        }
    }
    return szamlalo;
}

/*
2. feladat (5 pont)

Szamold ki egy s1, s2, s3, ... szamtani sorozat elemeinek szorzatat. A
fuggveny megkapja a sorozat elso ket elemet (elso es masodik),
valamint egy egesz intervallum also (a) es felso (b) vegpontjat.
A fuggveny visszateresi erteke az sa*...*sb szorzat.
A szamtani sorozat n-edik eleme: sn=s1+(n-1)*d
*/

double sorozat(double elso, double masodik, int a, int b) {
    double d = masodik - elso;
    double szorzat = 1;
    for (int i = a; i <= b; i++) {
        szorzat *= elso + (i-1) * d;
    }
    return szorzat;
    
}
/***********************************************************************
************************************************************************
**
**	EZEN A PONTON TÚL NE VÁLTOZTASS SEMMIT SEM A FÁJLON!
**
************************************************************************
***********************************************************************/

void call_1()
{
	int n, eredmeny;
	if(fscanf(stdin, "%d", &n)!=1) {
		fprintf(stderr, "HIBA: Nem olvasható adat!\n");
		return;
	}
	eredmeny=osztokszama(n);
	fprintf(stdout, "%d\n", eredmeny);
}
void test_1()
{
	int eredmeny, i;
	struct {int szam; int eredmeny;} testlist[2] = {{6, 2}, {24, 6}};
	for (i = 0; i < 2; ++i) {
		eredmeny = osztokszama(testlist[i].szam);
		if (eredmeny != testlist[i].eredmeny) {
			fprintf(stderr, "HIBA: osztokszama(%d)\n"
							"\telvárt eredmény: %d\n"
							"\tkapott eredmény: %d\n",
				testlist[i].szam, testlist[i].eredmeny, eredmeny);
		}
	}
}

void call_2()
{
	double e, m, eredmeny;
	int a, b;
	if(fscanf(stdin, "%lf %lf %d %d", &e, &m, &a, &b)!=4) {
		fprintf(stderr, "HIBA: Nem olvasható adat!\n");
		return;
	}
	eredmeny=sorozat(e, m, a, b);
	fprintf(stdout, "%.5lf\n", eredmeny);
}
void test_2()
{
  int i;
  double eredmeny;
  struct {double e; double m; int a; int b; double eredmeny;} testlist[2] = {{1.0, 3.0, 3, 5, 315.0}, {1.0, 1.5, 2, 4, 7.5}};
  for (i = 0; i < 2; ++i) {
    eredmeny = sorozat(testlist[i].e, testlist[i].m, testlist[i].a, testlist[i].b);
    if (eredmeny != testlist[i].eredmeny) {
      fprintf(stderr, "HIBA: sorozat(%lf, %lf, %d, %d)\n"
                      "\telvárt eredmény: %lf\n"
                      "\tkapott eredmény: %lf\n",
              testlist[i].e, testlist[i].m, testlist[i].a, testlist[i].b, testlist[i].eredmeny, eredmeny);
    }
  }
}

typedef void (*call_function)();

call_function call_table[] = {
	call_1,
	call_2,
	NULL
};


call_function test_table[] = {
	test_1,
	test_2,
	NULL
};

static int __arg_check_helper(const char * exp, const char * arg) {
	while (*exp && *arg && *exp == *arg) {
		++exp;
		++arg;
	}
	return *exp == *arg;
}

int main(int argc, char *argv[])
{
	int feladat, calltabsize;
	if ((argc > 1) && !(__arg_check_helper("-t", argv[1]) && __arg_check_helper("--test", argv[1]))) {
		if (argc > 2) {
			feladat = atoi(argv[2]);
			for (calltabsize=0; test_table[calltabsize]; calltabsize++);
			if (feladat <= 0 || calltabsize < feladat) {
				fprintf(stderr, "HIBA: rossz feladat sorszám: %d!\n", feladat);
				return 1;
			}
			(*test_table[feladat-1])();
		} else {
			for (feladat=0; test_table[feladat]; ++feladat) {
				(*test_table[feladat])();
			}
		}
		return 0;
	}
	if (!freopen("be.txt", "r", stdin)) {
		fprintf(stderr, "HIBA: Hiányzik a `be.txt'!\n");
		return 1;
	}
	if (!freopen("ki.txt", "w", stdout)) {
		fprintf(stderr, "HIBA: A `ki.txt' nem írható!\n");
		return 1;
	}
	for (calltabsize=0; call_table[calltabsize]; calltabsize++);
	if (fscanf(stdin, "%d%*[^\n]", &feladat)!=1) {
		fprintf(stderr, "HIBA: Nem olvasható a feladat sorszám!\n");
		return 1;
	}
	fgetc(stdin);
	if (0<feladat && feladat<=calltabsize) {
		(*call_table[feladat-1])();
	} else {
		fprintf(stderr, "HIBA: Rossz feladat sorszám: %d!\n", feladat);
		return 1;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
