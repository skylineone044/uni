/***********************************************************************
 * A PROGRAMBAN NEM SZEREPELHETNEK AZ ALÁBBI SOROK:
 * #include <string.h>
 * #include <math.h>
 ***********************************************************************/
#include <stdio.h>
#include <stdlib.h>

/***********************************************************************
************************************************************************
**		ETTŐL A PONTTÓL DOLGOZHATSZ A FELADATOKON
************************************************************************
***********************************************************************/

/*
1. feladat (5 pont)

Epits a memoriaban dinamikusan egy 10x10-es tablat. Hozz letre
dinamikusan egy egydimenzios char tombot amelyben sorfolytonosan
tarolod a tabla elemeit, majd a letrejott tombot toltsd is fel a
parameterben kapott igen, nem vagy hiba
karakterekkel aszerint, hogy a tabla mezojenek sorszama oszthato-e az
oszlopszammal (a 0-val osztas hiba). A fuggveny a lefoglalt dinamikus
memoriara mutato pointerrel terjen vissza. A lefoglalt memoria
felszabaditasaval nem kell foglalkoznod.
*/

char *oszthatosag(char igen, char nem, char hiba) {
    // 1. modszer
    // 1 dimenzios tombkent, es atszamitjuk az indexet
    // konnyu letrehiznui es felszabaditani, nehez indexelni
    // i*oszlopok+j

    // a tomb meretei
    int sorok = 10;
    int oszlopok = 10;
    // letrehozzuk a megelelo meretu tombot
    char *t1 = (char *)malloc(sizeof(char) * sorok * oszlopok);

    /* int sorszam = 0; */
    // vegigmegyunk a sorokon
    for (int i = 0; i < sorok; i++) {
        // minden sorhoz vegigmegyunk az oszlopokon
        for (int j = 0; j < oszlopok; j++) {
            if (j == 0) { // mert a 0-val valo osztas nincs ertelmezve, ez
                          // futasi hibat okozna
                t1[i * oszlopok + j] = hiba;
                // a sor oszthato maradek nelkul az oszlop szamaval
            } else if (i % j == 0) {
                t1[i * oszlopok + j] = igen;
                // nem oszthato maradek nelkul
            } else {
                t1[i * oszlopok + j] = nem;
            }
            /* printf("sorszam :%d, oszlop (j): %d, ertek: %c\n", sorszam, j,
             * t1[i*oszlopok+j]); */
            /* sorszam++; */
        }
    }
    return t1;
}

/*
2. feladat (5 pont)

Irj egy fuggvenyt, amely kiszamolja, hogy egy utasitassorozat hatasara
hova jutunk a (0,0) koordinatabol kezdetben eszak fele nezve! Az
utasitassorozat egy sztring, ami a kovetkezo karakterekbol allhat:
'b', 'j', 'e', 'k', 'd', 'n', '1'..'9'. Minden egyeb, a sztringben
szereplo karaktert figyelmen kivul kell hagyni! A karakterek jelentesei:
'b' - forduljunk balra 90 fokkal; 'j' - forduljunk jobbra 90 fokkal;
'e', 'k', 'd', 'n' - forduljunk eszaknak, keletnek, delnek, nyugatnak;
'1'..'9' - lepjunk elore 1..9 egyseget. A fuggveny visszateresi erteke
a vegpont koordinatainak (elojeles) osszege. Az egyik koordinata kelet,
a masik eszak fele novekszik.
*/

int koord(char *utasitas) {
    int poz_x = 0; // kelet fele no
    int poz_y = 0; // eszak fele no

    unsigned int nezesirany = 0; // 0 eszak, 1 kelet, 2 del, 3 nyugat;

    for (int i = 0; utasitas[i] != '\0'; i++) { // vegigmegyunk az utasitasokon
        //
        // a jelenleg vegrehajtando utasitasnak adunk szep nevet
        char jelen_utasitas = utasitas[i];
        //
        // balra fordul 90 fokkal
        if (jelen_utasitas == 'b') {
            // ha balra fordulunk akkor az irany "szamlalo" lefele megy
            nezesirany = (nezesirany - 1) % 4; // 0 es 3 kozott tartja

        } else if (jelen_utasitas == 'j') { // jobbra fordul 90 fokkal
            // ha jobbra fordulunk akkor az irany "szamlalo" felfele megy
            nezesirany = (nezesirany + 1) % 4; // 0 es 3 kozott tartja

            // ha nem relativan fordulunk akkor egyeruen csak beallitjunk a
            // megfelelo iranyt: 0 eszak, 1 kelet, 2 del, 3 nyugat;
        } else if (jelen_utasitas == 'e') { // eszakra fordul
            nezesirany = 0;
        } else if (jelen_utasitas == 'k') { // keletre fordul
            nezesirany = 1;
        } else if (jelen_utasitas == 'd') { // delle fordul
            nezesirany = 2;
        } else if (jelen_utasitas == 'n') { // nyugatra fordul
            nezesirany = 3;

            // ha az utasitas nem iranybeallitas, hanem *szam* akkor mindig a
            // megfelelo iranyba elore megyunk

            /// az utasitas szam karakter
        } else if ('0' <= jelen_utasitas && jelen_utasitas <= '9') {
            // kiszamoljuk a szam karakterbol, hogy az mekkora szamot jelent
            // mivel sorban vannak, az ascii tablaban, igy ha kivonjuk a 0
            // karakter erteket akkor a megfelelo szamerteket kapjuk
            int elore_avolsag = jelen_utasitas - '0';

            // megneyyuk hogy mien iranyba nezunk, noveljuk a megfelelo iranyoz
            // tartozo koordinatat, mivel etlosan nem mehetunk, ketto koordinata
            // soha nem valtozik egyzerre
            switch (nezesirany) {
            case 0:
                poz_y += elore_avolsag;
                break;
            case 1:
                poz_x += elore_avolsag;
                break;
            case 2:
                poz_y -= elore_avolsag;
                break;
            case 3:
                poz_x -= elore_avolsag;
                break;
            }
        }
    }
    // visszadjuk a vegso koordinatak osszeget
    return poz_x + poz_y;
}
/***********************************************************************
************************************************************************
**
**	EZEN A PONTON TÚL NE VÁLTOZTASS SEMMIT SEM A FÁJLON!
**
************************************************************************
***********************************************************************/

void call_1() {
    const int n = 10;
    int chck;
    int ij;
    char *ptr = NULL, igen, nem, hiba;
    if (fscanf(stdin, "%d %c %c %c", &chck, &igen, &nem, &hiba) != 4) {
        fprintf(stderr, "HIBA: Nem olvasható adat!\n");
        return;
    }
    ptr = oszthatosag(igen, nem, hiba);
    switch (chck) {
    case 1:
        if (ptr != NULL) {
            free(ptr);
            fprintf(stdout, "%d\n", chck);
        } else {
            fprintf(stdout, "NULL\n");
        }
        break;
    case 2:
        if (ptr != NULL) {
            ptr[n * n - 1] = '_';
            fprintf(stdout, "%d\n", chck);
        } else {
            fprintf(stdout, "NULL\n");
        }
        break;
    case 3:
        for (ij = 0; ij < n * n; ij++) {
            fputc(ptr[ij], stdout);
            if (ij % n == n - 1) {
                fputc('\n', stdout);
            }
        }
        break;
    default:
        if (fscanf(stdin, "%d", &ij) != 1) {
            fprintf(stderr, "HIBA: Nem olvasható adat!\n");
            return;
        }
        fprintf(stdout, "%c\n", ptr[ij]);
        break;
    }
}
void test_1() {
    const int n = 10;
    int i, j, k;
    char *ptr = NULL;
    struct {
        char ptr[100];
        char igen;
        char nem;
        char hiba;
    } testlist[1] = {{{"hiiiiiiiiihinnnnnnnnhiinnnnnnnhininnnnnnhiininnnnnhinnn"
                       "innnnhiiinninnnhinnnnninnhiininnninhininnnnni"},
                      'i',
                      'n',
                      'h'}};
    for (i = 0; i < 1; ++i) {
        ptr = oszthatosag(testlist[i].igen, testlist[i].nem, testlist[i].hiba);
        if (ptr == NULL) {
            fprintf(stderr, "HIBA: NULL pointer\n");
            return;
        } else {
            for (j = 0; j < n; ++j) {
                for (k = 0; k < n; ++k) {
                    if (ptr[j * n + k] != testlist[i].ptr[j * n + k]) {
                        fprintf(stderr,
                                "HIBA a(z) (%d,%d) pozíción\n"
                                "\telvárt eredmény: %c\n"
                                "\tkapott eredmény: %c\n",
                                j, k, testlist[i].ptr[j * n + k],
                                ptr[j * n + k]);
                        return;
                    }
                }
            }
        }
        free(ptr);
    }
}

void call_2() {
    int i = 0;
    char kif[100];
    if (fscanf(stdin, "%s", kif) != 1) {
        fprintf(stderr, "HIBA: Nem olvasható adat!\n");
        return;
    }
    i = koord(kif);
    fprintf(stdout, "%d\n", i);
}
void test_2() {
    int i, eredmeny;
    struct {
        char kif[100];
        int eredmeny;
    } testlist[1] = {{"2b3jj1j1ezer1", 0}};
    for (i = 0; i < 1; ++i) {
        eredmeny = koord(testlist[i].kif);
        if (testlist[i].eredmeny != eredmeny) {
            fprintf(stderr,
                    "HIBA: koord(\"%s\")\n"
                    "\telvárt eredmény: %d\n"
                    "\tkapott eredmény: %d\n",
                    testlist[i].kif, testlist[i].eredmeny, eredmeny);
        }
    }
}

typedef void (*call_function)();

call_function call_table[] = {call_1, call_2, NULL};

call_function test_table[] = {test_1, test_2, NULL};

static int __arg_check_helper(const char *exp, const char *arg) {
    while (*exp && *arg && *exp == *arg) {
        ++exp;
        ++arg;
    }
    return *exp == *arg;
}

int main(int argc, char *argv[]) {
    int feladat, calltabsize;
    if ((argc > 1) && !(__arg_check_helper("-t", argv[1]) &&
                        __arg_check_helper("--test", argv[1]))) {
        if (argc > 2) {
            feladat = atoi(argv[2]);
            for (calltabsize = 0; test_table[calltabsize]; calltabsize++)
                ;
            if (feladat <= 0 || calltabsize < feladat) {
                fprintf(stderr, "HIBA: rossz feladat sorszám: %d!\n", feladat);
                return 1;
            }
            (*test_table[feladat - 1])();
        } else {
            for (feladat = 0; test_table[feladat]; ++feladat) {
                (*test_table[feladat])();
            }
        }
        return 0;
    }
    if (!freopen("be.txt", "r", stdin)) {
        fprintf(stderr, "HIBA: Hiányzik a `be.txt'!\n");
        return 1;
    }
    if (!freopen("ki.txt", "w", stdout)) {
        fprintf(stderr, "HIBA: A `ki.txt' nem írható!\n");
        return 1;
    }
    for (calltabsize = 0; call_table[calltabsize]; calltabsize++)
        ;
    if (fscanf(stdin, "%d%*[^\n]", &feladat) != 1) {
        fprintf(stderr, "HIBA: Nem olvasható a feladat sorszám!\n");
        return 1;
    }
    fgetc(stdin);
    if (0 < feladat && feladat <= calltabsize) {
        (*call_table[feladat - 1])();
    } else {
        fprintf(stderr, "HIBA: Rossz feladat sorszám: %d!\n", feladat);
        return 1;
    }
    fclose(stdin);
    fclose(stdout);
    return 0;
}
