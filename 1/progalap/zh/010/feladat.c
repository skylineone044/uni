
#include <stdio.h>
#include <stdlib.h>

int main() {
    // beolvassuk a filet
    FILE *be_txt = fopen("be.txt", "r");
    char kulcsszoveg[102];
    char plaintext[202];

    // a biztonsag kedveert kinullazuk a ulcs es szoveg tomboket, hogy ne legyen
    // benne memoria szemet
    for (int i = 0; i < 102; i++) {
        kulcsszoveg[i] = '\0';
    }
    for (int i = 0; i < 202; i++) {
        plaintext[i] = '\0';
    }

    // beolvassuk a szovegeket a megfelelo tombokbe
    fscanf(be_txt, "%s", kulcsszoveg);
    fscanf(be_txt, "%s", plaintext);
    /* printf("KULCS: %s\nPLAIN:  %s\n", kulcsszoveg, plaintext); */
    fclose(be_txt);

    // plaintext hossza
    // megszamoljuk mien hosszua  tomb, es kozben nezzuk hogy van e benne olyan
    // karakter aminem nem kellene
    // ha van, akkor ezt korrihaljuk, es elmentjuk hogy hol van az elso hibas
    // karakter
    int len = 0;
    int plaintext_error = 0;
    int plaintext_error_pos = 0;
    for (; plaintext[len] != '\0'; len++) {
        // csak akkor mentjuk a hoba hejet ha ez az elso hiba
        if (!plaintext_error &&
            // nem megfelelo karakter
            !('a' <= plaintext[len] && plaintext[len] <= 'z')) {
            // feljegyeyyuk, hou egyaltalan van hiba
            plaintext_error = 1;
            // megjegyezzuk hol van a hiba
            // a len ekkor meg csak a jelenlegi elemim megy, nem az egeszz tomb
            // hosszaras
            plaintext_error_pos = len;
            // korrigaljuk a hibat
            plaintext[len] += 32;
        }
    }
    // kulcs hossza
    // megszamoljuk mien hosszua  tomb, es kozben nezzuk hogy van e benne olyan
    // karakter aminem nem kellene
    // ha van, akkor ezt korrihaljuk, es elmentjuk hogy hol van az elso hibas
    // karakter
    int k_len = 0;
    int key_error = 0;
    int key_error_pos = 0;
    for (; kulcsszoveg[k_len] != '\0'; k_len++) {
        // csak akkor mentjuk a hoba hejet ha ez az elso hiba
        if (!key_error &&
            // nem megfelelo karakter
            !('A' <= kulcsszoveg[k_len] && kulcsszoveg[k_len] <= 'Z')) {
            // feljegyeyyuk, hou egyaltalan van hiba
            key_error = 1;
            // megjegyezzuk hol van a hiba
            // a len ekkor meg csak a jelenlegi elemim megy, nem az egeszz tomb
            // hosszaras
            key_error_pos = k_len;
            // korrigaljuk a hibat
            kulcsszoveg[k_len] -= 32;
        }
    }

    // elkeszitunk egy uj tombot amiben ismetelve van benne a kulcs, pont anyi
    // karakterrel, amekkora a szoveg
    char kulcs_repeted[len];
    // biztonsag kedveert kinullazuk
    for (int i = 0; i < len; i++) {
        kulcs_repeted[i] = '\0';
    }
    // bemasuljuk a megfelelo elemeket
    int szamlalo = 0;
    for (int i = 0; i < len; i++) {
        kulcs_repeted[i] = kulcsszoveg[szamlalo];
        // ez mindig reseteli 0-ra ha elerte a kulcs hosszt, hogy a kulcs
        // kovetkezo karaktere a legelso legyen es nem a tombon kevullevo akarmi
        szamlalo = (szamlalo + 1) % k_len;
    }
    // printf("len: %d, k_len: %d; \nkulcs_repeted: %s\n    plaintext: %s\n",
    // len,
    /* k_len, kulcs_repeted, plaintext); */
    // printf("Plaintext error: %d pos: %d\nkey error: %d pos %d\n\n",
    /* plaintext_error, plaintext_error_pos, key_error, key_error_pos); */

    // elokeszitjuk a kodolt szoveg tombjet
    char kodoltszoveg[len];
    // a biztonsag kedveert kinullazuk
    for (int i = 0; i < len; i++) {
        kodoltszoveg[i] = '\0';
    }

    // vegigmegyunk a szovegen
    for (int i = 0; i < len; i++) {
        // kiszamoljuk mennyivel kell eltolni
        int offset = kulcs_repeted[i] - 'A';
        // ez nem csinal semmit
        offset = offset % 26;

        // if char it rolls over I have big dumb, it took 20 minutes
        int candidate = plaintext[i] + offset;

        if (candidate > 'z') { // ha a kodolt karakter tulfutott az abc-n,
                               // vissza kell  huznunk, egy abc-nyivel
            candidate -= 26;
        }
        // printf("%c + %d = candidate: %d = %c  ,", plaintext[i], offset,
        /* candidate, candidate); */
        // ha negativ, legyen pozitiv, elvileg nem csinal semmit, mert a char-os
        // tulcsordulast akartam ezzel fixelni, es ittmaradt
        if (candidate < 0) {
            candidate *= -1;
        }
        // elmentjuk a kodolast a tombbe
        kodoltszoveg[i] = candidate;

        // printf("i: %d, plain: %d, kodolt: %d, kulcsbit: %c, offset: %d\n", i,
        /* plaintext[i], kodoltszoveg[i], kulcs_repeted[i], offset); */
    }
    /* printf("KODOLT: %s\n", kodoltszoveg); */
    /* printf("HIBA:KULCS:%d\n", key_error_pos); */
    /* printf("HIBA:STR:%d\n", plaintext_error_pos); */

    // kiirjuk a filet
    FILE *ki_txt = fopen("ki.txt", "w");
    fprintf(ki_txt, "%s\n", kodoltszoveg);
    if (key_error) {
        fprintf(ki_txt, "HIBA:KULCS:%d\n", key_error_pos);
    }
    if (plaintext_error) {
        fprintf(ki_txt, "HIBA:STR:%d\n", plaintext_error_pos);
    }
    fclose(ki_txt);
    return 0;
}
