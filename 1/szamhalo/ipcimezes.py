#!/usr/bin/env python3
"""
ipv4 cimekhez maszk szamolas
"""
import math

DEBUG = False


def debugprint(text):
    """
    print if debug mode is on
    """
    if DEBUG:
        print("-- DEBUG   ", text)


def ip_class(ipcim_byte_1):
    """
    megadja az ip cim osztalyat az elso byte alapjan
    """
    if 1 <= ipcim_byte_1 <= 127:
        ipclass = "A"
    elif 128 <= ipcim_byte_1 <= 191:
        ipclass = "B"
    elif 192 <= ipcim_byte_1 <= 223:
        ipclass = "C"
    elif 224 <= ipcim_byte_1 <= 239:
        ipclass = "D"
    elif 240 <= ipcim_byte_1 <= 255:
        ipclass = "E"
    else:
        ipclass = "Hibás cím!"
    return ipclass


def alapmaszk(ipclass):
    """
    megadja az alapértelmezett maszkot az ip címhez
    """
    if ipclass == "A":
        return 8
    if ipclass == "B":
        return 16
    if ipclass == "C":
        return 24
    return 0


def cidr(str_ipcim, maszk):
    """
    kiirja a cidr formatumat a megadott ipcimnek
    """
    print(f"CIDR formátum: {str_ipcim}/{maszk}")


def get_alapmaszk_list(alapmaszk_szam):
    """
    eleksziti az alap mast lista formajat
    """
    maszklist = [0, 0, 0, 0]
    for i in range(4):
        if alapmaszk_szam > 0:
            maszklist[i] = 255
            alapmaszk_szam -= 8
    debugprint(str(maszklist))
    return maszklist


def print_alapmaszk(list_alapmaszk):
    """
    kiirja az alapertelmezett maszkot a liasta alapjan
    """
    print(
        f"Alapértelmezett alhálózati maszk: "
        f"{list_alapmaszk[0]}.{list_alapmaszk[1]}."
        f"{list_alapmaszk[2]}.{list_alapmaszk[3]}"
    )


def binprint(list_ipcim, list_alapmaszk):
    """
    kiirja binaros formaban az ipcimet es a hozza tartozo maszkot
    """
    print(
        f"IP-cím bináris formája: "
        f"{str(bin(list_ipcim[0]))[2:].zfill(8)} "
        f"{str(bin(list_ipcim[1]))[2:].zfill(8)} "
        f"{str(bin(list_ipcim[2]))[2:].zfill(8)} "
        f"{str(bin(list_ipcim[3]))[2:].zfill(8)}"
    )

    print(
        f"Alhálózati maszk bináris formája: "
        f"{str(bin(list_alapmaszk[0]))[2:].zfill(8)} "
        f"{str(bin(list_alapmaszk[1]))[2:].zfill(8)} "
        f"{str(bin(list_alapmaszk[2]))[2:].zfill(8)} "
        f"{str(bin(list_alapmaszk[3]))[2:].zfill(8)}"
    )


def upperlimit(list_alapmaszk):
    """
    kiszamolja hogy egy adott maszkhoz max mennyi alhalozatoto csinalhatunk
    """
    upperlim = 1
    for i in range(4):
        if (255 - list_alapmaszk[i]) > 0:
            upperlim *= 255 - list_alapmaszk[i]
    upperlim -= 2
    debugprint(str(f"upperlim: {upperlim}"))
    return upperlim


def maszk_kiir(str_maszk, hostok_per_alhalozat):
    """
    kiirja a maszkot decimalis, es binaris formaban
    """
    str_maszk_list = str_maszk.split(" ")
    print(
        f"Új alhálózati maszk (decimális): "
        f"{int(str_maszk_list[0], 2)}."
        f"{int(str_maszk_list[1], 2)}."
        f"{int(str_maszk_list[2], 2)}."
        f"{int(str_maszk_list[3], 2)}"
    )
    print(f"Új alhálózati maszk (bináris): " f"{str_maszk}")
    print(
        f"Megcímezhető host-ok száma egy alhálózatban: {hostok_per_alhalozat}"
    )


def felbontas(list_alapmaszk, alhalozatok):
    """
    kiszmolja es kiirja a felbontast
    """
    atalakitanodo_bitek = math.log2(alhalozatok)
    if atalakitanodo_bitek % 1 != 0:
        atalakitanodo_bitek = int(atalakitanodo_bitek) + 1
    debugprint("atalakitanodo_bitek: " + str(atalakitanodo_bitek))
    str_maszk = (
        f"{str(bin(list_alapmaszk[0]))[2:].zfill(8)} "
        f"{str(bin(list_alapmaszk[1]))[2:].zfill(8)} "
        f"{str(bin(list_alapmaszk[2]))[2:].zfill(8)} "
        f"{str(bin(list_alapmaszk[3]))[2:].zfill(8)}"
    )
    debugprint("str_maszk: " + str(str_maszk))
    str_maszk = list(str_maszk)
    for i in range(len(str_maszk)):
        if atalakitanodo_bitek > 0 and str_maszk[i] == "0":
            str_maszk[i] = "1"
            atalakitanodo_bitek -= 1
    else:
        str_maszk = "".join(str_maszk)
        debugprint("str_maszk: " + str(str_maszk))

    # hostok egy alhalozaton:
    hostok_egy_alhon = 0
    for i in range(len(str_maszk)):
        if str_maszk[i] == "0":
            hostok_egy_alhon += 1
    else:
        hostok_egy_alhon = math.pow(2, hostok_egy_alhon)
        hostok_egy_alhon -= 2
        hostok_egy_alhon = int(hostok_egy_alhon)

    maszk_kiir(str_maszk, hostok_egy_alhon)


def bontas_host(list_alapmaszk):
    """
    kiszamolja hogy mannyi alhalozatra van szugseg,
    es elkuldi a felbontas fgv nek
    """
    upperlim = upperlimit(list_alapmaszk)
    while True:
        try:
            hostokszama = int(input("Hány host-ot szeretne megcímezni?\n> "))
            if not 1 <= hostokszama <= upperlim:
                raise ValueError
            break
        except ValueError:
            print("Hiba!")
            continue
    alahalozatok = 1
    while ((upperlim + 2) / alahalozatok) - 2 >= hostokszama:
        alahalozatok *= 2
    alahalozatok /= 2
    alahalozatok = int(alahalozatok)
    debugprint(str(f"alhalozatok: {alahalozatok}"))
    felbontas(list_alapmaszk, alahalozatok)


def bontas_subnet(list_alapmaszk):
    """ "
    bekeri mennyi alhalozatra van szugseg,
    es elkuldi a felbontas fgv nek
    """
    upperlim = upperlimit(list_alapmaszk)
    while True:
        try:
            alhalozatok = int(
                input("Hány alhálózatot szeretne" "létrehozni?\n> ")
            )
            if not 1 <= alhalozatok <= upperlim:
                raise ValueError
            break
        except ValueError:
            print("Hiba!")
            continue
    felbontas(list_alapmaszk, alhalozatok)


def main():
    """
    main
    """
    while True:
        str_ipcim = input("\nAdjon meg egy IP-címet!\n> ")
        list_ipcim = str_ipcim.split(".")
        debugprint(str(list_ipcim))

        try:
            for i in range(len(list_ipcim)):
                list_ipcim[i] = int(list_ipcim[i])
        except ValueError:
            print("Hibás cím!")
            continue
        debugprint(str(list_ipcim))

        ipclass = ip_class(list_ipcim[0])
        print(f"Osztály: {ipclass}")

        if ipclass in ("D", "E"):
            break

        alapmaszk_szam = alapmaszk(ipclass)
        cidr(str_ipcim, alapmaszk_szam)
        alapmaszk_list = get_alapmaszk_list(alapmaszk_szam)
        print_alapmaszk(alapmaszk_list)
        binprint(list_ipcim, alapmaszk_list)

        while True:
            bontasmode = input(
                "\nMi alapján szeretne felbontást végezni? "
                "Válasszon az alábbi opciók közül!\n"
                "host = host-ok száma alapján\n"
                "subnet = alhálózatok száma alapján\n> "
            )
            if bontasmode == "host":
                bontas_host(alapmaszk_list)
                break
            elif bontasmode == "subnet":
                bontas_subnet(alapmaszk_list)
                break
            else:
                print("Hiba!")
                continue


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print()
