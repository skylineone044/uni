.intel_syntax noprefix

.section .data
HELLO:
    .asciz "Hello\n"
    .ascii "Not null terminated by default\n\0"

.section .text
.global main
main:
    /*fgv prologue*/
    push ebp
    mov ebp, esp

    push offset HELLO
    call printf
    pop eax

    /*fgv epilogue*/
    mov esp, ebp
    pop ebp
    ret

