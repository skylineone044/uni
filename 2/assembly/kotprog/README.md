# Assembly kötprog 1: Caesar ARM

## infók

- A c fileban bennehagytam a függvényt, ami alapján megcsináltam az assembly-t,
  hogy érthetőbb legyen, hogy miféle gondolkodás alapján csináltam.
- az assembly-ben, mivel ARM-on nincsen % (mod) utasítás, azt kiszerveztem a
  saját mini eljárásába az S file alján.
- én teszteltem mindenféle shiftekkel, 0, 1, 3, -1, -3, INT_MIN, INT_MAX, üres
  sztring, nagyon hosszú sztring és hasonlók, és nekem működött mindel;
  több féle bemenetre meg nincs ötletem.
- a Makefile-ban benne vannak a parancsok amivel fordítottam

## :)
