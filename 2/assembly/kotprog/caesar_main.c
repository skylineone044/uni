#include <limits.h>
#include <stdio.h>

extern void caesar(char *input, int shift, char *output);

void caesar_c(char *input, int shift, char *output) {
    // worked out the logic so I have a point of reference
    shift = shift % 26;
    shift = shift + 26;
    shift = shift % 26;
    printf("shift: %d\n", shift);
    for (int i = 0; input[i] != 0; i++) {
        output[i] = ((input[i] - 'A' + shift) % 26) + 'A';
    }
}

int main(int argc, char *argv[]) {
    /* char source[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; */
    char source[] = "HELLO";
    char result[30];
    caesar(source, 3, result);
    printf("\nsource: %s;\nresult: %s;\n", source, result);
    return 0;
}
