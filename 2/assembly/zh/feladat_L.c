#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAGIC1
#define MAGIC2
#define MAGIC3
#define MAGIC4
#define FILL 0xF0

#ifdef BIRO_MAGIC
#include "biro_feladat_L.c"
#endif

/** Feladat: */
extern int feladatSqrSeqL(int a, int b, int output[]);
/***/

#define MAX_ARRAY_SIZE (40)
static void snprint_array(char *output, int output_size, int array[], int length);

int main(int argc, char **argv) {
    struct {
        int a;
        int b;
        int expected[MAX_ARRAY_SIZE];
        int expected_length;
    } tests[] = {
        { 3, 5, { 9, 16, 25 }, 3 },
        { -2, 3, { 4, 1, 0, 1, 4, 9 }, 6 },
        { 5, 3, { }, 0 },
        MAGIC1
    };
    int test_count = sizeof(tests) / sizeof(tests[0]);
    int test_start = 0;
#ifdef BIRO_MAGIC
    normalize_test_count(argc, argv, &test_start, &test_count);
#endif

    int failed = 0;
    for (int idx = test_start; idx < test_count; idx++) {
        int output[MAX_ARRAY_SIZE];
        memset(output, FILL, sizeof(output));

        MAGIC2;

        int output_length = feladatSqrSeqL(tests[idx].a, tests[idx].b, output);

        char *result_text = "FAIL";
        if ((output_length == tests[idx].expected_length)
            && (memcmp(output, tests[idx].expected, tests[idx].expected_length * sizeof(int)) == 0)) {
            result_text = "PASS";
        } else {
            failed++;
        }

        MAGIC3;

        char output_array_text[1024];
        snprint_array(output_array_text, sizeof(output_array_text),
                      output, output_length);

        char expected_array_text[1024];
        snprint_array(expected_array_text, sizeof(expected_array_text),
                      tests[idx].expected, tests[idx].expected_length);

        printf(MAGIC4 "Test %s: feladatSqrSeqL(%d, %d) => output: [ %s ] return value: %d"
               " (expected output: [ %s ] return value: %d)\n",
               result_text, tests[idx].a, tests[idx].b,
               output_array_text, output_length, expected_array_text, tests[idx].expected_length);
    }

    return failed;
}

static void snprint_array(char *output, int output_size, int array[], int length) {
    int pos = 0;

    memset(output, 0, output_size);

    for (int idx = 0; idx < length; idx++) {
        pos += sprintf(output + pos, "%d, ", array[idx]);
    }
}

