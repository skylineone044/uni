.syntax unified
.cpu cortex-a7


.text
.global feladatSqrSeqL
feladatSqrSeqL:
    push {r4-r11, lr}

    mov r4, r0
    mov r5, r1
    mov r6, r2
    @ r4 a
    @ r5 b
    @ r6 output address
    cmp r4, r5
    bgt _end


    @ r7 current value
    @ r8 address offset
    @ r9 counter
    ldr r8, =0
    ldr r9, =0

_loop:

    cmp r4, r5
    bgt _end

    mul r7, r4, r4
    str r7, [r6, r8]
    add r8, #4  @ because ints
    add r9, #1  @ counter
    add r4, #1  @ next val to be multiplied
    b _loop




_end:
    mov r0, r9  @ ret val is in r9
    pop {r4-r11, pc}
