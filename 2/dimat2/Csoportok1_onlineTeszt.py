import sys

MAX = 100

if sys.argv[1] == "order":
    mode = sys.argv[2]
    if mode not in ("*", "+"):
        print("wrong mode!")
        sys.exit(1)

    Z_ = int(sys.argv[3])
    o = int(sys.argv[4])

    TRACKER = 0 if mode == "+" else 1
    RES = 0

    if mode == "*":
        for i in range(MAX):
            print(f"i+1: {i+1}  TRACKER: {TRACKER}")
            TRACKER = (TRACKER * o) % Z_
            if TRACKER == 1:
                RES = i + 1
                break

    if mode == "+":
        for i in range(MAX):
            print(f"i+1: {i+1}  TRACKER: {TRACKER}")
            TRACKER = (TRACKER + o) % Z_
            if TRACKER == 0:
                RES = i + 1
                break

    print(
        f"(Z{'*' if mode == '*' else ''}_{Z_};\
{'*' if mode == '*' else '+'}) o({o}) = {RES}\n"
    )

elif sys.argv[1] == "partGroup":
    from math import gcd

    print("THIS DOES NOT CALCULATE THE CORRECT ANSWER DO NOT USE")

    mode = sys.argv[2]
    if mode not in ("*", "+"):
        print("wrong mode!")
        sys.exit(1)

    Z_ = int(sys.argv[3])

    TRACKER = 0 if mode == "+" else 1

    nums = sys.argv
    del nums[0]
    del nums[0]
    del nums[0]
    del nums[0]
    nums = list(map(int, nums))
    print(nums)

    # find lnko
    lnko = gcd(*nums)
    print("lnko:", lnko)

    H = []

    if mode == "*":
        for i in range(MAX):
            TRACKER = (TRACKER * lnko) % Z_
            H.append(TRACKER)
            if TRACKER == 1:
                break

    if mode == "+":
        for i in range(MAX):
            TRACKER = (TRACKER + lnko) % Z_
            H.append(TRACKER)
            if TRACKER == 0:
                break
    H.sort()
    print("Reszcsoport:", H)
