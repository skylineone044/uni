( (∀z p( h(z, w), j(z, y, w) )) ↔ (∃y t( z, y, w )) ) ⋅ [z / f(z, y, w)]
( (∀z p( h(z, w), j(z, y, w) )) ↔ (∃y t( f(z, y, w), y, w )) ) ⋅ [z / f(z, y, w)]
( (∀z p( h(z, w), j(z, y, w) )) ↔ (∃x t( f(z, y, w), x, w )) )
