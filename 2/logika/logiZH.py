¬ ∀ ∃ ∧ Σ
((((((((((((((()))))))))))))))

∀x ∃w ∀y( ( q(y, j(y)) → r(x, y) ) ∧ q( w, y ) ∧ ( ¬ q( x, j(x) ) ∨ ¬ r( y, x ) ) )
∀x ∃w ∀y((¬q(y, j(y)) v r(x, y)) ∧ q(w, y) ∧ (¬q(x, j(x)) ∨ ¬r(y, x)))
    ∃w helyett h(x)
∀x ∀y((¬q(y, j(y)) v r(x, y)) ∧ q(h(x), y) ∧ (¬q(x, j(x)) ∨ ¬r(y, x)))

klozok:
{ {¬q(y, j(y)), r(x, y)}, {q(h(x), y)}, {¬q(x, j(x)), ¬r(y, x)} }


egyesiteses:
s =
c = {p(u, f(w), c) , p(v, z, z) , p(f(x), u, y)}
s = [u/v]
c = {p(v, f(w), c) , p(v, z, z) , p(f(x), v, y)}
s = [u/v][z/f(w)]
c = {p(v, f(w), c) , p(v, f(w), f(w)) , p(f(x), v, y)}
s = [u/v][z/f(w)]
c = {p(v, f(w), c) , p(v, f(w), f(w)) , p(f(x), v, y)}
STOP a kovi lepessben a c-t es az f(w) kellene egyesiteni, de egyik sem valtozo
ezert megall az algoritmus

helyettesitos:
( (∀x s( y, x, w )) → (∃y r( i(y, w), f(y, x, w) )) ) ⋅ [y / g(y, x, w)]
( (∀x s( g(y, x, w), x, w )) → (∃y r( i(y, w), f(y, x, w) )) )
( (∀x1 s( g(y, x, w), x1, w )) → (∃y r( i(y, w), f(y, x, w) )) )
