1.
Igaz-e, hogy ha \Sigma egy klózhalmaz, p egy változó és \Sigma|_{p=0} kielégíthető, akkor \Sigma|_{p=0}~\equiv~\Sigma~?
    attól hogy kielégíthető mindkét oldal, még nem lesznek ekvivalensek, ez hamis lesz, és egy ellenpélda a szigma = {{nemp}}, amire szigma p=0 = {} üres cnf, ami tautológia és nem ekvivalens az eredetivel, mert az eredetit nem elégíti ki a p=1, az üres cnft pedig igen

Igaz-e, hogy ha \Sigma klózok egy halmaza, p\neq q változók és \bigl(\Sigma|_{p=0}\bigr)|_{q=1} kielégíthetetlen, akkor \bigl(\Sigma|_{q=1}\bigr)|_{p=0} is kielégíthetetlen? Válaszod indokold!
                                              ^--- ez a != in latex
    igen, mert amikor megszoritunk egy kloz halmazt tobbszor, attol meg ugyan azokat az elemeket fogjuk kivenni, csak mas sorrendben, de az itt nem szamit, mert a vegeredmeny kloz halmaz ugyan az lesz

Igaz-e, hogy ha \Sigma egy klózhalmaz, p egy változó és \Sigma|_{p=0} kielégíthető, akkor \Sigma|_{p=1}\equiv \Sigma? Válaszod indokold!
    nem, mert pl { {nem p} } ez kielégíthető, es a p=1 es megszoritasa  az { {} } ami nyilvanvaloan nem ekvivalens a { {nem p} } -val, mert az meg p=0 ra {} lenne ami mindig igaz

Igaz-e, hogy ha \Sigma egy klózhalmazként felírt CNF, p egy változó és \Sigma tautológia, akkor \Sigma|_{p=0} is és \Sigma|_{p=1} is biztosan tautológia? Válaszod indokold!
    igaz, mert a tautológia, def szerint minden ertekadasra igaz, tehat midket p=1 es p=0 megszoritasra is

Igaz-e, hogy ha \Sigma egy klózhalmaz, p egy változó és \Sigma|_{p=0} kielégíthetetlen, akkor \Sigma|_{p=1}\equiv \Sigma? Válaszod indokold!
    nem feltetlenul, mert ha megszoritjuk a szigmat, pl p=1 akkor az orrszes olyen klozt kivesszuk amiben volt p, es az osszes nem p elemeket kivesszuk a klozokbol,
    pl szigma={ {p}, {q, nem q} } p=0 ra kielégíthetetlen ({ {}, {q, nem q} }), es ha nezzzuk a p=1 megszoritasat, akkor az { {q, nem q} } ami nem ekvivalens a szigmaval,
    mert ez ({ {q, nem q} }) mimindig igaz, a szigma pedig nem lasd: p=0, amikor a szigma kkielégíthetetlen

Igaz-e, hogy ha \Sigma egy klózhalmazként felírt CNF, ami tautológia, p pedig egy változó, akkor \Sigma|_{p=0}~=~\Sigma|_{p=1}? Válaszod indokold! (ha igaz, miért igaz, ha nem, adj konkrét ellenpéldát)
    nem, mert attol hogy minden vegkimenetel igaz barmeny ertekadasra, az oda vezeto ut nem biztos hogy ugyan az pl { {p, nem q, q} } eseten, p=0 ra { {nemq q, q} } p=1 re pedig { } amik nem egyenloek (, de ekvivalensek ?)

Igaz-e, hogy ha \Sigma egy klózhalmazként felírt CNF, p egy változó és \Sigma is és \Sigma|_{p=1} is kielégíthetőek, akkor \Sigma\equiv\Sigma|_{p=1}?
    nem, mert pl { {p, q} } nek a p=1 es megszoritasa az { } ami nem ekvivalens az eredetivel, mert pl q=0 ra az eredeti az { {p} }, ami nem ekvivalens a { } (az eredeti p=1 el megkotve) val

Igaz-e, hogy ha \Sigma egy klózhalmaz, p egy változó és \Sigma kielégíthető, akkor \Sigma|_{p=0} vagy \Sigma|_{p=1} (vagy mindkettő) is kielégíthető? Válaszod indokold!
    igen, mert ha a szigma kielégíthető, akkor ha nezzuk a fajat valameik iranyba ha megyunk a p menten, valahol tuti ki fog jonni hogy kielégíthető, vagy a p=0, vagy a p=1 vagy mindketton, de legalabb az egyiken tuti


2.
Igaz-e, hogy ha a \Sigma klózhalmazban n változó szerepel, akkor a \Sigma inputon futtatott DPLL algoritmus által generált teljes DPLL fában legfeljebb n^2 csúcs lesz? Válaszod indokold!
    nem, mert pl szigma={ {p} } bol lesz {} es ez a faban 2 csucs, de a szigmaban csak 1 elem volt, es 1^2 = 1 ami kisebb mint a 2, ahany csucs van a faban

Igaz-e a következő állítás? Válaszod indokold!i Azért fontos, hogy előbb a unit propagationt, és csak ezután a pure literal eliminationt alkalmazzuk ha lehet, mert különben lenne olyan \Sigma input, melyre az algoritmus hibás választ adna.
    nem, mert mind2 lepes teljesen helyes es a sorrend nem valtoztat az eredmenyen, tehat tokmindegy mien sorrendben csinaljuk oket

Igaz-e a következő állítás? Válaszod indokold! Ha \Sigma-ban \ell pure literál (azaz \overline{\ell} nem fordul elő \Sigma-ban, de \ell igen), akkor \Sigma minden modelljében \ell értéke 1.
    nem, mert van olyen amikor mindegy, hogy l erteke micsoda: pl {{p, q}, {q, r}} itt mindegy hogy a q az mi, hogyha a p meg az r az mindketto =1 el, akkor ez kielégíthető, pedig van benne pure literal ami lehet 0 is

Igaz-e, hogy ha egy DPLL fában egy \Sigma-val címkézett csúcsnak egy \Sigma'-vel címkézett csúcs a(z egyik) gyereke, akkor biztos, hogy \Sigma'-ben kevesebb klóz van, mint \Sigma-ban? Válaszod indokold!
    igen, a dpll nek ez a lenyege, hogy folyton kevesebb kloz legyen a halmazban (olyan sztem nincs hogy ugyanannyi elem lenne a szigmaban, meg a gyerekeben hogyha dpll-t csinalunk, fullos fa kiirasanal persze lehet olyen, de a dpll ezt kioptimalizalja; tobb kloz meg nem lehet benne, az a lenyege annak amit csinalunk hogy kevesebb legyen benne)

Igaz-e, hogy ha végigfuttatjuk a DPLL algoritmust egy \Sigma klózhalmazon, akkor a \emptyset címkéjű csúcsok együttesen mindig megadják \Sigma összes modelljét? Válaszod indokold!
    amikor a dpll nem mindennek ad erteket akkor az igazabol tobb ertekadas egyben, igy lefednek mindent az ures halmazos levelek, szoval vegul is igaz az allitas
    (de az elso tippem a nem volt, mert pl {{p, q}, {q, r}} nel a dpll elintezi egy pure literal elimination -el: q=1 es done, itt megall a dpll; de ha mi azt vesszul hogy p=1 es r=1, az is egy lehetseges modell, de oda nem megy a dpll)

Igaz-e, hogy ha a \Sigma klózhalmazban n darab különböző változó szerepel, akkor a belőle indított DPLL algoritmus által generált DPLL fa legfeljebb n magas lesz? (Egy fa magassága a leghosszabb gyökér-levél út hossza, a DPLL előadásfólián lévő példa fának a magassága pl. 3.)
    igaz, mert minden egyes lepessel (lefele a faban) pontosan 1 valtozot fogunk kivenni a halmazbol (vagy klozostol, vagy csak a valtozot a klozabol) igy max annyi lefele lepes lehet ahany valtozo van, mert ha nincs tobb valtozo akkor a dpll vegere ertunk

Ha az input \Sigma klózhalmazban legfeljebb m darab klóz van, akkor a belőle generált teljes DPLL fa milyen magas lehet legfeljebb? (Egy fa magassága a gyökér és a tőle legmesszebb levő levél távolsága, a DPLL példáé az előadás fóliákon pl 3.) Válaszodat indokold!
    mivel minden egyes lepessel lefele a faban kivesszuk egy valtozot es klozt, max olyan magas lehet amennyi kloz van; szoval m magas lehet max

???
Igaz-e a következő állítás? Válaszod indokold! Azért jó sorrend, hogy előbb a unit propagationt, és csak ezután a pure literal eliminationt alkalmazzuk ha lehet, mert ebben a sorrendben haladva garantáltan legfeljebb annyi csomópontja lesz a DPLL fának, mint ha a pure literal eliminationt vennénk előbbre.
    ??? nemtom tippre mindegy a sorrend

3.
Lezárási operátor-e a következő h: P(\mathbb{N}_0)~\to~P(\mathbb{N}_0) függvény?
h(X)~:=\{x\in X:~x\neq7\}
(Például h({2,3}) = {2,3} és h({1,5,7,8,10})={1,5,8,10}.)
    Nem, mert mert veszitet elemeket, mert pl ha van benne 7es, akkor az ki vesszuk belole

Lezárási operátor-e a következő h: P(\mathbb{N}_0)~\to~P(\mathbb{N}_0) függvény?
h(X)~:=~\{x+1:x\in X\}
(Például h({2,3}) = {3,4} és h({1,5})={2,6}.)
    nem, mert az elemek kozul pl a legkisebbet mindik kivesszuk, mert az egyel nagyobbat tesszuk a helyere, ezert a bovito tulajdonsag nem teljesul

Lezárási operátor-e a következő h: P(\mathbb{N}_0)~\to~P(\mathbb{N}_0) függvény?
h(X)~:=~X\union\{x+1:x\in X\}
(Például h({2,3}) = {2,3,4} és h({1,5})={1,2,5,6}.)
    nem idempotens, mert ha a {2, 3, 4} et pl meg egyszer bovitjuk, akkor abbol {2, 3, 4, 5} lesz, es nem pedig a {2, 3, 4} amibol kiindultunk; ezalol kivetel az N0, mert abban mar minden benne van

Lezárási operátor-e a következő h: P(\mathbb{N}_0)~\to~P(\mathbb{N}_0) függvény?
h(X)~:=~X-\{69\}
(itt a kivonás a halmazelméleti különbséget jelenti, pl. h({67,68,69,70,420}) = {67,68,70,420}.) Válaszod indokold!
    nem, mert ha van a halmazban 69 azt kivesszuk, igy ez nem lesz bovito

Lezárási operátor-e a következő h: P(\mathbb{N}_0)~\to~P(\mathbb{N}_0) függvény?
h(X)~:=~\begin{cases}\{0,2,4,6,8,\ldots\}&\hbox{ha }0\in X\\X&\hbox{egyébként}\end{cases}
(Pl. h({0,1}) = {0,2,4,6,8,10,..} és h({1})={1}.) Válaszod indokold!
    nem, ha van 0 is es egy paratlan szam is a halmazban, akkor kivesszuk az osszes paratlan szamot, es ezert ez nem lesz bovito

Lezárási operátor-e a következő h: P(\mathbb{N}_0)~\to~P(\mathbb{N}_0) függvény?
h(X)~:=X~\cup~\{2\cdot x:x\in X\}
(Például h({2,3}) = {2,3,4,6} és h({1,5})={1,2,5,10}.)
    nem, mert nem idempotens, mert ha {2, 3, 4, 6} ot megegyszer bovitjuk, akkor nem kapjuk vissza a {2, 3, 4, 6} -t; ez alol kiveve ha mar az egesz N0 benne van a halmazban

h(X)~:=~N_0, ha 0\inX
    igen, mert bovio pipa, mert vagy nem tesszuk bele semmit tobbet, vagy mindent, igy nem veszithet elemet
    idempotens pipa, mert ha mar minden benne van, akkor tobb nem mehet bele, vagy nem teszunk bele semmit, igy ha ismeteljuk a lepest, mindig ugyan azt a dolgot fogjuk ismetelni a semmi vagy minden beletevese kozul
    monoton pipa, mert vagy mindent teszunk bele, vagy semmit, ezert a kiindulasi halmaz mindig reszhalmaza lesz az eredmenynek, mert vagy ugyan azok, vagy az eredmeny az N_0

h(X)~:=~\{c\inN_0 : ∃a,b\inX : a\leqc\leqb\}
h(X)~:=~\{c\in \mathbb{N}_0:~\exists a,b\in X:a\leq c\leq b\}
    igen, mert ez csak kitolti a lyukakat az inputban, igy elemet nem vesz ki, ha tobbszor ismeteljuk tobb dolog nem kerul bele (mert a lyukakat mar az elso vegrehalytassal kitolti), es ha ket halmazon csinaljuk amik egymas reszhalmazai, akkor a ket eredmeny is egymas reszhalmaza lesz, mert az intervallumon minden elem bekerul

X\longmapsto\{x+y : x,y\inX\union\{0\}\}
    nem, mert nem idempotens, mert folyton nol: mindig tobb es tobb elem osszegei kerulnek bele

X\longmapsto\{x+y : x,y\inX\}
    nem, mert veszithet elemeket, ezert nem bovito pl {1} -> {2} es az 1 est elvesztettuk

Risus —
Am a lezárási operátor az olyan mint a diploma tudásanyaga, elsősként:
- az eddigi tudásodat is belefoglalja (monoton)
- tanulod kell még 2 évet mire megszerzed (bővítő)
- ha mégegyszer megcsinálod ugyanezt, nem lesz több tudásod (idempotens)
ha elfelejted amit eddig tanultál de megszerzed a diplomát, akkor sérül a monotonitás, mert kisebb tudásanyagot várna el
ha megszerezted egy félév után, utólag (kinda bullshit tho, mint az előző is), akkor nem bővítene, "felejtened" kéne egy félévet hozzá
ha mégegyszer megcsinálod ugyanezt és még adna hozzá, akkor eleve nem tanultál meg valamit most
egy ideális világban mindhárom előbbi szembe megy a diploma megszerzésével :D
—
4.
Szerinted igaz-e minden A struktúrára, hogy ha A⊨∀z(p(z)) ↔∀y(q(y)), akkor A⊨ ∀z(p(z) ↔ q(z))? Válaszod indokold!
    nem, mert pl ha q: paros szamokra igaz, p: paratlan szamokra igaz, akkor a bal oldali azt modja hogy barmely szam paratlan <-> barmely szam paros, ami hamis <-> hamis, ami meg igaz lesz,
    de a jobb oldal meg azt mondja hogy barmeny szam paros <-> paratlan, ami meg mindig  hamis, mert egy szam vagy paros, vagy paratlan, de sose mind2, igy kijon az a vegen hogy igaz -> hamis, ami hamis, igy hamis az allitas

Szerinted igaz-e minden A struktúrára, hogy ha A ⊨∃x(p(x)) ∧∃y(q(y)), akkor A ⊨∃z(p(z) ∧ q(z))? Válaszod indokold!
    nem, mert ha pl a pozitiv szamok halmazan; p: a szam paros, q: a szam paratlan akkor az elso fele a dolognak azt mondja hogy letezik paros szam, es letezik paratlan szam, ami igaz ∧ igaz, ami igaz,
    a jobb oldal meg azt mondja, hogy van egy szam ami paros is es paratlan is egyszere, ami meg nyilvan nincs, igy az hamis, es a vegen az jon ki hogy igaz -> hamis, ami hamis, igy hamis az allitas

Szerinted igaz-e minden A struktúrára, hogy ha A ⊨∃z(p(z) ∧ q(z)), akkor A ⊨∃x(p(x)) ∧∃y(q(y))? Válaszod indokold!
    nyilvan igaz, mert ha van olyan objektum amire a p is meg a q is igaz akkor nyilvan van objektum amire igaz a p, es van olyan objektum amire igaz a q: pl ugyan az amire mind2 igaz

Szerinted igaz-e minden A struktúrára, hogy ha A⊨ ∃x(p(x) → q(x)), akkor A⊨∃z(p(z)) →∃y(q(y))? Válaszod indokold!
    nem, mert pl ha a Termeszetes szamok strukturajan nezzuk pl p: igaz ha a szam paros, q semmire sem igaz, akkor mar egybol az elso fele a dolognak nem teljesul

Szerinted igaz-e minden A struktúrára, hogy ha A ⊨∀x(p(x)) ∧∀y(q(y)), akkor A ⊨ ∀z(p(z) ∧ q(z)) ? Válaszod indokold!
    igen, mert ha p minden szamra igaz, es q is minden szamra igaz, akkor tuti hogy minden szamra igaz a p is es a q is szoval a p ∧ q is igaz mindenre

Szerinted igaz-e minden A struktúrára, hogy ha A ⊨∀x(p(x)) V ∀y(q(y)), akkor A ⊨ ∀z(p(z) V q(z)) ? Válaszod indokold!
    igen, mert ha mar mondjuk p teljesul barmely elemre a bal oldalon, akkor ugyan ugy teljesul minden elemre a jobb oldalon, ez igy van a q val is, es mivel vagyolunk, mindegy hogy meik teljesul a bal oldalon, ez ugyan ugy a jobb oldalon is fog

Szerinted igaz-e minden A struktúrára, hogy ha A ⊨∀z(p(z) ∧ q(z)), akkor A ⊨∀x(p(x)) ∧∀y(q(y))? Válaszod indokold!
    igen, mert ha a bal oldalon minden elemre teljesul a p is es a q is, akkor tuti fix hogy a jobb oldalon teljesul a mindenre a p es mindenre a q

5.
Szerinted igaz-e zárt Skolem normálformák tetszőleges \Sigma halmazára, hogy \mathrm{E}(\Sigma)~\vDash~\Sigma?
Válaszod indokold! (Ha igen, miért, ha nem, mutass egy ellenpéldát és mondd el, miért ellenpélda! Itt E a Herbrand-kiterjesztés műveletének a jele.)
    nem, mert pl ha szigma={barmely x p(x)} es c/0, f/1 -eink vannak, akkor E(szigma)={ pc, pfc, pffc, ... }, es p interpretacioja mondjuk "nem negativ", c interpretacioja 0, f interpretacioja +1, akkor ez kielegiti az E(szigma)-t, de a szigmat nem

Ha Σ és  Γ zárt Skolem-normálformák halmazai, akkor igaz-e, hogy E(Σ) U E(Γ) = E(Σ U Γ)? Válaszod indokold! (Ha igen, miért, ha nem mindenképp, akkor adj meg egy ellenpéldát! Itt E a Herbrand-kiterjesztés műveletének a jele.)
    igen, mert mindegy hogy mikor uniozunk, igyis ugyis mindent beveszunk, mindegy hogy elobb Herbrang kiterjesztunk, es utana azokatt uniozzuk, vagy elobb uniozunk, es utana herbrand kiterjesztjuk az egeszet

Ha Σ és  Γ zárt Skolem-normálformák halmazai, akkor igaz-e, hogy E(Σ) ∩ E(Γ) = E(Σ ∩ Γ)? Válaszod indokold! (Ha igen, miért, ha nem mindenképp, akkor adj meg egy ellenpéldát! Itt E a Herbrand-kiterjesztés műveletének a jele.)
    nem, mert pl ha szigma={ barmely x p(x) }, gamma={ barmely y p(y) } akor a metszetuk, meg a metszetuk kiterjesztese is ures lesz, de ha pl T_0 = {c} akkor mind2nek a kiterjesztese az p(c) lesz, igy a metszetuk is pont az; es a {p(c)} != {}

Ha Σ zárt Skolem-normálformák halmaza, akkor igaz-e, hogy E(Σ) ⊆ Σ ? Válaszod indokold! (Ha igen, miért, ha nem mindenképp, akkor adj meg egy ellenpéldát! Itt E a Herbrand-kiterjesztés műveletének a jele.)
Ha Σ zárt Skolem-normálformák halmaza, akkor igaz-e, hogy Σ ⊆ E(Σ) ? Válaszod indokold! (Ha igen, miért, ha nem mindenképp, akkor adj meg egy ellenpéldát! Itt E a Herbrand-kiterjesztés műveletének a jele.)
    szinte sose igaz (kiveve ha szigma ures, vagy a szigmaban levo formulak egyikeben sincsen valtozo, csak konstans, mert akkor a szigma meg az E(szigma) az ugyan az, es igy egymas reszhalmazai, szoval azokra igaz, pl ha szigma={pc} akor E(szigma) is {pc}, amik egyenloek), a ket fele dolgoknak amik bennuk vanna nincs sok koze egymashoz, pl ha szigma={ barmely x p(x) } akkor mondjuk ha a T_0={c} akkor E(szigma) az {p(c)} lesz, es egyig sem reszhalmaza a masiknak

Szerinted van-e zárt Skolem normálformáknak olyan nemüres Σ halmaza, melyre Σ = E(Σ)? Válaszod indokold! (Ha igen, mutass egyet, ha nem, miért nincs? Itt E a Herbrand-kiterjesztés műveletének a jele.)
    igen van, pl ( amikor a szigma ures de azt most itt nem nezzuk ), vagy nincsen egyetlen benne levo formulavan sem valtozo, csak konstans pl szigma={ pc } annak a herbrand kiterjesztese is { pc }


6.
Szerinted igaz-e, hogy minden olyan elsőrendű formula, ami a természetes számok standard struktúrájában igaz, le is vezethető a Peano axiómákból elsőrendű rezolúcióval? Válaszodat indokold!
    persz, erre lettek kitalalva a peano axiomak, hogy lehessen termeszetes szamokon levezetgetni dolgokat res-el

Szerinted létezik-e olyan C elsőrendű nemtriviális klóz, amit saját magával lehet rezolválni úgy, hogy a rezolvens egy új klóz lesz? (Itt a C "nemtriviális" akkor, ha nincs benne olyan literál, melynek szerepel a komplementere is C-ben.)
Ha nincs ilyen C klóz, miért nincs, ha van, írj fel egy példát!
    letezik: pl C={nem p(f(x)), p(x)} abbol (ugye a rezovalashoz atnevezzuk az egyik masolatban az x et mondjuk x1 re: {nem p(f(x1)), p(x1)}) osszeresolvalva: {nem p(f(f(x)), p(x)} lesz

Mondjuk, hogy egy \{F,G\}\vDash H alakú következményt szeretnénk igazolni elsőrendű rezolúcióval úgy, hogy az F, G és \neg H formulákat külön-külön zárt Skolem alakra hozzuk és a kapott összes klózt egy \Sigma klózhalmazba tesszük, ezen indítjuk el az elsőrendű rezolúció algoritmusát.
Igaz-e, hogy ha egy x változó szabadon előfordul F-ben, G-ben és H-ban is, akkor mindhárom formulában ugyanarra az új konstansjelre kell cseréljük a szabad x-eket, hogy helyes legyen az algoritmus? Válaszod indokold!
    igen, mert amikor egy valtozo szabad akkor a struktura fi-jebol kapja az erteket, barmien klozban is van, mindben ugyan azt az ertteket kell kapnia, ezert mindben ugyan azt a nevet kell neki adni, ugymond globalis valtozok

Igaz-e, hogy ha egy elsőrendű logikai klózban szerepel két ellentétes előjelű, de előjel nélkül egyesíthető literál (pl. p(x) és \neg p(f(y)) ), akkor ez a klóz triviális és ugyanúgy eldobható \Sigma-ból, mint ítéletkalkulusban? Válaszod indokold (okfejtéssel vagy ellenpéldával)!

Szerinted igaz-e, hogy ha elsőrendű klózoknak egy Σ halmaza véges, akkor Res*(Σ) is véges? (azaz: igaz-e, hogy elsőrendű rezolválással csak véges sok klózt tudunk levezetni, ha eleve csak véges sok klózból indulunk?)
Ha igen, miért igaz? Ha nem, adj egy konkrét ellenpéldát! (Ahogy korábban is: ha két klóz megkapható egymásból változóátnevezéssel, akkor nem tekintjük őket különbözőnek.)
    nem, mert elsorendu logikaban lehet vegtelen sokat is csinalni: pl {px, nem pfx} bol lehet csinalni {px, nem pfx, nem pffx, nem pff...fx, stb} vegtelen sokat

Mondjuk, hogy egy \{F,G\}\vDash H alakú következményt szeretnénk igazolni elsőrendű rezolúcióval úgy, hogy az F, G és \neg H formulákat külön-külön zárt Skolem alakra hozzuk és a kapott összes klózt egy \Sigma klózhalmazba tesszük, ezen indítjuk el az elsőrendű rezolúció algoritmusát.
Igaz-e, hogy ha egy F-beli változó helyére bevezetünk egy f Skolem-függvényt, akkor ez az f nem szerepelhet F-ben sem és G-ben sem, de ha H-ban szerepel, az nem baj, mert a logikai követkemény két oldalán lévő szimbólumok nem keverednek? Válaszod indokold!
    nem, mert pl mar azon megbukik a tortenet, hogy nem keverednek a dolgok a logikai kovetekezmeny ket oldalan, mert de keverednek; de azon is hogy a ket fele dolog amire az f et akarjuk az tok mast jelent, nem nagyon lenne okes ugyan azt a jelet hasznalni rajuk; mert az ezgyik magabol a strukturabol jonne, a masik meg egy kvantortol fuggene, amik tok mas dolgok

Mondjuk, hogy egy \{F,G\}\vDash H alakú következményt szeretnénk igazolni elsőrendű rezolúcióval úgy, hogy az F, G és \neg H formulákat külön-külön zárt Skolem alakra hozzuk és a kapott összes klózt egy \Sigma klózhalmazba tesszük, ezen indítjuk el az elsőrendű rezolúció algoritmusát.
Igaz-e, hogy ügyelnünk kell arra, hogy ha F-ben is és G-ben is szerepel lekötve egy x változó, akkor a kiigazítás során ezek is más-más nevet kapjanak, pl. az egyik formulában x_1-et, a másikban x_2-t, különben nem lesz helyes az algoritmus? Válaszod indokold!
    nem kell, mert a rezolvenskepzes egyik lepese az atnevezgetes, ahol atnevezzuk a dolgokat, nem feltetlenul kell azt hamarabb megcsinalni

7.
prolog
Milyen input L listára és X objektumra lesz igaz az alábbi p(L,X) predikátum? (Hint: Ha nincs ötleted, próbáld meg pl. a p([1,4,2],4) predikátumot kiértékelni!)
\begin{align} p(~[X|T],~X~)&.\\ p(~[X|T],~Y~)&:-~p(T,Y). \end{align}
    vegigmegy az elemeken, es megnezi hogy benne van e a listaban a keresett elem
    addig csapkodja le a fejet a listanak, ameddig a fej az a keresett elem, amikor is igazat ad vissza, ha sehol sincs benne akkor hamisat

p([ X|[] ], X).
p([H|T], X) :- p(T, X).
    lefejti az osszes fejet es megnezi hogy a megadott X-e a lista utolso eleme

Milyen input L1 és L2 listára lesz igaz az alábbi p(L1,L2) predikátum? (Hint: Ha nincs ötleted, próbáld meg pl. a p([1,4,2],[1,2]) predikátumot kiértékelni!)
\begin{align} p(~[~],~[~]~)&.\\p(~[X|T1],~[X|T2]~)&:-~p(T1,T2).\\ p(~[X|T1],~L2~)&:-~p(T1,L2). \end{align}
    akkor ad vissza igazat, ha a elso listaban benne vannak a masoduk lista elemei, a megfelelo sorrendben
    mert addig csapkodja le az elso lista fejeit, ameddig nem egyeznek meg a fejeik a listaknak, amikor is mindkettorol lecsapja a fejeket, ha mindketto kiurul, akkor igazat ad vissza, mert jo sorrendben voltak, es le birta capkodni a fejeket, es eljut ures listakig, ha nem jut el addig hogy mind2 ures, akkor vagy rossz a 2. listaban a sorrend, vagy van benne olyan ami nincsen az elso listaban

Ha az e/2 predikátum egy gráf éleit reprezentálja tényállítások formájában, akkor az alábbi Prolog programban mikor lesz igaz a p(x,y) az x,y csúcsokra?
\begin{align*} p(x,y) &:- ~ e(x,y).\\ p(x,y) &:- ~ p(x,z),~e(z,y). \end{align*}
    ha vezet ut x bol y ba, egy vagy tobb csucson keresztul
    ha van egybol el x,y kozott, akkor egybol igazat ad az elso szabalyr illesztve, ha nem, akkor addig illeszti a masodikat ameddig nincs meg az ut
    a masodik sorban meg megnezi hogy van e ut x bol z be, ha van akkor megnezi hogy z bol megy e y ba,  ha van akkor z-n keresztul megy ut x es y kozott; es ez az x es z kozotti teszt ez rekurziv, addig nezi ameggig nincs meg az ut


masodrendu
Ha az elsőrendű nyelvünkben e/2 és =/2 a predikátumok, függvényjelek pedig csak konstansok vannak (azaz: a struktúrák, melyekről beszélünk, irányított gráfok), írd körül, milyen gráfokra igaz az alábbi másodrendű logikai formula!
\exists X\bigl( \forall x\forall y(e(x,y)\to (X(x)\leftrightarrow\neg X(y))) \bigr)
Itt X/1 másodrendű változó, x,y pedig elsőrendűek.
    az osszes x,y el kozul, mindnek pontosan az egyik vegpontja van benne a halmazban, like a paros graf

A természetes számok standard struktúrájában  mit jelent az alábbi másodrendű formula, ahol X egy egyváltozós predikátumváltozó? Igaz-e ez a formula abban a struktúrában?
\forall X\Bigl(\exists x\bigl((X(x)\wedge\forall y(X(y)\to\neg (y<x)))\bigl)\Bigr)
    ez azt modja, hogy ha x benne van, es y is benne van, akkor y nem kisebb mint az x (szoval nagyobb vagy egyenlo), tehat x a legkisebb elem, es ez igaz a termeszetes szamokra, mert azok kozott van legkisebb szam; mondjuk ha eppen az egesz N_0 van benne azzor az x az a legkisebb, a 0; de ha akarmien reszhalmazunk van, akkor is mindig lesz legkisebb elem, pl {6, 9} ben a legkisebb elem a 6

A természetes számok standard struktúrájában  mit jelent az alábbi másodrendű formula, ahol X egy egyváltozós predikátumváltozó? Igaz-e ez a formula abban a struktúrában?
\forall X\Bigl(\bigl(X(0)\wedge X(0')~\wedge~\forall x(X(x)\to X(x''))\bigr)~\to~\forall x X(x)\Bigr)
    ez azt modja, hogy ha benne van a 0, es benne van az 1 (0') es ha barmely szam benne van akkor a barmely szam +2 is benne van, akkor minden szam benne van; ez igaz is a termeszetes szamokon, mert van benne 0, van benne 1, es ebbol mar mindden benne van mert 0+2=2, 2+2=4, 4+2=6 ... is benne van, szoval minden paros szam, meg 1+2=3, 3+2=5, 5+2=7 ... is benne van, szoval minden paratlan szam, es ha benne van minden paros, paratlan, 0 es 1 szam akkor az az egesz N_0, szoval minden benne van igy igaz az N_0 ra

8.
Magyarázd el (saját szavaiddal), hogy mit jelent a Floyd-Hoare kalkulus alábbi következtetési szabálya, és miért helyes következtetési szabály!
\frac{\{F\}P_1\{G\},~~\{G\}P_2\{H\}}{\{F\}P_1;P_2\{H\}}
    ez a kompozicio; az ilyen az olyan, hogy ha a felso resz bal oldala igaz, es a felso resz jobb oldala is igaz, akkor az also resz is igaz
    szoval, ha igaz az F, es a P_1 lefut es igaz a G, vagy a P_1 vegtelen csiklusba esik; ES ha igaz a G, es P_2 lefut es igaz a H, vagy P_2 vegtelen csiklusba esik; AKKOR ha igaz az F, akkor futtatjuk P_1 et es P_2 -t, barmelyik eshet vegtelen ciklusba, vagy ha mind2 lefut, akkor a fenti resszbeol tudjuk hogy igaz a H
    ebbol azt tudjuk, hogy ha a fenti reszben mindten okesan lefut, akkor az also sorban is tuti mindten okes lesz, mert ha igaz az F, ami kell ugye a felso sorhoz is, akkor megyunk tovabb a P_1 el, meg ha az is lefut, akkor a P_2 vel is, es ha azok is lefutnak, es mivel eljutottunk idaig le fognak, akkor igaz lesz a H lent is, ebbol pedig latszik, hogy ez egy helyes kovetkeztetesi szabaly

ertekadas  {F[x/t]} x := t {F}  + ennek egy vonal van a tetejen
    ha x helyere t -t helyettesitunk a strukturankban, akkor az pont ugyan az mint amikor x et beallitjuk t -re, a helyettesitese lemma alalpjan a modositott strukturaban kiertekeljuk az F-et, akkor az igaz lesz, ez helyes kovetkeztetesi szabaly

felteteles utasitas
{F es r} P_1 {G}  {F es nem r} P_2 {G}
--------------------------------------
{F} if r then P_1 else P_2 {G}
    felso sor: csak akkor futtatjuk a P_1 et, ha igaz az F es az r feltetel is, es csak a P_2 -t akkor ha igaz az F, es hamis az r feltetel, es mindkeet esetben igaz lesz a G
    also sor: ha igaz az F, akkor nezzuk csak a feltetelt, ha igaz a feltel akkor a P_1 et futtatjuk, ha nem igaz akkor a P_2 -t, es a vegen a G mind2 esetben igaz lesz, igy ez helyes kovetekeztetesi szabaly

pipa [x>3]~\textsf{while}~(x<0)~\textsf{do}~x:=x-1~[x=x]
pipa \{10\leq x~\wedge~x\leq 15\}~\textsf{while}~(x\neq8)~\textsf{do}~x:=x-2~\{x\leq 16\}
pipa \{x>0\}~\textsf{while}~(x>1)~\textsf{do}~x:=x+1~\{x=1\}
pipa \{x>0\}~\textsf{while}~(x\neq 0)~\textsf{do}~x:=x-3~\{x=0\}
pipa \{x>0\}~\textsf{while}~(x\neq x)~\textsf{do}~x:=x+1~\{\uparrow\}
pipa \{x>2\}~\textsf{while}~(x\neq 1)~\textsf{do}~x:=x+1~\{x=1\}
iksz [10\leq x~\wedge~x\leq 15]~\textsf{while}~(x\neq8)~\textsf{do}~x:=x-2~[x\leq 16]
iksz [5\leq x~\wedge~x\leq 8]~\textsf{while}~(x\neq 3)~\textsf{do}~x:=x-2~[x\leq 10]
iksz [x<20]~\textsf{while}~(x\neq 3)~\textsf{do}~x:=x-4~[x<20]
iksz [x\geq 7]~\textsf{if}~(x>10)~\textsf{then}~x:=x+3~\textsf{else}~x:=x+3~[x=x+3]
iksz [x\leq 10]~\textsf{if}~(x=15)~\textsf{then}~x:=x+1~\textsf{else}~x:=x-1~[x=x-1]
iksz \{x>10\}~\textsf{if}~(x<5)~\textsf{then}~x:=x+1~\textsf{else}~x:=x+2~\{x=x+2\}


pipa \exists x\forall y(x\times y=x)~\in~\mathrm{Th}(\mathcal{Z})
pipa \mathcal{N}~\vDash~\exists x\forall y\bigl(x+y=y'\bigr)
pipa \mathcal{N}~\vDash~\exists x\forall y\bigl(~x+x\leq y~\bigr)
pipa \mathcal{N}~\vDash~\forall x\exists y\bigl(x+y=x+x'\bigr)
pipa \mathcal{N}~\vDash~\forall x\exists y\bigl(~x'=y'~\bigr)
pipa \mathcal{Z}~\vDash~\forall x\forall y\exists z(x+z=y)
iksz \exists x\forall y\bigl(x'\neq y'\bigr)~\in~\mathrm{Th}(\mathcal{N})
iksz \exists x\forall y\forall z\bigl(x+y=z\bigr)~\in~\mathrm{Th}(\mathcal{N})
iksz \mathcal{N}~\vDash~\forall x\exists y\bigl(x=y+y\bigr)
iksz \mathcal{N}~\vDash~\forall x\exists y\forall z\bigl(~y+z\leq x~\bigr)
iksz \mathcal{Z}~\vDash~\forall x\exists z\forall y(x+z=y)
iksz \mathcal{Z}~\vDash~\forall x\forall y\exists z(x\times z=y)

