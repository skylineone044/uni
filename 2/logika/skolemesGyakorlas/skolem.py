(¬(¬∃x1q(f(x1),y) ∨ ∃z2(q(f(x),c) ∧ p(f(x)))) ∧ ∀z3∃x4(¬∀y5p(f(x4)) v ¬q(f(y),y)))
 ∃x1 ∀z2 ∀z3 ∃x4 ∃y5 (¬(¬q(f(x1),y) ∨ (q(f(x),c) ∧ p(f(x)))) ∧ (¬p(f(x4)) v ¬q(f(y),y)))
    ∃x1 helyett h1
    ∃x4 helyett h2(z2, z3)
    ∃y5 helyett h3(z2, z3)

    y helyett cy
    x helyett cx
    c marad c
 ∀z2 ∀z3 (¬(¬q(f(h1),cy) ∨ (q(f(cx),c) ∧ p(f(cx)))) ∧ (¬p(f(h2(z2, z3))) v ¬q(f(cy),cy)))
