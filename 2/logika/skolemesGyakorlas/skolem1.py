(∃z((q(c,g(x,x)) ∧ ∀xp(g(x,z))) ∨ p(g(z,x))) ∨ ¬((∀xp(g(c,x)) → ∀yq(y,g(y,x))) ∨ (p(g(x,z)) ∨ ∀xp(x))))
(∃z1((q(c,g(x,x)) ∧ ∀x2p(g(x2,z1))) ∨ p(g(z1,x))) ∨ ¬((¬∀x3p(g(c,x3)) v ∀yq(y4,g(y4,x))) ∨ (p(g(x,z)) ∨ ∀x5p(x5))))
 ∃z1 ∀x2 ∀x3 ∃y4 ∃x5 (((q(c,g(x,x)) ∧ p(g(x2,z1))) ∨ p(g(z1,x))) ∨ ¬((¬p(g(c,x3)) v q(y4,g(y4,x))) ∨ (p(g(x,z)) ∨ p(x5))))
    ∃z1 helyett h1
    ∃y4 helyett h2(x2, x3)
    ∃x5 helyett h3(x2, x3)

    c marad c
    x helyett cx
    z helyett cz

 ∀x2 ∀x3 (((q(c,g(cx,cx)) ∧ p(g(x2,h1))) ∨ p(g(h1,cx))) ∨ ¬((¬p(g(c,x3)) v q(h2(x2, x3),g(h2(x2, x3),cx))) ∨ (p(g(cx,cz)) ∨ p(h3(x2, x3)))))
