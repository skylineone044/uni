(¬∀y1(p(x) ∨ ¬∀x2q(y1,y1)) v (¬q(z,c) ∧ ∃x3(∀y4p(g(y4,z)) ∨ ∃y5p(g(y5,z)))))
 ∃y1 ∀x2 ∃x3 ∀y4 ∃y5 (¬(p(x) ∨ ¬q(y1,y1)) v (¬q(z,c) ∧ (p(g(y4,z)) ∨ p(g(y5,z)))))
    ∃y1 helyett h1
    ∃x3 helyett h2(x2)
    ∃y5 helyett h3(x2, y4)

    x helyett cx
    z helyett cz
    c marad c

  ∀x2  ∀y4 (¬(p(cx) ∨ ¬q(h1,h1)) v (¬q(cz,c) ∧ (p(g(y4,cz)) ∨ p(g(h3(x2, y4),cz)))))
