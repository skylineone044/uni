(¬((∃zq(g(z,z),z) → ∃xq(g(y,z),x)) ∨ ∃y∀zp(z)) ∨ ∃yp(c))
(¬((¬∃z1q(g(z1,z1),z1) v ∃x2q(g(y,z),x2)) ∨ ∃y3∀z4p(z4)) ∨ ∃y5p(c))
 ∃z1 ∀x2 ∀y3 ∃z4 ∃y5 (¬((¬q(g(z1,z1),z1) v q(g(y,z),x2)) ∨ p(z4)) ∨ p(c))
    ∃z1 helyett h1
    ∃z4 helyett h2(x2, y3)
    ∃y5 helyett h3(x2, y3)

    y helyett cy
    z helyett cz
    c marad c
 ∀x2 ∀y3 (¬((¬q(g(h1,h1),h1) v q(g(cy,cz),x2)) ∨ p(h2(x2, y3))) ∨ p(c))
