## Tétel.A standard feladatnakakkorés csak akkorlétezik lehetségesmegoldása, ha w=0 a hozzá felírt segédfeladat optimuma.

## Tétel.Tetszőleges standard alakú lineáris programozási feladatrateljesülnek az alábbiállítások
    - Ha nincs optimális megoldása, akkor vagy nem korlátos vagy nincslehetséges megoldása.
    - Ha van lehetséges megoldása, akkor van lehetséges bázismegoldása is.
    - Ha van optimális megoldása, akkor van optimális bázismegoldása is.

## A kétfázisú szimplex módszer
    A Lineáris Programozás alaptétele
    Optimális megoldások számossága
    A szimplex algoritmus sebessége
    Szimplex módszer : kétfázisú algoritmus
    ### 1. fázis lépései
        - Ha a standard feladat szótárának bázismegoldása lehetségesmegoldás, akkor jöhet a 2. fázis
        - Ha nem, akkor társítsuk a segédfeladatot, és készítsük el annakátalakított szótárát
        - Oldjuk meg az átalakított szótárból indulva a segédfeladatot
        - Ha a segédfeladat optimuma<0, >akkor nincs 2. fázis, a standardfeladatnak nem létezik megoldása
        - Ha a segédfeladat optimuma 0, akkor készítsünk egy a kiindulásifeladat szótárával ekvivalens, lehetséges bázismegoldású szótárat az 1. fázisban futtatott szimplex algoritmus utolsó szótára alapján
    ### 2. fázis lépései
        - Hajtsuk végre a szimplex algoritmust az első fázisból kapott szótárból indulva

## Legnagyobb növekmény szabállyal
    (minden iterációs lépésben az a nembázis változót választjuk, melynek bázisba lépésével a leginkább nő a célfüggvény) is exponenciálisan sok iteráció kellhet (R. Jeroszlov,1973)

## Geomotriai alapfogalmak
    - ***Pont***: egy x∈En vektor
    - ***Lehetséges megoldások***↔Pontok az En térben
    - x1,x2 ∈ En ***különböző pontokat összekötő szakasz***:{x:x∈En, x=λx1+ (1−λ)x2}, ahol λ∈[0,1] tetszőleges
    - x1,x2 végpontú ***szakasz felezőpontja***:12x1+12x2 pont
    - Ponthalmaz ***csúcspontja*** : olyan pont, amely nem áll elő egyetlenponthalmazbeli szakasz felezőpontjaként sem
    - ***n-dimenziós sík*** x:x ∈ En, a1x1+a2x2+...+anxn=b},ahola1,a2,...,an,b∈ Rrögzített számok
    - ***n-dimenziós zárt féltér***: {x:x ∈ En, a1x1+a2x2+...+anxn≤b},ahola1,a2,...,an,b∈ Rrögzített számok
    - ***Lineáris feltételek*** ↔ Zárt félterek (‘≤’) és síkok (‘=’)
    - ***Lehetséges megoldások halmaza*** ↔ Zárt félterek (és síkok) metszete
    ### Konvex poliederek
        - ***Konvex ponthalmaz***: olyan ponthalmaz, amely tartalmazza bármely kétpontját összekötő szakasz pontjait is
        - ***Zárt ponthalmaz***: olyan ponthalmaz, amely tartalmazza a pontjaibólképezhető tetszőleges konvergens sorozat határértékét is
        - ***Korlátos ponthalmaz***: olyan ponthalmaz, amelynek mindenxpontjárateljesül, hogyd(0,x)≤K, ahol K egy rögzített valós szám (inkábbhasználatos : komplementere nyílt)
        - ***Poliéder***: zárt, véges sok csúcsponttal rendelkező ponthalmaz
        - *A lehetséges megoldások halmaza egy konvex poliéder*
        - ***Politóp***: korlátos poliéder

## Allítás.A duál feladat duálisa az eredeti primál feladat.
## A duál feladat megoldásában y∗i a primál (eredeti) feladatierőforrásáhoztartozó ún.marginális ár, vagy más névenárnyék ár.

## Tétel. (Gyenge dualitás)
    - Ha x= (x1, . . . , xn) lehetséges megoldása aprimál feladatnak és y= (y1, . . . , ym) lehetséges megoldása a duálfeladatnak, akkor cTx≤bTy,
      azaz n∑j=1 cj xj ≤ m∑i=1bi yi.
      Vagyis a duális feladat bármely lehetséges megoldása felső korlátot ad aprimál bármely lehetséges megoldására (azaz az optimális megoldásra is).

## Tétel. (Erős dualitás)
    - Ha x∗= (x1, . . . , xn)egy optimális megoldása aprimál feladatnak és y∗= (y1, . . . , ym)optimális megoldása a duálfeladatnak, akkor cTx∗=bTy∗,
      azaz n∑j=1 cj xj= m∑i=1 bi yi.
      Továbbá az is igaz, hogy y∗T(b−Ax∗) = 0 és x∗T(ATy∗−c) = 0.
    - Egyszerűen : ha valamely i-edik feltétel egyenlet nem éles(azaz nincsegyenlőség) a primál optimumban, akkora kapcsolódó duál yi változó 0 kell legyen. Visszafelé, ha egy primálxiváltozó szigorúan pozitív, akkor akapcsolódó duális feltétel egyenlet éles (=) kell legyen.Ezt ***komplementáris lazaságnak*** hívjuk.

## Tucker lehetetlenségi tétele
    egyenlet és egyenlőtlenség rendszerekre.Egyenletek és egyenlőtlenségek egy rendszere ***akkor és csak akkormegoldhatatlan, ha inkonzisztens***

## Tétel. (Komplementáris lazaság)
    - Tegyük fel, hogy xa primál feladatoptimális megoldása. Ekkor
    - Ha y a duál optimálismegoldása, akkor x és y komplementáris
    - Ha y lehetséges megoldása a duálisnak és komplementáris x-szel, akkor y optimálismegoldása a duálnak
    - Létezik olyan lehetséges y megoldása a duálnak, hogy x és y komplementáris.
    ### Osszefoglalva:
        - Adott x(javasolt primál megoldás), ellenőrizzük, hogy lehetséges-e
        - Nézzük meg mely y iváltozóknak kell 0-nak lennie
        - Nézzük meg mely duál feltételeknek kell élesnek lennie → egyenletrendszert kapunk
        - Oldjuk meg ezt a rendszert
        - Ellenőrizzük, hogy a kapott megoldás lehetséges megoldása-e aduálnak
        Ha minden lépés sikeres volt, akkor az adottxoptimális, különben nem.
