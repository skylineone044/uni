#!/usr/bin/awk -f

BEGIN {
    FS=":"
}

$2~/(https:|)www.+\.ru.+\.exe/{
    print $1
}
