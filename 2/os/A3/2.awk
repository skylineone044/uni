#!/usr/bin/awk -f

BEGIN {
    FS=" "
}

{
    honapok[$6] = honapok[$6]" "$9
}

END {
    for (i in honapok) {
        printf i" "
        print honapok[i]
    }

}
