#!/usr/bin/awk -f


BEGIN {
    FS=";"
}

FNR != 0 {
    for (i = 4; i <= NR; ++i) {
        jegyOsszeg[$3] += $i
        jegyekSzama[$3]++

    }
}

END {
    max = 0;
    for (i in jegyOsszeg) {
        if (jegyOsszeg[i] / jegyekSzama[i] > max) {
            max = jegyOsszeg[i] / jegyekSzama[i]
            maxNem = i
        }
    }
    print maxNem

}
