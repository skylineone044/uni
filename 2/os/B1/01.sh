#!/bin/bash

# 1. Írj szkriptet, amely két parancssori paramétert vár. Egy fájl nevét és egy
# számot. A script írja ki az 1. paraméterben megadott fájl, 2. paraméterben
# megadott sorát.

file="$1"
szam="$2"

cat $file | head --lines $szam | tail --lines 1
