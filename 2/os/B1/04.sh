#!/bin/bash

# 4. Írj szkriptet, amely két parancssori paramétert vár. Mindkét paraméter
# egy-egy szövegfájl legyen, melyek első sora egy-egy számot tartalmaz. A szkript
# írja ki ennek a két számnak az összegét a képernyőre.

let a=`cat $1 | head --lines 1`
let b=`cat $2 | head --lines 1`

echo $((a+b))
