#!/bin/bash

# 5. Írj szkriptet, amely két parancssori paramétert vár. Mindkét paraméter
# egy-egy szövegfájl legyen (tobb soros tartalommal). A szkript cserélje
# meg a két fájl első sorait.


let a=`cat $1 | head --lines 1`
let b=`cat $2 | head --lines 1`

let file1=$(tail --lines=+2 "$1")
let file2=$(tail --lines=+2 "$2")

# echo "$b\n$file1"
