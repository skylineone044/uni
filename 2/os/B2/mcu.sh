#!/bin/bash

if [[ $# -ne 2 ]]; then
    echo "Nem ket parameter jott"
    exit 1
fi

if [[ ! -e $1 ]]; then
    echo "no file"
    exit 2
fi

for filenev in *.kiterjeztes ; do
    ...
done

sorok=$(cat $1 | wc -l)
osszeg=0

for (( i = 2; i <= $sorok; i++ )); do
    sor=$(cat $1 | head -$i | tail -1)
    nem=$(echo $sor | cut -d "," -f 2)
    megjelenesek=$(echo $sor | cut -d "," -f 3)
    # echo $sor
    # echo $nem" "$megjelenesek

    if [[ $nem == $2 ]]; then
        # echo $megjelenesek
        let osszeg=$osszeg+$megjelenesek
    fi
done
echo "a megjelenesek szama: "$osszeg
