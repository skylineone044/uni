#!/bin/bash

# A parameterben erkezo fajl tartalmat kiirjuk, majd azokat a sorokat tartjuk meg, amelyekben szerepel az 'Akcio' string
#cat $1 | egrep 'Akcio'

# Csak a jatekok neveit es ertekeleseit kell megtartani, ezek az 1. es 4. oszlopok
#cat $1 | egrep 'Akcio' | cut -d ';' -f 1,4 

# Abece szerint rendezzuk a sorokat
#cat $1 | egrep 'Akcio' | cut -d ';' -f 1,4 | sort

# Majd eltavolitjuk az ismetlodo sorokat
cat $1 | egrep 'Akcio' | cut -d ';' -f 1,4 | sort | uniq
