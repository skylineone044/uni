#!/bin/bash

# letrehozunk ket valtozot amelyben eltaroljuk a paros szamok osszeget es a paratlan szamok szorzatat
osszeg=0
szorzat=1

# for ciklussal bejarjuk elemenkent a parancssorban megadott szamokat
for i in $* ;do
	  # ellenorizzuk, hogy az aktualis szam paros-e (azaz 2-vel osztva 0-t ad-e maradekul)
    	if (( $i % 2 == 0 )); then
    		# ha paros akkor noveljuk az osszeget
        	let osszeg=osszeg+i
    	else
    		# ha paratlan akkor a szorzatot
        	let szorzat=szorzat*i
    	fi
done

# kiirjuk az eredmenyt
echo $osszeg
echo $szorzat
