#!/bin/bash

/bin/ls -1 $1 | cut -d "." -f 1 | sort | uniq | wc -l
