#!/bin/bash

if [[ ! -d $1 ]]; then
    echo "Hiba"
    exit 1
fi

let leghosszabb=0
# let filenev=""
cd $1

for file in $(ls *.txt) ; do
    let hossz=$(cat $file | wc -l)
    if [[ $leghosszabb -lt $hossz ]]; then
        let leghosszabb=$hossz
        filenev="$file"
    fi
done

echo $filenev
