#!/bin/bash

cd $1

for file in $(ls *.c) ; do
    exitcode=$(cat $file |  grep -c "int main")
    if [[ $exitcode -eq 0 ]]; then
        echo "$file"
    fi
done
