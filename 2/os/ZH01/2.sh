#!/bin/bash

let couter=0

for file in $(ls $1/*.doc); do
    if [[ couter -lt 2 ]]; then
        mv $file "$file"x
        couter=$((couter + 1))
    fi
done
