#!/bin/bash

if [[ ! -d $1 ]]; then
    echo "Nem megfelelo parameter!"
    exit 2
fi

cd $1

for file in $( find . -type f) ; do
    let tart=$(echo $file | grep "20210324" -c)
    # echo $file
    # echo $tart
    if [[ $tart -gt 0 ]]; then
        mv $file zh/
    fi
done

cd ..
