#!/usr/bin/awk -f

# a feladatszoveg ";"-t irt, de a peldakimenetben ":" volt, megkerdeztem hogy mi van,
# es elvileg a ":" a jo a regex kozepen, szoval azt hagytam benne
#
$0~/^([1-9][a-z][A-Z0-9]:){2,3}[01]{4}$/{
    print $0
}
