#!/usr/bin/awk -f

BEGIN {
    FS="//"
    }

$3~/[A-Z]{3}-[0-9]{3}/{
    print $3
    km_ek += $4
    autok++
}

END {
    print "\nA megtett kilometerek atlaga: "km_ek / autok
}
