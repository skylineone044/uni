#!/usr/bin/awk -f
BEGIN {
    FS=","
}

FNR != 0 {
    megyenkenti_osszlakossag[$3] += $5
    megyenkenti_telepulesek[$3]++
}

END {
    for (megye in megyenkenti_osszlakossag) {
        print megye" "megyenkenti_osszlakossag[megye]" "megyenkenti_osszlakossag[megye] / megyenkenti_telepulesek[megye]
    }
}
