import java.text.DecimalFormat;

public class Complex {
    private final double real;
    private final double imag;

    public double getReal() {
        return real;
    }

    public double getImag() {
        return imag;
    }

    public Complex(double real, double imag) {
        this.real = real;
        this.imag = imag;
    }

    @Override
    public String toString() {
        // DecimalFormat format = new DecimalFormat("0.#");
        return String.format("%s+%si", Double.toString(this.real), Double.toString(this.imag));
        // " {real} + {imag}i"
    }

    public Complex add(Complex a) {
        return new Complex(this.real + a.real, this.imag + a.imag);
    }

    public Complex mul(Complex a) {
        return new Complex((this.real * a.real) - (this.imag * a.imag), (this.real * a.imag) + (this.imag * a.real));
    }
}
