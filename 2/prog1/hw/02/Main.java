
public class Main {
    public static void main(String[] args) {
        Complex a = new Complex(5.5, 3);
        Complex b = new Complex(10, 9.17);

        System.out.println(a.add(b).toString());
        System.out.println(a.mul(b).toString());
        System.out.println(String.format("%f, %f", a.getReal(), b.getImag()));
    }
}
