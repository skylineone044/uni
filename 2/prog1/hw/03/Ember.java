import jdk.jfr.Unsigned;

public class Ember {
    protected String nev;
    protected int eletEro;

    public void setEletEro(int eletEro) {
        if (eletEro < 0) {
            this.eletEro = 0;
        } else {
            this.eletEro = eletEro;
        }
    }

    public int getEletEro() {
        return eletEro;
    }

    public String getNev() {
        return nev;
    }

    public Ember() {
        this.nev = "ismeretlen";
        this.eletEro = 10;
    }

    public Ember(String nev, int eletEro) {
        this.nev = nev;
        this.eletEro = (eletEro < 0 ? -eletEro : eletEro);
    }

    @Override
    public String toString() {
        String allapot = "";
        if (this.eletEro > 10) {
            allapot = "majd kicsattan az egeszsegtol";
        } else if (1 <= this.eletEro && this.eletEro <= 10) {
            allapot = "atlagos az allapota";
        } else if (this.eletEro == 0) {
            allapot = "halott";
        }
        return "Emberunk neve " + this.nev + ", es jelenleg " + allapot + ".";
    }

    public boolean vajonElMeg() {
        return this.eletEro > 0;
    }

    public void gyogyul(int gyogyulMennyiseg) {
        if (vajonElMeg()) {
            this.eletEro += gyogyulMennyiseg;
        } else {
            System.err.println("sajnalom, elkestetek");
        }
    }

}
