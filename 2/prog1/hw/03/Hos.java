public class Hos extends Ember {
    private int tamadas;
    private int sikerSzam;

    public int getTamadas() {
        return tamadas;
    }

    public int getSikerSzam() {
        return sikerSzam;
    }

    public void setSikerSzam(int sikerSzam) {
        if (sikerSzam < this.sikerSzam) {
            return;
        } else {
            this.sikerSzam = sikerSzam;
        }
    }

    public Hos(String nev, int eletEro, int tamadas, int sikerSzam) {
        super(nev, eletEro);
        this.tamadas = Math.max(0, tamadas);
        this.sikerSzam = Math.max(0, sikerSzam);
    }

    @Override
    public String toString() {
        String allapot = "";
        if (this.eletEro > 10) {
            allapot = "majd kicsattan az egeszsegtol";
        } else if (1 <= this.eletEro && this.eletEro <= 10) {
            allapot = "atlagos az allapota";
        } else if (this.eletEro == 0) {
            allapot = "halott";
        }
        return "Emberunk neve " + this.nev + ", es jelenleg " + allapot +
               ". Ez az ember egy sarkanyolo hos, tamadasa " + this.tamadas +
               ", es eddig " + sikerSzam + " darab sarkanyt olt meg.";
    }

    @Override
    public void gyogyul(int mennyivel) {
        this.eletEro += mennyivel;
    }

    public void edzes() {
        tamadas += (super.vajonElMeg() ? 1 : 0);
    }
}
