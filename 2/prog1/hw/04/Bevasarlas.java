public class Bevasarlas extends Teendo {
    private String miket;
    private int maxOsszeg;

    public String getMiket() {
        return miket;
    }

    public int getMaxOsszeg() {
        return maxOsszeg;
    }

    public void setMiket(String miket) {
        this.miket = miket;
    }

    public void setMaxOsszeg(int maxOsszeg) {
        this.maxOsszeg = maxOsszeg;
    }

    public Bevasarlas(String ido, String miket, int maxOsszeg) {
        super("Bevasarlas", ido, 3);
        setMiket(miket);
        setMaxOsszeg(maxOsszeg);
    }

    public void frissit(String ujelem) {
        if ("<torol>".equals(ujelem)) {
            setMiket("");
            setMaxOsszeg(0);
        } else {
            setMiket(getMiket() + ", " + ujelem);
            setMaxOsszeg(getMaxOsszeg() + 1000);
        }
    }

    @Override
    public void setPrioritas(int prioritas) {
        super.setPrioritas(prioritas);
        return;
    }

    @Override
    public String toString() {
        return "Bevasarlas. Termekek: " + getMiket() + ", tervezett osszeg: " +
                getMaxOsszeg() + " Ft";
    }
}
