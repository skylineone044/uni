public class Megbeszeles extends Teendo {
    private String kivel;
    private String hol;
    public static int MEGBESZELES_DARAB = 0;

    {
        MEGBESZELES_DARAB++;
    }

    public String getKivel() {
        return kivel;
    }

    public String getHol() {
        return hol;
    }

    public void setKivel(String kivel) {
        this.kivel = kivel;
    }

    public void setHol(String hol) {
        this.hol = hol;
    }

    public Megbeszeles(String ido, String kivel, String hol) {
        super("Megbeszeles", ido, 1);
        setKivel(kivel);
        setHol(hol);
    }

    @Override
    public void setPrioritas(int prioritas) {
        super.setPrioritas(prioritas);
        return;
    }

    @Override
    public String toString() {
        return "Megbeszeles, partner: " + this.kivel + ". Idopont: " +
                this.getIdo() + ". Helyszin: " + this.hol;
    }

}
