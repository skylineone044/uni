public class Teendo {
    private String nev;
    private String ido;
    private int prioritas;
    private boolean teljesitettuk;

    public String getNev() {
        return nev;
    }

    public String getIdo() {
        return ido;
    }

    public int getPrioritas() {
        return prioritas;
    }

    public boolean isTeljesitettuk() {
        return teljesitettuk;
    }

    public void setNev(String nev) {
        this.nev = nev;
    }

    public void setIdo(String ido) {
        this.ido = ido;
    }

    public void setPrioritas(int prioritas) {
        if (1 <= prioritas && prioritas <= 5) {
            this.prioritas = prioritas;
        } else {
            this.prioritas = 5;
            System.err.println("no no");
        }
        if (this instanceof Bevasarlas) {
            this.prioritas = 3;
        } else if (this instanceof Megbeszeles) {
            this.prioritas = 1;
        }
    }

    public void atvalt() {
        this.teljesitettuk = !this.teljesitettuk;
    }

    public Teendo() {
        this.nev = "URES TEENDO";
        this.ido = "IDO";
        this.prioritas = 1;
        this.teljesitettuk = false;
    }

    public Teendo(String nev, String ido, int prioritas) {
        this.setNev(nev);
        this.setIdo(ido);
        setPrioritas(prioritas);
        this.teljesitettuk = false;
    }

    @Override
    public String toString() {
        return "Teendo neve: " + this.nev + ", ideje: " + this.ido + ", prioritasa: "
                + this.prioritas + ", teljesitettuk: " +
                (this.teljesitettuk ? "igen" : "nem");
    }

    public static Teendo legfontosabb(Teendo[] lista) {
        if (lista.length == 0) {
            return null;
        }

        Teendo eddigi_legfontosabb = lista[0];

        for (int i = lista.length - 1; i >= 0; i--) {
            if (lista[i].getPrioritas() <= eddigi_legfontosabb.getPrioritas()) {
                eddigi_legfontosabb = lista[i];
            }
        }
        return eddigi_legfontosabb;
    }

}
