import java.util.*;
import java.util.Map.Entry;

public class BestPriceFinder {
    private Map<String, String> bestBuys;

    public BestPriceFinder() {
        this.bestBuys = new HashMap<String, String>();
    }

    public void addProduct(Product product, String shop) {
        this.bestBuys.put(product.getName(), shop);
    }

    public String getShopFor(String productName) {
        return bestBuys.get(productName);
    }

    public void printBestBuys() {
        for (Entry<String, String> elem : this.bestBuys.entrySet()) {
            System.out.println("Buy " + elem.getKey() + " at " + elem.getValue() + ".");
        }
    }

    public int deleteShop(String shop) {
        int counter = 0;
        // Iterator it = bestBuys.entrySet().iterator();
        Iterator<Map.Entry<String, String>> it = this.bestBuys.entrySet().iterator();
        while (it.hasNext()) {
            Entry elem = (Entry<String, String>) it.next();
            if (elem.getValue().equals(shop)) {
                it.remove();
                counter++;
            }
        }
        return counter;
    }
}
