import java.util.*;
import java.util.Map.Entry;

public class ExtremeShopping {
    private Map<String, List<Product>> extremeList;

    public ExtremeShopping() {
        extremeList = new HashMap<String, List<Product>>();
    }

    public void addShoppingList(String shop, List<Product> products) {
        extremeList.put(shop, products);
    }

    public void printShoppingLists() {
        for (Entry<String, List<Product>> elem : this.extremeList.entrySet()) {
            System.out.println(elem.getKey());
            // for (Product product : elem.getValue()) {
            //     System.out.println(product.name);
            // }
            List<String> names = new ArrayList<String>();
            for (Product product : elem.getValue()) {
                names.add(product.getName());
            }
            String result = String.join(" ", names);
            System.out.println(result);
        }
    }

}
