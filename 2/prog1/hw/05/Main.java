public class Main {
    public static void main(String[] args) {
        Product p = new Product("banana", 69, true);
        Product pp = new Product("apple", 420, false);
        System.out.println(p.getName() + " " + p.getAmount() + " " + p.isImportant());
        System.out.println(pp.getName() + " " + pp.getAmount() + " " + pp.isImportant());

        BestPriceFinder bpf = new BestPriceFinder();
        bpf.addProduct(p, "aldi");
        bpf.addProduct(pp, "tesco");

        bpf.printBestBuys();
        System.out.println(bpf.deleteShop("aldi"));
        bpf.printBestBuys();
    }
}
