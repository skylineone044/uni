import java.util.*;

public class ShoppingList {
    private List<Product> items;

    public ShoppingList() {
        this.items = new ArrayList<Product>();
    }

    public void addProduct(Product product) {
        items.add(product);
    }

    public int countProducts() {
        return items.size();
    }

    public Product getProduct(int szam) {
        try {
            return items.get(szam);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public void printProducts() {
        for (Product elem : items) {
            System.out.println(elem.getName());
        }
    }

    public void delete(Product product) {
        Iterator<Product> it = items.iterator();
        while (it.hasNext()) {
            Product elem = it.next();
            // if (elem.name.equals(product.name) && elem.amount == product.amount &&
            // elem.important == product.important) {
            // it.remove();
            // }
            if (elem == product) {
                it.remove();
            }
        }
    }

    public int deleteUnimportant() {
        int counter = 0;
        Iterator<Product> it = items.iterator();
        while (it.hasNext()) {
            Product elem = it.next();
            // if (elem.name.equals(product.name) && elem.amount == product.amount &&
            // elem.important == product.important) {
            // it.remove();
            // }
            if (!elem.isImportant()) {
                counter++;
                it.remove();
            }
        }
        return counter;
    }
}
