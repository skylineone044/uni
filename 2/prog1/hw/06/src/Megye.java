import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class Megye {
    private String nev;
    private List<Telepules> telepulesek;
    private String web;

    public Megye(String nev) {
        this.nev = nev;
        telepulesek = new ArrayList<>();
        this.web = "";
    }

    public boolean ujTelepules(String telepulesTemplate) {
        String[] telepulesTemplateArray = telepulesTemplate.split(":");

        Telepules telepules = new Telepules(telepulesTemplateArray[0], Double.parseDouble(telepulesTemplateArray[1]));
        try {
            telepules.emailFrissitese(telepulesTemplateArray[2]);
            telepulesek.add(telepules);
            return true;
        } catch (TelepuleskezeloException e) {
            return false;
        }
    }

    public void webcimFrissites(String ujWebcim) {
        if (ujWebcim.toLowerCase().contains(this.nev.toLowerCase())) {
            this.web = ujWebcim;
        } else {
            throw new IllegalArgumentException("Hibas webcim: " + ujWebcim);
        }
    }

    public void ujLakok(int index, String kerulet, int lakokSzama) {
        if (index < telepulesek.size() && index >= 0) {
            Telepules telepules = telepulesek.get(index);
            try {
                telepules.ujLakok(kerulet, lakokSzama);
            } catch (TelepuleskezeloException e) {
                throw new IllegalArgumentException(telepules.getNev() + " varosban nem letezik a megadott kerulet!", e);
            }

        } else {
            throw new IllegalArgumentException("Nem letezik a megadott indexu varos!");
        }
    }

    public int keres(String keresettSzoveg) {
        int szamlalo = 0;
        for (Telepules telepules : telepulesek) {
            if (telepules.getNev().contains(keresettSzoveg)) {
                szamlalo++;
            }
        }
        return szamlalo;
    }

    public int lakossag() {
        int osszeg = 0;
        for (Telepules telepules : telepulesek) {
            osszeg += telepules.getLakosokSzama();
        }
        return osszeg;
    }

    @Override
    public String toString() {
        return this.nev + " megye (" + this.web + ")";
    }
}

