import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class Telepules {
    private String nev;
    private List<Kerulet> keruletek;
    private double terulet;
    private String email;

    public Telepules(String nev, double terulet) {
        if (Pattern.matches("^[A-Z].*", nev)) {
            this.nev = nev;
        } else {
            throw new IllegalArgumentException("Hibas varosnev: " + nev);
        }
        this.keruletek = new ArrayList<Kerulet>();
        setTerulet(terulet);
    }

    public String getNev() {
        return nev;
    }

    public double getTerulet() {
        return terulet;
    }

    public void setTerulet(double terulet) {
        if (terulet > 0) {
            this.terulet = terulet;
        } else {
            this.terulet = 1;
        }
    }

    public String getEmail() {
        return email;
    }

    public void emailFrissitese(String ujEmail) throws TelepuleskezeloException {
        if (Pattern.matches("^info[^@]*@[^@]*\\.hu$", ujEmail)) {
            this.email = ujEmail;
        } else {
            throw new TelepuleskezeloException(this, "Hibas e-mail cim: " + ujEmail);
        }
    }

    public void ujKerulet(String nev, int szam) {
        this.keruletek.add(new Kerulet(nev, szam));
    }

    public void ujLakok(String nev, int szam) throws TelepuleskezeloException {
        for (Kerulet kerulet : this.keruletek) {
            if (nev.equalsIgnoreCase(kerulet.getNev())) {
                kerulet.lakosokSzama += szam;
                return;
            }
        }
        throw new TelepuleskezeloException(this, "Nem talalhato a megadott kerulet: " + nev);
    }

    public int getLakosokSzama() {
        int osszeg = 0;
        for (Kerulet kerulet : this.keruletek) {
            osszeg += kerulet.getLakosokSzama();
        }
        return osszeg;
    }

    public double nepsuruseg() {
        return getLakosokSzama() / this.terulet;
    }

    public class Kerulet {
        private final String nev;
        private int lakosokSzama;

        private Kerulet(String nev, int lakosokSzama) {
            this.nev = nev;
            setLakosokSzama(lakosokSzama);
        }

        public String getNev() {
            return nev;
        }

        public int getLakosokSzama() {
            return lakosokSzama;
        }

        public void setLakosokSzama(int lakosokSzama) {
            this.lakosokSzama = lakosokSzama;
        }

        public double lakokAranya() {
            return (double) this.lakosokSzama / Telepules.this.getLakosokSzama();
        }

        @Override
        public String toString() {
            return this.nev + " (" + Telepules.this.getNev() + ")";
        }
    }
}
