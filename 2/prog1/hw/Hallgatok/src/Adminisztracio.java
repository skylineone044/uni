import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class Adminisztracio {
    private double legjobbAtlag;
    private String legjobbNeptun;

    public void tomegesKatasztrofa(List<Hallgato> hallgatok, String dir) {
        try {
            Files.createDirectory(Path.of(dir));
            for (Hallgato hallgato : hallgatok) {
                hallgato.katasztrofa(dir + "/" + hallgato.getNeptun() + ".student");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Hallgato> hallgatokatFelvesz(String fileNev) {
        int minimumPont = 0;
        List<Hallgato> resultList = new ArrayList<>();
        try (Scanner sc = new Scanner(new File(fileNev))) {
            while (sc.hasNextLine()) {
                String[] line = sc.nextLine().split("-");

                if ("pontszam".equals(line[0])) {
                    minimumPont = Integer.parseInt(line[1]);
                } else if ("hallgato".equals(line[0])) {
                    if (Integer.parseInt(line[3]) >= minimumPont) {
                        resultList.add(new Hallgato(line[1], line[2]));
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return resultList;
    }

    public void legjobbHallgato(String dir) {
        File[] directoryListing = new File(dir).listFiles();
        if (directoryListing != null) {
            for (File file : directoryListing) {
                if (file.isDirectory()) {
                    legjobbHallgato(file.getPath());
                } else {
                    if ("student".equals(file.getName().split("\\.")[1])) {
                        try (Scanner sc = new Scanner(file)) {
                            double atlag = sc.nextDouble();
                            if (atlag > this.legjobbAtlag) {
                                this.legjobbAtlag = atlag;
                                this.legjobbNeptun = file.getName().split("\\.")[0];
                            }
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        try(Writer writer = new FileWriter("legjobbhallgato.txt")) {
            writer.write(legjobbNeptun + ";" + legjobbAtlag);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
