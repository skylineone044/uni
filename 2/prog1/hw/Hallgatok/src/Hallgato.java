import java.io.*;
import java.util.*;

public final class Hallgato {
    private String nev;
    private String neptun;
    private Map<String, Set<Teljesites>> teljesitesek;

    public String getNev() {
        return nev;
    }

    public String getNeptun() {
        return neptun;
    }

    public void setNeptun(String neptun) {
        if (neptun.matches("^[A-Z0-9]{6}$")) {
            this.neptun = neptun;
        } else {
            throw new IllegalArgumentException("hibas neptun kod");
        }
    }

    public Hallgato(String nev, String neptun) {
        this.nev = nev;
        setNeptun(neptun);
        this.teljesitesek = new HashMap<>();
    }

    public void beiratkozik(String felev) {
        if (teljesitesek.containsKey(felev)) {
            System.err.println("nem lehet tobbszor beiratkozni");
        } else {
            teljesitesek.put(felev, new HashSet<>());
        }
    }
    public void teljesiteseketFelvesz(String fileNev) {
        try (Scanner sc = new Scanner(new File(fileNev))) {
            while (sc.hasNextLine()) {
                String[] line = sc.nextLine().split(";");
                String kurzusFile = line[0];
                int pontszam = Integer.parseInt(line[1]);

                Teljesites teljesites = new Teljesites(Kurzus.beolvas(kurzusFile), pontszam);
                String felevNev = fileNev.substring(0, fileNev.lastIndexOf(".txt"));

                Set set = new HashSet();
                set.add(teljesites);
                teljesitesek.put(felevNev, set);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public int bukas() {
        int szamlalo = 0;
        for (Map.Entry<String, Set<Teljesites>> entry : this.teljesitesek.entrySet()) {
            for (Teljesites teljesites : entry.getValue()) {
                if (teljesites.erdemjegy() == 1) {
                    szamlalo++;
                }
            }
        }
        return szamlalo;
    }

    public void felvettKurzusok(String felev, String filenev) {
        for (Map.Entry<String, Set<Teljesites>> entry : this.teljesitesek.entrySet()) {
            if (entry.getKey().equals(felev)) {
                try (Writer writer = new FileWriter(filenev)) {
                    for (Teljesites teljesites : entry.getValue()) {
                        writer.write(teljesites.kurzus + "\n");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void katasztrofa(String filenev) {
        int bukasok = this.bukas();

        if (bukasok == 0) {
            try (Writer writer = new FileWriter(filenev)) {
                writer.write("mindent teljesitett");
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        boolean toobien = false;
        int legtobbBukas = 0;
        String bukottFelev = "";

        for (Map.Entry<String, Set<Teljesites>> entry : this.teljesitesek.entrySet()) {
            int felevBukasok = 0;
            for (Teljesites teljesites : entry.getValue()) {
                if (teljesites.erdemjegy() == 1) {
                    felevBukasok++;
                }
            }
            if (felevBukasok > legtobbBukas) {
                legtobbBukas = felevBukasok;
                bukottFelev = entry.getKey();
                toobien = false;
            } else if (legtobbBukas == bukasok) {
                toobien = true;
            }
        }

        if (toobien) {
            try (Writer writer = new FileWriter(filenev)) {
                writer.write("tobb ilyen felev is van");
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try (Writer writer = new FileWriter(filenev)) {
                writer.write(bukottFelev);
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public double diplomaatlag() {
        double osszkredit = 0;
        double osszSulyozottJegyek = 0;

        for (Map.Entry<String, Set<Teljesites>> entry : this.teljesitesek.entrySet()) {
            for (Teljesites teljesites : entry.getValue()) {
                osszSulyozottJegyek += teljesites.erdemjegy() * teljesites.kurzus.getKreditertek();
                osszkredit += teljesites.kurzus.getKreditertek();
            }
        }
        return osszSulyozottJegyek / osszkredit;
    }
}
