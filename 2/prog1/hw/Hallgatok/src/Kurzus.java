import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Kurzus {
    private String kurzuskod;
    private String nev;
    private int maxpont;
    private int kreditertek;
    private OsztalyozasiMod osztalyozasiMod;

    public String getKurzuskod() {
        return kurzuskod;
    }

    public String getNev() {
        return nev;
    }

    public int getMaxpont() {
        return maxpont;
    }

    public int getKreditertek() {
        return kreditertek;
    }

    public OsztalyozasiMod getOsztalyozasiMod() {
        return osztalyozasiMod;
    }

    public Kurzus(String kurzuskod, String nev, int maxpont, OsztalyozasiMod osztalyozasiMod, int kreditertek) {
        this.kurzuskod = kurzuskod;
        this.nev = nev;
        this.maxpont = maxpont;
        this.kreditertek = kreditertek;
        this.osztalyozasiMod = osztalyozasiMod;
    }

    public static Kurzus beolvas(String filenev) {
        try(Scanner sc = new Scanner(new File(filenev))) {
            String dataLine = sc.nextLine();
            String osztalyozasFilenev = sc.nextLine();
            String data[] = dataLine.split(";");
            return new Kurzus(data[0], data[1], Integer.parseInt(data[2]), OsztalyozasiMod.beolvas(osztalyozasFilenev), Integer.parseInt(data[3]));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
