import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class OsztalyozasiMod {
    private final int[] erdemjegyek;

    public OsztalyozasiMod(int kettes, int harmas, int negyes, int otos) {
        this.erdemjegyek = new int[4];
        this.erdemjegyek[0] = kettes;
        this.erdemjegyek[1] = harmas;
        this.erdemjegyek[2] = negyes;
        this.erdemjegyek[3] = otos;
    }

    public int erdemjegy(int pontszam) {
        int erdemjegy = 1;
        for (int i = 0; i < this.erdemjegyek.length; i++) {
            if (pontszam >= this.erdemjegyek[i]) {
                erdemjegy = i+2;
            }
        }
        return erdemjegy;
    }

    public static OsztalyozasiMod beolvas(String filenev) {
        try (Scanner sc = new Scanner(new File(filenev))) {
            int kettes = sc.nextInt();
            int harmas = sc.nextInt();
            int neegyes = sc.nextInt();
            int otos = sc.nextInt();
            return new OsztalyozasiMod(kettes, harmas, neegyes, otos);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
