public class HaziFeladatUres {

    /**
     * Készíts egy statikus, `lustalkodas` nevű függvényt.
     * A függvény két paramétert kap: azt, hogy hétköznap van-e, és azt, hogy vakáció van-e.
     * Akkor lustálkodhatunk reggel, ha hétvége van, vagy ha vakáción vagyunk.
     *
     * @param hetkoznap hétköznap-e az adott nap
     * @param vakacio   vakáció van-e
     * @return true, ha lustálkodhatunk
     */
    public static boolean lustalkodas(boolean hetkoznap, boolean vakacio) {
        return false;
    }

    /**
     * Készíts egy statikus, `utolsoSzamjegyEgyenloseg` nevű függvényt. A függvény két számot vár.
     * Térjen vissza igazzal, ha a számok utolsó számjegyei megegyeznek.
     *
     * Tipp: %
     *
     * @param a az egyik szám
     * @param b a másik szám
     * @return true, ha megegyezik-e az utolsó számjegy
     */
    public static boolean utolsoSzamjegyEgyenloseg(int a, int b) {
        return false;
    }

    /**
     * Készíts egy statikus, `utolsoSzamjegyEgyenlosegN` nevű függvényt. A függvény három számot vár (a, b, n).
     * Térjen vissza igazzal, ha az első két szám utolsó n számjegyei megegyeznek (az utolsó 3. paraméter).
     *
     * Például: 99999, 119, 1 paraméterek esetén igazzal kell visszatérni, míg 22214, 9514, 3 esetén hamissal, mivel az utolsó 3 számjegy nem egyezik meg.
     * Tipp: %
     * Tipp: Hatványozni a Math.pow(alap, kitevő) függvénnyel lehet.
     *
     * @param a az egyik szám
     * @param b a másik szám
     * @param n az egyező utolsó számjegyek darabszáma
     * @return true, ha megegyezik-e az utolsó n számjegy
     */
    public static boolean utolsoSzamjegyEgyenlosegN(int a, int b, int n) {
        return false;
    }

    /**
     * Készíts egy statikus, `honap` nevű függvényt, ami egy számot vár.
     * A függvény térjen vissza az adott hónappal szövegesen (ékezet nélkül), ha a megadott szám 1-12 között van.
     * Minden más esetben pedig a "nem ismert" szöveggel!
     *
     * @param honap az adott hónap számként
     * @return a hónap szövegesen, vagy "nem ismert"
     */
    public static String honap(int honap) {
        return "nem ismert";
    }


    /**
     * Készíts egy statikus, `primek` nevű függvényt, ami egy számot vár.
     * A függvény térjen vissza azzal, hogy az adott számig (a paraméterként kapott szám is benne van) hány darab prímszám van!
     *
     * @param meddig A szám, ameddig szeretnénk megtudni, hogy hány darab prímszám van
     * @return a prímek száma
     */
    public static int primek(int meddig) {
        return 0;
    }

    /*
     * ----------------------------------------------------------------------------------------------------------------
     * ----------------------------------------------------------------------------------------------------------------
     * --------------------------------- Ezen a ponton túl ne módosíts a forráskódon! ---------------------------------
     * ----------------------------------------------------------------------------------------------------------------
     * ----------------------------------------------------------------------------------------------------------------
     */
    private static int OK = 0;
    private static int FAIL = 0;

    public static String egyezik(Object valasz, Object referencia, String msg) {
        boolean egyezik = valasz.equals(referencia);
        if (egyezik) {
            OK++;
        } else {
            FAIL++;
        }
        return "\t- " + msg + ": " + (egyezik ? "Helyes" : "Helytelen");
    }

    public static void main(String[] args) {
        System.out.println("Lustalkodas tesztelése");
        System.out.println(egyezik(lustalkodas(true, false), false, "Hétköznap, nincs vakáció"));
        System.out.println(egyezik(lustalkodas(false, false), true, "Nincs hétköznap, nincs vakáció"));
        System.out.println(egyezik(lustalkodas(true, true), true, "Hétköznap, vakáció"));
        System.out.println(egyezik(lustalkodas(false, true), true, "Nincs Hétköznap, vakáció"));
        System.out.println();
        System.out.println("utolsoSzamjegyEgyenloseg tesztelése");
        System.out.println(egyezik(utolsoSzamjegyEgyenloseg(21, 991), true, "21, 991 (megegyezik)"));
        System.out.println(egyezik(utolsoSzamjegyEgyenloseg(1, 99991), true, "1, 99991 (megegyezik)"));
        System.out.println(egyezik(utolsoSzamjegyEgyenloseg(21, 22), false, "21, 22 (nem egyezik meg)"));
        System.out.println(egyezik(utolsoSzamjegyEgyenloseg(654, 968), false, "654, 968 (nem egyezik meg)"));
        System.out.println(egyezik(utolsoSzamjegyEgyenloseg(653, 9283), true, "653, 9283 (megegyezik)"));
        System.out.println(egyezik(utolsoSzamjegyEgyenloseg(2, 2), true, "2, 2 (megegyezik)"));
        System.out.println();
        System.out.println("utolsoSzamjegyEgyenlosegN tesztelése");
        System.out.println(egyezik(utolsoSzamjegyEgyenlosegN(21, 991, 2), false, "21, 991 (nem egyezik meg)"));
        System.out.println(egyezik(utolsoSzamjegyEgyenlosegN(1, 99991, 1), true, "1, 99991 (megegyezik)"));
        System.out.println(egyezik(utolsoSzamjegyEgyenlosegN(66621, 66622, 2), false, "66621, 66622 (nem egyezik meg)"));
        System.out.println(egyezik(utolsoSzamjegyEgyenlosegN(95954, 31954, 2), true, "95954, 31954 (megegyezik)"));
        System.out.println(egyezik(utolsoSzamjegyEgyenlosegN(999995, 1199995, 3), true, "999995, 1199995 (megegyezik)"));
        System.out.println();
        System.out.println("honap tesztelése");
        System.out.println(egyezik(honap(1), "januar", "januar"));
        System.out.println(egyezik(honap(2), "februar", "februar"));
        System.out.println(egyezik(honap(3), "marcius", "marcius"));
        System.out.println(egyezik(honap(4), "aprilis", "aprilis"));
        System.out.println(egyezik(honap(5), "majus", "majus"));
        System.out.println(egyezik(honap(6), "junius", "junius"));
        System.out.println(egyezik(honap(7), "julius", "julius"));
        System.out.println(egyezik(honap(8), "augusztus", "augusztus"));
        System.out.println(egyezik(honap(9), "szeptember", "szeptember"));
        System.out.println(egyezik(honap(10), "oktober", "oktober"));
        System.out.println(egyezik(honap(11), "november", "november"));
        System.out.println(egyezik(honap(12), "december", "december"));
        System.out.println(egyezik(honap(0), "nem ismert", "0. honap"));
        System.out.println(egyezik(honap(100), "nem ismert", "100. honap"));
        System.out.println(egyezik(honap(-999), "nem ismert", "-999. honap"));
        System.out.println();
        System.out.println("primek tesztelése");
        System.out.println(egyezik(primek(100), 25, "100-ig 25"));
        System.out.println(egyezik(primek(4), 2, "4-ig 2"));
        System.out.println(egyezik(primek(41), 13, "41-ig 13"));
        System.out.println(egyezik(primek(5265), 698, "5265-ig 698"));
        System.out.println(egyezik(primek(2), 1, "2-ig 1"));
        System.out.println();
        System.out.println("Végeredmény: " + OK + " helyes, " + FAIL + " helytelen.");
        System.out.println((((double) OK / (OK + FAIL)) * 100) + " % (" + OK + "/" + (OK + FAIL) + ")");
    }
}
