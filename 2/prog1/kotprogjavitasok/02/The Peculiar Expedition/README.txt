A játék futtatásához a JátékStart > main -t kell futtatnod.

A játék JDK 15 verzióban készült, ezért arra kérlek, hogy ezen a verzión próbáld meg futtatni, mert mákülönben hibát fog dobni.
Amennyiben, olyan hibát dobna a program, hogy nem találja a pályát ( map.txt ) , akkor a következőt kell csinálnod:
	-Inderekt módon meg kell adnod az elérési útvonalat.
		->ProgramEngine > ProgramEngine > NewGameStart > (63. sor ) "File file = new File(x)"
			! x helyére kerüljön a "map.txt" elérési útvonala, amelyet a következő képpen tudsz a legegyszerőbben megtalálni:
			Megkeresed a számítógépeden a "map.txt" fájlt ( a The Peculiar Expedition mappán belül lesz ) , jobb klikk a fájlra > Tulajdonságok > Hely
			  (Valahogyan úgy kezdődik, hogy "Meghajtó:\user\...\) .
			Amikor bemásoltad így nézzen ki a sor : "File file = new File(Meghajtó:\user\...\)".
			A végére oda kell írnod, a fájl nevét azaz a "map.txt" - t. -> (idézőjelekben kell bemásolnod természetesen.)
			"File file = new File(Meghajtó:\user\...\map.txt)"

Egy jó tanács még, hogy nagyobb képernyőn teszteld , ugyanis pixelpontos elhelyezéseket használok és egy laptop képernyőn nem 100% , hogy megfelelő lesz az élmény.

Igyekeztem mindent leírni a metódusokkal kapcsolatosan, remélem érthető lesz :)
