package Frames;

import javax.swing.*;
import java.awt.*;
import ProgramEngine.*;

/**
 *FaluShop osztály, amely azért felelős, hogy ebben valósítsam meg a faluhoz tartozó boltot (frame).
 */

public class FaluShop {
    /**
     * @param item létrehzása
     * @param faluShopFrame létrehozása
     * @param foodChooseFalu ActionListener implementálása (ProgramEngine ChoosenFaluShop 403-)
     */
    public static JFrame faluShopFrame = new JFrame();
    public static JButton[][] item = new JButton[5][1];
    ProgramEngine.ChoosenFaluShopFood foodChooseFalu = new ProgramEngine.ChoosenFaluShopFood();

    /**
     *A FaluShop-hoz tartozó default konstruktor , amelyben meghívom a shopFrameGenerate() és buttons() metódusaim.
     */

    public FaluShop(){
        shopFrameGenerate();
        buttons();
    }

    /**
     *Ez a metódus felelős azért, hogy létre hozza magát az üres ablakot.
     * Beállítom az ablak bal fent elhelyezkedő logoját a "logo.jpg" képre;
     * Beállítom az ablak nevét ("Falu shop");
     * Beállítom az ablak méretét , azt, hogy ne legyen átméretezhető, illetve azt is, hogy látható legyen, ugyanis alapértelmezetten a setVisile értéke false;
     * Beállítom, hogy kiikszeléskor az ablak csak eltűnjön , ne lépjen ki a programból;
     * Beállítom, hogy hol helyezkedjen el a képernyőn a megjelenő ablak és azt is, hogy oda kerüljön a focus (olyan, mintha automatikusan belekattint)
     */

    public void shopFrameGenerate(){
        faluShopFrame = new JFrame();
        ImageIcon logo = new ImageIcon("logo.jpg");
        faluShopFrame.setTitle("Falu shop"); // keret neve
        faluShopFrame.setSize(420, 300); // mindkettő ( h , w ) px értéket vesz fel
        faluShopFrame.setResizable(false); // fix méretű marad a keret
        faluShopFrame.setVisible(true); // látható lesz az alap
        faluShopFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE); // nem eltünteni (default , hanem kilép a programből)
        faluShopFrame.setLocation(1500 , 620);
        faluShopFrame.setFocusable(true);
        faluShopFrame.setIconImage(logo.getImage()); // betöltjük ( kicseréli az alap java logot) a választott képre
    }

    /**
     *
     *Ez a metódus azért felel, hogy az előbb létrehozott ablakot, feltöltse gombokkal.
     *Létrehozok, hogy containert, amely felveszik az ablak méreteit és tuladjonképpen teljesen reprezentálja azt.
     *Ebbe a containerbe 5 darab gombot helyezek el, egy for ciklus segítségével, olyan módon, hogy a
     *item nevezetű kétdimenziós JButton tömböm i. elemének adok egy új gombot, olyan névvel
     *amilyet a foodChooseFalu classom ad vissza, a for ciklus i változó értéke alapján.
     *Ezek után, ennek a @param item i. elemét átadom a containernek (frame-nek) is .
     * Plusz minden gombhoz hozzárendelem a @param foodChooseFalu ActionListener metódusom (ProgramEngine 403 - )
     */

    public void buttons(){
        Container container = faluShopFrame.getContentPane();
        container.setLayout(new GridLayout(5 , 1));
        for (int i = 0; i < 5; i++) {
            item[i][0] = new JButton(ProgramEngine.chosenFaluShopFood(i));
            item[i][0].addActionListener(foodChooseFalu);
            container.add(item[i][0]);
        }

    }
}
