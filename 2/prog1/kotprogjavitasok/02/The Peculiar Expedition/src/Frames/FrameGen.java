package Frames;

import ProgramEngine.*;

import javax.swing.*;
import java.awt.*;

/**
 * FrameGen osztály , ami azért felelős, hogy létrehozza a játék fő ablakát, amelyben majd tud mozogni a játékos.
 * Ez az osztály össze van kötve a ProgramEngine osztállyal (gyerekosztálya) , ugyanis ott történnek meg a változtatások.
 */

public class FrameGen extends ProgramEngine {

    /**
     * @param localVariable egy változó, amely a ProgramEngine első 2 scannelő metódusa során létrehozott Array list bejárásáhot szükséges.
     * @param playerX a player jelenlegi X kordinátáját tároló változó.
     * @param playerY a player jelenlegi Y kordinátáját tároló változó.
     * @param frameMapGen a FrameGen ablaka, amely a továbbiakban maga a pályát fogja tartalmazni, ezen történnek a lépések , stb... .
     * @param seged a player léptetése során az elöző tartozkodási helyének képe tárolódik benne, és következő lépénél ebből is töltődik vissza, seged gomb változó.
     * @param map az eredeti beolvasott , megadott , alap map, amelyben minden, ami nem víz , játékos vagy hajó , fekete. Ez tükrözi azt, hogy nem látható még a nem járt terület.
     * @param segedMap a map teljes rekonsturciója lefedések nélkül. Amikor látható lesz egy terület, akkor ennek a mapnak az adott indexéről töltöm vissza a sima mapba.
     * @param openInventory egy KeyListener implementálása, amely az "i" betű megnyomására, előhozza az inventoryt.
     */

    public static int localVariable = 0;
    public static int playerX = 0;
    public static int playerY = 0;
    public static JFrame frameMapGen = new JFrame();
    public static JButton seged = new JButton();
    public static String[][] segedMap = new String[20][30];
    public static JButton[][] map = new JButton[20][30];
    ProgramEngine.InventoryOpen openInventory = new ProgramEngine.InventoryOpen();

    /**
     * Default konstruktor, amelyben meghívom a
     * @basicFrameGen()
     * @basicFrameFillUpButtons()
     * @basicFrameFillUpImages()
     * metódusaim.
     */

    public FrameGen(){
        basicFrameGen();
        basicFrameFillUpButtons();
        basicFrameFillUpImages();
    }

    /**
     * Ez a metódus felel azért, hogy a megjelenő ablak
     * neve,mérete,átméretezhetősége,láthatósága,kilépési folyamata,elhelyezkedése és fókusza be legyen állítva.
     * Az egész alakra beállítom a @param openInventory KeyListenert.
     */

    public void basicFrameGen(){
        ImageIcon logo = new ImageIcon("logo.jpg"); // létrehoz egy ImageIcont
        frameMapGen.setTitle("The Peculiar Expedition"); // keret neve
        frameMapGen.setSize(1500, 1000); // mindkettő ( h , w ) px értéket vesz fel
        frameMapGen.setResizable(false); // fix méretű marad a keret
        frameMapGen.setVisible(true); // látható lesz az alap
        frameMapGen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // nem eltünteni (default , hanem kilép a programből)
        frameMapGen.setLocation(0 , 20);
        frameMapGen.setFocusable(true);
        frameMapGen.setIconImage(logo.getImage()); // betöltjük ( kicseréli az alap java logot) a választott képre
        frameMapGen.addKeyListener(openInventory);
    }

    /**
     * Ez a metódus azért felelős, hogy az imént létrehozott alakot feltöltse gombokkal, számszerint 20x30 -al , ugyanis
     * ekkora méretre deklaráltuk az alblakot.
     * A container reprezentálja az ablakot, amelyet feltöltöm a map adott indexére elhelyezett gombokkal 2 for ciklus segítségével, ugyanis 2 dimenziós mátrixként kell kezelni a map illetve segedMap tömböt.
     * Minden egyes gombhoz hozzáadok egy Action, illetve egy KeyListenert , amelykre referencia a ProgramEngine osztályomban található.
     * ProgramEngine.Move , ProramEngine.InventoryOpen.
     */

    public void basicFrameFillUpButtons(){
        ProgramEngine.InventoryOpen inventoryOpen = new ProgramEngine.InventoryOpen();
        ProgramEngine.Move mozgat = new ProgramEngine.Move();
        Container contain = frameMapGen.getContentPane();
        contain.setLayout(new GridLayout(20, 30));
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 30; j++) {
                map[i][j] = new JButton();
                map[i][j].addActionListener(mozgat);
                map[i][j].addKeyListener(inventoryOpen);
                contain.add(map[i][j]);
            }
        }
    }

    /**
     * Az utolsó metódus azért felel, hogy az előbbiekben feltöltött map és container, egyben ablak gombjainak "háttérképet" állítson be, a beolvasás során létrehozott mapList Array lista
     * , majd ebben eltárolt "xy.jpg/png" formátomú Stringek alapján.
     * Ehhez kapcsolódó ProgramEngine kódrészlet :
     * ProgramEngine.NewGameStart
     * ProgramEngine.Scan
     * Magyarázat:
     * Két ciklussal végigmegyek az ablak méretein 20x30 as, majd a segedMap feltöltöm a mapList alapján -> Ez reprezentálni fogja a beszkennelt fájl 2 dimenziós tömbben, nem arrayként
     * Ezek után megnézem, hogy az ArrayList adott indexén , milyen String szerepel. Ha "tenger.png" vagy "hajó.png" vagy "játékos.png" , akkor egyszerűen csak beállítom az eredeti ablak(frame) ben
     * azon az indexen lévő gom háttérképét, arra, ami azon az indexen az ArrayListben szerepelt, azaz "tenger.png" vagy "hajó.png" vagy "játékos.png" re.
     * Illetve, ha az adott indexen a "játékos.png" String szerepel, akkor beállítom a játékos x és y kordinátáját is, erre az indexre.
     * Egyéb esetben pedig fekete hátteret állítok be a gomboknak -> nem látszanak a feltérképezetlen területek.
     */

    public void basicFrameFillUpImages(){
        for(int i = 0 ; i < 20 ; i++){
            for(int j = 0 ; j < 30 ; j++) {
                segedMap[i][j] = Source.mapList.get(localVariable);
                if(Source.mapList.get(localVariable).equals("tenger.png") || Source.mapList.get(localVariable).equals("hajó.png") || Source.mapList.get(localVariable).equals("játékos.png")) {
                    ImageIcon hatter = new ImageIcon(Source.mapList.get(localVariable));
                    map[i][j].setIcon(hatter);
                    localVariable++;
                    if(Source.mapList.get(localVariable).equals("játékos.png")){
                        playerY = j+1;
                        playerX = i;
                    }
                }
                else{
                    ImageIcon fekete = new ImageIcon("fekete.png");
                    map[i][j].setIcon(fekete);
                    localVariable++;
                }
            }
        }
    }
}


