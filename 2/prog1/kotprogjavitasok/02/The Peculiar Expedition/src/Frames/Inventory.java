package Frames;

import Ételek.Foods;
import Tárgyak.Items;
import ProgramEngine.*;

import javax.swing.*;
import java.awt.*;

/**
 * Ebben az osztályban hozom kétre az Inventory ablakot
 */

public class Inventory{

    /**
     * @param viszonyBoost a Bölcs által nyújtott előny , ami akkor változik, ha éppen Bölcs csapattársunk van.
     * @param energyCounter az energia mennyiséget számolja (default 100).
     * @param goldCounter a pénz mennyiséget számolja (default 250).
     * @param viszonyCounter az alap viszonyunkat számolja (default 3).
     * @param memberCounter a csapattársak száma.
     * @param yVariable egy számláló, amely minden egyes Inventoryba bekerült tárgy után 50-el növekszik , lényegében a ∑ gombok magasságát számolja.
     * @param kötélCounter nálunk lévő kötelek számát számolja.
     * @param bozótvágóCounter nálunk lévő bozótvágók számát számolja.
     * @param fáklyaCounter nálunk lévő fáklyák számát számolja.
     * @param üveggolyóCounter nálunk lévő üveggolyók számát számolja.
     * @param húsCounter nálunk lévő húsok számát számolja.
     * @param whiskeyCounter nálunk lévő whiskeyk számát számolja.
     * @param csokoládéCounter nálunk lévő csokoládék számát számolja.
     * @param kábítószerCounter nálunk lévő káítószerek számát számolja.
     * @param gyümölcsCounter nálunk lévő gyümölcsök számát számolja.
     *
     * @param kötél kötél objektum.
     * @param bozótvágó bozótvágó objektum.
     * @param fáklya fákyla objektum.
     * @param üveggolyó üveggolyó objektum.
     * @param hús hús objektum.
     * @param whiskey whiskey objektum.
     * @param csokoládé csokoládé objektum.
     * @param kábítószer kábítószer objektum.
     * @param gyümölcs gyümölcs objektum.
     *
     * @param inventoryFrame a megjelenő inventory ablaka(frame)
     *
     * @param buttonEnergy energiához tartozó gomb, ami belekerül majd az Inventory frame-be.
     * @param buttonGold pénzhez tartozó gomb, ami belekerül majd az Inventory frame-be.
     * @param buttonViszony viszonyhoz tartozó gomb, ami belekerül majd az Inventory frame-be.
     * @param buttonMembers csapattársakhoz tartozó gomb, ami belekerül majd az Inventory frame-be.
     * @param buttonKötél kötélhez tartozó gomb, ami belekerül majd az Inventory frame-be.
     * @param buttonBozótvágó bozótvágóhoz tartozó gomb, ami belekerül majd az Inventory frame-be.
     * @param buttonFáklya fáklyához tartozó gomb, ami belekerül majd az Inventory frame-be.
     * @param buttonÜveggolyó üveggolyóhoz tartozó gomb, ami belekerül majd az Inventory frame-be.
     * @param buttonHús húshoz tartozó gomb, ami belekerül majd az Inventory frame-be.
     * @param buttonWhiskey whiskeyhez tartozó gomb, ami belekerül majd az Inventory frame-be.
     * @param buttonCsokoládé csokoládéhoz tartozó gomb, ami belekerül majd az Inventory frame-be.
     * @param buttonKábítószer kábítószerhez tartozó gomb, ami belekerül majd az Inventory frame-be.
     * @param buttonGyümölcs gyümölcshöz tartozó gomb, ami belekerül majd az Inventory frame-be.
     *
     * @param inventoryUpgrade a Szamár kisérőhöz tartozó számláló, ami 2 gombnyival növeli meg az Inventory magasságunkat -> 2-vel több gomb fér el benne (+2 slot). Csak akkor változik az értéke, ha Szamár csapattársunk van.
     * @param inventoryMax az Inventory - ba elférő gombok maximális száma , azaz magának az Inventory slotjainak száma.
     * @param closeInventory egy ProgramEngine metódusból származtatott KeyListener metódus , amely "i" betű megnyomására bezárja az Inventory ablakot.
     * @param giveEnergy egy ProgramEngine metódusból származtatott metódus, ami az energyCounter höz ad hozzá + energiát a feladatban leírtak alapján.
     * @param container amely reprezentálja az Inventory ablakot.
     */

    public static int viszonyBoost = 0;
    public static double energyCounter = 100;
    public static int goldCounter = 250000;
    public static int viszonyCounter = 3;
    public static int memberCounter = 0;
    public static int yVariable = 0;
    public static int kötélCounter = 0;
    public static int bozótvágóCounter = 0;
    public static int fáklyaCounter = 0;
    public static int üveggolyóCounter = 0;
    public static int húsCounter = 0;
    public static int whiskeyCounter = 0;
    public static int csokoládéCounter = 0;
    public static int kábítószerCounter = 0;
    public static int gyümölcsCounter = 0;

    public static Items kötél;
    public static Items bozótvágó;
    public static Items fáklya;
    public static Items üveggolyó;
    public static Foods hús;
    public static Foods whiskey;
    public static Foods csokoládé;
    public static Foods kábítószer;
    public static Foods gyümölcs;

    public static JFrame inventoryFrame = new JFrame();

    public static JButton buttonEnergy = new JButton("Energia: " + energyCounter);
    public static JButton buttonGold = new JButton("Pénz: " + goldCounter);
    public static JButton buttonViszony = new JButton("Viszony: " + viszonyCounter);
    public static JButton buttonMembers = new JButton("Csapattárs: " + memberCounter);
    public static JButton buttonKötél = new JButton("Items.Kötél: " + kötélCounter);
    public static JButton buttonBozótvágó = new JButton("Items.Bozótvágó: " + bozótvágóCounter);
    public static JButton buttonFáklya = new JButton("Items.Fáklya: " + fáklyaCounter);
    public static JButton buttonÜveggolyó = new JButton("Items.Üveggolyó: " + üveggolyóCounter);
    public static JButton buttonHús = new JButton("Foods.Hús: " + húsCounter);
    public static JButton buttonWhiskey = new JButton("Foods.Whiskey: " + whiskeyCounter);
    public static JButton buttonCsokoládé = new JButton("Foods.Csokoládé: " + csokoládéCounter);
    public static JButton buttonKábítószer = new JButton("Foods.Kábítószer: " + kábítószerCounter);
    public static JButton buttonGyümölcs = new JButton("Foods.Gyümölcs: " + gyümölcsCounter);

    public static int inventoryUpgrade = 0;
    public static int inventoryMax = 12 + inventoryUpgrade;
    public static ProgramEngine.InventoryClose closeInventory = new ProgramEngine.InventoryClose();
    public static ProgramEngine.giveEnergy giveEnergy = new ProgramEngine.giveEnergy();
    public static Container container = inventoryFrame.getContentPane();



    /**
     * Default konstrukor , amiben meghívom a frameInventory() és labelFillUp() metódusaim.
     */
    public Inventory(){
        frameInventory();
        labelFillUp();
    }

    /**
     * Az ablak létrehozására szolgáló metódus, ahol beállítom az ablak
     * nevét , méretét , átméretezhetőségét , elhelyezkedését , illetve
     * hozzá rendelem az Inventory bezáró KeyListenert.
     */

    public void frameInventory(){
        inventoryFrame.setTitle("Frames.Inventory"); // keret neve
        inventoryFrame.setSize(300, 600 ); // mindkettő ( h , w ) px értéket vesz fel
        inventoryFrame.setResizable(false); // fix méretű marad a keret
        inventoryFrame.setLocation(1500 , 20);
        inventoryFrame.addKeyListener(closeInventory);
    }

    /**
     * Ebben a metódusban valósítom meg azt, hogy az inventory fel legyen töltve gombokkal.
     * A játék kezdetekor, fixen 4 gomb szerepel az inventoryban , amelyek tájékoztató jellegűek és a következők:
     * Energia , Pénz , Csapattárs , Viszony. Ezekhez nincs semmilyen Listener hozzárendelve , azaz, ha rájuk kattintunk,
     * akkor nem történik semmi.
     * a maradék gombot pedig, a Counterek segítségével csak akkor rakom bele az Inventory frame-be, ha az adott elemből
     * legalább 1 db van nálunk, egyébként nem.
     * A gombok Inventoryhoz adása a ProgramEngine-ben történik, a ProgramEngine.inventoryButtonFillUp() metódussal.
     */

    public void labelFillUp(){
        container.setLayout(new GridLayout(inventoryMax  , 1));
        ProgramEngine.inventoryButtonFillUp(buttonEnergy , container , yVariable);
        ProgramEngine.inventoryButtonFillUp(buttonGold, container , yVariable);
        ProgramEngine.inventoryButtonFillUp(buttonMembers, container , yVariable);
        ProgramEngine.inventoryButtonFillUp(buttonViszony, container , yVariable );
        if(kötélCounter > 0) {
            ProgramEngine.inventoryButtonFillUp(buttonKötél, container, yVariable );
        }
        if(bozótvágóCounter > 0){
            ProgramEngine.inventoryButtonFillUp(buttonBozótvágó, container, yVariable);
        }
        if(fáklyaCounter > 0){
            ProgramEngine.inventoryButtonFillUp(buttonFáklya, container, yVariable );
        }
        if(üveggolyóCounter > 0){
            ProgramEngine.inventoryButtonFillUp(buttonÜveggolyó, container, yVariable );
        }
        if(húsCounter > 0){
            ProgramEngine.inventoryButtonFillUp(buttonHús, container, yVariable );
        }
        if(whiskeyCounter > 0){
            ProgramEngine.inventoryButtonFillUp(buttonWhiskey, container, yVariable );
        }
        if(csokoládéCounter > 0){
            ProgramEngine.inventoryButtonFillUp(buttonCsokoládé, container, yVariable );
        }
        if(gyümölcsCounter > 0){
            ProgramEngine.inventoryButtonFillUp(buttonGyümölcs, container, yVariable );
        }
        if(kábítószerCounter > 0){
            ProgramEngine.inventoryButtonFillUp(buttonKábítószer, container, yVariable );
        }
    }


}
