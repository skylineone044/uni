package Frames;

import javax.swing.*;
import java.awt.*;

import ProgramEngine.*;

/**
 * Ebben az osztályban valósítom meg azt az ablakot, amely egy csapattársat kínál fel a játék elején, vagy 20% eséllyel a faluban.
 */

public class MembersChosseFrame {

    /**
     * @param startingVariable 0 , 1 értéket vesz fel, az alapján, hogy az indulásnál milyen csapattársakat ajánlhat , vagy a faluban milyeneket -> ProgramEngine.chooseRandomVillagerMember()
     * @param memberName a ProgramEngine.chooseRandomVillagerMember() által visszaadott String értéket fogja felvenni ( pl : "Bölcs" ).
     * @param map egy 1x1 es tömb, amiben fogom majd megjeleníteni a csapattárs nevét ( memberName ) gomb formájában
     * @param membersFrame ennek az 1x1 tömbnek az ablaka , frame-je.
     * @param chooseAMember ProgramEngine.ChoosenMember metódus példányosítása chooseAMember változóként.
     */

    public static int startingVariable = 0;
    public static String memberName;
    public static JButton[][] map = new JButton[1][1];
    public static JFrame membersFrame = new JFrame();
    ProgramEngine.ChoosenMember chooseAMember = new ProgramEngine.ChoosenMember();

    /**
     * Default konstruktor , amelyben meghívom a
     * ProgramEngine.chooseRandomVillageMember() metódusom
     * @return String és ezt átadom a memberName változónak, illetve
     * a membersFrameGenerate() és membersFrameFillUpButton() metódusaim.
     */

    public MembersChosseFrame(){
        memberName = ProgramEngine.chooseRandomVillageMember();
        membersFrameGenerate();
        membersFrameFillUpButton();
    }

    /**
     * Metódus, ami létrehozza az ablakot, és beállítja a:
     * logoját, nevét , méretét , átméretezhetőégét , láthatóságát , kilépési módszerét ( csak eltűnik ) , helyzetét  és
     * fókuszba helyezi ( olyan, mintha bele kattintottunk volna ).
     */

    public void membersFrameGenerate(){
        membersFrame = new JFrame();
        ImageIcon logo = new ImageIcon("logo.jpg");
        membersFrame.setTitle("Member Shop"); // keret neve
        membersFrame.setSize(250, 150); // mindkettő ( h , w ) px értéket vesz fel
        membersFrame.setResizable(false); // fix méretű marad a keret
        membersFrame.setVisible(true); // látható lesz az alap
        membersFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE); // nem eltünteni (default , hanem kilép a programből)
        membersFrame.setLocation(1650 , 20);
        membersFrame.setFocusable(true);
        membersFrame.setIconImage(logo.getImage()); // betöltjük ( kicseréli az alap java logot) a választott képre
    }

    /**
     * Ezzel a metódussal töltöm fel 1 db gombbal a containert, amely reprezentálja a frame - t azaz az ablakot.
     * minden esetben ehhez az egy gombhoz hozzárendelem a chooseAMember actionListenert.
     */

    public void membersFrameFillUpButton(){
        Container contain = membersFrame.getContentPane();
        contain.setLayout(new GridLayout(1, 1));
        map[0][0] = new JButton(memberName);
        map[0][0].addActionListener(chooseAMember);
        contain.add(map[0][0]);
    }
}
