package Frames;

import javax.swing.*;
import java.awt.*;

import ProgramEngine.*;

/**
 * Ebben az osztályban valósítom meg a játék kezdetekor megnyíló bolt albakát.
 */

public class Shop {

    /**
     * @param kereskedőUpgrade amennyiben kereskedő csapattársunk van változik.
     * @param shopFrame az alak létrehozása.
     * @param item egy gomb lista
     * @param foodChoose ProgramEngine.ChoosenFood metódus példánya
     */

    public static int kereskedőUpgrade;
    public static JFrame shopFrame = new JFrame();
    public static JButton[][] item = new JButton[7][1];
    public static ProgramEngine.ChoosenFood foodChoose = new ProgramEngine.ChoosenFood();

    /**
     * Default konstruktor, amelyben meghívom a
     * shopFrameGenerate() és buttons metódusaim.
     */

    public Shop(){
        shopFrameGenerate();
        buttons();
    }


    /**
     * Metódus, ami létrehoz egy új ablakot, és beállítja a:
     * logoját, nevét , méretét , átméretezhetőégét , láthatóságát , kilépési módszerét ( csak eltűnik ) , helyzetét  és
     * fókuszba helyezi ( olyan, mintha bele kattintottunk volna ).
     */

    public void shopFrameGenerate(){
        shopFrame = new JFrame();
        ImageIcon logo = new ImageIcon("logo.jpg");
        shopFrame.setTitle("Frames.Shop"); // keret neve
        shopFrame.setSize(420, 300); // mindkettő ( h , w ) px értéket vesz fel
        shopFrame.setResizable(false); // fix méretű marad a keret
        shopFrame.setVisible(true); // látható lesz az alap
        shopFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE); // nem eltünteni (default , hanem kilép a programből)
        shopFrame.setLocation(1500 , 620);
        shopFrame.setFocusable(true);
        shopFrame.setIconImage(logo.getImage()); // betöltjük ( kicseréli az alap java logot) a választott képre
    }

    /**
     * Ezzel a metódussal töltöm fel 7 db gombbal a containert, amely reprezentálja a frame - t azaz az ablakot.
     * minden esetben ehhez az egy gombhoz hozzárendelem a foodChoose actionListenert.
     */

    public void buttons(){
        Container container = shopFrame.getContentPane();
        container.setLayout(new GridLayout(7 , 1));
        for (int i = 0; i < 7; i++) {
            item[i][0] = new JButton(ProgramEngine.chosenStartFood(i));
            item[i][0].addActionListener(foodChoose);
            container.add(item[i][0]);
        }

    }
}
