package Frames;

import javax.swing.*;
import java.util.ArrayList;

import ProgramEngine.*;

/**
 * Ez az osztály azért felel, hogy a program indítása után megjelenő alakon a "megnyitás" ( azaz beolvasás ) gombra kattintva
 * előhozza azt a felületet, ahol megtudjuk adni az általunk generált pálya elérési útvonalát.
 */

public class Source extends ProgramEngine {
    /**
     * @param frameSource az ablak kétrehozása.
     * @param framePanel az ablakon megjelenő felület.
     * @param frameTextField a felületen elhelyezett szöveges mező, ahová a fájl elérési útvonalát kell beírni.
     * @param frameButton egy gomb ("kész") , amellyel jelezzük a programnak, hogy jól megadtuk az elérési útvonalat
     * @param mapList ArrayList , amelybe eltárolom a megfelelő kódolás alapján az egyes számokhoz tartozó Stringeket ( pl : 13-player stb ).
     * @param scannedData az aktuálisan beolvasott adat.
     */

    public static JFrame frameSource = new JFrame();
    public static JPanel framePanel = new JPanel();
    public static JTextField frameTextField = new JTextField(30);
    public static JButton frameButton = new JButton("Kész");
    public static ArrayList<String> mapList = new ArrayList<String>();
    public static String scannedData;

    /**
     * Default konstruktor , amelyben meghívom a sourceFrameGen() metódusom.
     */
    public Source(){
        sourceFrameGen();
    }

    /**
     * Ebben a metódusban állítom be az alak nevét , láthatóságát , méretét és elhelyezkedését , továbbá
     * hozzá adom a felülethez a szöveges mezőt , és a gombot, majd később ezt a felületet adom hozzá magához az ablakhoz.
     * Közben példényosítom és meghívom a ProgramEngine.Sca() metódusom és mint action hozzá is rendelem a gombhoz.
     */

    public void sourceFrameGen(){
       frameSource.setTitle("Map elérési útvonala");
       frameSource.setVisible(true);
       frameSource.setSize(400, 200);
       frameSource.setLocation(765 , 200);


       framePanel.add(frameTextField);
       framePanel.add(frameButton);

       ProgramEngine.Scan scanning = new ProgramEngine.Scan();
       frameButton.addActionListener(scanning);

       frameSource.add(framePanel);
    }
}

