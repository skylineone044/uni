package Frames;

import javax.swing.*;

import ProgramEngine.*;

/**
 * Ez az osztály felelős azért , hogy a program indítása után egy kezdő képernyőt jelenítsen meg, ahol tudunk választani mit szeretnénk:
 * Új játékot indítani, vagy beolvasni egy általunk generált pályát , vagy kilépni.
 */

public class StartPage extends ProgramEngine {

    /**
     * @param frameStartPage az ablak létrehozása.
     * @param framePanel a felület létrehozásas.
     * @param frameutton 1. gomb létrehozása("Új játék").
     * @param frameButon2 2. gomb létrehozása ("Megnyitás").
     * @param frameButton3 3. gomb létrehozásas("Kilépés").
     */

    public static JFrame frameStartPage = new JFrame();
    public static JPanel framePanel = new JPanel();
    public static JButton frameButton = new JButton("Új játék");
    public static JButton frameButton2 = new JButton( "Megnyitás");
    public static JButton frameButton3 = new JButton("Kilépés");

    /**
     * Default konstruktor, amelyben meghívom a startPageFrameGen() metódusom.
     */

    public StartPage(){
        startPageFrameGen();
    }

    /**
     * Ebben a metódusban beállítom az ablak láthatóságát , méretét és elhelyezkedését.
     * Majd az ablakhoz hozzá adom a felületet , az ablaknak abszolút pozíciót adok.
     * Minden gombnak beállítom a méretét ( 300px , 400/500/600 px az elhelyezkedése a felületen , 400px széles és 100px magas gombok)
     * A felülethez hozzáadom a gombokat, majd attól függően , hogy melyik - melyik gomb , hozzá rendelem a ProgramEngine megfelelő metódusát, amelny elvégzi azt a feladatot,
     * amelyet alapból a gombok is tükröznek ("új játék" , "megnyitás" , "kilépés").
     */
    public void startPageFrameGen(){
        frameStartPage.setVisible(true);
        frameStartPage.setSize(1000, 1000);
        frameStartPage.setLocation(460 , 20);

        frameStartPage.add(framePanel);
        framePanel.setLayout(null);
        frameButton.setBounds(300 , 400 , 400 , 100);
        frameButton2.setBounds(300 , 500 , 400 , 100);
        frameButton3.setBounds(300 , 600 , 400 , 100);
        framePanel.add(frameButton);
        framePanel.add(frameButton2);
        framePanel.add(frameButton3);

        ProgramEngine.NewGameStart newStart = new ProgramEngine.NewGameStart();
        frameButton.addActionListener(newStart);

        ProgramEngine.SourceOpen SourceOpen = new ProgramEngine.SourceOpen();
        frameButton2.addActionListener(SourceOpen);

        ProgramEngine.StartPageExit startPageExit = new ProgramEngine.StartPageExit();
        frameButton3.addActionListener(startPageExit);
    }
}
