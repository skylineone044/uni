package Kisérők;

/**
 * Bölcs kisérő , aki a GroupMembers osztály gyerekosztálya.
 */

public class Bölcs extends GroupMembers {
    /**
     * @param viszonyBoost a Bölcs kisérő különleges képessége
     */
    private int viszonyBoost = 0;

    public int getViszonyBoost() {
        return viszonyBoost;
    }

    public void setViszonyBoost(int viszonyBoost) {
        this.viszonyBoost = viszonyBoost;
    }
}
