package Kisérők;

/**
 * Felderítő kisérő , aki a GroupMembers osztály gyerekosztálya.
 */

public class Felderítő extends GroupMembers {
    /**
     * @param viewDistanceUpgrade a Felderítő különleges képessége
     */

    private int viewDistanceUpgrade = 0;

    public int getViewDistanceUpgrade() {
        return viewDistanceUpgrade;
    }

    public void setViewDistanceUpgrade(int viewDistanceUpgrade) {
        this.viewDistanceUpgrade = viewDistanceUpgrade;
    }


}
