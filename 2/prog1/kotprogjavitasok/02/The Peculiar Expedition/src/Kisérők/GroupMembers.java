package Kisérők;

/**
 * A kisérők ősosztálya
 */

public class GroupMembers {
    static double moveCost = 0.15;
    static int goldCost = 150;

    public static double getMoveCost() {
        return moveCost;
    }

    public static void setMoveCost(double moveCost) {
        GroupMembers.moveCost = moveCost;
    }

    public static int getGoldCost() {
        return goldCost;
    }

    public static void setGoldCost(int goldCost) {
        GroupMembers.goldCost = goldCost;
    }
}
