package Kisérők;

/**
 * Katona kisérő , aki a GroupMembers osztály gyerekosztálya.
 */

public class Katona extends GroupMembers {
    /**
     * @param katonaWhiskeySkill a Katona különleges képessége.
     */
    private int katonaWhiskeySkill = 0;

    public int getKatonaWhiskeySkill() {
        return katonaWhiskeySkill;
    }

    public void setKatonaWhiskeySkill(int katonaWhiskeySkill) {
        this.katonaWhiskeySkill = katonaWhiskeySkill;
    }
}
