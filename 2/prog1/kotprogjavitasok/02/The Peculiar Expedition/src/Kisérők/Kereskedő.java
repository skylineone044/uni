package Kisérők;

import java.util.Random;

/**
 * Kereskedő kisérő , aki a GroupMembers osztály gyerekosztálya.
 */

public class Kereskedő extends GroupMembers {
    /**
     * @param goldDecrese a Kereskedő különleges képessége.
     */

    private int goldDecrese = 0;

    public int getGoldDecrese() {
        return goldDecrese;
    }

    public void setGoldDecrese(int goldDecrese) {
        this.goldDecrese = goldDecrese;
    }

    /**
     * egy random szám generálása, amennyivel olcsóbbak lesznek a tárgyak a boltokban.
     */

    public int generateRandomInt(){
        Random rand = new Random();
        int szam = rand.nextInt(20-5)+5;
        return szam;
    }

}
