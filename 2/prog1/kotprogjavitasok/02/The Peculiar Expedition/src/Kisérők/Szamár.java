package Kisérők;

/**
 * Szamár kisérő , aki a GroupMembers osztály gyerekosztálya.
 */

public class Szamár extends GroupMembers {
    /**
     * @param inventorySlotUpgrade a Szamár különleges képessége.
     */
    private int inventorySlotUpgrade = 0;

    public int getInventorySlotUpgrade() {
        return inventorySlotUpgrade;
    }

    public void setInventorySlotUpgrade(int inventorySlotUpgrade) {
        this.inventorySlotUpgrade = inventorySlotUpgrade;
    }
}
