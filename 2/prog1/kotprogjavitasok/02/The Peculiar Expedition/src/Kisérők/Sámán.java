package Kisérők;

/**
 * Sámán kisérő , aki a GroupMembers osztály gyerekosztálya.
 */

public class Sámán extends GroupMembers {
    /**
     * @param kábítószerEnergyUpgrade a Sámán különleges képessége.
     */
    private int kábítószerEnergyUpgrade = 0;

    public int getKábítószerEnergyUpgrade() {
        return kábítószerEnergyUpgrade;
    }

    public void setKábítószerEnergyUpgrade(int kábítószerEnergyUpgrade) {
        this.kábítószerEnergyUpgrade = kábítószerEnergyUpgrade;
    }
}
