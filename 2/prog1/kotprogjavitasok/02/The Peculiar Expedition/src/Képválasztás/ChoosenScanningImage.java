package Képválasztás;

/**
 * Ez az osztály felelős azért, hogy a beolvasott adatokat az alábbi kódolás alapján átkonvertálja a képek neveire.
 *      1 - barlang
 *      2 - fa
 *      3 - fű
 *      4 - hajó
 *      5 - hegy
 *      6 - nedves
 *      7 - oltár
 *      8 - piramis
 *      9 - tenger
 *      10 - tó
 *      11 - üres
 *      12 - játékos
 *      13 - falu
 * A Scource osztályban deklarált scannedData értékfelvétele alapján ad vissza egy Stringet a ProgramEngine-nek.
 */

public class ChoosenScanningImage {

    public static String returnImage(int scannedData){
        if (scannedData == 1) {
            return "barlang.png";
        }
        if (scannedData == 2) {
            return "fa.png";
        }
        if (scannedData == 3) {
            return "fű.png";
        }
        if (scannedData == 4) {
            return "hajó.png";
        }
        if (scannedData == 5) {
            return "hegy.png";
        }
        if (scannedData == 6) {
            return "nedves.png";
        }
        if (scannedData == 7) {
            return "oltár.png";
        }
        if (scannedData == 8) {
            return "pirmais.png";
        }
        if (scannedData == 9) {
            return "tenger.png";
        }
        if (scannedData == 10) {
            return "tó.png";
        }
        if (scannedData == 11) {
            return "üres.png";
        }
        if (scannedData == 12) {
            return "játékos.png";
        }
        if( scannedData == 13){
            return "falu.png";
        }
        return null;
    }
}
