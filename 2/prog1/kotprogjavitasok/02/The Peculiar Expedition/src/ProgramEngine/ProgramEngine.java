package ProgramEngine;

import Frames.*;
import Kisérők.*;
import Képválasztás.ChoosenScanningImage;
import Riválisok.Karcsi;
import Riválisok.Peti;
import Riválisok.Zoli;
import Tárgyak.*;
import Ételek.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.Random;
import java.util.Scanner;

/**
 * Ez a metódus felel minden egyes, a pályán történő változás háttérben való megvalósításához.
 * A ProgramEngine-ben töb külön class is szerepel, ennek a nyomós indoka, az, hogy logikailag egy cél érdekében működnek
 * , illetve a szeparáltság és a könyebben kezelést szolgálják.
 */

public class ProgramEngine{
    /**
     * Ez az osztály felelős azért, hogy amikor a StartPage által generált ablakon rányomunk az 1. gombra ( "Új játék" ), akkor
     * beolvassa nekünk a gyökérkönyvtárban található map.txt ből az adatokat , és lényegében legenerálja a mapunkat.
     */
    public static class NewGameStart implements ActionListener{
        /**
         * @param scannedData egy String ( nem egyezik azzal a scannedData-val, amit a Source classomban deklaráltam .
         */

        String scanneData = "foo bar";

        /**
         * Egy actionListener-hez van kötve ez a metódus ( StartPage newStart ). Amikor rányomunk erre a gombra ( 1. gomb ( "Új játék ) )
         * , akkor megtörténik ez az "akció" az alábbiak szerint:
         *      1, Megnyitjuk a, jelen esetben - map.txt fájlunkat.
         *      2, Deklarálunk egy scannert
         *      3, Kivételkezeléssel megnézzük, hogy létezik e az adott fájl,
         *          Ha nem, akkor hibaüzenetet íratunk ki.
         *          Ha igen, akkor addig olvasunk a fájlól, amíg van következő sor.
         *      4, A scannedData - ban eltároljuk a beolvasott adatokat
         *      5, Hozzáadjuk a Source - ban szereplő mapList ArrayLit - hez ezt az adatot, olyan módon, hogy
         *          mindig meghívjuk rá a ChoosenScanningImage osztályban lévő returnImage() metódust, ami az ott szereplő
         *          kódolás alapján átkonvertálja a eolvasott számot , a megadott kép nevére.
         *          Így a mapList ArrayList - ben sorláncos módon a képek nevei fognak szerepelni.
         *      6, Ha ninc több sor, befejezzük a beolvasást.
         *      7, Létrehozunk egy új FrameGen() -t ( fő ablak ).
         *      8, Létrehozunk egy új MembersChosseFrame() - t ( a csapattárs választó ablak ).
         *      9, Létrehozunk egy új Shop() - t ( kezdő bolt ) -> átállítjuk a MembersChosseFrame osztályban szereplő
         *          startingVariable változót 1 - re, hogy amikor falura lépünk, akkor más kiosztású boltot kaphassunk ( lásd : ProgramEngine chooseRandomVillager metódus )
         *      10, Láthatatlanná tesszük a kezdő ablakunkat.
         */
        public void actionPerformed(ActionEvent e) {
            File file = new File("map.txt");
            Scanner scan = null;
            try {
                scan = new Scanner(file);
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
            while (scan.hasNextLine()) {
                scanneData = scan.next();
                Source.mapList.add(ChoosenScanningImage.returnImage(Integer.parseInt(scanneData)));
            }

            scan.close();
            new FrameGen();
            new MembersChosseFrame();
            new Shop();
            MembersChosseFrame.startingVariable = 1;
            StartPage.frameStartPage.setVisible(false);
        }
    }
    /**
     * Ez az osztály hívja meg a Source (Beolvasás) ablakot , a StartPage 2. gomb ("Megnyitás") lenyomása után.
     * Ami pedig a Scan metódust hívja majd meg a megadott útvonalú fájl beolvasására.
     * Ezek után eltünteti a "Map elérési útvonala" ablakot.
     */
    public static class SourceOpen implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            new Source();
            StartPage.frameStartPage.setVisible(false);
        }
    }

    /**
     * Ez az osztály állítja le a programunk futását a StartPage 3. gomb ("Kilépés") lenyomása után.
     */
    public static class StartPageExit implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    }

    /**
     * Amikor megadjuk a Source ablakan a "fájl elérési útvonalát" , akkor lényegében ugyanaz történik, mint
     * a ProgramEngine NewGameStart - nál, csak a fájl, amit beolvasunk , az a megadott elérési útvonal alapján lesz detektálva.
     *      1, Megnyitjuk a megadott útvonalon lévő fájlt.
     *      2, Deklarálunk egy scannert
     *      3, Kivételkezeléssel megnézzük, hogy létezik e az adott fájl,
     *          Ha nem, akkor hibaüzenetet íratunk ki.
     *          Ha igen, akkor addig olvasunk a fájlól, amíg van következő sor.
     *      4, A scannedData - ban eltároljuk a beolvasott adatokat
     *      5, Hozzáadjuk a Source - ban szereplő mapList ArrayLit - hez ezt az adatot, olyan módon, hogy
     *          mindig meghívjuk rá a ChoosenScanningImage osztályban lévő returnImage() metódust, ami az ott szereplő
     *          kódolás alapján átkonvertálja a eolvasott számot , a megadott kép nevére.
     *          Így a mapList ArrayList - ben sorláncos módon a képek nevei fognak szerepelni.
     *      6, Ha ninc több sor, befejezzük a beolvasást.
     *      7, Láthatatlanná tesszük a beolvasás ablakunkat.
     *      8, Létrehozunk egy új FrameGen() -t ( fő ablak ).
     *      9, Létrehozunk egy új MembersChosseFrame() - t ( a csapattárs választó ablak ).
     *      10, Létrehozunk egy új Shop() - t ( kezdő bolt ) -> átállítjuk a MembersChosseFrame osztályban szereplő
     *          startingVariable változót 1 - re, hogy amikor falura lépünk, akkor más kiosztású boltot kaphassunk ( lásd : ProgramEngine chooseRandomVillager metódus )
     */
    public static class Scan implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            File file = new File(Source.frameTextField.getText());
            Scanner scan = null;
            try {
                scan = new Scanner(file);
            } catch (FileNotFoundException fileNotFoundException) {
                JOptionPane.showMessageDialog(null, "Nem megfelelő a fájl elérési útvonala!", "Hiba", JOptionPane.ERROR_MESSAGE);

            }
            while (scan.hasNextLine()) {
                Source.scannedData = scan.next();
                Source.mapList.add(ChoosenScanningImage.returnImage(Integer.parseInt(Source.scannedData)));
            }

            scan.close();
            Source.frameSource.setVisible(false);
            new FrameGen();
            new MembersChosseFrame();
            new Shop();
            MembersChosseFrame.startingVariable = 1;
        }
    }

    /**
     * Ez az osztály felelős 1/2 részben a mozgásért a pályán.
     *
     */
    public static class Move implements ActionListener {

        /**
         * @param inventoryCounter az Inventoryban lévő slotokat számolja
         * @param inventoryPlusItems amennyiben az alap 8 tárgy fölé megyünk, +4 a tájékoztató gombok miatt , azaz 12 slot fölé megy , akkor += 1 lesz az értéke, azaz a túlterhelést szimbolizálja
         * @param df egy lebbegőpontos minta, hogy milyen formátumban szeretném kiíratni az energiamennyiéget
         * @param viewDistance a Felderítő különleges képessége. Logikailag ide tartozik, ugyanis a mozgás során a látókört növeli.
         */

        static int inventoryCounter = 4;
        static double inventoryPlusItems = 0;
        public static DecimalFormat df = new DecimalFormat("###.##");
        public static int viewDistance = 0;

        /**
         * Ebben a metódusban ellenőrzök és keresem meg azt index szerűen, hogy hova kattintott a felhaználó.
         * Leellenőrzöm, hogy 12 tárgynál több van e a játékosnál, ha igen, akkor a inventoryPlusItems változómnak a annyiszor 0.2 értéket adok, amennyi plusz tárgy van a játékosnál.
         * Leellenőrzöm, hogy az energia több-e, mint 0.
         *      Ha igen, akkor minden kattintás után a megfelelő módon számolva csökkentem az energiaCounter értékét és felülírom az eddigi gombot egy már új értékeket tartalmazó gombbal.
         *      Ha nem , akkor úgyanúgy felülírom az eddigi energia gombot az inventoryban, de 0 értékkel, ez jelzi, hogy elfogyott az energia.
         * Majd egy source Objectbe eltárolom , hogy hova ( melyik index-re) kattintott a player .
         * Ezek után 2 for ciklus segítségével megkeresem az adott indexű elemet és tovább adom a kordinátákat a clickMove metódusomnak.
         */
        public void actionPerformed(ActionEvent e) {
            if(inventoryCounter > Inventory.inventoryMax ){
                inventoryPlusItems = (inventoryCounter - Inventory.inventoryMax) * 0.2;
            }
            if(Inventory.energyCounter >= 0){
                Inventory.inventoryFrame.remove(Inventory.buttonEnergy);
                Inventory.buttonEnergy = new JButton("Energia: " + df.format(Inventory.energyCounter -= (1 + (Inventory.memberCounter * GroupMembers.getMoveCost()) + inventoryPlusItems)));
            }
            else{
                Inventory.inventoryFrame.remove(Inventory.buttonEnergy);
                Inventory.buttonEnergy = new JButton("Energia: " + 0);
            }
            Object source = e.getSource();
            for (int i = 0; i < 20; i++) {
                for (int j = 0; j < 30; j++) {
                    if (source == FrameGen.map[i][j]) {
                        clickMove(i, j);
                    }
                }
            }
        }
    }
    /**
     * Ez az osztály felelős a másik részben a mozgásért. Lényegében itt történik meg a player mozgatása és az egyéb ellenőrzések.
     * Megvizsgálom, hogy a paraméterben kapott értékek azok a játékostól balra , jobbra , felette vagy alatta vannak
     *      Ha igen, akkor megvizsgálom, hogy ahová kattintott az nem tenger vagy hegy vagy tó
     *          Ha nem az, akkor :
     *              Megvizsgálom, hogy 0 - e az energiája > Ha igen, akkor 8% eséllyel veszít a játékos egy csapattársat , és ha nincs csapattársa, akkor 8% el a játékos maga hagyja el a csapatot , ezzel veszít.
     *              Ez után a játékos kordinátáit beállítom a kattintott kordinátákra
     *              Közben megvizsgálom azt is, hogy a játékos jelenlegi kordinátája falu - e > Ha igen, akkor 20% eséllyel ajánlok egy új falubeli csapattársat és 100% eséllyel előhívom a falu boltját.
     *              Megvizsgálom azt is, hogy a jelenlegi kordináta hajó - e > Ha igen , akkor az energiCounter - t 100 ra állítom . Ez a pihenés.
     *              Megvizsgálom ezen kívűl az is, hogy piramis - e > Ha igen , akkor létrehozom a riválisokat, akiknek a hírnevét , azt hogy "győztél" kiíratom  és kilépek a játékból.
     *              Mindezek után pedig 2 for ciklus segítségével kirajzolatom a pályán a játékos körüli ( 8 ) területeket , olyan módon, hogy ha olyan területre lép, ahol nincs mindenhol pályarész,
     *                  azaz a pálya szélén van, akkor azokat a területeket ignorálja.
     */
    public static void clickMove(int i , int j) {
        if ((i == FrameGen.playerX + 1 && j == FrameGen.playerY) || (i == FrameGen.playerX - 1 && j == FrameGen.playerY) || (j == FrameGen.playerY + 1 && i == FrameGen.playerX ) || (j == FrameGen.playerY - 1 && i == FrameGen.playerX )) {
            if (!FrameGen.segedMap[i][j].matches("tenger.png|hegy.png|tó.png")) {
                if(Inventory.energyCounter <= 0){
                    Random rand = new Random();
                    int szam = rand.nextInt(100);
                    if(szam < 8){
                            if(Inventory.memberCounter == 0) {
                                System.exit(0);
                                System.out.println("Vesztetél");
                            }
                            else{
                                Inventory.inventoryFrame.remove(Inventory.buttonMembers);
                                Inventory.buttonMembers = new JButton("Csapattárs: " + (Inventory.memberCounter -= 1));
                            }
                    }
                }
                FrameGen.playerX = i;
                FrameGen.playerY = j;
                if(FrameGen.segedMap[FrameGen.playerX][FrameGen.playerY].matches("falu.png")){
                    Random rand = new Random();
                    int szam = rand.nextInt(100);
                    if(szam <= 20) {
                        MembersChosseFrame.membersFrame.setVisible(false);
                        new MembersChosseFrame();
                    }
                    FaluShop.faluShopFrame.setVisible(false);
                    new FaluShop();
                }
                if(FrameGen.segedMap[FrameGen.playerX][FrameGen.playerY].matches("hajó.png")){
                    Inventory.energyCounter = 100;
                    Inventory.inventoryFrame.remove(Inventory.buttonEnergy);
                    Inventory.buttonEnergy = new JButton("Energia: " + Inventory.energyCounter);
                }
                if(FrameGen.segedMap[FrameGen.playerX][FrameGen.playerY].matches("pirmais.png")){
                    Karcsi karcsi = new Karcsi();
                    Zoli zoli = new Zoli();
                    Peti peti = new Peti();
                    System.out.println("Gratulálok, győztél!");
                    System.out.println(karcsi.getNév() + " hírneve: " + karcsi.getHírnév());
                    System.out.println(zoli.getNév() + " hírneve: " + zoli.getHírnév());
                    System.out.println(peti.getNév() + " hírneve: " + peti.getHírnév());
                    System.exit(0);
                }
                int from = -1 - Move.viewDistance;
                int to = 2 + Move.viewDistance;
                for (int a = from; a < to; a++) {
                    for (int b = from; b < to; b++) {
                        ImageIcon jatekos = new ImageIcon("játékos.png");
                        FrameGen.map[FrameGen.playerX][FrameGen.playerY].setIcon(FrameGen.seged.getIcon());
                        FrameGen.seged.setIcon(FrameGen.map[i][j].getIcon());
                        FrameGen.map[i][j].setIcon(jatekos);
                        if (FrameGen.playerY - a >= 30) {
                            continue;
                        } else if (FrameGen.playerY - a < 0) {
                            continue;
                        } else if (FrameGen.playerX - b >= 20) {
                            continue;
                        } else if (FrameGen.playerX - b < 0) {
                            continue;
                        } else {
                            ImageIcon feltolt = new ImageIcon(FrameGen.segedMap[FrameGen.playerX - b][FrameGen.playerY - a]);
                            FrameGen.map[FrameGen.playerX - b][FrameGen.playerY - a].setIcon(feltolt);
                        }
                    }
                }

            }
        }
    }

    /**
     * Ez az osztály felelős azért, hogy ha a pályában vagyunk ( azon van a fókusz ) , akkor az "i" betű lenyomására előhozza az Inventorynkat.
     */
    public static class InventoryOpen implements KeyListener {
        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_I) {
                new Inventory();
                Inventory.inventoryFrame.setVisible(true);
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {

        }
    }

    /**
     * Ez az osztály felelős azért, hogy ha a pályában vagyunk ( azon van a fókusz ) , akkor az "i" betű lenyomására eltüntesse az Inventorynkat.
     */
    public static class InventoryClose implements KeyListener{

        @Override
        public void keyTyped(KeyEvent e) {

        }

        @Override
        public void keyPressed(KeyEvent e) {
            if(e.getKeyCode() == KeyEvent.VK_I){
              Inventory.inventoryFrame.setVisible(false);
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {

        }
    }

    /**
     * Ez a metódus felelős azért, hogy a paraméterben kapott gombot áméretezze , a closeInventory és giveEnergy litenert hozzárendelje majd
     *  hozzáadja a paraméterben kapott containerhez és növelje a paraméterben kapott változót 50 - el , ami a gomb magassága. Ez jó elhelyezkedés miatt szükséges
     *  különben összezsugorodna az inventory.
     */
    public static void inventoryButtonFillUp(JButton button , Container container , int variable){
        button.setPreferredSize(new Dimension(100 , 50));
        button.setLocation(0 , 110 + variable);
        button.addKeyListener(Inventory.closeInventory);
        button.addActionListener(Inventory.giveEnergy);
        container.add(button);
        variable += 50;
    }

    /**
     * Ez a metódus felelős azért, hogy attól függően, hogy a játék kezdetén vagy a faluban vagyunk ( startingVariable = 0/1 )
     *      visszadjon egy véletlenszerűen kiválasztott csapattársat.
     * @return csapattárs
     */
    public static String chooseRandomVillageMember(){
        Random rand = new Random();
        int szam = rand.nextInt(3);
            if(MembersChosseFrame.startingVariable == 1) {
                if (szam == 0 ) {
                    return "Felderítő";
                }
                else if (szam == 1) {
                    return "Sámán";
                }
                else if (szam == 2) {
                    return "Bölcs";
                }
                else {
                    return null;
                }
            }
            if(MembersChosseFrame.startingVariable == 0){
                if(szam == 0){
                    return "Kereskedő";
                }
                else if(szam == 1){
                    return "Katona";
                }
                else if(szam == 2){
                    return "Szamár";
                }
                else{
                    return null;
                }
            }
            return null;
    }

    /**
     * Ez az osztály felelős azért, hogy a kiválaszott csapattársat létrehozza és implementálja azoknak előnyeit a megfelelő változók beállításával.
     * Mindezek mellett ellenörzőm, hogy :
     *      1, van e elég pénze a játékosnak, hogy új csapattárat vegyen fel ( 150 )
     *      2, rendelkezik-e már a maximum csapattárs létszámmal ( 3 )
     *          Ha egyik sem igaz, akkor újragenerálom a pénz és csapattárs gombot az inventoryban, úgy, hogy a pénzCounter - ből kivonom a csapattárs árát ( 150 )
     *              és a csapattársCounter - t 1 - el növelem.
     *          Ha valamelyik igaz, akkor hibaüzenetet jelenítek meg , attól függően, hogy melyik feltétel volt igaz.
     */
    public static class ChoosenMember implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            if(Inventory.goldCounter >= GroupMembers.getGoldCost()){
                if(Inventory.memberCounter != 3) {
                    Inventory.inventoryFrame.remove(Inventory.buttonGold);
                    Inventory.buttonGold = new JButton("Pénz:" + (Inventory.goldCounter -= GroupMembers.getGoldCost()));
                    Inventory.inventoryFrame.remove(Inventory.buttonMembers);
                    Inventory.buttonMembers = new JButton("Csapattárs:" + (Inventory.memberCounter += 1));
                }
                else if(Inventory.memberCounter == 3){
                    JOptionPane.showMessageDialog(null, "Elérted a maximum társak számát(3)!", "Hiba", JOptionPane.ERROR_MESSAGE);
                }
            }
            else if(Inventory.goldCounter < GroupMembers.getGoldCost()){
                JOptionPane.showMessageDialog(null, "Nincs elegendő aranyad új társ vásárlásához!", "Hiba", JOptionPane.ERROR_MESSAGE);
            }
            MembersChosseFrame.membersFrame.setVisible(false);
            if(MembersChosseFrame.memberName .equals("Bölcs")){
                Bölcs bölcs = new Bölcs();
                bölcs.setViszonyBoost(3);
                Inventory.viszonyBoost = bölcs.getViszonyBoost();
                Inventory.inventoryFrame.remove(Inventory.buttonViszony);
                Inventory.buttonViszony = new JButton("Viszony: " + (Inventory.viszonyBoost + Inventory.viszonyCounter));
            }
            else if(MembersChosseFrame.memberName.equals("Felderítő")){
                Felderítő felderítő = new Felderítő();
                felderítő.setViewDistanceUpgrade(1);
                Move.viewDistance = felderítő.getViewDistanceUpgrade();
            }
            else if(MembersChosseFrame.memberName.equals("Katona")){
                Katona katona = new Katona();
                katona.setKatonaWhiskeySkill(20);
                giveEnergy.whiskeyUpgrade = katona.getKatonaWhiskeySkill();
            }
            else if(MembersChosseFrame.memberName.equals("Kereskedő")){
                Kereskedő kereskedő = new Kereskedő();
                kereskedő.setGoldDecrese(kereskedő.generateRandomInt());
                Shop.kereskedőUpgrade = kereskedő.getGoldDecrese();
            }
            else if(MembersChosseFrame.memberName.equals("Szamár")){
                Szamár szamár = new Szamár();
                szamár.setInventorySlotUpgrade(2);
                Inventory.inventoryUpgrade = szamár.getInventorySlotUpgrade();
            }
            else if(MembersChosseFrame.memberName.equals("Sámán")){
                Sámán sámán = new Sámán();
                sámán.setKábítószerEnergyUpgrade(20);
                giveEnergy.kábítószerUpgrade = sámán.getKábítószerEnergyUpgrade();
            }

        }
    }

    /**
     * Ez a metódusom felel azért, hogy amikor rálépek egy falura, akkor megjelent bolt frame-be bekerüljenek a falusi tárgyak.
     * A paraméterben kapott szám alapján hoz létre ételt vagy tárgyat és állítja be az adott változókat.
     *      Ezt a metódust egy for cikluson belül hívom meg a FaluShop.buttons() metóduson belül , amely i = 0-5  és paraméterül mindig az i - t adom , így minden elem szerepelni fog a boltban.
     * @param szam
     * @return String
     */
    public static String chosenFaluShopFood(int szam){
        if(szam == 0){
            Inventory.kötél = new Kötél();
            return Inventory.kötél.getName()  + "           ár: " + Inventory.kötél.getÁr();
        }
        if(szam == 1){
            Inventory.fáklya = new Fáklya();
            return Inventory.fáklya.getName() + "           ár: " + Inventory.fáklya.getÁr();
        }
        if(szam == 2){
            Inventory.gyümölcs = new Gyümölcs();
            return Inventory.gyümölcs.getName() + "           ár: " + Inventory.gyümölcs.getÁr();
        }
        if(szam == 3){
            Inventory.hús = new Hús();
            return Inventory.hús.getName() + "           ár: " + Inventory.hús.getÁr();
        }
        if(szam == 4){
            Inventory.kábítószer = new Kábítószer();
            return Inventory.kábítószer.getName() + "           ár: " + Inventory.kábítószer.getÁr();
        }
        else{
            return null;
        }
    }

    /**
     * Ez a metódusom felel azért, hogy amikor rálépek egy falura, akkor megjelent bolt frame-be bekerüljenek a falusi tárgyak.
     * A paraméterben kapott szám alapján hoz létre ételt vagy tárgyat és állítja be az adott változókat.
     *      Ezt a metódust egy for cikluson belül hívom meg a Shop.buttons() metóduson belül , amely i = 0-7  és paraméterül mindig az i - t adom , így minden elem szerepelni fog a boltban.
     * @param szam
     * @return String
     */
    public static String chosenStartFood(int szam){
        if(szam == 0){
            Inventory.kötél = new Kötél();
            return Inventory.kötél.getName()  + "           ár: " + Inventory.kötél.getÁr();
        }
        if(szam == 1){
            Inventory.bozótvágó = new Bozótvágó();
            return Inventory.bozótvágó.getName() + "           ár: " + Inventory.bozótvágó.getÁr();
        }
        if(szam == 2){
            Inventory.fáklya = new Fáklya();
            return Inventory.fáklya.getName() + "           ár: " + Inventory.fáklya.getÁr();
        }
        if(szam == 3){
            Inventory.üveggolyó = new Üveggolyó();
            return Inventory.üveggolyó.getName() + "           ár: " + Inventory.üveggolyó.getÁr();
        }
        if(szam == 4){
            Inventory.hús = new Hús();
            return Inventory.hús.getName() + "           ár: " + Inventory.hús.getÁr();
        }
        if(szam == 5){
            Inventory.whiskey = new Whiskey();
            return Inventory.whiskey.getName() + "           ár: " + Inventory.whiskey.getÁr();
        }
        if(szam == 6){
            Inventory.csokoládé = new Csokoládé();
            return Inventory.csokoládé.getName() + "           ár: " + Inventory.csokoládé.getÁr();
        }
        else{
            return null;
        }
    }

    /**
     * Ez a metódusom felel azért, hogy a paraméterben kapott tárgy adatait legkérdezve újragenerálja a pénz gombomat
     *   és növelje az inventoryCounter változót. > Vásárlás
     * @param item
     * @param kereskedőUpgrade
     */
    public static void goldButtonChngerItems(Items item , int kereskedőUpgrade) {
        Inventory.goldCounter -= item.getÁr();
        Inventory.goldCounter += kereskedőUpgrade;
        Inventory.inventoryFrame.remove(Inventory.buttonGold);
        Inventory.buttonGold = new JButton("Pénz: " + Inventory.goldCounter);
        Move.inventoryCounter ++;
    }
    /**
     * Ez a metódusom felel azért, hogy a paraméterben kapott étel adatait legkérdezve újragenerálja a pénz gombomat
     *   és növelje az inventoryCounter változót. > Vásárlás
     * @param item
     * @param kereskedőUpgrade
     */
    public static void goldButtonChngerFoods(Foods item , int kereskedőUpgrade) {
        Inventory.goldCounter -= item.getÁr();
        Inventory.goldCounter += kereskedőUpgrade;
        Inventory.inventoryFrame.remove(Inventory.buttonGold);
        Inventory.buttonGold = new JButton("Pénz: " + Inventory.goldCounter);
        Move.inventoryCounter ++;
    }

    /**
     * Ez az osztályom felelős azért, hogy a falu boltjában a rákattintott elem belekerüljön az Inventory - ba és az egyes tárgyak ára le legyen vonva a játékos pénzéből.
     *  Ha nincs elég aranya a játékonak , hogy megvegye az adott tárgyat, akkor hibaüzenetet , kap.
     *  Ha sikeres a vásárlás, akkor az adott tárgy ( mindig 1 darab ) belekerül az Inventoryra egy slotra ( 7 ig ) , különben új slotra.
     */
    public static class ChoosenFaluShopFood implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().contains("Kötél")) {
                if (Inventory.goldCounter - Inventory.kötél.getÁr() > 0 && Inventory.kötélCounter < Inventory.kötél.getMennyiség()) {
                    goldButtonChngerItems(Inventory.kötél, Shop.kereskedőUpgrade);
                    Inventory.kötélCounter += 1;
                    if (Inventory.kötélCounter < 8) {
                        Inventory.inventoryFrame.remove(Inventory.buttonKötél);
                        Inventory.buttonKötél = new JButton(Inventory.kötél.getName() + Inventory.kötélCounter);
                    }
                    if (Inventory.kötélCounter == 8) { inventoryButtonFillUp(new JButton(Inventory.kötél.getName() + "+") , Inventory.container , Inventory.yVariable ); }
                }
                else{ JOptionPane.showMessageDialog(null, "Nincs elegendő aranyad kötél vásárlásához vagy elfogyott a boltból!", "Hiba", JOptionPane.ERROR_MESSAGE); }
            }
            if (e.getActionCommand().contains("Fáklya")){
                if(Inventory.goldCounter - Inventory.fáklya.getÁr() > 0 && Inventory.fáklyaCounter < Inventory.fáklya.getMennyiség()) {
                    goldButtonChngerItems(Inventory.fáklya , Shop.kereskedőUpgrade);
                    Inventory.fáklyaCounter += 1;
                    if (Inventory.fáklyaCounter < 8) {
                        Inventory.inventoryFrame.remove(Inventory.buttonFáklya);
                        Inventory.buttonFáklya = new JButton(Inventory.fáklya.getName() + Inventory.fáklyaCounter);
                    }
                    if (Inventory.fáklyaCounter == 8) { inventoryButtonFillUp(new JButton(Inventory.fáklya.getName() + "+") , Inventory.container , Inventory.yVariable );}
                }
                else{ JOptionPane.showMessageDialog(null, "Nincs elegendő aranyad fáklya vásárlásához vagy elfogyott a boltból!", "Hiba", JOptionPane.ERROR_MESSAGE); }
            }
            if (e.getActionCommand().contains("Gyümölcs")){
                if(Inventory.goldCounter - Inventory.gyümölcs.getÁr() > 0 && Inventory.gyümölcsCounter < Inventory.gyümölcs.getMennyiség()) {
                    goldButtonChngerFoods(Inventory.gyümölcs , Shop.kereskedőUpgrade);
                    Inventory.gyümölcsCounter += 1;
                    if (Inventory.gyümölcsCounter < 8) {
                        Inventory.inventoryFrame.remove(Inventory.buttonGyümölcs);
                        Inventory.buttonGyümölcs = new JButton(Inventory.gyümölcs.getName() + Inventory.gyümölcsCounter);
                    }
                    if (Inventory.gyümölcsCounter == 8) { inventoryButtonFillUp(new JButton(Inventory.gyümölcs.getName() + "+") , Inventory.container , Inventory.yVariable); }
                }
                else{ JOptionPane.showMessageDialog(null, "Nincs elegendő aranyad gyümölcs vásárlásához vagy elfogyott a boltból!", "Hiba", JOptionPane.ERROR_MESSAGE); }
            }
            if (e.getActionCommand().contains("Hús")){
                if(Inventory.goldCounter - Inventory.hús.getÁr() > 0 && Inventory.húsCounter < Inventory.hús.getMennyiség()) {
                    goldButtonChngerFoods(Inventory.hús , Shop.kereskedőUpgrade);
                    Inventory.húsCounter += 1;
                    if (Inventory.húsCounter < 8) {
                        Inventory.inventoryFrame.remove(Inventory.buttonHús);
                        Inventory.buttonHús = new JButton(Inventory.hús.getName() + Inventory.húsCounter);
                    }
                    if (Inventory.húsCounter == 8) { inventoryButtonFillUp(new JButton(Inventory.hús.getName() + "+") , Inventory.container , Inventory.yVariable); }
                }
                else{ JOptionPane.showMessageDialog(null, "Nincs elegendő aranyad hús vásárlásához vagy elfogyott a boltból!", "Hiba", JOptionPane.ERROR_MESSAGE); }
            }
            if (e.getActionCommand().contains("Kábítószer")){
                if(Inventory.goldCounter - Inventory.kábítószer.getÁr() > 0 && Inventory.kábítószerCounter < Inventory.kábítószer.getMennyiség()) {
                    goldButtonChngerFoods(Inventory.kábítószer , Shop.kereskedőUpgrade);
                    Inventory.kábítószerCounter += 1;
                    if (Inventory.kábítószerCounter < 8) {
                        Inventory.inventoryFrame.remove(Inventory.buttonKábítószer);
                        Inventory.buttonKábítószer = new JButton(Inventory.kábítószer.getName() + Inventory.kábítószerCounter);
                    }
                    if (Inventory.kábítószerCounter == 8) { inventoryButtonFillUp(new JButton(Inventory.kábítószer.getName() + "+") , Inventory.container , Inventory.yVariable); }
                }
                else{ JOptionPane.showMessageDialog(null, "Nincs elegendő aranyad kábítószer vásárlásához vagy elfogyott a boltból!", "Hiba", JOptionPane.ERROR_MESSAGE); }
            }
        }
    }

    /**
     * Ez az osztályom felelős azért, hogy a kezdeti boltban a rákattintott elem belekerüljön az Inventory - ba és az egyes tárgyak ára le legyen vonva a játékos pénzéből.
     *  Ha nincs elég aranya a játékonak , hogy megvegye az adott tárgyat, akkor hibaüzenetet , kap.
     *  Ha sikeres a vásárlás, akkor az adott tárgy ( mindig 1 darab ) belekerül az Inventoryra egy slotra ( 7 ig ) , különben új slotra.
     */
    public static class ChoosenFood implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().contains("Kötél")) {
                if (Inventory.goldCounter - Inventory.kötél.getÁr() > 0 && Inventory.kötélCounter < Inventory.kötél.getMennyiség()) {
                    goldButtonChngerItems(Inventory.kötél, Shop.kereskedőUpgrade);
                    Inventory.kötélCounter += 1;
                    if (Inventory.kötélCounter < 8) {
                        Inventory.inventoryFrame.remove(Inventory.buttonKötél);
                        Inventory.buttonKötél = new JButton(Inventory.kötél.getName() + Inventory.kötélCounter);
                    }
                    if (Inventory.kötélCounter == 8) { inventoryButtonFillUp(new JButton(Inventory.kötél.getName() + "+") , Inventory.container , Inventory.yVariable ); }
                }
                else{ JOptionPane.showMessageDialog(null, "Nincs elegendő aranyad kötél vásárlásához vagy elfogyott a boltból!", "Hiba", JOptionPane.ERROR_MESSAGE); }
            }
            if (e.getActionCommand().contains("Bozótvágó")){
                if(Inventory.goldCounter - Inventory.bozótvágó.getÁr() > 0 && Inventory.bozótvágóCounter < Inventory.bozótvágó.getMennyiség()) {
                    goldButtonChngerItems(Inventory.bozótvágó , Shop.kereskedőUpgrade);
                    Inventory.bozótvágóCounter += 1;
                    if (Inventory.bozótvágóCounter < 8) {
                        Inventory.inventoryFrame.remove(Inventory.buttonBozótvágó);
                        Inventory.buttonBozótvágó = new JButton(Inventory.bozótvágó.getName() + Inventory.bozótvágóCounter);
                    }
                    if (Inventory.bozótvágóCounter == 8) { inventoryButtonFillUp(new JButton(Inventory.bozótvágó.getName() + "+") , Inventory.container , Inventory.yVariable ); }
                }
                else{ JOptionPane.showMessageDialog(null, "Nincs elegendő aranyad bozótvágó vásárlásához vagy elfogyott a boltból!", "Hiba", JOptionPane.ERROR_MESSAGE); }
            }
            if (e.getActionCommand().contains("Fáklya")){
                if(Inventory.goldCounter - Inventory.fáklya.getÁr() > 0 && Inventory.fáklyaCounter < Inventory.fáklya.getMennyiség()) {
                    goldButtonChngerItems(Inventory.fáklya , Shop.kereskedőUpgrade);
                    Inventory.fáklyaCounter += 1;
                    if (Inventory.fáklyaCounter < 8) {
                        Inventory.inventoryFrame.remove(Inventory.buttonFáklya);
                        Inventory.buttonFáklya = new JButton(Inventory.fáklya.getName() + Inventory.fáklyaCounter);
                    }
                    if (Inventory.fáklyaCounter == 8) { inventoryButtonFillUp(new JButton(Inventory.fáklya.getName() + "+") , Inventory.container , Inventory.yVariable );}
                }
                else{ JOptionPane.showMessageDialog(null, "Nincs elegendő aranyad fáklya vásárlásához vagy elfogyott a boltból!", "Hiba", JOptionPane.ERROR_MESSAGE); }
            }
            if (e.getActionCommand().contains("Üveggolyó")){
                if(Inventory.goldCounter - Inventory.üveggolyó.getÁr() > 0 && Inventory.üveggolyóCounter < Inventory.üveggolyó.getMennyiség()) {
                    goldButtonChngerItems(Inventory.üveggolyó , Shop.kereskedőUpgrade);
                    Inventory.üveggolyóCounter += 1;
                    if (Inventory.üveggolyóCounter < 8) {
                        Inventory.inventoryFrame.remove(Inventory.buttonÜveggolyó);
                        Inventory.buttonÜveggolyó = new JButton(Inventory.üveggolyó.getName() + Inventory.üveggolyóCounter);
                    }
                    if (Inventory.üveggolyóCounter == 8) { inventoryButtonFillUp(new JButton(Inventory.üveggolyó.getName() + "+") , Inventory.container , Inventory.yVariable); }
                }
                else{ JOptionPane.showMessageDialog(null, "Nincs elegendő aranyad üveggolyó vásárlásához vagy elfogyott a boltból!", "Hiba", JOptionPane.ERROR_MESSAGE); }
            }
            if (e.getActionCommand().contains("Hús")){
                if(Inventory.goldCounter - Inventory.hús.getÁr() > 0 && Inventory.húsCounter < Inventory.hús.getMennyiség()) {
                    goldButtonChngerFoods(Inventory.hús , Shop.kereskedőUpgrade);
                    Inventory.húsCounter += 1;
                    if (Inventory.húsCounter < 8) {
                        Inventory.inventoryFrame.remove(Inventory.buttonHús);
                        Inventory.buttonHús = new JButton(Inventory.hús.getName() + Inventory.húsCounter);
                    }
                    if (Inventory.húsCounter == 8) { inventoryButtonFillUp(new JButton(Inventory.hús.getName() + "+") , Inventory.container , Inventory.yVariable); }
                }
                else{ JOptionPane.showMessageDialog(null, "Nincs elegendő aranyad hús vásárlásához vagy elfogyott a boltból!", "Hiba", JOptionPane.ERROR_MESSAGE); }
            }
            if (e.getActionCommand().contains("Whiskey")){
                if(Inventory.goldCounter - Inventory.whiskey.getÁr() > 0 && Inventory.whiskeyCounter < Inventory.whiskey.getMennyiség()) {
                    goldButtonChngerFoods(Inventory.whiskey , Shop.kereskedőUpgrade);
                    Inventory.whiskeyCounter += 1;
                    if (Inventory.whiskeyCounter < 8) {
                        Inventory.inventoryFrame.remove(Inventory.buttonWhiskey);
                        Inventory.buttonWhiskey = new JButton(Inventory.whiskey.getName() + Inventory.whiskeyCounter);
                    }
                    if (Inventory.whiskeyCounter == 8) { inventoryButtonFillUp(new JButton(Inventory.whiskey.getName() + "+") , Inventory.container , Inventory.yVariable ); }
                }
                else{ JOptionPane.showMessageDialog(null, "Nincs elegendő aranyad whiskey vásárlásához vagy elfogyott a boltból!", "Hiba", JOptionPane.ERROR_MESSAGE); }
            }
            if (e.getActionCommand().contains("Csokoládé")){
                if(Inventory.goldCounter - Inventory.csokoládé.getÁr() > 0 && Inventory.csokoládéCounter < Inventory.csokoládé.getMennyiség()) {
                    goldButtonChngerFoods(Inventory.csokoládé , Shop.kereskedőUpgrade);
                    Inventory.csokoládéCounter += 1;
                    if (Inventory.csokoládéCounter < 8) {
                        Inventory.inventoryFrame.remove(Inventory.buttonCsokoládé);
                        Inventory.buttonCsokoládé = new JButton(Inventory.csokoládé.getName() + Inventory.csokoládéCounter);
                    }
                    if (Inventory.csokoládéCounter == 8) { inventoryButtonFillUp(new JButton(Inventory.csokoládé.getName() + "+") , Inventory.container , Inventory.yVariable); }
                }
                else{ JOptionPane.showMessageDialog(null, "Nincs elegendő aranyad csokoládé vásárlásához vagy elfogyott a boltból!", "Hiba", JOptionPane.ERROR_MESSAGE); }
            }
        }
    }


    /**
     * Ez a metódus felel azért, hogy az Inventoryban szereplő élelmiszerekre kattintva, +enegiát kapjunk.
     *  Ha a válaszott élelmiszer energia értéke és a játékos enrgiája több lenne, mint 100, akkor nem tudjuk megenni az adott élelmiszert,
     *      egyébként pedig, úgyragenerálom az energia gombot és a válaszott élelmiszer energia értékével növelem az energyCountert.
     */
    public static class giveEnergy implements ActionListener{
        static int whiskeyUpgrade = 0;
        static int kábítószerUpgrade = 0;
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().equals(Inventory.whiskey.getName() + Inventory.whiskeyCounter)) {
                if (Inventory.energyCounter + Inventory.whiskey.getEnergyFillUp() < 100) {
                    Inventory.energyCounter += Inventory.whiskey.getEnergyFillUp() + whiskeyUpgrade;
                    Inventory.inventoryFrame.remove(Inventory.buttonEnergy);
                    Inventory.buttonEnergy = new JButton("Energia: " + Inventory.energyCounter);
                    Inventory.whiskeyCounter -= 1;
                    Inventory.inventoryFrame.remove(Inventory.buttonWhiskey);
                    Inventory.buttonWhiskey = new JButton(Inventory.whiskey.getName() + Inventory.whiskeyCounter);
                }
            }
            else if (e.getActionCommand().equals(Inventory.hús.getName() + Inventory.húsCounter)) {
                if (Inventory.energyCounter + Inventory.hús.getEnergyFillUp() < 100) {
                    Inventory.energyCounter += Inventory.hús.getEnergyFillUp();
                    Inventory.inventoryFrame.remove(Inventory.buttonEnergy);
                    Inventory.buttonEnergy = new JButton("Energia: " + Inventory.energyCounter);
                    Inventory.húsCounter -= 1;
                    Inventory.inventoryFrame.remove(Inventory.buttonHús);
                    Inventory.buttonHús = new JButton(Inventory.hús.getName() + Inventory.húsCounter);
                }
            }
            else if(e.getActionCommand().equals(Inventory.csokoládé.getName() + Inventory.csokoládéCounter)) {
                if (Inventory.energyCounter + Inventory.csokoládé.getEnergyFillUp() < 100) {
                    Inventory.energyCounter += Inventory.csokoládé.getEnergyFillUp();
                    Inventory.inventoryFrame.remove(Inventory.buttonEnergy);
                    Inventory.buttonEnergy = new JButton("Energia: " + Inventory.energyCounter);
                    Inventory.csokoládéCounter -= 1;
                    Inventory.inventoryFrame.remove(Inventory.buttonCsokoládé);
                    Inventory.buttonCsokoládé = new JButton(Inventory.csokoládé.getName() + Inventory.csokoládéCounter);
                }
            }
            else if(e.getActionCommand().equals(Inventory.kábítószer.getName() + Inventory.kábítószerCounter)) {
                if (Inventory.energyCounter + Inventory.kábítószer.getEnergyFillUp() < 100) {
                    Inventory.energyCounter += Inventory.kábítószer.getEnergyFillUp() + kábítószerUpgrade;
                    Inventory.inventoryFrame.remove(Inventory.buttonEnergy);
                    Inventory.buttonEnergy = new JButton("Energia: " + Inventory.energyCounter);
                    Inventory.kábítószerCounter -= 1;
                    Inventory.inventoryFrame.remove(Inventory.buttonKábítószer);
                    Inventory.buttonKábítószer = new JButton(Inventory.kábítószer.getName() + Inventory.kábítószerCounter);
                }
            }
            else if(e.getActionCommand().equals(Inventory.gyümölcs.getName() + Inventory.gyümölcsCounter)) {
                if (Inventory.energyCounter + Inventory.gyümölcs.getEnergyFillUp() < 100) {
                    Inventory.energyCounter += Inventory.gyümölcs.getEnergyFillUp();
                    Inventory.inventoryFrame.remove(Inventory.buttonEnergy);
                    Inventory.buttonEnergy = new JButton("Energia: " + Inventory.energyCounter);
                    Inventory.gyümölcsCounter -= 1;
                    Inventory.inventoryFrame.remove(Inventory.buttonGyümölcs);
                    Inventory.buttonGyümölcs = new JButton(Inventory.gyümölcs.getName() + Inventory.gyümölcsCounter);
                }
            }
        }
    }
}

