package Riválisok;

import java.util.Random;

/**
 * Riválisok ősosztály.
 */

public class Riválisok {
    private String név;
    private int hírnév;

    public int getHírnév() {
        return hírnév;
    }

    public void setHírnév(int hírnév) {
        this.hírnév = hírnév;
    }

    public String getNév() {
        return név;
    }

    public void setNév(String név) {
        this.név = név;
    }

    public int randomHírnévGenerate(){
        Random rand = new Random();
        int szam = rand.nextInt(3000);
        return szam;
    }
}
