package Tárgyak;

import java.util.Random;

public class Items {

    /**
     * @param ár az egyes tárgyak ára.
     * @param name az egyes tárgyak neve.
     * @param mennyiség az egyes tárgyak darabszáma.
     */

    private int ár = 0;
    private String name ;
    private int mennyiség;

    public int getÁr() {
        return ár;
    }

    public void setÁr(int ár) {
        this.ár = ár;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMennyiség() {
        return mennyiség;
    }

    public void setMennyiség(int mennyiség) {
        this.mennyiség = mennyiség;
    }

    /**
     * 20 - 80 között egy véletlenszerű ár generálását hajtja végre ez a metódus.
     */

    public static int generateRandomPrize(){
        int boundMin = 20;
        int boundMax = 80;
        Random rand = new Random();
        int szam = rand.nextInt(boundMax - boundMin) + boundMin;
        return szam;
    }

    /**
     * 1 - 8 között egy véletlenszerű mennyiség generálását hajtja végre ez a metódus.
     */

    public static int generateRandomAmount(){
        int boundMin = 1;
        int boundMax = 8;
        Random rand = new Random();
        int szam = rand.nextInt(boundMax - boundMin) + boundMin;
        return szam;
    }
}
