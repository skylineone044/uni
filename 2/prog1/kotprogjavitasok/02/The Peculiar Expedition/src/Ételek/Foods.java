package Ételek;

import java.util.Random;

public class Foods {

    /**
     * @param energyFillUp az egyes ételek mennyi energiát töltenek vissza elfogyasztáskor
     * @param name az egyes ételek neve.
     * @param ár az egyes ételek ára.
     * @param mennyiség az egyes ételek darabszáma.
     */

    private int energyFillUp = 0;
    private String name;
    private int ár;
    private int mennyiség;

    public int getEnergyFillUp() {
        return energyFillUp;
    }

    public void setEnergyFillUp(int energyFillUp) {
        this.energyFillUp = energyFillUp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getÁr() {
        return ár;
    }

    public void setÁr(int ár) {
        this.ár = ár;
    }

    public int getMennyiség() {
        return mennyiség;
    }

    public void setMennyiség(int mennyiség) {
        this.mennyiség = mennyiség;
    }

    /**
     * 20 - 80 között egy véletlenszerű ár generálását hajtja végre ez a metódus.
     */

    public static int generateRandomPrize(){
        int boundMin = 20;
        int boundMax = 80;
        Random rand = new Random();
        int szam = rand.nextInt(boundMax - boundMin) + boundMin;
        return szam;
    }

    /**
     * 1 - 8 között egy véletlenszerű mennyiség generálását hajtja végre ez a metódus.
     */

    public static int generateRandomAmount(){
        int boundMin = 1;
        int boundMax = 8;
        Random rand = new Random();
        int szam = rand.nextInt(boundMax - boundMin) + boundMin;
        return szam;
    }
}
