# harmadik kotprog  55+38=93

## notes

- a readme nagyon hianyos
- nincs külön leirva sehova hogy mi mi a terkepen...
- a classokon nincs jaadoc, csak a methodokon
- az iranyitashoz ki kell irni merre akarsz meddi (nem wasd >:| )
- ha nincs senki a csapatban, es Whiskey-t iszunk, akkor a
  ramdomnak 0 argot adunk -> IllegalArgumentExcepton

## Éertékelés

### funcitonal  5+5+5+5+5+5+5+5+5+5+5+0=55

- A térkép vízből és szárazföldből áll. A pálya egyik oldalán tenger van.
  A tavak körül nedves a terület. Vannak hegyek, barlangok, oltárok,
  dzsungelek és arany piramis - ***5/5%***
- A térképnek csak azon részeit látjuk, amelyek szomszédosak, vagy már jártunk
  ott, a többit nem - ***5/5%***
- A játékos tud mozogni a pályán. A játékos nem tud kilépni a pályáról, és a
  nem járható területekre nem tud rálépni (tenger, tó, hegy). A lépésekért a
  játékos energiát fizet a megfelelő módon számolva. - ***5/5%***
- Ha a játékos energiája elfogy, minden csapattag lépésenként 8% eséllyel
  elhagyja a csapatot. Ha a felfedező is elhagyja a csapatot,
  a játékos veszít - ***5/5%***
- Meg van valósítva az inventory, a felfedező 8 slottal rendelkezik. Egy slotra
  azonos tárgyak kerülhetnek, típusonként maximum 7 darab. Ha a játékos több
  tárgyat cipel, akkor a mozgási költség slotonként 20%-kal növekszik - ***5/5%***
- Rendelkezésre álnak élelmiszerek (gyümölcs, hús, csokoládé, kábítószer,
  whiskey). Ezeket el lehet fogyasztani és plusz energiát adnak - ***5/5%***
- Kábítószer és whiskey esetében a függőségek meg vannak valósítva, 30 kör után a
  megadott eséllyel kiléphet az adott csapattag. A függőséget meg lehet szűntetni
  a kiváltó élelmiszerrel - ***5/5%***
- Minden küldetés esetén a játék ajánl egy csapattársat, akit felvehetünk. A
  csapattárs lehet kereskedő, katona, szamár, amelyek előnyei is
  implementálva vannak - ***5/5%***
  // a kereskedo cuccal pont forditva van, de az nem nagy cucc, megadom
- A falvak meg vannak valósítva, ahol különböző csapattársakat vehetünk fel aranyért
  cserébe. A küldetések végén a játékos eladhatja vagy eladományozhatja a kincseket.
  A riválisok által elért eredmények meg vannak jelenítve - ***5/5%***
- Lehetőség van küldetések közötti vásárlásra (kötél, bozótvágó, fáklya, üveggolyó,
  hús, whiskey, csokoládé), illetve a falvakban lehet vásárolni kötelet, fáklyát,
  gyümölcsöt, húst és kábítószert - ***5/5%***
- A katasztrófák és átkok meg vannak valósítva a megadott szempontok szerint - ***5/5%***
- Van lehetőség a térkép fájlból való beolvasására, de a játék működik
  anélkül is - ***0/5%***
  // en erre nem talaltam opciot, sehol se, a readme se ir errol semmit

### implementation  5+5+5+5+5+5+3+5=38

- A program tartalmaz legalább 5 különböző, értelmes osztályt, amelyek egy-egy,
  a játékhoz kapcsolódó entitást írnak le - ***5/5%***
- A programok az osztályok funkcionalitásuk alapján package-ekbe vannak
  rendezve - ***5/5%***
- A programban szerepel legalább 2 olyan osztály, aminek van több, értelmes
  gyerekosztálya - ***5/5%***
- Minden osztálynak 1 (egy) meghatározott feladata van, egy osztály nem végez több,
  lényegében különböző műveletet - ***5/5%*** // a player class kicsit vegyes,
  // de szerintem az meg belefer
- Az osztályok, adattagok és metódusok nevei konvenció szerint vannak elnevezve,
  a nevek tükrözik az osztályok, metódusok, adattagok feladatait. A program
  megfelelő módon van indentálva - ***5/5%***
- Nincsenek a programban hosszú metódusok. A programban sehol nincs 100 sornál
  hosszabb metódus, és a metódusok nagy része kevesebb, mint 50 sorból áll. Ezek
  alól kivételt képeznek a GUI-val rendelkező programok esetén a grafikus elemek
  létrehozására, megjelenítésére szolgáló metódusok - ***5/5%***
  // csak par method hosszabb mint 50 sor
- A program JavaDoc dokumentációval el van látva. Az osztályok mindegyike meg van
  magyarázva JavaDoc dokumentációval, és a metódusoknak is legalább a 70%-a (a getter
  és setter metódusokon kívül) - ***3/5%***
  // a methodok jok, de a classokon nincs javadoc
- A program fel van készülve érvénytelen felhasználói inputokra. Ezek vagy meg vannak
  tiltva, vagy le vannak kezelve. Pl. nem létezik a megadott fájl, olyan cselekvést
  szeretnénk végrehajtani, ami az adott helyzetben nem megengedett, szám helyett
  szöveget adunk meg, nem a megfelelő értéktartományból írunk be értékeket, stb.
  Az ilyen esetek nagy része le van kezelve - ***5/5%***
  // a dolgok nagyresze le van kezelve
