import Expeditor.Player;
import Expeditor.Rival;
import Expeditor.Ship;
import Map.Map;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Controller {
	private Scanner sc = new Scanner(System.in);
	private FileIO F = new FileIO();
//	private Map map = new Map(F.readIn(),F.getM());
	private List<Map> maps = new ArrayList<Map>();
	private Player player = new Player();
	private Ship ship = new Ship();
	private List<Rival> rivals = new ArrayList<>();


	private Mission m;

	/**
	 * letrehozza a jatekos rivalisait es egyet a jatekosnak
	 */
	public Controller(){
		rivals.add(new Rival("John"));
		rivals.add(new Rival("Will"));
		rivals.add(new Rival("Rick"));
		rivals.add(new Rival("Gary"));
		rivals.add(new Rival("Alan"));
		rivals.add(new Rival("Te"));
	}

	/**
	 * fomenukent szolgalo iranyitas, kuldetes inditasa es a vegeredmeny kiiratasa a fo dolga
	 */
	public void menuPrompt() {
		F = new FileIO();
		maps = F.getMaps();
		System.out.println(maps.size());
		while (true) {
			System.out.println("[start][kilepes]");
			switch (sc.nextLine()){
				case "start" ->{
					for (int i=0;i<maps.size();i++){
						m=new Mission(maps.get(i),player,ship);
						m.start();
						displayRivals();
					}
					System.out.println("Nyertes: "+rivals.get(0).getName()+"\nEnter a folytatashoz...");
					sc.nextLine();
				}
				case "kilepes" -> System.exit(0);
				default -> System.out.println("Nem ertettem a parancsot!");
			}
		}
	}

	/**
	 * pontokat sozt random a rivalisoknak, a jatekoset beallitja a jatekosera
	 * csokkenosorba helyezi oket majd kiiratja a kepernyore
	 */
	private void displayRivals(){
		for (Rival r:rivals) {
			if (r.getName()!="Te")
				r.addReputation();
			else
				r.setReputation(player.getReputation());
		}
		for(int i=0;i<rivals.size();i++) {
			for (int j=0;j<rivals.size();j++) {
				if(rivals.get(i).getReputation()>rivals.get(j).getReputation()) {
					Rival temp = rivals.get(i);
					rivals.set(i,rivals.get(j));
					rivals.set(j,temp);
				}
			}
		}
		int i=1;
		for (Rival r:rivals) {
			System.out.println(i+". "+r.getName()+", Hirnev: "+r.getReputation());
			i++;
		}
		System.out.print("Enter a folytatashoz...");
		sc.nextLine();
	}
}
