package Expeditor.InventoryPck;

import Expeditor.Player;
import Items.Item;
import Map.Map;
import Map.Tiles.Tile;

import java.util.ArrayList;
import java.util.List;

public class Inventory {
    private int maxInventorySlots=8;
    private final int maxDb=7;
    private final List<Slot> slots = new ArrayList<>();
    public Inventory() {

    }

    /**mindig uj slotra rakja
     *
     * @param t Item amit belerakunk az inventoryban
     * @param db az item db szama hogy mennyit rakunk bele
     *
     */
    public void newItem(Item t, int db) {
        if (db<=0) return;
        if (db>maxDb || t.isBig()) {
            while (db>=maxDb && !t.isBig()) {
                slots.add(new Slot(t, maxDb));
                db-=maxDb;
            }
            while (t.isBig() && db!=0) {
                slots.add(new Slot(t, 1));
                db--;
            }
        } else slots.add(new Slot(t,db));
    }

    /** feltetelezzuk hogy mar van az inventoryban,
     * ha van akkor feltolti mig telenem lesz
     * ha van meg item akkor uj slotra rakja
     * @param t Item amit belerakunk az inventoryban
     * @param db az item db szama hogy mennyit rakunk bele
     */
    public void addItem(Item t, int db) {
        while (t.isBig() && db!=0) {
            slots.add(new Slot(t, 1));
            db--;
        }
        Slot s = getSlotWithLowestCount(t.getName());
        if (s==null) {
            newItem(t, db);
            return;
        }
        while (db!=0){
            if (s.getDb()==maxDb) {
                newItem(t, db);
                return;
            }
            s.setDb(s.getDb() + 1);
            db--;
        }
    }

    /** levon egyet a slotbol ahol van olyan item, ha 0 lesz a slot akkor toroljuk a slotot
     * @param id az item idje amit torolni akarunk
     */
    public void removeItem(int id) {
        Slot s =getSlotWithLowestCount(id);
        s.setDb(s.getDb()-1);
        if(s.getDb()==0) slots.remove(s);
    }

    public int getMaxInventorySlots() {
        return maxInventorySlots;
    }
    public void setMaxInventorySlots(int maxInventorySlots) {
        this.maxInventorySlots = maxInventorySlots;
    }

    /**
     * @return az itemet tarolo slotok szama
     */
    public int getNumberOfUsedSlot() {
        return slots.size();
    }

    /**
     * @param id targy id amit keresunk az inventorban
     * @return a slot amin taroljuk
     */
    public Slot getSlotByItemId(int id){
        for (Slot s: slots){
            if (s.getItem().getId()==id) return s;
        }
        return null;
    }

    /**
     * @param itemName targy nev amit keresunk az inventoriban
     * @return a slot amin taroljuk
     */
    public Slot getSlotByItemName(String itemName){
        for (Slot s: slots){
            if (s.getItem().getName().equals(itemName)) return s;
        }
        return null;
    }

    /**
     * @param itemName targy nev amit keresunk az inventoriban
     * @return a legalacsonyabb db-szammal rendelkezo slot amin taroljuk
     */
    public Slot getSlotWithLowestCount(String itemName) {
        Slot ret= getSlotByItemName(itemName);
        for (Slot s: slots){
            if (s.getItem().getName().equals(itemName) && s.getDb()<=ret.getDb()) ret=s;
        }
        return ret;
    }

    /**
     * @param id targy id amit keresunk az inventorban
     * @return a legalacsonyabb db-szammal rendelkezo slot amin taroljuk
     */
    public Slot getSlotWithLowestCount(int id) {
        Slot ret= getSlotByItemId(id);
        for (Slot s: slots){
            if (s.getItem().getId()==id && s.getDb()<=ret.getDb()) ret=s;
        }
        return ret;
    }

    /**
     * @param itemName a targy neve aminek szeretnek lekerdezni hogy hany db van az inventoriban
     * @return a targy szama az inventoriban
     */
    public int getItemCount(String itemName){
        int ret = 0;
        for (Slot s: slots){
            if (s.getItem().getName().equals(itemName)) {
                ret+=s.getDb();
            }
        }
        return ret;
    }

    /**megnezi hogy a targy amit hasznalni szeretnek van-e az inventoriban
     * ha igen eldonti hogy eszkoz-e vagy eheto es annak megfeleloen kezeli
     * @param M Palya
     * @param P jatekos
     * @param itemName Haszanlando item
     */
    public void useItem(Map M, Player P, String itemName) {
        Slot s = getSlotWithLowestCount(itemName);
        Tile originalTile = M.getOriginalTile(P.getPoz());
        if (s != null && s.getItem().getType()== Item.Type.tool) {
            originalTile.interakcio(M, P, s.getItem());
        } else if (s!=null && s.getItem().getType()==Item.Type.edible) {
            P.eat(s.getItem());
        } else {
            System.out.println("Nemletezik ilyen targy");
        }
    }

    /** atmozgatja az a kapott targyat a kapott inventoribol ebbe a kapott mennyiseg
     *
     * @param from ebbol az inventoribol mozgatja ebbe
     * @param itemName ezt az itemet mozgatja
     * @param count ennyit mozgat at
     */
    public void moveItem(Inventory from, String itemName, int count){
        Slot s = from.getSlotWithLowestCount(itemName);
        addItem(s.getItem(),count);
        while (count!=0) {
            from.removeItem(s.getItemId());
            count--;
        }
    }

    /**
     * @param i keresendo item
     * @return ha van az inventoriban true, ha nincs false
     */
    public boolean hasItem(Item i) {
        return getSlotByItemId(i.getId()) != null;
    }

    /**
     * @return az osszes item fajta ami az inventoriban van
     */
    public List<Item> getAllItems(){
        List<Item> i= new ArrayList<>();
        for(Slot s: slots){
            i.add(s.getItem());
        }
        return i;
    }

    @Override
    public String toString() {
        if (getNumberOfUsedSlot()==0)
            return "\nUres az inventory";
        StringBuilder ret= new StringBuilder(maxInventorySlots + "/" + slots.size() + " \n");
        for (Slot s: slots) {
            ret.append('[').append(s.toString()).append("]\n");
        }
        return ret.toString();
    }

    /**
     * @return toString de a veteli arakkal
     */
    public String toStringForSale(){
        if (getNumberOfUsedSlot()==0)
            return "\nUres az inventory";
        StringBuilder ret= new StringBuilder("\n");
        for (Slot s: slots) {
            ret.append('[').append(s.toStringForSale()).append("]\n");
        }
        return ret.toString();
    }

}
