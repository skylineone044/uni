package Expeditor.InventoryPck;

import Items.Item;

public class Slot {
    private int db;
    private boolean full =false;
    private Item item;

    public Slot(Item t, int db) {
        item =t;
        this.db=db;
        if (t.isBig()) full =true;
    }

    public boolean isFull() {
        return full;
    }

    public void setFull(boolean full) {
        this.full = full;
    }

    public void setDb(int db) {
        this.db = db;
    }

    public int getDb() {
        return db;
    }

    public Item getItem() {
        return item;
    }

    public int getItemId() {
        return item.getId();
    }

    @Override
    public String toString() {
        return db + " db " + item.toString();
    }
    public String toStringForSale() {
        return db + " db " + item.toStringForSale();
    }
}
