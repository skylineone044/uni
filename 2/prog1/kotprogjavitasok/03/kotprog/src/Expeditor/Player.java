package Expeditor;
import Expeditor.InventoryPck.*;
import Expeditor.Teams.*;
import Items.Edible.Kabitoszer;
import Items.Edible.Whiskey;
import Items.Item;
import Items.Tools.Faklya;
import Map.Tiles.Jatekosmezo;
import Map.Tiles.Tile;
import OwnUtilities.Vector;

import java.util.Random;
import java.util.Scanner;
import static java.lang.Math.pow;

public class Player {
	private Vector poz = new Vector();
	private final Tile playerTile = new Jatekosmezo(new Vector());
	private Inventory i = new Inventory();
	private Team team = new Team();

	private int reputation=0;
	private int viewDistance =1;
	private double moveCost =1;
	private int relationship=3;
	private double energy =100.0f;
	private int gold =250;
	private int priceModifyer;
	private boolean disaster =false;
	private boolean curse =false;
	private Item lastEaten=new Faklya();

	public Player() {
		setPriceModifyer();
	}

	public void setReputation(int reputation) {
		this.reputation = reputation;
	}

	public int getReputation() {
		return reputation;
	}

	public int getPriceModifyer() {
		return priceModifyer;
	}

	/**
	 * mindig a kereskedo csapattarsak szamara allitja
	 */
	public void setPriceModifyer() {
		this.priceModifyer = getTeam().countOfTeamMate(Teammate.Type.Kereskedo);
	}

	public double getMoveCost() {
		return moveCost;
	}

	public void setMoveCost(int movingCost) {
		moveCost =1;
		moveCost *=pow(1.15d,team.getTeamSize()) ;
		int itemPenalty=i.getNumberOfUsedSlot()-getInventory().getMaxInventorySlots();
		moveCost *=pow(1.20d, Math.max(itemPenalty, 0)) ;
		moveCost *=movingCost;
	}

	public boolean isCurse() {
		return curse;
	}

	public void setCurse(boolean curse) {
		this.curse = curse;
	}

	public boolean isDisaster() {
		return disaster;
	}

	public void setDisaster(boolean disaster) {
		this.disaster = disaster;
	}

	public double getEnergy() {
		return energy;
	}

	/**
	 * @param energy erre fogja beallitani
	 * meg ellenorzi hogy nemenjen 100 fole es 0 ala
	 */
	public void setEnergy(double energy) {
		this.energy = (energy >100) ? 100 : (energy<0) ? 0 : energy;
	}

	public int getGold() {
		return gold;
	}

	public void setGold(int gold) {
		this.gold = gold;
	}

	public Vector getPoz(){
		return poz;
	}

	public void setPos(int x, int y) {
		poz.set(x,y);
	}
	public void setPos(Vector v) {
		poz.set(v.x(),v.y());
	}

	public int getRelationship() {
		return relationship;
	}

	/**
	 * Beallitja a viszonyt
	 * 3 az alap es ehhez jon hozza a Bolcsek szama
	 */
	public void setRelationship() {
		this.relationship = 3+(getTeam().countOfTeamMate(Teammate.Type.Bolcs)*3);
	}

	public void minusRelationship() {
		relationship-=2;
	}

	public int getViewDistance() {
		return viewDistance;
	}

	public void setViewDistance(int viewDistance) {
		this.viewDistance = viewDistance;
	}

	public Tile getPlayerTile() {
		return playerTile;
	}

	public Inventory getInventory() {
		return i;
	}

	public Team getTeam() {
		return team;
	}

	/**
	 * az ossze jatekos inventorijaban levo Whiskey attributumat 20%-al noveli katonankent
	 */
	public void katonaEffect() {
		Slot slot=getInventory().getSlotByItemId(7);
		if (slot!= null){
			slot.getItem().getA().setValue(
					(int)(new Whiskey().getA().getValue()*
							(pow(1.2d,getTeam().countOfTeamMate(Teammate.Type.Katona)))));
		}
	}

	/**
	 * az ossze jatekos inventorijaban levo Kabitoszer attributumat 20%-al noveli samanonkent
	 */
	public void samanEffect(){
		Slot slot=getInventory().getSlotByItemId(6);
		if (slot!= null){
			slot.getItem().getA().setValue(
					(int)(new Kabitoszer().getA().getValue()*
							(pow(1.2d,getTeam().countOfTeamMate(Teammate.Type.Saman)))));
		}
	}

	/**
	 * +2-vel noveli a max inventoryt
	 */
	public void szamarEffect(){
		getInventory().setMaxInventorySlots(8+(2*getTeam().countOfTeamMate(Teammate.Type.Szamar)));
	}

	/**
	 * beallitja a viewDistance-t a felderito csapattarsak szama szerint
	 */
	public void felfedezoEffect(){
		setViewDistance(1+(getTeam().countOfTeamMate(Teammate.Type.Felderito)));
	}

	/**
	 * noveli az energiat az kapott etel szerint
	 * es ellenorzi hogy kell e alkoholiznust, vagy kabitoszerfuggoseget beallitania az utolso evett alapjan
	 * vegul beallitja ezt utolsonak evettre
	 * es hasznalja az Itemet
	 * @param t elfogyasztando etel
	 */
	public void eat(Item t){
		System.err.println(t.getA().getValue());
		setEnergy(getEnergy()+t.getA().getValue());
		if (lastEaten.getId()==t.getId()) {
			if (t.getId()==new Whiskey().getId()) {
				getTeam().getRandomTeammate().setAlcoholic(true);
			} else if (t.getId()==new Kabitoszer().getId()){
				getTeam().getRandomTeammate().setDrugAddict(true);
			}
		}
		if (t.getId()==new Whiskey().getId()) {
			for (Teammate tm:getTeam().getAlcoholics()){
				tm.resetAlcoholCountDown();
			}
		}
		if (t.getId()==new Kabitoszer().getId()) {
			for (Teammate tm:getTeam().getDrugAdditcs()){
				tm.resetDrugCountDown();
			}
		}
		lastEaten = t;
		t.use(this);
	}

	/**
	 * @param falu ha igaz akkor a faluban kaphato csapattarsak kozul sorsol egyet
	 *             ha nem igaz akkor a jatek elejen ajanhatoak kozul
	 * @return vissza adja a sorsolt csapattarsat
	 */
	public Teammate getRandomTeammateForSale(boolean falu) {
		Teammate.Type[] s;
		if (falu) {
			s = new Teammate.Type[] {Teammate.Type.Felderito, Teammate.Type.Bolcs, Teammate.Type.Saman};
		} else {
			s = new Teammate.Type[] {Teammate.Type.Kereskedo, Teammate.Type.Szamar, Teammate.Type.Katona};
		}
		Random R = new Random();
		int random = R.nextInt(s.length);
		Teammate tm = new Teammate(s[random]);
		return tm;
	}

	/**
	 * ajanlja a parameterben kapott csapattarsat ha kapott
	 * @param tm csapattars amit felberelhet
	 * @return ha felberelt valakit igazat ad vissza, ha nem vagy nem sikerult akkor hamisat
	 */
	public boolean teammateOffer(Teammate tm){
		if (getTeam().getTeamSize()==getTeam().getMaxTeamSize()) {
			System.out.println("A csapatod letszama mar elerte a maximumot, így nem vehetsz fel még egyet.");
			return false;
		}
		if (tm==null) {
			System.out.println("Nincs felberelheto csapattars!");
			return false;
		}
		if (relationship<=1) {
			System.out.println("Nincs elegendo viszonyod.\nMinimum viszony: 2");
			return false;
		}
		System.out.println("Ha felakarod venni ird be a nevet. Ha nem akarsz\nfelvenni senkit akkor csak us egy entert.\n Kozuluk valaszthatsz: ");
		System.out.println('['+tm.getName()+"], Perk:"+tm.getDesc());
		System.out.println("150 Arany/tars, ");
		System.out.println("Arany : "+getGold());
		Scanner sc = new Scanner(System.in);
		while (true){
			System.out.print("Felbérel: ");
			String in = sc.nextLine();
			if (in.equals(tm.getName())) {
				return getTeam().addTeamMate(tm, this);
			}
			if (in.equals("")) return false; else System.out.println("Probald ujra");
		}
	}
}
