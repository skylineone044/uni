package Expeditor;

import java.util.Random;

public class Rival {
    private String name;
    private int reputation;

    public Rival(String name){
        this.name=name;
        reputation=0;
    }

    public String getName() {
        return name;
    }

    public int getReputation() {
        return reputation;
    }

    public void setReputation(int reputation) {
        this.reputation = reputation;
    }

    /**
     * random ad a reputaciohoz
     * botoknal hasznalt reputacioszamitas
     */
    public void addReputation() {
        Random r = new Random();
        this.reputation+=100*r.nextInt(8);
        this.reputation+=1000*r.nextInt(2);
    }
}
