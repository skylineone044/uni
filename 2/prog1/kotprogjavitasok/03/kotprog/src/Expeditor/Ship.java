package Expeditor;

import Expeditor.InventoryPck.Inventory;
import Map.Map;
import Map.Tiles.Hajo;
import Map.Tiles.Tile;
import OwnUtilities.AmountMethods;
import OwnUtilities.Vector;

import java.util.Scanner;

public class Ship implements AmountMethods {
    private Inventory i = new Inventory();
    private final Tile playerTile = new Hajo(new Vector(),this);

    public Ship(){
    }

    public Tile getShipTile() {
        return playerTile;
    }

    public Inventory getInventory() {
        return i;
    }

    /**
     * a hajoval valo interakckiok vannak lekezelve
     * -kivesz: hajo inventoriabol kerul a plyaer inventoijaba
     * -betesz: a jatekos inventorijabol kerul a hajoeba
     * -pihen: 100-ra allitja a jatekos energiaszintjet
     * @param M Palya, nem hasznalja de van olyan mezo ami igen es absztrakt modon ide is kell
     * @param P Player aki interaktal a hajova
     * @return mindig igaz ezen a mezon
     */
    public boolean interaction(Map M,Player P){
        Scanner sc = new Scanner(System.in);
        while (true) {
            infoOut(P);
            switch (sc.nextLine()) {
                case "betesz" -> {
                    System.out.print("mit:");
                    String mit=sc.nextLine();
                    if (P.getInventory().getSlotWithLowestCount(mit)==null) {
                        System.out.println("Ilyen targy nincs az inventoridban");
                        break;
                    }
                    int mennyit=amountInput(mit, P.getInventory());
                    i.moveItem(P.getInventory(),mit,mennyit);
                }
                case "kivesz" ->{
                    System.out.print("mit:");
                    String mit=sc.nextLine();
                    if (i.getSlotWithLowestCount(mit)==null) {
                        System.out.println("Ilyen targy nincs a hajoban");
                        break;
                    }
                    int mennyit=amountInput(mit,i);
                    P.getInventory().moveItem(i,mit,mennyit);
                }
                case "pihen" -> {
                    P.setEnergy(100);
                    System.out.println("Pihentel. Az energiad ujra tele");
                }
                case "exit" -> {
                    return true;
                }
                default -> {
                    System.out.println("Bocsi ezt nem ertettem");
                }
            }
        }
    }

    /**
     * @param P Player amirol az infokat kiirja
     * kiirja az inventorikat meg a beirhato parancsokat
     */
    private void infoOut(Player P) {
        System.out.println("\nA te inventorid: "+P.getInventory().toString());
        System.out.println("A hajo inventorija: "+i.toString());
        System.out.print("[betesz][kivesz][pihen][exit]\nHajo: ");
    }
}

