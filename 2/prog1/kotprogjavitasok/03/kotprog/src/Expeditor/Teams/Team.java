package Expeditor.Teams;

import Expeditor.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Team {
    private int maxTeamSize = 3;
    private List<Teammate> team = new ArrayList<>();

    public Team(){

    }

    /** ellenorzi hogy van-e eleg aranyunk es ha van hozzaadja a csapathoz
     *
     * @param teammate akit hozza szeretnek adni a csapathoz
     * @param P a jatekos akinek a csapataba rakjukbe
     *
     * @return sikerult-e berakni
     */
    public boolean addTeamMate(Teammate teammate, Player P) {
        if (P.getGold()<150) {
            System.out.println("Nincs eleg aranyad kifizetni a tarsat!");
            return false;
        }
//        if (P.getRelationship()<=1) {
//            System.out.println("Nincs elegendo viszonyod. Minimum viszony: 2");
//            return false;
//        }
        P.setGold(P.getGold()-150);
        team.add(teammate);
        teammate.effekt(P);
        return true;
    }

    /**
     * @param tm csapattars aki elhagyja a csapatot
     */
    public void remTeamMate(Teammate tm) {
        for (Teammate t: team) {
            if(t.equals(tm)) {
                System.out.println("A "+t.getName()+" elhagyta a csapatod!");
                team.remove(t);
                break;
            }
        }
    }

    /**
     * @param tm csapattars aki lehethogy elhagyja a csapatot
     * @param BelowValue 1-0 kozotti ertek ami alatt elhagyja a csapatot pl.: 0.10 -> 10% eselye van hogy elhagyja a csapatot
     */
    public void remTeammateAtChance(Teammate tm,double BelowValue){
        Random r = new Random();
        if (r.nextDouble()<=BelowValue){
            System.out.println("A "+tm.getName()+" elhagyta a csapatod!");
            team.remove(tm);
        }

    }

    /**
     * @return A csapatbol egy random csapattars
     */
    public Teammate getRandomTeammate(){
        Random R = new Random();
        int r=R.nextInt(getTeamSize());
        return team.get(r);
    }

    /**
     * @return leserul egy random csapattars
     */
    public Teammate injureTeammate(){
        Random R = new Random();
        int r=R.nextInt(getTeamSize());
        team.get(r).setInjured(true);
        return team.get(r);
    }

    /**
     * @return az osszes jelenleg serult csapattarsat adja vissza
     */
    public List<Teammate> getInjuredTeammates() {
        List<Teammate> ret = new ArrayList<>();
        for(Teammate tm:team) {
            if (tm.isInjured()) ret.add(tm);
        }
        return ret;
    }
    public List<Teammate> getTeammates(){
        return team;
    }

    /**
     * @param t tipusu csapattas
     * @return a t tipusu csapattarsak szama
     */
    public int countOfTeamMate(Expeditor.Teams.Teammate.Type t) {
        int ret = 0;
        for (Teammate tm: team) {
            if(tm.getType()==t) ret++;
        }
        return ret;
    }

    /**
     * @return hany csapattars van a csapatba
     */
    public int getTeamSize(){
        return team.size();
    }
    public int getMaxTeamSize() {
        return maxTeamSize;
    }

    /**
     * @return az osszes csapattars aki alkoholista
     */
    public List<Teammate> getAlcoholics(){
        List<Teammate> ret = new ArrayList<>();
        for (Teammate tm:team) {
            if (tm.isAlcoholic()) {
                ret.add(tm);
            }
        }
        return ret;
    }

    /**
     * @return az osszes csapattars aki drogfuggo
     */
    public List<Teammate> getDrugAdditcs(){
        List<Teammate> ret = new ArrayList<>();
        for (Teammate tm:team) {
            if (tm.isDrugAddict()) {
                ret.add(tm);
            }
        }
        return ret;
    }

    /**
     * @param t csapattars tipus amit keresunk
     * @return van-e a csapatban t
     */
    public boolean isTeamMate(Teammate.Type t){
        if(getTeamSize()==0) return false;
        for (Teammate tm:this.team) {
            if (tm.getType()==t) return true;
        }
        return false;
    }

    @Override
    public String toString() {
        String ret = "";
        for (Teammate t:this.team){
            ret+="["+t.getName()+ (t.isInjured() ? "(serult)" : "")+((t.isAlcoholic()) ? "(alkoholista)" : ((t.isDrugAddict() ? "(drogfuggo)" : "")))+"]";
        }
        return ret;
    }
}

