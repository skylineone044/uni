package Expeditor.Teams;

import Expeditor.Player;

public class Teammate {
    public enum Type {
        Bolcs,
        Felderito,
        Katona,
        Kereskedo,
        Saman,
        Szamar
    }

    private Type type;
    private String name;
    private String desc;
    private boolean injured = false;
    private boolean alcoholic = false;
    private int alcoholCountDown = 30;
    private boolean drugAddict = false;
    private int drugCountDown = 30;

    /**
     * @param t tipusa a csapattarsnak
     *          ezalapjan adja a nevet is
     *          es a leirasat is
     */
    public Teammate(Type t){
        this.type=t;
        this.name=t.name();
        setDesc();
    }

    public int getAlcoholCountDown() {
        return alcoholCountDown;
    }

    public int getDrugCountDown() {
        return drugCountDown;
    }

    public void resetAlcoholCountDown() {
        this.alcoholCountDown = 30;
    }

    public void minusDrugCountDown() {
        this.drugCountDown -= 1;
    }

    public void minusAlcoholCountDown() {
        this.alcoholCountDown -= 1;
    }

    public void resetDrugCountDown() {
        this.drugCountDown = 30;
    }

    public boolean isAlcoholic() {
        return alcoholic;
    }

    public boolean isDrugAddict() {
        return drugAddict;
    }

    public void setAlcoholic(boolean alcoholic) {
        System.out.println(name + " alkoholista lett.");
        this.alcoholic = alcoholic;
    }

    public void setDrugAddict(boolean drugAddict) {
        System.out.println(name + " drogfuggo lett.");
        this.drugAddict = drugAddict;
    }

    public String getDesc() {
        return desc;
    }

    /**
     * beallitaj a tipusa alapjan a leirasat
     */
    private void setDesc() {
        switch (type){
            case Bolcs -> desc="+3 alap viszony uj terkepen";
            case Saman -> desc="A Kabitoszer +20% energia/Saman";
            case Katona -> desc="A Whiskey +20% energia/Katona";
            case Szamar -> desc="+2 inventory slot";
            case Felderito -> desc="+1 latokor";
            case Kereskedo -> desc="olcsobban vehetsz picivel es dragabban adhatsz el";
        }
    }

    public boolean isInjured() {
        return injured;
    }

    public void setInjured(boolean injured) {
        this.injured = injured;
    }

    public Type getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    /**
     * @param P jatokos akire az effektet elkell inditani
     */
     public void effekt(Player P) {
        switch (type){
            case Saman -> {
                P.samanEffect();
            }
            case Katona -> {
                P.katonaEffect();
            }
            case Szamar -> {
                P.szamarEffect();
            }
            case Felderito -> {
                P.felfedezoEffect();
            }
            case Kereskedo -> {
                P.setPriceModifyer();
            }
        }
     }
}
