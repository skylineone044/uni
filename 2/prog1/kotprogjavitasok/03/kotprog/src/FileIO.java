import Map.Map;
import Map.Tiles.*;
import OwnUtilities.Vector;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class FileIO {
    private Scanner olvaso;
    private int m;
    private File mapDirPath = new File("src/Maps");


    /**
     * Palya beolvasa filebol
     */
    public FileIO () {
    }

    public int getM() {
        return m;
    }

    /**
     * A palya file elso soranak a palya meretenek kell lenni, es onnan beolvassa azt a szamot
     */
    public void setM() {
        String str=olvaso.nextLine();
        this.m = parseInt(str);
    }

    /**
     * @return Beolvassa a src/Maps mappaban talalhato osszes file-t es egy Map listaban visszaadja
     */
    public List<Map> getMaps(){
        List<Map> ret = new ArrayList<Map>();
        for (int i=0;i<mapDirPath.list().length;i++) {
            String mapFiles[] = mapDirPath.list();
            File fajl = new File(mapDirPath.getPath()+"/"+mapFiles[i]);
            try {
                olvaso = new Scanner(fajl);
                setM();
                ret.add(new Map(readIn(olvaso),m));
            } catch (FileNotFoundException e){
                System.err.println(mapFiles[i]+" nem talalhato");
            }
        }
        return ret;
    }

    /**
     * beolvassa a palyat es vegig megy karakterenkent rajta
     * a carakter alapjan letrehozza a mezo objektumokat
     * @return a letrehozott mezoobjektumokbol allo 2 dimenzios tomb, maga a palya
     */
    public Tile[][] readIn(Scanner olvaso) {
        Tile[][] ret = new Tile[m][m];
        int i=0;
        while (olvaso.hasNextLine()) {
            String str=olvaso.nextLine();
            for (int k = 0;k<str.length();k++) {
                switch (str.charAt(k)) {
                    case 'T' -> {
                        ret[i][k] = new Tenger(new Vector(i,k));
                    }
                    case 't' -> {
                        ret[i][k] = new To(new Vector(i,k));
                    }
                    case 'H' -> {
                        ret[i][k] = new Hegy(new Vector(i, k));
                    }
                    case 'B' -> {
                        ret[i][k] = new Barlang(new Vector(i, k));
                    }
                    case 'V' -> {
                        ret[i][k] = new Vulkan(new Vector(i, k));
                    }
                    case 'G' -> {
                        ret[i][k] = new Gejzir(new Vector(i, k));
                    }
                    case 'D' -> {
                        ret[i][k] = new Dzsungel(new Vector(i, k));
                    }
                    case 'N' -> {
                        ret[i][k] = new Nedves(new Vector(i, k));
                    }
                    case 'F' -> {
                        ret[i][k] = new Fuves(new Vector(i, k));
                    }
                    case 'A' -> {
                        ret[i][k] = new Aranypiramis(new Vector(i, k));
                    }
                    case 'O' -> {
                        ret[i][k] = new Oltar(new Vector(i, k));
                    }
                    case '@' -> {
                        ret[i][k] = new Falu(new Vector(i, k));
                    }
                    case 'E' -> {
                        ret[i][k] = new Hajo(new Vector(i, k));
                    }
                    default -> {
                        ret[i][k] = new Ures(new Vector(i, k));
                    }
                }
            }
            i++;
        }
        return ret;
    }
}
