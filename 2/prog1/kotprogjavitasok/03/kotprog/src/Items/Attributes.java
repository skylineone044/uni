package Items;

public class Attributes {
    public enum Type {
        Health,
        other
    }
    private final Type type;
    private int value;

    /**
     * ures attribute ha nincs szukseg ra,de hiba elkerulse vegett kell
     */
    public Attributes(){
        this.type=Type.other;
        value=0;
    }

    /**
     * Eheto targyaknak van megadva altalaban ezzel a taperteke
     * @param type milyen tipusu
     * @param value az erteke
     */
    public Attributes(Type type,int value){
        this.type=type;
        this.value =value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
