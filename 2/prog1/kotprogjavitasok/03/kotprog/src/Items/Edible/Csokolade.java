package Items.Edible;

import Items.Attributes;
import Items.Item;

public class Csokolade extends Item {
    public Csokolade(){
        super(8,"Csokolade", Type.edible,false,20,new Attributes(Attributes.Type.Health,20));
    }
}
