package Items.Edible;

import Items.Attributes;
import Items.Item;

public class Hus extends Item {
    public Hus() {
        super(5, "Hus",Type.edible,false,25, new Attributes(Attributes.Type.Health,25));
    }
}
