package Items.Edible;

import Items.Attributes;
import Items.Item;

public class Kabitoszer extends Item {
    public Kabitoszer(){
        super(6,"Kabitoszer",Type.edible,false,20,new Attributes(Attributes.Type.Health,20));
    }
}
