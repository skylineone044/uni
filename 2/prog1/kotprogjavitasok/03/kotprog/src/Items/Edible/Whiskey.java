package Items.Edible;

import Items.Attributes;
import Items.Item;

public class Whiskey extends Item {
    public Whiskey() {
        super(7,"Whiskey",Type.edible,false,20,
                new Attributes(Attributes.Type.Health,20));

    }
}
