package Items;

import Expeditor.Player;

public class Item {

    /**
     * egy item ilyen tipusu lehet
     */
    public enum Type {
        tool,
        edible,
        other
    }

    private final int id;
    private final Type type;
    private final String name;
    private final boolean big;// ha nagymeretu akkor csak egy fer el egy slot-on
    private final int originalprice;
    private int price;
    private int buyPrice;
    private final Attributes A;

    /**
     * @param id az item id-je
     * @param name az item neve
     * @param type az item tipusa
     * @param big nagy meretue, ha nagymeretu csak egyfer el inventory helyen
     * @param price az ar amibe kerul
     *
     * a buyprice amin kinaljak a jatekosnak
     * az attributomban ures, mert nincs neki
     */
    public Item(int id, String name, Type type, boolean big, int price) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.big = big;
        this.price = price;
        this.originalprice = price;
        this.buyPrice=(int)(price*1.2d);
        this.A = new Attributes();
    }
    /**
     * @param id az item id-je
     * @param name az item neve
     * @param type az item tipusa
     * @param big nagy meretue, ha nagymeretu csak egyfer el inventory helyen
     * @param price az ar amibe kerul
     * @param attributes tulajdonsagai
     *
     * a buyprice amin kinaljak a jatekosnak
     *
     */
    public Item(int id, String name, Type type, boolean big, int price, Attributes attributes) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.big = big;
        this.price = price;
        this.originalprice = price;
        this.buyPrice=(int)(price*1.2d);
        this.A = attributes;
    }

    public int getBuyPrice() {
        return buyPrice;
    }

    /**
     * @param kereskedocount kereskedo csapattarsak szama
     *                       eszerint valtozik az eladasi es veteli ar
     */
    public void kereskedoEffect(int kereskedocount){
        this.buyPrice-=kereskedocount;
        this.price+=kereskedocount;
    }

    /**
     * lenullaza az effektet alapertelmezettre
     */
    public void resetKereskedoEffect() {
        this.price = originalprice;
        this.buyPrice=(int)(originalprice*1.2d);
    }

    public Attributes getA() {
        return A;
    }

    public Type getType() {
        return type;
    }

    public int getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public boolean isBig() {
        return big;
    }

    /**
     * @param P ha az item-et hasznaltak akkor eltavolitja magat a P jatekos inventorijabol
     */
    public void use(Player P) {
            P.getInventory().removeItem(getId());
    }

    @Override
    public String toString() {
        return name +" ("+price+" arany)";
    }

    /**
     * sima toString csak az veteli arral
     * @return veteli aras string
     */
    public String toStringForSale() {
        return name +" ("+buyPrice+" arany)";
    }
}
