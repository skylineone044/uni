package Map;
import Map.Tiles.Jatekosmezo;
import Map.Tiles.Tile;
import OwnUtilities.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Map {
    private final int size; //A map.Terkep merete meretXmeret
    private Tile[][] tiles;  //A terkep tartalma hodosítva lesz a jatekmenet alatt
    private Tile[][] originalTiles;  //A terkep tartalma
    private boolean[][] hidden; //amit meg nem fedezett fel a jatekos az rejtett=>true, ha mar felfedezte=>false
    private boolean done = false;

    /**
     * @param bemenet 2d mezo tomb amit a file-bol beolvasasbol
     * @param size a palya merete
     */
    public Map(Tile[][] bemenet, int size) {
        this.size =size;
        tiles =bemenet;
        originalTiles = new Tile[size][size];
        for (int i=0;i<size;i++){
            System.arraycopy(tiles[i], 0, originalTiles[i], 0, size);
        }
        hidden = new boolean[size][size];
        fillHidden(true);

    }
    public Map(Map M){
        this.size=M.getSize();
        tiles=M.getTiles();
        originalTiles = new Tile[size][size];
        for (int i=0;i<size;i++){
            System.arraycopy(tiles[i], 0, originalTiles[i], 0, size);
        }
        hidden = new boolean[size][size];
        fillHidden(true);
    }

    public Tile[][] getTiles() {
        return tiles;
    }

    /**
     * @param b erre allitja
     * vegig megy egy palyaval azonos meretu tombon es beallitja mindet a kapott ertekre
     */
    public void fillHidden(boolean b){
        for (int i = 0; i< size; i++){
            for (int k = 0; k< size; k++) {
                hidden[i][k]= b;
            }
        }
    }

    public boolean isDone() {
        return done;
    }

    public void setDone() {
        this.done = true;
    }

    /**
     * megkeresi a hajot a palyan, ha nem talalja visszaadja 0,0-val
     * @return a hajo koordinataja
     */
    public Vector getShipPoz() {
        for (int i = 0; i< size; i++){
            for (int k = 0; k< size; k++) {
                if (originalTiles[i][k].getId()=='E') {
                    Vector ret = new Vector(i,k);
                    return ret;
                }
            }
        }
        Vector ret = new Vector(0,0);
        return ret;
    }
    @Override
    public String toString() {
        String ret="";
        for (int i = 0; i< size; i++) {
            for (int k = 0; k< size; k++) {
                ret=ret+"|"+(hidden[i][k] ? '#' : tiles[i][k]);
            }
            ret=ret+"\n";
        }
        return ret;
    }

    /**
     * a hajokorul keres egy valid mezot ahova kiszalhat a jatekos
     *
     * @return a valid mezo koordinataja
     */
    public Vector createPlayer() {
        Vector poz= getShipPoz();
        if (!isCharInCharArray(tiles[poz.x()+1][poz.y()].getId(),new char[]{'T','t','H'})) {
            tiles[poz.x()+1][poz.y()]= new Jatekosmezo(poz.get(1,0));
            poz.x(poz.x()+1);
            return poz;
        } else if (!isCharInCharArray(tiles[poz.x()-1][poz.y()].getId(),new char[]{'T','t','H'})) {
            tiles[poz.x()-1][poz.y()]= new Jatekosmezo(poz.get(-1,0));
            poz.x(poz.x()-1);
            return poz;
        } else if (!isCharInCharArray(tiles[poz.x()][poz.y()+1].getId(),new char[]{'T','t','H'})) {
            setTile(new Vector(poz.x(),poz.y()+1),new Jatekosmezo(poz.get(0,1)));
            poz.y(poz.y()+1);
            return poz;
        } else if (!isCharInCharArray(tiles[poz.x()][poz.y()-1].getId(),new char[]{'T','t','H'})) {
            tiles[poz.x()][poz.y()-1]= new Jatekosmezo(poz.get(0,-1));
            poz.y(poz.y()-1);
            return poz;
        } else {
            System.err.println("Nincs hajo a terkepen vagy nincs jarhato szarazfoldkozelebe");
        }
        return poz;
    }

    public int getSize() {
        return size;
    }

    public void setTile(Vector poz, Tile tile) {
        this.tiles[poz.x()][poz.y()] = tile;
    }
    public Tile getTile(Vector poz) {
        try {
            return tiles[poz.x()][poz.y()];
        } catch (ArrayIndexOutOfBoundsException e){
            return null;
        }
    }

    public Tile getOriginalTile(Vector poz) {
        try {
            return originalTiles[poz.x()][poz.y()];
        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }
    public void setOriginalTile(Vector poz, Tile originalTile) {
        this.originalTiles[poz.x()][poz.y()] = originalTile;
    }

    /**
     * @param poz visszaallitja a mezot az eredeti mezore
     */
    public void resetTile(Vector poz) {
        setTile(poz, getOriginalTile(poz));
    }

    /**
     * @param poz pozicio ami korul szeretnen felfdezni a palyat, alltalaban a jatekos pozicioja
     * @param viewDistance a felfedezes sugara
     *
     * a poz korul viewDistance tavolsagban felfedezettre allitja a palyat
     */
    public void exploreTiles(Vector poz, int viewDistance) {
        for (int h = viewDistance *- 1; h <= viewDistance; h++) {
            for (int v = viewDistance *- 1; v <= viewDistance; v++) {
                if (getTile(poz.get(v,h))!=null) hidden[poz.x() + v][poz.y() + h]=false;
            }
        }
    }

    /**
     * @param poz a vulkan pozicioja
     * a vulkankitoresnel szetterulo lavat tunteti el
     */
    public void resetLava(Vector poz){
        for (int h = -3; h<= 3 ; h++) {
            for (int v = -3; v<= 3;v++) {
                if (getTile(poz.get(v,h))!=null && getTile(new Vector(poz.x()+v, poz.y()+h)).getId()=='L') resetTile(new Vector(poz.x()+v,poz.y()+h));
            }
        }
    }

    /**
     * @param poz kereses kozeppontja
     * @param keresendoMezoId milyen mezot keresunk, annak az idjei
     * @return a talalt mezo pozicioja
     *
     * megkeresi a legkozelebbi mezot amit kapott, egyre nagyobb sugarban
     * ha tobbet is talalt random vissza adja az egyiket
     */
    public Tile nearestTile(Vector poz, char[] keresendoMezoId) {
        List<Tile> ret = new ArrayList<>();
        int tavolsag=0;
        while (ret.size()==0 && tavolsag< getSize()-1){
            tavolsag++;
            for (int h = tavolsag * -1; h <= tavolsag; h++) {
                for (int v = tavolsag * -1; v <= tavolsag; v++) {
                    if (getOriginalTile(poz.get(v,h))!=null && isCharInCharArray(getOriginalTile(poz.get(v,h)).getId(),keresendoMezoId)
                        && ((poz.x()+v)!=poz.x() || (poz.y()+h)!=poz.y()))
                        ret.add(getOriginalTile(poz.get(v,h)));
                }
            }
        }
        int R = new Random().nextInt(ret.size());
        return ret.get(R);
    }

    /**
     * @param c  char amit keresunk
     * @param ca charArray amiben keresunk
     * @return talat-e vagy sem
     */
    public boolean isCharInCharArray(char c, char[] ca){
        for (char cc : ca) {
            if (cc == c) return true;
        }
        return false;
    }
}
