package Map.Tiles;

import Expeditor.Player;
import Items.Item;
import Map.Map;
import OwnUtilities.Vector;

public class Aranypiramis extends Tile {
    public Aranypiramis(Vector poz) {
        super(poz, 'A');
        super.setMoveCost(1);
    }

    @Override
    public boolean interakcio(Map M, Player P, Item t) {
        P.getInventory().newItem(new Items.Others.Aranypiramis(),1);
        M.setOriginalTile(getPoz(),new Ures(getPoz()));
        return true;
    }

    @Override
    public String toString() {
        return "\u001B[1;93m" + super.getId() + "\u001B[0m";
    }

}
