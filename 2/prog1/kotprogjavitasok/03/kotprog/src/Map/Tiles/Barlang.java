package Map.Tiles;

import Items.Others.Treasure;
import Items.Item;
import Expeditor.Player;
import Map.Map;
import OwnUtilities.Vector;

import java.util.Random;

public class Barlang extends Tile {
    private boolean bejart=false;

    public Barlang(Vector poz) {
        super(poz, 'B');
        super.setMoveCost(1);
        super.setUsableItems(new int[]{1});
    }


    /** Barlangaszas lekezelese
     *  Ha fakylat hasznalunk csak kap a jatekos egy kincset es levonodik a faklya
     *  ha nemhasznal semmit akkor 65% esely katasztrofara
     *
     * @param M
     * @param P
     * @param t
     * @return
     */
    @Override
    public boolean interakcio(Map M, Player P, Item t) {
        if (bejart) {
            System.out.println("Itt mar voltal");
            return false;
        }
        if (t==null) {
            P.getInventory().addItem(new Treasure(), 1);
            Random R = new Random();
            if (R.nextDouble()<0.65f) P.setDisaster(true);
            bejart = true;
            return true;
        }  else if (t.getId()== getUsableItems()[0]) {
            P.getInventory().addItem(new Treasure(), 1);
            bejart = true;
            t.use(P);
            return true;
        } else {
            System.out.println("Nem megfelelo targy");
            return false;
        }
    }

    @Override
    public String toString() {
        return "\u001B[0;97m" + super.getId() + "\u001B[0m";
    }
}
