package Map.Tiles;

import Items.Item;
import Expeditor.Player;
import Map.Map;
import OwnUtilities.Vector;

public class Dzsungel extends Tile {
    public Dzsungel(Vector poz) {
        super(poz, 'D');
        super.setMoveCost(2);
        super.setUsableItems(new int[]{3});
    }

    @Override
    public String toString() {
        return "\u001B[1;32m" + super.getId() + "\u001B[0m";
    }

    /**ha bozotvagot hasznalunk akkor lecserelodik uresre
     * automatikusan meghivodik ha van nalunk bozotvago
     *
     * @param M
     * @param P
     * @param t
     * @return
     */
    @Override
    public boolean interakcio(Map M, Player P, Item t) {
        if (t==null) return false;
        if (t.getId()== getUsableItems()[0]) {
            M.setOriginalTile(getPoz(),new Ures(getPoz()));
            t.use(P);
            return true;
        } else {
            System.out.println("Ez a targy nem hasznalhato itt");
            return false;
        }
    }
}
