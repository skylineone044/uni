package Map.Tiles;

import Expeditor.Teams.Teammate;
import Items.Edible.Gyumolcs;
import Items.Edible.Hus;
import Items.Edible.Kabitoszer;
import Items.Item;
import Expeditor.InventoryPck.Inventory;
import Expeditor.Player;
import Items.Tools.Faklya;
import Items.Tools.Kotel;
import Map.Map;
import OwnUtilities.AmountMethods;
import OwnUtilities.Vector;

import java.util.Random;
import java.util.Scanner;

public class Falu extends Tile implements AmountMethods {
    private Inventory i = new Inventory();
    private Scanner sc;
    private Teammate tm;

    /**letrehozza a falu mezot es feltolti az "arukeszletet" (inventorijat) random
     * @param poz falu pozicioja
     */
    public Falu(Vector poz) {
        super(poz, '@');
        super.setMoveCost(1);
        Random r = new Random();
        i.newItem(new Gyumolcs(),r.nextInt(3));
        i.newItem(new Hus(),r.nextInt(2));
        i.newItem(new Faklya(),r.nextInt(2));
        i.newItem(new Kabitoszer(),r.nextInt(2));
        i.newItem(new Kotel(),r.nextInt(1));
    }

    @Override
    public String toString() {
        return "\u001B[1;93m" + super.getId() + "\u001B[0m";
    }

    /**a faluban levo interakcio kezelese
     *
     * @param M palya
     * @param P jatekos
     * @param t targy
     * @return mindig igaz
     */
    @Override
    public boolean interakcio(Map M, Player P, Item t) {
        sc = new Scanner(System.in);
        tm= P.getRandomTeammateForSale(true);
        for (Item i:i.getAllItems()){
            i.resetKereskedoEffect();
            i.kereskedoEffect(P.getPriceModifyer());
        }
        while (true) {
            System.out.println("\nArany: "+P.getGold());
            System.out.println("A te inventorid: "+P.getInventory().toString());
            System.out.println("A Falu inventorija: "+i.toStringForSale());
            System.out.print("[vesz][elad][felberel][exit]\nFalu: ");
            switch (sc.nextLine()) {
                case "vesz" -> {
                    System.out.print("mit:");
                    String mit=sc.nextLine();
                    if (i.getSlotWithLowestCount(mit)==null) {
                        System.out.println("Ilyen targy nincs a faluban");
                        break;
                    }
                    int mennyit=amountInput(mit,i);
                    P.setGold(P.getGold()-(i.getSlotWithLowestCount(mit).getItem().getBuyPrice()*mennyit));
                    P.getInventory().moveItem(i,mit,mennyit);
                }
                case "elad" -> {
                    System.out.print("mit:");
                    String mit=sc.nextLine();
                    if (mit.equals("Kincs")) {
                        System.out.println("Nem érdekli őket az ajánlat");
                        break;
                    }
                    if (P.getInventory().getSlotWithLowestCount(mit)==null) {
                        System.out.println("Ilyen targy nincs az inventoridban");
                        break;
                    }
                    int mennyit=amountInput(mit,P.getInventory());
                    P.setGold(P.getGold()+(P.getInventory().getSlotWithLowestCount(mit).getItem().getPrice()*mennyit));
                    i.moveItem(P.getInventory(),mit,mennyit);
                }
                case "felberel" -> {
                    if (P.teammateOffer(tm)) {
                        tm=null;
                        P.minusRelationship();
                    }
                }
                case "exit" -> {
                    return true;
                }
                default -> System.out.println("Bocsi ezt nem ertettem");
            }
        }
    }
}

