package Map.Tiles;

import OwnUtilities.Vector;

public class Fuves extends Tile {
    public Fuves(Vector poz) {
        super(poz, 'F');
        super.setMoveCost(1);
    }

    @Override
    public String toString() {
        return "\u001B[1;92m" + super.getId() + "\u001B[0m";
    }


}
