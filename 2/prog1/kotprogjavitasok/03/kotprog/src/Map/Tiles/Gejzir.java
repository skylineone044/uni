package Map.Tiles;

import Expeditor.Player;
import Items.Item;
import Map.Map;
import OwnUtilities.Vector;

public class Gejzir extends Tile {
    private int lepesigTerjed=5;
    private int terjedseg=1;

    public Gejzir(Vector poz) {
        super(poz, 'G');
        super.setMoveCost(0);
    }

    public int getLepesigTerjed() {
        return lepesigTerjed;
    }

    /**a gejzir poziciojabol kifele terjed koronkent amig a lepesig terjed nem null
     *
     * @param M palya
     * @param P jatekos
     * @param t targy
     * @return igaz
     */
    @Override
    public boolean interakcio(Map M, Player P, Item t) {
        for (int h = terjedseg* -1; h <= terjedseg; h++) {
            for (int v = terjedseg* -1; v <= terjedseg; v++) {
                if((v!=0 || h!=0)&& M.getOriginalTile(getPoz().get(v,h))!=null  && M.getOriginalTile(getPoz().get(v,h)) instanceof Ures) {
                    M.setOriginalTile(getPoz().get(v,h),new Nedves(getPoz().get(v,h)));
                    if (M.getTile(getPoz().get(v,h)).getId()!='&')M.resetTile(getPoz().get(v,h));
                }
            }
        }
        terjedseg++;
        lepesigTerjed--;
        return true;
    }
    @Override
    public String toString() {
        return "\u001B[0;31m" + super.getId() + "\u001B[0m";
    }


}
