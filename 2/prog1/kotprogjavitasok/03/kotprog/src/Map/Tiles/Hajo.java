package Map.Tiles;
import Expeditor.Ship;
import Items.Item;
import Expeditor.InventoryPck.Inventory;
import Expeditor.Player;
import Map.Map;
import OwnUtilities.AmountMethods;
import OwnUtilities.Vector;

public class Hajo extends Tile implements AmountMethods {
    private Inventory i = new Inventory();
    private Ship ship;

    public Hajo(Vector poz) {
        super(poz, 'E');
        super.setMoveCost(1);
    }

    public Hajo(Vector poz,Ship s) {
        super(poz, 'E');
        super.setMoveCost(1);
        this.ship = s;
    }

    @Override
    public String toString() {
        return "\u001B[47m" + super.getId() + "\u001B[0m";
    }

    @Override
    public boolean interakcio(Map M, Player P, Item t) {
        return ship.interaction(M,P);
    }
}
