package Map.Tiles;

import Items.Item;
import Expeditor.Player;
import Map.Map;
import OwnUtilities.Vector;

public class Hegy extends Tile {
    public Hegy(Vector poz) {
        super(poz, 'H');
        super.setMoveCost(0);
    }

    @Override
    public boolean interakcio(Map M, Player P, Item t) {
        M.setOriginalTile(getPoz(),new Vulkan(getPoz()));
        M.resetTile(getPoz());
        return true;
    }

    @Override
    public String toString() {
        return "\u001B[0;37m" + super.getId() + "\u001B[0m";
    }

}
