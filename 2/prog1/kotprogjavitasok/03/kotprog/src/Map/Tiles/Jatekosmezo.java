package Map.Tiles;

import OwnUtilities.Vector;

public class Jatekosmezo extends Tile {
    public Jatekosmezo(Vector poz) {
        super(poz, '&');
        super.setMoveCost(0);
    }

    @Override
    public String toString() {
        return "\u001B[1;97m" + super.getId() + "\u001B[0m";
    }

}
