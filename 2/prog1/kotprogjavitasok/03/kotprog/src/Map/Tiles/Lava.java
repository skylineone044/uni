package Map.Tiles;

import Items.Item;
import Expeditor.Player;
import Map.Map;
import OwnUtilities.Vector;

public class Lava extends Tile {
    public Lava(Vector poz) {
        super(poz, 'L');
        super.setMoveCost(0);
    }

    /**Ha dzyungel(D) vagy Fuves(F) teruletre folyik akkor leegeti es ures lesz
     * ha olyan helyre tejred ahova nem tudna akkor reseteli magat az eredeti mezore
     *
     * @param M
     * @param P
     * @param t
     * @return
     */
    @Override
    public boolean interakcio(Map M, Player P, Item t) {
        char c= M.getOriginalTile(getPoz()).getId();
        if (M.isCharInCharArray(c,new char[]{'T','t','H','V','B','E'})) M.resetTile(getPoz());
        if (M.isCharInCharArray(c,new char[]{'D','F'})) M.setOriginalTile(getPoz(),new Ures(getPoz()));
        return false;
    }

    @Override
    public String toString() {
        return "\u001B[0;31m" + super.getId() + "\u001B[0m";
    }

}
