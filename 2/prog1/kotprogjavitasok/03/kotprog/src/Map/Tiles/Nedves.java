package Map.Tiles;

import OwnUtilities.Vector;

public class Nedves extends Tile {
    public Nedves(Vector poz) {
        super(poz, 'N');
        super.setMoveCost(1);
    }

    @Override
    public String toString() {
        return "\u001B[1;32m" + super.getId() + "\u001B[0m";
    }

}
