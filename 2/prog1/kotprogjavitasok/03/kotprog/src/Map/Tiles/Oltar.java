package Map.Tiles;

import Items.Others.Treasure;
import Items.Item;
import Expeditor.Player;
import Map.Map;
import OwnUtilities.Vector;

import java.util.Random;

public class Oltar extends Tile {
    private boolean bejart=false;

    public Oltar(Vector poz) {
        super(poz, 'O');
        super.setMoveCost(1);
    }

    @Override
    public String toString() {
        return "\u001B[1;93m" + super.getId() + "\u001B[0m";
    }

    /**80% atokra es ad egy kincset
     *
     * @param M
     * @param P
     * @param t
     * @return
     */
    @Override
    public boolean interakcio(Map M, Player P, Item t) {
        if (bejart) {
            System.out.println("Itt mar voltal");
            return false;
        }
        if (t==null) {
            P.getInventory().addItem(new Treasure(), 1);
            P.minusRelationship();
            System.out.println("-2 viszony");
            Random R = new Random();
            if (R.nextDouble()<0.80f) P.setCurse(true);
            bejart = true;
            return true;
        } else {
            System.out.println("Nem megfelelo targy");
            return false;
        }
    }
}
