package Map.Tiles;

import OwnUtilities.Vector;

public class Tenger extends Tile {
    public Tenger(Vector poz) {
        super(poz, 'T');
        super.setMoveCost(0);
    }

    @Override
    public String toString() {
        return "\u001B[0;34m" + super.getId() + "\u001B[0m";
    }


}
