package Map.Tiles;

import Items.Item;
import Expeditor.Player;
import Map.Map;
import OwnUtilities.Vector;

public class Tile {
    private final Vector poz;
    private char id;
    private int moveCost;
    private int[] usableItems = {};

    /**
     * a palyan talalhato mezo obj
     * @param poz pozicioja
     * @param id char azonositoja
     * a jarhatosag alapertelmezeten 1, kulonboz teruleteken masra van allitva pl.: Dungel
     */
    public Tile(Vector poz, char id) {
        this.poz=poz;
        this.id=id;
        moveCost =1;
    }

    public Vector getPoz() {
        return poz;
    }

    public int getMoveCost() {
        return moveCost;
    }

    public void setMoveCost(int moveCost) {
        this.moveCost = moveCost;
    }

    public void setId(char id) {
        this.id = id;
    }

    public char getId() {
        return id;
    }

    /**
     * @param usableItems hazsnalhato itemek idjei amiket lehet hasznalni ezen a mezon interakcio soran
     */
    public void setUsableItems(int[] usableItems) {
        this.usableItems = usableItems;
    }

    public int[] getUsableItems() {
        return usableItems;
    }

    public boolean interakcio(Map M, Player P, Item t) {
        return false;
    }
}

