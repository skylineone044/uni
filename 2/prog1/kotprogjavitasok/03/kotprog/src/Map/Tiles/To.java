package Map.Tiles;

import OwnUtilities.Vector;

public class To extends Tile {
    public To(Vector poz) {
        super(poz, 't');
        super.setMoveCost(0);
    }


    @Override
    public String toString() {
        return "\u001B[0;34m" + super.getId() + "\u001B[0m";
    }

}
