package Map.Tiles;

import Items.Item;
import Expeditor.Player;
import Map.Map;
import OwnUtilities.Vector;

public class Ures extends Tile {
    public Ures(Vector poz) {
        super(poz, 'U');
        super.setMoveCost(1);
    }

    @Override
    public boolean interakcio(Map M, Player P, Item t) {
        if (P !=null) return false;
        M.setOriginalTile(getPoz(),new Gejzir(getPoz()));
        M.resetTile(getPoz());
        return true;
    }

    @Override
    public String toString() {
        return "\u001B[0;92m" + super.getId() + "\u001B[0m";
    }

}
