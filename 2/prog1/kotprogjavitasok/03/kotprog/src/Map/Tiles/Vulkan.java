package Map.Tiles;

import Items.Item;
import Expeditor.Player;
import Map.Map;
import OwnUtilities.Vector;

public class Vulkan extends Tile {
    private int lepesigTerjed=4;
    private int terjedseg=1;
    public Vulkan(Vector poz) {
        super(poz, 'V');
        super.setMoveCost(0);
    }

    /** a vulkan poziciojabol kifele terjed koronkent amig a lepesigTerjed nem null
     *
     * @param M
     * @param P
     * @param t
     * @return
     */
    @Override
    public boolean interakcio(Map M, Player P, Item t) {
        if (getLepesigTerjed()==0) {
            M.resetLava(getPoz());
            return false;
        }
        for (int h = terjedseg * -1; h<=terjedseg; h++) {
            for (int v = terjedseg * -1; v<=terjedseg; v++) {
                if((v != 0 || h != 0) && M.getOriginalTile(getPoz().get(v,h))!=null) {
                    if (M.getTile(getPoz().get(v,h)).getId()!='&') {
                        M.setTile(getPoz().get(v,h),new Lava(getPoz().get(v,h)));
                        M.getTile(getPoz().get(v,h)).interakcio(M,null,null);
                    }
                }
            }
        }
        terjedseg+=(terjedseg<3) ? 1 : 0;
        lepesigTerjed--;
        return true;
    }

    public int getLepesigTerjed() {
        return lepesigTerjed;
    }


    @Override
    public String toString() {
        return "\u001B[0;31m" + super.getId() + "\u001B[0m";
    }

}
