import Expeditor.InventoryPck.Inventory;
import Expeditor.*;
import Expeditor.Teams.Teammate;
import Items.Attributes;
import Items.Edible.Csokolade;
import Items.Edible.Hus;
import Items.Edible.Whiskey;
import Items.Item;
import Items.Others.Aranypiramis;
import Items.Others.Treasure;
import Items.Others.Uveggolyo;
import Items.Tools.Bozotvago;
import Items.Tools.Faklya;
import Items.Tools.Kotel;
import Map.Map;
import Map.Tiles.*;
import OwnUtilities.AmountMethods;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Mission implements AmountMethods {

	private Scanner sc = new Scanner(System.in);
	private final Map map;
	private final Player player;
	private final Ship ship;

	private Random R = new Random();
	private double random;
	private boolean gameOver = false;
	private final List<Vulkan> kitortVulkan= new ArrayList<>();
	private final List<Gejzir> feltortGejzir= new ArrayList<>();

	/**
	 * felepiti a kuldetest, es beallitja alaphelyzetbe.
	 * @param M ebben kapja meg a palyat amin menni fog a kuldetes
	 * @param P a jatek aki maszkalni fog a palyan
	 * @param S a jatekos hajoja
	 */
	public Mission(Map M, Player P, Ship S){
		this.map=M;
		this.player=P;
		this.ship=S;
		player.setPos(map.createPlayer());
		map.setTile(map.getShipPoz(),S.getShipTile());
		map.setOriginalTile(map.getShipPoz(), S.getShipTile());

		map.exploreTiles(player.getPoz(), player.getViewDistance());
		player.setEnergy(100);
		player.setRelationship();
	}

	/**
	 * egy kuldetes fo ciklusa, ajanl egy csapattarsat, kikotovasarlast majd folyamatos bemenetet var
	 * ami szerint iranyitja a jatek menetet
	 */

	public void start() {
		player.teammateOffer(player.getRandomTeammateForSale(false));
		harborMarket();
		turn();
		while (!gameOver) {
			if (map.isDone()){
				treasureDeal();
				return;
			}
			infoOut();
			switch (sc.nextLine()){
				case "jobb" -> step(0, 1);
				case "bal" -> step(0, -1);
				case "le" -> step(1, 0);
				case "fel" -> step(-1, 0);
				case "var" -> turn();
				case "exit" -> {
					System.out.println("Vissza a fomenube!");
					return;
				}
				case "hasznal" -> {
					System.out.print("Hasznal: ");
					String mit=sc.nextLine();
					player.getInventory().useItem(map, player, mit);
				}
				case "interakt" -> {
					if(map.getOriginalTile(player.getPoz()).interakcio(map, player,null)) turn();
				}
				case "test" -> {
					player.getInventory().addItem(new Treasure(),2);
				}
				case "haza" -> {
					if (player.getPoz().equals(map.getShipPoz())) {
						map.setDone();
						turn();
					} else {
						System.out.println("Csak a hajon allva tudsz hazamenni");
					}
				}
				default -> System.out.println("Bocsi de ezt nem értettem!");
			}

		}
	}

	/**
	 * Infok kiiratasa es terkep kiirasa. Minden kor elejen megvan hivva.
	 */
	private void infoOut(){
		System.out.println("Terkep:");
		System.out.println(map.toString());
		System.out.println("Arany: "+ player.getGold()+", Energia: "+((player.getEnergy()<10)?"\u001B[0;31m":"")+String.format("%.2f", player.getEnergy())+"\u001B[0m, Viszony: "+player.getRelationship());
		System.out.println("Csapat: "+player.getTeam().toString());
		System.out.println("Inventory: "+player.getInventory().toString());
		System.out.println("[jobb][bal][fel][le][var][haza][hasznal][interakt]");
		System.out.print("Terep: ");
	}

	/**
	 * a karakter/felfedezo lepese a palyan es annak ellenorzese, hogy valid-e a lepes. Eleg bonyolultan oldottam meg, de mukodik!
	 *
	 * @param ud up-down rovidites ha minus akkor up, ha pozitiv akkor down
	 * @param rl right-left rovidites ha minus akkor right, ha pozitiv akkor left
	 */

	public void step(int ud, int rl) { //ud <-> up-down, rl <-> right-left
		if (player.getPoz().x()+ud< map.getSize() && player.getPoz().x()+ud>=0 && player.getPoz().y()+rl< map.getSize() && player.getPoz().y()+rl>=0 && map.getTile(player.getPoz().get(ud,rl)).getMoveCost()!=0) {
			map.resetTile(player.getPoz());
			player.setPos(player.getPoz().x()+ud, player.getPoz().y()+rl);
			if (map.getTile(player.getPoz()).getId()=='D' && player.getInventory().hasItem(new Bozotvago())) player.getInventory().useItem(map, player,"Bozotvago");
			player.setMoveCost(map.getOriginalTile(player.getPoz()).getMoveCost());
			player.setEnergy(player.getEnergy()- player.getMoveCost());
			map.exploreTiles(player.getPoz(), player.getViewDistance());
			map.setTile(player.getPoz(), player.getPlayerTile());
			turn();
		} else System.out.println("Arra nem tudsz menni");
	}

	/**
	 * A katasztrofa esemeny. Eldonti hogy melyik fajta katsztrofa tortenik es vegrehajtja azt.
	 */
	private void disaster() {
		if (random<=0.20d && player.getTeam().getTeamSize()>0) { //0.20d
			player.getTeam().injureTeammate();
			System.out.println("Katasztofa tortent! A "+player.getTeam().injureTeammate().getName()+" csapattarsad leserult.");
		} else if (random<=0.30d && player.getTeam().getTeamSize()>0) { //0.30d
			System.out.println("Katasztofa tortent! Egy csapattarsad hatrahagyta a csapatot.");
			player.getTeam().remTeamMate(player.getTeam().getRandomTeammate());
			if (player.getTeam().getTeamSize()<0) endGame();
		} else {
			System.out.println("Katasztofa tortent! Vesztettel 45 energiat.");
			player.setEnergy(player.getEnergy()-45);
		}
		player.setDisaster(false);
	}

	/**
	 * Az atok esemeny. Eldonti melyik fajta atok tortenik es elinditja azt.
	 */
	private void curse() {
		if (random<=0.35d) { //0.35d
			//vulkankitores
			System.out.println("Elatkozott lettel! A kozeledben kitort egy vulkan es ontja magabol a lavat.");
			Tile m= map.nearestTile(player.getPoz(), new char[]{'H'});
			m.interakcio(map,null,null);
			kitortVulkan.add(new Vulkan(m.getPoz()));
		} else {
			//gejzir feltores
			System.out.println("Elatkozott lettel! A kozeledben feltort egy gejzir es nedvesse tette a korulotte levo talajt.");
			Tile m= map.nearestTile(player.getPoz(),new char[]{'U'});
			m.interakcio(map,null,null);
			feltortGejzir.add(new Gejzir(m.getPoz()));
		}
		player.setCurse(false);
	}

	/**
	 * ez fut le ha meghal a jatekos
	 */
	private void endGame() {
		System.out.println("Vesztettel");
		gameOver=true;
	}

	/**
	 * egy korben vegrehajtando ellenorzesek:
	 * -jatekos vegzett e akuldetessel
	 * -a jatekos energiaja elfogyott-e
	 * -vesztett-e a jatekos
	 * -elvan-e atkozva
	 * -tortent-e katasztrofa
	 * -a serult csapattarsakra elhagyjak-e a csapatot a serulesuk miatt
	 * -alkoholistak kaptak-e alkoholt es ha nem akkor kovetkezmenye
	 * -drog fuggok kaptak-e drogot es, ha nem akkor annak a kovetkezmenye
	 * -kitort vulkanok terjedese
	 * -kirort gejzirek terjedese
	 * -kulonbozo csapattars effektek futtatasa
	 * -a terkep felfedezese az uj pozicioban
	 */
	private void turn(){
		if (map.isDone()) {
			System.out.println("Vegeztel a kuldetessel! haza hajozol");
			return;

		}
		if (player.getEnergy()==0) {
			for(Teammate tm:player.getTeam().getTeammates()) {
				player.getTeam().remTeammateAtChance(tm,0.08d);
				break;
			}
			if (player.getTeam().getTeamSize()==0 && random<=0.08d) gameOver=true;
		}
		if (gameOver) {
			System.out.println("A jateknak vege, vesztettel! A felfedezod is elhagyta a csapatot.");
			return;
		}
		if (player.isCurse()) curse();
		if (player.isDisaster()) disaster();

		for (Teammate tm:player.getTeam().getInjuredTeammates()) {
			player.getTeam().remTeammateAtChance(tm,0.05d);
			break;

		}
		for(Teammate tm:player.getTeam().getAlcoholics()){
			if (tm.getAlcoholCountDown()<=0) {
				player.getTeam().remTeammateAtChance(tm, 0.10d);
				break;
			}
			tm.minusAlcoholCountDown();
		}
		for(Teammate tm:player.getTeam().getDrugAdditcs()){
			if (tm.getDrugCountDown()<=0) {
				player.getTeam().remTeammateAtChance(tm, 0.10d);
				break;
			}
			tm.minusDrugCountDown();
		}

		for (Vulkan v:kitortVulkan) {
			v.interakcio(map,null,null);
			if (v.getLepesigTerjed()==0) {
				map.resetLava(v.getPoz());
				kitortVulkan.remove(v);
				break;
			}
		}
		for (Gejzir g:feltortGejzir) {
			g.interakcio(map,null,null);
			if (g.getLepesigTerjed()==0){
				map.setOriginalTile(g.getPoz(),new Nedves(g.getPoz()));
				map.resetTile(g.getPoz());
				feltortGejzir.remove(g);
				break;
			}
		}
		random = R.nextDouble();
		player.katonaEffect();
		player.samanEffect();
		player.szamarEffect();
		player.felfedezoEffect();
		map.exploreTiles(player.getPoz(), player.getViewDistance());
		for (Item i:player.getInventory().getAllItems()){
			i.resetKereskedoEffect();
			i.kereskedoEffect(player.getPriceModifyer());
		}
		System.out.println("\n");
	}

	/**
	 * a kuldees elejen levo, kikoto vasarlas
	 */
	private void harborMarket() {
		Inventory harbor = new Inventory();
		harbor.newItem(new Kotel(), 2);
		harbor.newItem(new Bozotvago(), 2);
		harbor.newItem(new Faklya(), 2);
		harbor.newItem(new Hus(), 2);
		harbor.newItem(new Whiskey(), 2);
		harbor.newItem(new Csokolade(), 2);
		harbor.newItem(new Uveggolyo(), 2);
		for (Item i : harbor.getAllItems()) {
			i.resetKereskedoEffect();
			i.kereskedoEffect(player.getPriceModifyer());
		}
		while (true) {
			System.out.println("\nArany: " + player.getGold());
			System.out.println("A te inventorid: " + player.getInventory().toString());
			System.out.println("A kikoto inventorija: " + harbor.toStringForSale());
			System.out.print("[vesz][elad][exit]\nKikoto: ");
			switch (sc.nextLine()) {
				case "vesz" -> {
					System.out.print("mit:");
					String wantedItem = sc.nextLine();
					if (harbor.getSlotWithLowestCount(wantedItem) == null) {
						System.out.println("Ilyen targy nincs a kikotoben");
						break;
					}
					int amount = amountInput(wantedItem, harbor);
					player.setGold(player.getGold() - (harbor.getSlotWithLowestCount(wantedItem).getItem().getBuyPrice() * amount));
					player.getInventory().moveItem(harbor, wantedItem, amount);
				}
				case "elad" -> {
					System.out.print("mit:");
					String wantedItem = sc.nextLine();
					if (wantedItem.equals("Kincs")) {
						System.out.println("Nem érdekli őket az ajánlat");
						break;
					}
					if (player.getInventory().getSlotWithLowestCount(wantedItem) == null) {
						System.out.println("Ilyen targy nincs az inventoridban");
						break;
					}
					int amount = amountInput(wantedItem, player.getInventory());
					player.setGold(player.getGold() + (player.getInventory().getSlotWithLowestCount(wantedItem).getItem().getPrice() * amount));
					harbor.moveItem(player.getInventory(), wantedItem, amount);
				}
				case "exit" -> {
					return;
				}
				default -> System.out.println("Bocsi ezt nem ertettem");
			}
		}
	}

	/**
	 * kuldetes vegen a kincsek eladomanyozasa vagy eladasat kezeli es eszerint szamolja a hirnevet
	 */
	private void treasureDeal(){
		final String kincs = "Kincs";
		Inventory treasures = new Inventory();
		while (player.getInventory().hasItem(new Treasure())) {
			treasures.moveItem(player.getInventory(),kincs,1);
		}
		while (ship.getInventory().hasItem(new Treasure())) {
			treasures.moveItem(ship.getInventory(),kincs,1);
		}
		if (treasures.getNumberOfUsedSlot()==0) {
			System.out.println("Nincs kincsed amit eladhatnal vagy eladomanyozhatnal");
			return;
		}
		System.out.println(treasures.toString());
		System.out.println(treasures.getItemCount(kincs)+" kincsed van.");
		System.out.print("A kincseket eladhatod aranyert vagy eladomanyozhatok oket hirnevert.\nMennyit szeretnel eladni beloluk? A maradekot automatikusan eladomanyozod hirnevert. ");
		int amount = amountInput(kincs,treasures);
		player.setGold(player.getGold()+(amount*treasures.getSlotByItemName(kincs).getItem().getPrice()));
		player.setReputation(100*(treasures.getItemCount(kincs)-amount)+player.getReputation());
		if (player.getInventory().hasItem(new Aranypiramis())) {
			player.getInventory().removeItem(11);
			player.setReputation(player.getReputation()+1000);
			System.out.println("Aranypiramis: +1000 hirnev");
		}
		if (ship.getInventory().hasItem(new Aranypiramis())) {
			player.getInventory().removeItem(11);
			player.setReputation(player.getReputation()+1000);
			System.out.println("Aranypiramis: +1000 hirnev");
		}

	}

}
