package OwnUtilities;

import Expeditor.InventoryPck.Inventory;

import java.util.Scanner;

public interface AmountMethods {

    /**
     * @param mit a targy neve amit keresunk
     * @param in az inventory aiben keressuk
     * @return egy helyes mennyiseg
     * folyamatosan beker egy szamot amit ellenoriz hogy szam-e
     * ha szam megnezi valid-e majd hogy a szam helyes-e
     */
    default int amountInput(String mit, Inventory in) {
        if (in.getItemCount(mit)==0) return 0;
        Scanner sc=new Scanner(System.in);
        int mennyit=-1;
        do try {
            System.out.print("mennyit:");
            mennyit=sc.nextInt();
        } catch (Exception e) {
            System.out.println("Szamot kerek!");
            sc.nextLine();
        } while (amountValidation(mennyit,in.getItemCount(mit)));
        return mennyit;
    }

    /**
     * @param mennyit ennyit szeretnek
     * @param max az osszes elerheto
     * @return van-e annyi amit bekertunk
     */
    private boolean amountValidation(int mennyit, int max){
        if (mennyit<0 || mennyit>max) {
            System.out.println("Ennyit nem lehet");
            return true;
        }
        return false;
    }

}
