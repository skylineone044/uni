package OwnUtilities;

import java.util.Objects;

public class Vector {
    private int x;
    private int y;

    /**
     * ures Vektor obj
     */
    public Vector(){}

    /**
     * @param x a vektor x koordinataja
     * @param y a vektor y koordinataja
     */
    public Vector(int x, int y){
        this.x=x;
        this.y=y;
    }
    //  setters
    public void x(int x) {
        this.x = x;
    }

    public void y(int y) {
        this.y = y;
    }

    public void set(int x,int y){
        this.x=x;
        this.y=y;
    }
    //  getters
    public int x() {
        return x;
    }
    public int y() {
        return y;
    }

    /**
     * @param x x
     * @param y y
     * @return vissza adja a relativ koordinatat magahoz kepes a kapott szamokkal eltolva
     */
    public Vector get(int x, int y){
        return new Vector(this.x+x,this.y+y);
    }

    @Override
    public String toString() {
        return "Vector{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    /**
     * @param v
     * @return megnezi hogy eggyezik-e a koordinata a kapott vektorral
     */
    public boolean equals(Vector v) {
        if (v==null) return false;
        return v.x() == x && v.y() == y;
    }

}
