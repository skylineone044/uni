import java.lang.Math;

public class Eger {
    static int kurzormozgatas(Pozicio a, Pozicio b) {
        if (a == b) {
        return 0;
        }
        Pozicio vectorBA = new Pozicio();
        vectorBA.x = a.x - b.x;
        vectorBA.y = a.y - b.y;

        Pozicio Xaxis = new Pozicio();
        Xaxis.x = 1;
        Xaxis.y = 0;
        double smallestAngle;

        double dotProduct = (vectorBA.x * Xaxis.x + vectorBA.y * Xaxis.y);
        if (dotProduct == 0) { // if dot product is 0, then they are perpendicular
            smallestAngle = 90;
        } else {
            double magnitudeV = Math.sqrt(Math.pow((vectorBA.x), 2) + Math.pow((vectorBA.y), 2));
            double magnitudeX = Math.sqrt(Math.pow((Xaxis.x), 2) + Math.pow((Xaxis.y), 2));
            smallestAngle = Math.acos(dotProduct / (magnitudeV * magnitudeX));
        }
        if (vectorBA.x < 0) {
            smallestAngle = 360-smallestAngle;
        }

        return (int)smallestAngle;
    }
}
