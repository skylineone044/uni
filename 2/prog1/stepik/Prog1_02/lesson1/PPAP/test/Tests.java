import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Tests {
    @Test
    public void teszt() {
        assertEquals("Hibas eredmeny", 3,  Gyar.PPAP(6, 3, 3));
        assertEquals("Hibas eredmeny", 3, Gyar.PPAP(11, 3, 3));
        assertEquals("Hibas eredmeny", 4, Gyar.PPAP(10, 5, 4));
        assertEquals("Hibas eredmeny", 2, Gyar.PPAP(10, 2, 7));
        assertEquals("Hibas eredmeny", 60, Gyar.PPAP(120, 71, 69));
    }
}