import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class Tests {

    private String doTest(String[] args) {
        PrintStream old = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(baos));
        Argumentumok.main(args);
        System.setOut(old);
        return baos.toString().trim();
    }

    @Test
    public void teszt1() {
        assertEquals("hibas eredmeny", "", doTest(new String[] {}));
        assertEquals("hibas eredmeny", "", doTest(new String[] {"egy"}));
        assertEquals("hibas eredmeny", "ketto", doTest(new String[] {"egy", "ketto"}));
        assertEquals("hibas eredmeny", "macska", doTest(new String[] {"neptun", "macska"}));
        assertEquals("hibas eredmeny", "kaktusz", doTest(new String[] {"neptun", "kaktusz", "nyuszi"}));
        assertEquals("hibas eredmeny", "macska" + System.lineSeparator() + "lovacska", doTest(new String[] {"neptun", "macska", "nyuszi", "lovacska"}));
        assertEquals("hibas eredmeny", "neptun" + System.lineSeparator() + "prog1", doTest(new String[] {"macska", "neptun", "ora", "prog1", "ember"}));
        assertEquals("hibas eredmeny", "neptun" + System.lineSeparator() + "prog1" + System.lineSeparator() + "utolso", doTest(new String[] {"macska", "neptun", "ora", "prog1", "ember", "utolso"}));
    }
}