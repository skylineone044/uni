import java.lang.Math;

public class Eger {
    static int kurzormozgatas(Pozicio a, Pozicio b) {
        System.out.println("");
        if (a.x == b.x && a.y == b.y) {
            return 0;
        }
        boolean negative = false;
        double distance = Math.sqrt(Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2));
        double sin = Math.sin((b.y - a.y) / distance);
        double cos = Math.cos((b.x - a.x) / distance);
        double rad_sin = Math.asin((b.y - a.y) / distance);
        double rad_cos = Math.acos((b.x - a.x) / distance);
        negative = (rad_sin < 0);
        if (negative) {
            System.out.println("negativ");
            System.out.println(rad_sin);
            rad_sin = (2*Math.PI) + rad_sin;
            System.out.println(rad_sin);
        }
        double angle = Math.toDegrees(rad_sin);
        int intangle = (int) Math.round(angle);

        // trnsform them to the origin
        b.x -= a.x;
        b.y -= a.y;
        a.x = 0;
        a.y = 0;

        System.out.println(b.x);
        System.out.println(b.y);
        System.out.println(sin);
        System.out.println(cos);
        System.out.println(rad_sin);
        System.out.println(rad_cos);
        System.out.println(angle);
        System.out.println(intangle);

        if (b.y > 0) { // above sin  elso 2 negyed
            if (b.x > 0) { // 1. negyed
                System.out.println("1.negyed");
                return intangle;
            } else if (b.x < 0) { // 2. negyed
                System.out.println("2.negyed");
                return 180-intangle;
            } else { // fuggolegesen fel
                return 90;
            }
        } else if (b.y < 0) { // below sin
            if (b.x < 0) { // 3. negyed
                System.out.println("3.negyed");
                return 180-(-360+intangle);
            } else if (b.x > 0) { // 4. negyed
                System.out.println("4.negyed");
                return intangle;
            } else { // fuggolegesen le
                return 270;
            }
        } else { // sin is 0
            if (cos > 0) { // poxitiv iranyban vizszintes
                return (int)Math.round(Math.toDegrees(rad_cos));
            } else { // negativ iranyban vizszintes
                return (int)Math.round(Math.toDegrees(rad_sin));
            }

        }
    }
}