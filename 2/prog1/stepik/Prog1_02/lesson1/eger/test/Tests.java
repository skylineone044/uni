import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Tests {

    public Pozicio createPozision(int a, int b) {
        Pozicio p = new Pozicio();
        p.x = a;
        p.y = b;
        return p;
    }

    @Test
    public void teszt() {
        assertEquals("Hibas eredmeny", 0,  Eger.kurzormozgatas(createPozision(10, 10), createPozision(10, 10)));
        assertEquals("Hibas eredmeny", 90, Eger.kurzormozgatas(createPozision(100, 300), createPozision(100, 600)));
        assertEquals("Hibas eredmeny", 270, Eger.kurzormozgatas(createPozision(100, 600), createPozision(100, 353)));
        assertEquals("Hibas eredmeny", 0, Eger.kurzormozgatas(createPozision(100, 300), createPozision(500, 300)));
        assertEquals("Hibas eredmeny", 180, Eger.kurzormozgatas(createPozision(500, 300), createPozision(100, 300)));

        assertEquals("Hibas eredmeny", 45, Eger.kurzormozgatas(createPozision(100, 100), createPozision(500, 500)));
        assertEquals("Hibas eredmeny", 53, Eger.kurzormozgatas(createPozision(100, 100), createPozision(400, 500)));
        assertEquals("Hibas eredmeny", 355, Eger.kurzormozgatas(createPozision(313, 523), createPozision(745, 484)));
        assertEquals("Hibas eredmeny", 174, Eger.kurzormozgatas(createPozision(1030, 523) ,createPozision(444, 587)));
        assertEquals("Hibas eredmeny", 186, Eger.kurzormozgatas(createPozision(500, 500), createPozision(100, 460)));
    }
}