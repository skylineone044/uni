public class Erdemjegy {

    static String ertekeles(int erdemjegy) {
        switch (erdemjegy) {
            case 1: return "elégtelen";
            case 2: return "elégséges";
            case 3: return "közepes";
            case 4: return "jó";
            case 5: return "jeles";
            default: return "Jutka néni, figyeljen oda milyen jegyet ad!";
        }
    }

}