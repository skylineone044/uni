import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Tests {
  @Test
  public void teszt() {
    assertEquals("hibas eredmeny", "Jutka néni, figyeljen oda milyen jegyet ad!", Erdemjegy.ertekeles(6));
    assertEquals("hibas eredmeny", "elégtelen", Erdemjegy.ertekeles(1));
    assertEquals("hibas eredmeny", "közepes", Erdemjegy.ertekeles(3));
    assertEquals("hibas eredmeny", "Jutka néni, figyeljen oda milyen jegyet ad!", Erdemjegy.ertekeles(381770));
    assertEquals("hibas eredmeny", "jó", Erdemjegy.ertekeles(4));
    assertEquals("hibas eredmeny", "elégséges", Erdemjegy.ertekeles(2));
    assertEquals("hibas eredmeny", "jeles", Erdemjegy.ertekeles(5));
    assertEquals("hibas eredmeny", "Jutka néni, figyeljen oda milyen jegyet ad!", Erdemjegy.ertekeles(0));
    assertEquals("hibas eredmeny", "Jutka néni, figyeljen oda milyen jegyet ad!", Erdemjegy.ertekeles(-23411));
  }
}