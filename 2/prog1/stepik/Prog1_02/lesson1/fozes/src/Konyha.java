public class Konyha {
    /**
     * this does the thing
     * @author ben
     * @version v1
     */
    static int fozes(Szek normal, Szek epikus, int szuksegesFutoero) {

        normal.futoeroLabankent *= 4;
        epikus.futoeroLabankent *= 4;

        int futoereo = 0;
        int kiadottPenz = 0;
        int legkevesebb_penz = Integer.MAX_VALUE;

        for (int i = 0; i < 1000; i++) {
            futoereo = i * normal.futoeroLabankent;
            kiadottPenz = i * normal.ar;

            while (futoereo < szuksegesFutoero) {
                futoereo += epikus.futoeroLabankent;
                kiadottPenz += epikus.ar;
                if (futoereo == szuksegesFutoero) {
                    break;
                }
            }
            if (kiadottPenz < legkevesebb_penz) {
                legkevesebb_penz = kiadottPenz;
            }
        }
        return legkevesebb_penz;

    }
}