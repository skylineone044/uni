import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Tests {
    @Test
    public void harom() {
        Fuggvenyek f = new Fuggvenyek();
        assertEquals("Hibas eredmeny", f.harom(), 3);
    }

    @Test
    public void echo() {
        Fuggvenyek f = new Fuggvenyek();
        assertEquals("Hibas eredmeny", 6, f.echo(6));
        assertEquals("Hibas eredmeny", 641, f.echo(641));
        assertEquals("Hibas eredmeny", -66, f.echo(-66));
    }

    @Test
    public void semmi() {
        Fuggvenyek f = new Fuggvenyek();
        f.semmi();
    }

}