import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class Tests {
  @Test
  public void teszt1() {
    PrintStream old = System.out;
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    System.setOut(new PrintStream(baos));
    Futas.main(new String[] {});
    System.setOut(old);
    assertEquals("Hibas kiiratas", "JAVA", baos.toString());
  }
}