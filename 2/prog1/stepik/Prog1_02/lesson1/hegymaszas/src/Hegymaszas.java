public class Hegymaszas {

    static int maszas(int[] magassagok) {
        int maszasTav = 0;
        maszasTav += (Math.max(magassagok[0], 0));
        for (int i = 0; i < magassagok.length-1; i++) {
            if (magassagok[i] < magassagok[i+1]) {
                maszasTav += magassagok[i+1] - magassagok[i];
            }
        }
        return maszasTav;
    }

}