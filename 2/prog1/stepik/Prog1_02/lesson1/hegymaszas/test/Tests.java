import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class Tests {
  @Test
  public void teszt() {
    try {
      Class klass = Hegymaszas.class;
      Method m = klass.getDeclaredMethod("maszas", int[].class);
      if (m.getReturnType() != int.class) {
        fail("Rossz visszateresi tipus");
      }
    } catch (Exception e) {
      fail("Hibas fuggveny fejlec");
    }

    assertEquals("hibas eredmeny", 15, Hegymaszas.maszas(new int[] {4, 7, 10, 15}));
    assertEquals("hibas eredmeny", 100, Hegymaszas.maszas(new int[] {100, 65, 53, 31, 10}));
    assertEquals("hibas eredmeny", 15, Hegymaszas.maszas(new int[] {5, 10, 15, 10}));
    assertEquals("hibas eredmeny", 23, Hegymaszas.maszas(new int[] {3, 5, 3, 6, 2, 5, 4, 8, 2, 10}));
    assertEquals("hibas eredmeny", 34, Hegymaszas.maszas(new int[] {10, 12, 7, 7, 7, 20, 11, 20}));
    assertEquals("hibas eredmeny", 170, Hegymaszas.maszas(new int[] {-100, 70}));
    assertEquals("hibas eredmeny", 1700, Hegymaszas.maszas(new int[] {100, -100, 100, -100, 100, -100, 100, -1000, -500, -500, -500, -500, -500, 0}));
  }
}