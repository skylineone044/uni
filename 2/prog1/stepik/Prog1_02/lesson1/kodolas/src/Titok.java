public class Titok {
    static String kodolas(String eredeti) {
        String parityBit;
        int ones = 0;
        for (int i = 0; i < eredeti.length(); i++) {
            if (eredeti.charAt(i) == '1') {
                ones++;
            }
        }
        if (ones % 2 == 0) {
            parityBit = "0";
        } else {
            parityBit = "1";
        }
        String message_with_parity = eredeti + parityBit;
        return message_with_parity + message_with_parity;
    }

    static String dekodolas(String kodolt) {
        int eredeti_len = (kodolt.length() / 2) - 1;
        int ones = 0;
        for (int i = 0; i < eredeti_len+1; i++) {
            ones++;
        }
        if (ones % 2 == 0) {
            return kodolt.substring(0, eredeti_len);
        } else {
            return kodolt.substring(eredeti_len+1, kodolt.length()-1);
        }
    }

}