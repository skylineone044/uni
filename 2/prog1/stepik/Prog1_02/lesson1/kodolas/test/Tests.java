import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertEquals;

public class Tests {
    @Test
    public void nincsHiba() {
        String eredeti = "1100";

        String kodolt = Titok.kodolas(eredeti);
        String dekodolt = Titok.dekodolas(kodolt);
        assertEquals("Hibas eredmeny", eredeti, dekodolt);

        eredeti = "111100010101";

        kodolt = Titok.kodolas(eredeti);
        dekodolt = Titok.dekodolas(kodolt);
        assertEquals("Hibas eredmeny", eredeti, dekodolt);
    }

    @Test
    public void vanHiba() {
        Random random = new Random();

        String eredeti = "1100";
        String kodolt = Titok.kodolas(eredeti);

        int pozicio = random.nextInt(kodolt.length());
        System.out.println(pozicio);
        kodolt = kodolt.substring(0, pozicio) + (kodolt.charAt(pozicio) == '1' ? '0' : '1') + kodolt.substring(pozicio + 1);

        String dekodolt = Titok.dekodolas(kodolt);
        assertEquals("Hibas eredmeny", eredeti, dekodolt);


        eredeti = "111100010101";
        kodolt = Titok.kodolas(eredeti);

        pozicio = random.nextInt(kodolt.length());
        System.out.println(pozicio);
        kodolt = kodolt.substring(0, pozicio) + (kodolt.charAt(pozicio) == '1' ? '0' : '1') + kodolt.substring(pozicio + 1);

        dekodolt = Titok.dekodolas(kodolt);
        assertEquals("Hibas eredmeny", eredeti, dekodolt);
    }
}