public class Ko {
    static int kovek(int[] osszegyujtottKovek, int utazasSzam, int jelenlegiKovek) {
        int sum = 0;
        for (int j : osszegyujtottKovek) {
            sum += j;
        }
        return sum - jelenlegiKovek;
    }
}