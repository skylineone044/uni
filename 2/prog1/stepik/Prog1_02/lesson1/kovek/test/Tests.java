import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Tests {
    @Test
    public void teszt() {
        int[] kovek1 = {4, 7, 2, 1, 6, 3};
        int[] kovek2 = {1};
        int[] kovek3 = {3, 3, 3, 3, 3, 3, 3, 3, 3, 3};
        int[] kovek4 = {101, 4, 2};

        assertEquals("Hibas eredmeny", 0, Ko.kovek(kovek1, 6, 23));
        assertEquals("Hibas eredmeny", 0, Ko.kovek(kovek2, 1, 1));
        assertEquals("Hibas eredmeny", 1, Ko.kovek(kovek3, 10, 29));
        assertEquals("Hibas eredmeny", 6, Ko.kovek(kovek4, 3, 101));
        assertEquals("Hibas eredmeny", 107, Ko.kovek(kovek4, 3, 0));
    }
}