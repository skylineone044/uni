public class Munkahely {
    static int lepcsozes(int[] viragok, int viragszam, int lepcsoszam) {
        int steps = 0;
        for (int emelet : viragok) {
            steps += emelet * lepcsoszam * 2;
        }
        return steps;
    }
}