import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Tests {
    @Test
    public void teszt() {
        int[] tomb1 = {1, 3, 5, 7, 10};
        int[] tomb2 = {10};
        int[] tomb3 = {7, 9, 11};
        int[] tomb4 = {1, 5, 7};
        int[] tomb5 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] tomb6 = {1, 4, 8, 9, 12, 42};
        int[] tomb7 = {20, 40, 55, 72};

        assertEquals("Hibas eredmeny", 52, Munkahely.lepcsozes(tomb1, 5, 1));
        assertEquals("Hibas eredmeny", 100, Munkahely.lepcsozes(tomb2, 1, 5));
        assertEquals("Hibas eredmeny", 432, Munkahely.lepcsozes(tomb3, 3, 8));
        assertEquals("Hibas eredmeny", 260, Munkahely.lepcsozes(tomb4, 3, 10));
        assertEquals("Hibas eredmeny", 4400, Munkahely.lepcsozes(tomb5, 10, 40));
        assertEquals("Hibas eredmeny", 3040, Munkahely.lepcsozes(tomb6, 6, 20));
        assertEquals("Hibas eredmeny", 5610, Munkahely.lepcsozes(tomb7, 4, 15));
    }
}