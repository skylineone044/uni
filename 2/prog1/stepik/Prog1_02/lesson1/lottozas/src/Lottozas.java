public class Lottozas {

    static int lotto(int[] guesses, int[] numbers) {
        int talalatok = 0;
        for (int i : guesses) {
            for (int j : numbers) {
                if (i == j) {
                    talalatok++;
                }
            }
        }
        return talalatok;
    }

}