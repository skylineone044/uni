import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class Tests {
    @Test
    public void teszt1() {
        PrintStream old = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(baos));
        Main.main(new String[] {});
        System.setOut(old);
        assertEquals("Hibas kiiratas", "Macska", baos.toString().trim());
    }
}