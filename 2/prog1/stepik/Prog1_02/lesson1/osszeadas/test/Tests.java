import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Random;

public class Tests {
  @Test
  public void testSolution() {
    Random r = new Random();
    int a = r.nextInt(100);
    int b = r.nextInt(50) + 20;
    String string = a + " " + b;
    InputStream old = System.in;
    InputStream stream = new ByteArrayInputStream(string.getBytes());
    System.setIn(stream);

    PrintStream oldOut = System.out;
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    System.setOut(new PrintStream(baos));

    Osszeadas.main(new String[] {});

    System.setOut(oldOut);
    System.setIn(old);

    Assert.assertEquals("Hibas eredmeny", (a+b) + "", baos.toString().trim());
  }
}