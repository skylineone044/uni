import java.util.Scanner;

public class Palindrom {
    static boolean palindrome(String szo) {
        StringBuilder builder = new StringBuilder();
        builder.append(szo);
        builder.reverse();
        return (builder.toString().equals(szo));
    }

    static void main(String[] args) {
        String szo;
        Scanner sc = new Scanner(System.in);
        while (true) {
            szo = sc.nextLine();
            if (szo.equals("")) {
                return;
            }
            System.out.println( (Palindrom.palindrome(szo)? "igen" : "nem") );
        }
    }

}