import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Random;

import static org.junit.Assert.assertEquals;

public class Tests {

  private String doTest(String[] args) {
    StringBuilder input = new StringBuilder("");
    for (String s: args) {
      input.append(s).append(System.lineSeparator());
    }

    InputStream oldInput = System.in;
    InputStream stream = new ByteArrayInputStream(input.toString().getBytes());
    System.setIn(stream);

    PrintStream old = System.out;
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    System.setOut(new PrintStream(baos));

    Palindrom.main(args);

    System.setOut(old);
    System.setIn(oldInput);
    return baos.toString().trim();
  }

  @Test
  public void teszt() {
    assertEquals("Hibas eredmeny", "nem", doTest(new String[] {"macska", ""}));
    assertEquals("Hibas eredmeny", "nem" + System.lineSeparator() + "nem" + System.lineSeparator() + "nem", doTest(new String[] {"macska", "kutya", "lo", ""}));
    assertEquals("Hibas eredmeny", "igen", doTest(new String[] {"ollo", ""}));
    assertEquals("Hibas eredmeny", "nem" + System.lineSeparator() + "igen" + System.lineSeparator() + "igen", doTest(new String[] {"lovacska", "ollo", "med dem", ""}));
    assertEquals("Hibas eredmeny", "igen" + System.lineSeparator() + "igen", doTest(new String[] {"asd   dsa", "asdsa", ""}));
    assertEquals("Hibas eredmeny", "igen" + System.lineSeparator() + "nem" + System.lineSeparator() + "igen" + System.lineSeparator() + "nem" + System.lineSeparator() + "igen" + System.lineSeparator() + "igen" + System.lineSeparator() + "igen" + System.lineSeparator() + "nem",
            doTest(new String[] {"asddsa", "asfdsa", "asdfdsa", "asdqqsa", "aa", "v", "qaq", "aqq", ""}));
  }
}