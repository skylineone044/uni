public class Pizza {
    static int pizzaszallitas(Rendeles[] rendelesek, int darabszam) {
        int bevetel = 0;
        for (Rendeles megrendeles : rendelesek) {
            double kiszallitasiIdo = (60.0 - megrendeles.elkeszitesiIdo)/60.0; // oraban
//            double kiszallitasiIdopont = megrendeles.idopont + (megrendeles.elkeszitesiIdo)/60.0;
            int sebesseg = (megrendeles.idopont == 8 || megrendeles.idopont == 16 || megrendeles.idopont == 17 ? 25 : 40);
            double megtehetoTav = kiszallitasiIdo * sebesseg;
            if (megtehetoTav >= megrendeles.tavolsag) {
                bevetel += megrendeles.ar;
            } else {
                bevetel -= megrendeles.ar * 2;
            }
        }
        return bevetel;
    }

}