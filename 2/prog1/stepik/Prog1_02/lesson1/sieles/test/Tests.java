import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Tests {
    @Test
    public void teszt() {
        assertEquals("Hibas eredmeny", 1, Sieles.sieles(5, 10, 11));
        assertEquals("Hibas eredmeny", 1, Sieles.sieles(7, 10, 2));
        assertEquals("Hibas eredmeny", 1, Sieles.sieles(3, 5, 2));
        assertEquals("Hibas eredmeny", 1, Sieles.sieles(5, 50, 10));
        assertEquals("Hibas eredmeny", 0, Sieles.sieles(3, 10, 3));
        assertEquals("Hibas eredmeny", 0, Sieles.sieles(5, 50, 9));
        assertEquals("Hibas eredmeny", 0, Sieles.sieles(50, 51, 1));
    }
}