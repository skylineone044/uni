import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Szamok {
    static void main(String[] args) {
        int counter = 0;
        for (String szo : args) {
            Pattern pattern = Pattern.compile("^[0-9]+$", Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(szo);
            if(matcher.find()) {
                counter++;
            }
        }
        System.err.println(counter);
    }
}