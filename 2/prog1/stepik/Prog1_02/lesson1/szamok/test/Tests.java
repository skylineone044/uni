import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class Tests {

  private String doTest(String[] args) {
    PrintStream old = System.err;
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    System.setErr(new PrintStream(baos));
    Szamok.main(args);
    System.setErr(old);
    return baos.toString().trim();
  }

  @Test
  public void teszt() {
    assertEquals("Hibas eredmeny", "3", doTest(new String[] {"412", "111", "0"}));
    assertEquals("Hibas eredmeny", "0", doTest(new String[] {"macska", "lo", "tehen"}));
    assertEquals("Hibas eredmeny", "0", doTest(new String[] {" "}));
    assertEquals("Hibas eredmeny", "1", doTest(new String[] {"8907592478125504891244516428937526417854237830562222469154123891938172540179624141524812"}));
    assertEquals("Hibas eredmeny", "1", doTest(new String[] {"3124124", "macska", "77434n", "neptun5", "abalawiqopin4w5634angoa46eagn4oa4a44", "n55"}));
    assertEquals("Hibas eredmeny", "4", doTest(new String[] {"100", "442", "0", "12412415", "-200"}));
  }
}