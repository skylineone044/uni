public class Telefon {

    static int eladas(int hardverKoltseg, int szoftverKoltseg, int eladasiAr) {
        int penz = -szoftverKoltseg;
        int eladottTelefonok = 0;
        while (penz <= 0) {
            penz -= hardverKoltseg;
            penz += eladasiAr;
            eladottTelefonok +=1;
            if (eladottTelefonok > 100) {
                if (eladasiAr <= hardverKoltseg) {
                    return -1;
                }
            }
        }
        return eladottTelefonok;
    }

}