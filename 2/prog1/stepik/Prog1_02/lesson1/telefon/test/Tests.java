import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class Tests {
  @Test
  public void teszt() {
    Class klass = Telefon.class;
    try {
      Method m = klass.getDeclaredMethod("eladas", int.class, int.class, int.class);
      if (m.getReturnType() != int.class) {
        fail("A fuggveny visszateresi tipusa nem megfelelo");
      }
    } catch (NoSuchMethodException e) {
      fail("A fuggveny fejlece nem megfelelo");
    }

    assertEquals("hibas eredmeny", 1, Telefon.eladas(1000, 500, 2000));
    assertEquals("hibas eredmeny", 2, Telefon.eladas(1000, 1200, 2000));
    assertEquals("hibas eredmeny", 3, Telefon.eladas(1000, 1000, 1500));
    assertEquals("hibas eredmeny", 501, Telefon.eladas(300, 10000, 320));
    assertEquals("hibas eredmeny", 1, Telefon.eladas(300, 0, 320));
    assertEquals("hibas eredmeny", -1, Telefon.eladas(300, 0, 300));
    assertEquals("hibas eredmeny", -1, Telefon.eladas(300, 0, 299));
    assertEquals("hibas eredmeny", -1, Telefon.eladas(300, 1000, 300));
    assertEquals("hibas eredmeny", -1, Telefon.eladas(15, 100, 0));
  }
}