import java.util.Arrays;

public class Tolvaj {

    static boolean koSzamlalas(int lefoglaltKovek, int[] bejelentettKoEltunesek) {
        int sum = Arrays.stream(bejelentettKoEltunesek).sum();
        return sum <= lefoglaltKovek;
    }

}