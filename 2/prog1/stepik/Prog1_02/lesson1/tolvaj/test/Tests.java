import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class Tests {
    @Test
    public void teszt() {
      assertTrue("hibas eredmeny", Tolvaj.koSzamlalas(5, new int[] {3}));
      assertTrue("hibas eredmeny", Tolvaj.koSzamlalas(15, new int[] {3, 5, 6}));
      assertTrue("hibas eredmeny", Tolvaj.koSzamlalas(30, new int[] {3, 5, 5, 4, 2, 11}));
      assertTrue("hibas eredmeny", Tolvaj.koSzamlalas(5, new int[] {1, 1, 1, 1, 1}));
      assertFalse("hibas eredmeny", Tolvaj.koSzamlalas(5, new int[] {1, 1, 1, 2, 2}));
      assertTrue("hibas eredmeny", Tolvaj.koSzamlalas(140, new int[] {30, 30, 30, 30, 20}));
      assertFalse("hibas eredmeny", Tolvaj.koSzamlalas(1000, new int[] {10, 10, 980, 1}));
    }
}