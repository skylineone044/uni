import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Tests {
  @Test
  public void teszt() {
      assertEquals("Hibas eredmeny", 1, Promocio.utazas("Jaj de szep delfin!"));
      assertEquals("Hibas eredmeny", 1, Promocio.utazas("Meg szerencse, hogy nincs tulsozva ez a hus"));
      assertEquals("Hibas eredmeny", 0, Promocio.utazas("sos, sos! Segitseg, bajban vagyunk!"));
      assertEquals("Hibas eredmeny", 0, Promocio.utazas("Beleestem a vizbe, sos!"));
      assertEquals("Hibas eredmeny", 0, Promocio.utazas("Hatalmas viharba keveredtunk, sos!"));
      assertEquals("Hibas eredmeny", 1, Promocio.utazas("Nezd mar Jocoka, milyen szep a naplemente"));
  }
}