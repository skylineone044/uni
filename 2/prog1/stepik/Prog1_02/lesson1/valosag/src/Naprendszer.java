public class Naprendszer {
    static int valosagos(Bolygo[] bolygok, int darabszam) {
        for (int i = 0; i < darabszam-1; i++) {
            if (bolygok[i].kozetbolygo < bolygok[i+1].kozetbolygo) {
                return 0;
            }
        }
        return 1;
    }
}