import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Tests {

    public Bolygo createBolygo(int kozet) {
        Bolygo b = new Bolygo();
        b.nev = "Mars";
        b.tomeg = 2;
        b.kozetbolygo = kozet;
        return b;
    }

    @Test
    public void teszt() {
        Bolygo[]bolygok1 = {
                createBolygo(1),
                createBolygo(1),
                createBolygo(1),
                createBolygo(0),
                createBolygo(0)
        };

        assertEquals("Hibas eredmeny", 1, new Naprendszer().valosagos(bolygok1, 5));

        Bolygo[]bolygok2 = {
                createBolygo(1),
                createBolygo(0),
                createBolygo(0),
                createBolygo(0),
                createBolygo(0),
                createBolygo(0)
        };

        assertEquals("Hibas eredmeny", 1, new Naprendszer().valosagos(bolygok2, 6));

        Bolygo[]bolygok3 = {
                createBolygo(1),
                createBolygo(1),
                createBolygo(1)
        };

        assertEquals("Hibas eredmeny", 1, new Naprendszer().valosagos(bolygok3, 3));

        Bolygo[]bolygok4 = {
                createBolygo(1),
                createBolygo(1),
                createBolygo(0),
                createBolygo(0),
                createBolygo(1)
        };

        assertEquals("Hibas eredmeny", 0, new Naprendszer().valosagos(bolygok4, 5));

        Bolygo[]bolygok5 = {
                createBolygo(1),
                createBolygo(1),
                createBolygo(0),
                createBolygo(0),
                createBolygo(1)
        };

        assertEquals("Hibas eredmeny", 0, new Naprendszer().valosagos(bolygok5, 5));

        Bolygo[]bolygok6 = {
                createBolygo(1),
                createBolygo(0),
                createBolygo(0),
                createBolygo(1),
                createBolygo(1)
        };

        assertEquals("Hibas eredmeny", 0, new Naprendszer().valosagos(bolygok6, 5));

        Bolygo[]bolygok7 = {
                createBolygo(0),
                createBolygo(0),
                createBolygo(0),
                createBolygo(0)
        };

        assertEquals("Hibas eredmeny", 1, new Naprendszer().valosagos(bolygok7, 4));

        Bolygo[]bolygok8 = {
                createBolygo(1),
                createBolygo(0),
                createBolygo(1),
                createBolygo(0),
                createBolygo(1)
        };

        assertEquals("Hibas eredmeny", 0, new Naprendszer().valosagos(bolygok8, 5));
    }
}