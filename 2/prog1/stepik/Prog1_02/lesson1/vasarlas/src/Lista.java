public class Lista {
    static String vasarlas(Bolt[] boltok, int boltSzam) {
        double legolcsobbar = boltok[0].almaAr / boltok[0].almaTomeg;
        String legolcsobbolt = boltok[0].nev;
        for (Bolt bb : boltok) {
            if ((bb.almaAr / bb.almaTomeg) < legolcsobbar) {
                legolcsobbar = (bb.almaAr / bb.almaTomeg);
                legolcsobbolt = bb.nev;
            }
        }
        return legolcsobbolt;
    }
}