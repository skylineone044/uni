public class Munka {
    static int villamos(int[] felszallok, int megallok) {
        int utasok = 0;
        for (int i : felszallok) {
            utasok += i;
            if (utasok < 0) {
                return 0;
            }
        }
        return 1;
    }
}