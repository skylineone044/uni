import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Tests {
    @Test
    public void teszt() {
        int[] array1 = {1, 4};
        int[] array2 = {7, 3, 5, 2, 6, 0};
        int[] array3 = {5, 7, -3, -2, -3};
        int[] array4 = {3, 5, -8, 3, -3, 5, -5};
        int[] array5 = {3, -4, 2, 5, 7};
        int[] array6 = {5, 5, -3, -3, -3, -2, 1};
        int[] array7 = {6, 3, 5, -30};

        assertEquals("Hibas eredmeny", 1, Munka.villamos(array1, 2));
        assertEquals("Hibas eredmeny", 1, Munka.villamos(array2, 6));
        assertEquals("Hibas eredmeny", 1, Munka.villamos(array3, 5));
        assertEquals("Hibas eredmeny", 1, Munka.villamos(array4, 7));
        assertEquals("Hibas eredmeny", 0, Munka.villamos(array5, 5));
        assertEquals("Hibas eredmeny", 0, Munka.villamos(array6, 7));
        assertEquals("Hibas eredmeny", 0, Munka.villamos(array7, 4));
    }
}