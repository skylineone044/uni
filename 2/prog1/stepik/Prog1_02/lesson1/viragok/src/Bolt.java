public class Bolt {
    static int viragRendeles(Csokor[] raktarkeszlet, int raktarDarab, Csokor[] rendeles, int rendelesDarab) {
        for (Csokor rendelesCsokor : rendeles) {
            boolean van_ilyen_virag = false;
            for (Csokor raktarCsokor : raktarkeszlet) {
                if (rendelesCsokor.tipus == raktarCsokor.tipus) {
                    van_ilyen_virag = true;
                }
            }
            if (van_ilyen_virag ==false) {
                return 0;
            }
        }


        for (Csokor raktarCSokor : raktarkeszlet) {
            for (Csokor rendelesCsokor : rendeles) {
                if (raktarCSokor.tipus == rendelesCsokor.tipus) {
                    if (raktarCSokor.mennyiseg < rendelesCsokor.mennyiseg) {
                        return 0;
                    }
                }
            }
        }
        return 1;
    }
}