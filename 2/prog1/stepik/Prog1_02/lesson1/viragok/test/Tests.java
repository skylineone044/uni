import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Tests {

    public Csokor createCsokor(int a, int b) {
        Csokor cs = new Csokor();
        cs.tipus = a;
        cs.mennyiseg = b;
        return cs;
    }

    @Test
    public void alapEset() {
        Csokor[] raktarkeszlet = {
            createCsokor(1, 5),
            createCsokor(2, 3),
            createCsokor(3, 7),
            createCsokor(4, 2)
        };

        Csokor[] rendeles = {
            createCsokor(1, 3),
            createCsokor(2, 1),
            createCsokor(3, 4),
            createCsokor(4, 1)
        };

        assertEquals("Hibas eredmeny", 1, Bolt.viragRendeles(raktarkeszlet, 4, rendeles, 4));

        Csokor[] raktarkeszlet2 = {
            createCsokor(1, 5),
            createCsokor(2, 3),
            createCsokor(3, 7),
            createCsokor(4, 2)
        };

        Csokor[] rendeles2 = {
            createCsokor(1, 3),
            createCsokor(2, 4),
            createCsokor(3, 4),
            createCsokor(4, 1)
        };

        assertEquals("Hibas eredmeny", 0, Bolt.viragRendeles(raktarkeszlet2, 4, rendeles2, 4));
    }

    @Test
    public void kulonbozoHossz() {
        Csokor[] raktarkeszlet = {
            createCsokor(1, 5),
            createCsokor(2, 3)
        };

        Csokor[] rendeles = {
            createCsokor(1, 3),
            createCsokor(2, 1),
            createCsokor(3, 4),
            createCsokor(4, 1),
        };

        assertEquals("Hibas eredmeny", 0, Bolt.viragRendeles(raktarkeszlet, 2, rendeles, 4));

        Csokor[] raktarkeszlet2 = {
            createCsokor(1, 5),
            createCsokor(2, 3),
            createCsokor(3, 7),
            createCsokor(4, 2)
        };

        Csokor[] rendeles2 = {
            createCsokor(1, 3),
        };

        assertEquals("Hibas eredmeny", 1, Bolt.viragRendeles(raktarkeszlet2, 4, rendeles2, 1));
    }

    @Test
    public void randomSorrend() {
        Csokor[] raktarkeszlet = {
            createCsokor(2, 5),
            createCsokor(5, 3)
        };

        Csokor[] rendeles = {
            createCsokor(5, 4),
            createCsokor(2, 2)
        };

        assertEquals("Hibas eredmeny", 0, Bolt.viragRendeles(raktarkeszlet, 2, rendeles, 2));

        Csokor[] raktarkeszlet2 = {
            createCsokor(7, 55),
            createCsokor(4, 30),
            createCsokor(5, 71),
            createCsokor(11, 21),
            createCsokor(110, 51),
            createCsokor(66, 30),
            createCsokor(21, 71),
            createCsokor(1, 4)
        };

        Csokor[] rendeles2 = {
            createCsokor(5, 40),
            createCsokor(66, 20),
            createCsokor(110, 1),
            createCsokor(7, 5),
            createCsokor(21, 69)
        };

        assertEquals("Hibas eredmeny", 1, Bolt.viragRendeles(raktarkeszlet2, 8, rendeles2, 5));
    }

}