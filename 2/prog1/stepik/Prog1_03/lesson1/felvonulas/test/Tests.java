import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Tests {
  @Test
  public void teszt() {
    assertEquals("hibas eredmeny", 0, Felvonulas.szavazas(new String[] {"sarkanyszeker", "sarkanyszeker", "gyalogosok", "gyalogosok"}));
    assertEquals("hibas eredmeny", 1, Felvonulas.szavazas(new String[] {"sarkanyszeker", "lovasszeker", "gyalogosok", "gyalogosok"}));
    assertEquals("hibas eredmeny", 4, Felvonulas.szavazas(new String[] {"lovasszeker", "lovasszeker", "lovasszeker", "lovasszeker"}));
    assertEquals("hibas eredmeny", 2, Felvonulas.szavazas(new String[] {"lovasszeker", "sarkanyszeker", "gyalogosok", "lovasszeker", "auto", "motor", "bicikli"}));
    assertEquals("hibas eredmeny", 1, Felvonulas.szavazas(new String[] {"lovaskocsi", "lovaskocsi", "lovasszeker", "szeker", "szekereslo"}));
  }
}
