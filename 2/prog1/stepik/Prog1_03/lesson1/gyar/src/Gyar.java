public class Gyar {

    static int[] ures10ElemuTomb() {
        int[] tomb = new int[10];
        for (int i = 0; i < 10; i++) {
            tomb[i] = 0;
        }
        return tomb;
    }

    static int[] uresXElemuTomb(int x) {
        int[] tomb = new int[x];
        for (int i = 0; i < x; i++) {
            tomb[i] = 0;
        }
        return tomb;
    }

    static int[] novekvoXElemuTomb(int x) {
        int[] tomb = new int[x];
        for (int i = 0; i < x; i++) {
            tomb[i] = i;
        }
        return tomb;
    }

    static String[] jatekmaciXElemuTomb(int x) {
        String[] tomb = new String[x];
        for (int i = 0; i < x; i++) {
            tomb[i] = "jatekmaci";
        }
        return tomb;
    }

    static String[] vegyesjatekXElemuTomb(String paros, String paratlan, int darab) {
        String[] tomb = new String[darab];
        for (int i = 0; i < darab; i++) {
            if (i % 2 == 0) {
                tomb[i] = paros;
            } else {
                tomb[i] = paratlan;
            }
        }
        return tomb;
    }

}
