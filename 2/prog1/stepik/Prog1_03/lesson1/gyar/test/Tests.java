import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class Tests {
  @Test
  public void testUres10ElemuTomb() {
    int[] tomb = Gyar.ures10ElemuTomb();
    assertEquals("a tomb merete nem megfelelo", 10, tomb.length);

    for (int i = 0; i < 10; i++) {
      assertEquals("a tomb N. eleme nem 0", 0, tomb[i]);
    }
  }

  @Test
  public void testUresXElemuTomb() {
    int[] tomb1 = Gyar.uresXElemuTomb(17);
    assertEquals("a tomb merete nem megfelelo", 17, tomb1.length);

    for (int i = 0; i < 17; i++) {
      assertEquals("a tomb N. eleme nem 0", 0, tomb1[i]);
    }

    int[] tomb2 = Gyar.uresXElemuTomb(4);
    assertEquals("a tomb merete nem megfelelo", 4, tomb2.length);

    for (int i = 0; i < 4; i++) {
      assertEquals("a tomb N. eleme nem 0", 0, tomb2[i]);
    }
  }

  @Test
  public void testNovekvoXElemuTomb() {
    int[] tomb1 = Gyar.novekvoXElemuTomb(17);
    assertEquals("a tomb merete nem megfelelo", 17, tomb1.length);

    for (int i = 0; i < 17; i++) {
      assertEquals("a tomb N. eleme nem megfelelo", i, tomb1[i]);
    }

    int[] tomb2 = Gyar.novekvoXElemuTomb(40000);
    assertEquals("a tomb merete nem megfelelo", 40000, tomb2.length);

    for (int i = 0; i < 40000; i++) {
      assertEquals("a tomb N. eleme nem megfelelo", i, tomb2[i]);
    }
  }

  @Test
  public void testJatekmaciXElemuTomb() {
    String[] tomb1 = Gyar.jatekmaciXElemuTomb(6);
    assertEquals("a tomb merete nem megfelelo", 6, tomb1.length);

    for (int i = 0; i < 6; i++) {
      assertEquals("a tomb N. eleme nem jatekmaci", "jatekmaci", tomb1[i]);
    }

    String[] tomb2 = Gyar.jatekmaciXElemuTomb(46);
    assertEquals("a tomb merete nem megfelelo", 46, tomb2.length);

    for (int i = 0; i < 46; i++) {
      assertEquals("a tomb N. eleme nem jatekmaci", "jatekmaci", tomb2[i]);
    }
  }

  @Test
  public void testVegyesjatekXElemuTomb() {
    String[] tomb1 = Gyar.vegyesjatekXElemuTomb("jatekvonat", "jatekkatona", 1);
    assertEquals("a tomb merete nem megfelelo", 1, tomb1.length);

    assertEquals("a tomb N. eleme nem megfelelo", "jatekvonat", tomb1[0]);

    String[] tomb2 = Gyar.vegyesjatekXElemuTomb("igazivonat", "igazikatona", 24);
    assertEquals("a tomb merete nem megfelelo", 24, tomb2.length);

    for (int i = 0; i < 24; i++) {
      assertEquals("a tomb N. eleme nem megfelelo", (i%2==0)?"igazivonat":"igazikatona", tomb2[i]);
    }
  }

}