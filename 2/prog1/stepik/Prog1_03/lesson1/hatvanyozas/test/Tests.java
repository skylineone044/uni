import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Tests {
  @Test
  public void testSolution() {
    assertEquals("hibas eredmeny", 8, Hatvanyozas.hatvany(2, 3));
    assertEquals("hibas eredmeny", 36, Hatvanyozas.hatvany(6, 2));
    assertEquals("hibas eredmeny", 1024, Hatvanyozas.hatvany(2, 10));
    assertEquals("hibas eredmeny", 1, Hatvanyozas.hatvany(1, 5));
    assertEquals("hibas eredmeny", 1, Hatvanyozas.hatvany(141, 0));
    assertEquals("hibas eredmeny", 0, Hatvanyozas.hatvany(0, 41));
    assertEquals("hibas eredmeny", 3200, Hatvanyozas.hatvany(3200, 1));
  }
}