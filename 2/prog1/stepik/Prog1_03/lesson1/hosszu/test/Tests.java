import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static org.junit.Assert.*;

public class Tests extends BaseTest {
    @Test
    public void helyesseg() {
        assertEquals("Hibas eredmeny", 2, Hosszu.hossz("110010"));
        assertEquals("Hibas eredmeny", 0, Hosszu.hossz("00000000000"));
        assertEquals("Hibas eredmeny", 3, Hosszu.hossz("01011000001110"));
        assertEquals("Hibas eredmeny", 3, Hosszu.hossz("0111011101110111"));
        assertEquals("Hibas eredmeny", 10, Hosszu.hossz("1111111111"));
        assertEquals("Hibas eredmeny", 10, Hosszu.hossz("11111111110111111111"));
        assertEquals("Hibas eredmeny", 3, Hosszu.hossz("10110110110101110"));
        assertEquals("Hibas eredmeny", 1, Hosszu.hossz("1"));
        assertEquals("Hibas eredmeny", 1, Hosszu.hossz("101"));
        assertEquals("Hibas eredmeny", 3, Hosszu.hossz("111011"));
        assertEquals("Hibas eredmeny", 3, Hosszu.hossz("110111"));
    }

    @Test
    public void programMeret() {
        checkFileSize("./src/Hosszu.java", 140, 1, 2);
    }
}