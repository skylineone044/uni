public class Kepkeret {

    public static void main(String[] args) {
        int lowest = Integer.MAX_VALUE;
        int highest = Integer.MIN_VALUE;

        for (int i = 0; i < args.length; i++) {
            if (Integer.parseInt(args[i]) < lowest) {
                lowest = Integer.parseInt(args[i]);
            }
            if (Integer.parseInt(args[i]) > highest) {
                highest = Integer.parseInt(args[i]);
            }
        }
        System.out.println(highest - lowest);
    }

}