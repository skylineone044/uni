public class Kincsesterkep {

    static int terkep(int[][][] terkep) {
        int[] counters = new int[10];
        for (int i = 0; i < terkep.length; i++) {
            for (int j = 0; j < terkep[i].length; j++) {
                for (int k = 0; k < terkep[i][j].length; k++) {
                    counters[terkep[i][j][k]]++;
                }
            }
        }
        int min = Integer.MAX_VALUE;
        for (int counter : counters) {
            min = Math.min(counter, min);
        }
        return min;
    }

}