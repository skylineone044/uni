import java.util.Arrays;

public class Szamsokasag {
    public static void main(String[] args) {
        int[] ints = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            ints[i] = Integer.parseInt(args[i]);
        }
        Arrays.sort(ints);
        int leggakoribbGyakorisag = 1;
        int leggyakoriggElem = ints[0];

        int egyenlogyak = 0;

        int gyak = 1;
        int elem = ints[0];

        for (int i = 1; i < ints.length; i++) { // vegigmegyunk az elemeken
            if (ints[i] == ints[i-1]) { // megszamoljuk egy fajtabom mennyi van
                gyak += 1;
                elem = ints[i];
                if (gyak > leggakoribbGyakorisag) { // az uj gyakorubb mint az eddigi leggyakorib
                    leggakoribbGyakorisag = gyak;
                    leggyakoriggElem = elem;
                } else if (gyak == leggakoribbGyakorisag) { // az uj leggyakoribb ugyanolyan gyakori mint az eddigi leggyakoribb
                    if (gyak >= egyenlogyak) {
                        egyenlogyak = gyak;
                    }
                }
            } else { // uj fajtara terunk at
                gyak = 1;
                if (gyak >= egyenlogyak) {
                    egyenlogyak = gyak;
                }
//                elem = 0;
            }
        }
        if (egyenlogyak >= leggakoribbGyakorisag) {
            System.out.println("Tobb ilyen szam is van");
        } else {
            System.out.println("Ebbol van a legtobb: " + leggyakoriggElem);
        }
    }
}