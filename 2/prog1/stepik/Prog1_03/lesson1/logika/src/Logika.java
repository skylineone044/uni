public class Logika {

    static boolean logikai(boolean[] ertekek) {
        for (boolean ertek : ertekek) {
            if (!ertek) {
                return false;
            }
        }
        return true;
    }

}