import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Tests {

  @Test
  public void testSolution() {
    assertFalse("hibas eredmeny", Logika.logikai(new boolean[]{true, false, true, true, false, true, false, true, true, true, true}));
    assertTrue("hibas eredmeny", Logika.logikai(new boolean[]{true, true, true, true, true}));
    assertFalse("hibas eredmeny", Logika.logikai(new boolean[]{true, true, false}));
    assertFalse("hibas eredmeny", Logika.logikai(new boolean[]{true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false}));
    assertTrue("hibas eredmeny", Logika.logikai(new boolean[]{}));
  }
}