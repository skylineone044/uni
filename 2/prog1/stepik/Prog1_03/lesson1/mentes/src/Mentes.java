public class Mentes {
    private int gameSave = 0;

    public void completeLevel(int index) {
        if (!this.isCompleted(index)) {
            this.gameSave += Math.pow(2, index);
        }
    }

    public boolean isCompleted(int index) {
        int bean = this.gameSave;
        return (bean >> index & 1) == 1;
    }

    public void reset() {
        this.gameSave = 0;
    }
}