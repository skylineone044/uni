import java.math.BigDecimal;

public class Bolygo {
    int tavolsag;
    double keringesiSebesseg;
    double forgasiIdo;

    int napokSzamaEvenkent() {
        double palyaHossz = 2L * tavolsag * Math.PI;
        double keringesiIdo = (palyaHossz / keringesiSebesseg)/60L;
        double forgasokAForgasidoAlatt = keringesiIdo / forgasiIdo;
        return (int)forgasokAForgasidoAlatt;
    }
}