import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class Tests {

  private String doTest(String... args) {
    PrintStream old = System.out;
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    System.setOut(new PrintStream(baos));
    Pluszpontmentes.main(args);
    System.setOut(old);
    return baos.toString().trim();
  }

  private String concatenate(String... params) {
    StringBuilder sb = new StringBuilder();

    for (String s: params) {
      sb.append(s).append(System.lineSeparator());
    }

    return sb.toString().trim();
  }

  @Test
  public void testSolution() {
    assertEquals("hibas eredmeny", concatenate("David", "Peter", "Gyorgy", "Anett"), doTest("David", "0", "Peter", "0", "Gyorgy", "0", "Anett", "0"));
    assertEquals("hibas eredmeny", concatenate("Akos"), doTest("David", "5", "Peter", "2", "Gyorgy", "1", "Akos", "0"));
    assertEquals("hibas eredmeny", concatenate("Joco"), doTest("Joco", "0", "Peter", "1.1", "Gyorgy", "4.2", "Anett", "0.7"));
    assertEquals("hibas eredmeny", concatenate("David", "Antal"), doTest("Mark", "1000000", "Tamas", "1", "David", "0", "Juliska", "15", "Antal", "0", "Gabor", "0.1", "Laszlo", "0.9"));
  }
}