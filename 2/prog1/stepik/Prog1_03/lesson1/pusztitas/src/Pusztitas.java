import java.util.Arrays;

public class Pusztitas {

    public static int osszegyujtes (int[] emeletek, int hordozokapacitas) {
        int osszeshalottVirag = Arrays.stream(emeletek).sum();
        return (int)Math.ceil((double)osszeshalottVirag / hordozokapacitas);
    }

}