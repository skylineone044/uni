import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class Tests {
  @Test
  public void teszt() {
    try {
      Class klass = Pusztitas.class;
      Method m = klass.getDeclaredMethod("osszegyujtes", int[].class, int.class);

      if (m.getReturnType() != int.class && m.getReturnType() != long.class) {
        fail("Hibas visszateresi tipus");
      }

    } catch (Exception e) {
      fail("Hibas fuggveny fejlec");
    }

    assertEquals("hibas eredmeny", 2, Pusztitas.osszegyujtes(new int[] {2, 2, 2}, 3));
    assertEquals("hibas eredmeny", 3, Pusztitas.osszegyujtes(new int[] {5, 8, 11, 6}, 10));
    assertEquals("hibas eredmeny", 1, Pusztitas.osszegyujtes(new int[] {3, 0, 0, 0, 0, 0, 0, 7}, 10));
    assertEquals("hibas eredmeny", 1, Pusztitas.osszegyujtes(new int[] {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, 12));
    assertEquals("hibas eredmeny", 2, Pusztitas.osszegyujtes(new int[] {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, 12));
    assertEquals("hibas eredmeny", 0, Pusztitas.osszegyujtes(new int[] {0, 0, 0, 0, 0, 0, 0}, 10));
    assertEquals("hibas eredmeny", 2, Pusztitas.osszegyujtes(new int[] {4, 7, 2, 5}, 11));
  }
}