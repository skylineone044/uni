public class Reakcio {
    static boolean megvalosithato(String szoveg) {
        for (int i = 0; i < szoveg.length(); i++) {
            for (int j = 0; j < szoveg.length(); j++) {
                if (szoveg.charAt(i) == szoveg.charAt(j) && i != j) {
                    return false;
                }
            }
        }
        return true;
    }

    static String atalakit(String eredeti) {
        String eredmeny = "";
        for (int i = 0; i < eredeti.length(); i++) {
            if (!" ".equals(Character.toString(eredeti.charAt(i)))) {
                eredmeny = eredmeny.concat(":regional_indicator_").concat(Character.toString(eredeti.charAt(i))).concat(": ");
            } else {
                eredmeny = eredmeny.concat("   ");
            }
        }
        return eredmeny;
    }
}