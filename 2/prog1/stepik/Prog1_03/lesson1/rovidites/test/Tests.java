import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class Tests {

  @Test
  public void fejlec() {
    try {
      Class klass = Rovidites.class;
      Method m = klass.getDeclaredMethod("dekodolas", String.class);

      if (m.getReturnType() != String.class) {
        fail("Hibas visszateresi tipus");
      }
    } catch (Exception e) {
      fail("Hibas fuggveny fejlec");
    }
  }

  @Test
  public void nincsIsmetles() {
    assertEquals("hibas eredmeny", "uzenet", Rovidites.dekodolas("uzenet"));
    assertEquals("hibas eredmeny", "macska", Rovidites.dekodolas("macska"));
    assertEquals("hibas eredmeny", "itt a hold", Rovidites.dekodolas("itt a hold"));
    assertEquals("hibas eredmeny", "a leanderekkel mi lett?", Rovidites.dekodolas("a leanderekkel mi lett?"));
    assertEquals("hibas eredmeny", "ez volt az utolso feladat", Rovidites.dekodolas("ez volt az utolso feladat"));
  }

  @Test
  public void egyszeruIsmetles() {
    assertEquals("hibas eredmeny", "A leanderek kipusztultak", Rovidites.dekodolas("1A1 1l1e1a1n1d1e1r1e1k1 1k1i1p1u1s1z1t1u1l1t1a1k"));
    assertEquals("hibas eredmeny", "abbb", Rovidites.dekodolas("1a3b"));
    assertEquals("hibas eredmeny", "bbbbbbbbbbaaaa", Rovidites.dekodolas("5b5b4a"));
    assertEquals("hibas eredmeny", "xxxxxxxxxx", Rovidites.dekodolas("10x"));
    assertEquals("hibas eredmeny", "???????????????????????????????????????????????????????????????????????????????????????????????????????????????????????", Rovidites.dekodolas("119?"));
  }

  @Test
  public void zarojelesIsmetles() {
    assertEquals("hibas eredmeny", "aaa", Rovidites.dekodolas("3(a)"));
    assertEquals("hibas eredmeny", "abababab", Rovidites.dekodolas("4(ab)"));
    assertEquals("hibas eredmeny", "ssassassassassa", Rovidites.dekodolas("5(ssa)"));
    assertEquals("hibas eredmeny", "qhqhqhqhqhqhqhqhqhqhqhqhqh", Rovidites.dekodolas("13(qh)"));
    assertEquals("hibas eredmeny", "xyxyxyxyxyxyxyxyxyxyxyxyxyxyxyxyxyxyxy", Rovidites.dekodolas("19(xy)"));
    assertEquals("hibas eredmeny", "abababababcdcdcd", Rovidites.dekodolas("5(ab)3(cd)"));
    assertEquals("hibas eredmeny", "qsqsqxlalalalX.", Rovidites.dekodolas("q2(sq)xl3(al)X."));
  }

  @Test
  public void vegyesIsmetles() {
    assertEquals("hibas eredmeny", "mmmmmmmmmmssssss", Rovidites.dekodolas("10m3(ss)"));
    assertEquals("hibas eredmeny", "A mama otthon van...", Rovidites.dekodolas("A 2(ma) otthon van3."));
    assertEquals("hibas eredmeny", "Osszesitve: abababcccaaabcabcabcabcss", Rovidites.dekodolas("O2szesitve: 3(ab)3caa4(abc)2s"));
    assertEquals("hibas eredmeny", "aaabbbcccdddedededed", Rovidites.dekodolas("3a3b3c3(d)4(ed)"));
    assertEquals("hibas eredmeny", "ababcttt", Rovidites.dekodolas("1a1b1(abc)3t"));
    assertEquals("hibas eredmeny", "macskamacskamacskamacskamacskamacskamacskamacskamacskamacskamacska", Rovidites.dekodolas("11(macska)"));
  }

  @Test
  public void egymasbaAgyazottIsmetles() {
    assertEquals("hibas eredmeny", "AAxlkbasssssbasssss", Rovidites.dekodolas("AAxlk2(ba5s)"));
    assertEquals("hibas eredmeny", "aabbaabbsaabbaabbsaabbaabbsq", Rovidites.dekodolas("3(2(aa2b)s)q"));
    assertEquals("hibas eredmeny", "absssssqqqqqqqqabsssssqqqqqqqqabsssssqqqqqqqq", Rovidites.dekodolas("3(ab5s4(qq))"));
    assertEquals("hibas eredmeny", "lomplplplplamplplplplaqlomplplplplamplplplplaqlomplplplplamplplplplaqy", Rovidites.dekodolas("3(lo2(m4(pl)a)q)y"));
    assertEquals("hibas eredmeny", "lommmmmmmmmmmmplplplplammmmmmmmmmmmplplplplaqlommmmmmmmmmmmplplplplammmmmmmmmmmmplplplplaqlommmmmmmmmmmmplplplplammmmmmmmmmmmplplplplaqy", Rovidites.dekodolas("3(lo2(12m4(pl)a)q)y"));
    assertEquals("hibas eredmeny", "ma", Rovidites.dekodolas("ma0(sa0hsa3(bk)5a)"));
    assertEquals("hibas eredmeny", "ma", Rovidites.dekodolas("ma0(sa10000hsa300(bk)5a)"));
    assertEquals("hibas eredmeny", "skbaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooobaooo", Rovidites.dekodolas("sk100(ba3(o))"));
  }

}