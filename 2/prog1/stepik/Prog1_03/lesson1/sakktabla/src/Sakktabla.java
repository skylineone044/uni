public class Sakktabla {

    static boolean[][] sakk(int x, int y) {
        boolean[][] tabla = new boolean[x][y];
        boolean val = true;
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                tabla[i][j] = val;
                val = !val;
            }
            if (y % 2 == 0) {
                val = !val;
            }
        }
        return tabla;
    }

}