import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class Tests {
  @Test
  public void testSolution() {
    boolean[][] tomb1 = {
            {true, false, true, false, true},
            {false, true, false, true, false},
            {true, false, true, false, true}
    };

    boolean[][] tomb2 = {
            {true, false},
            {false, true},
            {true, false},
            {false, true},
            {true, false},
            {false, true},
            {true, false}
    };

    boolean[][] tomb3 = {
            {true}
    };

    boolean[][] tomb4 = {
            {true, false, true, false, true, false, true, false}
    };

    boolean[][] tomb5 = {
            {true}, {false}, {true}, {false}, {true}, {false}, {true}, {false}, {true}
    };


    assertArrayEquals("hibas eredmeny", tomb1, Sakktabla.sakk(3, 5));
    assertArrayEquals("hibas eredmeny", tomb2, Sakktabla.sakk(7, 2));
    assertArrayEquals("hibas eredmeny", tomb3, Sakktabla.sakk(1, 1));
    assertArrayEquals("hibas eredmeny", tomb4, Sakktabla.sakk(1, 8));
    assertArrayEquals("hibas eredmeny", tomb5, Sakktabla.sakk(9, 1));
  }
}