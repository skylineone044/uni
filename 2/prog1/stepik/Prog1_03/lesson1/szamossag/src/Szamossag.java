public class Szamossag {

    public static void main(String[] args) {
        int szamlalo = 0;
        for (String arg : args) {
            int szam = Integer.parseInt(arg);
            if (szam > 0) {
                szamlalo += 1;
            } else if (szam < 0) {
                szamlalo -= 1;
            }
        }
        if (szamlalo > 0) {
            System.out.println("pozitivbol van tobb");
        } else if (szamlalo < 0) {
            System.out.println("negativbol van tobb");
        } else {
            System.out.println("mind a kettobol ugyanannyi van");
        }
    }

}