import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Tests extends BaseTest {
    @Test
    public void teszt() {
        assertEquals("Hibas eredmeny", "pozitivbol van tobb", doArgumentTest(Szamossag.class, "2", "5", "8", "11"));
        assertEquals("Hibas eredmeny", "pozitivbol van tobb", doArgumentTest(Szamossag.class, "2", "-5", "-8", "11", "77"));
        assertEquals("Hibas eredmeny", "pozitivbol van tobb", doArgumentTest(Szamossag.class, "-2", "5", "8", "11"));
        assertEquals("Hibas eredmeny", "negativbol van tobb", doArgumentTest(Szamossag.class, "-2", "5", "-8", "-11"));
        assertEquals("Hibas eredmeny", "negativbol van tobb", doArgumentTest(Szamossag.class, "-51", "-11", "-73", "0"));
        assertEquals("Hibas eredmeny", "mind a kettobol ugyanannyi van", doArgumentTest(Szamossag.class, "2", "5", "8", "11", "-55", "-12", "-1", "-22"));
        assertEquals("Hibas eredmeny", "mind a kettobol ugyanannyi van", doArgumentTest(Szamossag.class));
        assertEquals("Hibas eredmeny", "mind a kettobol ugyanannyi van", doArgumentTest(Szamossag.class, "1", "-2"));
        assertEquals("Hibas eredmeny", "pozitivbol van tobb", doArgumentTest(Szamossag.class, "2", "0", "0"));
        assertEquals("Hibas eredmeny", "negativbol van tobb", doArgumentTest(Szamossag.class, "-3", "0", "0"));
        assertEquals("Hibas eredmeny", "mind a kettobol ugyanannyi van", doArgumentTest(Szamossag.class, "0", "0", "0", "0", "0"));
    }
}