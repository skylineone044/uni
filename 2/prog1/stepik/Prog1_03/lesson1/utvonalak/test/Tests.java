import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Tests {
  @Test
  public void teszt() {
    assertEquals("hibas eredmeny", 1, Utvonalak.legrovidebb(new int[] {6, 3, 5}));
    assertEquals("hibas eredmeny", 1, Utvonalak.legrovidebb(new int[] {6, 3, 5, 4, 5, 7}));
    assertEquals("hibas eredmeny", 4, Utvonalak.legrovidebb(new int[] {6, 4, 9, 11, 3, 6}));
    assertEquals("hibas eredmeny", 0, Utvonalak.legrovidebb(new int[] {6000}));
    assertEquals("hibas eredmeny", 2, Utvonalak.legrovidebb(new int[] {6, 6, 2, 2, 5, 4, 5, 7}));
    assertEquals("hibas eredmeny", 0, Utvonalak.legrovidebb(new int[] {5, 5, 5, 5, 5, 5}));
  }
}