import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class Tests {
  @Test
  public void teszt() {
    assertTrue("Hibas eredmeny", Vasarlas.vasarlas(100, 40));
    assertTrue("Hibas eredmeny", Vasarlas.vasarlas(10, 7));
    assertTrue("Hibas eredmeny", Vasarlas.vasarlas(23, 23));
    assertFalse("Hibas eredmeny", Vasarlas.vasarlas(100, 111));
    assertFalse("Hibas eredmeny", Vasarlas.vasarlas(10, 23));
    assertFalse("Hibas eredmeny", Vasarlas.vasarlas(23, 24));
    assertFalse("Hibas eredmeny", Vasarlas.vasarlas(10, 40));
  }
}