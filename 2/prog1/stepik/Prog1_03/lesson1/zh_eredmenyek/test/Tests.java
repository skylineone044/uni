import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Random;

import static org.junit.Assert.assertEquals;

public class Tests {

    private String doTest(String... args) {
      PrintStream old = System.out;
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      System.setOut(new PrintStream(baos));
      ZhEredmenyek.main(args);
      System.setOut(old);
      return baos.toString().trim();
    }

    @Test
    public void testSolution() {
        assertEquals("hibas eredmeny", 1, Double.parseDouble(doTest("1")), 0.01);
        assertEquals("hibas eredmeny", 5, Double.parseDouble(doTest("0", "10")), 0.01);
        assertEquals("hibas eredmeny", 4, Double.parseDouble(doTest("1", "4", "6", "5")), 0.01);
        assertEquals("hibas eredmeny", 4.5, Double.parseDouble(doTest("4", "5")), 0.01);
        assertEquals("hibas eredmeny", 6.625, Double.parseDouble(doTest("4", "3", "8", "10", "10", "10", "2", "6")), 0.01);
    }
}