import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class BaseTest {

    @SuppressWarnings("SameParameterValue")
    protected boolean testField(Class<?> c, String name, Class<?> type) {
        try {
            Field res = c.getDeclaredField(name);
            return res.getType() == type;
        } catch (NoSuchFieldException ignored) {}

        return false;
    }

    protected boolean testMethod(Class<?> c, String name, Class<?> returnType, Class<?>... params) {
        try {
            Method res = c.getDeclaredMethod(name, params);
            return res.getReturnType() == returnType;
        } catch (NoSuchMethodException ignored) {}

        return false;
    }

    protected String doArgumentTest(Class<?> cl, String... args) {
        PrintStream old = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(baos));
        try {
            Method m = cl.getDeclaredMethod("main", String[].class);
            m.invoke(null, (Object) args);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        System.setOut(old);
        return baos.toString().trim();
    }

}
