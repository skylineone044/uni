import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class Tests extends BaseTest {
  @Test
  public void testSolution() {
    assertTrue("nev hibas", testField(Alkalmazas.class, "nev", String.class));
    assertTrue("keszito hibas", testField(Alkalmazas.class, "keszito", String.class));
    assertTrue("verzioszam hibas", testField(Alkalmazas.class, "verzioszam", int.class));
    assertTrue("ertekeles hibas", testField(Alkalmazas.class, "ertekeles", double.class));
  }
}