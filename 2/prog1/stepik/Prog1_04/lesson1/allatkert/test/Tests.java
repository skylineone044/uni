import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.assertTrue;

public class Tests {

  @SuppressWarnings("SameParameterValue")
  private boolean testField(Class<?> c, String name, Class<?> type) {
    try {
      Field res = c.getDeclaredField(name);
      return res.getType() == type;
    } catch (NoSuchFieldException ignored) {}

    return false;
  }

  @Test
  public void testSolution() {
    assertTrue("allat: nev hibas", testField(Allat.class, "nev", String.class));
    assertTrue("allat: eletkor hibas", testField(Allat.class, "eletkor", int.class));
    assertTrue("allat: meret hibas", testField(Allat.class, "meret", double.class));

    assertTrue("allatkert: nev hibas", testField(Allatkert.class, "nev", String.class));
    assertTrue("allatkert: varos hibas", testField(Allatkert.class, "varos", String.class));
    assertTrue("allatkert: allatok hibas", testField(Allatkert.class, "allatok", Allat[].class));
  }
}