public class Bankszamla {
    private String tulajdonos;
    private int osszeg;

    public Bankszamla(String tulajdonos) {
        this.tulajdonos = tulajdonos;
        this.osszeg = 1000;
    }

    public boolean jovairas(int pluszpenz) {
        this.osszeg += pluszpenz;
        return true;
    }

    public boolean koltes(int levonas) {
        if (levonas <= this.osszeg) {
            this.osszeg -= levonas;
            return true;
        } else {
            return false;
        }
    }
}