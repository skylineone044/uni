public class Edeny {
    private String anyag;
    private int meret;
    private Mazsola[] mazsolak;

    public Edeny(String enyag, int meret, int mazsolakszama) {
        this.mazsolak = new Mazsola [mazsolakszama];
        this.anyag = enyag;
        this.meret = meret;

        for (int i = 0; i < mazsolakszama; i++) {
            mazsolak[i] = null;
        }
    }

    public int getMeret() {
        return meret;
    }

    public Mazsola[] getMazsolak() {
        return this.mazsolak;
    }

    public String getAnyag() {
        return anyag;
    }

    public void addMazsola(Mazsola mazsola) {
        for (int i = 0; i < mazsolak.length; i++) {
            if (mazsolak[i] == mazsola) {
                break;
            } else if (mazsolak[i] == null) {
                mazsolak[i] = mazsola;
                break;
            }
        }
    }
    public void deleteMazsola(Mazsola mazsola) {
        for (int i = 0; i < mazsolak.length; i++) {
            if (mazsolak[i] == mazsola) {
                mazsolak[i] = null;
                for (int j = i; j < mazsolak.length; j++) {
                    mazsolak[j] = mazsolak[j+1];
                }
            }
        }
    }
}