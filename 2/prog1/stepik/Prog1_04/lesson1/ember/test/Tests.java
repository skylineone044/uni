import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class Tests {

  private boolean testMethod(Class<?> c, String name, Class<?> returnType, Class<?>... params) {
    try {
      Method res = c.getDeclaredMethod(name, params);
      return res.getReturnType() == returnType;
    } catch (NoSuchMethodException ignored) {}

    return false;
  }

  @Test
  public void testSolution() {
    assertTrue("asit metodus hibas", testMethod(Ember.class, "asit", void.class));
    assertTrue("alszik metodus hibas", testMethod(Ember.class, "alszik", void.class, int.class));
    assertTrue("eszik metodus hibas", testMethod(Ember.class, "eszik", void.class, Etel.class));
    assertTrue("iszik metodus hibas", testMethod(Ember.class, "iszik", void.class, Ital.class));
    assertTrue("okos metodus hibas", testMethod(Ember.class, "okos", boolean.class));
    assertTrue("ferfi metodus hibas", testMethod(Ember.class, "ferfi", boolean.class));
    assertTrue("vasarol metodus hibas", testMethod(Ember.class, "vasarol", boolean.class, int[].class));
    assertTrue("szamol metodus hibas", testMethod(Ember.class, "szamol", int.class, int.class, int.class, int.class));
    assertTrue("velemeny metodus hibas", testMethod(Ember.class, "velemeny", String.class, int.class, double.class));
  }
}