import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import static org.junit.Assert.*;

public class Tests {

  @SuppressWarnings("SameParameterValue")
  private boolean testField(Class<?> c, String name, Class<?> type) {
    try {
      Field res = c.getDeclaredField(name);
      return res.getType() == type;
    } catch (NoSuchFieldException ignored) {}

    return false;
  }

  private boolean testMethod(Class<?> c, String name, Class<?> returnType, Class<?>... params) {
    try {
      Method res = c.getDeclaredMethod(name, params);
      return res.getReturnType() == returnType;
    } catch (NoSuchMethodException ignored) {}

    return false;
  }

  @Test
  public void testSolution() {
    assertTrue("nev adattag hibas", testField(Pilota.class, "nev", String.class));
    assertTrue("eletkor adattag hibas", testField(Pilota.class, "eletkor", int.class));
    assertTrue("tapasztalat adattag hibas", testField(Pilota.class, "tapasztalat", double.class));

    assertTrue("vezet metodus hibas", testMethod(Pilota.class, "vezet", boolean.class, int.class));
    assertTrue("fejlodik metodus hibas", testMethod(Pilota.class, "fejlodik", void.class));
    assertTrue("oregszik metodus hibas", testMethod(Pilota.class, "oregszik", void.class));

    Pilota p = new Pilota();

    p.eletkor = 20;
    p.oregszik();
    assertEquals("az oregszik metodus nem mukodik", 21, p.eletkor);
    p.oregszik();
    assertEquals("az oregszik metodus nem mukodik", 22, p.eletkor);

    p.tapasztalat = 30;
    p.fejlodik();
    assertEquals("a fejlodik metodus nem mukodik", 33, p.tapasztalat, 0.001);

    p.tapasztalat = 21.2;
    p.fejlodik();
    assertEquals("a fejlodik metodus nem mukodik", 23.32, p.tapasztalat, 0.001);

    p.tapasztalat = 20;
    assertTrue("a vezet metodus nem mukodik", p.vezet(1));
    assertTrue("a vezet metodus nem mukodik", p.vezet(5));
    assertFalse("a vezet metodus nem mukodik", p.vezet(40));
    assertFalse("a vezet metodus nem mukodik", p.vezet(48));
    assertFalse("a vezet metodus nem mukodik", p.vezet(66));
  }
}