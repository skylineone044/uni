import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Tests {
  @Test
  public void testSolution() {
    Gyar gyar = new Gyar();

    Csoki csoki = gyar.csokiGyartas();
    Szaloncukor szaloncukor = gyar.szaloncukorGyartas();
    Keksz keksz = gyar.kekszGyartas();
    Nyaloka nyaloka = gyar.nyalokaGyartas();

    assertEquals("csokigyartas hibas", Csoki.class, csoki == null ? null : csoki.getClass());
    assertEquals("szaloncukorGyartas hibas", Szaloncukor.class, szaloncukor == null ? null : szaloncukor.getClass());
    assertEquals("kekszGyartas hibas", Keksz.class, keksz == null ? null : keksz.getClass());
    assertEquals("nyalokaGyartas hibas", Nyaloka.class, nyaloka == null ? null : nyaloka.getClass());
  }
}