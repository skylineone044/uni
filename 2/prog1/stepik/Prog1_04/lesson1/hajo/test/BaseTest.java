import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class BaseTest {

    @SuppressWarnings("SameParameterValue")
    protected boolean testField(Class<?> c, String name, Class<?> type, Integer modifier) {
        try {
            Field res = c.getDeclaredField(name);

            if (res.getType() != type) {
                return false;
            }

            return modifier == null || res.getModifiers() == modifier;
        } catch (NoSuchFieldException ignored) {}

        return false;
    }

    protected boolean testMethod(Class<?> c, String name, Class<?> returnType, Integer modifier, Class<?>... params) {
        try {
            Method res = c.getDeclaredMethod(name, params);

            if (res.getReturnType() != returnType) {
                return false;
            }

            return modifier == null || res.getModifiers() == modifier;
        } catch (NoSuchMethodException ignored) {}

        return false;
    }

    protected Object fieldValue(Class<?> c, String name, Object o) {
        try {
            Field f = c.getDeclaredField(name);
            f.setAccessible(true);
            return f.get(o);
        } catch (NoSuchFieldException | IllegalAccessException ignored) {}

        return null;
    }

    protected String doArgumentTest(Class<?> cl, String... args) {
        PrintStream old = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(baos));
        try {
            Method m = cl.getDeclaredMethod("main", String[].class);
            m.invoke(null, (Object) args);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        System.setOut(old);
        return baos.toString().trim();
    }

}
