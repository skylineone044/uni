import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Tests extends BaseTest {
  @Test
  public void testSolution() {
    Hajo hajo = new Hajo();
    assertEquals("nev hibas", "MARIJAAA", fieldValue(Hajo.class, "nev", hajo));
    assertEquals("meret hibas", 10, fieldValue(Hajo.class, "meret", hajo));
    assertEquals("agyukSzama hibas", 2, fieldValue(Hajo.class, "agyukSzama", hajo));
    assertEquals("felavatva hibas", false, fieldValue(Hajo.class, "felavatva", hajo));
  }
}