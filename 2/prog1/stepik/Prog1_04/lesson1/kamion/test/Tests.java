import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.*;

public class Tests extends BaseTest {

  @Test
  public void testSolution() {
    assertTrue("rendszam hibas", testField(Kamion.class, "rendszam", String.class));
    assertTrue("max telefonok hibas", testField(Kamion.class, "maxTelefonok", int.class));
    assertTrue("hossz hibas", testField(Kamion.class, "hossz", double.class));
    assertTrue("szelesseg hibas", testField(Kamion.class, "szelesseg", double.class));
  }
}