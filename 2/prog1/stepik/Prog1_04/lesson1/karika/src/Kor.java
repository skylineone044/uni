public class Kor {
    private int atmero;

    public int getAtmero() {
        return atmero;
    }

    public void setAtmero(int atmero) {
        this.atmero = (atmero <= 0 ? 1 : atmero);
    }
    public Kor(int atmero) {
        setAtmero(atmero);
    }
}
