public class Lampa {
    boolean vilagit;

    public void setVilagit(boolean vilagit) {
        this.vilagit = vilagit;
    }

    public boolean isVilagit() {
        return vilagit;
    }

    public Lampa(boolean vilagitas) {
        setVilagit(vilagitas);
    }

    @Override
    public String toString() {
        return (this.vilagit ? "vilagos van" : "sotet van");
    }
}