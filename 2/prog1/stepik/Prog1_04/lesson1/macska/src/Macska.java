public class Macska {
    final private String nev;
    private String szin;
    private int meret;

    public Macska(String nev, String szin, int meret) {
        this.nev = nev;
        setSzin(szin);
        setMeret(meret);
    }

    public String getNev() {
        return nev;
    }

    public String getSzin() {
        return szin;
    }

    public int getMeret() {
        return meret;
    }

    public void setNev(String nev) {
    }

    public void setMeret(int meret) {
        this.meret = meret;
    }

    public void setSzin(String szin) {
        this.szin = szin;
    }
}