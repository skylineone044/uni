public class Main {
    public static void main(String[] args) {
        for (int i = 0; i < args.length; i++) {
            if ("Macska".equals(args[i])) {
                Macska m = new Macska(args[i+1], Integer.parseInt(args[i+2]));
                System.out.println(m.toString());
                i += 2;
            } else if ("Beka".equals(args[i])) {
                Beka b = new Beka(args[i+1]);
                System.out.println(b.toString());
                i += 1;
            } else {
                System.err.println("HIBA");
            }
        }
    }
}