import javax.swing.text.MaskFormatter;

public class Madar {
  private String nev;
  private int repulesiSebesseg;
  private int meret;

    public Madar(String nev, int repulesiSebesseg, int meret) {
        this.nev = nev;
        this.repulesiSebesseg = repulesiSebesseg;
        this.meret = meret;
    }

    public static boolean megtalalhato(Madar[] madarak, String nev) {
        for (Madar m : madarak) {
            if (m.getNev().equals(nev)) {
                return true;
            }
        }
        return false;
    }

    public static Madar leggyorsabb(Madar[] madarak) {
        if (madarak.length == 0) {
            return null;
        }
        int legnagyobb_i = 0;
        for (int i = 0; i < madarak.length; i++) {
            if (madarak[i].getRepulesiSebesseg() > madarak[legnagyobb_i].getRepulesiSebesseg()) {
                legnagyobb_i = i;
            }
        }
        return madarak[legnagyobb_i];
    }

    public static int kicsik(Madar[] madarak, int kuszob) {
        int szamlalo = 0;
        for (Madar m : madarak) {
            if (m.getMeret() < kuszob) {
                szamlalo += 1;
            }
        }
        return szamlalo;
    }

    public String getNev() {
        return nev;
    }

    public void setNev(String nev) {
        this.nev = nev;
    }

    public int getRepulesiSebesseg() {
        return repulesiSebesseg;
    }

    public void setRepulesiSebesseg(int repulesiSebesseg) {
        this.repulesiSebesseg = repulesiSebesseg;
    }

    public int getMeret() {
        return meret;
    }

    public void setMeret(int meret) {
        this.meret = meret;
    }

    @Override
    public String toString() {
        return "Madar{" +
                "nev='" + nev + '\'' +
                ", repulesiSebesseg=" + repulesiSebesseg +
                ", meret=" + meret +
                '}';
    }

}