import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class Tests {
  @Test
  public void testSolution() {
    Madar m1 = new Madar("Hollo", 3, 5);
    Madar m2 = new Madar("Vereb", 3, 6);
    Madar m3 = new Madar("kecske", 7, 3);
    Madar m4 = new Madar("rigo", 1, 130);
    Madar m5 = new Madar("rigo", 4, 2);
    Madar m6 = new Madar("nyuszi", 12, 7);
    Madar m7 = new Madar("repulo", 12, 1);

    Madar[] madarak1 = { m1, m2, m3, m4 };
    Madar[] madarak2 = { m4, m5, m6, m7, m2 };
    Madar[] madarak3 = {};

    assertTrue("megtalalhato metodus hibas", Madar.megtalalhato(madarak1, "Hollo"));
    assertTrue("megtalalhato metodus hibas", Madar.megtalalhato(madarak1, "rigo"));
    assertFalse("megtalalhato metodus hibas", Madar.megtalalhato(madarak1, "rigorigo"));
    assertFalse("megtalalhato metodus hibas", Madar.megtalalhato(madarak2, "Hollo"));
    assertTrue("megtalalhato metodus hibas", Madar.megtalalhato(madarak2, "rigo"));
    assertFalse("megtalalhato metodus hibas", Madar.megtalalhato(madarak3, "rigo"));

    assertSame("leggyorsabb metodus hibas", m3, Madar.leggyorsabb(madarak1));
    assertSame("leggyorsabb metodus hibas", m6, Madar.leggyorsabb(madarak2));
    assertSame("leggyorsabb metodus hibas", null, Madar.leggyorsabb(madarak3));

    assertEquals("kicsik metodus hibas", 3, Madar.kicsik(madarak1, 10));
    assertEquals("kicsik metodus hibas", 0, Madar.kicsik(madarak1, 3));
    assertEquals("kicsik metodus hibas", 2, Madar.kicsik(madarak2, 5));
    assertEquals("kicsik metodus hibas", 1, Madar.kicsik(madarak2, 2));
    assertEquals("kicsik metodus hibas", 0, Madar.kicsik(madarak2, -500));
    assertEquals("kicsik metodus hibas (ures tomb)", 0, Madar.kicsik(madarak3, 5));
  }
}