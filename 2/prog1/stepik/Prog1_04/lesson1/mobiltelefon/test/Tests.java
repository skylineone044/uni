import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.assertTrue;

public class Tests {

  @SuppressWarnings("SameParameterValue")
  private boolean testField(Class<?> c, String name, Class<?> type) {
    try {
      Field res = c.getDeclaredField(name);
      return res.getType() == type;
    } catch (NoSuchFieldException ignored) {}

    return false;
  }

  @Test
  public void testSolution() {
    assertTrue("a meret adattag nem jo", testField(Mobiltelefon.class, "meret", int.class));
    assertTrue("a gyarto adattag nem jo", testField(Mobiltelefon.class, "gyarto", String.class));
    assertTrue("a memoria adattag nem jo", testField(Mobiltelefon.class, "memoria", int.class));
  }
}