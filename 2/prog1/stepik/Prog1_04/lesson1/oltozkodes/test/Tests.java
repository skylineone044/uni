import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class Tests extends BaseTest {
  @Test
  public void testSolution() {
    assertTrue("nev hibas", testField(Ember.class, "nev", String.class));
    assertTrue("a cipo hibas", testField(Ember.class, "cipo", Cipo.class));
    assertTrue("a nadrag hibas", testField(Ember.class, "nadrag", Nadrag.class));
    assertTrue("a polo hibas", testField(Ember.class, "polo", Polo.class));

    assertTrue("a Cipo osztaly modositva lett", testField(Cipo.class, "szin", String.class));
    assertTrue("a Nadrag osztaly modositva lett", testField(Nadrag.class, "szin", String.class));
    assertTrue("a Nadrag osztaly modositva lett", testField(Nadrag.class, "tipus", String.class));
    assertTrue("a Polo osztaly modositva lett", testField(Polo.class, "mintas", boolean.class));
    assertTrue("a Polo osztaly modositva lett", testField(Polo.class, "meret", int.class));
  }
}