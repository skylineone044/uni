import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.assertTrue;

public class Tests {

  @SuppressWarnings("SameParameterValue")
  private boolean testField(Class<?> c, String name, Class<?> type) {
    try {
      Field res = c.getDeclaredField(name);
      return res.getType() == type;
    } catch (NoSuchFieldException ignored) {}

    return false;
  }

  @Test
  public void testSolution() {
    assertTrue("papagaj: nev hibas", testField(Papagaj.class, "nev", String.class));
    assertTrue("papagaj: szin hibas", testField(Papagaj.class, "szin", String.class));
    assertTrue("papagaj: szavak hibas", testField(Papagaj.class, "szavak", String[].class));
  }
}