import java.text.DecimalFormat;

public class Penztarca {
    private String szin;
    private String anyag;
    private double meret;
    private int penzmennyiseg;

    public Penztarca(String szin, String anyag, double meret, int penzmennyiseg) {
        this.szin = szin;
        this.anyag = anyag;
        this.meret = meret;
        this.penzmennyiseg = penzmennyiseg;
    }

    @Override
    public String toString() {
        DecimalFormat df = new DecimalFormat("0.##");
        return String.format("A penztarca %s szinu, %s anyagu. A merete: %s, a benne levo penzmennyiseg: %d.",
                this.szin, this.anyag, df.format(this.meret), this.penzmennyiseg);
    }
}
