import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Tests extends BaseTest {
    @Test
    public void t1() {
        Pokemon p = new Pokemon("a", "b");
        assertEquals(sout.toString(), "EGY POKEMON LETREJOTT" + System.lineSeparator());
        assertEquals("a", fieldValue(Pokemon.class, "nev", p));
        assertEquals("b", fieldValue(Pokemon.class, "allapot", p));

        Pokemon pp = new Pokemon("bbb", "aaa");
        assertEquals(sout.toString(), "EGY POKEMON LETREJOTT" + System.lineSeparator() + "EGY POKEMON LETREJOTT" + System.lineSeparator());
        assertEquals("bbb", fieldValue(Pokemon.class, "nev", pp));
        assertEquals("aaa", fieldValue(Pokemon.class, "allapot", pp));
    }

    @Test
    public void t2() {
        Pokemon p = new Pokemon("a");
        assertEquals(sout.toString(), "EGY POKEMON LETREJOTT" + System.lineSeparator());
        assertEquals("a", fieldValue(Pokemon.class, "nev", p));
        assertEquals("szabad", fieldValue(Pokemon.class, "allapot", p));

        Pokemon pp = new Pokemon("qqq");
        assertEquals(sout.toString(), "EGY POKEMON LETREJOTT" + System.lineSeparator() + "EGY POKEMON LETREJOTT" + System.lineSeparator());
        assertEquals("qqq", fieldValue(Pokemon.class, "nev", pp));
        assertEquals("szabad", fieldValue(Pokemon.class, "allapot", pp));
    }
}