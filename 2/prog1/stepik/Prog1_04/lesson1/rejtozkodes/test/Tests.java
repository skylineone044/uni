import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;

public class Tests extends BaseTest {
  @Test
  public void testSolution() {
    assertTrue("alfaj hibas", testField(Kameleon.class, "alfaj", String.class, Modifier.PRIVATE));
    assertTrue("meret hibas", testField(Kameleon.class, "meret", int.class, Modifier.PRIVATE));
    assertTrue("szin hibas", testField(Kameleon.class, "szin", String.class, Modifier.PRIVATE));
  }
}