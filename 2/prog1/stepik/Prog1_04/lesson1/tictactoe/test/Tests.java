import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Tests extends BaseTest {
    @Test
    public void mukodes() {
        assertEquals("Hibas eredmeny", "Attila nyert",  doArgumentTest(A.class, "0", "1", "0", "1", "0", "1", "1", "0", "0"));
        assertEquals("Hibas eredmeny", "Attila nyert",  doArgumentTest(A.class, "0", "-1", "-1", "0", "-1", "-1", "0", "1", "1"));
        assertEquals("Hibas eredmeny", "Attila nyert",  doArgumentTest(A.class, "0", "0", "0", "1", "0", "-1", "-1", "1", "1"));
        assertEquals("Hibas eredmeny", "Bella nyert",  doArgumentTest(A.class, "0", "1", "0", "0", "1", "-1", "1", "1", "0"));
        assertEquals("Hibas eredmeny", "Nincs nyertes",  doArgumentTest(A.class, "0", "1", "0", "1", "1", "0", "0", "0", "1"));
        assertEquals("Hibas eredmeny", "Attila nyert",  doArgumentTest(A.class, "-1", "-1", "0", "-1", "0", "1", "0", "1", "-1"));
        assertEquals("Hibas eredmeny", "Attila nyert",  doArgumentTest(A.class, "0", "1", "1", "0", "1", "1", "0", "0", "0"));
        assertEquals("Hibas eredmeny", "Attila nyert",  doArgumentTest(A.class, "0", "1", "1", "0", "-1", "1", "0", "-1", "0"));
        assertEquals("Hibas eredmeny", "Bella nyert",  doArgumentTest(A.class, "0", "0", "1", "0", "0", "1", "-1", "1", "1"));
        assertEquals("Hibas eredmeny", "Bella nyert",  doArgumentTest(A.class, "-1", "-1", "0", "1", "1", "1", "-1", "0", "0"));
        assertEquals("Hibas eredmeny", "Bella nyert",  doArgumentTest(A.class, "0", "-1", "0", "0", "0", "1", "1", "1", "1"));
        assertEquals("Hibas eredmeny", "Bella nyert",  doArgumentTest(A.class, "1", "0", "0", "0", "1", "0", "0", "1", "1"));
        assertEquals("Hibas eredmeny", "Bella nyert",  doArgumentTest(A.class, "-1", "0", "1", "0", "1", "1", "0", "0", "1"));
        assertEquals("Hibas eredmeny", "Nincs nyertes",  doArgumentTest(A.class, "0", "1", "0", "0", "1", "0", "1", "0", "1"));
        assertEquals("Hibas eredmeny", "Nincs nyertes",  doArgumentTest(A.class, "1", "1", "0", "0", "0", "1", "1", "1", "0"));
        assertEquals("Hibas eredmeny", "Nincs nyertes",  doArgumentTest(A.class, "0", "0", "1", "1", "0", "0", "0", "1", "1"));
        assertEquals("Hibas eredmeny", "Nincs nyertes",  doArgumentTest(A.class, "0", "1", "0", "0", "1", "1", "1", "0", "0"));
        assertEquals("Hibas eredmeny", "Bella nyert",  doArgumentTest(A.class, "1", "1", "0", "1", "0", "0", "1", "0", "1"));
        assertEquals("Hibas eredmeny", "Bella nyert",  doArgumentTest(A.class, "0", "1", "-1", "0", "1", "0", "-1", "1", "-1"));
        assertEquals("Hibas eredmeny", "Attila nyert",  doArgumentTest(A.class, "0", "0", "0", "-1", "1", "1", "1", "0", "1"));
        assertEquals("Hibas eredmeny", "Bella nyert",  doArgumentTest(A.class, "-1", "0", "0", "1", "1", "0", "1", "1", "1"));;
    }

    @Test
    public void fajlMeret() {
        checkFileSize("src/A.java", 250, 1, 2);
    }
}