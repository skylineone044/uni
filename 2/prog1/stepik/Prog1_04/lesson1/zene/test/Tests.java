import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Tests extends BaseTest {
  @Test
  public void testSolution() {
    Zene zene = new Zene();
    setFieldValue(Zene.class, "eloado", zene, "en");
    setFieldValue(Zene.class, "cim", zene, "EN");
    setFieldValue(Zene.class, "hossz", zene, 40);

    assertEquals("eloado gettere nem jo", "en", zene.getEloado());
    assertEquals("cim gettere nem jo", "EN", zene.getCim());
    assertEquals("hossz gettere nem jo", 40, zene.getHossz());
  }
}