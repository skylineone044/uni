public class ButorGyar {

    public Butor[] butorGyartas() {
        Butor[] butorok = new Butor[8];
        butorok[0] = new Asztal();
        butorok[1] = new Asztal();
        butorok[2] = new Szek();
        butorok[3] = new Szek();
        butorok[4] = new Szekreny();
        butorok[5] = new Szekreny();
        butorok[6] = new Komod();
        butorok[7] = new Komod();
        return butorok;
    }

}
