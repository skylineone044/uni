import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class Tests {
  @SuppressWarnings("ConstantConditions")
  @Test
  public void testSolution() {
    ButorGyar gyar = new ButorGyar();
    Butor[] butorok = gyar.butorGyartas();

    Asztal a = new Asztal();
    Komod k = new Komod();
    Szekreny sz = new Szekreny();
    Szek s = new Szek();

    assertTrue("oroklodes hibas", a instanceof Butor);
    assertTrue("oroklodes hibas", k instanceof Butor);
    assertTrue("oroklodes hibas", sz instanceof Butor);
    assertTrue("oroklodes hibas", s instanceof Butor);

    assertEquals("a tomb merete nem megfelelo", 8, butorok.length);
    assertSame(butorok[0].getClass(), Asztal.class);
    assertSame(butorok[1].getClass(), Asztal.class);
    assertSame(butorok[2].getClass(), Szek.class);
    assertSame(butorok[3].getClass(), Szek.class);
    assertSame(butorok[4].getClass(), Szekreny.class);
    assertSame(butorok[5].getClass(), Szekreny.class);
    assertSame(butorok[6].getClass(), Komod.class);
    assertSame(butorok[7].getClass(), Komod.class);


  }
}