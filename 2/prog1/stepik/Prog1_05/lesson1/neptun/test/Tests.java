import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Tests {
  @Test
  public void testSolution() {
    TanulmanyiRendszer t = new TanulmanyiRendszer();
    assertEquals("tanulmanyi rendszer hibas", "a tanulmanyi rendszer elindult!", t.elindit(9500));
    assertEquals("tanulmanyi rendszer hibas", "a tanulmanyi rendszer elindult!", t.elindit(10000));
    assertEquals("tanulmanyi rendszer hibas", "tul sok felhasznalo!", t.elindit(10001));

    Neptun n = new Neptun();
    assertEquals("tanulmanyi rendszer hibas", "Neptun elinditva!", n.elindit(1));
    assertEquals("tanulmanyi rendszer hibas", "Neptun elinditva!", n.elindit(10000));
    assertEquals("tanulmanyi rendszer hibas", "Neptun elinditva!", n.elindit(15000));
    assertEquals("tanulmanyi rendszer hibas", "Neptun elinditva!", n.elindit(135211));
    assertEquals("tanulmanyi rendszer hibas", "service is unavailable", n.elindit(135212));
    assertEquals("tanulmanyi rendszer hibas", "service is unavailable", n.elindit(1000001));
  }
}