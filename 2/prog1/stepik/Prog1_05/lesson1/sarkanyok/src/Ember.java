import java.util.Arrays;

public class Ember {
    private final String nev;
    private Sarkany[] sarkanyok;

    public Ember(String nev, int maxSarkanyok) {
        this.nev = nev;
        this.sarkanyok = new Sarkany[maxSarkanyok];
    }
    public Ember(int maxSarkanyok) {
        this.nev = "Noemi neni";
        this.sarkanyok = new Sarkany[maxSarkanyok];
    }
    public void sarkanySzuletik(Sarkany s) {
        int legGyangebb_id = 0;
        for (int i = 0; i < sarkanyok.length; i++) {
            if (sarkanyok[i] == null) {
                sarkanyok[i] = s;
                return;
            }
            if (sarkanyok[i].ero < sarkanyok[legGyangebb_id].ero) {
                legGyangebb_id = i;
            }
        }
        sarkanyok[legGyangebb_id] = s;
    }
    public double osszErosseg() {
        double sum = 0.0;
        for (Sarkany sa : this.sarkanyok) {
            sum += sa.ero;
        }
        return sum;
    }
    public String leggyakrabbi() {
        int zold = 0;
        int kek = 0;
        int voros = 0;

        for (Sarkany sa : sarkanyok) {
            if ("zold".equals(sa.szin)) {
                zold++;
            }
            if ("kek".equals(sa.szin)) {
                kek++;
            }
            if ("voros".equals(sa.szin)) {
                voros++;
            }
        }
        if (zold > kek && zold > voros) {
            return "zold";
        }
        if (kek > zold && kek > voros) {
            return "kek";
        }
        if (voros > kek && voros > zold) {
            return "voros";
        }
        return "zold";
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder("" + this.nev + "\n");
        for (Sarkany sa: sarkanyok) {
            res.append(sa.toString()).append("\n");
        }
        return res.toString();
    }
}