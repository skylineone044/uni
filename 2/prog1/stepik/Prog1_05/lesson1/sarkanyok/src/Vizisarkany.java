public class Vizisarkany extends Sarkany {
    private int vizkopoKepesseg;
    private double vizbenLet;

    public int getVizkopoKepesseg() {
        return vizkopoKepesseg;
    }

    public double getVizbenLet() {
        return vizbenLet;
    }

    public Vizisarkany(String nev, String szin, double ero, int vizkopoKepesseg, double vizbenLet) {
        super(nev, szin, ero, false);
        this.vizkopoKepesseg = vizkopoKepesseg;
        this.vizbenLet = vizbenLet;
    }
    public Vizisarkany(String nev, double ero, int vizkopoKepesseg) {
        super(nev, nev, ero, false);
        this.vizbenLet = 1;
        if (nev.contains("zold")) {
            this.szin = "zold";
        } else if (nev.contains("kek")){
            this.szin = "kek";
        } else {
            this.szin = "voros";
        }
        this.vizkopoKepesseg = vizkopoKepesseg;
    }
    public boolean uszik(int idotartam) {
        return idotartam <= this.vizbenLet * 60;
    }

    public double vizetKop() {
        return this.vizkopoKepesseg * ero;
    }

    @Override
    public String toString() {
        return super.toString() + " A vizisarkany vizkopo kepessege " +
                (this.vizkopoKepesseg < 10 ? "alacsony" : "magas") + ", "
                + this.vizbenLet + " orat kepes vizben tolteni.";
    }
}