public class Bolt {

    public static boolean vasarlas(int ora, int perc) {
        int ido = ora * 60 + perc;
        return 9 * 60 <= ido && ido <= 12 * 60;
    }

}