import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Tests {
  @Test
  public void testSolution() {
    assertTrue("hibas eredmeny", Bolt.vasarlas(9, 0));
    assertTrue("hibas eredmeny", Bolt.vasarlas(9, 4));
    assertTrue("hibas eredmeny", Bolt.vasarlas(9, 59));
    assertTrue("hibas eredmeny", Bolt.vasarlas(10, 0));
    assertTrue("hibas eredmeny", Bolt.vasarlas(10, 59));
    assertTrue("hibas eredmeny", Bolt.vasarlas(11, 0));
    assertTrue("hibas eredmeny", Bolt.vasarlas(11, 59));
    assertTrue("hibas eredmeny", Bolt.vasarlas(12, 0));

    assertFalse("hibas eredmeny", Bolt.vasarlas(8, 59));
    assertFalse("hibas eredmeny", Bolt.vasarlas(2, 31));
    assertFalse("hibas eredmeny", Bolt.vasarlas(0, 0));
    assertFalse("hibas eredmeny", Bolt.vasarlas(23, 59));
    assertFalse("hibas eredmeny", Bolt.vasarlas(12, 1));
    assertFalse("hibas eredmeny", Bolt.vasarlas(12, 10));
    assertFalse("hibas eredmeny", Bolt.vasarlas(8, 1));
  }
}