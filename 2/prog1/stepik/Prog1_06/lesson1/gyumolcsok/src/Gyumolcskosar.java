import java.util.Arrays;

public class Gyumolcskosar {
    private Gyumolcs[] gyumolcsok;

    public Gyumolcskosar(int meret) {
        gyumolcsok = new Gyumolcs[meret];
        Arrays.fill(gyumolcsok, null);
    }

    public Gyumolcs[] getGyumolcsok() {
        return gyumolcsok;
    }
    public int gyumolcsSzam() {
        for (int i = 0; i < this.gyumolcsok.length; i++) {
            if (this.gyumolcsok[i] == null) {
                return i;
            }
        }
        return this.gyumolcsok.length;
    }
    public boolean gyumolcsotHozzaad(Gyumolcs gyumolcs) {
        for (Gyumolcs gy : this.gyumolcsok) {
            if (gy != null && gy.getClass().equals(gyumolcs.getClass())) {
                return false;
            }
        }
        for (int i = 0; i < this.gyumolcsok.length; i++) {
            if (this.gyumolcsok[i] == null) {
                this.gyumolcsok[i] = gyumolcs;
                return true;
            }
        }
        return false;
    }
}