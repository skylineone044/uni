import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;

public class Tests extends BaseTest {
  @Test
  public void testSolution() {
    assertTrue("HatalmiJelkep hibas", testClass(HatalmiJelkep.class, Modifier.PUBLIC | Modifier.ABSTRACT, Object.class));
    assertTrue("Jogar hibas", testClass(Jogar.class, Modifier.PUBLIC, HatalmiJelkep.class));
    assertTrue("Korona hibas", testClass(Korona.class, Modifier.PUBLIC, HatalmiJelkep.class));
    assertTrue("Orszagalma hibas", testClass(Orszagalma.class, Modifier.PUBLIC, HatalmiJelkep.class));
  }
}