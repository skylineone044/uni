public interface Karakter {
    boolean tamad(Karakter karakter);
    boolean tamad(Epulet epulet);
    void tamad(KornyezetiElem kornyezetiElem);
    boolean epites(Epulet epulet);
    void sebzestElszenved(int sebzes);
    void fejlodik();
    int gyogyit(Karakter karakter, int X);
    int pusztitas(Karakter[] karakters);
    Karakter[] ellensegek();

}