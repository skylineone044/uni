import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.*;
import java.util.Arrays;
import java.util.List;

public class BaseTest {

    @SuppressWarnings("SameParameterValue")
    protected boolean testField(Class<?> c, String name, Class<?> type, Integer modifier) {
        try {
            Field res = c.getDeclaredField(name);

            if (res.getType() != type) {
                return false;
            }

            return modifier == null || res.getModifiers() == modifier;
        } catch (NoSuchFieldException ignored) {}

        return false;
    }

    protected boolean testMethod(Class<?> c, String name, Class<?> returnType, Integer modifier, Class<?>... params) {
        try {
            Method res = c.getDeclaredMethod(name, params);

            if (res.getReturnType() != returnType) {
                return false;
            }

            return modifier == null || res.getModifiers() == modifier;
        } catch (NoSuchMethodException ignored) {}

        return false;
    }

    protected boolean testConstructor(Class<?> c, Integer modifier, Class<?>... params) {
        try {
            Constructor<?> t = c.getConstructor(params);

            if (t == null) {
                return false;
            }

            return t.getModifiers() == modifier;
        } catch (Exception ignored) {}

        return false;
    }

    protected boolean testClass(Class<?> c, Integer modifier, Class<?> superclass, Class<?>... interfaces) {
        if (c.getModifiers() != modifier) {
            return false;
        }

        if (c.getSuperclass() != superclass) {
            return false;
        }

        List<Class<?>> implemented = Arrays.asList(c.getInterfaces());
        if (implemented.size() != interfaces.length) {
            return false;
        }

        outer: for (Class<?> cc: interfaces) {
            for (Class<?> ccc: implemented) {
                if (ccc == cc) {
                    continue outer;
                }
            }

            return false;
        }

        return true;
    }

    protected boolean testInterface(Class<?> c, Integer modifier) {
        if (!c.isInterface()) {
            return false;
        }

        return modifier == c.getModifiers();
    }

    protected Object fieldValue(Class<?> c, String name, Object o) {
        try {
            Field f = c.getDeclaredField(name);
            f.setAccessible(true);
            return f.get(o);
        } catch (NoSuchFieldException | IllegalAccessException ignored) {}

        return null;
    }

    protected void setFieldValue(Class<?> c, String name, Object o, Object value) {
        try {
            Field f = c.getDeclaredField(name);
            f.setAccessible(true);
            f.set(o, value);
        } catch (Exception ignored) {}
    }

    protected String doArgumentTest(Class<?> cl, String... args) {
        PrintStream old = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(baos));
        try {
            Method m = cl.getDeclaredMethod("main", String[].class);
            m.invoke(null, (Object) args);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        System.setOut(old);
        return baos.toString().trim();
    }

    protected Method getMethod(Class<?> c, String name, Class<?>... params) {
        try {
            return c.getDeclaredMethod(name, params);
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    protected String getSout(Method m, Object o, Object... params) {
        PrintStream old = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(baos));

        try {
            m.invoke(o, params);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        System.setOut(old);
        return baos.toString().trim();
    }

    protected int fieldCount(Class<?> c) {
        return c.getDeclaredFields().length;
    }

    protected int methodCount(Class<?> c) {
        return c.getDeclaredMethods().length;
    }

    protected int constructorCount(Class<?> c) {
        return c.getConstructors().length;
    }

}
