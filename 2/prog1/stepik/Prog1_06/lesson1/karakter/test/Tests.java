import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;

public class Tests extends BaseTest {
  @Test
  public void testSolution() {
    assertTrue("interface nem jo", testInterface(Karakter.class, Modifier.PUBLIC | Modifier.INTERFACE | Modifier.ABSTRACT));
    assertTrue("tamad (karakter) hibas", testMethod(Karakter.class, "tamad", boolean.class, Modifier.PUBLIC | Modifier.ABSTRACT, Karakter.class));
    assertTrue("tamad (epulet) hibas", testMethod(Karakter.class, "tamad", boolean.class, Modifier.PUBLIC | Modifier.ABSTRACT, Epulet.class));
    assertTrue("tamad (kornyezeti elem) hibas", testMethod(Karakter.class, "tamad", void.class, Modifier.PUBLIC | Modifier.ABSTRACT, KornyezetiElem.class));
    assertTrue("epites hibas", testMethod(Karakter.class, "epites", boolean.class, Modifier.PUBLIC | Modifier.ABSTRACT, Epulet.class));
    assertTrue("sebzestElszenved hibas", testMethod(Karakter.class, "sebzestElszenved", void.class, Modifier.PUBLIC | Modifier.ABSTRACT, int.class));
    assertTrue("fejlodik hibas", testMethod(Karakter.class, "fejlodik", void.class, Modifier.PUBLIC | Modifier.ABSTRACT));
    assertTrue("gyogyit hibas", testMethod(Karakter.class, "gyogyit", int.class, Modifier.PUBLIC | Modifier.ABSTRACT, Karakter.class, int.class));
    assertTrue("pusztitas hibas", testMethod(Karakter.class, "pusztitas", int.class, Modifier.PUBLIC | Modifier.ABSTRACT, Karakter[].class));
    assertTrue("ellensegek hibas", testMethod(Karakter.class, "ellensegek", Karakter[].class, Modifier.PUBLIC | Modifier.ABSTRACT));
  }
}