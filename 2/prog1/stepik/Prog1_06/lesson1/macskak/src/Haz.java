public class Haz {
    private Allat[] allatok;

    public Haz(int maxAllatszam) {
        allatok = new Allat[maxAllatszam];
    }

    public Allat[] getAllatok() {
        return allatok;
    }

    public void tevekenyseg() {
        for (Allat a : this.allatok) {
            if(a instanceof Macska) {
                ((Macska) a).nyavog();
            }
            if (a instanceof Tehen) {
                ((Tehen) a).tejetAd();
            }
            if (a instanceof Lo) {
                ((Lo) a).nyerit();
            }
        }
    }

    public void allatJon(Allat allat) {
        if (allat == null) {
            return;
        }
        boolean vanHely = false;
        for (Allat a : this.allatok) {
            if (a == null) {
                vanHely = true;
                break;
            }
            if (allat.equals(a)) {
                vanHely = false;
                break;
            }
        }
        if (vanHely) {
            for (int i = 0; i < this.allatok.length; i++) {
                if (this.allatok[i] == null) {
                    this.allatok[i] = allat;
                    return;
                }
            }
        }
    }

}
