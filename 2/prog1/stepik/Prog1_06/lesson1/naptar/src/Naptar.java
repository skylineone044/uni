import java.time.DayOfWeek;
import java.time.LocalDate;

public class Naptar {
    public boolean hetvege(int ev, int honap, int nap) {
        LocalDate date = LocalDate.of(ev, honap, nap);
        DayOfWeek dayOfWeek = DayOfWeek.from(date);
        if (dayOfWeek.getValue() == 6 || dayOfWeek.getValue() == 7) {
            return true;
        }
        return false;
    }
}