import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Tests {
  @Test
  public void testSolution() {
    Naptar n = new Naptar();
    assertTrue("hibas megoldas", n.hetvege(2020, 3, 21));
    assertTrue("hibas megoldas", n.hetvege(2020, 3, 22));
    assertFalse("hibas megoldas", n.hetvege(2020, 3, 23));

    assertFalse("hibas megoldas", n.hetvege(2021, 12, 31));
    assertTrue("hibas megoldas", n.hetvege(2022, 1, 1));
    assertTrue("hibas megoldas", n.hetvege(2022, 1, 2));

    assertTrue("hibas megoldas", n.hetvege(2119, 11, 25));
    assertFalse("hibas megoldas", n.hetvege(2119, 11, 24));
    assertFalse("hibas megoldas", n.hetvege(2119, 11, 27));

    assertFalse("hibas megoldas", n.hetvege(1848, 3, 15));
    assertTrue("hibas megoldas", n.hetvege(1848, 3, 18));
    assertTrue("hibas megoldas", n.hetvege(1848, 3, 19));

    assertFalse("hibas megoldas", n.hetvege(1904, 2, 29));
    assertTrue("hibas megoldas", n.hetvege(1908, 2, 29));
  }
}