import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Array;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.stream.Stream;

import static org.junit.Assert.*;

public class Tests extends BaseTest {

  private Stream<Object> flatten(Object[] array) {
    return Arrays.stream(array).flatMap(o -> o instanceof Object[] ? flatten((Object[])o): Stream.of(o));
  }

  private boolean doOneDimensionTest(int size) {
    Object[] res = Sakktabla.sakktabla(1, size);

    for (int i = 0; i < res.length; i++) {
      if (i % 2 == 0 && (int) res[i] != 0) {
        return false;
      } else if (i % 2 == 1 && (int) res[i] != 1) {
        return false;
      }
    }

    return true;
  }

  private boolean doHigherDimensionTest(int dimension, int size) {
    Object[] arr = Sakktabla.sakktabla(dimension, size);
    Object[] array = flatten(arr).toArray();

    int resdim = 2;
    while (arr[0] instanceof Object[]) {
      arr = (Object[]) arr[0];
      resdim++;
    }

    if (resdim != dimension) {
      fail("a visszaadott tomb dimenzioszama nem megfelelo");
    }

    if (((int[]) array[0])[0] != 0) {
      return false;
    }

    if (((int[]) array[0]).length != size) {
      return false;
    }

    int visszanezes = 1;
    int hatvany = 1;
    for (int i = 0; i < array.length; i++) {
      int[] res = (int[]) array[i];

      for (int j = 1; j < res.length; j++) {
        if (!(res[j-1] == 0 ? res[j] == 1 : res[j] == 0)) {
          return false;
        }
      }

      if (i == 0) {
        continue;
      }

      if (i == Math.pow(size, hatvany)) {
        visszanezes *= size;
        hatvany++;
      }

      int[] old = (int[]) array[i - visszanezes];

      for (int j = 0; j < old.length; j++) {
        if (old[j] == res[j]) {
          return false;
        }
      }
    }

    return true;
  }

  @Test
  public void metodusFejlecTeszt() {
    assertTrue("A metodus fejlece nem megfelelo", testMethod(Sakktabla.class, "sakktabla", Object[].class, Modifier.PUBLIC | Modifier.STATIC, int.class, int.class));
  }

  @Test
  public void egydimenzioTeszt() {
    assertTrue("Egy dimenzios tomb hibas", doOneDimensionTest(1));
    assertTrue("Egy dimenzios tomb hibas", doOneDimensionTest(2));
    assertTrue("Egy dimenzios tomb hibas", doOneDimensionTest(5));
    assertTrue("Egy dimenzios tomb hibas", doOneDimensionTest(6));
    assertTrue("Egy dimenzios tomb hibas", doOneDimensionTest(15040));
  }

  @Test
  public void ketdimenzioTeszt() {
    assertTrue("Ket dimenzios tomb hibas", doHigherDimensionTest(2, 1));
    assertTrue("Ket dimenzios tomb hibas", doHigherDimensionTest(2, 2));
    assertTrue("Ket dimenzios tomb hibas", doHigherDimensionTest(2, 3));
    assertTrue("Ket dimenzios tomb hibas", doHigherDimensionTest(2, 420));
    assertTrue("Ket dimenzios tomb hibas", doHigherDimensionTest(2, 1051));
  }

  @Test
  public void haromdimenzioTeszt() {
    assertTrue("Harom dimenzios tomb hibas", doHigherDimensionTest(3, 1));
    assertTrue("Harom dimenzios tomb hibas", doHigherDimensionTest(3, 2));
    assertTrue("Harom dimenzios tomb hibas", doHigherDimensionTest(3, 3));
    assertTrue("Harom dimenzios tomb hibas", doHigherDimensionTest(3, 4));
    assertTrue("Harom dimenzios tomb hibas", doHigherDimensionTest(3, 40));
    assertTrue("Harom dimenzios tomb hibas", doHigherDimensionTest(3, 61));
  }

  @Test
  public void magasdimenzioTeszt() {
    assertTrue("Magas dimenzios tomb hibas (1 meretu)", doHigherDimensionTest(4, 1));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(4, 2));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(4, 3));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(4, 4));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(4, 5));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(4, 11));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(4, 16));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(4, 30));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(4, 35));

    assertTrue("Magas dimenzios tomb hibas (1 meretu)", doHigherDimensionTest(5, 1));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(5, 3));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(5, 8));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(5, 11));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(5, 14));

    assertTrue("Magas dimenzios tomb hibas (1 meretu)", doHigherDimensionTest(6, 1));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(6, 2));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(6, 3));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(6, 7));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(6, 8));

    assertTrue("Magas dimenzios tomb hibas (1 meretu)", doHigherDimensionTest(7, 1));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(7, 2));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(7, 3));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(7, 8));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(7, 9));

    assertTrue("Magas dimenzios tomb hibas (1 meretu)", doHigherDimensionTest(8, 1));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(8, 2));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(8, 3));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(8, 7));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(8, 8));

    assertTrue("Magas dimenzios tomb hibas (1 meretu)", doHigherDimensionTest(9, 1));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(9, 2));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(9, 3));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(9, 4));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(9, 5));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(9, 6));

    assertTrue("Magas dimenzios tomb hibas (1 meretu)", doHigherDimensionTest(10, 1));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(10, 2));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(10, 3));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(10, 4));

    assertTrue("Magas dimenzios tomb hibas (1 meretu)", doHigherDimensionTest(11, 1));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(11, 2));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(11, 3));

    assertTrue("Magas dimenzios tomb hibas (1 meretu)", doHigherDimensionTest(12, 1));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(12, 2));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(12, 3));

    assertTrue("Magas dimenzios tomb hibas (1 meretu)", doHigherDimensionTest(13, 1));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(13, 2));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(13, 3));

    assertTrue("Magas dimenzios tomb hibas (1 meretu)", doHigherDimensionTest(14, 1));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(14, 2));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(14, 3));

    assertTrue("Magas dimenzios tomb hibas (1 meretu)", doHigherDimensionTest(15, 1));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(15, 2));

    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(18, 2));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(19, 2));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(21, 2));
    assertTrue("Magas dimenzios tomb hibas", doHigherDimensionTest(22, 2));

    assertTrue("Magas dimenzios tomb hibas (1 meretu)", doHigherDimensionTest(18, 1));
    assertTrue("Magas dimenzios tomb hibas (1 meretu)", doHigherDimensionTest(19, 1));
    assertTrue("Magas dimenzios tomb hibas (1 meretu)", doHigherDimensionTest(21, 1));
    assertTrue("Magas dimenzios tomb hibas (1 meretu)", doHigherDimensionTest(22, 1));
    assertTrue("Magas dimenzios tomb hibas (1 meretu)", doHigherDimensionTest(58, 1));
    assertTrue("Magas dimenzios tomb hibas (1 meretu)", doHigherDimensionTest(77, 1));
    assertTrue("Magas dimenzios tomb hibas (1 meretu)", doHigherDimensionTest(158, 1));
  }


}