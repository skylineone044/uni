import org.junit.After;
import org.junit.Before;

import java.io.*;
import java.lang.reflect.*;
import java.nio.file.Files;
import java.util.*;

import static org.junit.Assert.*;

public class BaseTest {

    protected ByteArrayOutputStream sout = new ByteArrayOutputStream();
    protected ByteArrayOutputStream serr = new ByteArrayOutputStream();

    protected PrintStream oldOut = System.out;
    protected PrintStream oldErr = System.err;

    @Before
    public void saveOut() {
        System.setOut(new PrintStream(sout));
        System.setErr(new PrintStream(serr));
    }

    @After
    public void restoreOut() {
        System.setOut(oldOut);
        System.setErr(oldErr);
        sout.reset();
        serr.reset();
    }

    @SuppressWarnings("SameParameterValue")
    protected boolean testField(Class<?> c, String name, Class<?> type, Integer modifier) {
        try {
            Field res = c.getDeclaredField(name);

            if (res.getType() != type) {
                return false;
            }

            return modifier == null || res.getModifiers() == modifier;
        } catch (NoSuchFieldException ignored) {}

        return false;
    }

    protected boolean testMethod(Class<?> c, String name, Class<?> returnType, Integer modifier, Class<?>... params) {
        try {
            Method res = c.getDeclaredMethod(name, params);

            if (res.getReturnType() != returnType) {
                return false;
            }

            return modifier == null || res.getModifiers() == modifier;
        } catch (NoSuchMethodException ignored) {}

        return false;
    }

    protected boolean testMethodInherited(Class<?> c, String name, Class<?> returnType, Integer modifier, Class<?>... params) {
        try {
            Method res = c.getMethod(name, params);

            if (res.getReturnType() != returnType) {
                return false;
            }

            return modifier == null || res.getModifiers() == modifier;
        } catch (Exception ignored) {}

        return false;
    }

    protected boolean testConstructor(Class<?> c, Integer modifier, Class<?>... params) {
        try {
            Constructor<?> t = c.getDeclaredConstructor(params);

            if (t == null) {
                return false;
            }

            return t.getModifiers() == modifier;
        } catch (Exception ignored) {}

        return false;
    }

    protected boolean testClass(Class<?> c, Integer modifier, Class<?> superclass, Class<?>... interfaces) {
        if (c.getModifiers() != modifier) {
            return false;
        }

        if (c.getSuperclass() != superclass) {
            return false;
        }

        List<Class<?>> implemented = Arrays.asList(c.getInterfaces());
        if (implemented.size() != interfaces.length) {
            return false;
        }

        outer: for (Class<?> cc: interfaces) {
            for (Class<?> ccc: implemented) {
                if (ccc == cc) {
                    continue outer;
                }
            }

            return false;
        }

        return true;
    }

    protected boolean testInterface(Class<?> c, Integer modifier) {
        if (!c.isInterface()) {
            return false;
        }

        return modifier == c.getModifiers();
    }

    protected Object fieldValue(Class<?> c, String name, Object o) {
        try {
            Field f = c.getDeclaredField(name);
            f.setAccessible(true);
            return f.get(o);
        } catch (NoSuchFieldException | IllegalAccessException ignored) {}

        return null;
    }

    protected void setFieldValue(Class<?> c, String name, Object o, Object value) {
        try {
            Field f = c.getDeclaredField(name);
            f.setAccessible(true);
            f.set(o, value);
        } catch (Exception ignored) {}
    }

    protected String doArgumentTest(Class<?> cl, String... args) {
        PrintStream old = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(baos));
        try {
            Method m = cl.getDeclaredMethod("main", String[].class);
            m.invoke(null, (Object) args);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        System.setOut(old);
        return baos.toString().trim();
    }

    protected Method getMethod(Class<?> c, String name, Class<?>... params) {
        try {
            return c.getDeclaredMethod(name, params);
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    protected String getSout(Method m, Object o, Object... params) {
        PrintStream old = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(baos));

        try {
            m.invoke(o, params);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        System.setOut(old);
        return baos.toString().trim();
    }

    protected int fieldCount(Class<?> c) {
        return c.getDeclaredFields().length;
    }

    protected int methodCount(Class<?> c) {
        return c.getDeclaredMethods().length;
    }

    protected int constructorCount(Class<?> c) {
        return c.getConstructors().length;
    }

    protected List<String> readFile(String filename) {
        try (Scanner sc = new Scanner(new File(filename))) {
            List<String> ret = new ArrayList<>();

            while (sc.hasNextLine()) {
                ret.add(sc.nextLine());
            }

            return ret;
        } catch (IOException e) {
            fail("Nem sikerult a fajlbol olvasni!");
        }

        return null;
    }

    protected void writeFile(String filename, List<String> content) {
        try (PrintWriter pw = new PrintWriter(filename)) {
            for (String s: content) {
                pw.println(s);
            }
        } catch (IOException e) {
            fail("Nem sikerult a fajlba irni!");
        }
    }

    protected void createNewDirectory(String name) {
        deleteDirectory(new File(name));
        //noinspection ResultOfMethodCallIgnored
        new File(name).mkdir();
    }

    protected void deleteDirectory(File dir) {
        File[] files = dir.listFiles();
        if (files != null) {
            for (File file : files) {
                deleteDirectory(file);
            }
        }

        //noinspection ResultOfMethodCallIgnored
        dir.delete();
    }

    protected void checkFileSize(String sourcePath, int maxSize, int sourceCount, int testCount) {
        File f = new File(sourcePath);

        try {
            long size = Files.size(f.toPath());
            assertTrue("A forraskod merete tul nagy (" + size + " bajt)!", size <= maxSize);
            oldOut.println(System.lineSeparator() + "[OK] A fajl merete: " + size + " bajt." + System.lineSeparator());
        } catch (IOException e) {
            e.printStackTrace();
            fail("Nem sikerult a fajl meretet lekerdezni!");
        }

        int sourceC = new File("./src").listFiles().length;
        int testC = new File("./test").listFiles().length;

        assertEquals("Nem engedelyezett uj fajlokat letrehozni!", sourceC, sourceCount);
        assertEquals("Nem engedelyezett uj fajlokat letrehozni!", testC, testCount);
    }

    public boolean objectEquals(Object a, Object b) {
        if (a.getClass() != b.getClass()) {
            return false;
        }

        Field[] fields = a.getClass().getDeclaredFields();

        for (Field f: fields) {
            f.setAccessible(true);
            try {
                if (!Objects.deepEquals(f.get(a), f.get(b))) {
                    return false;
                }
            } catch (IllegalAccessException e) {
                return false;
            }
        }

        return true;
    }

    public boolean listEquals(List<?> a, List<?> b, boolean referenceEquals) {
        if (a.size() != b.size()) {
            return false;
        }

        for (int i = 0; i < a.size() && i < b.size(); i++) {
            if (referenceEquals) {
                if (a.get(i) != b.get(i)) {
                    return false;
                }
            } else {
                if (!objectEquals(a.get(i), b.get(i))) {
                    return false;
                }
            }
        }

        return true;
    }

    public boolean setEquals(Set<?> a, Set<?> b, boolean referenceEquals) {
        if (a.size() != b.size()) {
            return false;
        }

        kulso: for (Object aa: a) {
            if (referenceEquals) {
                if (!b.contains(aa)) {
                    return false;
                }
            } else {
                for (Object bb: b) {
                    if (objectEquals(aa, bb)) {
                        continue kulso;
                    }
                }

                return false;
            }
        }

        return true;
    }

}
