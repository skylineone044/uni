import java.util.HashMap;
import java.util.Map;

public class Betuszamolo {
    public static Map<Character, Integer> szamolas(String s) {
        Map<Character, Integer> map = new HashMap<>();

        for (int i = 0; i < s.length(); i++) {
            char betu = s.charAt(i);
            Integer occourances = map.get(betu);
            if (occourances != null) {
                map.put(betu, occourances+1);
            } else {
                map.put(betu, 1);
            }
        }

        return map;
    }
}