import java.util.Collection;
import java.util.Collections;
import java.util.Map;

public class Borton {
    Map<Ember, Integer> rabok;

    public Borton(Map<Ember, Integer> rabok) {
        this.rabok = rabok;
    }

    public Ember valasztas() {
        int min =  Collections.min(this.rabok.values());
        for (Map.Entry<Ember, Integer> par : rabok.entrySet()) {
            if (par.getValue() == min) {
                return par.getKey();
            }
        }
        return null;
    }

}