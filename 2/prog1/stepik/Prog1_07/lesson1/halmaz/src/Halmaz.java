import java.util.HashSet;
import java.util.Set;

public class Halmaz {

    public static Set<Integer> feltolt(int... szamok) {
        Set<Integer> halmaz = new HashSet<Integer>();
        for (int szam : szamok) {
            halmaz.add(szam);
        }
        return halmaz;
    }

    public static Set<Integer> unio(Set<Integer> a, Set<Integer> b) {
        Set<Integer> c = new HashSet<Integer>();
        c.addAll(b);
        c.addAll(a);
        return c;
    }

    public static Set<Integer> metszet(Set<Integer> a, Set<Integer> b) {
        Set<Integer> c = new HashSet<Integer>();
        c.addAll(a);
        c.retainAll(b);
        return c;
    }

    public static Set<Integer> kulonbseg(Set<Integer> a, Set<Integer> b) {
        Set<Integer> c = new HashSet<Integer>();
        c.addAll(a);
        c.removeAll(b);
        return c;
    }

}