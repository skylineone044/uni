import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class Tests {
    @Test
    public void teszt() {
        Legnagyobb l = new Legnagyobb();

        Set<Integer> s1 = new HashSet<>(Arrays.asList(3, 5, 2, 8));
        Set<Integer> s2 = new HashSet<>(Arrays.asList(-4, -6, -1, -88, -53));
        Set<Integer> s3 = new HashSet<>(Arrays.asList(10, -4, -130, 52, 0));
        Set<Integer> s4 = new HashSet<>(Collections.singletonList(-4));
        Set<Integer> s5 = new HashSet<>(Collections.singletonList(1));

        assertEquals("Hibas eredmeny", 8, l.legnagyobb(s1));
        assertEquals("Hibas eredmeny", -1, l.legnagyobb(s2));
        assertEquals("Hibas eredmeny", 52, l.legnagyobb(s3));
        assertEquals("Hibas eredmeny", -4, l.legnagyobb(s4));
        assertEquals("Hibas eredmeny", 1, l.legnagyobb(s5));
    }
}