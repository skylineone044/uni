import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Paros {
    private List<Integer> szamok = new ArrayList<>();

    public void hozzaadas(int szam) {
        if (szam % 2 == 0) {
            this.szamok.add(szam);
        }
    }

    public void primszamokatTorol() {
        this.szamok.removeAll(Arrays.asList(2));
    }

    public List<Integer> getSzamok() {
        return new ArrayList<>(szamok);
    }
}