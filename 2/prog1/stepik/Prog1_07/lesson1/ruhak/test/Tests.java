import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class Tests {
  @Test
  public void testSolution() {
    Ruha r1 = new Ruha(5, "piros");
    Ruha r2 = new Ruha(7, "kek");
    Ruha r3 = new Ruha(7, "piros");
    Ruha r4 = new Ruha(12, "zold");
    Ruha r5 = new Ruha(15, "szines");
    Ruha r6 = new Ruha(17, "fekete");
    Ruha r7 = new Ruha(20, "furaszinu");
    Ruha r8 = new Ruha(22, "atlatszo");
    Ruha r9 = new Ruha(200000, "tukrozodo");
    Ruha r10 = new Ruha(40, "piros");

    RuhasSzekreny szekreny1 = new RuhasSzekreny(new ArrayList<>(Arrays.asList(r1, r2, r5, r6, r7)));
    RuhasSzekreny szekreny2 = new RuhasSzekreny(new ArrayList<>(Collections.singletonList(r2)));
    RuhasSzekreny szekreny3 = new RuhasSzekreny(new ArrayList<>(Arrays.asList(r1, r3, r10)));
    RuhasSzekreny szekreny4 = new RuhasSzekreny(new ArrayList<>(Arrays.asList(r4, r5, r6, r7, r8, r9)));

    szekreny1.pirosakatTorol();
    szekreny2.pirosakatTorol();
    szekreny3.pirosakatTorol();
    szekreny4.pirosakatTorol();

    assertEquals("hibas megoldas", Arrays.asList(r2, r5, r6, r7), szekreny1.getRuhak());
    assertEquals("hibas megoldas", Collections.singletonList(r2), szekreny2.getRuhak());
    assertEquals("hibas megoldas", Collections.emptyList(), szekreny3.getRuhak());
    assertEquals("hibas megoldas", Arrays.asList(r4, r5, r6, r7, r8, r9), szekreny4.getRuhak());
  }
}