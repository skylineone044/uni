import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Varos {
    List<Haz> hazak = new ArrayList<>();

    public void epites(Haz haz) {
        this.hazak.add(haz);
    }
    public void epites(Haz[] haztomb) {
        Collections.addAll(this.hazak, haztomb);
    }

    public void epites(List<Haz> hazList) {
        this.hazak.addAll(hazList);
    }

    public void epites(Varos varos) {
        this.hazak.addAll(varos.hazak);
    }

}