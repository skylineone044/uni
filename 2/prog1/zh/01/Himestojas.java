public class Himestojas extends Tojas {
    private String minta;
    private int szinekSzama;

    public String getMinta() {
        return minta;
    }

    public int getSzinekSzama() {
        return szinekSzama;
    }

    public void setMinta(String minta) {
        if (minta.equals(this.minta)) {
            System.err.println("hat igy nem valtozna semmi");
        } else {
            this.minta = minta;
        }
    }

    public Himestojas(String meret, double tomeg, String minta, int szinekSzama) {
        super(meret, tomeg, false);
        this.setMinta(minta);
        this.szinekSzama = Math.max(1, szinekSzama);
    }

    @Override
    public String toString() {
        String szinek = "";
        if (this.szinekSzama == 1) {
            szinek = "egy";
        } else if (1 < this.szinekSzama && this.szinekSzama < 5) {
            szinek = "nehany";
        } else if (5 <= this.szinekSzama) {
            szinek = "sok";
        }
        return "A himestojas merete " + this.getMeret() + ", tomege " + this.getTomeg() + ", es " + this.getMinta()
                + " mintajura van " + szinek + " szinnel kifestve.";
    }

    @Override
    public void elromlik() {
        if (1 < this.szinekSzama) {
            this.szinekSzama = 1;
            this.minta = "maszatos";
        } else {
            this.minta = "";
        }
    }

    public boolean kifest(int felhasznalandoSzinek) {
        if ("maszatos".equals(this.minta)) {
            return false;
        }
        if (this.szinekSzama + felhasznalandoSzinek < 10) {
            this.szinekSzama += felhasznalandoSzinek;
            return true;
        }
        if (10 <= this.szinekSzama + felhasznalandoSzinek) {
            this.elromlik();
            return false;
        }
        return false;
    }
}
