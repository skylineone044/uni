import java.util.Arrays;

public class HusvetiNyuszi {
    private final String nev;
    private String szin;
    private Himestojas[] himestojasok;

    public HusvetiNyuszi(String nev, String szin, int tojasokSzama) {
        this.nev = nev;
        this.szin = szin;
        this.himestojasok = new Himestojas[tojasokSzama];
        Arrays.fill(himestojasok, null);
    }

    public boolean tojastHozzaad(Himestojas himestojas) {
        for (int i = 0; i < this.himestojasok.length; i++) {
            if (this.himestojasok[i] == null) {
                this.himestojasok[i] = himestojas;
                return true;
            }
        }
        return false;
    }

    public Himestojas ugrik() {
        if (himestojasok[0] != null) {
            himestojasok[0].elromlik();
            return himestojasok[0];
        } else {
            return null;
        }
    }

    public String legszinesebb() {
        if (himestojasok.length <= 0) {
            return "nincs ilyen";
        }
        int legtobbszin = 0;
        int id = 0;
        for (int i = 0; i < himestojasok.length; i++) {
            if (himestojasok[i] != null) {
                if (himestojasok[i].getSzinekSzama() > legtobbszin) {
                    legtobbszin = himestojasok[i].getSzinekSzama();
                    id = i;
                }
            }
        }
        for (int i = 0; i < himestojasok.length; i++) {
            if (himestojasok[i] != null) {
                if (legtobbszin == himestojasok[i].getSzinekSzama() && id != i) {
                    return "nincs ilyen";
                }
            }
        }
        return himestojasok[id].getMinta();
    }

    public int kidiszit(int szinek) {
        int szamlalo = 0;
        for (int i = 0; i < himestojasok.length; i++) {
            if (himestojasok[i] != null) {
                if (himestojasok[i].kifest(szinek)) {
                    szamlalo++;
                }

            }
        }
        return szamlalo;
    }
}
