public class Tojas {
    protected String meret;
    protected double tomeg; // nemnegativ, ha negatived akarunk allitani akk az abs ert kell
    protected boolean zap;

    public String getMeret() {
        return meret;
    }

    public double getTomeg() {
        return tomeg;
    }

    public boolean isZap() {
        return zap;
    }

    public Tojas() {
        this.meret = "M";
        this.tomeg = 50;
        this.zap = false;
    }

    public Tojas(String meret, double tomeg, boolean zap) {
        this.meret = meret;
        this.tomeg = Math.abs(tomeg);
        this.zap = zap;

        if ("S".equals(this.meret) || "M".equals(this.meret) || "L".equals(this.meret) || "XL".equals(this.meret)) {
            // all good
        } else {
            this.meret = "M";
        }
    }

    @Override
    public String toString() {
        if (this.zap) {
            return "A tojas merete " + this.meret + ", tomege " + this.tomeg + ", es zap.";
        } else {
            return "A tojas merete " + this.meret + ", tomege " + this.tomeg + ", es meg friss.";
        }
    }

    public boolean fogyaszthato() {
        return ((!this.zap) && this.tomeg > 30);
    }

    public void elromlik() {
        this.zap = true;
        this.tomeg -= 10;
        this.tomeg = Math.max(5, this.tomeg);
    }

}
