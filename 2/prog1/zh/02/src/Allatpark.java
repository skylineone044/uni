import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

public class Allatpark {
    public Map<String, Macskafele> macskafelek;

    public Allatpark() {
        this.macskafelek = new HashMap<>();
    }

    public boolean macskafeletHozzaad(String nev, Macskafele macskafele) {
        if (macskafelek.containsKey(nev) || macskafele.getAlmossag() == 10) {
            return false;
        } else {
            macskafelek.put(nev, macskafele);
            return true;
        }
    }

    public int etetes(String szoveg) {
        int szamlalo = 0;

        for (Map.Entry<String, Macskafele> macskafele : macskafelek.entrySet()) {
            try {
                macskafele.getValue().eszik(szoveg);
                szamlalo++;
            } catch (KarmolasException e) {
                // nothing to do
            }
        }
        return szamlalo;
    }

    public List<Hazimacska> cicaKereses(String keresettMinta) {
        List<Hazimacska> list = new ArrayList<>();

        if (keresettMinta == null) {
            return list;
        }

        for (Map.Entry<String, Macskafele> macskafele : macskafelek.entrySet()) {
            if (macskafele.getValue().getMintazat().equals(keresettMinta) && macskafele.getValue() instanceof Hazimacska) {
                list.add((Hazimacska) macskafele.getValue());
            }
        }
        return list;
    }

    public double gyorsasag(String nev) {
        Macskafele macskafele = this.macskafelek.get(nev);

        if (macskafele == null) {
            return -1;
        } else {
            if (macskafele instanceof Tigris) {
                return ((Tigris) macskafele).getGyorsasag();
            } else {
                return 5;
            }
        }
    }

    public boolean befogadas(String fileNev) {
        try (Scanner sc = new Scanner(new File(fileNev))) {
            while (sc.hasNextLine()) {
                String[] line = sc.nextLine().split(";");

                Hazimacska macskafele = new Hazimacska(line[1], Integer.parseInt(line[2]), Double.parseDouble(line[3]));
                this.macskafelek.put(line[0], macskafele);
            }
            return true;
        } catch (FileNotFoundException e) {
            // do nothing
            return false;
        }
    }

    public void jelentes(String fileNEv) {
        try (PrintWriter pw = new PrintWriter(fileNEv)) {
            for (Map.Entry<String, Macskafele> macskafele : macskafelek.entrySet()) {
                if (macskafele.getValue() instanceof Tigris) {
                    pw.println(macskafele.getKey() + " tigris");
                } else {
                    pw.println(macskafele.getKey() + " hazimacska");
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
