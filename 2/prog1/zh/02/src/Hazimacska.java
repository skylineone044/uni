public final class Hazimacska extends Macskafele {
    public Hazimacska(String mintazat, int ehseg, double almossag) {
        super(mintazat, ehseg, almossag);
    }

    @Override
    public void eszik(String szoveg) throws KarmolasException {
        if (szoveg.contains("romlott")) {
            throw new KarmolasException(szoveg.length());
        } else {
            setEhseg(getEhseg() - 3);
        }
    }

    public void nyavog(String szoveg) {
        String[] szavak = szoveg.split(" ");
        for (String szo : szavak) {
            System.out.println(szo);
        }
    }
}
