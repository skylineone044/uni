public class KarmolasException extends Exception {
    public KarmolasException(int szam) {
        super(szam >= 10 ? "Sulyos serulest szenvedtel." : "Ez csak egy karcolas.");
    }
}
