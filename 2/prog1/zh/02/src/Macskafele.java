public abstract class Macskafele {
    protected String mintazat;
    protected int ehseg;
    protected double almossag;

    public Macskafele(String mintazat, int ehseg, double almossag) {
        setMintazat(mintazat);
        setEhseg(ehseg);
        setAlmossag(almossag);
    }

    public String getMintazat() {
        return mintazat;
    }

    public void setMintazat(String mintazat) {
        this.mintazat = mintazat;
    }

    public int getEhseg() {
        return ehseg;
    }

    public void setEhseg(int ehseg) {
        this.ehseg = Math.max(ehseg, 0);
    }

    public double getAlmossag() {
        return almossag;
    }

    public void setAlmossag(double almossag) {
        if (almossag < 1) {
            this.almossag = 1;
        } else if (almossag > 10) {
            this.almossag = 10;
        } else {
            this.almossag = almossag;
        }
    }

    public abstract void eszik(String szoveg) throws KarmolasException;

    public double alszik() {
        setAlmossag(getAlmossag() / 2.0);
        return getAlmossag();
    }
}
