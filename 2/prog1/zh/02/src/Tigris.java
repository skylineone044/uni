public class Tigris extends Macskafele {
    private double gyorsasag;

    public double getGyorsasag() {
        return gyorsasag;
    }

    public Tigris(String mintazat, int ehseg, double almossag, double gyorsasag) {
        super(mintazat, ehseg, almossag);
        this.gyorsasag = gyorsasag;
    }

    @Override
    public final void eszik(String szoveg) throws KarmolasException {
        if (szoveg == null && getEhseg() > 5) {
            throw new KarmolasException(getEhseg());
        } else {
            setEhseg(0);
        }
    }

    public void vadaszik(String szoveg) throws KarmolasException {
        try {
            eszik(szoveg);
        } catch (KarmolasException e) {
            throw new KarmolasException(getEhseg() * 2);
        }
    }
}
