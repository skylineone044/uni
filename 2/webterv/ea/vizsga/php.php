<!DOCTYPE html>
<html>
<head>
<style>body *[class~="y"] {color: blue;}</style>
</head>
<body>

<?php
class O {
    public static function kiir() { echo 'o'; }
    public static function hiv() { static::kiir(); }
}
class P extends O {
    public static function kiir() { echo 'p'; }
}
class R extends P {
    public static function kiir() { echo 'r'; }
    public static function teszt() {
        O::hiv(); parent::hiv(); self::hiv(); static::hiv(); }
}
class S extends R {
    public static function kiir() { echo 's'; }
}

S::teszt();

echo "<br>";


class R{ public static $x = 1, $y = 3; }
class S extends R {  public static $x = 5;
    public static function novel(){
        self::$x++; self::$y++; }
}
S::novel();
echo "R::x = ".R::$x.", R::y = ".R::$y.", S::x = ".S::$x.", S::y = ".S::$y."<br />";

echo "<br>";


$a = 1; $b = 2;
function &f($a, &$e){
    $a +=1; $e += 1; return $e;
}
$g = 'f'; $c = &$g($a, $b); $d = $c; $c = 5;

echo "a = $a, b = $b, c = $c, d = $d";

echo "<br>";


$x=2; $y=3; $z=$x++**++$y; echo "$z";

$v = [ ['a', 1], ['b', 2] ];
[1=>$x, 0=>$y] = $v[1];
echo "x = $x, y = $y <br />";

echo "<br>";

function f2(){ static $a=1;
     static $b; $b=2; $c=3;
     $a++; $b++; $c++;
     echo "a=$a, b=$b, c=$c; "; }
f2(); f2();

echo "<br>";


function g1($c){ return $c[1]; }
function g2($c){ return $c[3]; }
class R{
function __call($a, $b){
    return call_user_func($a.'2', $b);
}
function __get($a){ return $a.'2'; }
function __set($a, $b){ $this->$a = $b+3; }
} // class R
$r = new R(); $x = $r->g(1,5,9,13); $r->z = 17;
echo "x = $x, r->y = $r->y, r->z = $r->z";

echo "<br>";


$t = [ 1, 2, 3 ];
foreach ($t as $e) $e = $e + 2;
print_r($t);


echo "<br>";

trait T{
      function f() { static $x = 12; $x *= 2; echo $x; }
}
class O1{ use T; }
class O2{ use T; }
$o1 = new O1(); $o1->f(); $o1->f();
$o2 = new O2(); $o2->f();


echo "<br>";

class P{ private $x1; protected $x2; public $x3;
      function __construct($y1, $y2, $y3){
      $this->x1 = $y1; $this->x2 = $y2; $this->x3 = $y3; }
      function kiir(){ echo $this->x1.$this->x2.$this->x3; }
}
class Q extends P{ public $x3; }
class R {
      function kiir(P $p){ foreach ($p as $x) echo $x; }
}
$q = new Q(1,2,3); $q->x3 = 4;
$r = new R(); $q->kiir(); $r->kiir($q);


echo "<br>";

trait T1{ public static $x1; }
trait T2{ public static $x2; }
class O{ use T1; public static $y; }
class O1 extends O{ use T2; }
class O2 extends O{ use T2; }
O1::$x1 = 14; O2::$x1 = 16;
O1::$x2 = 18; O2::$x2 = 20;
O1::$y = 22; O2::$y = 24;
echo O1::$x1.O1::$x2.O1::$y;


trait T1{ public static $x1; }
trait T2{ public static $x2; }
class O{ use T1; public static $y; }
class O1 extends O{ use T2; }
class O2 extends O{ use T2; }
O1::$x1 = 24; O1::$x2 = 22; O1::$y = 20;
O2::$x1 = 18; O2::$x2 = 16; O2::$y = 14;
echo O1::$x1.O1::$x2.O1::$y;



trait T1 { function g1() { echo '1'; } }
trait T2 { function g2() { echo '2'; } }
trait T12 { use T1, T2; }
class P1{ function g1() { echo '3'; } }
class P2 extends P1{ use T12;
      function g2() { echo '4'; }
}
$p2 = new P2(); $p2->g1(); $p2->g2();


class P{ function kiir(){ echo 'p'; }
              function hiv(){ echo 's'; $this->kiir(); } }
class Q extends P{ function kiir(){ echo 'q'; } }
class R extends Q{
      function kiir(){ echo "r"; parent::kiir(); }
      function hiv(){ echo "t"; parent::hiv(); }
}
$r = new R(); $r->hiv();

?>

<div> 13
    <b>9</b>
    <p>1 <b class="x y">5</b></p>
    <p>7 <b class="xx yy">4</b></p>
    <b>10</b></div>
<div class="x z"> 14
    <b class="x y z">11</b>
    <p>2 <b>6</b></p>
    <p class="y x">8 <b>3</b></p>
    <b>12</b></div>
</body>
</html>
