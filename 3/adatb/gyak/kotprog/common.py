import tkinter as tk
from tkinter import messagebox

MAX_VARCHAR = 256
MAX_DECIMAL = 1024

DBHOST = "localhost"
DBUSER = "root"
DBPASS = ""
DBNAME = "ingatlan"


def update_optionmenu_list(parent_frame, om, get_new_list, om_var, prev_hash=0):
    new_list = tuple(get_new_list())
    list_hash = hash(new_list)
    if list_hash != prev_hash:
        menu = om["menu"]
        menu.delete(0, "end")
        for item in new_list:
            menu.add_command(label=item, command=lambda value=item: om_var.set(value))
    parent_frame.after(1000, update_optionmenu_list, parent_frame, om, get_new_list, om_var, list_hash)


def table_deselect(table):
    if len(table.selection()) > 0:
        table.selection_remove(table.selection()[0])


def update_table(table_frame, table, prev_hash, get, auto_update=True):
    content = tuple(get())
    if hash(content) != prev_hash:
        table.delete(*table.get_children())
        for i, adatok in enumerate(content):
            table.insert(parent='', index='end', iid=i, text='', values=adatok)
    if auto_update:
        table_frame.after(1000, update_table, table_frame, table, hash(content), get)


def id_valid(id_):
    if number_valid(id_):
        return True
    else:
        tk.messagebox.showerror("Error", "Invalid id!")
        return False


def number_valid(num):
    try:
        if int(num) == float(num):
            return True
        else:
            raise TypeError
    except:
        return False
