from tkinter import ttk
from tkinter import messagebox
import mysql.connector as mysql

from common import *



def id_exists(ember_id):
    if id_valid(ember_id):
        con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
        cursor = con.cursor()
        cursor.execute(f"select * "
                       f"from EMBER "
                       f"where uid={ember_id}")
        rows = cursor.fetchall()
        print(f"{ember_id=}: {rows}")
        con.close()
        if rows != []:
            return True
        else:
            tk.messagebox.showerror("Error", "Nincs ember ilyen id-vel!")
            return False
    else:
        return False


def get(uid_text_field, nev_text_field, email_text_field):
    print(f"getting ember: {uid_text_field.get()=}, {nev_text_field.get()=}, {email_text_field.get()=}")
    if uid_text_field.get() == "":
        tk.messagebox.showerror("Error", "ID mező kötelező")
    elif not id_exists(uid_text_field.get()):
        pass
    else:
        if id_exists(uid_text_field.get()):
            con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
            cursor = con.cursor()
            cursor.execute(f"select * "
                           f"from EMBER "
                           f"where uid={uid_text_field.get()}")
            rows = cursor.fetchmany(1)
            nev_text_field.delete(0, 'end')
            email_text_field.delete(0, 'end')
            for row in rows:
                nev_text_field.insert(0, row[1])
                email_text_field.insert(0, row[2])
            con.close()
        else:
            tk.messagebox.showerror("Error", "Nincs emebr ilyen id-vel!")


def get_all():
    con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
    cursor = con.cursor()
    cursor.execute("select * "
                   "from EMBER")
    rows = cursor.fetchall()
    con.close()

    return rows


def add(uid_text_field, nev_text_field, email_text_field):
    if  nev_text_field.get() == "" or email_text_field.get() == "":
        tk.messagebox.showerror("Error", "A név és e-mail cím megadása kötelező!")
    else:
        con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
        cursor = con.cursor()

        cmd = f'insert into EMBER values(NULL,"{nev_text_field.get()}","{email_text_field.get()}")'
        print(f"adding ember:\n    {cmd}")
        cursor.execute(cmd)

        cursor.execute('commit')

        clear_fields(uid_text_field, nev_text_field, email_text_field)
        tk.messagebox.showinfo('Info', 'Sikeres beszúrás')
        con.close()


def edit(uid_text_field, nev_text_field, email_text_field, table):
    if uid_text_field.get() == "" or nev_text_field.get() == "" or email_text_field.get() == "":
        tk.messagebox.showerror("Error", "Minden mező kötelező")
    elif not id_exists(uid_text_field.get()):
        pass
    else:
        con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
        cursor = con.cursor()
        cursor.execute(
            f'update EMBER set nev="{nev_text_field.get()}", email="{email_text_field.get()}" where uid={uid_text_field.get()}')
        cursor.execute('commit')

        clear_fields(uid_text_field, nev_text_field, email_text_field)
        tk.messagebox.showinfo('Info', 'Sikeres frissítés')
        con.close()
        table_deselect(table)


def delete(uid_text_field, nev_text_field, email_text_field):
    if uid_text_field.get() == "":
        tk.messagebox.showerror("Error", "ID mező kötelező")
    elif not id_exists(uid_text_field.get()):
        pass
    else:
        con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
        cursor = con.cursor()
        cmd = f"delete from EMBER " \
              f"where uid={uid_text_field.get()}"
        print(f"deleting emger: {cmd}")
        cursor.execute(cmd)
        cursor.execute("commit")

        clear_fields(uid_text_field, nev_text_field, email_text_field)
        tk.messagebox.showinfo('Info', 'Törlés végrehajtva')
        con.close()


def clear_fields(uid_text_field, nev_text_field, email_text_field, table=None):
    uid_text_field.delete(0, 'end')
    nev_text_field.delete(0, 'end')
    email_text_field.delete(0, 'end')

    if table is not None:
        table_deselect(table)


def table_select(event, table, uid_text_field, nev_text_field, email_text_field):
    current_item = table.focus()
    focus = table.item(current_item)
    print(f"selected: {focus}")

    clear_fields(uid_text_field, nev_text_field, email_text_field)

    if focus["values"] != "":
        uid_text_field.insert(0, focus["values"][0])
        nev_text_field.insert(0, focus["values"][1])
        email_text_field.insert(0, focus["values"][2])


def tab(tab_parent):
    tab_frame = tk.Frame(tab_parent)

    entry_frame = tk.Frame(tab_frame)
    fields_frame = tk.Frame(entry_frame)

    tk.Label(fields_frame, text="uid: ").grid(row=0, column=0)
    uid_text_field = tk.Entry(fields_frame)
    uid_text_field.grid(row=0, column=1)

    tk.Label(fields_frame, text="nev: ").grid(row=1, column=0)
    nev_text_field = tk.Entry(fields_frame)
    nev_text_field.grid(row=1, column=1)

    tk.Label(fields_frame, text="email: ").grid(row=2, column=0)
    email_text_field = tk.Entry(fields_frame)
    email_text_field.grid(row=2, column=1)

    fields_frame.grid(row=0, column=0)

    button_frame = tk.Frame(entry_frame)

    add_button = tk.Button(button_frame, text=" + Add", command=lambda: add(uid_text_field, nev_text_field, email_text_field))
    add_button.grid(row=0, column=0)

    delete_button = tk.Button(button_frame, text=" X Delete", command=lambda: delete(uid_text_field, nev_text_field, email_text_field))
    delete_button.grid(row=0, column=1)

    modify_button = tk.Button(button_frame, text=" ~ Modify", command=lambda: edit(uid_text_field, nev_text_field, email_text_field, table))
    modify_button.grid(row=0, column=2)

    get_button = tk.Button(button_frame, text=" v Get", command=lambda: get(uid_text_field, nev_text_field, email_text_field))
    get_button.grid(row=0, column=3)

    clead_fields_button = tk.Button(button_frame, text=" ^ clear", command=lambda: clear_fields(uid_text_field, nev_text_field, email_text_field, table))
    clead_fields_button.grid(row=0, column=4)

    button_frame.grid(row=3, column=0)

    entry_frame.grid(row=0, column=0)

    table_frame = tk.Frame(tab_frame)
    scrollbar_v = tk.Scrollbar(table_frame)
    scrollbar_v.pack(side=tk.RIGHT, fill=tk.Y)
    scrollbar_h = tk.Scrollbar(table_frame, orient="horizontal")
    scrollbar_h.pack(side=tk.BOTTOM, fill=tk.X)

    table = ttk.Treeview(table_frame, yscrollcommand=scrollbar_v.set, xscrollcommand=scrollbar_h.set)
    table.bind('<ButtonRelease-1>',
               lambda event: table_select(event, table, uid_text_field, nev_text_field, email_text_field))
    table.bind('<Delete>', lambda event: delete(uid_text_field, nev_text_field, email_text_field))
    table.pack()

    scrollbar_v.config(command=table.yview)
    scrollbar_h.config(command=table.xview)

    table["columns"] = ("uid", "nev", "email")
    table_column_width = 200

    table.column("#0", width=0, stretch=tk.NO)
    table.column("uid", width=int(table_column_width / 2), anchor=tk.CENTER)
    table.column("nev", width=table_column_width, anchor=tk.CENTER)
    table.column("email", width=table_column_width, anchor=tk.CENTER)

    table.heading("#0", text="", anchor=tk.CENTER)
    table.heading("uid", text="User id", anchor=tk.CENTER)
    table.heading("nev", text="Név", anchor=tk.CENTER)
    table.heading("email", text="e-mail", anchor=tk.CENTER)

    table.pack()
    table_frame.grid(row=1, column=0)

    update_table(table_frame, table, 0, get_all)


    return tab_frame
