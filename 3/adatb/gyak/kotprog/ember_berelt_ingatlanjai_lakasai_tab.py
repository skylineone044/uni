from tkinter import ttk
from tkinter import messagebox
import mysql.connector as mysql

from common import *

import lakas

def get_lakasai(id_nev):
    uid, nev = id_nev.split(" ")[0], " ".join(id_nev.split(" ")[1:])
    con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
    cursor = con.cursor()
    cursor.execute(f'select INGATLAN.id, INGATLAN.cim, INGATLAN.terulet, LAKAS.lakas_szam, LAKAS.helysegek_szama, BERLOJE.mikortol, BERLOJE.meddig '
                   f'from LAKAS, INGATLAN, BERLOJE '
                   f'where LAKAS.id = INGATLAN.id AND BERLOJE.id = INGATLAN.id AND LAKAS.id IN (select id from BERLOJE where uid = "{uid}") AND BERLOJE.uid = "{uid}"')
    rows = cursor.fetchall()
    con.close()
    return rows


def get_lakasok():
    lakasok = lakas.get_all()
    return [f"{item[0]}; {item[1]}; {item[3]}" for item in lakasok]


def add_lakas(ingatlan, ember, mikortol, meddig):
    if  ember == "" or ingatlan == "":
        tk.messagebox.showerror("Error", "Invalid selection!")
    else:
        con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
        cursor = con.cursor()

        ember_id = str(ember).split(" ")[0]
        ingatlan_id = str(ingatlan).split(";")[0]

        if mikortol.get() == "":
            mikortol_str = "NULL"
        else:
            mikortol_str = f'"{mikortol.get()}"'

        if meddig.get() == "":
            meddig_str = "NULL"
        else:
            meddig_str = f'"{meddig.get()}"'


        try:
            cmd = f'insert into BERLOJE values("{ember_id}","{ingatlan_id}", {mikortol_str}, {meddig_str})'
            print(f"adding berlo:\n    {cmd}")
            cursor.execute(cmd)

            cursor.execute('commit')

            mikortol.delete(0, "end")
            meddig.delete(0, "end")
            tk.messagebox.showinfo('Info', 'Sikeres beszúrás')
        except TypeError:
            tk.messagebox.showerror("Error", "Invalid date!")
        except mysql.errors.DataError:
            tk.messagebox.showerror("Error", "Invalid date!")
        except mysql.errors.IntegrityError:
            tk.messagebox.showerror("Error", "Ez már létezig az adatbázisban!")
        finally:
            con.close()


def remove_selected_lakasa(lakasai_table, ember):
    current_item = lakasai_table.focus()
    focus = lakasai_table.item(current_item)
    print(f"selected: {focus}")

    con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
    cursor = con.cursor()

    try:
        ember_id = ember.split(" ")[0]
        ingatlan_id = focus["values"][0]

        cmd = f'delete from BERLOJE ' \
              f'where BERLOJE.uid={ember_id} AND BERLOJE.id={ingatlan_id}'
        print(f"deleting berloje: {cmd}")
        cursor.execute(cmd)
        cursor.execute("commit")

        tk.messagebox.showinfo('Info', 'Törlés végrehajtva')
    except IndexError:
        tk.messagebox.showinfo('Info', 'Hibbás Kijelölés!')
    finally:
        con.close()


def edit_selected_lakasa(lakasai_table, ember, mikortol, meddig):
    current_item = lakasai_table.focus()
    focus = lakasai_table.item(current_item)
    print(f"selected: {focus}")

    con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
    cursor = con.cursor()

    try:
        ember_id = ember.split(" ")[0]
        ingatlan_id = focus["values"][0]

        if mikortol.get() == "":
            mikortol_str = "NULL"
        else:
            mikortol_str = f'"{mikortol.get()}"'

        if meddig.get() == "":
            meddig_str = "NULL"
        else:
            meddig_str = f'"{meddig.get()}"'

        cmd = f'update BERLOJE set mikortol={mikortol_str}, meddig={meddig_str} where BERLOJE.uid={ember_id} AND BERLOJE.id={ingatlan_id}'
        print(f"deleting berloje: {cmd}")
        cursor.execute(cmd)
        cursor.execute("commit")

        mikortol.delete(0, "end")
        meddig.delete(0, "end")
        tk.messagebox.showinfo('Info', 'Sikeres frissítés')
    except IndexError:
        tk.messagebox.showinfo('Info', 'Hibbás Kijelölés!')
    except mysql.errors.DataError:
        tk.messagebox.showerror("Error", "Invalid date!")
    finally:
        con.close()

def lakasai_table_select(event, table, selected_ingatlan, mikortol_text_field, meddig_text_field):
    current_item = table.focus()
    focus = table.item(current_item)
    print(f"selected: {focus}")
    mikortol_text_field.delete(0, "end")
    meddig_text_field.delete(0, "end")

    if focus["values"] != "":
        mikortol_text_field.insert(0, focus["values"][5] if focus["values"][5] is not None and focus["values"][5] != "None" else "")
        meddig_text_field.insert(0, focus["values"][6] if focus["values"][6] is not None and focus["values"][6] != "None" else "")
        selected_ingatlan.set(f"{focus['values'][0]}; {focus['values'][1]}; {focus['values'][3]}; {focus['values'][4]}")


def lakas_sub_tab(tab_parent, seletcted_ember):
    sub_tab_frame = tk.Frame(tab_parent)

    entry_frame = tk.Frame(sub_tab_frame)
    fields_frame = tk.Frame(entry_frame)

    tk.Label(fields_frame, text="lakas: ").grid(row=1, column=0)
    lakas = tk.StringVar()
    lakas_dropdown = tk.OptionMenu(fields_frame, lakas, *(get_lakasok() if len(get_lakasok()) >= 2 else ["#1", "#2"]))
    lakas_dropdown.grid(row=1, column=2)
    update_optionmenu_list(fields_frame, lakas_dropdown, get_lakasok, lakas)

    tk.Label(fields_frame, text="mikortol: ").grid(row=1, column=3)
    mikortol_text_field = tk.Entry(fields_frame)
    mikortol_text_field.grid(row=1, column=4)

    tk.Label(fields_frame, text="meddig: ").grid(row=1, column=5)
    meddig_text_field = tk.Entry(fields_frame)
    meddig_text_field.grid(row=1, column=6)

    fields_frame.grid(row=0, column=0)

    button_frame = tk.Frame(entry_frame)

    add_button = tk.Button(button_frame, text=" + Add", command=lambda: add_lakas(lakas.get(), seletcted_ember.get(), mikortol_text_field, meddig_text_field))
    add_button.grid(row=0, column=0)

    delete_button = tk.Button(button_frame, text=" X Remove", command=lambda: remove_selected_lakasa(lakasai_table, seletcted_ember.get()))
    delete_button.grid(row=0, column=1)

    edit_button = tk.Button(button_frame, text=" ~ Modify", command=lambda: edit_selected_lakasa(lakasai_table, seletcted_ember.get(), mikortol_text_field, meddig_text_field))
    edit_button.grid(row=0, column=2)

    button_frame.grid(row=3, column=0)

    entry_frame.grid(row=0, column=0)

    # lakasai table
    lakasai_table_frame = tk.Frame(sub_tab_frame)
    scrollbar_v = tk.Scrollbar(lakasai_table_frame)
    scrollbar_v.pack(side=tk.RIGHT, fill=tk.Y)
    scrollbar_h = tk.Scrollbar(lakasai_table_frame, orient="horizontal")
    scrollbar_h.pack(side=tk.BOTTOM, fill=tk.X)

    lakasai_table = ttk.Treeview(lakasai_table_frame, yscrollcommand=scrollbar_v.set, xscrollcommand=scrollbar_h.set)
    lakasai_table.bind('<ButtonRelease-1>', lambda event: lakasai_table_select(event, lakasai_table, lakas, mikortol_text_field, meddig_text_field))
    lakasai_table.bind('<Delete>', lambda event: remove_selected_lakasa(lakasai_table, seletcted_ember.get()))
    lakasai_table.pack()

    scrollbar_v.config(command=lakasai_table.yview)
    scrollbar_h.config(command=lakasai_table.xview)

    lakasai_table["columns"] = ("id", "cim", "terulet", "lakas_szam", "helysegek_szama", "mikortol", "meddig")
    table_column_width = 200

    lakasai_table.column("#0", width=0, stretch=tk.NO)
    lakasai_table.column("id", width=int(table_column_width / 2), anchor=tk.CENTER)
    lakasai_table.column("cim", width=int(table_column_width * 1.5), anchor=tk.CENTER)
    lakasai_table.column("terulet", width=int(table_column_width / 2), anchor=tk.CENTER)
    lakasai_table.column("lakas_szam", width=int(table_column_width / 2), anchor=tk.CENTER)
    lakasai_table.column("helysegek_szama", width=int(table_column_width / 2), anchor=tk.CENTER)
    lakasai_table.column("mikortol", width=int(table_column_width / 2), anchor=tk.CENTER)
    lakasai_table.column("meddig", width=int(table_column_width / 2), anchor=tk.CENTER)

    lakasai_table.heading("#0", text="", anchor=tk.CENTER)
    lakasai_table.heading("id", text="Ingatlan id", anchor=tk.CENTER)
    lakasai_table.heading("cim", text="Cím", anchor=tk.CENTER)
    lakasai_table.heading("terulet", text="Terület", anchor=tk.CENTER)
    lakasai_table.heading("lakas_szam", text="Lakás szám", anchor=tk.CENTER)
    lakasai_table.heading("helysegek_szama", text="Helységek száma", anchor=tk.CENTER)
    lakasai_table.heading("mikortol", text="Mikortól", anchor=tk.CENTER)
    lakasai_table.heading("meddig", text="Meddig", anchor=tk.CENTER)

    lakasai_table.pack()
    lakasai_table_frame.grid(row=1, column=0)

    update_table(lakasai_table_frame, lakasai_table, 0, lambda: get_lakasai(seletcted_ember.get()))

    return sub_tab_frame
