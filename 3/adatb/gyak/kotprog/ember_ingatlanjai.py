from tkinter import ttk
import mysql.connector as mysql

from common import *

import ember_ingatlanjai_telkei_tab
import ember_ingatlanjai_epuletei_tab
import ember_ingatlanjai_lakasai_tab


#TODO: add complex queries
#TODO finish doc: usage instructions,


def get_ember_nevek():
    con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
    cursor = con.cursor()
    cursor.execute("select * from EMBER")
    rows = cursor.fetchall()
    nevek = [f"{row[0]} {row[1]}" for row in rows]
    con.close()
    return nevek


def tab(tab_parent):
    tab_frame = tk.Frame(tab_parent)

    entry_frame = tk.Frame(tab_frame)
    fields_frame = tk.Frame(entry_frame)

    tk.Label(fields_frame, text="ember: ").grid(row=0, column=0)
    nev = tk.StringVar()
    nev_dropdown = tk.OptionMenu(fields_frame, nev, *(get_ember_nevek() if len(get_ember_nevek()) >= 2 else ["#1", "#2"]))
    nev_dropdown.grid(row=0, column=1)
    update_optionmenu_list(fields_frame, nev_dropdown, get_ember_nevek, nev)

    fields_frame.pack()
    entry_frame.pack()

    sub_tab_parent = ttk.Notebook(tab_frame)

    telektab = ember_ingatlanjai_telkei_tab.telek_sub_tab(sub_tab_parent, nev)
    sub_tab_parent.add(telektab, text="Telkei")

    epulettab = ember_ingatlanjai_epuletei_tab.epulet_sub_tab(sub_tab_parent, nev)
    sub_tab_parent.add(epulettab, text="Épületei")

    lakastab = ember_ingatlanjai_lakasai_tab.lakas_sub_tab(sub_tab_parent, nev)
    sub_tab_parent.add(lakastab, text="Lakásai")

    sub_tab_parent.pack(expand=1, fill="both")


    return tab_frame
