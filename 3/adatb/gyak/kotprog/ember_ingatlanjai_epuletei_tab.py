from tkinter import ttk
from tkinter import messagebox
import mysql.connector as mysql

from common import *

import epulet

def get_epuletei(id_nev):
    uid, nev = id_nev.split(" ")[0], " ".join(id_nev.split(" ")[1:])
    con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
    cursor = con.cursor()
    cursor.execute(f'select INGATLAN.id, INGATLAN.cim, INGATLAN.terulet, EPULET.nev, EPULET.epulet_szam, TULAJDONOSA.mikortol '
                   f'from EPULET, INGATLAN, TULAJDONOSA '
                   f'where EPULET.id = INGATLAN.id AND TULAJDONOSA.id = INGATLAN.id AND EPULET.id IN (select id from TULAJDONOSA where uid = "{uid}") AND TULAJDONOSA.uid = "{uid}"')
    rows = cursor.fetchall()
    con.close()
    return rows


def get_epuletek():
    epuletek = epulet.get_all()
    return [f"{item[0]}; {item[1]}; {item[3]}" for item in epuletek]


def add_epulet(ingatlan, ember, mikortol):
    if  ember == "" or ingatlan == "":
        tk.messagebox.showerror("Error", "Invalid selection!")
    else:
        con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
        cursor = con.cursor()

        ember_id = str(ember).split(" ")[0]
        ingatlan_id = str(ingatlan).split(";")[0]

        if mikortol.get() == "":
            mikortol_str = "NULL"
        else:
            mikortol_str = f'"{mikortol.get()}"'

        try:
            cmd = f'insert into TULAJDONOSA values("{ember_id}","{ingatlan_id}", {mikortol_str})'
            print(f"adding tulajdonos:\n    {cmd}")
            cursor.execute(cmd)

            cursor.execute('commit')

            mikortol.delete(0, "end")
            tk.messagebox.showinfo('Info', 'Sikeres beszúrás')
        except TypeError:
            tk.messagebox.showerror("Error", "Invalid date!")
        except mysql.errors.DataError:
            tk.messagebox.showerror("Error", "Invalid date!")
        except mysql.errors.IntegrityError:
            tk.messagebox.showerror("Error", "Ez már létezig az adatbázisban!")
        finally:
            con.close()


def remove_selected_epulete(epuletei_table, ember):
    current_item = epuletei_table.focus()
    focus = epuletei_table.item(current_item)
    print(f"selected: {focus}")

    con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
    cursor = con.cursor()

    try:
        ember_id = ember.split(" ")[0]
        ingatlan_id = focus["values"][0]

        cmd = f'delete from TULAJDONOSA ' \
              f'where TULAJDONOSA.uid={ember_id} AND TULAJDONOSA.id={ingatlan_id}'
        print(f"deleting tulajdonosa: {cmd}")
        cursor.execute(cmd)
        cursor.execute("commit")

        tk.messagebox.showinfo('Info', 'Törlés végrehajtva')
    except IndexError:
        tk.messagebox.showinfo('Info', 'Hibbás Kijelölés!')
    finally:
        con.close()


def edit_selected_epulete(epuletei_table, ember, mikortol):
    current_item = epuletei_table.focus()
    focus = epuletei_table.item(current_item)
    print(f"selected: {focus}")

    con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
    cursor = con.cursor()

    try:
        ember_id = ember.split(" ")[0]
        ingatlan_id = focus["values"][0]

        cmd = f'update TULAJDONOSA set mikortol="{mikortol.get()}" where TULAJDONOSA.uid={ember_id} AND TULAJDONOSA.id={ingatlan_id}'
        print(f"deleting tulajdonosa: {cmd}")
        cursor.execute(cmd)
        cursor.execute("commit")

        mikortol.delete(0, "end")
        tk.messagebox.showinfo('Info', 'Sikeres frissítés')
    except IndexError:
        tk.messagebox.showinfo('Info', 'Hibbás Kijelölés!')
    except mysql.errors.DataError:
        tk.messagebox.showerror("Error", "Invalid date!")
    finally:
        con.close()

def epuletei_table_select(event, table, selected_ingatlan, mikortol_text_field):
    current_item = table.focus()
    focus = table.item(current_item)
    print(f"selected: {focus}")
    mikortol_text_field.delete(0, "end")

    if focus["values"] != "":
        mikortol_text_field.insert(0, focus["values"][5] if focus["values"][5] is not None and focus["values"][5] != "None" else "")
        selected_ingatlan.set(f"{focus['values'][0]}; {focus['values'][1]}; {focus['values'][3]}; {focus['values'][4]}")



def epulet_sub_tab(tab_parent, seletcted_ember):
    sub_tab_frame = tk.Frame(tab_parent)

    entry_frame = tk.Frame(sub_tab_frame)
    fields_frame = tk.Frame(entry_frame)

    tk.Label(fields_frame, text="epulet: ").grid(row=1, column=0)
    epulet = tk.StringVar()
    epulet_dropdown = tk.OptionMenu(fields_frame, epulet, *(get_epuletek() if len(get_epuletek()) >= 2 else ["#1", "#2"]))
    epulet_dropdown.grid(row=1, column=2)
    update_optionmenu_list(fields_frame, epulet_dropdown, get_epuletek, epulet)

    tk.Label(fields_frame, text="mikortol: ").grid(row=1, column=3)
    mikortol_text_field = tk.Entry(fields_frame)
    mikortol_text_field.grid(row=1, column=4)

    fields_frame.grid(row=0, column=0)

    button_frame = tk.Frame(entry_frame)

    add_button = tk.Button(button_frame, text=" + Add", command=lambda: add_epulet(epulet.get(), seletcted_ember.get(), mikortol_text_field))
    add_button.grid(row=0, column=0)

    delete_button = tk.Button(button_frame, text=" X Remove", command=lambda: remove_selected_epulete(epuletei_table, seletcted_ember.get()))
    delete_button.grid(row=0, column=1)

    edit_button = tk.Button(button_frame, text=" ~ Modify", command=lambda: edit_selected_epulete(epuletei_table, seletcted_ember.get(), mikortol_text_field))
    edit_button.grid(row=0, column=2)

    button_frame.grid(row=3, column=0)

    entry_frame.grid(row=0, column=0)

    # epuletei table
    epuletei_table_frame = tk.Frame(sub_tab_frame)
    scrollbar_v = tk.Scrollbar(epuletei_table_frame)
    scrollbar_v.pack(side=tk.RIGHT, fill=tk.Y)
    scrollbar_h = tk.Scrollbar(epuletei_table_frame, orient="horizontal")
    scrollbar_h.pack(side=tk.BOTTOM, fill=tk.X)

    epuletei_table = ttk.Treeview(epuletei_table_frame, yscrollcommand=scrollbar_v.set, xscrollcommand=scrollbar_h.set)
    epuletei_table.bind('<ButtonRelease-1>', lambda event: epuletei_table_select(event, epuletei_table, epulet, mikortol_text_field))
    epuletei_table.bind('<Delete>', lambda event: remove_selected_epulete(epuletei_table, seletcted_ember.get()))
    epuletei_table.pack()

    scrollbar_v.config(command=epuletei_table.yview)
    scrollbar_h.config(command=epuletei_table.xview)

    epuletei_table["columns"] = ("id", "cim", "terulet", "nev", "epulet_szam", "mikortol")
    table_column_width = 200

    epuletei_table.column("#0", width=0, stretch=tk.NO)
    epuletei_table.column("id", width=int(table_column_width / 2), anchor=tk.CENTER)
    epuletei_table.column("cim", width=int(table_column_width * 1.5), anchor=tk.CENTER)
    epuletei_table.column("terulet", width=int(table_column_width / 2), anchor=tk.CENTER)
    epuletei_table.column("nev", width=int(table_column_width * 1.5), anchor=tk.CENTER)
    epuletei_table.column("epulet_szam", width=int(table_column_width / 2), anchor=tk.CENTER)
    epuletei_table.column("mikortol", width=int(table_column_width / 2), anchor=tk.CENTER)

    epuletei_table.heading("#0", text="", anchor=tk.CENTER)
    epuletei_table.heading("id", text="Ingatlan id", anchor=tk.CENTER)
    epuletei_table.heading("cim", text="Cím", anchor=tk.CENTER)
    epuletei_table.heading("terulet", text="Terület", anchor=tk.CENTER)
    epuletei_table.heading("nev", text="Név", anchor=tk.CENTER)
    epuletei_table.heading("epulet_szam", text="Épület szám", anchor=tk.CENTER)
    epuletei_table.heading("mikortol", text="Mikortól", anchor=tk.CENTER)

    epuletei_table.pack()
    epuletei_table_frame.grid(row=1, column=0)

    update_table(epuletei_table_frame, epuletei_table, 0, lambda: get_epuletei(seletcted_ember.get()))

    return sub_tab_frame
