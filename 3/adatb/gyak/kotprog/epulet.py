from tkinter import ttk
from tkinter import messagebox
import mysql.connector as mysql

from common import *


def id_exists(id_):
    if id_valid(id_):
        con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
        cursor = con.cursor()
        cursor.execute(f"select * "
                       f"from EPULET "
                       f"where id={id_}")
        rows = cursor.fetchall()
        print(f"{id_=}: {rows}")
        con.close()
        if rows != []:
            return True
        else:
            tk.messagebox.showerror("Error", "Nincs epulet ilyen id-vel!")
            return False
    else:
        return False


def get(id_text_field, cim_text_field, terulet_text_field, epulet_szam_text_field, nev_text_field):
    print(f"getting epulet: {id_text_field.get()=}, {cim_text_field.get()=}, {terulet_text_field.get()=}, {nev_text_field.get()=}, {epulet_szam_text_field=}")
    if id_text_field.get() == "":
        tk.messagebox.showerror("Error", "ID mező kötelező")
    elif not id_exists(id_text_field.get()):
        pass
    else:
        if id_exists(id_text_field.get()):
            con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
            cursor = con.cursor()
            cursor.execute(f"select INGATLAN.id, INGATLAN.cim, INGATLAN.terulet, EPULET.nev, EPULET.epulet_szam "
                           f"from INGATLAN, EPULET "
                           f"where EPULET.id={id_text_field.get()} AND EPULET.id = INGATLAN.id")
            rows = cursor.fetchmany(1)

            clear_fields(id_text_field, cim_text_field, terulet_text_field, epulet_szam_text_field, nev_text_field)
            for row in rows:
                id_text_field.insert(0, row[0])
                cim_text_field.insert(0, row[1])
                terulet_text_field.insert(0, row[2])
                nev_text_field.insert(0, row[3])
                epulet_szam_text_field.insert(0, row[4])
            con.close()
        else:
            tk.messagebox.showerror("Error", "Nincs epulet ilyen id-vel!")


def get_all():
    con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
    cursor = con.cursor()
    cursor.execute( f"select INGATLAN.id, INGATLAN.cim, INGATLAN.terulet, EPULET.nev, EPULET.epulet_szam "
                    f"from INGATLAN, EPULET "
                    f"where EPULET.id = INGATLAN.id")
    rows = cursor.fetchall()
    con.close()

    return rows


def add(id_text_field, cim_text_field, terulet_text_field, epulet_szam_text_field, nev_text_field):
    if  cim_text_field.get() == "" or terulet_text_field.get() == "" or nev_text_field.get() == "" or epulet_szam_text_field.get() == "":
        tk.messagebox.showerror("Error", "A cím, terület és név megadása kötelező!")
    elif not number_valid(terulet_text_field.get()):
        tk.messagebox.showerror("Error", "Invalid terulet!")
    elif not number_valid(epulet_szam_text_field.get()):
        tk.messagebox.showerror("Error", "Invalid epulet_szam!")
    else:
        con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
        cursor = con.cursor()

        cmd_add_ingatlan = f'insert into INGATLAN values(NULL,"{cim_text_field.get()}","{terulet_text_field.get()}")'
        print(f"adding ingatlan:\n    {cmd_add_ingatlan}")
        cursor.execute(cmd_add_ingatlan)
        cursor.execute('commit')

        cmd_add_epulet = f'insert into EPULET ' \
                         f'values((select  LAST_INSERT_ID() from INGATLAN LIMIT 1),"{epulet_szam_text_field.get()}", "{nev_text_field.get()}")'
        print(f"adding epulet:\n    {cmd_add_epulet}")
        cursor.execute(cmd_add_epulet)
        cursor.execute('commit')

        clear_fields(id_text_field, cim_text_field, terulet_text_field, epulet_szam_text_field, nev_text_field)
        tk.messagebox.showinfo('Info', 'Sikeres beszúrás')
        con.close()


def edit(id_text_field, cim_text_field, terulet_text_field, epulet_szam_text_field, nev_text_field, table):
    if id_text_field.get() == "" or cim_text_field.get() == "" or terulet_text_field.get() == "" or nev_text_field.get() == "" or epulet_szam_text_field.get() == "":
        tk.messagebox.showerror("Error", "Minden mező kötelező")
    elif not id_exists(id_text_field.get()):
        pass
    elif not number_valid(terulet_text_field.get()):
        tk.messagebox.showerror("Error", "Invalid terulet!")
    elif not number_valid(epulet_szam_text_field.get()):
        tk.messagebox.showerror("Error", "Invalid epulet_szam!")
    else:
        con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
        cursor = con.cursor()

        cmd_update_ingatlan = f'update INGATLAN set cim="{cim_text_field.get()}", terulet="{terulet_text_field.get()}" where id={id_text_field.get()}'
        cmd_update_epulet = f'update EPULET set epulet_szam="{epulet_szam_text_field.get()}", nev="{nev_text_field.get()}" where id={id_text_field.get()}'
        cursor.execute(cmd_update_ingatlan)
        cursor.execute(cmd_update_epulet)

        cursor.execute('commit')

        clear_fields(id_text_field, cim_text_field, terulet_text_field, epulet_szam_text_field, nev_text_field, table)
        tk.messagebox.showinfo('Info', 'Sikeres frissítés')
        con.close()


def delete(id_text_field, cim_text_field, terulet_text_field, epulet_szam_text_field, nev_text_field):
    if id_text_field.get() == "":
        tk.messagebox.showerror("Error", "ID mező kötelező")
    elif not id_exists(id_text_field.get()):
        pass
    else:
        con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
        cursor = con.cursor()

        cmd_delete_ingatlan = f"delete from INGATLAN " \
                              f"where id={id_text_field.get()}"
        print(f"deleting ingatlan: {cmd_delete_ingatlan}")
        cursor.execute(cmd_delete_ingatlan)
        cursor.execute("commit")

        clear_fields(id_text_field, cim_text_field, terulet_text_field, epulet_szam_text_field, nev_text_field)
        tk.messagebox.showinfo('Info', 'Törlés végrehajtva')
        con.close()


def clear_fields(id_text_field, cim_text_field, terulet_text_field, epulet_szam_text_field, nev_text_field, table=None):
    id_text_field.delete(0, 'end')
    cim_text_field.delete(0, 'end')
    terulet_text_field.delete(0, 'end')
    epulet_szam_text_field.delete(0, 'end')
    nev_text_field.delete(0, 'end')

    if table is not None:
        table_deselect(table)


def table_select(event, table, id_text_field, cim_text_field, terulet_text_field, epulet_szam_text_field, nev_text_field):
    current_item = table.focus()
    focus = table.item(current_item)
    print(f"selected: {focus}")

    if focus["values"] != "":
        clear_fields(id_text_field, cim_text_field, terulet_text_field, epulet_szam_text_field, nev_text_field)

        id_text_field.insert(0, focus["values"][0])
        cim_text_field.insert(0, focus["values"][1])
        terulet_text_field.insert(0, focus["values"][2])
        nev_text_field.insert(0, focus["values"][3])
        epulet_szam_text_field.insert(0, focus["values"][4])


def tab(tab_parent):
    tab_frame = tk.Frame(tab_parent)

    entry_frame = tk.Frame(tab_frame)
    fields_frame = tk.Frame(entry_frame)

    tk.Label(fields_frame, text="id: ").grid(row=0, column=0)
    id_text_field = tk.Entry(fields_frame)
    id_text_field.grid(row=0, column=1)

    tk.Label(fields_frame, text="cim: ").grid(row=1, column=0)
    cim_text_field = tk.Entry(fields_frame)
    cim_text_field.grid(row=1, column=1)

    tk.Label(fields_frame, text="terulet: ").grid(row=2, column=0)
    terulet_text_field = tk.Entry(fields_frame)
    terulet_text_field.grid(row=2, column=1)

    tk.Label(fields_frame, text="nev: ").grid(row=3, column=0)
    nev_text_field = tk.Entry(fields_frame)
    nev_text_field.grid(row=3, column=1)

    tk.Label(fields_frame, text="epulet szam: ").grid(row=4, column=0)
    epulet_szam_text_field = tk.Entry(fields_frame)
    epulet_szam_text_field.grid(row=4, column=1)

    fields_frame.grid(row=0, column=0)

    button_frame = tk.Frame(entry_frame)

    add_button = tk.Button(button_frame, text=" + Add", command=lambda: add(id_text_field, cim_text_field, terulet_text_field, epulet_szam_text_field, nev_text_field))
    add_button.grid(row=0, column=0)

    delete_button = tk.Button(button_frame, text=" X Delete", command=lambda: delete(id_text_field, cim_text_field, terulet_text_field, epulet_szam_text_field, nev_text_field))
    delete_button.grid(row=0, column=1)

    modify_button = tk.Button(button_frame, text=" ~ Modify", command=lambda: edit(id_text_field, cim_text_field, terulet_text_field, epulet_szam_text_field, nev_text_field,  table))
    modify_button.grid(row=0, column=2)

    get_button = tk.Button(button_frame, text=" v Get", command=lambda: get(id_text_field, cim_text_field, terulet_text_field, epulet_szam_text_field, nev_text_field))
    get_button.grid(row=0, column=3)

    clead_fields_button = tk.Button(button_frame, text=" ^ clear", command=lambda: clear_fields(id_text_field, cim_text_field, terulet_text_field, epulet_szam_text_field, nev_text_field, table))
    clead_fields_button.grid(row=0, column=4)

    button_frame.grid(row=3, column=0)

    entry_frame.grid(row=0, column=0)

    table_frame = tk.Frame(tab_frame)
    scrollbar_v = tk.Scrollbar(table_frame)
    scrollbar_v.pack(side=tk.RIGHT, fill=tk.Y)
    scrollbar_h = tk.Scrollbar(table_frame, orient="horizontal")
    scrollbar_h.pack(side=tk.BOTTOM, fill=tk.X)

    table = ttk.Treeview(table_frame, yscrollcommand=scrollbar_v.set, xscrollcommand=scrollbar_h.set)
    table.bind('<ButtonRelease-1>',
               lambda event: table_select(event, table, id_text_field, cim_text_field, terulet_text_field, epulet_szam_text_field, nev_text_field))
    table.bind('<Delete>', lambda event: delete(id_text_field, cim_text_field, terulet_text_field, epulet_szam_text_field, nev_text_field))
    table.pack()

    scrollbar_v.config(command=table.yview)
    scrollbar_h.config(command=table.xview)

    table["columns"] = ("id", "cim", "terulet", "nev", "epulet_szam")
    table_column_width = 180

    table.column("#0", width=0, stretch=tk.NO)
    table.column("id", width=int(table_column_width / 2), anchor=tk.CENTER)
    table.column("cim", width=int(table_column_width * 1.5), anchor=tk.CENTER)
    table.column("terulet", width=int(table_column_width / 2), anchor=tk.CENTER)
    table.column("nev", width=int(table_column_width * 1.5), anchor=tk.CENTER)
    table.column("epulet_szam", width=int(table_column_width / 2), anchor=tk.CENTER)

    table.heading("#0", text="", anchor=tk.CENTER)
    table.heading("id", text="Ingatlan id", anchor=tk.CENTER)
    table.heading("cim", text="Cím", anchor=tk.CENTER)
    table.heading("terulet", text="Terület", anchor=tk.CENTER)
    table.heading("nev", text="Név", anchor=tk.CENTER)
    table.heading("epulet_szam", text="Épület szám", anchor=tk.CENTER)

    table.pack()
    table_frame.grid(row=1, column=0)

    update_table(table_frame, table, 0, get_all)

    return tab_frame
