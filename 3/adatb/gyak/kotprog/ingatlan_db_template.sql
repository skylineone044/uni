-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 25, 2021 at 09:55 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ingatlan`
--

-- --------------------------------------------------------

--
-- Table structure for table `BERLOJE`
--

CREATE TABLE `BERLOJE` (
  `uid` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `mikortol` date DEFAULT NULL,
  `meddig` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `EMBER`
--

CREATE TABLE `EMBER` (
  `uid` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `email` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `EPULET`
--

CREATE TABLE `EPULET` (
  `id` int(11) NOT NULL,
  `epulet_szam` decimal(65,0) DEFAULT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `INGATLAN`
--

CREATE TABLE `INGATLAN` (
  `id` int(11) NOT NULL,
  `cim` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `terulet` decimal(65,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `LAKAS`
--

CREATE TABLE `LAKAS` (
  `id` int(11) NOT NULL,
  `lakas_szam` decimal(65,0) DEFAULT NULL,
  `helysegek_szama` decimal(65,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `TELEK`
--

CREATE TABLE `TELEK` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `TULAJDONOSA`
--

CREATE TABLE `TULAJDONOSA` (
  `uid` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `mikortol` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `BERLOJE`
--
ALTER TABLE `BERLOJE`
  ADD PRIMARY KEY (`uid`,`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `EMBER`
--
ALTER TABLE `EMBER`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `EPULET`
--
ALTER TABLE `EPULET`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `INGATLAN`
--
ALTER TABLE `INGATLAN`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `LAKAS`
--
ALTER TABLE `LAKAS`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `TELEK`
--
ALTER TABLE `TELEK`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `TULAJDONOSA`
--
ALTER TABLE `TULAJDONOSA`
  ADD PRIMARY KEY (`uid`,`id`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `EMBER`
--
ALTER TABLE `EMBER`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `INGATLAN`
--
ALTER TABLE `INGATLAN`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `BERLOJE`
--
ALTER TABLE `BERLOJE`
  ADD CONSTRAINT `BERLOJE_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `EMBER` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `BERLOJE_ibfk_2` FOREIGN KEY (`id`) REFERENCES `INGATLAN` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `EPULET`
--
ALTER TABLE `EPULET`
  ADD CONSTRAINT `EPULET_ibfk_1` FOREIGN KEY (`id`) REFERENCES `INGATLAN` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `LAKAS`
--
ALTER TABLE `LAKAS`
  ADD CONSTRAINT `LAKAS_ibfk_1` FOREIGN KEY (`id`) REFERENCES `INGATLAN` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `TELEK`
--
ALTER TABLE `TELEK`
  ADD CONSTRAINT `TELEK_ibfk_1` FOREIGN KEY (`id`) REFERENCES `INGATLAN` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `TULAJDONOSA`
--
ALTER TABLE `TULAJDONOSA`
  ADD CONSTRAINT `TULAJDONOSA_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `EMBER` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `TULAJDONOSA_ibfk_2` FOREIGN KEY (`id`) REFERENCES `INGATLAN` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
