-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 28, 2021 at 05:09 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ingatlan`
--

-- --------------------------------------------------------

--
-- Table structure for table `BERLOJE`
--

CREATE TABLE `BERLOJE` (
  `uid` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `mikortol` date DEFAULT NULL,
  `meddig` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `BERLOJE`
--

INSERT INTO `BERLOJE` (`uid`, `id`, `mikortol`, `meddig`) VALUES
(45, 282, NULL, NULL),
(45, 289, NULL, NULL),
(46, 272, NULL, NULL),
(46, 277, NULL, NULL),
(46, 282, NULL, NULL),
(46, 289, NULL, NULL),
(47, 272, NULL, NULL),
(47, 282, NULL, NULL),
(47, 352, '2020-02-03', '2025-12-31'),
(48, 272, NULL, NULL),
(48, 278, NULL, NULL),
(48, 282, NULL, NULL),
(48, 289, NULL, NULL),
(48, 290, '2021-08-15', '2023-12-31'),
(48, 298, NULL, NULL),
(48, 302, NULL, NULL),
(48, 305, NULL, NULL),
(48, 315, NULL, NULL),
(48, 319, NULL, NULL),
(48, 321, NULL, NULL),
(48, 349, NULL, NULL),
(49, 272, NULL, NULL),
(49, 282, NULL, NULL),
(49, 289, NULL, NULL),
(50, 282, NULL, NULL),
(51, 282, NULL, NULL),
(51, 289, NULL, NULL),
(52, 272, NULL, NULL),
(52, 282, NULL, NULL),
(54, 289, NULL, NULL),
(55, 289, NULL, NULL),
(55, 329, NULL, NULL),
(55, 334, NULL, NULL),
(55, 341, NULL, NULL),
(55, 342, NULL, NULL),
(55, 344, NULL, NULL),
(55, 355, NULL, NULL),
(55, 360, NULL, NULL),
(55, 364, NULL, NULL),
(55, 368, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `EMBER`
--

CREATE TABLE `EMBER` (
  `uid` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `email` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `EMBER`
--

INSERT INTO `EMBER` (`uid`, `nev`, `email`) VALUES
(45, 'Koaxk Ábel', 'koaxkabel@example.com'),
(46, 'Riz Ottó', 'rizotto@gmail.com'),
(47, 'Teás K.Anna', 'teask.anna@hotmail.com'),
(48, 'Citad Ella', 'citadella@example.com'),
(49, 'Zsíros B.Ödön', 'zsirosb.odon@ingatlan.com'),
(50, 'Techno Kolos', 'technokolos@citromail.hu'),
(51, 'Git Áron', 'gitaron@ingatlan.com'),
(52, 'Rabsz Olga', 'rabszolga@citromail.hu'),
(54, 'Winch Eszter', 'wincheszter@gmail.com'),
(55, 'Metall Ica', 'metallica@hotmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `EPULET`
--

CREATE TABLE `EPULET` (
  `id` int(11) NOT NULL,
  `epulet_szam` decimal(65,0) DEFAULT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `EPULET`
--

INSERT INTO `EPULET` (`id`, `epulet_szam`, `nev`) VALUES
(296, '17', 'Tiszabecs épület'),
(297, '2', 'Arkhangelos épület'),
(298, '1', 'Tekenye épület'),
(299, '19', 'Tarattyó épület'),
(301, '4', 'Egerfarmos épület'),
(302, '14', 'Szekrenykepuszta épület'),
(304, '11', 'Kroisbach épület'),
(305, '6', 'Gizellatanya épület'),
(306, '12', 'Zsira épület'),
(307, '2', 'Szegi épület'),
(308, '8', 'Parnis épület'),
(309, '4', 'Tekenye épület'),
(310, '11', 'Sima épület'),
(311, '10', 'Egermalom épület'),
(312, '12', 'Csemetekert épület'),
(313, '13', 'Egermalom épület'),
(314, '7', 'Nagyoros épület'),
(315, '20', 'Nagyoros épület'),
(316, '20', 'Kossuthtelep épület'),
(317, '17', 'Tekenye épület'),
(318, '3', 'Gyulapuszta épület'),
(319, '12', 'Sima épület'),
(321, '5', 'Mucsi épület'),
(322, '4', 'Zsilip épület'),
(323, '6', 'Csemetekert épület'),
(324, '4', 'Garadna épület');

-- --------------------------------------------------------

--
-- Table structure for table `INGATLAN`
--

CREATE TABLE `INGATLAN` (
  `id` int(11) NOT NULL,
  `cim` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `terulet` decimal(65,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `INGATLAN`
--

INSERT INTO `INGATLAN` (`id`, `cim`, `terulet`) VALUES
(272, 'Egerfarmos, Kossuth Lajos u. 14', '212'),
(273, 'Dabrony, Kossuth Lajos u. 16', '167'),
(274, 'Mocsa, Zrínyi u. 37', '83'),
(275, 'Csukma, Damjanich u. 4', '65'),
(276, 'Polichni, Nagy u. 27', '250'),
(277, 'Ludas, Vásárhelyi Pál u. 36', '103'),
(278, 'Szentsimon, Damjanich u. 47', '39'),
(282, 'Arkhangelos, Damjanich u. 30', '313'),
(283, 'Macsugatanya, Kelemen László u. 32', '468'),
(284, 'Dabrony, Tavasz u. 16', '241'),
(285, 'Egerfarmos, Zrínyi u. 17', '324'),
(286, 'Markscheidtanya, Kelemen László u. 43', '19'),
(288, 'Csukma, Kelemen László u. 50', '417'),
(289, 'Chios, Nagy u. 21', '108'),
(290, 'Tarattyó, Zrínyi u. 45', '18'),
(291, 'Gyulapuszta, Kossuth Lajos u. 3', '433'),
(292, 'Bothmerpuszta, Széchenyi u. 7', '382'),
(293, 'Parnis, Széchenyi u. 41', '361'),
(294, 'Velence, Vásárhelyi Pál u. 30', '31'),
(296, 'Zsira, Vásárhelyi Pál u. 23', '123'),
(297, 'Ungortanya, Kossuth Lajos u. 44', '39'),
(298, 'Nincstelentelep, Kossuth Lajos u. 3', '86'),
(299, 'Kisvadas, Kelemen László u. 22', '377'),
(301, 'Aggtelek, Vásárhelyi Pál u. 42', '152'),
(302, 'Ince, Széchenyi u. 41', '466'),
(304, 'Tiszabecs, Kossuth Lajos u. 14', '450'),
(305, 'Ungortanya, Széchenyi u. 4', '370'),
(306, 'Gizellatanya, Tavasz u. 8', '172'),
(307, 'Arkhangelos, Kelemen László u. 1', '456'),
(308, 'Gizellatanya, Kelemen László u. 25', '48'),
(309, 'Gizellatanya, Nagy u. 23', '476'),
(310, 'Kythira, Kelemen László u. 3', '185'),
(311, 'Kisvadas, Vásárhelyi Pál u. 40', '477'),
(312, 'Pusztapó, Vásárhelyi Pál u. 30', '212'),
(313, 'Szekrenykepuszta, Tavasz u. 36', '434'),
(314, 'Mucsi, Damjanich u. 7', '11'),
(315, 'Mocsa, Széchenyi u. 22', '165'),
(316, 'Zsira, Nagy u. 5', '167'),
(317, 'Kythira, Széchenyi u. 22', '392'),
(318, 'Bothmerpuszta, Damjanich u. 34', '209'),
(319, 'Bothmerpuszta, Kelemen László u. 4', '483'),
(321, 'Ludas, Tavasz u. 5', '355'),
(322, 'Dokos, Széchenyi u. 40', '416'),
(323, 'Velence, Damjanich u. 49', '245'),
(324, 'Nincstelentelep, Damjanich u. 30', '440'),
(326, 'Mocsa, Nagy u. 32', '134'),
(327, 'Bothmerpuszta, Vásárhelyi Pál u. 8', '37'),
(328, 'Tekenye, Damjanich u. 3', '200'),
(329, 'Mucsi, Kelemen László u. 39', '267'),
(330, 'Ince, Széchenyi u. 48', '99'),
(331, 'Garadna, Vásárhelyi Pál u. 6', '214'),
(332, 'Macsugatanya, Tavasz u. 38', '495'),
(334, 'Arkhangelos, Széchenyi u. 28', '207'),
(335, 'Dabrony, Kelemen László u. 27', '124'),
(336, 'Nagyoros, Vásárhelyi Pál u. 15', '383'),
(337, 'Tarattyó, Damjanich u. 21', '477'),
(338, 'Pincesor, Széchenyi u. 43', '255'),
(339, 'Zsira, Széchenyi u. 8', '409'),
(340, 'Tekenye, Damjanich u. 43', '93'),
(341, 'Lakhegy, Széchenyi u. 20', '275'),
(342, 'Nagyoros, Széchenyi u. 15', '153'),
(343, 'Pusztapó, Damjanich u. 43', '395'),
(344, 'Bothmerpuszta, Nagy u. 29', '155'),
(345, 'Tekenye, Zrínyi u. 11', '305'),
(346, 'Garadna, Damjanich u. 7', '231'),
(347, 'Csukma, Zrínyi u. 43', '227'),
(348, 'Lakhegy, Kelemen László u. 39', '298'),
(349, 'Dabrony, Kossuth Lajos u. 6', '235'),
(350, 'Mariskapuszta, Nagy u. 12', '288'),
(351, 'Tarattyó, Kossuth Lajos u. 8', '226'),
(352, 'Polichni, Kossuth Lajos u. 31', '54'),
(353, 'Pusztapó, Tavasz u. 16', '200'),
(354, 'Szentsimon, Kelemen László u. 10', '437'),
(355, 'Pelion, Tavasz u. 24', '442'),
(356, 'Kisvadas, Kossuth Lajos u. 37', '238'),
(357, 'Tekenye, Nagy u. 40', '492'),
(358, 'Tarattyó, Kossuth Lajos u. 46', '203'),
(359, 'Tekenye, Nagy u. 2', '90'),
(360, 'Gizellatanya, Széchenyi u. 33', '277'),
(361, 'Kroisbach, Damjanich u. 34', '267'),
(362, 'Csukma, Nagy u. 22', '36'),
(363, 'Arkhangelos, Zrínyi u. 21', '474'),
(364, 'Gizellatanya, Kossuth Lajos u. 13', '63'),
(365, 'Tekenye, Kelemen László u. 19', '75'),
(366, 'Csukma, Zrínyi u. 48', '46'),
(367, 'Bothmerpuszta, Nagy u. 31', '484'),
(368, 'Velence, Zrínyi u. 31', '275'),
(369, 'Zsilip, Kossuth Lajos u. 3', '239'),
(370, 'Gyulapuszta, Kelemen László u. 15', '412'),
(371, 'Zsilip, Széchenyi u. 16', '15'),
(372, 'Arkhangelos, Nagy u. 18', '147'),
(373, 'Nagyoros, Kossuth Lajos u. 45', '41'),
(374, 'Dokos, Kelemen László u. 33', '389'),
(375, 'Mocsa, Zrínyi u. 37', '99999');

-- --------------------------------------------------------

--
-- Table structure for table `LAKAS`
--

CREATE TABLE `LAKAS` (
  `id` int(11) NOT NULL,
  `lakas_szam` decimal(65,0) DEFAULT NULL,
  `helysegek_szama` decimal(65,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `LAKAS`
--

INSERT INTO `LAKAS` (`id`, `lakas_szam`, `helysegek_szama`) VALUES
(326, '31', '16'),
(327, '37', '14'),
(328, '12', '13'),
(329, '6', '10'),
(330, '33', '16'),
(331, '28', '8'),
(332, '38', '14'),
(334, '10', '17'),
(335, '25', '14'),
(336, '32', '10'),
(337, '35', '3'),
(338, '5', '20'),
(339, '21', '8'),
(340, '8', '2'),
(341, '29', '12'),
(342, '20', '18'),
(343, '24', '2'),
(344, '14', '9'),
(345, '13', '5'),
(346, '14', '11'),
(347, '30', '14'),
(348, '18', '18'),
(349, '35', '7'),
(350, '15', '13'),
(351, '37', '5'),
(352, '3', '3'),
(353, '13', '17'),
(354, '23', '13'),
(355, '29', '6'),
(356, '26', '8'),
(357, '4', '5'),
(358, '1', '4'),
(359, '7', '8'),
(360, '21', '18'),
(361, '11', '17'),
(362, '16', '18'),
(363, '34', '6'),
(364, '7', '7'),
(365, '26', '8'),
(366, '9', '15'),
(367, '23', '8'),
(368, '26', '7'),
(369, '6', '11'),
(370, '10', '12'),
(371, '13', '15'),
(372, '39', '12'),
(373, '36', '11'),
(374, '23', '11');

-- --------------------------------------------------------

--
-- Table structure for table `TELEK`
--

CREATE TABLE `TELEK` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `TELEK`
--

INSERT INTO `TELEK` (`id`, `nev`) VALUES
(272, 'Ince ház'),
(273, 'Nagyoros porta'),
(274, 'Madocsa telek'),
(275, 'Szekrenykepuszta telek'),
(276, 'Csukma ház'),
(277, 'Egermalom telek'),
(278, 'Parnis porta'),
(282, 'Kossuthtelep telek'),
(283, 'Egermalom telek'),
(284, 'Simontornya telek'),
(285, 'Mariskapuszta porta'),
(286, 'Egerfarmos porta'),
(288, 'Mocsa ház'),
(289, 'Kroisbach ház'),
(290, 'Chios porta'),
(291, 'Nincstelentelep ház'),
(292, 'Velence telek'),
(293, 'Csukma ház'),
(294, 'Aggtelek porta'),
(375, 'Madocsa telek');

-- --------------------------------------------------------

--
-- Table structure for table `TULAJDONOSA`
--

CREATE TABLE `TULAJDONOSA` (
  `uid` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `mikortol` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `TULAJDONOSA`
--

INSERT INTO `TULAJDONOSA` (`uid`, `id`, `mikortol`) VALUES
(45, 272, NULL),
(45, 273, NULL),
(45, 312, '1890-10-14'),
(45, 357, '2021-10-02'),
(46, 272, NULL),
(47, 272, NULL),
(47, 278, NULL),
(48, 272, '1999-01-02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `BERLOJE`
--
ALTER TABLE `BERLOJE`
  ADD PRIMARY KEY (`uid`,`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `EMBER`
--
ALTER TABLE `EMBER`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `EPULET`
--
ALTER TABLE `EPULET`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `INGATLAN`
--
ALTER TABLE `INGATLAN`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `LAKAS`
--
ALTER TABLE `LAKAS`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `TELEK`
--
ALTER TABLE `TELEK`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `TULAJDONOSA`
--
ALTER TABLE `TULAJDONOSA`
  ADD PRIMARY KEY (`uid`,`id`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `EMBER`
--
ALTER TABLE `EMBER`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `INGATLAN`
--
ALTER TABLE `INGATLAN`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=376;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `BERLOJE`
--
ALTER TABLE `BERLOJE`
  ADD CONSTRAINT `BERLOJE_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `EMBER` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `BERLOJE_ibfk_2` FOREIGN KEY (`id`) REFERENCES `INGATLAN` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `EPULET`
--
ALTER TABLE `EPULET`
  ADD CONSTRAINT `EPULET_ibfk_1` FOREIGN KEY (`id`) REFERENCES `INGATLAN` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `LAKAS`
--
ALTER TABLE `LAKAS`
  ADD CONSTRAINT `LAKAS_ibfk_1` FOREIGN KEY (`id`) REFERENCES `INGATLAN` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `TELEK`
--
ALTER TABLE `TELEK`
  ADD CONSTRAINT `TELEK_ibfk_1` FOREIGN KEY (`id`) REFERENCES `INGATLAN` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `TULAJDONOSA`
--
ALTER TABLE `TULAJDONOSA`
  ADD CONSTRAINT `TULAJDONOSA_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `EMBER` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `TULAJDONOSA_ibfk_2` FOREIGN KEY (`id`) REFERENCES `INGATLAN` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
