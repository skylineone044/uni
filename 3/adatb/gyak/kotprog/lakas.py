from tkinter import ttk
from tkinter import messagebox
import mysql.connector as mysql

from common import *


def id_exists(id_):
    if id_valid(id_):
        con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
        cursor = con.cursor()
        cursor.execute(f"select * "
                       f"from LAKAS "
                       f"where id={id_}")
        rows = cursor.fetchall()
        print(f"{id_=}: {rows}")
        con.close()
        if rows != []:
            return True
        else:
            tk.messagebox.showerror("Error", "Nincs lakas ilyen id-vel!")
            return False
    else:
        return False


def get(id_text_field, cim_text_field, terulet_text_field, lakas_szam_text_field, helysegek_szama_text_field):
    print(f"getting lakas: {id_text_field.get()=}, {cim_text_field.get()=}, {terulet_text_field.get()=}, {helysegek_szama_text_field.get()=}, {lakas_szam_text_field=}")
    if id_text_field.get() == "":
        tk.messagebox.showerror("Error", "ID mező kötelező")
    elif not id_exists(id_text_field.get()):
        pass
    else:
        if id_exists(id_text_field.get()):
            con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
            cursor = con.cursor()
            cursor.execute(f"select INGATLAN.id, INGATLAN.cim, INGATLAN.terulet, LAKAS.lakas_szam, LAKAS.helysegek_szama "
                           f"from INGATLAN, LAKAS "
                           f"where LAKAS.id={id_text_field.get()} AND LAKAS.id = INGATLAN.id")
            rows = cursor.fetchmany(1)

            clear_fields(id_text_field, cim_text_field, terulet_text_field, lakas_szam_text_field, helysegek_szama_text_field)
            for row in rows:
                id_text_field.insert(0, row[0])
                cim_text_field.insert(0, row[1])
                terulet_text_field.insert(0, row[2])
                lakas_szam_text_field.insert(0, row[3])
                helysegek_szama_text_field.insert(0, row[4])
            con.close()
        else:
            tk.messagebox.showerror("Error", "Nincs lakas ilyen id-vel!")


def get_all():
    con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
    cursor = con.cursor()
    cursor.execute( f"select INGATLAN.id, INGATLAN.cim, INGATLAN.terulet, LAKAS.lakas_szam, LAKAS.helysegek_szama "
                    f"from INGATLAN, LAKAS "
                    f"where LAKAS.id = INGATLAN.id")
    rows = cursor.fetchall()
    con.close()

    return rows


def add(id_text_field, cim_text_field, terulet_text_field, lakas_szam_text_field, helysegek_szama_text_field):
    if  cim_text_field.get() == "" or terulet_text_field.get() == "" or helysegek_szama_text_field.get() == "" or lakas_szam_text_field.get() == "":
        tk.messagebox.showerror("Error", "A cím, terület és név megadása kötelező!")
    elif not number_valid(terulet_text_field.get()):
        tk.messagebox.showerror("Error", "Invalid terulet!")
    elif not number_valid(lakas_szam_text_field.get()):
        tk.messagebox.showerror("Error", "Invalid lakas_szam!")
    else:
        con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
        cursor = con.cursor()

        cmd_add_ingatlan = f'insert into INGATLAN ' \
                           f'values(NULL,"{cim_text_field.get()}","{terulet_text_field.get()}")'
        print(f"adding ingatlan:\n    {cmd_add_ingatlan}")
        cursor.execute(cmd_add_ingatlan)
        cursor.execute('commit')

        cmd_add_lakas = f'insert into LAKAS ' \
                        f'values((select  LAST_INSERT_ID() from INGATLAN LIMIT 1),"{lakas_szam_text_field.get()}", "{helysegek_szama_text_field.get()}")'
        print(f"adding lakas:\n    {cmd_add_lakas}")
        cursor.execute(cmd_add_lakas)
        cursor.execute('commit')

        clear_fields(id_text_field, cim_text_field, terulet_text_field, lakas_szam_text_field, helysegek_szama_text_field)
        tk.messagebox.showinfo('Info', 'Sikeres beszúrás')
        con.close()


def edit(id_text_field, cim_text_field, terulet_text_field, lakas_szam_text_field, helysegek_szama_text_field, table):
    if id_text_field.get() == "" or cim_text_field.get() == "" or terulet_text_field.get() == "" or helysegek_szama_text_field.get() == "" or lakas_szam_text_field.get() == "":
        tk.messagebox.showerror("Error", "Minden mező kötelező")
    elif not id_exists(id_text_field.get()):
        pass
    elif not number_valid(terulet_text_field.get()):
        tk.messagebox.showerror("Error", "Invalid terulet!")
    elif not number_valid(lakas_szam_text_field.get()):
        tk.messagebox.showerror("Error", "Invalid lakas_szam!")
    else:
        con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
        cursor = con.cursor()

        cmd_update_ingatlan = f'update INGATLAN set cim="{cim_text_field.get()}", terulet="{terulet_text_field.get()}" where id={id_text_field.get()}'
        cmd_update_lakas = f'update LAKAS set lakas_szam="{lakas_szam_text_field.get()}", helysegek_szama="{helysegek_szama_text_field.get()}" where id={id_text_field.get()}'
        cursor.execute(cmd_update_ingatlan)
        cursor.execute(cmd_update_lakas)

        cursor.execute('commit')

        clear_fields(id_text_field, cim_text_field, terulet_text_field, lakas_szam_text_field, helysegek_szama_text_field, table)
        tk.messagebox.showinfo('Info', 'Sikeres frissítés')
        con.close()


def delete(id_text_field, cim_text_field, terulet_text_field, lakas_szam_text_field, helysegek_szama_text_field):
    if id_text_field.get() == "":
        tk.messagebox.showerror("Error", "ID mező kötelező")
    elif not id_exists(id_text_field.get()):
        pass
    else:
        con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
        cursor = con.cursor()

        cmd_delete_ingatlan = f"delete from INGATLAN " \
                              f"where id={id_text_field.get()}"
        print(f"deleting ingatlan: {cmd_delete_ingatlan}")
        cursor.execute(cmd_delete_ingatlan)
        cursor.execute("commit")

        clear_fields(id_text_field, cim_text_field, terulet_text_field, lakas_szam_text_field, helysegek_szama_text_field)
        tk.messagebox.showinfo('Info', 'Törlés végrehajtva')
        con.close()


def clear_fields(id_text_field, cim_text_field, terulet_text_field, lakas_szam_text_field, helysegek_szama_text_field, table=None):
    id_text_field.delete(0, 'end')
    cim_text_field.delete(0, 'end')
    terulet_text_field.delete(0, 'end')
    lakas_szam_text_field.delete(0, 'end')
    helysegek_szama_text_field.delete(0, 'end')

    if table is not None:
        table_deselect(table)


def table_select(event, table, id_text_field, cim_text_field, terulet_text_field, lakas_szam_text_field, helysegek_szama_text_field):
    current_item = table.focus()
    focus = table.item(current_item)
    print(f"selected: {focus}")

    if focus["values"] != "":
        clear_fields(id_text_field, cim_text_field, terulet_text_field, lakas_szam_text_field, helysegek_szama_text_field)

        id_text_field.insert(0, focus["values"][0])
        cim_text_field.insert(0, focus["values"][1])
        terulet_text_field.insert(0, focus["values"][2])
        lakas_szam_text_field.insert(0, focus["values"][3])
        helysegek_szama_text_field.insert(0, focus["values"][4])


def tab(tab_parent):
    tab_frame = tk.Frame(tab_parent)

    entry_frame = tk.Frame(tab_frame)
    fields_frame = tk.Frame(entry_frame)

    tk.Label(fields_frame, text="id: ").grid(row=0, column=0)
    id_text_field = tk.Entry(fields_frame)
    id_text_field.grid(row=0, column=1)

    tk.Label(fields_frame, text="cim: ").grid(row=1, column=0)
    cim_text_field = tk.Entry(fields_frame)
    cim_text_field.grid(row=1, column=1)

    tk.Label(fields_frame, text="terulet: ").grid(row=2, column=0)
    terulet_text_field = tk.Entry(fields_frame)
    terulet_text_field.grid(row=2, column=1)

    tk.Label(fields_frame, text="lakas szam: ").grid(row=3, column=0)
    lakas_szam_text_field = tk.Entry(fields_frame)
    lakas_szam_text_field.grid(row=3, column=1)

    tk.Label(fields_frame, text="helysegek szama: ").grid(row=4, column=0)
    helysegek_szama_text_field = tk.Entry(fields_frame)
    helysegek_szama_text_field.grid(row=4, column=1)

    fields_frame.grid(row=0, column=0)

    button_frame = tk.Frame(entry_frame)

    add_button = tk.Button(button_frame, text=" + Add", command=lambda: add(id_text_field, cim_text_field, terulet_text_field, lakas_szam_text_field, helysegek_szama_text_field))
    add_button.grid(row=0, column=0)

    delete_button = tk.Button(button_frame, text=" X Delete", command=lambda: delete(id_text_field, cim_text_field, terulet_text_field, lakas_szam_text_field, helysegek_szama_text_field))
    delete_button.grid(row=0, column=1)

    modify_button = tk.Button(button_frame, text=" ~ Modify", command=lambda: edit(id_text_field, cim_text_field, terulet_text_field, lakas_szam_text_field, helysegek_szama_text_field,  table))
    modify_button.grid(row=0, column=2)

    get_button = tk.Button(button_frame, text=" v Get", command=lambda: get(id_text_field, cim_text_field, terulet_text_field, lakas_szam_text_field, helysegek_szama_text_field))
    get_button.grid(row=0, column=3)

    clead_fields_button = tk.Button(button_frame, text=" ^ clear", command=lambda: clear_fields(id_text_field, cim_text_field, terulet_text_field, lakas_szam_text_field, helysegek_szama_text_field, table))
    clead_fields_button.grid(row=0, column=4)

    button_frame.grid(row=3, column=0)

    entry_frame.grid(row=0, column=0)

    table_frame = tk.Frame(tab_frame)
    scrollbar_v = tk.Scrollbar(table_frame)
    scrollbar_v.pack(side=tk.RIGHT, fill=tk.Y)
    scrollbar_h = tk.Scrollbar(table_frame, orient="horizontal")
    scrollbar_h.pack(side=tk.BOTTOM, fill=tk.X)

    table = ttk.Treeview(table_frame, yscrollcommand=scrollbar_v.set, xscrollcommand=scrollbar_h.set)
    table.bind('<ButtonRelease-1>',
               lambda event: table_select(event, table, id_text_field, cim_text_field, terulet_text_field, lakas_szam_text_field, helysegek_szama_text_field))
    table.bind('<Delete>', lambda event: delete(id_text_field, cim_text_field, terulet_text_field, lakas_szam_text_field, helysegek_szama_text_field))
    table.pack()

    scrollbar_v.config(command=table.yview)
    scrollbar_h.config(command=table.xview)

    table["columns"] = ("id", "cim", "terulet", "lakas_szam", "helysegek_szama")
    table_column_width = 180

    table.column("#0", width=0, stretch=tk.NO)
    table.column("id", width=int(table_column_width / 2), anchor=tk.CENTER)
    table.column("cim", width=int(table_column_width * 1.5), anchor=tk.CENTER)
    table.column("terulet", width=int(table_column_width / 2), anchor=tk.CENTER)
    table.column("lakas_szam", width=int(table_column_width / 2), anchor=tk.CENTER)
    table.column("helysegek_szama", width=int(table_column_width / 2), anchor=tk.CENTER)

    table.heading("#0", text="", anchor=tk.CENTER)
    table.heading("id", text="Ingatlan id", anchor=tk.CENTER)
    table.heading("cim", text="Cím", anchor=tk.CENTER)
    table.heading("terulet", text="Terület", anchor=tk.CENTER)
    table.heading("lakas_szam", text="Lakás szám", anchor=tk.CENTER)
    table.heading("helysegek_szama", text="Helységek száma", anchor=tk.CENTER)

    table.pack()
    table_frame.grid(row=1, column=0)

    update_table(table_frame, table, 0, get_all)

    return tab_frame
