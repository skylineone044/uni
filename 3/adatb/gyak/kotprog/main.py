from tkinter import ttk

from common import *

import ember
import telek
import epulet
import lakas

import ember_ingatlanjai
import ember_berelt_ingatlanjai

import stats


if __name__ == "__main__":
    root = tk.Tk()
    style = ttk.Style(root)

    root.geometry("1280x720")
    root.title("Ingatlan adatbazis")

    tab_parent = ttk.Notebook(root)

    embertab = ember.tab(tab_parent)
    tab_parent.add(embertab, text="Ember")

    telektab = telek.tab(tab_parent)
    tab_parent.add(telektab, text="Telek")

    epulettab = epulet.tab(tab_parent)
    tab_parent.add(epulettab, text="Épület")

    lakastab = lakas.tab(tab_parent)
    tab_parent.add(lakastab, text="Lakás")

    emebr_ingatlanjai_tab = ember_ingatlanjai.tab(tab_parent)
    tab_parent.add(emebr_ingatlanjai_tab, text="Ember Ingatlanjai")

    ember_berelt_ingatlanjai_tab = ember_berelt_ingatlanjai.tab(tab_parent)
    tab_parent.add(ember_berelt_ingatlanjai_tab, text="Ember Bérelt Ingatlanjai")

    statstab = stats.tab(tab_parent)
    tab_parent.add(statstab, text="Statisztikák")

    tab_parent.pack(expand=1, fill="both")

    root.mainloop()
