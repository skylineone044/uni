from tkinter import ttk
import mysql.connector as mysql

from common import *


def get_per_person_summary():
    con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
    cursor = con.cursor()
    cursor.execute("SELECT "
                   "EMBER.uid, "
                   "EMBER.nev, "
                   "(SELECT COUNT(*) FROM TULAJDONOSA WHERE EMBER.uid = TULAJDONOSA.uid) as tulajdonCount, "
                   "(SELECT COUNT(*)     FROM BERLOJE     WHERE EMBER.uid = BERLOJE.uid) as bereltCount "
                   "FROM EMBER, TULAJDONOSA, BERLOJE "
                   "GROUP BY EMBER.uid "
                   "HAVING tulajdonCount > 0 OR bereltCount > 0 "
                   )
    rows = cursor.fetchall()
    con.close()

    return rows

def panel_person_summary(tab_frame):
    panel_frame = tk.Frame(tab_frame)
    entry_frame = tk.Frame(panel_frame)
    fields_frame = tk.Frame(entry_frame)

    tk.Label(fields_frame, text="Emberenkénti összesítés").grid(row=0, column=0)

    fields_frame.grid(row=0, column=0)

    entry_frame.grid(row=0, column=0)

    table_frame = tk.Frame(panel_frame)
    scrollbar_v = tk.Scrollbar(table_frame)
    scrollbar_v.pack(side=tk.RIGHT, fill=tk.Y)
    scrollbar_h = tk.Scrollbar(table_frame, orient="horizontal")
    scrollbar_h.pack(side=tk.BOTTOM, fill=tk.X)

    table = ttk.Treeview(table_frame, yscrollcommand=scrollbar_v.set, xscrollcommand=scrollbar_h.set)
    table.pack()

    scrollbar_v.config(command=table.yview)
    scrollbar_h.config(command=table.xview)

    table["columns"] = ("uid", "nev", "ingatlanjai", "berlesei")
    table_column_width = 200

    table.column("#0", width=0, stretch=tk.NO)
    table.column("uid", width=int(table_column_width / 2), anchor=tk.CENTER)
    table.column("nev", width=int(table_column_width * 1.5), anchor=tk.CENTER)
    table.column("ingatlanjai", width=int(table_column_width / 3), anchor=tk.CENTER)
    table.column("berlesei", width=int(table_column_width / 3), anchor=tk.CENTER)

    table.heading("#0", text="", anchor=tk.CENTER)
    table.heading("uid", text="Ember id", anchor=tk.CENTER)
    table.heading("nev", text="Név", anchor=tk.CENTER)
    table.heading("ingatlanjai", text="Ingatlanjai", anchor=tk.CENTER)
    table.heading("berlesei", text="Bérlései", anchor=tk.CENTER)

    table.pack()
    table_frame.grid(row=1, column=0)
    update_table(table_frame, table, 0, get_per_person_summary)

    panel_frame.pack()



def get_per_ingatlan_ownership_and_berlesek():
    con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
    cursor = con.cursor()
    cursor.execute("SELECT "
                   "INGATLAN.id, "
                   "INGATLAN.cim, "
                   "(SELECT COUNT(EMBER.nev) FROM EMBER, TULAJDONOSA WHERE EMBER.uid = TULAJDONOSA.uid AND TULAJDONOSA.id = INGATLAN.id) as tulajdonosai, "
                   "(SELECT COUNT(EMBER.nev) FROM EMBER, BERLOJE WHERE EMBER.uid = BERLOJE.uid AND BERLOJE.id = INGATLAN.id) as berloi "
                   "FROM INGATLAN, EMBER, TULAJDONOSA, BERLOJE "
                   "GROUP BY INGATLAN.id "
                   "HAVING tulajdonosai > 0 OR berloi > 0 "
                   )
    rows = cursor.fetchall()
    con.close()

    return rows

def panel_per_ingatlan_ownership_and_berlesek(tab_frame):
    panel_frame = tk.Frame(tab_frame)

    entry_frame = tk.Frame(panel_frame)
    fields_frame = tk.Frame(entry_frame)

    tk.Label(fields_frame, text="Ingatlanonkénti tulajdonosok és bérlők összesítése").grid(row=0, column=0)
    fields_frame.grid(row=0, column=0)
    entry_frame.grid(row=0, column=0)

    table_frame = tk.Frame(panel_frame)
    scrollbar_v = tk.Scrollbar(table_frame)
    scrollbar_v.pack(side=tk.RIGHT, fill=tk.Y)
    scrollbar_h = tk.Scrollbar(table_frame, orient="horizontal")
    scrollbar_h.pack(side=tk.BOTTOM, fill=tk.X)

    table = ttk.Treeview(table_frame, yscrollcommand=scrollbar_v.set, xscrollcommand=scrollbar_h.set)
    table.pack()

    scrollbar_v.config(command=table.yview)
    scrollbar_h.config(command=table.xview)

    table["columns"] = ("id", "cim", "tulajdonosok", "berlok")
    table_column_width = 200

    table.column("#0", width=0, stretch=tk.NO)
    table.column("id", width=int(table_column_width / 2), anchor=tk.CENTER)
    table.column("cim", width=int(table_column_width * 1.5), anchor=tk.CENTER)
    table.column("tulajdonosok", width=int(table_column_width / 3), anchor=tk.CENTER)
    table.column("berlok", width=int(table_column_width / 3), anchor=tk.CENTER)

    table.heading("#0", text="", anchor=tk.CENTER)
    table.heading("id", text="Ingatlan id", anchor=tk.CENTER)
    table.heading("cim", text="Cím", anchor=tk.CENTER)
    table.heading("tulajdonosok", text="Tulajdonokok", anchor=tk.CENTER)
    table.heading("berlok", text="Bérlők", anchor=tk.CENTER)

    table.pack()
    table_frame.grid(row=1, column=0)
    update_table(table_frame, table, 0, get_per_ingatlan_ownership_and_berlesek)

    panel_frame.pack()

def get_avg_berlog_szama():
    con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
    cursor = con.cursor()
    cursor.execute("SELECT AVG(berlokSzama) "
                   "FROM (SELECT COUNT(*) as berlokSzama FROM BERLOJE GROUP BY id) as counts "
                   )
    rows = cursor.fetchall()[0][0]
    con.close()

    return rows

def update_avg_label(parent_frame, label, get_new):
    label["text"] = f"Egy kibérelt ingatlan átkagos bérlőinek száma: {get_new()}"

    parent_frame.after(1000, update_avg_label, parent_frame, label, get_new)


def panel_atlagos_berlok(tab_frame):
    panel_frame = tk.Frame(tab_frame)

    entry_frame = tk.Frame(panel_frame)
    fields_frame = tk.Frame(entry_frame)

    avg_label = tk.Label(fields_frame, text="")
    avg_label.grid(row=0, column=0)
    fields_frame.grid(row=0, column=0)
    entry_frame.grid(row=0, column=0)

    update_avg_label(fields_frame, avg_label, get_avg_berlog_szama)

    panel_frame.pack()


def get_max_berlo():
    con = mysql.connect(host=DBHOST, user=DBUSER, password=DBPASS, database=DBNAME)
    cursor = con.cursor()
    cursor.execute("SELECT EMBER.nev, COUNT(*) "
                   "FROM EMBER, BERLOJE "
                   "WHERE EMBER.uid = BERLOJE.uid "
                   "GROUP BY EMBER.uid "
                   "ORDER BY COUNT(*) DESC "
                   "LIMIT 1 "
                   )
    rows = cursor.fetchall()[0]
    con.close()

    return rows


def update_max_berlo_label(parent_frame, label, get_new):
    max_berlo = get_new()
    label["text"] = f"Az ember aki a legtöbb ingatlant bérli jelenleg: {max_berlo[0]}: {max_berlo[1]} ingatlant bérel"

    parent_frame.after(1000, update_max_berlo_label, parent_frame, label, get_new)


def panel_max_berlo(tab_frame):
    panel_frame = tk.Frame(tab_frame)

    entry_frame = tk.Frame(panel_frame)
    fields_frame = tk.Frame(entry_frame)

    max_label = tk.Label(fields_frame, text="")
    max_label.grid(row=0, column=0)
    fields_frame.grid(row=0, column=0)
    entry_frame.grid(row=0, column=0)

    update_max_berlo_label(fields_frame, max_label, get_max_berlo)

    panel_frame.pack()


def tab(tab_parent):
    tab_frame = tk.Frame(tab_parent)

    panel_person_summary(tab_frame)
    panel_per_ingatlan_ownership_and_berlesek(tab_frame)
    panel_atlagos_berlok(tab_frame)
    panel_max_berlo(tab_frame)

    return tab_frame
