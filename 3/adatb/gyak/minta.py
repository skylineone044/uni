import tkinter as tk
from tkinter import messagebox as MessageBox
import mysql.connector as mysql

def get():
    if e_id.get() == "":
        MessageBox.showinfo("Info", "ID mező kötelező")
    else:
        con = mysql.connect(host=dbhost, user=dbuser, password=dbpass, database=dbname)
        cursor = con.cursor()
        cursor.execute('select * from nevtel where id={}'.format(e_id.get()))
        rows = cursor.fetchmany(1)
        for row in rows:
            e_name.insert(0, row[1])
            e_phone.insert(0, row[2])
        con.close()
        show()


def insert():
    if e_id.get() == "" or e_name.get() == "" or e_phone.get() == "":
        MessageBox.showinfo("Info", "Minden mező kötelező")
    else:
        con = mysql.connect(host=dbhost, user=dbuser, password=dbpass, database=dbname)
        cursor = con.cursor()
        cursor.execute('insert into nevtel values("{}","{}","{}")'.format(e_id.get(), e_name.get(), e_phone.get()))
        cursor.execute('commit')
        e_id.delete(0, 'end')
        e_name.delete(0, 'end')
        e_phone.delete(0, 'end')
        MessageBox.showinfo('Info', 'Sikeres beszúrás')
        con.close()
        show()


def delete():
    if e_id.get() == "":
        MessageBox.showinfo("Info", "ID mező kötelező")
    else:
        con = mysql.connect(host=dbhost, user=dbuser, password=dbpass, database=dbname)
        cursor = con.cursor()
        cursor.execute('delete from nevtel where id="{}"'.format(e_id.get()))
        cursor.execute('commit')
        e_id.delete(0, 'end')
        e_name.delete(0, 'end')
        e_phone.delete(0, 'end')
        MessageBox.showinfo('Info', 'Törlés végrehajtva')
        con.close()
        show()


def update():
    if e_id.get() == "" or e_name.get() == "" or e_phone.get() == "":
        MessageBox.showinfo("Info", "Minden mező kötelező")
    else:
        con = mysql.connect(host=dbhost, user=dbuser, password=dbpass, database=dbname)
        cursor = con.cursor()
        cursor.execute('update nevtel set nev="{}", tel="{}" where id={}'.format(e_name.get(), e_phone.get(), e_id.get()))
        cursor.execute('commit')
        e_id.delete(0, 'end')
        e_name.delete(0, 'end')
        e_phone.delete(0, 'end')
        MessageBox.showinfo('Info', 'Sikeres frissítés')
        con.close()
        show()


def show():
    con = mysql.connect(host=dbhost, user=dbuser, password=dbpass, database=dbname)
    cursor = con.cursor()
    cursor.execute('select * from nevtel')
    rows = cursor.fetchall()
    list.delete(0, list.size())
    for row in rows:
        insertData = '{}  {}'.format(row[0], row[1])
        list.insert(list.size() + 1, insertData)
    con.close()


if __name__ == '__main__':
    dbhost = 'localhost'
    dbuser = 'test'
    dbpass = 'test123'
    dbname = 'test'

    root = tk.Tk()
    root.geometry("600x300")
    root.title("SQL teszt")

    id = tk.Label(root, text='Azonosító', font=('bold', 10))
    id.place(x=20, y=30)

    name = tk.Label(root, text='Név', font=('bold', 10))
    name.place(x=20, y=60)

    phone = tk.Label(root, text='Telefon', font=('bold', 10))
    phone.place(x=20, y=90)

    e_id = tk.Entry()
    e_id.place(x=150, y=30)

    e_name = tk.Entry()
    e_name.place(x=150, y=60)

    e_phone = tk.Entry()
    e_phone.place(x=150, y=90)

    insert_button = tk.Button(root, text="Beszúr", font=('italic', 10), bg="white", command=insert)
    insert_button.place(x=20, y=140)

    update_button = tk.Button(root, text="Frissít", font=('italic', 10), bg="white", command=update)
    update_button.place(x=80, y=140)

    delete_button = tk.Button(root, text="Töröl", font=('italic', 10), bg="white", command=delete)
    delete_button.place(x=140, y=140)

    get_button = tk.Button(root, text="Lekér", font=('italic', 10), bg="white", command=get)
    get_button.place(x=190, y=140)

    list = tk.Listbox(root)
    list.place(x=290, y=30)

    show()
    root.mainloop()