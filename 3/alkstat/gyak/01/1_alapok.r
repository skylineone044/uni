# encoding: UTF8

# munkakönyvtár beállítása

setwd("/home/skyline/git/uni/3/alkstat/gyak/01/adatok/")
getwd()
dir()

# értékadás és kiíratás

assign("v",5)
v=5
v<-5

v
print(v)

typeof(v)

v=pi
v="pi"
v="hello world!"
v=TRUE
v=(5+2)^8*3
v=8+2i

# vektor, faktor

v1=c(5,2,1,5,2)  # vektor létrehozása
v2=c(1,2,3,4,5)

v1+2 # (+,-,*,/,^) komponensenkénti műveletek
v1^v2 # (+,-,*,/,^) komponensenkénti műveletek

sum(v1)
mean(v1)
min(v1)

typeof(v1)
class(v1)
str(v1)
summary(v1)

f1=factor(v1) # vektor faktorrá konvertálása
str(f1)
summary(f1)

v3=c("kék", "zöld", "kék")
str(v3)
summary(v3)

f3=factor(v3)
str(f3)
summary(f3)

# szűrés
bool=c(T, T, F, T, T) # bool vektor
v1[bool]
v1[v1>1]

# adatfájl beolvasás

load("data.rda")

x
data$x

# Beépített adatbázis:

airLine = data.frame(AirPassengers)
?AirPassengers
 
# fájl import

emberek <- read.table("emberek.txt", header=TRUE, sep="", na.strings="NA", dec=",", strip.white=TRUE, stringsAsFactors=TRUE)
kerdoiv <- read.table("kerdoiv.csv", header=TRUE, sep=";", na.strings="", dec=",", strip.white=TRUE, stringsAsFactors=TRUE, fileEncoding = "iso-8859-2")
kerdoiv2 <- read.csv2("kerdoiv.csv", dec=",", na.strings="", fileEncoding = "iso-8859-2") # 

library(foreign, pos=4)
brain <- read.spss("brain.sav", use.value.labels=TRUE, max.value.labels=Inf, to.data.frame=TRUE) # SPSS adatfájl

library("readxl")
autok <- read_excel("autok.xls") 

# adathalmaz csatolása, lecsatolása

Gender
brain$Gender
attach(brain)
Gender
detach(brain)

# hiányzó adatok kihagyása

v1=Weight
summary(v1)
length(v1)
v2=v1[!is.na(v1)]
length(v2)
na.omit(brain)

# adattábla mentése

write.table("brain", file="brain.txt") # szövegfájlba
save("brain", file="brain.rda") # R adatfájlba