# encoding: UTF8
# v1, v2 numerikus vektor, kat faktor


# Egymintás t: 
setwd("/home/skyline/git/uni/3/alkstat/gyak/06_gyakorlas/adatok/")

library(foreign, pos=4)
brain <- read.spss("brain.sav", use.value.labels=TRUE, max.value.labels=Inf, to.data.frame=TRUE) # SPSS adatfájl
attach(brain)

v1 = VIQ
v2 = PIQ
kat = factor(Gender)
table(kat)

t.test(v1, alternative='two.sided', mu=100, conf.level=0.95)

# ha a p value nagyob mint alfa (1 - conf.level) akkor jo a null hipotezis
# ha kisebb, akkor elvetjuk a null hipotezist

# egyes argumentumok külön:
t.test(v1)$estimate # becslés
t.test(v1, conf.level=0.95)$conf.int # konfidencia intervallum
t.test(v1, alternative='two.sided', mu=100)$statistic # próbastatisztika
t.test(v1, alternative='two.sided', mu=100)$p.value # p-érték



# Páros t: 
# pl elotte - utana ==> nem fuggetlenek a mintak

# ketmintas t:
# pl kezelt - kontrol csoport ==> fuggetlenek a mintak

t.test(v1, v2, conf.level=0.95, paired=TRUE)


# F próba:

var.test(VIQ ~ kat) 
# p-value nagyobb .05 nal, elfogadjuk a nullhip-et: a szoras-homogenitas fennall, lehet ketmintas t-probat csinalni

# Kétmintás t: 

t.test(VIQ~kat, conf.level=0.95, var.equal=TRUE) # v1 folytonos, kateg 2 értékű változó
# p-val nagyob  .05nal, elfogadjuk a nullhipotezist

# Welch próba (nem feltétlen egyenlő szórás esetén):
t.test(v1~kat, conf.level=0.95) 


#  feltételellenőrzés

bartlett.test(v1~kat) # szórásteszt 
# library(car)
# leveneTest(v1, kat, center=mean)

# várható érték tesztek

oneway.test(v1 ~ kat, var.equal = TRUE) # egyszempontos ANOVA teszt
oneway.test(v1 ~ kat, var.equal = FALSE) # Welch-próba általánosítása több csoportra

# Pearson korrelációteszt: 

cor.test(v1, v2)


# hipotezisvizsgalat
# 1. hipotezisek: H0 - nullhipotezis
#                 H1 - alternative hipotezis
# 2. proba kivalasztasa
# 3. probastatisztika kivalasztasa
# 4. kritikus ertek
# 5. Dontes: p-ertek alapjan elfogadjuk vagy elvetjuk a nullhipotezist

# feladatok
# Konfidencia intervallum, paraméteres próbák / 1
mean(FSIQ)
t.test(FSIQ)
t.test(FSIQ, conf.level = .99)
t.test(FSIQ, alternative = 'two.sided', mu=100, conf.level = .95)
# p-value = 0.001077 az kisebb mint .05, ezert elvetjuk a nullhipotezist

t.test(FSIQ, alternative = 'two.sided', mu=110, conf.level = .95)
# p-value = 0.3705 nagyobb mint .05, tehaz elfogadjuk a nullhipotezist


# Konfidencia intervallum, paraméteres próbák / 2
mean(VIQ - PIQ)
t.test(VIQ, PIQ, conf.level = .95, paired = TRUE)
# p-value = 0.5891 nagyobb mint .05, elfogadjuk a nullhipotezist


# 5. ora

# ANOVA teszt "variancia analizis" (nem szorast, hanem varhato erteket tesztel)
# tobb (mint 2) csoport vizsgalatakor
# feltetel: azonos szorasu, normalis eloszlasu --> Leven teszt, Bartler teszt
# varhato erteket
# a ketmintas t-prova altalanositasa -> ha a csoportok szama = 2, akkor ANOVA = ketmintas t-probo
# H0 = mu1 == mu2 == ... mun
# H1 = ha H0 nem teljesul
# egyszompontos, tobbszempontos verziok
# egyszempontos 

# 1. hipotezisek
# 2. proba kivalasztasa
# 3. probastatisztika kivalasztasa
# 4. kritikus ertek
# 5. p ertek alapjan dontes

#  feltételellenőrzés
bartlett.test(v1~kat) # szórásteszt 
# library(car)
# leveneTest(v1, kat, center=mean)

# várható érték tesztek
oneway.test(v1 ~ kat, var.equal = TRUE) # egyszempontos ANOVA teszt
oneway.test(v1 ~ kat, var.equal = FALSE) # Welch-próba általánosítása több csoportra

# Korrelacios teszt a korrelacios eggyutthatot (r) teszteli
# r(kszi, eta)
# 2 folytonos valtozo kapcsolata -1 is 1 kozott

# kapcsolat iranya:
# pozitit, negativ, 0: nincs korrelacio
# kapcsolat erossege:
# az intervallum szeleihez van kozel (01, 1) hez kozel

# H0: a ket valtozo fuggetlen: r(kszi, eta) = 0
# H1: a H0 nem igaz

# Pearson korrelációteszt: 
cor.test(v1, v2)


# feladatok:
# konf int, param prob / 3
nem = factor(Gender)
var.test(VIQ ~ nem) 

# konf int, param prob / 4
t.test(VIQ ~ nem, conf.level = 0.90)
var.test(VIQ ~ nem)
t.test(VIQ ~ nem, conf.level = 0.95)




# gyakorlo ora
setwd("/home/skyline/git/uni/3/alkstat/gyak/06_gyakorlas/adatok/")

library(foreign, pos=4)
brain <- read.spss("brain.sav", use.value.labels=TRUE, max.value.labels=Inf, to.data.frame=TRUE) # SPSS adatfájl
attach(brain)

kerdoiv <- read.table("kerdoiv.csv", header=TRUE, sep=";", na.strings="", dec=",", strip.white=TRUE, stringsAsFactors=TRUE, fileEncoding = "iso-8859-2")
# kerdoiv2 <- read.csv2("kerdoiv.csv", dec=",", na.strings="", fileEncoding = "iso-8859-2") # 
attach(kerdoiv)

# konv int, param probak / 1
neme= factor(Gender)
table(Gender)
t.test(PIQ ~ neme, conf.level = 0.95, var.equal = TRUE)

oneway.test(PIQ ~ neme, var.equal = TRUE)


# 7.
plot(Magassag, Suly)
cor.test(Magassag,Suly)

# 6.

table(Hajszin)
haj = factor(Hajszin)
bartlett.test(Magassag ~ haj)
oneway.test(Magassag ~ haj)

# alkohol tesztsuly
plot(Alkohol, Suly)
cor.test(Alkohol, Suly)

# varhato ertek (atlag) vagy szoras
# varhato ertek
# almintak hany darab


## mintaZH gyakorlas
setwd("/home/skyline/git/uni/3/alkstat/gyak/07_zh_segedeszkozok/adatok/")

home_office <- read.table("home_office.txt", sep="", header = TRUE, strip.white=TRUE, stringsAsFactors=FALSE, fileEncoding = "iso-8859-2")

summary(home_office)
relGyak = table(home_office) / length(table(row(home_office)))
relGyak
pie(relGyak)



magas <- read.table("magas.txt", sep = " ", header = TRUE, fileEncoding = "iso-8859-2")
csoport = factor(magas$csoport)
var.test(magas$magassag ~ csoport, conf.level = 0.90)

t.test(magas$magassag ~ csoport)

t.test(magas$magassag ~ csoport, conf.level = 0.95)


sor <- read.table("sor.csv", header=TRUE, sep=";", na.strings="", dec=",", strip.white=TRUE, stringsAsFactors=TRUE, fileEncoding = "iso-8859-2")

cor.test(sor$sor, sor$pont)
cor.test(sor$sor, sor$pont, conf.level = 0.95)









survery <- read.table("survey.txt", header=TRUE, sep=" ", na.strings="NA", dec=".", strip.white=TRUE, stringsAsFactors=FALSE, fileEncoding = "iso-8859-2")

boxplot(survery$Pulse)
plot(survery$Pulse)


toot = data.frame(ToothGrowth)
mean(toot$len)


library("mice")
nh = data.frame(nhanes)
summary(nh)


admission <- read.table("admission.csv", header=TRUE, sep=",", na.strings="", dec=".", strip.white=TRUE, stringsAsFactors=FALSE, fileEncoding = "utf-8")
admitFactor = factor(admission$De, exclude = "border")

t.test(admission$GPA ~ admitFactor)    


var.test(toot$len, toot$dose, conf.level = 0.95)

masik = admission[admission$De!="noadmit"]

t.test(admission$GPA, alternative = 'two.sided', mu=2.4, conf.level = .95)


