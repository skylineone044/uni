# encoding: UTF8
# v1, v2 numerikus vektor


# regresszióanalízis modellje

(model=lm(v2~v1)) # lineáris regreszió egyváltozós esetben
(model=lm(v2~log(v1))) # log függvényt illeszti

summary(model) # paraméterek, tesztek, R^2
summary(model)$r.squared # r^2 becslése, ennyi részben magyarázza a modell a függő változó varianciáját
(a=model$coefficient[2]) # meredekség
(b=model$coefficient[1]) # tengelymetszet
confint(model, level = 0.95) # konfidencia intervallum a paraméterekre 

# előrejelzés 

f = function(x) a*x+b # a becslő függvény lineáris esetben 
f = function(x) a*log(x)+b # a becslő függvény nemlineáris esetben
f(v) # előrejelzés a fenti függvénnyel, v lehet vektor is
predict(model, newdata=data.frame(x=v), interval = "prediction", level = 0.95) # előrejelzés + előrejelzési intervallum a kimenetelek értékére
# itt x helyébe a magyarázó változó nevét kell írni!
 
# ábra

plot(v1,v2) # vagy plot(v2~v1)
abline(model) # regressziós egyenes lineáris esetben
curve(f(x), add=T) # regressziós görbe

################################
# diagnosztika

plot(linm)