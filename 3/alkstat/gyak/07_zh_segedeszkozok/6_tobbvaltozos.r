# encoding: UTF8
# v1, v2, stb. numerikus vektor, kat faktor


library(car)

# címkézés, ha van az adathalmazban név változó: rownames(adathalmaz)<-adathalmaz$név

# Főkomponens analízis:

 pcm=princomp(~v1+v2+v3+v4+v5+v6, cor=TRUE) # modell
 summary(pcm)
 loadings(pcm) # átviteli mátrix
 pcm$sd # főkomponensek szórásai
 pcm$sd^2 # főkomponensek szórásnégyzetei
 screeplot(pcm) # ua. ábrán
 biplot(pcm)
 pc=data.frame(pcm$scores) # főkomponensek
 plot(pc[,1:2]) # első két főkomponens ábrán
 var(pc) # főkomponensek varianciái, kovarianciái
 cor(pc) # főkomponensek korrelációi

 
# Diszkriminancia analízis:

 library(MASS)
 da=lda(kat~v1+v2+v3+v4+v5+v6, na.action="na.exclude")
 da # modell kiíratása
 pred=predict(da, data.frame(v1,v2,v3,v4,v5,v6))$class # előrejelzett csoportbatartozás
 table(kat, pred, dnn=c("original", "predicted"), useNA = "ifany") # klasszifikációs tábla
 mean(kat==pred, na.rm = TRUE) # előrejelzés pontossága
 pairs(brain[,2:7], col=kat) # eredeti csoportosítás szerinti ábra
# plot(pc[,1:2], col=kat) # az első két főkomponens, eredeti csoportosítás
 pairs(brain[,2:7], col=pred) # előrejelzés szerinti ábra
 
 
# Logisztikus regresszió:

 lr=glm(kat~v1+v2+v3+v4+v5+v6, family=binomial(logit)) # modell
 summary(lr)
 # predp=predict(lr,data) # log p/(1-p) előrejelzése
 predp=predict(lr, data.frame(v1,v2,v3,v4,v5,v6)) # log p/(1-p) előrejelzése
 pred2=factor(predp > rep(0,length(predp))) # pozitív-nemnegatív értékek külön csoportba sorolása 
 table (kat, pred2, dnn=c("original", "predicted"), useNA = "ifany") # klasszifikációs tábla
 kat2=as.numeric(kat)
 orig=(kat2==2) # ha 1/2-nél kisebb a pontosság, 2 helyett 1-et írunk (megcseréljük a csoportok szerepét)
 mean(orig==pred2, na.rm = TRUE) # előrejelzés pontossága


# k-közép klaszter:

 library(cluster)

 k=3 # klaszterek száma
 kcl=kmeans(model.matrix(~-1+v1+v2+v3+v4+v5), centers = k) # modell
 kcl # modell kiíratása
 kcl$cluster # az egyes mintaelemek klaszterbetartozása
 kcl$size # pontok száma az egyes klaszterekben
 plot(pc[,1:2], col=kcl$cluster) # klaszterek szerinti ábra
 
 
# Hierarchikus klaszter:

 hcl=hclust(dist(model.matrix(~-1+v1+v2+v3+v4)), method="ward.D") # modell
# method="ward.D"/"single"/"complete"/"average"/"mcquitty"/"median"/"centroid"
 name=NULL # ha van név változó, azzal címkézünk
 plot(hcl, labels=name) # dendrogram
 c=as.factor(cutree(hcl, k = 2)) # k klaszternél leállítva az eljárást a kapott klaszterezés
 summary(c) # klaszter méretek 2 klaszter esetén