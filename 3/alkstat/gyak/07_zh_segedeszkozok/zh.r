 
setwd("/home/skyline/git/uni/3/alkstat/gyak/07_zh/adatok/")

zhDAta = read.table("zh1_2.txt", header=TRUE, sep=" ", na.strings=NA, dec=",", strip.white=TRUE, stringsAsFactors=FALSE, fileEncoding = "utf-8")
attach(zhDAta)


# 1.feladat
summary(post.test.1)
ecdf(post.test.1)(mean(post.test.1))

#2. feladat
plot(ecdf(post.test.1))


#3. feladat
t.test(post.test.1, conf.level = 0.99)


#4. feladat
groupFactor = factor(group)
#t.test(pretest.1 ~ groupFactor, conf.level=0.95)
oneway.test(pretest.1 ~ groupFactor, var.equal = TRUE)


#5.feladat
colorFactor = factor(color)
var.test(extra ~ colorFactor, conf.level=0.95)


