# encoding: UTF8
# v1, v2 numerikus vektor, kat, kat2 faktor

setwd("/home/skyline/git/uni/3/alkstat/gyak/10/adatok")

# binomiális teszt (kétértékű változóra)

(n=length(kat))
k=sum(as.numeric(kat)-1)
binom.test(k, n, p = 0.25) # k: megfigyelt események száma, n: elemszám


# Khi^2 eloszlásra:

gyak=table(kat) # gyakoriságok
gyak=c(15,16,14,15,20,20) # kézzel megadva
valsz=c(0.75,0.25) # valószínűségektor
chisq.test(gyak) # diszkrét uniform teszt
chisq.test(gyak, p=valsz) # p valószínűségvektor

library(nortest)
pearson.test(v1) # chi^2 becsléses normalitásteszt


# Kolmogorov-Smirnov becsléses normalitásteszt (Lilliefors-teszt):

lillie.test(v1)


# kétmintás Kolmogorov-Smirnov:

k=as.numeric(kat)
ks.test(v1[k==1], v1[k==2])


# Khi^2 függetlenségre:

xtabs(~kat+kat2) # kontingenciatábla
table(kat, kat2)
addmargins(table(kat, kat2))
chisq.test(kat, kat2)$expected # elvárt értékek
chisq.test(kat, kat2)


# korrelációtesztek:

cor.test(v1, v2, method="pearson") # Spearman esetén: method="spearman"







# nemparatemeres probak
# ferdeseg
# atlag > median  -- jobbra ferde
# atlag < median  -- balra ferde
# atlag == median -- szimetrikus

# grafikus modszerek:
#   hisztogramot keszutunk: hist()
#   Q-Q plot: standardizalunk

# eloszlasvizsgalatra: Kolmogorov-Szmirnov teszt
#    ks-test()
# egymintas eset:
# H0: Fx(x) == F(x0)
# H1: Fx(x) != F(x0)
# tobbmintas eset:
# H0:...
#
# ki-negyzet proba
#


# 9. ora
# valoszinusegek tesztelese
# khi negyzet probaval
# khi negyzet az egy probacsalad
# 1. eloszlasvizsgalatok
# 2. valoszinusegek tesztelese
# 3. fuggetlensegvizsgalat
#
# pl szabalyos e az erme
#  E1 = fej
#  E2 = iras
#  H0 = P(E1) = P(E2) = 1/2
#  n = 100 kiserlet
#       fej 40
#       iras 60
#
#  gyak tabla  E1    E2     ossz
#  H0          40    60     100
#  kapott      50    50     100
#
#
# nemparatemeres pdf / 2
gyakorisagok = c(15, 16, 14, 15, 20, 20);
valoszinusegek = c(1/6, 1/6, 1/6, 1/6, 1/6, 1/6);
chisq.test(gyakorisagok, p=valoszinusegek)

gyakorisagok = c(150, 160, 140, 150, 200, 200);
chisq.test(gyakorisagok, p=valoszinusegek)

# nemparatemeres pdf / 3
gyakorisagok = c(80, 35);
valoszinusegek = c(3/4, 1/4);
chisq.test(gyakorisagok, p=valoszinusegek)

ossz = 35 + 80;
binom.test(35, ossz, p = 0.25)



# nemparatemeres pdf / 1
kerdoiv <- read.csv2("kerdoiv.csv", dec=",", na.strings="", fileEncoding = "iso-8859-2")
gyak = c(55, 65);
valsz = c(0.5, 0.5);

chisq.test(gyak, p=valsz);

ossz = length(kerdoiv$Nem);
binom.test(55, ossz, p = 0.5)



# 10.ora
# fuggetlensegvizsgalat, khi negyzet probaval
salary = read.table("salary.txt", fileEncoding = "UTF-8", header = TRUE)

# khi negyzet probacsalad: 3 proba
# eloszlar, valoszinusegek, fuggetlensegvizsg.

# 2 diszkret valtozo
# a 2 diszkret valtozo fuggetlen-e egymastol
# H0: fuggetlenek a valtozok
# H1: osszefuggo a ket valtozo

# megfigyelt gyakorisagok: amik az adatb-ben vannak
# kszi, eta ket diszkret valoszinusegi valtozok
# kszi \ eta  10   20   sum
#   1        10   20   30
#   2        20   10   30
#   3        10   10   20
#    sum      40   40   n = 80

# vart gyakorisag
# kszi \ eta     10       20   sum
#   1        (40*30)/80   _     30
#   2        _            _     30
#   3        _            _     20
#    sum      40   40   n = 80

a = table(salary$jobkat, salary$educ)
addmargins(a)
prop.table(a)
b = prop.table(a, 2)
addmargins(b)

c = chisq.test(salary$jobkat, salary$educ)$expected
addmargins(c)

chisq.test(salary$jobkat, salary$educ)


################################################
a = table(salary$educ, salary$minority)
prop.table(a)

c = chisq.test(salary$educ, salary$minority)$expected
addmargins(c)
chisq.test(salary$educ, salary$minority)

################################################
a = table(salary$child, salary$gender)
prop.table(a)

c = chisq.test(salary$child, salary$gender)$expected
addmargins(c)
chisq.test(salary$child, salary$educ)


# diszkret  -  diszkret: khi negyzet proba
# diszkret  - folytonos: ANOVA
# folytonos - folytonos: korrelacios teszt