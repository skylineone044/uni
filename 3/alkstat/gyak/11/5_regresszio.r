# encoding: UTF8
# v1, v2 numerikus vektor


# regresszióanalízis modellje

(model=lm(v2~v1)) # lineáris regreszió egyváltozós esetben
(model=lm(v2~log(v1))) # log függvényt illeszti

summary(model) # paraméterek, tesztek, R^2
summary(model)$r.squared # r^2 becslése, ennyi részben magyarázza a modell a függő változó varianciáját
(a=model$coefficient[2]) # meredekség
(b=model$coefficient[1]) # tengelymetszet
confint(model, level = 0.95) # konfidencia intervallum a paraméterekre 

# előrejelzés 

f = function(x) a*x+b # a becslő függvény lineáris esetben 
f = function(x) a*log(x)+b # a becslő függvény nemlineáris esetben
f(v) # előrejelzés a fenti függvénnyel, v lehet vektor is
predict(model, newdata=data.frame(x=v), interval = "prediction", level = 0.95) # előrejelzés + előrejelzési intervallum a kimenetelek értékére
# itt x helyébe a magyarázó változó nevét kell írni!
 
# ábra

plot(v1,v2) # vagy plot(v2~v1)
abline(model) # regressziós egyenes lineáris esetben
curve(f(x), add=T) # regressziós görbe

################################
# diagnosztika

plot(linm)









###################################
# elmeleti attekintes
# korrelacio:
#   ket mennyiseg (kszi; eta) kapcsolatara vonatkozik
#   kapcsolat: van iranya, erossege
#     cor()
# 
# regresszios modell: korrelacios teszt utani lepes, mert csak akkor van ertelme, ha a ket valtozo osszefugg
#   kszi: magyarazo / fuggetlen valtozo - x tengelyen
#   eta:  magyarazando / fuggo valtozo - y tengelyen
# valami "y" eygenes, ami atmegy a ponthalmazon: y = a kalap * x + b kalap + epszilon (az epszilon a hiba)
# E(epszilon) = 0 (varhato erteke epszilonnak 0)
# R^2 mutato: az egyenes illeszkedesenek meroszama
# eta = a * kszi + b + epszilon (epszilon hivatag vagy rezidualis)
# linear modell:
#   cor() -> cor.test() -> lm()   lm mint linear modell



# pdf regresszios feladatok / 1
setwd("/home/skyline/git/uni/3/alkstat/gyak/11/adatok/")
emberek <- read.table("emberek.txt", header=TRUE, sep="", na.strings="NA", dec=",", strip.white=TRUE, stringsAsFactors=TRUE)
attach(emberek)
plot(magassag, suly)

cor(magassag, suly)
cor.test(magassag, suly)
lin_mod = lm(magassag ~ suly)
summary(lin_mod) # a es b a coeff tablazatban az Estimate oszlopban vannak 148.20... es 0.336...

abline(lin_mod)


# pdf regresszios feladatok / 2
confint(lin_mod, level = 0.95)
(a=lin_mod$coefficient[2]) # meredekség
(b=lin_mod$coefficient[1]) # tengelymetszet

cor(magassag, suly)
