setwd("/home/skyline/git/uni/3/alkstat/gyak/12/adatok")

# nemparameteres / 4
kerdoiv <- read.table("kerdoiv.csv", header=TRUE, sep=";", na.strings="", dec=",", strip.white=TRUE, stringsAsFactors=TRUE, fileEncoding = "iso-8859-2")
# nullhipotezist teszelunk, kell valamien teszt: valoszinuseget tesztelunk: khi negyzet proba
table(kerdoiv$Szemszin)
gyakorisagok = table(kerdoiv$Szemszin)
valoszinusegek = c(0.5, 0.3, 0.1, 0.1)
chisq.test(gyakorisagok, p=valoszinusegek)
# p kisebb mint 0.05, elvetjuk a nullhipotezist

# nemparameteres / 5
kerdoiv <- read.table("kerdoiv.csv", header=TRUE, sep=";", na.strings="", dec=",", strip.white=TRUE, stringsAsFactors=TRUE, fileEncoding = "iso-8859-2")
std_suly = scale(kerdoiv$Suly)
qqnorm(std_suly) # ez a qq abra
abline(a=0,1, col="red")

library(nortest)
pearson.test(std_suly)
lillie.test(std_suly)

std_magassag = scale(kerdoiv$Magassag)
qqnorm(std_magassag)
abline(a=0,1, col="red")
pearson.test(std_magassag)
lillie.test(std_magassag)

# nemparameteres / 6
# a ket eloszlas azonos-e: ketmintas kornomorov-smirnov proba
table(kerdoiv$Nem)
ks.test(kerdoiv$Jovedelem[kerdoiv$Nem=="férfi"], kerdoiv$Jovedelem[kerdoiv$Nem=="nő"])

# nemparameteres / 9
# fuggetlenseget vizsgalunk: khi negyzet (ha mind2 diszkret) vagy korrelacio teszt (ha mind2 folytonos) 
# a) mind2 folytonos -> korrelacio teszt
cor.test(kerdoiv$Kor, kerdoiv$Suly)

# b) nem az diszkret, sport is diszkret -> khi negyzet proba
chisq.test(kerdoiv$Sport, kerdoiv$Nem)



# regresszio / 4
library("readxl")
autok <- read_excel("autok.xls") 
attach(autok)
plot(vegseb ~ loero) # log fgv alaku
model = lm(vegseb ~ log(loero))
summary(model)
predict(model, newdata = data.frame(loero = 120), interval = "prediction", level = 0.95)






