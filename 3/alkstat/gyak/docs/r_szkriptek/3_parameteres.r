# encoding: UTF8
# v1, v2 numerikus vektor, kat faktor


# Egymintás t: 

t.test(v1, alternative='two.sided', mu=100, conf.level=0.95)

# egyes argumentumok külön:
t.test(v1)$estimate # becslés
t.test(v1, conf.level=0.95)$conf.int # konfidencia intervallum
t.test(v1, alternative='two.sided', mu=100)$statistic # próbastatisztika
t.test(v1, alternative='two.sided', mu=100)$p.value # p-érték


# Páros t: 

t.test(v1, v2, conf.level=0.95, paired=TRUE)


# F próba:

var.test(v1 ~ kat) 

# Kétmintás t: 

t.test(v1~kat, conf.level=0.95, var.equal=TRUE) # v1 folytonos, kateg 2 értékű változó

# Welch próba (nem feltétlen egyenlő szórás esetén):

t.test(v1~kat, conf.level=0.95) 


#  feltételellenőrzés

bartlett.test(v1~kat) # szórásteszt 
# library(car)
# leveneTest(v1, kat, center=mean)

# várható érték tesztek

oneway.test(v1 ~ kat, var.equal = TRUE) # egyszempontos ANOVA teszt
oneway.test(v1 ~ kat, var.equal = FALSE) # Welch-próba általánosítása több csoportra

# Pearson korrelációteszt: 

cor.test(v1, v2)