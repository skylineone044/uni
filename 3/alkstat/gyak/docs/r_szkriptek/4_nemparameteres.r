# encoding: UTF8
# v1, v2 numerikus vektor, kat, kat2 faktor


# binomiális teszt (kétértékű változóra)

(n=length(kat))
k=sum(as.numeric(kat)-1)
binom.test(k, n, p = 0.25) # k: megfigyelt események száma, n: elemszám


# Khi^2 eloszlásra: 

gyak=table(kat) # gyakoriságok
gyak=c(15,16,14,15,20,20) # kézzel megadva
valsz=c(0.75,0.25) # valószínűségektor
chisq.test(gyak) # diszkrét uniform teszt
chisq.test(gyak, p=valsz) # p valószínűségvektor 

library(nortest) 
pearson.test(v1) # chi^2 becsléses normalitásteszt


# Kolmogorov-Smirnov becsléses normalitásteszt (Lilliefors-teszt): 

lillie.test(v1) 


# kétmintás Kolmogorov-Smirnov: 

k=as.numeric(kat)
ks.test(v1[k==1], v1[k==2])


# Khi^2 függetlenségre: 

xtabs(~kat+kat2) # kontingenciatábla
table(kat, kat2)
addmargins(table(kat, kat2))
chisq.test(kat, kat2)$expected # elvárt értékek
chisq.test(kat, kat2) 


# korrelációtesztek: 

cor.test(v1, v2, method="pearson") # Spearman esetén: method="spearman"