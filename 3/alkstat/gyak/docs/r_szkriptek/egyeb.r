# encoding: UTF8
# munkakönyvtár beállítása

setwd("C:\\sajatkonyvtar")

# csomag telepítése, betöltése 

install.packages("moments")
library(moments)


# függvények és help

?mean
help(summary)
args(mean) # opcionális argumentumok


# objektumok listázása

ls()
objects()

# objektum törlése

remove(v)


# változók típusának lekérdezése
class(v)
typeof(v)
str(v)
is.numeric(v)
is.character(v)


# For-ciklus: 10! kiszamolasa.
f<-1; 
for(i in 2:10) {f<-f*i}; 
f 


# vektor

v1=c(5,2,1,5,2)  # vektor létrehozása
v2=c(TRUE,TRUE,FALSE,TRUE,TRUE)

class(v1)
str(v1)
length(v1)

# szűrők

v1[2]
v1[2:4]
v1[c(2,4)]
v1[v1>3]
v1[v2]
v1[bool]

# vektorműveletek

v2=c(5:9)
v3=rep(1,5)
v4=c("egy","kettő","egy","kettő")
v5=c(TRUE,TRUE,FALSE)
v6=v1-3
v7=v1*v2
sum(v7)
v1 %*% v2
log(v1,base=2)
bool=(1<v1 & v1<5)

v8=v3=rep(0,5)

for(i in c(1:5)){
  if (bool[i]==TRUE) v8[i]=v1[i] else v8[i]=0
}


# faktor

f1=factor(v1) # vektor faktorrá konvertálása
t1=table(f1)  # táblázattá alakítás: szintek és gyakoriságok
barplot(v1)
barplot(t1)


# mátrix

u1=c(v1,v2,v3) # vektorok összefűzése
u1=c(v1,v4) # különböző típusok esetén automatikus konverzió

M1<-matrix(1:50,nrow=5,ncol=10)

dim(M1)
class(M1)
typeof(M1)

M2=cbind(v1,v2,v3)
M3=rbind(v1,v2,v3)
M4=matrix(c(v1,v2),nrow=2,byrow=TRUE)

M5=t(M1) # transzponálás
M1[3,2]
M1[1,]
M1[,2]

addmargins(M1) # soronkénti és oszlopokénkénti összeg

M6=matrix(rep(0,25), nrow=5)
for(i in c(1:5)){
  for(j in c(1:5)){
    M6[i,j]=v1[i]*v2[j]
  }
}


# lista:

lista = list(team = "Manchester United", PLTitle = 20, 
bestPlayers = c("Best", "Cantona", "Schmeichel", "Giggs", "Rooney"))
lista[[1]]
lista$team


# adattáblázat:

nevek = c("Anna", "Béla", "Csaba")
pontszamok = c(123, 84, 96)
adat = data.frame(nevek, pontszamok)
adat[1, 2]
elsosor = adat[1, ]
masodikoszlop = adat[, 2]
kisebbmintszaz = adat[adat$pontszamok < 100, ]
rendezo = order(adat$pontszamok)
rendezett = adat[rendezo, ]  


# függvények

?function() expr

norm = function(x) sqrt(x%*%x) # saját függvény definiálása - itt 2-es norma 
norm(1:4)

Phi = function(x) pnorm(x, mean=0, sd=1) # eloszlásfüggvény
Phi(2)
curve(Phi)
curve(dnorm(x, mean=0, sd=1), from = -5, to =5, col="red", add=FALSE) # sűrűségfüggvény ábrázolása
x = rnorm(50, mean = 10, sd = 2) # véletlen számok generálása


# címkézés, ha van az adathalmazban név változó: 

rownames(emberek) <- emberek$nev


# adatok manipulálása

brain$Gender <- tolower(brain$Gender) # kisbetűsre állítja
brain$meanIQ <- (brain$FSIQ+brain$PIQ+brain$VIQ)/3 # átlagolt IQ érték

names(brain)[c(1,7)] <- c("Sex","MRI") # változók átnevezése

na.omit(brain) # hiányzó adatokat tartalmazó sorok kihagyása
brain[-c(3,5),] # 3. és 5. sor törlése

brain2 <- brain[which(brain$Gender=='Male  '),]


# output fájlba/terminálra irányítása
 
sink("output.txt")
summary(brain)
sink() # vissza a terminálra


# kép fájlba mentése

png("abra.png") # képfájl megnyitása
# pdf("abra.pdf"), win.metafile("abra.wmf"), jpeg("abra.jpg"), bmp("abra.bmp"), postscript("abra.ps")
boxplot(v1) # ábra elkészítése 
dev.off() # képfájl mentése