# encoding: UTF8
# adat adattáblázat (dataframe), v1 numerikus vektor, kat faktor


length(adat) # változók száma 
ncol(adat) # változók száma
nrow(adat) # mintaelemszám 

summary(kerdoiv) 

plot(ecdf(v1)) # empirikus eloszlásfüggvény - csak vektorra!
ecdf(v1)(x) # az empirikus eloszlásfüggvény értéke az x helyen


# diszkrét változó (faktor legyen!)

length(kat) # mintaelemszám
(n=length(kat[!is.na(kat)])) # hiányzó adatok nélkül

summary(kat) # gyakoriságok
table(kat) # ua. hiányzó adatok nélkül
prop.table(table(kat)) # relatív gyakoriságok
table(kat)/n # ua. hiányzó adatok nélkül

pie(table(kat)) # kördiagram
barplot(table(kat)) # oszlopdiagram - gyakoriságok
barplot(table(kat)/n) # relatív gyakoriságok

# folytonos változó 

summary(v1)
length(v1) # mintaelemszám
length(v1[!is.na(v1)]) # hiányzó adatok nélkül
range(v1, na.rm=TRUE) # min, max
max(v1, na.rm=TRUE) - min(v1, na.rm=TRUE) # mintaterjedelem
mean(v1, na.rm=TRUE)  # átlag, hiányzó adatok átugrása

var(v1, na.rm=TRUE) # variancia becslése
sd(v1, na.rm=TRUE) # szórás becslése
sd(v1, na.rm=TRUE)**2 # variancia becslése

std_v1 = scale(v1) # standardizálás

library(moments)
skewness(v1, na.rm=TRUE) # ferdeség

median(v1, na.rm=TRUE)

quantile(v1, na.rm=TRUE)  # min, 1. kvartilis, medián (= 2. kvartilis), 3. kvartilis, max
quantile(v1, probs=c(0.1,0.9), na.rm=TRUE) # 0.1- és 0.9-kvantilis

IQR(v1) # interkvartilis terjedelem

boxplot(v1)

hist(v1) # hisztogram
hist(v1,freq=FALSE) # sűrűséghisztogram

# Q-Q ábra (standardizált változóra)
std_v1 = scale(v1) # standardizálás
qqnorm(std_v1)
abline(0,1, col="red") # x=y egyenes

# csoportosítva (summary, min, max, range, mean, var, sd, skewness, median, quantile, IQR, stb.)

tapply(v1, list(kat), mean, na.rm=TRUE) # kateg szerint csoportosított átlagok
aggregate(v1, list(kat), mean, na.rm=TRUE) # ua.

barplot(tapply(v1, list(kat), mean, na.rm=TRUE)) # átlagok csoportonként
boxplot(v1~kat) # boxplot csoportonként

# Kovariancia, korreláció:

cov(v1, v2, use="complete.obs") # kovariancia
cor(v1, v2, use="complete.obs") # korreláció

plot(v1,v2) 