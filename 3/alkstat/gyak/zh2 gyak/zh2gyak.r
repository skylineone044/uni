setwd("/home/skyline/git/uni/3/alkstat/gyak/docs/adatok") 

# 2
plant = read.table("PlantGrowth.txt", header = TRUE)
attach(plant)
library(nortest)
lillie.test(weight[group=="ctrl"])
detach(plant)

# 3
binom.test(c(703, 527), 1230, p=0.5)

# 4
plant = read.table("PlantGrowth.txt", header = TRUE)
attach(plant)
ks.test(weight[group=="ctrl"], weight[group=="trt2"])
detach(plant)

# 5 
chisq.test(c(14, 20, 90-14-20), p=c(0.1, 0.2, 0.7))

# 6
adatok = read.table("adatok.txt", header = TRUE, sep = " ")
attach(adatok)
pearson.test(pretest.2)
detach(adatok)

# 7
adatok1 = read.table("adatok1.txt", header = TRUE, sep = " ")
attach(adatok1)
chisq.test(education, region)
detach(adatok1)

# 8
kolcson = read.table("kolcson.txt", header = TRUE, sep = "\t", dec = ",")
attach(kolcson)
chisq.test(rbind(table(tipus[adostip==1]), table(tipus[adostip==2])))
detach(kolcson)

# 9
family = read.table("family.txt", header = TRUE, sep = "\t", dec = ",")
attach(family)
cor.test(expend, income)
plot(expend ~ income)

model = lm(formula = expend ~ income)
summary(model)
summary(model)$r.squared
confint(model, level = 0.95)
detach(family)


# 10
sadv = read.table("sale-advertising.txt", header = TRUE, sep = "\t", dec = ",")
attach(sadv)
plot(sale ~ advertising)
abline(1, 1.5, col = "red")
md = lm(formula = sale ~ advertising)
summary(md)
predict(md, newdata = data.frame(advertising = 6.2), interval = "prediction", level = 0.95)
detach(sadv)

# 11
adatok2 = read.table("adatok2.txt", header = TRUE, sep = " ")
attach(adatok2)
cor.test(pop15, dpi, method="spearman")
detach(adatok2)















