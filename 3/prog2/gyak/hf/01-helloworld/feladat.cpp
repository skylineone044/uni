#include <iostream>
#include <stdexcept>
#include <cassert>

using namespace std;


/////////////////////////
//Ide dolgozz!!
void hellomars() {
    cout << "Hello Mars!\n";
}
////////////////////////

//=== Teszteles bekapcsolasa kikommentezessel
#define TEST_hellomars
//=== Teszteles bekapcsolas vege


/*
Készíts egy metódust, amely kiírja a "Hello Mars!" szöveget (idézőjelek nélkül) a képernyőre úgy,
hogy a szöveg után sortörés következzen! A kiíratást a hellomars nevű függvényben végezd el!
*/
void test_hellomars(){
  #if defined TEST_hellomars && !defined TEST_BIRO
  hellomars();
  #endif
}

int main(){
  test_hellomars();
  return 0;
}
