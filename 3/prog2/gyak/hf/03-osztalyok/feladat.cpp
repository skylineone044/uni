#include <iostream>
#include <string>
#include <cassert>

#include <vector>
#include <sstream>

using namespace std;

/////////////////////////
//Ide dolgozz!!
class Telepes {
private:
    string nev;
    string szul_bolygo;
    string bolygo;
    unsigned ero;

public:
    const string &get_nev() const {
        return nev;
    }

    void set_nev(const string &nev) {
        Telepes::nev = nev;
    }

    const string &get_szul_bolygo() const {
        return szul_bolygo;
    }

    void set_szul_bolygo(const string &szulBolygo) {
        szul_bolygo = szulBolygo;
    }

    const string &get_bolygo() const {
        return bolygo;
    }

    void set_bolygo(const string &bolygo) {
        Telepes::bolygo = bolygo;
    }

    unsigned int get_ero() const {
        return ero;
    }

    void set_ero(unsigned int ero) {
        Telepes::ero = ero;
    }

    Telepes(const string &nev, const string &szulBolygo, const string &bolygo, unsigned int ero) : nev(nev),
                                                                                                   szul_bolygo(
                                                                                                           szulBolygo),
                                                                                                   bolygo(bolygo),
                                                                                                   ero(ero) {}

    Telepes() {
        this->nev = "";
        this->szul_bolygo = "";
        this->bolygo = "";
        this->ero = 1;
    }

    Telepes(const string &nev, const string &bolygo, unsigned int ero = 1) : nev(nev), szul_bolygo(bolygo),
                                                                             bolygo(bolygo),
                                                                             ero(ero) {}

    bool kivandorlo() const {
        return my_tolower(this->szul_bolygo) != my_tolower(this->bolygo);
    }

    void dolgozik(string& munkak) const {
        stringstream test(munkak);
        string segment;
        vector<string> seglist;
        while(getline(test, segment, ';'))
        {
            seglist.push_back(segment);
        }

        munkak = "";
        for (int i = this->ero; i < seglist.size(); ++i) {
            munkak += seglist[i];
            if (i + 1 < seglist.size()) {
                munkak += ";";
            }
        }
    }

private:
    string my_tolower(string s) const {
        for (int i = 0; i < s.length(); ++i) {
            s[i] = tolower(s[i]);
        }
        return s;
    }
};
////////////////////////

//=== Teszteles bekapcsolasa kikommentezessel
#define TEST_default_konstruktor
#define TEST_param_konstruktor
#define TEST_param_konstruktor2
#define TEST_kivandorlo
#define TEST_munkavegzes
//=== Teszteles bekapcsolas vege

/*
Készíts egy Telepes nevű osztályt, mely a különféle bolygókon letelepedett emberek adatait tárolja.
Négy adattagja van, ezek mindegyikéhez tartozik getter és setter. 
- szul_bolygo: születési bolygó
- bolygo: jelenlegi bolygó
- nev: a telepes neve
- ero: munkavégző képesség (unsigned int)
Rendelkezzen egy default konstruktorral, amely a nev, szul_bolygo és bolygo adattagokat üres sztringre inicializálja, az ero-t pedig 1-re.
*/
void test_default_konstruktor() {
#if defined TEST_default_konstruktor && !defined TEST_BIRO
    Telepes t1;
    assert(t1.get_nev() == "");
    assert(t1.get_szul_bolygo() == "");
    assert(t1.get_bolygo() == "");
    assert(t1.get_ero() == 1);

    t1.set_szul_bolygo("Mars");
    t1.set_bolygo("Venus");
    t1.set_nev("Jevgenyij Anyegin");
    t1.set_ero(10);
    assert(t1.get_nev() == "Jevgenyij Anyegin");
    assert(t1.get_szul_bolygo() == "Mars");
    assert(t1.get_bolygo() == "Venus");
    assert(t1.get_ero() == 10);
#endif
}

/*
Legyen egy konstruktora, mely négy paraméterrel rendelkezik és mind a négy adattagot a paramétereknek megfelelően állítja be. 
Paraméterek sorrendje: nev, szul_bolygo, bolygo, ero
*/
void test_param_konstruktor() {
#if defined TEST_param_konstruktor && !defined TEST_BIRO
    const Telepes t1("Franz Kafka", "Mars", "Venus", 50);
    assert(t1.get_nev() == "Franz Kafka");
    assert(t1.get_szul_bolygo() == "Mars");
    assert(t1.get_bolygo() == "Venus");
    assert(t1.get_ero() == 50);
#endif
}

/*
Legyen egy konstruktora, amely két sztringet és egy unsigned paramétert vár. Az első sztring paraméter alapján a telepes neve legyen inicializálva, 
míg a második sztring paraméter a telepes jelenlegi bolygójának a neve. 
Ez alapján legyen inicializálva a bolygo és a szul_bolygo adattag is.
Lehessen úgy is használni, hogy meghívásakor nem adunk meg unsigned értéket, csak a két sztringet. Ilyenkor az ero érték legyen 1-re állítva. 
*/
void test_param_konstruktor2() {
#if defined TEST_param_konstruktor2 && !defined TEST_BIRO
    const Telepes t1("Kurt Vonnegut", "Venus", 4);
    assert(t1.get_nev() == "Kurt Vonnegut");
    assert(t1.get_szul_bolygo() == "Venus");
    assert(t1.get_bolygo() == "Venus");
    assert(t1.get_ero() == 4);

    const Telepes t2("Kurt Vonnegut", "Venus");
    assert(t2.get_nev() == "Kurt Vonnegut");
    assert(t2.get_szul_bolygo() == "Venus");
    assert(t2.get_bolygo() == "Venus");
    assert(t2.get_ero() == 1);
#endif
}

/*
Készíts egy kivandorlo() metódust a Telepes osztályba, mely segít eldönteni, hogy a telepes kivandorlónak minősül-e. 
A visszatérési értéke akkor legyen igaz, ha a szul\_bolygo és bolygo adattagok értéke nem egyenlő egymással. 
Két bolygó neve egyenlőnek számít, ha csak kisbetű/nagybetű eltérés van a nevükben (pl. ``Mars'' és ``mars'' egyenlőek). 
A bolygók nevei ascii karakterek (tehát nem tartalmaznak pl. ékezetes betűket). Ha mindkét adattag értéke üres sztring egyenlőnek tekinthetőek. 
*/
void test_kivandorlo() {
#if defined TEST_kivandorlo && !defined TEST_BIRO
    const Telepes t1("Franz Kafka", "Mars", "Px-2312", 50); //Kafka kivandorlo
    assert(t1.kivandorlo());
#endif
}

/*
Készíts egy dolgozik() metódust, amely naplózza a telepes munkavégézését.
A paraméterként kapott sztring reprezentálja az egymás után következő feladatokat pontosvesszővel elválasztva. 
A feladatok alfanumerikus kódokkal vannak reprezentálva, pl.: b1 - kapálás, b22 - ültetés stb. 
A telepesek minden feladatot el tudnak végezni, azonban csak annyi darabot, 
amennyi az ero értékük, utána kifáradnak. A metódus célja, hogy levágja azokat a munkákat a sztring elejéről, amiket a telepes elvégzett.
*/
void test_munkavegzes() {
#if defined TEST_munkavegzes && !defined TEST_BIRO
    const Telepes t1("Kovacs Geza", "Uranus", "Px-2312", 2);
    string munkak = "b1;b22;c3;x823";
    t1.dolgozik(munkak);
    assert(munkak == "c3;x823");
#endif
}

int main() {

    test_default_konstruktor();
    test_param_konstruktor();
    test_param_konstruktor2();
    test_kivandorlo();
    test_munkavegzes();

    return 0;
}
