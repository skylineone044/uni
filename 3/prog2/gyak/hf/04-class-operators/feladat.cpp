#include <iostream>
#include <string>
#include <cassert>

#include <sstream>
#include <vector>

using namespace std;


/////////////////////////
//Ide dolgozz!!
string my_tolower(string s) {
    for (int i = 0; i < s.length(); ++i) {
        s[i] = tolower(s[i]);
    }
    return s;
}

class Kolonia;

bool veganE(Kolonia k);

class Telepes {
private:
    string nev;
    string szul_bolygo;
    string bolygo;
    unsigned ero;
    bool vegan = false;

public:
    const string &get_nev() const {
        return nev;
    }

    void set_nev(const string &nev) {
        this->nev = nev;
    }

    const string &get_szul_bolygo() const {
        return szul_bolygo;
    }

    void set_szul_bolygo(const string &szulBolygo) {
        szul_bolygo = szulBolygo;
    }

    const string &get_bolygo() const {
        return bolygo;
    }

    void set_bolygo(const string &bolygo) {
        this->bolygo = bolygo;
    }

    unsigned int get_ero() const {
        return ero;
    }

    void set_ero(unsigned int ero) {
        this->ero = ero;
    }

    bool is_vegan() const {
        return vegan;
    }

    void set_vegan(bool vegan) {
        this->vegan = vegan;
    }

    Telepes(const string &nev, const string &szulBolygo, const string &bolygo, unsigned int ero) : nev(nev),
                                                                                                   szul_bolygo(
                                                                                                           szulBolygo),
                                                                                                   bolygo(bolygo),
                                                                                                   ero(ero) {}

    Telepes() {
        this->nev = "";
        this->szul_bolygo = "";
        this->bolygo = "";
        this->ero = 1;
        this->vegan = false;
    }

    Telepes(const string &nev, const string &bolygo, unsigned int ero = 1) : nev(nev), szul_bolygo(bolygo),
                                                                             bolygo(bolygo),
                                                                             ero(ero) {}

    bool kivandorlo() const {
        return my_tolower(this->szul_bolygo) != my_tolower(this->bolygo);
    }

    void dolgozik(string &munkak) const {
        stringstream test(munkak);
        string segment;
        vector<string> seglist;
        while (getline(test, segment, ';')) {
            seglist.push_back(segment);
        }

        munkak = "";
        for (int i = this->ero; i < seglist.size(); ++i) {
            munkak += seglist[i];
            if (i + 1 < seglist.size()) {
                munkak += ";";
            }
        }
    }
};

bool veganE(Kolonia k);

class Kolonia {
    string nev;
    string bolygo;
    unsigned letszam;
    Telepes lakok[25];

public:
    const string &get_nev() const {
        return nev;
    }

    void set_nev(const string &nev) {
        Kolonia::nev = nev;
    }

    const string &get_bolygo() const {
        return bolygo;
    }

    void set_bolygo(const string &bolygo) {
        Kolonia::bolygo = bolygo;
    }

    unsigned int get_letszam() const {
        return letszam;
    }

    const Telepes *get_lakok() const {
        return lakok;
    }

    Kolonia(string nev, string bolygo) {
        this->nev = nev;
        this->bolygo = bolygo;
        this->letszam = 0;
    }

    Kolonia() {
        this->nev = "";
        this->bolygo = "";
        this->letszam = 0;
    }

//    friend bool veganE(Kolonia k);

    void add_telepes(Telepes t) {
        if (this->letszam < 25) {
            if ((veganE(*this) && t.is_vegan()) | !veganE(*this)) {
                t.set_bolygo(this->get_bolygo());
                this->lakok[this->letszam] = t;
                this->letszam++;
            } else {
                cout << "Akolonia vegan" << endl;
            }
        }
        cout << "A kolonia megtelt" << endl;
    }


};

bool veganE(Kolonia k) {
    const Telepes *telepesek = k.get_lakok();
    int veganok_szama = 0;

    for (int i = 0; i < k.get_letszam(); ++i) {
        if (telepesek[i].is_vegan()) {
            veganok_szama++;
        }
    }
    if (k.get_letszam() == 0) {
        return false;
    }

    return (double) veganok_szama / (double) k.get_letszam() >= 0.5;
}

//void operator+=(Kolonia& k, Telepes t) {
//    if (!k.add_telepes(t)) MINTA-gantt-diagram{
//        cout << "A kolonia megtelt" << endl;
//    }
//}

Kolonia operator+=(Kolonia &k, const Telepes t) {
    k.add_telepes(t);
    return k;
}

Kolonia operator+=(Kolonia &k1, Kolonia k2) {
    if (my_tolower(k1.get_bolygo()) == my_tolower(k2.get_bolygo())) {
        if (25 - k1.get_letszam() >= k2.get_letszam()) {
            for (int i = k1.get_letszam(), j = 0; j < k2.get_letszam(); ++i, ++j) {
                k1.add_telepes(k2.get_lakok()[j]);
            }
        } else {
            cout << "Hiba azegyesitesben: meret" << endl;
        }
    } else {
        cout << "Hiba az egyesitesben: bolygo" << endl;
    }
    return k1;
}

bool operator==(Telepes t1, Telepes t2) {
    return t1.get_nev() == t2.get_nev() && t1.get_ero() == t2.get_ero() &&
           my_tolower(t1.get_bolygo()) == my_tolower(t2.get_bolygo()) &&
           my_tolower(t1.get_szul_bolygo()) == my_tolower(t2.get_szul_bolygo()) && t1.is_vegan() == t2.is_vegan();
}

Kolonia operator-(Kolonia k, Telepes t) {
    int torolni = 26;
    for (int i = 0; i < k.get_letszam(); ++i) {
        if (k.get_lakok()[i] == t) {
            torolni = i;
            break;
        }
    }
    if (torolni == 26) {
        return k;
    } else {
        Kolonia uj_kolonia(k.get_nev(), k.get_bolygo());
        for (int i = 0; i < k.get_letszam(); ++i) {
            if (i != torolni) {
                uj_kolonia.add_telepes(k.get_lakok()[i]);
            }
        }
        return uj_kolonia;
    }
}
////////////////////////

//=== Teszteles bekapcsolasa kikommentezessel
#define TEST_telepes_osztaly
#define TEST_kolonia_osztaly1
#define TEST_kolonia_osztaly2
#define TEST_operator_plusz
#define TEST_vegan_kolonia
#define TEST_operator_plusz2
#define TEST_kolonia_operator_plusz
#define TEST_telepes_egyenloseg
#define TEST_kolonia_minusz
//=== Teszteles bekapcsolas vege

/*
Készíts egy Telepes nevű osztályt, mely a különféle bolygókon letelepedett emberek adatait tárolja.
Négy adattagja van, ezek mindegyikéhez tartozik getter és setter. 
- szul_bolygo: születési bolygó
- bolygo: jelenlegi bolygó
- nev: a telepes neve
- ero: munkavégző képesség (unsigned int)
Rendelkezzen egy default konstruktorral, amely a nev, szul_bolygo és bolygo adattagokat üres sztringre inicializálja, az ero-t pedig 1-re.

Bővítsd ki a Telepes osztályt egy új adattaggal, melynek default értéke false legyen. Inicializálja ezt is a default konstruktor! 
- vegan: vegán-e a telepes (bool)
*/
void test_telepes_osztaly() {
#if defined TEST_telepes_osztaly && !defined TEST_BIRO
    Telepes t1;
    assert(t1.get_nev() == "");
    assert(t1.get_szul_bolygo() == "");
    assert(t1.get_bolygo() == "");
    assert(t1.get_ero() == 1);
    assert(!t1.is_vegan());

    t1.set_szul_bolygo("Mars");
    t1.set_bolygo("Venus");
    t1.set_nev("Jevgenyij Anyegin");
    t1.set_ero(10);
    t1.set_vegan(true);
    assert(t1.get_nev() == "Jevgenyij Anyegin");
    assert(t1.get_szul_bolygo() == "Mars");
    assert(t1.get_bolygo() == "Venus");
    assert(t1.get_ero() == 10);
    assert(t1.is_vegan());
#endif
}


/*
Készíts egy Kolonia nevű osztályt, mely a telepesek által létesített kommunákat reprezentálják.
Gazdasági okok miatt minden kolónia csak 25 telepes befogadására képes. Adattagok:
 - nev: a kolónia neve 
 - bolygo: a kolónia helye 
 - letszam: a telepesek aktuális száma (unsigned)
 - lakok: telepesek tömbje, 25 méretű (Telepes[])

Legyen egy default konstruktora, mely a kolónia nevét és a bolygóját üres sztringre, a létszámot pedig 0-ra inicializálja.
*/
void test_kolonia_osztaly1() {
#if defined TEST_kolonia_osztaly1 && !defined TEST_BIRO
    Kolonia k;
    assert(k.get_nev() == "");
    assert(k.get_bolygo() == "");
    assert(k.get_letszam() == 0);
    assert(k.get_lakok()[24].get_ero() == 1);

    k.set_nev("Anarchy for all!");
    k.set_bolygo("Mars");

    assert(k.get_nev() == "Anarchy for all!");
    assert(k.get_bolygo() == "Mars");
    assert(k.get_letszam() == 0);
    assert(k.get_lakok()[24].get_ero() == 1);
#endif
}


/*
Legyen a Kolonia osztálynak konstruktora, mely két string paraméterrel rendelkezik. 
Az első sztring alapján a kolónia neve, a második alapján a kolónia bolygója legyen beállítva. A letszam kezdetben legyen 0-ra állítva.
*/
void test_kolonia_osztaly2() {
#if defined TEST_kolonia_osztaly2 && !defined TEST_BIRO
    const Kolonia k("Anarchy for all!", "Mars");
    assert(k.get_nev() == "Anarchy for all!");
    assert(k.get_bolygo() == "Mars");
    assert(k.get_letszam() == 0);
    assert(k.get_lakok()[24].get_ero() == 1);
#endif
}


/*
Valósítsd meg a += operátort, hogy telepest lehessen hozzáadni a kolóniához.
A telepes úgy legyen letárolva, hogy a bolygo adattagja egyezzen meg a kolónia bolygo adattagjával. 
Ha a 25 fős limit miatt már nem lehet újabb telepest a kolóniához adni, akkor a metódus pontosan ezt a sztringet írja ki a standard outputra: "A kolonia megtelt". A kiíratást sortörés kövesse.
*/
void test_operator_plusz() {
#if defined TEST_operator_plusz && !defined TEST_BIRO
    Kolonia k("Anarchy for all!", "Mars");
    Telepes t;
    t.set_nev("Bruce Willis");
    const Telepes t2(t);
    k += t2;
    assert(k.get_letszam() == 1);
    assert(k.get_lakok()[0].get_nev() == "Bruce Willis");
#endif

}

/*
Készíts egy globális függvényt a Kolonia osztályon kívül, amely megvizsgálja, hogy a kolónia vegán-e, többségben vannak-e a vegán életfelfogású lakók. 
A függvény akkor ad vissza igaz értéket, ha kolónia lakóinak több, mint 50%-a vegán (a vegan adattag értéke igaz). 
Ha nincs egy lakó sem a kolóniában, akkor alapértelmezetten nem vegán (azaz false a függvény visszatérési értéke). 
*/
void test_vegan_kolonia() {
#if defined TEST_vegan_kolonia && !defined TEST_BIRO
    Kolonia k("Anarchy for all!", "Mars");
    assert(!veganE(k));

    Telepes t;
    t.set_vegan(true);
    k += t;
    assert(veganE(k));
#endif
}


/*
Fejleszd tovább a += operátort!
Az új telepes letárolása előtt azt is vizsgáld meg, hogy a telepes vegán orientációja egyezik-e a kolónia vegán orientációjával. 
Ha a kolónia vegán, akkor az új telepes letárolása csak akkor lehetséges, ha ő is vegán. 
Amennyiben nem vegán az alábbi hibaüzenet kerüljön a standard outputra (utána sortörés következzen): ``A kolonia vegan''.

Figyelem, a veganE használatához a függvény és/vagy a Kolonia osztály forward deklarációja szükséges (https://www.learncpp.com/cpp-tutorial/forward-declarations/)
*/
void test_operator_plusz2() {
#if defined TEST_operator_plusz2 && !defined TEST_BIRO
    Kolonia k("Anarchy for all!", "Mars");
    Telepes t;
    t.set_vegan(true);
    k += t;
    assert(k.get_letszam() == 1);

    Telepes t2;
    k += t2;
    assert(k.get_letszam() == 1);
#endif
}


/*
Valósítsd meg a += operátort kolóniák összeolvasztására! A jobboldali paraméter tehát most egy kolónia objektum lesz. 
Az összevonás csak akkor lehetséges, ha a két kolónia azonos bolygón található, ehhez ellenőrizni kell a két kolónia bolygo adattagját. 
Két bolygó neve egyenlőnek számít, ha csak kisbetű/nagybetű eltérés van a nevükben (pl. "Mars" és "mars" egyenlőek), két üres sztring szintén egyenlő. 
(Ez ugyanaz a bolygónév összehasonlító logika, ami a Telepes feladatsorban volt) 
Ha a kolóniák bolygói nem egyeznek meg az alábbi hibaüzenet legyen kiírva (utána sortörés): "Hiba az egyesitesben: bolygo". 
Az összeolvasztás másik feltétele, hogy a befogadó kolóniában legyen legalább annyi szabad hely, amennyi az érkező kolónia létszáma. 
Ha kevés a hely ez a hibaüzenet legyen kiíratva (a kiíratás után sortörés következzen): "Hiba az egyesitesben: meret". 
A kolóniák vegán orientációjával itt nem kell foglalkozni. Ha lehetséges az összeolvasztás, akkor másoljuk át az új telepeseket a befogadó kolónia lakok tömbjébe az első szabad helytől kezdve.
*/
void test_kolonia_operator_plusz() {
#if defined TEST_kolonia_operator_plusz && !defined TEST_BIRO
    Kolonia k1("Anarchy for all!", "Mars"), k2("Democracy is the best", "Mars");
    Telepes t;
    k1 += t;
    k2 += t;
    k1 += k2;
    assert(k1.get_letszam() == 2);
#endif
}


/*
Valósítsd meg az == operátort a Telepes osztályra. A metódus akkor adjon vissza igaz értéket, ha az alábbiak mindegyike teljesül a két operandusra :
- a telepesek neve karakterre pontosan megegyezik
- a telepesek erő értéke azonos
- a bolygó adattagok a kis- és nagybetűktől eltekintve megegyeznek (ugyanaz az egyenlőség feltétele, mint korábban)
- a születési bolygó adattagok a kis- és nagybetűktől eltekintve megegyeznek
- a vegan adattagok megegyeznek 
*/
void test_telepes_egyenloseg() {
#if defined TEST_telepes_egyenloseg && !defined TEST_BIRO
    Telepes t1;
    t1.set_nev("Jules Verne");
    const Telepes t2(t1);
    t1.set_nev("Verne");
    const Telepes t3(t1);
    assert(!(t2 == t3));
#endif
}


/*
Valósítsd meg a - operátort egy kolónia és egy telepes között, hogy telepest lehessen likvidálni a kolóniából. 
A baloldali operandus a kolónia, a jobboldali a telepes legyen.
A likvidálás azt jelenti, hogy meg kell vizsgálni, hogy a kolónia lakói között van-e a paraméterben kapott telepessel egyező telepes (a == operátort felhasználva). 
Ha van ilyen telepes, akkor a lakok tömbben legelőször előfordulót törölni kell.
A törlés során eggyel balra kell shiftelni a lakókat tároló tömb tartalmát, az aktuális létszám karbantartásával pedig jelezni kell, hol ér véget a tömb.
*/
void test_kolonia_minusz() {
#if defined TEST_kolonia_minusz && !defined TEST_BIRO
    Telepes t1, t2;
    t1.set_nev("Jules Verne");
    t2.set_nev("Jack London");
    Kolonia k;
    k+=t1;
    k+=t2;
    const Telepes t3(t2);
    Kolonia k2 = k - t3;
    assert(k2.get_letszam() == 1);
#endif
}


int main() {

    test_telepes_osztaly();
    test_kolonia_osztaly1();
    test_kolonia_osztaly2();
    test_operator_plusz();
    test_vegan_kolonia();
    test_operator_plusz2();
    test_kolonia_operator_plusz();
    test_telepes_egyenloseg();
    test_kolonia_minusz();


    return 0;
}