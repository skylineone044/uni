#include <iostream>
#include <string>
#include <cassert>

using namespace std;

/////////////////////////
//Ide dolgozz!!

class Horcsog {
    string nev;
    unsigned int kor;
    bool nosteny;

public:
    const string &get_nev() const {
        return nev;
    }

    void set_nev(const string &nev) {
        Horcsog::nev = nev;
    }

    unsigned int get_kor() const {
        return kor;
    }

    void set_kor(unsigned int kor) {
        Horcsog::kor = kor;
    }

    bool get_nosteny() const {
        return nosteny;
    }

    void set_nosteny(bool nosteny) {
        Horcsog::nosteny = nosteny;
    }

    Horcsog(const string &nev="", unsigned int kor=0, bool nosteny=false) : nev(nev), kor(kor), nosteny(nosteny) {}

    const string print() const {
        return "" + nev + " neme:" + to_string((int)this->nosteny);
    }

    bool operator||(const Horcsog& h2) const {
        return this->get_kor() == h2.get_kor() && this->get_nosteny() != h2.get_nosteny();
    }

    Horcsog operator++() {
        if (this->get_kor() >= 10) {
            cout << "A horcsog halott" << endl;
            return *this;
        } else {
            this->kor++;
        }
        return *this;
    }

    Horcsog operator++(int) {
        Horcsog old = *this;
        if (this->get_kor() >= 10) {
            cout << "A horcsog halott" << endl;
            return old;
        } else {
            this->kor++;
        }
        return old;
    }
};

class Ketrec {
    unsigned int letszam;
    Horcsog horcsogok[5];

public:
    unsigned int get_letszam() const {
        return letszam;
    }

    const Horcsog *get_horcsogok() const {
        return horcsogok;
    }

    Ketrec() {
        this->letszam = 0;
    }

    Ketrec operator*=(const Horcsog& h) {
        if (this->letszam < 5) {
            if (this->letszam > 1) {
                if (this->horcsogok[0].get_nosteny() != h.get_nosteny()) {
                    cout << "" + h.get_nev() + " neme nem megfelelo" << endl;
                    return *this;
                }
            }
            this->horcsogok[this->letszam] = h;
            this->letszam++;
        } else {
            cout << "A ketrec megtelt" << endl;
        }
        return *this;
    }

    Horcsog& operator[](int index) {
        if (index >= 0 && index < this->letszam) {
            return this->horcsogok[index];
        } else {
            return this->horcsogok[0];
        }
    }

    const Horcsog operator[](int index) const {
        if (index >= 0 && index < this->letszam) {
            return this->horcsogok[index];
        } else {
            return this->horcsogok[0];
        }
    }

    double operator!() const {
        if (this->letszam == 0) {
            return 0;
        }
        int osszeg = 0;

        for (int i = 0; i < this->letszam; ++i) {
            osszeg += this->horcsogok[i].get_kor();
        }

        return (double) osszeg / (double) this->letszam;
    }
};
////////////////////////

//=== Teszteles bekapcsolasa kikommentezessel
#define TEST_horcsog_osztaly1
#define TEST_horcsog_osztaly2
#define TEST_print
#define TEST_ketrec
#define TEST_hozzaadas
#define TEST_utodnemzes
#define TEST_prefix_postfix
#define TEST_indexer
#define TEST_atlag
//=== Teszteles bekapcsolas vege


/*
Írj egy Horcsog osztályt. Adattagok: 
  - nev: string
  - kor: unsigned
  - nosteny: bool

  Legyen default konstruktor. 
  Default értékek: string üres, kor 0, nosteny false. 
  
*/
void test_horcsog_osztaly1(){
  #if defined TEST_horcsog_osztaly1 && !defined TEST_BIRO
  Horcsog h1;
  assert(h1.get_nev() == "");
  assert(h1.get_kor() == 0);
  assert(!h1.get_nosteny());

  h1.set_kor(10);
  h1.set_nev("Lajos");
  h1.set_nosteny(true);

  assert(h1.get_nev() == "Lajos");
  assert(h1.get_kor() == 10);
  assert(h1.get_nosteny());
  #endif
}

/*
Legyen paraméteres konstruktor is az osztályban. 
A három adattag beállítására legyenek a konstruktor paraméterei a felsorolás sorrendjében.
*/
void test_horcsog_osztaly2(){
  #if defined TEST_horcsog_osztaly2 && !defined TEST_BIRO
  const Horcsog h1("Jutka", 3, true);
  assert(h1.get_nev() == "Jutka");
  assert(h1.get_kor() == 3);
  assert(h1.get_nosteny());
  #endif
}

/*
Írj egy print metódust a Horcsog osztályhoz, mely sztringet ad vissza. A visszaadott szöveg ez legyen:
"<nev> neme:0|1"
A <nev> érték helyére az adattag legyen beillesztve. 0 jelenti a hím egyedet, 1 a nőstényt.
*/
void test_print(){
  #if defined TEST_print && !defined TEST_BIRO
  const Horcsog h1("Jutka", 3, true);
  string s = h1.print();
  assert(s == "Jutka neme:1");
  #endif
}

/*
Írj egy Ketrec  osztályt. Privát adattagjai:
- letszam:unsigned
- horcsogok:Horcsog[5] (max 5 hörcsögöt képes tárolni)
Legyen default konstruktor. A létszám kezdetben 0.
*/
void test_ketrec(){
  #if defined TEST_ketrec && !defined TEST_BIRO
  const Ketrec k;
  assert(k.get_letszam() == 0);
  assert(k.get_horcsogok()[4].get_kor() == 0);
  #endif
}

/*
Valósítsd meg a *= operátort, amely hörcsögöt ad a ketrechez, ha a létszám nem haladja meg a tömb méretét. 
A paraméter tehát egy hörcsög objektum legyen.
- ha meghaladja a létszám az ötöt ez a hibaüzenet legyen csak kiírva, utána sortöréssel: "A ketrec megtelt"
- a hörcsög akkor sem kerülhet be a ketrecbe, ha a neme nem azonos a ketrec többi lakójáéval. Ilyenkor a hibaüzenet: "<nev> neme nem megfelelo"
*/
void test_hozzaadas(){
  #if defined TEST_hozzaadas && !defined TEST_BIRO
  Ketrec k;
  const Horcsog h("Gyula", 2, false);
  k*=h;
  assert(k.get_letszam() == 1);
  #endif
}

/*
Valósítsd meg a || operátort két hörcsög objektumra. Figyelj oda rá, hogy konstans hörcsögökön is működjön. 
Az operátor akkor ad vissza igazat, ha a hörcsögök tudnak utódot nemzeni egymással. 
Ez csak akkor lehetséges, ha mindkettő életkora azonos, a nemük viszont eltérő. 
*/
void test_utodnemzes(){
  #if defined TEST_utodnemzes && !defined TEST_BIRO
  const Horcsog h1("Gyula", 2, false), h2("Manci", 2, true);
  assert(h1 || h2);
  #endif
}

/*
Valósítsd meg a prefix és a postfix ++ operátort hörcsög objektumra. 
A funkciója az életkor növelése. Ha az életkor a növeléssel 10 fölé menne, akkor nem kell tovább növelni, 
hanem az alábbi hibaüzenetet kell kiírni (utána sortörés): "A horcsog halott".
*/
void test_prefix_postfix(){
  #if defined TEST_prefix_postfix && !defined TEST_BIRO
  Horcsog h1("Gyula", 2, false);
  h1++;
  assert(h1.get_kor() == 3);
  ++h1;
  assert(h1.get_kor() == 4);
  #endif
}

/*
Valósítsd meg a [ ] (indexelő) operátort a ketrecre, hogy lehessen a tárolt hörcsögöket közvetlenül indexelni. 
Működjön konstans és nem konstans objektumra is, nem konstans objektum esetén lehessen módosítani is az indexelt hörcsögöt. 
Ha a kapott index több mint a tárolt hörcsögök száma vagy negatív akkor a 0 indexű tömbelem (hörcsög) legyen visszaadva.
*/
void test_indexer(){
  #if defined TEST_indexer && !defined TEST_BIRO
  Ketrec k;
  const Horcsog h("Gyula", 2, false);
  k*=h;
  assert(k[0].get_nev() == "Gyula");
  k[0].set_nev("Lajos");
  assert(k[0].get_nev() == "Lajos");
  #endif
}

/*
Valósítsd meg az unáris ! operátort a ketrec osztályra, úgy, hogy double értéket adjon vissza. 
A metódus adja vissza a ketrecben tárolt hörcsögök életkorának átlagát. 
Ez a metódus is működjön konstans ketrec objektumra. Ha nincs tárolt hörcsög, akkor 0 legyen visszaadva.
*/
void test_atlag(){
  #if defined TEST_atlag && !defined TEST_BIRO
  Ketrec k;
  const Horcsog h1("Gyula", 2, false), h2("Janos", 4, false);
  k*=h1;
  k*=h2;
  double atlag = !k;
  assert(atlag == 3.0);
  #endif
}

int main(){
  test_horcsog_osztaly1();
  test_horcsog_osztaly2();
  test_print();
  test_ketrec();
  test_hozzaadas();
  test_utodnemzes();
  test_prefix_postfix();
  test_indexer();
  test_atlag();

  return 0;
}
