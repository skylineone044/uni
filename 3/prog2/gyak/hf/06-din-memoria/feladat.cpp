#include <iostream>
#include <cassert>

using std::string;

/////////////////////////
//Ide dolgozz!!
class VilagitoDisz {
    unsigned int fenyesseg;
    bool bekapcsolva;

public:
    unsigned int get_fenyesseg() const {
        return (this->bekapcsolva ? this->fenyesseg : 0);
    }

    bool is_bekapcsolva() const {
        return bekapcsolva;
    }

    void set_bekapcsolva(bool bekapcsolva=false) {
        VilagitoDisz::bekapcsolva = bekapcsolva;
    }

    VilagitoDisz(unsigned int fenyesseg=0) : fenyesseg(fenyesseg) {}

    VilagitoDisz operator++() {
        this->bekapcsolva = true;
        return *this;
    }

    VilagitoDisz operator--() {
        this->bekapcsolva = false;
        return *this;
    }

    VilagitoDisz(const VilagitoDisz& masik) {
        this->fenyesseg = masik.fenyesseg;
        this->bekapcsolva = masik.is_bekapcsolva();
    }

    VilagitoDisz operator=(VilagitoDisz masik) {
        this->fenyesseg = masik.fenyesseg;
        this->bekapcsolva = masik.bekapcsolva;
        return *this;
    }

};

class KisKaracsonyfa {
    VilagitoDisz* csucs_disz;
    string fa_tipus;

public:
    VilagitoDisz *get_csucs_disz() const {
        return csucs_disz;
    }

    const string &get_fa_tipus() const {
        return fa_tipus;
    }

    KisKaracsonyfa(const string &faTipus="luc") : fa_tipus(faTipus) {
        this->csucs_disz = nullptr;
    }

    void disz_felhelyezese(const VilagitoDisz& disz) {
        if (this->csucs_disz != nullptr) {
            delete this->csucs_disz;
            this->csucs_disz = nullptr;
        }
        this->csucs_disz = new VilagitoDisz(disz);
    }

    ~KisKaracsonyfa() {
        delete this->csucs_disz;
    }

    void bekapcsol() {
        if (this->csucs_disz != nullptr) {
            this->csucs_disz->set_bekapcsolva(true);
        }
    }

    void kikapcsol() {
        if (this->csucs_disz != nullptr) {
            this->csucs_disz->set_bekapcsolva(false);
        }
    }

    unsigned int get_fenyesseg() {
        if (this->csucs_disz == nullptr) {
            return 0;
        } else {
            return this->csucs_disz->get_fenyesseg();
        }
    }

    KisKaracsonyfa(const KisKaracsonyfa& masik) {
        this->fa_tipus = masik.fa_tipus;
        this->csucs_disz = new VilagitoDisz(*masik.csucs_disz);
    }

    KisKaracsonyfa operator=(const KisKaracsonyfa& masik) {
        this->fa_tipus = masik.fa_tipus;
        delete this->csucs_disz;
        this->csucs_disz = new VilagitoDisz(*masik.csucs_disz);
        return *this;
    }
};

class NagyKaracsonyfa {
    VilagitoDisz* diszek;
    unsigned int diszek_szama;
    unsigned int act = 0;

public:
    const VilagitoDisz *get_diszek() const {
        return diszek;
    }

    unsigned int get_act() const {
        return act;
    }

    NagyKaracsonyfa(unsigned int diszekSzama) : diszek_szama(diszekSzama) {
        this->diszek = new VilagitoDisz[diszekSzama];
    }

    ~NagyKaracsonyfa() {
        delete[] this->diszek;
    }

    void disz_felhelyezese(VilagitoDisz disz) {
        if (act < diszek_szama) {
            this->diszek[act] = disz;
            act++;
        }
    }

    void bekapcsol() {
        for (int i = 0; i < this->act; ++i) {
            ++this->diszek[i];
        }
    }

    void kikapcsol() {
        for (int i = 0; i < this->act; ++i) {
            --this->diszek[i];
        }
    }

    unsigned int get_fenyesseg() {
        unsigned int sum = 0;
        for (int i = 0; i < this->act; ++i) {
            sum += this->diszek[i].get_fenyesseg();
        }
        return sum;
    }

    NagyKaracsonyfa(const NagyKaracsonyfa& masik) {
        this->diszek_szama = masik.diszek_szama;
        this->diszek = new VilagitoDisz[this->diszek_szama];
        this->act = 0;
        for (; this->act < masik.act; ++this->act) {
            this->diszek[this->act] = VilagitoDisz(masik.diszek[this->act]);
        }
    }

    NagyKaracsonyfa operator=(const NagyKaracsonyfa& masik) {
        delete[] this->diszek;
        this->diszek_szama = masik.diszek_szama;
        this->diszek = new VilagitoDisz[this->diszek_szama];
        this->act = 0;
        for (; this->act < masik.act; ++this->act) {
            this->diszek[this->act] = VilagitoDisz(masik.diszek[this->act]);
        }
        return *this;
    }

};
////////////////////////

//=== Teszteles bekapcsolasa kikommentezessel
#define TEST_vilagitodisz
#define TEST_pre_operators
#define TEST_kiskaracsonyfa
#define TEST_felhelyez
#define TEST_kibekapcsol
#define TEST_get_fenyesseg
#define TEST_masolo_ctor
#define TEST_ertekadas_operator
#define TEST_nagykaracsonyfa
#define TEST_felhelyez_nagykfa
#define TEST_get_fenyesseg_nagykfa
#define TEST_masolo_ctor_nagykfa
#define TEST_ertekadas_operator_nagykfa
//=== Teszteles bekapcsolas vege

/*
Készíts egy VilagitoDisz nevű osztályt, mely egy karácsonyfa díszítésére alkalmas! Adattagok:
- fenyesseg: a dísz fényessége (unsigned)
- bekapcsolva: a dísz állapota, világít-e vagy sem (bool) 

Készítsd el a VilagitoDisz konstruktorát is, mely a fényességet várja és állítja be.
Default konstruktorként (default paraméter érték) a 0 értéket állítsa be! A bekapcsolva adattag default értéke false.
*/
void test_vilagitodisz(){
    #if defined TEST_vilagitodisz && !defined TEST_BIRO
    const VilagitoDisz vd;
    assert(vd.get_fenyesseg() == 0);
    assert(!vd.is_bekapcsolva());

    const VilagitoDisz vd2(50);
    assert(!vd2.is_bekapcsolva());
    #endif
    
}


/*
Definiáld felül a VilagitoDisz pre ++ és pre -- operátorát!
A ++ operátor kapcsolja be a világítást, a -- operátor kapcsolja ki azt.
Az operátorok pre verzióként működjenek, azaz a módosított értékkel az eredeti objektumot adják vissza!
*/
void test_pre_operators(){
    #if defined TEST_pre_operators && !defined TEST_BIRO
    VilagitoDisz vd;
    ++vd;
    assert(vd.is_bekapcsolva());
    --vd;
    assert(!vd.is_bekapcsolva());
    #endif
    
}

/*
Készíts egy KisKaracsonyfa nevű osztályt! Adattagok:
- csucs_disz: a KisKarácsonyfára helyezhető egyetlen dísz (VilagitoDisz*)  & KisKarácsonyfára helyezhető egyetlen dísz   \\ \hline
- fa_tipus: a fa típusa  &   std::string   & A fa típusa                                 \\ \hline


Valósítsd meg a KisKaracsonyfa konstruktorát, mely a fa típusát várja paraméterként!
Default paraméter értékként a "luc" értéket add meg!
A csucs_disz paramétert nullptr -el inicializálja!
*/
void test_kiskaracsonyfa(){
    #if defined TEST_kiskaracsonyfa && !defined TEST_BIRO
    KisKaracsonyfa fa;
    assert(fa.get_fa_tipus() == "luc");
    assert(fa.get_csucs_disz() == nullptr);

    KisKaracsonyfa fa2("cedrus");
    assert(fa2.get_fa_tipus() == "cedrus");
    assert(fa2.get_csucs_disz() == nullptr);
    #endif
}


/*
Írj egy disz_felhelyezese metódust, mely egy VilagitoDisz-t vár paraméterben, figyelj annak pontos típusára!
A metódus törölje a fán lévő díszt (ha van) és foglaljon dinamikusan egy új díszt, lemásolva a paraméterben kapottat.
A metódus ne térjen vissza semmivel.
*/
void test_felhelyez(){
    #if defined TEST_felhelyez && !defined TEST_BIRO
    KisKaracsonyfa fa;
    {
        const VilagitoDisz vd;
        fa.disz_felhelyezese(vd);
        assert(fa.get_csucs_disz()->get_fenyesseg() == 0);
    }
    {
        VilagitoDisz vd2(20);
        ++vd2;
        fa.disz_felhelyezese(vd2);
        assert(fa.get_csucs_disz()->get_fenyesseg() == 20);
    }
    #endif

}


/*
Legyen egy void bekapcsol() és egy void kikapcsol() metódus, melyek rendre be vagy kikapcsolják a csúcsdíszt, ha van a fán!
*/
void test_kibekapcsol(){
    #if defined TEST_kibekapcsol && !defined TEST_BIRO
    KisKaracsonyfa fa;
    const VilagitoDisz vd2(20);

    fa.disz_felhelyezese(vd2);
    fa.bekapcsol();
    assert(fa.get_csucs_disz()->is_bekapcsolva());

    fa.kikapcsol();
    assert(!(fa.get_csucs_disz()->is_bekapcsolva()));
    #endif
}


/*
Legyen egy get_fenyesseg metódus, mely a fa aktuális fényességét határozza meg!
Ha nincsen csúcsdísz a fán, adjon vissza nullát, különben a dísz fényességét!
Visszatérési típusát a csúcsdísz metódusához igazítsd! (Egyezzen meg vele!)
*/
void test_get_fenyesseg(){
    #if defined TEST_get_fenyesseg && !defined TEST_BIRO
    KisKaracsonyfa fa;
    VilagitoDisz vd2(20);
    ++vd2; //bekapcsol
    fa.disz_felhelyezese(vd2);
    assert(fa.get_fenyesseg() == 20);
    #endif
}


/*
Valósítsd meg a KisKaracsonyfa másoló konstruktorát, mely az adattagokat lemásolja!
Figyelj a dinamikusan foglalt adattagok helyes kezelésére!
*/
void test_masolo_ctor(){
    #if defined TEST_masolo_ctor && !defined TEST_BIRO
    KisKaracsonyfa fa;
    VilagitoDisz vd2(20);
    ++vd2; //bekapcsol
    fa.disz_felhelyezese(vd2);
    assert(fa.get_fenyesseg() == 20);
    {
        KisKaracsonyfa fa2(fa);
        assert(fa.get_csucs_disz()->get_fenyesseg() == 20);
    }
    assert(fa.get_csucs_disz()->get_fenyesseg() == 20);
    #endif
}


/*
Valósítsd meg a értékadás (assignment) operátort!
Az operátor kezelje a lehetséges hibákat és hasonlóan működjön a másoló konstruktorhoz!
Figyelj a dinamikusan foglalt adattagok helyes kezelésére!
*/
void test_ertekadas_operator(){
    #if defined TEST_ertekadas_operator && !defined TEST_BIRO
    KisKaracsonyfa fa1("cedrus");
    {
        KisKaracsonyfa fa2;
        VilagitoDisz vd1(20), vd2(30);
        ++vd1; //bekapcsol
        ++vd2; //bekapcsol
        fa1.disz_felhelyezese(vd1);
        fa2.disz_felhelyezese(vd2);
        fa1=fa2;
    }
    assert(fa1.get_csucs_disz()->get_fenyesseg() == 30);
    assert(fa1.get_fa_tipus() == "luc");
    #endif
}

/*
Valósítsd meg a NagyKaracsonyfa osztályt!
Adattagjai:
- diszek: a NagyKarácsonyfára helyezhető díszek tömbje (VilagitoDisz*) 
- diszek_szama: a fára helyezhető díszek száma (unsigned) 
- act: a tömb üres pozícióját jelző változó (unsigned) 

Legyen egy konstruktora, mely a díszek számát várja!
Állítsa be az értékeket és a megadott elemszámú memóriát foglaljon VilagitoDisz-eknek.
*/
void test_nagykaracsonyfa(){
    #if defined TEST_nagykaracsonyfa && !defined TEST_BIRO
    NagyKaracsonyfa fa(10);
    assert(fa.get_diszek()[9].get_fenyesseg() == 0);
    #endif
}

/*
Legyen egy void disz_felhelyezese metódus, mely az aktuális pozícióra a paraméterben kapott VilagitoDisz-t helyezi el!
Az üres hely pozícióját frissítse!
Ha nem fér el több dísz a fán, ne tegyen semmit!
*/
void test_felhelyez_nagykfa(){
    #if defined TEST_felhelyez_nagykfa && !defined TEST_BIRO
    NagyKaracsonyfa fa(10);
    VilagitoDisz vd1(20), vd2(30);
    ++vd1; //bekapcsol
    ++vd2; //bekapcsol
    fa.disz_felhelyezese(vd1);
    fa.disz_felhelyezese(vd2);
    assert(fa.get_diszek()[0].get_fenyesseg() == 20);
    assert(fa.get_diszek()[1].get_fenyesseg() == 30);
    #endif
}

/*
Legyen egy void bekapcsol() és egy void kikapcsol() metódus, melyek rendre be- és kikapcsolják az éppen fán lévő díszeket (mindet)!
Csak azokat, melyeket már felraktunk a fára!
*/
void test_kibekapcsol_nagykfa(){
    #if defined test_kibekapcsol_nagykfa && !defined TEST_BIRO
    NagyKaracsonyfa fa(10);
    const VilagitoDisz vd1(20), vd2(30);
    fa.disz_felhelyezese(vd1);
    fa.disz_felhelyezese(vd2);
    fa.bekapcsol();
    assert(fa.get_diszek()[0].is_bekapcsolva());
    assert(fa.get_diszek()[1].is_bekapcsolva());
    assert(!(fa.get_diszek()[2].is_bekapcsolva()));
    fa.kikapcsol();
    assert(!(fa.get_diszek()[0].is_bekapcsolva()));
    assert(!(fa.get_diszek()[1].is_bekapcsolva()));
    assert(!(fa.get_diszek()[2].is_bekapcsolva()));
    #endif
}

/*
Legyen egy unsigned get_fenyesseg metódus, mely a fa fényességét adja meg!
A fa fényessége az éppen rajta lévő díszek fényességének összege.
*/
void test_get_fenyesseg_nagykfa(){
    #if defined TEST_get_fenyesseg_nagykfa && !defined TEST_BIRO
    NagyKaracsonyfa fa(10);
    VilagitoDisz vd1(20), vd2(30);
    ++vd1; //bekapcsol
    ++vd2; //bekapcsol
    fa.disz_felhelyezese(vd1);
    fa.disz_felhelyezese(vd2);
    assert(fa.get_fenyesseg() == 50);
    #endif
}


/*
Valósítsd meg a NagyKaracsonyfa másoló konstruktorát!
Figyelj oda a dinamikus elemek helyes másolására!
*/
void test_masolo_ctor_nagykfa(){
    #if defined TEST_masolo_ctor_nagykfa && !defined TEST_BIRO
    NagyKaracsonyfa fa(10);
    VilagitoDisz vd1(20), vd2(30);
    ++vd1; //bekapcsol
    ++vd2; //bekapcsol
    fa.disz_felhelyezese(vd1);
    fa.disz_felhelyezese(vd2);
    {
        NagyKaracsonyfa fa2(fa);
        assert(fa2.get_diszek()[0].get_fenyesseg() == 20);
        assert(fa2.get_diszek()[1].get_fenyesseg() == 30);
    }
    assert(fa.get_diszek()[0].get_fenyesseg() == 20);
    assert(fa.get_diszek()[1].get_fenyesseg() == 30);
    #endif
}

/*
Valósítsd meg az értékadás (assignment) operátort!
Figyelj a lehetséges hibákra, és az operátor hasonlóan működjön a másoló konstruktorhoz!
Figyelj oda a dinamikus elemek helyes másolására!
*/
void test_ertekadas_operator_nagykfa(){
    #if defined TEST_ertekadas_operator_nagykfa && !defined TEST_BIRO
    NagyKaracsonyfa fa(10);
    VilagitoDisz vd1(20), vd2(30);
    ++vd1; //bekapcsol
    ++vd2; //bekapcsol
    fa.disz_felhelyezese(vd1);
    fa.disz_felhelyezese(vd2);
    assert(fa.get_diszek()[0].get_fenyesseg() == 20);
    assert(fa.get_diszek()[1].get_fenyesseg() == 30);
    {
        NagyKaracsonyfa fa2(3);
        fa2.disz_felhelyezese(vd2);
        fa2.disz_felhelyezese(vd1);
        assert(fa2.get_diszek()[0].get_fenyesseg() == 30);
        assert(fa2.get_diszek()[1].get_fenyesseg() == 20);
        fa = fa2;
    }
    assert(fa.get_diszek()[0].get_fenyesseg() == 30);
    assert(fa.get_diszek()[1].get_fenyesseg() == 20);
    #endif
}

int main(){
    test_vilagitodisz();
    test_pre_operators();
    test_kiskaracsonyfa();
    test_felhelyez();
    test_kibekapcsol();
    test_get_fenyesseg();
    test_masolo_ctor();
    test_ertekadas_operator();
    test_nagykaracsonyfa();
    test_felhelyez_nagykfa();
    test_get_fenyesseg_nagykfa();
    test_masolo_ctor_nagykfa();
    test_ertekadas_operator_nagykfa();

}