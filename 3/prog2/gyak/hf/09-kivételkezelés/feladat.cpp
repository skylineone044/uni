#include <iostream>
#include <string>
#include <stdexcept>
#include <cassert>

using namespace std;

/////////////////////////
//Ide dolgozz!!
////////////////////////

//=== Teszteles bekapcsolasa kikommentezessel
//#define TEST_etel1
//#define TEST_etel2
//#define TEST_print
//#define TEST_hibaosztalyok
//#define TEST_hutoegyseg
//#define TEST_operator_plusz
//#define TEST_print_hutoegyseg
//#define TEST_feltolt
//=== Teszteles bekapcsolas vege

/*
Készíts egy Etel nevű struktúrát! Két publikus adattagja van:
- nev: az étel neve
- hutes: az étel hűtési igénye (int)
Legyen egy default konstruktora, amely a sztringet üresre, az int-et 0-ra állítja be
*/
void test_etel1(){
  #ifdef TEST_etel1
  Etel e;
  assert(e.nev == "");
  assert(e.hutes == 0);
  #endif
}

/*
Legyen egy paraméteres konstruktora, melynek első paramétere egy string, a második egy int és beállítja a két adattagot.
*/
void test_etel2(){
  #ifdef TEST_etel2
  Etel e("uborka", 10);
  assert(e.nev == "uborka");
  assert(e.hutes == 10);
  #endif
}


/*
Készíts egy print() publikus metódust az Etel struktúrához. 
A visszaadott sztring tartalma az alábbi legyen: "nev:<nev>,hofok:<hutes>". A <nev> és a <hutes> helyettesítődjenek az adattagokkal, a vessző után nincs szóköz.
*/
void test_print(){
  #ifdef TEST_print
  Etel e("uborka", 10);
  string s = e.print();
  assert(s == "nev:uborka,hofok:10");
  #endif
}

/*
Készítsd el a program hibaosztályait!

- Hiba. Ősosztály. Az alábbiakkal rendelkezik:
    * Egy virtuális, stringet visszaadó hiba() nevű metódus, mely ezt a sztringet adja vissza: "Hiba tortent".
    
- Megromlik. Publikusan öröklődik a Hiba ősosztályból. Akkor kell dobni, ha az ételt rossz hőfokon tároljuk le. Az alábbiakkal rendelkezik:
    * Egy privát, Etel típusú adattag (neve etel)
    * Egy konstruktor, amely egy Etel-re mutató konstans referenciát vár paraméterül és inicializálja az adattagot.
    * Felüldefiniálja az őse hiba() metódusát úgy, hogy az alábbi sztringet adja vissza:
        -"A <etel.nev> tarolasahoz legalabb <etel.hutes> fok kell"
        - Ahol a kacsacsőrök közötti részek helyettesítődnek a hiba objektumban letárolt etel megfelelő adattagjaival
    
- Megtelt. Publikusan öröklődik a Hiba ősosztályból. Az alábbiakkal rendelkezik:
    * Egy privát, string típusú adattag (neve mit)
    * Egy konstruktor, amely egy egy string-et vár paraméterül és beállítja az adattagot.
    * Felüldefiniálja az őse hiba() metódusát úgy, hogy az alábbi sztringet adja vissza:
        - "Nem sikerult letarolni a <mit>-t"
        - Ahol a kacsacsőrök közötti rész helyettesítődik az adattaggal

*/
void test_hibaosztalyok(){
  #ifdef TEST_hibaosztalyok
  Hiba h;
  string hibauzenet = h.hiba();
  assert(hibauzenet == "Hiba tortent");

  Etel e("kifli", 10);
  Hiba * h2 = new Megromlik(e);
  hibauzenet = h2->hiba();
  assert(hibauzenet == "A kifli tarolasahoz legalabb 10 fok kell");
  delete h2;

  Hiba * h3 = new Megtelt("sajt");
  hibauzenet = h3->hiba();
  assert(hibauzenet == "Nem sikerult letarolni a sajt-t");
  delete h3;
  #endif
}


/*
Készíts egy Hutoegyseg nevű osztályt!
Adattagok:

- hofok: hűtési hőfok (int) 
- tartalom: az egységben tárolt ételek, max 10 (Etel[10]) 
- aktualis: az aktuálisan tárolt étel mennyiség (unsigned)

Készítsd el a Hutoegyseg osztály konstruktorát is! 
Egy string paramétert vár, amely a hűtőegység hűtési hőfokát reprezentálja sztringes formában. 
Konvertáld intté a kapott értéket, de figyelj oda rá, hogy hiba esetén a konstruktor ne dobjon kivételt, hanem állítsa be -1-re a hőfokot! 
Az aktuális méret kezdetben 0.
*/
void test_hutoegyseg(){
  #ifdef TEST_hutoegyseg
  const Hutoegyseg egyseg("5");
  assert(egyseg.get_hofok() == 5);
  assert(egyseg.get_tartalom()[9].nev == "");
  assert(egyseg.get_aktualis() == 0);
  #endif
}

/*
Valósítsd meg a += operátort a Hutoegyseg osztályban. 
Paramétere egy Etel legyen! Az operátor feladata, hogy hozzáadja a paraméterben kapott ételt a tartalom tömbhöz. 
Legyen benne hibakezelés is! Először a hőmérséklet megfelelősége legyen vizsgálva. Az étel csak akkor tehető be a hűtőegységbe, 
ha hutes értéke nagyobb vagy egyenlő, mint a hűtőegység által biztosított hofok. 
Ha ez nem teljesül, akkor legyen egy Megromlik kivétel dobva, mely kivétel a paraméterben kapott étellel van inicializálva. 
Ha a hőfok feltétel teljesül, akkor az legyen ellenőrizve másodiknak, hogy van-e még hely a tömbben. 
Ha nincs, akkor legyen egy Megtelt kivétel dobva az étel nevével inicializálva.
*/
void test_operator_plusz(){
  #ifdef TEST_operator_plusz
  Hutoegyseg egyseg("15");
  Etel e1("keksz", 20), e2("csirkecomb", 5);

  egyseg += e1;
  assert(egyseg.get_aktualis() == 1);
  assert(egyseg.get_tartalom()[0].nev == "keksz");
  try{
    egyseg += e2;
    assert(false);
  }catch(const Hiba& h){
    string hibauzenet = h.hiba();
    assert(hibauzenet == "A csirkecomb tarolasahoz legalabb 5 fok kell");
  }
  assert(egyseg.get_aktualis() == 1);
  #endif
}

/*
Készíts egy print() publikus metódust a Hutoegyseg osztályhoz. 
Feladata, hogy végigmenjen az aktuálisan tárolt ételeken és a standard outputra kiírja, amit az ételek print() metódusa visszaad. 
Az egyes print hívások után legyen mindig egy-egy soremelés is a standard outputra írva. Legyen Hiba típusú kivétel dobva, ha a hűtőegységben nincs étel tárolva.
*/
void test_print_hutoegyseg(){
  #ifdef TEST_print_hutoegyseg
  const Hutoegyseg egyseg("15");
  try{
    egyseg.print();
    assert(false);
  }catch(const Hiba& h){
    string hibauzenet = h.hiba();
    assert(hibauzenet == "Hiba tortent");
  }
  #endif
}

/*
Készíts egy globális (osztályokon kívüli) feltolt(Hutoegyseg& h, Etel etelek[], unsigned etelszam) függvényt, 
amelynek feladata, hogy feltöltse a hűtőt a += operátor meghívogatásával. 
Menjen végig a függvény a paraméterben kapott tömb elemein. A hosszt a harmadik paraméter adja meg. 
Mivel a Hutoegyseg interfészéből nem kérdezhető le a kapacitás, így előzetes méretellenőrzést nem kell/nem lehet belerakni. Fontos, hogy a feltolt függvényből ne dobódjon kivétel. 
Ha hiba történne fejeződjön be a hűtő feltöltése és legyen false érték visszaadva. Ha minden étel letárolása sikerült, akkor legyen true visszaadva.
*/
void test_feltolt(){
  #ifdef TEST_feltolt
  Hutoegyseg egyseg("2");
  Etel etelek[] = {Etel("kacsa", 5), Etel("repa", 10)};
  bool result = feltolt(egyseg, etelek, 2);
  assert(result);
  #endif
}

int main(){

  test_etel1();
  test_etel2();
  test_print();
  test_hibaosztalyok();
  test_hutoegyseg();
  test_operator_plusz();
  test_print_hutoegyseg();
  test_feltolt();

  return 0;
}
