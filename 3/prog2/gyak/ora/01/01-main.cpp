#include <iostream> //alapertelmezett input/output muveletekhez, a stdio.h c++-beli megfeleloje
// Viszont amire mindig oda kell figyelni, hogy ez nem iostream.h!
// Maga a header fajl jo esellyel letezik, de ez nem az, amit szeretnenk hasznalni a felev soran

// az std nevteret hasznaljuk (kesobb lesz rola szo)
using namespace std;

// a main fuggveny teljesen megegyezik a c-ben tanultakkal
// int main() {
// int main(int argc, char** argv, char** envp) {
int main(int argc, char **argv) {
    cout << "Hello World!" << endl;
    return 0;
    // a return 0; utasitas akar el is hagyhato, ezt a C++ standardban definialtak:
    // http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2014/n4296.pdf 3.6.1. fejezet, 5. pont
}
