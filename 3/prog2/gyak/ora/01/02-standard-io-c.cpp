#include <stdio.h>

int main(int argc, char *argv[]) {
    int kor = 0;
    float magassag = 0.0;
    char nev[10];

    printf("Adja meg a korat (evek szama) es magassagat szokozzel elvalasztva: ");
    scanf("%d %f", &kor, &magassag);
    printf("\n");

    printf("Adja meg a nevet: ");
    scanf("%s", nev);

    printf("Nev: %s, kor: %d, magassaga: %f", nev, kor, magassag);

    return 0;
}