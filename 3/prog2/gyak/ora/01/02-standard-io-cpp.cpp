#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
    int kor = 0;
    float magassag = 0.0;
    char nev[10];

    cout << "Adja meg a korat (evek szama) es magassagat szokozzel elvalasztva: ";
    cin >> kor >> magassag;

    cout << "Adja meg a nevet: ";
    cin >> nev;

    cout << "Nev: " << nev << " kor: " << kor << " ev, magassag: " << magassag << " cm" << endl;

    return 0;
}