cmake_minimum_required(VERSION 3.16)
project(gyak01)

set(CMAKE_CXX_STANDARD 14)

add_executable(01_c 01-main-c.cpp)
add_executable(01_cpp 01-main.cpp)
add_executable(02_c 02-standard-io-c.cpp)
add_executable(02_cpp 02-standard-io-cpp.cpp)
add_executable(default main.cpp)