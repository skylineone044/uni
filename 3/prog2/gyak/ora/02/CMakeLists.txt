cmake_minimum_required(VERSION 3.20)
project(gyak02)

set(CMAKE_CXX_STANDARD 14)

add_executable(string string.cpp)
add_executable(ufo1 ufo1.cpp)
add_executable(ufo2 ufo2.cpp)
add_executable(ufo3 ufo3.cpp)
add_executable(ufo4 ufo4.cpp)
add_executable(ufo5 ufo5.cpp)