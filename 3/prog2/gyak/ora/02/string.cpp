#include <iostream>
#include <string>

using namespace std;

int main() {
    // beolvasas, kiiras
    string s1 = "cica"; // string s1("cica");

    string s2; // ures string
    cin >> s2;

    cout << s1 << " --- " << s2 << endl << endl;

    // ==
    if (s1 == s2) {
        cout << "A megadott stringek egyformak" << endl << endl;
    } else {
        cout << "A megadott stringek nem egyformak" << endl << endl;
    }

    // <, >, <=, >=
    if (s1 < s2) {
        cout << s1 << " van korabban" << endl << endl;
    } else if (s1 > s2) {
        cout << s2 << " van korabban" << endl << endl;
    }

    // size, length
    if (s1.size() > s2.size()) {
        cout << "A(z) " << s1 << " hosszabb" << endl << endl;
    } else if (s1.size() < s2.size()) {
        cout << "A(z) " << s2 << " hosszabb" << endl << endl;
    } else {
        cout << "Egyforma hosszuak" << endl;
    }

    // +, +=
    string s3 = s1 + s2;
    cout << "Az eredeti szovegek nem valtoztak: " << s1 << ", " << s2 << endl;
    cout << "Az uj szoveg: " << s3 << endl;
    s3 += "lovacska";
    cout << "A legujabb szoveg: " << s3 << endl << endl;

    // index
    cout << "A(z) " << s1  << " legelso karaktere: " << s1[0] << endl;
    s1[0] = 't';
    s1[2] = 't';
    cout << "s1: " << s1 << endl << endl;

    // konverziok
    int szam = 119;
    string konvertalt = to_string(szam); // szam --> szoveg
    cout << "Egesz szam atkonvertalva szovegge: " << konvertalt << endl;

    float ertek = 5.54;
    konvertalt = to_string(ertek);
    cout << "Valos szam atkonvertalva szovegge: " << konvertalt << endl << endl;

    string ss1 = "4662";
    int egesz1 = stoi(ss1); // szoveg --> szam
    cout << "Elso: " << egesz1 << endl;

    string ss2 = "512.73";
    float valos1 = stof(ss2);
    cout << "Masodik: " << valos1 << endl;

    string ss3 = "5-os gyak";
    int egesz2 = stoi(ss3);
    cout << "Harmadik: " << egesz2 << endl;

    // string ss4 = "A macska megevett 5 szelet hust";
    // int egesz3 = stoi(ss4);
    // cout << "Negyedik: " << egesz3 << endl;

    return 0;
}
