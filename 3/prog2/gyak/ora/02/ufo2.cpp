#include <iostream>

using namespace std;

struct Cseszealj {
    unsigned aktualis_terheles = 0;
    unsigned max_terheles;
};

// elrabolunk egy t tomegu targyat
void operator|=(Cseszealj& cs, unsigned t) {
    if (cs.aktualis_terheles + t <= cs.max_terheles) {
        cs.aktualis_terheles += t;
        cout << "Sikeres elrablas" << endl;
    } else {
        cout << "Elrablas nem lehetseges" << endl;
    }
}

int main() {
    Cseszealj cs1;
    cs1.max_terheles = 15;

    // elrabol(cs1, 4);
    // elrabol(cs1, 7);
    // elrabol(cs1, 5);

    cs1 |= 4; // operator|=(cs1, 4);
    cs1 |= 7; // operator|=(cs1, 7);
    cs1 |= 5; // operator|=(cs1, 5);

    cout << "A cseszealj terheltsege: " << cs1.aktualis_terheles << endl;

    return 0;
}