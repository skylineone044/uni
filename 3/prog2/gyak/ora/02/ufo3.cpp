#include <iostream>
#include <string>

using namespace std;

struct Cseszealj {
    unsigned aktualis_terheles = 0;
    unsigned max_terheles;
};

struct Tehen {
    string nev;
    unsigned tomeg;
};

// elrabolunk egy t tomegu targyat
void operator|=(Cseszealj& cs, unsigned t) {
    if (cs.aktualis_terheles + t <= cs.max_terheles) {
        cs.aktualis_terheles += t;
        cout << "Sikeres elrablas" << endl;
    } else {
        cout << "Elrablas nem lehetseges" << endl;
    }
}

// elrabolunk egy tehenet
void operator|=(Cseszealj& cs, Tehen& t) { // t lehetne const
    if (cs.aktualis_terheles + t.tomeg <= cs.max_terheles) {
        cs.aktualis_terheles += t.tomeg;
        cout << t.nev << " elrabolva" << endl;
    } else {
        cout << t.nev << " elrablasa nem lehetseges" << endl;
    }
}

int main() {
    Cseszealj cs1;
    cs1.max_terheles = 15;

    Tehen t1 = {"Milka", 6};
    Tehen t2 = {"Olga", 5};


    cs1 |= 4;
    cs1 |= t1;
    cs1 |= 3;
    cs1 |= t2;

    cout << "A cseszealj terheltsege: " << cs1.aktualis_terheles << endl;

    return 0;
}