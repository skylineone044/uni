#include <iostream>
#include <string>

using namespace std;

struct Cseszealj {
    unsigned aktualis_terheles = 0;
    unsigned max_terheles;
};

struct Tehen {
    string nev;
    unsigned tomeg;
};

// elrabolunk egy t tomegu targyat
void operator|=(Cseszealj& cs, unsigned t) {
    if (cs.aktualis_terheles + t <= cs.max_terheles) {
        cs.aktualis_terheles += t;
        cout << "Sikeres elrablas" << endl;
    } else {
        cout << "Elrablas nem lehetseges" << endl;
    }
}

// elrabolunk egy tehenet
void operator|=(Cseszealj& cs, Tehen& t) { // t lehetne const
    if (cs.aktualis_terheles + t.tomeg <= cs.max_terheles) {
        cs.aktualis_terheles += t.tomeg;
        cout << t.nev << " elrabolva" << endl;
    } else {
        cout << t.nev << " elrablasa nem lehetseges" << endl;
    }
}

Cseszealj operator|(Cseszealj& cs, unsigned t) { // cs es t lehetne const
    Cseszealj uj = cs;
    uj |= t;
    return uj;
}

Cseszealj operator|(Cseszealj& cs, Tehen& t) { // cs es t lehetne const
    Cseszealj uj = cs;
    uj |= t;
    return uj;
}

int main() {
    Cseszealj cs1;
    cs1.max_terheles = 15;

    Tehen t1 = {"Milka", 6};

    Cseszealj cs2 = cs1 | 5;
    Cseszealj cs3 = cs2 | t1;

    cout << "Eredeti cseszealj terheltsege: " << cs1.aktualis_terheles << endl;
    cout << "Uj cseszealj terheltsege: " << cs2.aktualis_terheles << endl;
    cout << "Legujabb cseszealj terheltsege: " << cs3.aktualis_terheles << endl;

    return 0;
}