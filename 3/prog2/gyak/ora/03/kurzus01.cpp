// osztaly, lathatosag, konstruktor, getter-setter
#include <iostream>
#include <string>

using namespace std;

class Kurzus {
private:
    string nev;
    string kod;
    unsigned max_letszam;

public:
    Kurzus(string nev, string kod, unsigned max_letszam) {
        this->nev = nev;
        this->kod = kod;
        this->max_letszam = max_letszam;
    }

    string get_nev() {
        return nev;
    }

    string get_kod() {
        return kod;
    }

    unsigned get_max_letszam() {
        return max_letszam;
    }

    void set_nev(string nev) {
        this->nev = nev;
    }

    void set_kod(string kod) {
        this->kod = kod;
    }

    void set_max_letszam(unsigned max_letszam) {
        this->max_letszam = max_letszam;
    }
};

int main() {
    Kurzus k1("Kalkulus III.", "MBNK39E", 30);
    cout << k1.get_nev() << ", " << k1.get_kod() << ", " << k1.get_max_letszam();

    return 0;
}
