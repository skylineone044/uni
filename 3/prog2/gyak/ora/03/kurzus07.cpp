// parameter atadasi modok
#include <iostream>
#include <string>

using namespace std;

class Kurzus {
private:
    string nev;
    string kod;
    unsigned max_letszam;

public:
    Kurzus(string nev, string kod): Kurzus(nev, kod, 25) {}

    Kurzus(string nev, string kod, unsigned max_letszam): nev(nev), kod(kod), max_letszam(max_letszam) {}

    string get_nev() {
        return nev;
    }

    string get_kod() {
        return kod;
    }

    unsigned get_max_letszam() {
        return max_letszam;
    }

    void set_nev(string nev) {
        this->nev = nev;
    }

    void set_kod(string kod) {
        this->kod = kod;
    }

    void set_max_letszam(unsigned max_letszam) {
        this->max_letszam = max_letszam;
    }

    void letszamot_novel(int noveles) {
        max_letszam += noveles;
    }
};

void letszam_valtoztatas(Kurzus* kurzus) {
    int valtoztatas;
    cin >> valtoztatas;
    kurzus->letszamot_novel(valtoztatas);
}

void letszam_valtoztatas(Kurzus& kurzus) {
    int valtoztatas;
    cin >> valtoztatas;
    kurzus.letszamot_novel(valtoztatas);
}

int main() {
    Kurzus k2("Programozas III.", "IB402G-1");
    cout << k2.get_nev() << ", " << k2.get_kod() << ", " << k2.get_max_letszam() << endl;

    letszam_valtoztatas(&k2);
    cout << k2.get_nev() << ", " << k2.get_kod() << ", " << k2.get_max_letszam() << endl;

    letszam_valtoztatas(k2);
    cout << k2.get_nev() << ", " << k2.get_kod() << ", " << k2.get_max_letszam() << endl;

    return 0;
}
