// parameter atadasi modok
#include <iostream>
#include <string>

using namespace std;

class Kurzus {
private:
    string nev;
    string kod;
    unsigned max_letszam;

public:
    Kurzus(string nev, string kod): Kurzus(nev, kod, 25) {}

    Kurzus(string nev, string kod, unsigned max_letszam): nev(nev), kod(kod), max_letszam(max_letszam) {}

    string get_nev() { return nev; }

    friend class TanulmanyiOsztaly;
};

class TanulmanyiOsztaly {
    string vezeto = "Aniko";

public:
    void kurzus_atnevezes(Kurzus& kurzus, string uj_nev) {
        kurzus.nev = uj_nev;
    }

    string get_vezeto() { return vezeto; }

    friend void vezeto_valtas(TanulmanyiOsztaly& to, string uj_vezeto);
};

void vezeto_valtas(TanulmanyiOsztaly& to, string uj_vezeto) {
    to.vezeto = uj_vezeto;
}

int main() {
    TanulmanyiOsztaly to;

    cout << "Volt vezeto: " << to.get_vezeto() << endl;

    vezeto_valtas(to, "Anett");

    cout << "Uj vezeto: " << to.get_vezeto() << endl;

    Kurzus k1("asd", "asd");

    cout << "Regi kurzusnev: " << k1.get_nev() << endl;

    to.kurzus_atnevezes(k1, "Kozelito es szimbolikus szamitasok III.");

    cout << "Uj kurzusnev: " << k1.get_nev() << endl;

    return 0;
}
