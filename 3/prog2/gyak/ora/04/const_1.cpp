// const valtozok, pointerek

#include <iostream>

using namespace std;

int main() {
    int a = 3;
    int b = 5;

    /* KONSTANS VALTOZOK */
    const int c = 4;
    // c = 7;

    /* KONSTANS MUTATOTT ERTEK */
    const int* p1 = &a;
    // *p1 = 10; // a p1 altal mutatott ertek legyen 10
    p1 = &b; // a p1 mutasson inkabb a 'b'-re

    /* KONSTANS POINTER */
    int* const p2 = &a;
    *p2 = 10; // a p2 altal mutatott ertek legyen 10
    // p2 = &b; // a p2 mutasson inkabb a 'b'-re

    /* KONSTANS POINTER ES MUTATOTT ERTEK */
    const int* const p3 = &a;
    // *p3 = 10; // a p3 altal mutatott ertek legyen 10
    // p3 = &b; // a p3 mutasson inkabb a 'b'-re
}