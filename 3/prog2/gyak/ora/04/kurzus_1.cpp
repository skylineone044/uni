// const metodus, visszateresi ertek, Kurzus osztaly

#include <iostream>
#include <string>

using namespace std;

class Hallgato {
    string nev;
    string neptun;

public:
    Hallgato(const string& nev, const string& neptun): nev(nev), neptun(neptun) {}

    const string& get_nev() const {
        return nev;
    }

    const string& get_neptun() const {
        return neptun;
    }
};

class Kurzus {
    string kurzuskod;
    unsigned int letszam;
    Hallgato hallgatok[10];
};

int main() {
    Hallgato h1("Kiss Janos", "AAAAAA");
    // h1.get_nev() = "macska";
    cout << h1.get_nev();
}