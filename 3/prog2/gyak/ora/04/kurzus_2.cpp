// default konstruktor, default es deleted metodusok

#include <iostream>
#include <string>

using namespace std;

class Hallgato {
    string nev;
    string neptun;

public:
    Hallgato() = default;

    // Hallgato& operator=(const Hallgato&) = delete;

    Hallgato(const string& nev, const string& neptun): nev(nev), neptun(neptun) {}

    const string& get_nev() const {
        return nev;
    }

    const string& get_neptun() const {
        return neptun;
    }
};

class Kurzus {
    string kurzuskod;
    unsigned int letszam;
    Hallgato hallgatok[10];

public:
    Kurzus(const string& kurzuskod): kurzuskod(kurzuskod), letszam(0) {}
};

int main() {
    const Hallgato h1("Kiss Janos", "AAAAAA");
    Hallgato h2("Nagy Anett", "BBBBBB");

    Kurzus k1("IB302G-1");
}