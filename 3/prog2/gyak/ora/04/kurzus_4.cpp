// operator overloading (+ es += kurzus es hallgato objektumra) - osztalyon belul

#include <iostream>
#include <string>

using namespace std;

class Hallgato {
    string nev;
    string neptun;

public:
    Hallgato() = default;

    Hallgato(const string& nev, const string& neptun): nev(nev), neptun(neptun) {}

    const string& get_nev() const {
        return nev;
    }

    const string& get_neptun() const {
        return neptun;
    }
};

class Kurzus {
    string kurzuskod;
    unsigned int letszam;
    Hallgato hallgatok[10];

public:
    Kurzus(const string& kurzuskod): kurzuskod(kurzuskod), letszam(0) {}

    void hallgato_lista() {
        cout << kurzuskod << " hallgatolista:" << endl;
        for (int i = 0; i < letszam; i++) {
            cout << hallgatok[i].get_nev() << endl;
        }
        cout << endl;
    }

    Kurzus& operator+=(const Hallgato& hallgato) {
        if (this->letszam >= 10) {
            cerr << "Mar megtelt a kurzus!" << endl;
            return *this;
        }

        this->hallgatok[this->letszam] = hallgato;
        this->letszam++;

        return *this;
    }

    Kurzus operator+(const Hallgato& hallgato) const {
        if (this->letszam >= 10) {
            cerr << "Mar megtelt a kurzus!" << endl;
            return *this;
        }

        Kurzus uj = *this;
        uj.hallgatok[uj.letszam] = hallgato;
        uj.letszam++;
        return uj;
    }
};

int main() {
    const Hallgato h1("Kiss Janos", "AAAAAA");
    Hallgato h2("Nagy Anett", "BBBBBB");
    Hallgato h3("Kozepes Agnes", "CCCCCC");

    Kurzus k1("IB302G-1");

    k1 += h1;
    k1 += h2;

    k1.hallgato_lista();

    // ---

    Kurzus k2 = k1 + h3;
    k1.hallgato_lista();
    k2.hallgato_lista();
}