// operator overloading (+ es += 2 kurzus objektumra)

#include <iostream>
#include <string>

using namespace std;

class Hallgato {
    string nev;
    string neptun;

public:
    Hallgato() = default;

    Hallgato(const string& nev, const string& neptun): nev(nev), neptun(neptun) {}

    const string& get_nev() const {
        return nev;
    }

    const string& get_neptun() const {
        return neptun;
    }
};

class Kurzus {
    string kurzuskod;
    unsigned int letszam;
    Hallgato hallgatok[10];

public:
    Kurzus(const string& kurzuskod): kurzuskod(kurzuskod), letszam(0) {}

    void hallgato_lista() {
        cout << kurzuskod << " hallgatolista:" << endl;
        for (int i = 0; i < letszam; i++) {
            cout << hallgatok[i].get_nev() << endl;
        }
        cout << endl;
    }

    Kurzus& operator+=(const Hallgato& hallgato) {
        if (this->letszam >= 10) {
            cerr << "Mar megtelt a kurzus!" << endl;
            return *this;
        }

        this->hallgatok[this->letszam] = hallgato;
        this->letszam++;

        return *this;
    }

    Kurzus operator+(const Hallgato& hallgato) const {
        if (this->letszam >= 10) {
            cerr << "Mar megtelt a kurzus!" << endl;
            return *this;
        }

        Kurzus uj = *this;
        uj.hallgatok[uj.letszam] = hallgato;
        uj.letszam++;
        return uj;
    }

    Kurzus& operator+=(const Kurzus& masik) {
        if (this->letszam + masik.letszam > 10) {
            cerr << "Nem egyesithetoek a kurzusok!" << endl;
            return *this;
        }

        unsigned masikLetszam = masik.letszam; // k1 += k1 eset kivedesere, mert akkor a masik.letszam folyamatosan novekszik a cikluson belul
        for (int i = 0; i < masikLetszam; i++) {
            this->hallgatok[this->letszam] = masik.hallgatok[i];
            this->letszam++;
        }

        return *this;
    }

    Kurzus operator+(const Kurzus& masik) const {
        if (this->letszam + masik.letszam > 10) {
            cerr << "Nem egyesithetoek a kurzusok!" << endl;
            return *this;
        }

        Kurzus uj = *this;
        for (int i = 0; i < masik.letszam; i++) {
            uj.hallgatok[uj.letszam] = masik.hallgatok[i];
            uj.letszam++;
        }

        return uj;
    }
};

int main() {
    const Hallgato h1("Kiss Janos", "AAAAAA");
    Hallgato h2("Nagy Anett", "BBBBBB");
    Hallgato h3("Kozepes Agnes", "CCCCCC");

    Kurzus k1("IB302G-1");

    k1 += h1;
    k1 += h2;

    k1.hallgato_lista();

    // ---

    Kurzus k2 = k1 + h3;
    k1.hallgato_lista();
    k2.hallgato_lista();

    // ---

    Kurzus k3 = k1 + k2;
    k3.hallgato_lista();

    k3 += k1;
    k3.hallgato_lista();

    ((k3 += k1) += k1) += k1;
    k3.hallgato_lista();

    k1 += k1;
    k1.hallgato_lista();
}