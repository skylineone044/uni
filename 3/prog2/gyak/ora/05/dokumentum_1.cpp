// Dokumentum osztaly (ismetles), explicit
#include <iostream>
#include <string>

using namespace std;

class Dokumentum {
    string cim;
    string tartalom;
    string szerzo;
    mutable unsigned megnyitasok_szama = 0;

public:
    Dokumentum(const string& cim, const string& tartalom = "", const string& szerzo = "anonym"):
        cim(cim), tartalom(tartalom), szerzo(szerzo) {}

    const string& get_cim() const {
        megnyitasok_szama++;
        return cim;
    }

    const string& get_tartalom() const {
        megnyitasok_szama++;
        return tartalom;
    }

    const string& get_szerzo() const {
        megnyitasok_szama++;
        return szerzo;
    }

    unsigned int get_megnyitasok_szama() const {
        return megnyitasok_szama;
    }
};

void kiir(const Dokumentum& d) {
    cout << d.get_cim() << " " << d.get_tartalom() << " " << d.get_szerzo() << " " << d.get_megnyitasok_szama() << endl;
}

int main() {
    Dokumentum d1("biros hazi", "megcsinalom a biros hazit max pontra", "minden hallgato");
    kiir(d1);

    // What?
    string s = "macska";
    kiir(s);
}