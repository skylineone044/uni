// + es += operator (ismetles)
#include <iostream>
#include <string>

using namespace std;

class Dokumentum {
    string cim;
    string tartalom;
    string szerzo;
    mutable unsigned megnyitasok_szama = 0;

public:
    explicit Dokumentum(const string& cim, const string& tartalom = "", const string& szerzo = "anonym"):
        cim(cim), tartalom(tartalom), szerzo(szerzo) {}

    const string& get_cim() const {
        megnyitasok_szama++;
        return cim;
    }

    const string& get_tartalom() const {
        megnyitasok_szama++;
        return tartalom;
    }

    const string& get_szerzo() const {
        megnyitasok_szama++;
        return szerzo;
    }

    unsigned int get_megnyitasok_szama() const {
        return megnyitasok_szama;
    }

    Dokumentum& operator+=(const string& szoveg) {
        this->tartalom += szoveg;
        return *this;
    }

    Dokumentum operator+(const string& szoveg) {
        Dokumentum uj = *this;
        uj.tartalom += szoveg;
        return uj;
    }
};

int main() {
    Dokumentum d1("biros hazi", "megcsinalom a biros hazit ", "minden hallgato");
    d1 += "max pontra";
    d1 += ", de aztan meg tesztelgetem";
    cout << d1.get_tartalom();
}