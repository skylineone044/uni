// ! operator
#include <iostream>
#include <string>

using namespace std;

class Dokumentum {
    string cim;
    string tartalom;
    string szerzo;
    mutable unsigned megnyitasok_szama = 0;

public:
    explicit Dokumentum(const string& cim, const string& tartalom = "", const string& szerzo = "anonym"):
        cim(cim), tartalom(tartalom), szerzo(szerzo) {}

    const string& get_cim() const {
        megnyitasok_szama++;
        return cim;
    }

    const string& get_tartalom() const {
        megnyitasok_szama++;
        return tartalom;
    }

    const string& get_szerzo() const {
        megnyitasok_szama++;
        return szerzo;
    }

    unsigned int get_megnyitasok_szama() const {
        return megnyitasok_szama;
    }

    Dokumentum& operator+=(const string& szoveg) {
        this->tartalom += szoveg;
        return *this;
    }

    Dokumentum operator+(const string& szoveg) const {
        Dokumentum uj = *this;
        uj.tartalom += szoveg;
        return uj;
    }

    unsigned operator!() {
        if (this->tartalom.empty()) {
            this->tartalom = "Lorem ipsum...";
        } else {
            this->tartalom.clear();
        }

        return this->tartalom.length();
    }
};

int main() {
    Dokumentum d1("videok", "megnezem a tavalyi gyakokat", "minden hallgato");
    unsigned eredmeny = !d1;
    cout << "tartalom: " << d1.get_tartalom() << endl;
    cout << "hossz: " << eredmeny << endl;

    cout << endl;

    Dokumentum d2("uresseg");
    eredmeny = !d2;
    cout << "tartalom: " << d2.get_tartalom() << endl;
    cout << "hossz: " << eredmeny << endl;
}