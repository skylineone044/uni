// ++ operatorok (prefix es postfix)
#include <iostream>
#include <string>

using namespace std;

class Dokumentum {
    string cim;
    string tartalom;
    string szerzo;
    mutable unsigned megnyitasok_szama = 0;

public:
    explicit Dokumentum(const string& cim, const string& tartalom = "", const string& szerzo = "anonym"):
        cim(cim), tartalom(tartalom), szerzo(szerzo) {}

    const string& get_cim() const {
        megnyitasok_szama++;
        return cim;
    }

    const string& get_tartalom() const {
        megnyitasok_szama++;
        return tartalom;
    }

    const string& get_szerzo() const {
        megnyitasok_szama++;
        return szerzo;
    }

    unsigned int get_megnyitasok_szama() const {
        return megnyitasok_szama;
    }

    Dokumentum& operator+=(const string& szoveg) {
        this->tartalom += szoveg;
        return *this;
    }

    Dokumentum operator+(const string& szoveg) const {
        Dokumentum uj = *this;
        uj.tartalom += szoveg;
        return uj;
    }

    unsigned operator!() {
        if (this->tartalom.empty()) {
            this->tartalom = "Lorem ipsum...";
        } else {
            this->tartalom.clear();
        }

        return this->tartalom.length();
    }

    // prefix (++d1)
    Dokumentum& operator++() {
        megnyitasok_szama++;
        return *this;
    }

    // postfix (d1++)
    Dokumentum operator++(int) {
        Dokumentum regi = *this;
        operator++(); // meghivjuk a prefix ++ operatort. Vagy: ++(*this);
        return regi;
    }
};

int main() {
    Dokumentum d1("stepik feladatok", "a stepik feladatokat is megcsinalom mindet es megnezem a streameket", "minden hallgato");
    Dokumentum d2 = ++d1;
    cout << d1.get_megnyitasok_szama() << ", " << d2.get_megnyitasok_szama() << endl;

    Dokumentum d3 = d1++;
    cout << d1.get_megnyitasok_szama() << ", " << d3.get_megnyitasok_szama() << endl;
}