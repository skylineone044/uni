#include <string>
#include <cassert>

using namespace std;

/////////////////////////
//Ide dolgozz!!
////////////////////////

//=== Teszteles bekapcsolasa kikommentezessel
//#define TEST_MUNKAS
//#define TEST_MUNKAS_DOLGOZIK
//#define TEST_MUNKAS_PLUSZ_PLUSZ

//#define TEST_ARANYBANYA
//#define TEST_ARANYBANYA_MINUSZ_EGYENLO
//#define TEST_ARANYBANYA_NEGALAS

//#define TEST_ARANYBANYA_PLUSZEGYENLO
//#define TEST_ARANYBANYA_UNSIGNED_KONVERZIO
//#define TEST_ARANYBANYA_PLUSZ
//=== Teszteles bekapcsolas vege

/*
 * Munkas osztaly - adattagok, getterek, setterek, konstruktorok
 */
void test_munkas(){
#if defined TEST_MUNKAS && !defined TEST_BIRO
    Munkas m1;
    assert(m1.get_nev().empty());
    assert(abs(m1.get_csakany_allapot() - 0) < 0.001f);
    assert(m1.get_sebesseg() == 0);
    assert(m1.get_munkaber() == 0);

    m1.set_nev("Jozsi");
    m1.set_sebesseg(5);
    m1.set_munkaber(700);

    assert(m1.get_nev() == "Jozsi");
    assert(abs(m1.get_csakany_allapot() - 0) < 0.001f);
    assert(m1.get_sebesseg() == 5);
    assert(m1.get_munkaber() == 700);

    Munkas m2("Marika", 7, 1000);
    assert(m2.get_nev() == "Marika");
    assert(m2.get_sebesseg() == 7);
    assert(m2.get_munkaber() == 1000);
#endif
}

/*
 * Munkas osztaly - dolgozik metodus
 */
void test_munkas_dolgozik() {
#if defined TEST_MUNKAS_DOLGOZIK && !defined TEST_BIRO
    const Munkas m1("Arpad", 7, 100);
    unsigned res = m1.dolgozik();
    assert(abs(m1.get_csakany_allapot() - 0.9) < 0.001f);
    assert(res == 7);
#endif
}

/*
 * Munkas osztaly - prefix es postfix ++
 */
void test_munkas_plusz_plusz() {
#if defined TEST_MUNKAS_PLUSZ_PLUSZ && !defined TEST_BIRO
    Munkas m1;
    Munkas m3 = m1++;

    assert((abs(m1.get_csakany_allapot()) - 1) < 0.0001f);
    assert((abs(m3.get_csakany_allapot()) - 0) < 0.0001f);

    Munkas m2("Marika", 7, 1000);
    ++m2;
    assert(m2.get_sebesseg() == 8);
    assert(&++m2 == &m2); // NOLINT(bugprone-assert-side-effect)
#endif
}

/*
 * Aranybanya osztaly - adattagok, getterek, konstruktorok
 */
void test_aranybanya() {
#if defined TEST_ARANYBANYA && !defined TEST_BIRO
    Aranybanya a("Irinyi 217", 300);
    assert(a.get_bejarat() == "Irinyi 217");
    assert(a.get_kapacitas() == 300);
    assert(!a.is_beomlott());
#endif
}

/*
 * Aranybanya osztaly - -= operator
 */
void test_aranybanya_minusz_egyenlo() {
#if defined TEST_ARANYBANYA_MINUSZ_EGYENLO && !defined TEST_BIRO
    Aranybanya a("Bolyai", 5);

    a -= 4;
    assert(a.get_kapacitas() == 1);

    a -= 3;
    assert(a.get_kapacitas() == 0);

    assert(&(a -= 5) == &a); // NOLINT(bugprone-assert-side-effect)
#endif
}

/*
 * Aranybanya osztaly - ! operator
 */
void test_aranybanya_negalas() {
#if defined TEST_ARANYBANYA_NEGALAS && !defined TEST_BIRO
    Aranybanya a("TIK", 50);
    bool elso = !a;
    assert(a.is_beomlott());
    assert(elso);

    bool masodik = !a;
    assert(a.is_beomlott());
    assert(!masodik);
#endif
}

/*
 * Aranybanya osztaly - += operator
 */
void test_aranybanya_pluszegyenlo() {
#if defined TEST_ARANYBANYA_PLUSZEGYENLO && !defined TEST_BIRO
    Munkas m1("Marika", 50, 700);
    Munkas m2; m2.set_nev("Andras");
    Munkas m3("Jozsef", 20, 100);

    Aranybanya a("TIK", 10);
    a += m1;
    a += m2;
    assert(a.get_munkasok()[0].get_nev() == "Marika");
    assert(a.get_munkasok()[1].get_nev() == "Andras");

    a += m3;
    assert(a.get_munkasok()[0].get_nev() == "Marika");
    assert(a.get_munkasok()[1].get_nev() == "Jozsef");

    assert(&(a += m3) == &a); // NOLINT(bugprone-assert-side-effect)
#endif
}

/*
 * Aranybanya osztaly - unsigned konverzio
 */
void test_aranybanya_unsigned_konverzio() {
#if defined TEST_ARANYBANYA_UNSIGNED_KONVERZIO && !defined TEST_BIRO
    Munkas m1("Marika", 50, 700);
    Munkas m2("Jozsef", 20, 100);
    Munkas m3("Karoly", 11, 75);

    Aranybanya a("Szokokut", 40);
    a += m1;
    a += m2;
    a += m3;

    assert((unsigned) a == 875);
#endif
}

/*
 * Aranybanya osztaly - + operator
 */
void test_aranybanya_plusz() {
#if defined TEST_ARANYBANYA_PLUSZ && !defined TEST_BIRO
    const Munkas m1("Marika", 50, 700);
    const Munkas m2("Jozsef", 20, 100);
    const Munkas m3("Karoly", 11, 75);
    const Munkas m4("Peter", 30, 150);

    Aranybanya a1("Karacsonyfa", 20);
    Aranybanya a2("Telapo", 33);
    !a2;

    a1 += m1;
    a1 += m2;
    a1 += m3;
    a2 += m4;

    const Aranybanya aa1 = a1;
    const Aranybanya aa2 = a2;

    Aranybanya res = aa1 + aa2;
    assert(res.get_bejarat() == "Karacsonyfa-Telapo");
    assert(res.get_kapacitas() == 53);
    assert(!res.is_beomlott());
    assert(res.get_munkasok()[0].get_nev().empty());
#endif
}

int main(){
    test_munkas();
    test_munkas_dolgozik();
    test_munkas_plusz_plusz();

    test_aranybanya();
    test_aranybanya_minusz_egyenlo();
    test_aranybanya_negalas();

    test_aranybanya_pluszegyenlo();
    test_aranybanya_unsigned_konverzio();
    test_aranybanya_plusz();
}
