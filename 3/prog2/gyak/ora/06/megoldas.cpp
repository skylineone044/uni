#include <iostream>
#include <string>
#include <cassert>

using namespace std;

class Munkas {
private:
    string nev;
    mutable float csakany_allapot;
    unsigned sebesseg;
    unsigned munkaber;

public:
    Munkas(): nev(""), csakany_allapot(0), sebesseg(0), munkaber(0) {}

    Munkas(const string &nev, unsigned int sebesseg, unsigned int munkaber) :
        nev(nev), sebesseg(sebesseg), munkaber(munkaber), csakany_allapot(1) {}

    unsigned dolgozik() const {
        float eredeti = csakany_allapot;
        csakany_allapot *= 0.9;

        if (eredeti > 0.5) {
            return sebesseg;
        } else {
            return sebesseg * 0.2f;
        }
    }

    Munkas operator++(int) {
        Munkas ret = *this;
        csakany_allapot = 1;
        return ret;
    }

    Munkas& operator++() {
        sebesseg++;
        return *this;
    }

    const string &get_nev() const {
        return nev;
    }

    void set_nev(const string &nev) {
        Munkas::nev = nev;
    }

    float get_csakany_allapot() const {
        return csakany_allapot;
    }

    unsigned int get_sebesseg() const {
        return sebesseg;
    }

    void set_sebesseg(unsigned int sebesseg) {
        Munkas::sebesseg = sebesseg;
    }

    unsigned int get_munkaber() const {
        return munkaber;
    }

    void set_munkaber(unsigned int munkaber) {
        Munkas::munkaber = munkaber;
    }

};


class Aranybanya {
    const string bejarat;
    unsigned kapacitas;
    Munkas munkasok[6];
    bool beomlott;

public:
    Aranybanya(const string &bejarat, unsigned int kapacitas) :
        bejarat(bejarat), kapacitas(kapacitas), beomlott(false) {}

    Aranybanya& operator-=(unsigned mennyivel) {
        if (kapacitas < mennyivel) {
            kapacitas = 0;
        } else {
            kapacitas -= mennyivel;
        }

        return *this;
    }

    bool operator!() {
        bool bevoltomolva = beomlott;
        beomlott = true;

        if (bevoltomolva) {
            return false;
        } else {
            return true;
        }
    }

    Aranybanya& operator+=(const Munkas& munkas) {
        for (int i = 0; i < 6; i++) {
            if (munkasok[i].get_nev().empty() || munkasok[i].get_csakany_allapot() == 0) {
                munkasok[i] = munkas;
                return *this;
            }
        }

        // ...
        cerr << "a banyaban nem dolgozhatnak tobben" << endl;
        return *this;
    }

    operator unsigned() {
        unsigned osszeg = 0;
        for (int i = 0; i < 6; i++) {
            osszeg += munkasok[i].get_munkaber();
        }

        return osszeg;
    }

    Aranybanya operator+(const Aranybanya& masik) const {
        if (this == &masik) {
            Aranybanya ret = *this;
            return ret;
        }

        Aranybanya uj(this->bejarat + "-" + masik.bejarat, this->kapacitas + masik.kapacitas);

        if (this->beomlott && masik.beomlott) {
            uj.beomlott = true;
        } else {
            uj.beomlott = false;
        }

        return uj;
    }

    Munkas& operator[](unsigned index) {
        if (index >= 6) {
            return munkasok[0];
        }

        return munkasok[index];
    }

    const Munkas& operator[](unsigned index) const {
        if (index >= 6) {
            return munkasok[0];
        }

        return munkasok[index];
    }

    const string &get_bejarat() const {
        return bejarat;
    }

    unsigned int get_kapacitas() const {
        return kapacitas;
    }

    const Munkas* get_munkasok() const {
        return munkasok;
    }

    bool is_beomlott() const {
        return beomlott;
    }
};

//=== Teszteles bekapcsolasa kikommentezessel
#define TEST_MUNKAS
#define TEST_MUNKAS_DOLGOZIK
#define TEST_MUNKAS_PLUSZ_PLUSZ

#define TEST_ARANYBANYA
#define TEST_ARANYBANYA_MINUSZ_EGYENLO
#define TEST_ARANYBANYA_NEGALAS

#define TEST_ARANYBANYA_PLUSZEGYENLO
#define TEST_ARANYBANYA_UNSIGNED_KONVERZIO
#define TEST_ARANYBANYA_PLUSZ
//=== Teszteles bekapcsolas vege

/*
 * Munkas osztaly - adattagok, getterek, setterek, konstruktorok
 */
void test_munkas(){
#if defined TEST_MUNKAS && !defined TEST_BIRO
    Munkas m1;
    assert(m1.get_nev().empty());
    assert(abs(m1.get_csakany_allapot() - 0) < 0.001f);
    assert(m1.get_sebesseg() == 0);
    assert(m1.get_munkaber() == 0);

    m1.set_nev("Jozsi");
    m1.set_sebesseg(5);
    m1.set_munkaber(700);

    assert(m1.get_nev() == "Jozsi");
    assert(abs(m1.get_csakany_allapot() - 0) < 0.001f);
    assert(m1.get_sebesseg() == 5);
    assert(m1.get_munkaber() == 700);

    Munkas m2("Marika", 7, 1000);
    assert(m2.get_nev() == "Marika");
    assert(m2.get_sebesseg() == 7);
    assert(m2.get_munkaber() == 1000);
#endif
}

/*
 * Munkas osztaly - dolgozik metodus
 */
void test_munkas_dolgozik() {
#if defined TEST_MUNKAS_DOLGOZIK && !defined TEST_BIRO
    const Munkas m1("Arpad", 7, 100);
    unsigned res = m1.dolgozik();
    assert(abs(m1.get_csakany_allapot() - 0.9) < 0.001f);
    assert(res == 7);
#endif
}

/*
 * Munkas osztaly - prefix es postfix ++
 */
void test_munkas_plusz_plusz() {
#if defined TEST_MUNKAS_PLUSZ_PLUSZ && !defined TEST_BIRO
    Munkas m1;
    Munkas m3 = m1++;

    assert((abs(m1.get_csakany_allapot()) - 1) < 0.0001f);
    assert((abs(m3.get_csakany_allapot()) - 0) < 0.0001f);

    Munkas m2("Marika", 7, 1000);
    ++m2;
    assert(m2.get_sebesseg() == 8);
    assert(&++m2 == &m2); // NOLINT(bugprone-assert-side-effect)
#endif
}

/*
 * Aranybanya osztaly - adattagok, getterek, konstruktorok
 */
void test_aranybanya() {
#if defined TEST_ARANYBANYA && !defined TEST_BIRO
    Aranybanya a("Irinyi 217", 300);
    assert(a.get_bejarat() == "Irinyi 217");
    assert(a.get_kapacitas() == 300);
    assert(!a.is_beomlott());
#endif
}

/*
 * Aranybanya osztaly - -= operator
 */
void test_aranybanya_minusz_egyenlo() {
#if defined TEST_ARANYBANYA_MINUSZ_EGYENLO && !defined TEST_BIRO
    Aranybanya a("Bolyai", 5);

    a -= 4;
    assert(a.get_kapacitas() == 1);

    a -= 3;
    assert(a.get_kapacitas() == 0);

    assert(&(a -= 5) == &a); // NOLINT(bugprone-assert-side-effect)
#endif
}

/*
 * Aranybanya osztaly - ! operator
 */
void test_aranybanya_negalas() {
#if defined TEST_ARANYBANYA_NEGALAS && !defined TEST_BIRO
    Aranybanya a("TIK", 50);
    bool elso = !a;
    assert(a.is_beomlott());
    assert(elso);

    bool masodik = !a;
    assert(a.is_beomlott());
    assert(!masodik);
#endif
}

/*
 * Aranybanya osztaly - += operator
 */
void test_aranybanya_pluszegyenlo() {
#if defined TEST_ARANYBANYA_PLUSZEGYENLO && !defined TEST_BIRO
    Munkas m1("Marika", 50, 700);
    Munkas m2; m2.set_nev("Andras");
    Munkas m3("Jozsef", 20, 100);

    Aranybanya a("TIK", 10);
    a += m1;
    a += m2;
    assert(a.get_munkasok()[0].get_nev() == "Marika");
    assert(a.get_munkasok()[1].get_nev() == "Andras");

    a += m3;
    assert(a.get_munkasok()[0].get_nev() == "Marika");
    assert(a.get_munkasok()[1].get_nev() == "Jozsef");

    assert(&(a += m3) == &a); // NOLINT(bugprone-assert-side-effect)
#endif
}

/*
 * Aranybanya osztaly - unsigned konverzio
 */
void test_aranybanya_unsigned_konverzio() {
#if defined TEST_ARANYBANYA_UNSIGNED_KONVERZIO && !defined TEST_BIRO
    Munkas m1("Marika", 50, 700);
    Munkas m2("Jozsef", 20, 100);
    Munkas m3("Karoly", 11, 75);

    Aranybanya a("Szokokut", 40);
    a += m1;
    a += m2;
    a += m3;

    assert((unsigned) a == 875);
#endif
}

/*
 * Aranybanya osztaly - + operator
 */
void test_aranybanya_plusz() {
#if defined TEST_ARANYBANYA_PLUSZ && !defined TEST_BIRO
    const Munkas m1("Marika", 50, 700);
    const Munkas m2("Jozsef", 20, 100);
    const Munkas m3("Karoly", 11, 75);
    const Munkas m4("Peter", 30, 150);

    Aranybanya a1("Karacsonyfa", 20);
    Aranybanya a2("Telapo", 33);
    !a2;

    a1 += m1;
    a1 += m2;
    a1 += m3;
    a2 += m4;

    const Aranybanya aa1 = a1;
    const Aranybanya aa2 = a2;

    Aranybanya res = aa1 + aa2;
    assert(res.get_bejarat() == "Karacsonyfa-Telapo");
    assert(res.get_kapacitas() == 53);
    assert(!res.is_beomlott());
    assert(res.get_munkasok()[0].get_nev().empty());
#endif
}

int main(){
    test_munkas();
    test_munkas_dolgozik();
    test_munkas_plusz_plusz();

    test_aranybanya();
    test_aranybanya_minusz_egyenlo();
    test_aranybanya_negalas();

    test_aranybanya_pluszegyenlo();
    test_aranybanya_unsigned_konverzio();
    test_aranybanya_plusz();
}