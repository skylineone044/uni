// destruktor
#include <iostream>

using namespace std;

class Eso {
    unsigned mennyiseg;

public:
    Eso(unsigned int mennyiseg): mennyiseg(mennyiseg) {
        cout << "Eleredt az eso (" << mennyiseg << " mm)!" << endl;
    }

    ~Eso() {
        cout << "Elallt az eso (" << mennyiseg << " mm)!" << endl;
    }
};

int main() {
    cout << "IDOJARAS" << endl;

    Eso e1(5);
    Eso e2(7);

    cout << "VIZ" << endl;
}