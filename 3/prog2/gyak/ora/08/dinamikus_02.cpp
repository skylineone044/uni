// objektumok heapen
#include <iostream>
#include <string>

using namespace std;

class Macska {
    string nev;
    int eletkor;

public:
    Macska(const string& nev, int eletkor): nev(nev), eletkor(eletkor) {
        cout << "LETREJOTT!" << endl;
    }

    ~Macska() {
        cout << "MEGSZUNT" << endl;
    }

    const string& get_nev() const { return nev; }
    int get_eletkor() const { return eletkor; }
};

int main() {
    cout << "main start" << endl;

    Macska m1("cirmi", 5); // stack-en jon letre, automatikusan felszabadul
    Macska* m2 = new Macska("kaligula", 7); // heapen jon letre, nekunk kell felszabaditani

    cout << m1.get_eletkor() << ", " << m2->get_eletkor() << endl;

    delete m2;

    cout << "main end" << endl;
}