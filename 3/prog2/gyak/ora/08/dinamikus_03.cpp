// dinamikus tombok
#include <iostream>
#include <string>

using namespace std;

class Macska {
    string nev;
    int eletkor;

public:
    Macska() {
        cout << "LETREJOTT!" << endl;
    }

    Macska(const string& nev, int eletkor): nev(nev), eletkor(eletkor) {
        cout << "LETREJOTT!" << endl;
    }

    ~Macska() {
        cout << "MEGSZUNT" << endl;
    }

    const string& get_nev() const { return nev; }
    int get_eletkor() const { return eletkor; }

    friend ostream& operator<<(ostream& os, const Macska& macska) {
        os << "nev: " << macska.nev << " eletkor: " << macska.eletkor;
        return os;
    }
};

int main() {
    cout << "main start" << endl;

    // dinamikus int tomb
    int* tomb = new int[10];
    for (int i = 0; i < 10; i++) {
        tomb[i] = i;
    }

    for (int i = 0; i < 10; i++) {
        cout << tomb[i] << " ";
    }

    delete[] tomb;

    cout << endl << "--------------------------" << endl;

    // dinamikus Macska tomb
    Macska* macskak = new Macska[5];
    macskak[0] = Macska("cirmi", 7);
    macskak[1] = Macska("Kaligula", 3);

    for (int i = 0; i < 5; i++) {
        cout << macskak[i] << endl;
    }

    delete[] macskak;

    cout << "main end" << endl;
}
