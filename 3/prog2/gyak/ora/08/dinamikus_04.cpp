// dinamikus adattagok
#include <iostream>
#include <string>

using namespace std;

class Hallgato {
    string neptun;
    string nev;

public:
    Hallgato() { cout << "Kurzus letrehozva" << endl; }; // tombot szeretnenk belole letrehozni
    Hallgato(const string& neptun, const string& nev): neptun(neptun), nev(nev) {}
};

class Kurzus {
    string kurzuskod;

    Hallgato* hallgatok;
    unsigned aktualis_hallgatok;
    unsigned max_hallgatok;

public:
    Kurzus(const string& kurzuskod, unsigned max_letszam): kurzuskod(kurzuskod), max_hallgatok(max_letszam), aktualis_hallgatok(0) {
        hallgatok = new Hallgato[max_letszam];
    }

    ~Kurzus() {
        delete[] hallgatok;
    }
};

int main() {
    cout << "Elso kurzus:" << endl;
    Kurzus k("ABCDEF-1", 3);

    cout << "Masodik kurzus: " << endl;
    Kurzus k2("ABCDEF-2", 5);
}