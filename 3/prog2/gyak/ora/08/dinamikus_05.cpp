// copy konstruktor
#include <iostream>
#include <string>

using namespace std;

class Hallgato {
    string neptun;
    string nev;

public:
    Hallgato() = default; // tombot szeretnenk belole letrehozni
    Hallgato(const string& neptun, const string& nev): neptun(neptun), nev(nev) {}
};

class Kurzus {
    string kurzuskod;

    Hallgato* hallgatok;
    unsigned aktualis_hallgatok;
    unsigned max_hallgatok;

public:
    Kurzus(const string& kurzuskod, unsigned max_letszam): kurzuskod(kurzuskod), max_hallgatok(max_letszam), aktualis_hallgatok(0) {
        hallgatok = new Hallgato[max_letszam];
    }

    // hasznalat: Kurzus k2(k1); vagy: Kurzus k2 = k1; -- mindket esetben a copy konstruktor hivodik meg, mert a k2 meg nem letezett eddig
    Kurzus(const Kurzus& masik) {
        this->kurzuskod = masik.kurzuskod;
        this->aktualis_hallgatok = masik.aktualis_hallgatok;
        this->max_hallgatok = masik.max_hallgatok;

        // this->hallgatok = masik.hallgatok; // NEM jo, mert igy ugyanarra a tombre mutatna
        // ehelyett uj tombot hozunk letre es lemasoljuk az elemeket
        this->hallgatok = new Hallgato[this->max_hallgatok];
        for (int i = 0; i < this->aktualis_hallgatok; i++) {
            this->hallgatok[i] = masik.hallgatok[i];
        }
    }

    ~Kurzus() {
        delete[] hallgatok;
    }
};
