// teljes pelda
#include <iostream>
#include <string>

using namespace std;

class Hallgato {
    string neptun;
    string nev;

public:
    Hallgato() = default; // tombot szeretnenk belole letrehozni
    Hallgato(const string& neptun, const string& nev): neptun(neptun), nev(nev) {}
    friend ostream& operator<<(ostream& os, const Hallgato& hallgato) {
        os << hallgato.nev << " (" << hallgato.neptun << ")";
        return os;
    }
};

class Kurzus {
    string kurzuskod;

    Hallgato* hallgatok;
    unsigned aktualis_hallgatok;
    unsigned max_hallgatok;

public:
    Kurzus(const string& kurzuskod, unsigned max_letszam): kurzuskod(kurzuskod), max_hallgatok(max_letszam), aktualis_hallgatok(0) {
        hallgatok = new Hallgato[max_letszam];
    }

    void kiir() {
        for (int i = 0; i < max_hallgatok; i++) {
            cout << hallgatok[i] << endl;
        }
    }

    Kurzus& operator+=(const Hallgato& hallgato) {
        if (aktualis_hallgatok == max_hallgatok) {
            cout << "A kurzusfelvetel nem sikerult, mert nem sikerult a targyfelvetel!" << endl;
            return *this;
        }

        hallgatok[aktualis_hallgatok++] = hallgato;
        return *this;
    }

    Kurzus& operator+=(unsigned mennyivel) {
        // kurzus max letszamanak megnovelese
        max_hallgatok += mennyivel;

        // uj tombot kell kesziteni, hogy abban tobb hallgato elferjen
        Hallgato* uj = new Hallgato[max_hallgatok];

        // az eddigi hallgatokat at kell masolni
        for (int i = 0; i < aktualis_hallgatok; i++) {
            uj[i] = this->hallgatok[i];
        }

        // torolni kell a regi tombot, mivel mar nem kell
        delete[] hallgatok;

        // a hallgatok adattag atallitasa az uj tombre
        this->hallgatok = uj;

        return *this;
    }

    // hasznalat: Kurzus k2(k1); vagy: Kurzus k2 = k1; -- mindket esetben a copy konstruktor hivodik meg, mert a k2 meg nem letezett eddig
    Kurzus(const Kurzus& masik) {
        this->kurzuskod = masik.kurzuskod;
        this->aktualis_hallgatok = masik.aktualis_hallgatok;
        this->max_hallgatok = masik.max_hallgatok;

        // this->hallgatok = masik.hallgatok; // NEM jo, mert igy ugyanarra a tombre mutatna
        // ehelyett uj tombot hozunk letre es lemasoljuk az elemeket
        this->hallgatok = new Hallgato[this->max_hallgatok];
        for (int i = 0; i < this->aktualis_hallgatok; i++) {
            this->hallgatok[i] = masik.hallgatok[i];
        }
    }

    // hasznalat: k2 = k1; -- itt a k2 mar korabban letezett, most felulirtuk
    Kurzus& operator=(const Kurzus& masik) {
        // a = a eset lekezelese
        if (&masik == this) {
            return *this;
        }

        // felszabaditas
        delete[] hallgatok;

        // atmasolas
        this->kurzuskod = masik.kurzuskod;
        this->aktualis_hallgatok = masik.aktualis_hallgatok;
        this->max_hallgatok = masik.max_hallgatok;

        this->hallgatok = new Hallgato[this->max_hallgatok];
        for (int i = 0; i < this->aktualis_hallgatok; i++) {
            this->hallgatok[i] = masik.hallgatok[i];
        }

        return *this;
    }

    ~Kurzus() {
        delete[] hallgatok;
    }
};

int main() {
    // letrehozunk nehany hallgatot
    Hallgato h1("ABCDEF", "Kiss Bela");
    Hallgato h2("VBAJDD", "Reti Laszlo");
    Hallgato h3("BMDFJK", "Kovacs Anita");
    Hallgato h4("BDFMNK", "Nagy Anna");
    Hallgato h5("KANVDS", "Toth Peter");

    // letrehozunk egy kurzust, hozzaadjuk a hallgatokat
    Kurzus k1("AAAAAA", 3);
    k1 += h1;
    k1 += h2;
    k1 += h3;

    k1.kiir();
    cout << "---------------------" << endl;

    // ot mar nem tudjuk hozzadni
    k1 += h4;

    cout << "---------------------" << endl;

    // noveljuk a maximalis letszamot
    k1 += 2;

    // most már hozzá tudjuk adni
    k1 += h4;

    k1.kiir();

    cout << "---------------------" << endl;

    Kurzus k2 = k1; // copy konstruktor fut le
    k2.kiir();

    k2 += h5;

    cout << "---------------------" << endl;

    cout << "elso kurzus:" << endl;
    k1.kiir();

    cout << "masodik kurzus:" << endl;
    k2.kiir();

    cout << "---------------------" << endl;

    k2 = k1; // ertekadas operator fut le

    cout << "elso kurzus:" << endl;
    k1.kiir();

    cout << "masodik kurzus:" << endl;
    k2.kiir();
}