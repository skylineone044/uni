1. Készítsd el az Eso osztályt. Az objektum létrejöttekor íródjon ki, hogy eleredt az eső, míg az objektum megszűntekor íródjon ki, hogy elállt az eső.

2. Készítsd el a Macska osztályt! Nézd meg, hogy hogyan lehet belőle objektumot létrehozni a stacken és a heapen!

3. Nézd meg, hogy hogyan lehet intekből, illetve macskákból álló dinamikus tömböt létrehozni, megszűntetni!

4. Készítsd el a Hallgato, illetve Kurzus osztályokat. A kurzus osztály konstruktora várja, hogy hány hallgató veheti fel a kurzust.
   A hallgatók tömböt ez alapján hozzuk létre!

5. Készíts copy konstruktort a Kurzus osztályhoz!

6. Készíts értékadás operátort a Kurzus osztályhoz!

7. Lehessen hallgatót hozzáadni a kurzushoz, illetve lehessen a létszámát bővíteni a += operátor segítségével!
