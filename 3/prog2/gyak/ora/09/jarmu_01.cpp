// protected lathatosag

#include <iostream>
#include <string>

using namespace std;

class Jarmu {
private:
    string uzemanyag_tipus;

protected:
    string terep;
    string szin;

public:
    Jarmu(const string& uzemanyagTipus, const string& terep, const string& szin): uzemanyag_tipus(uzemanyagTipus), terep(terep), szin(szin) {}

    void halad() const {
        cout << "A jarmu " << terep << " terepen " << uzemanyag_tipus << "-t felhasznalva halad" << endl;
    }

    void atfest(const string& uj) {
        szin = uj;
    }

    const string& get_uzemanyag_tipus() const {
        return uzemanyag_tipus;
    }

    const string& get_szin() const {
        return szin;
    }
};

