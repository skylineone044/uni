// os metodus meghivasa, feluldefinialas

#include <iostream>
#include <string>

using namespace std;

class Jarmu {
private:
    string uzemanyag_tipus;

protected:
    string terep;
    string szin;

public:
    Jarmu(const string& uzemanyagTipus, const string& terep, const string& szin): uzemanyag_tipus(uzemanyagTipus), terep(terep), szin(szin) {}

    void halad() const {
        cout << "A jarmu " << terep << " terepen " << uzemanyag_tipus << "-t felhasznalva halad" << endl;
    }

    void atfest(const string& uj) {
        szin = uj;
    }

    const string& get_uzemanyag_tipus() const {
        return uzemanyag_tipus;
    }

    const string& get_szin() const {
        return szin;
    }
};

class Auto: public Jarmu {
    unsigned sebesseg;

public:
    Auto(const string& uzemanyag_tipus, const string& szin, unsigned int sebesseg):
            Jarmu(uzemanyag_tipus, "fold", szin), sebesseg(sebesseg) {}

    Auto(const string& uzemanyag_tipus, const string& szin): Jarmu(uzemanyag_tipus, "fold", szin), sebesseg(0) {}

    void atfest(const string& uj) {
        if (uj == "rozsaszin") {
            cout << "Az auto nem festheto at rozsaszinre!" << endl;
        }

        Jarmu::atfest(uj);
    }

    void halad() const {
        cout << "Az auto a foldon megy, " << get_uzemanyag_tipus() << "-t fogyaszt. Jelenlegi sebessege: " << sebesseg << " km/h." << endl;
    }
};

int main() {
    Jarmu jarmu("gazolaj", "levego", "sarga");
    Auto car("gazolaj", "kek", 50);

    jarmu.halad();
    car.halad();

    jarmu.atfest("rozsaszin");
    car.atfest("rozsaszin");

    cout << "-------------------------" << endl;

    // Polimorfizmus
    Jarmu* j2 = new Auto("gazolaj", "fekete", 21);

    j2->halad();
    j2->atfest("rozsaszin");

    delete j2;
}
