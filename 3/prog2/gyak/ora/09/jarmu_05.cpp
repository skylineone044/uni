// tobbszoros oroklodes

#include <iostream>
#include <string>

using namespace std;

class Jarmu {
private:
    string uzemanyag_tipus;

protected:
    string terep;
    string szin;

public:
    Jarmu(const string& uzemanyagTipus, const string& terep, const string& szin): uzemanyag_tipus(uzemanyagTipus), terep(terep), szin(szin) {}

    virtual void halad() const {
        cout << "A jarmu " << terep << " terepen " << uzemanyag_tipus << "-t felhasznalva halad" << endl;
    }

    virtual void atfest(const string& uj) {
        szin = uj;
    }

    const string& get_uzemanyag_tipus() const {
        return uzemanyag_tipus;
    }

    const string& get_szin() const {
        return szin;
    }
};

class Auto: public Jarmu {
    unsigned sebesseg;

public:
    Auto(const string& uzemanyag_tipus, const string& szin, unsigned int sebesseg):
            Jarmu(uzemanyag_tipus, "fold", szin), sebesseg(sebesseg) {}

    Auto(const string& uzemanyag_tipus, const string& szin): Jarmu(uzemanyag_tipus, "fold", szin), sebesseg(0) {}

    void atfest(const string& uj) override {
        if (uj == "rozsaszin") {
            cout << "Az auto nem festheto at rozsaszinre!" << endl;
        }

        Jarmu::atfest(uj);
    }

    void halad() const override {
        cout << "Az auto a foldon megy, " << get_uzemanyag_tipus() << "-t fogyaszt. Jelenlegi sebessege: " << sebesseg << " km/h." << endl;
    }
};

class Haz {
    unsigned terulet;

public:
    Haz(unsigned terulet): terulet(terulet) {}
};

class Lakokocsi: public Auto, public Haz {
public:
    Lakokocsi(unsigned int terulet, const string& uzemanyagTipus, const string& szin, unsigned int sebesseg):
        Haz(terulet), Auto(uzemanyagTipus, szin, sebesseg) {}
};

int main() {
    Jarmu jarmu("gazolaj", "levego", "sarga");
    Auto car("gazolaj", "kek", 50);

    jarmu.halad();
    car.halad();

    jarmu.atfest("rozsaszin");
    car.atfest("rozsaszin");

    cout << "-------------------------" << endl;

    // Polimorfizmus
    Jarmu* j2 = new Auto("gazolaj", "fekete", 21);

    j2->halad();
    j2->atfest("rozsaszin");

    delete j2;

    cout << "-------------------------" << endl;

    Lakokocsi lakokocsi(12, "arany", "feher", 0);
    lakokocsi.atfest("rozsaszin");
}
