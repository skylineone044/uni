// stream output operator (erdekesseg)

#include <iostream>
#include <string>

using namespace std;

class Allat {
    string szin;
    int eletkor;
    bool veszelyes;

public:
    Allat(const string& szin, int eletkor, bool veszelyes): szin(szin), eletkor(eletkor), veszelyes(veszelyes) {}

    friend ostream& operator<<(ostream& os, const Allat& allat) {
        os << allat.szin << " szinu, " << allat.eletkor << " eves allat (" << (allat.veszelyes ? "veszelyes" : "nem veszelyes") << ")";
        return os;
    }
};

int main() {
    Allat a("piros", 20, true);
    cout << a << endl;

    Allat b("kek", 3, false);
    cout << b << endl;
}