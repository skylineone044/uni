// virtualis destruktor

#include <iostream>
#include <string>

using namespace std;

class Allat {
    string szin;
    int eletkor;
    bool veszelyes;

public:
    Allat(const string& szin, int eletkor, bool veszelyes): szin(szin), eletkor(eletkor), veszelyes(veszelyes) {
        cout << "allat konstruktor" << endl;
    }

    virtual ~Allat() {
        cout << "allat destruktor" << endl;
    }

    bool idos() const {
        return (eletkor > 10 && veszelyes) || (eletkor > 5 && !veszelyes);
    }

    virtual string hangot_ad() const {
        return "???";
    }
};

class Krokodil: public Allat {
    double harapasErosseg;

public:
    Krokodil(const string& szin, int eletkor, double harapasErosseg):
        Allat(szin, eletkor, true),harapasErosseg(harapasErosseg) {
        cout << "krokodil konstruktor" << endl;
    }

    ~Krokodil() {
        cout << "krokodil destruktor" << endl;
    }

    string hangot_ad() const override {
        return "Mrrrr";
    }
};

class Oroszlan: public Allat {
    double orditasiHangossag;

public:
    Oroszlan(const string& szin, int eletkor, double orditasiHangossag):
        Allat(szin, eletkor, true), orditasiHangossag(orditasiHangossag) {
        cout << "oroszlan konstruktor" << endl;
    }

    ~Oroszlan() {
        cout << "oroszlan destruktor" << endl;
    }

    string hangot_ad() const override {
        return "RrRrr";
    }
};

int main() {
    cout << "---------- ALLAT ----------" << endl;
    Allat* a = new Allat("kek", 7, false);
    cout << a->hangot_ad() << endl;
    delete a;

    cout << "---------- KROKODIL ----------" << endl;
    Krokodil* k = new Krokodil("zold", 5, 0.92);
    cout << k->hangot_ad() << endl;
    delete k;

    cout << "---------- OROSZLAN ----------" << endl;
    Oroszlan* o = new Oroszlan("sarga", 7, 0.689);
    cout << o->hangot_ad() << endl;
    delete o;

    cout << "---------- POLIMORFIZMUS ----------" << endl;
    Allat* aa = new Krokodil("fekete", 10, 0.44);
    cout << aa->hangot_ad() << endl;
    delete aa;
}