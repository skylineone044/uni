// osszetett pelda (ismetles)

#include <iostream>
#include <string>

using namespace std;

class Allat {
    string szin;
    int eletkor;
    bool veszelyes;

public:
    Allat(const string& szin, int eletkor, bool veszelyes): szin(szin), eletkor(eletkor), veszelyes(veszelyes) {
        cout << "allat konstruktor" << endl;
    }

    virtual ~Allat() {
        cout << "allat destruktor" << endl;
    }

    bool idos() const {
        return (eletkor > 10 && veszelyes) || (eletkor > 5 && !veszelyes);
    }

    virtual string hangot_ad() const = 0;
};

class Krokodil: public Allat {
    double harapasErosseg;

public:
    Krokodil(): Allat("", 0, false), harapasErosseg(0) {
        cout << "krokodil konstruktor (default)" << endl;
    }

    Krokodil(const string& szin, int eletkor, double harapasErosseg):
        Allat(szin, eletkor, true),harapasErosseg(harapasErosseg) {
        cout << "krokodil konstruktor" << endl;
    }

    ~Krokodil() {
        cout << "krokodil destruktor" << endl;
    }

    string hangot_ad() const override {
        return "Mrrrr";
    }
};

class Oroszlan: public Allat {
    double orditasiHangossag;

public:
    Oroszlan(const string& szin, int eletkor, double orditasiHangossag):
        Allat(szin, eletkor, true), orditasiHangossag(orditasiHangossag) {
        cout << "oroszlan konstruktor" << endl;
    }

    ~Oroszlan() {
        cout << "oroszlan destruktor" << endl;
    }

    string hangot_ad() const override {
        return "RrRrr";
    }
};

class Folyo {
    Krokodil* krokodilok;
    unsigned krokodil_szam;

public:
    Folyo(): krokodilok(nullptr), krokodil_szam(0) {}

    Folyo(const Folyo& masik) {
        krokodil_szam = masik.krokodil_szam;

        if (krokodil_szam > 0) {
            krokodilok = new Krokodil[krokodil_szam];

            for (int i = 0; i < krokodil_szam; i++) {
                krokodilok[i] = masik.krokodilok[i];
            }
        } else {
            krokodilok = nullptr;
        }
    }

    Folyo& operator=(const Folyo& masik) {
        if (this == &masik) {
            return *this;
        }

        delete[] krokodilok;

        krokodil_szam = masik.krokodil_szam;

        if (krokodil_szam > 0) {
            krokodilok = new Krokodil[krokodil_szam];

            for (int i = 0; i < krokodil_szam; i++) {
                krokodilok[i] = masik.krokodilok[i];
            }
        } else {
            krokodilok = nullptr;
        }

        return *this;
    }

    Folyo& operator+=(const Krokodil& krokodil) {
        Krokodil* uj = new Krokodil[krokodil_szam + 1];
        for (int i = 0; i < krokodil_szam; i++) {
            uj[i] = krokodilok[i];
        }

        uj[krokodil_szam] = krokodil;
        krokodil_szam++;

        delete[] krokodilok;

        krokodilok = uj;

        return *this;
    }

    friend ostream& operator<<(ostream& os, const Folyo& folyo) {
        for (int i = 0; i < folyo.krokodil_szam; i++) {
            os << folyo.krokodilok[i].hangot_ad() << endl;
        }

        return os;
    }

    virtual ~Folyo() {
        delete[] krokodilok;
    }
};

int main() {
    Krokodil k1("piros", 4, 0.9);
    Krokodil k2("zold", 5, 0.4);
    Krokodil k3("barna", 10, 0.25);

    Folyo f;
    ((f += k1) += k2) += k3;
    cout << f;
}