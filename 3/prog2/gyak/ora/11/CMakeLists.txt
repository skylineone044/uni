cmake_minimum_required(VERSION 3.20)
project(gyak11)

set(CMAKE_CXX_STANDARD 14)

add_executable(kivetelek_1 kivetelek_1.cpp)
add_executable(kivetelek_2 kivetelek_2.cpp)
add_executable(kivetelek_3 kivetelek_3.cpp)
add_executable(kivetelek_4 kivetelek_4.cpp)
add_executable(kivetelek_5 kivetelek_5.cpp)
