// kivetelek

#include <iostream>
#include <string>

using namespace std;

int konvertal(const string& szoveg) {
    return stoi(szoveg);
}

int main() {
    cout << "Irj be egy szamot!" << endl;
    string szoveg;
    cin >> szoveg;
    int res = konvertal(szoveg);
    cout << "eredmeny: " << res << endl;
}