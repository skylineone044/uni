// try-catch

#include <iostream>
#include <string>

using namespace std;

int konvertal(const string& szoveg) {
    try {
        return stoi(szoveg);
    } catch (const invalid_argument& hiba) { // kivetelek ososztalya: exception
        cerr << "Sikertelen konverzio!" << endl;
        return -1;
    }
}

int main() {
    cout << "Irj be egy szamot!" << endl;
    string szoveg;
    cin >> szoveg;
    int res = konvertal(szoveg);
    cout << "eredmeny: " << res << endl;
}