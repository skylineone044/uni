// nem exception tipusu kivetelek, minden kivetel elkapasa

#include <iostream>
#include <string>

using namespace std;

int osszeg(const int* tomb, int meret) {
    if (meret < 0) {
        throw out_of_range("a tomb merete nem lehet negativ");
    }

    if (tomb == nullptr) {
        throw invalid_argument("a tomb nem lehet null");
    }

    int osszeg = 0;
    for (int i = 0; i < meret; i++) {
        if (tomb[i] < 0) {
            throw "HIBA";
        }

        osszeg += tomb[i];
    }

    return osszeg;
}

int main() {
    int tomb[] = {3, 5, 10, 4};

    try {
        int res = osszeg(tomb, 4);
        cout << "A tomb elemeinek osszege: " << res << endl;
    } catch (const out_of_range& e) {
        cout << "out of range: " << e.what() << endl;
    } catch (const invalid_argument& e) {
        cout << "invalid argument: " << e.what() << endl;
    } catch (const exception& e) {
        cout << "exception: " << e.what() << endl;
    } catch (const char* e) {
        cout << "char*: " << e << endl;
    } catch (int e) {
        cout << "int: " << e << endl;
    } catch (...) {
        cout << "valamilyen egyeb hiba" << endl;
    }
}