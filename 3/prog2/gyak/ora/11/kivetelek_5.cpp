// sajat kivetel osztaly

#include <iostream>
#include <string>

using namespace std;

class PoharException: public exception {
    string message;

public:
    PoharException(const string& message): message(message) {}

    const char * what() const noexcept override {
        return message.c_str();
    }
};

class Pohar {
    unsigned urtartalom;
    unsigned folyadek_mennyiseg;

public:
    Pohar(unsigned int urtartalom): urtartalom(urtartalom), folyadek_mennyiseg(0) {}

    Pohar& operator++() {
        if (folyadek_mennyiseg == urtartalom) {
            throw PoharException("Megtelt a pohar!");
        }

        folyadek_mennyiseg++;
        return *this;
    }

    Pohar& operator--() {
        if (folyadek_mennyiseg == 0) {
            throw PoharException("Nincs semmi a poharban!");
        }

        folyadek_mennyiseg--;
        return *this;
    }
};

int main() {
    Pohar p(2);

    try {
        ++p;
    } catch (const PoharException& e) {
        cerr << "1. HIBA: " << e.what() << endl;
    }

    try {
        ++p;
    } catch (const PoharException& e) {
        cerr << "2. HIBA: " << e.what() << endl;
    }

    try {
        ++p;
    } catch (const PoharException& e) {
        cerr << "3. HIBA: " << e.what() << endl;
    }

    try {
        --p;
    } catch (const PoharException& e) {
        cerr << "4. HIBA: " << e.what() << endl;
    }

    try {
        --p;
    } catch (const PoharException& e) {
        cerr << "5. HIBA: " << e.what() << endl;
    }

    try {
        --p;
    } catch (const PoharException& e) {
        cerr << "6. HIBA: " << e.what() << endl;
    }

}