#include <iostream>
#include <string>
#include <cassert>
#include <cstring>
#include <cmath>

using namespace std;

/////////////////////////
//Ide dolgozz!!
////////////////////////

//=== Teszteles bekapcsolasa kikommentezessel
//#define TEST_ALKALMAZAS_HIBA

//#define TEST_SZOFTVER
//#define TEST_SZOFTVER_BEEPIT

//#define TEST_ALKALMAZAS
//#define TEST_ALKALMAZAS_BEEPIT
//#define TEST_ALKALMAZAS_PLUSZ_EGYENLO
//#define TEST_ALKALMAZAS_PLUSZ

//#define TEST_TELEFON
//#define TEST_TELEFON_COPY_KONSTRUKTOR
//#define TEST_TELEFON_ERTEKADAS_OPERATOR
//#define TEST_TELEFON_PLUSZ_PLUSZ
//#define TEST_TELEFON_PLUSZ_EGYENLO
//#define TEST_TELEFON_STRING_KONVERZIO
//#define TEST_TELEFON_TOMBINDEX_OPERATOR

//#define TEST_HUIE_TELEFON
//#define TEST_HUIE_TELEFON_PLUSZ_EGYENLO
//=== Teszteles bekapcsolas vege

#if !defined TEST_BIRO
/*
 * AlkalmazasHiba osztaly - konstruktor
 */
void test_alkalmazas_hiba() {
#if defined TEST_ALKALMAZAS_HIBA && !defined TEST_BIRO
    AlkalmazasHiba e(2);
    assert(strcmp(e.what(), "Eltavolitasi hiba") == 0);
#endif
}

/*
 * Szoftver osztaly - adattagok, getterek, konstruktor
 */
void test_szoftver() {
#if defined TEST_SZOFTVER && !defined TEST_BIRO
    class Szoftver2: public Szoftver {
        public: Szoftver2(const string& gyarto, const string& operaciosRendszer): Szoftver(gyarto, operaciosRendszer) {}
        void beepit(int mac_id, string *szoftverlista) override {}
    };

    Szoftver2 m("meta", "android");
    assert(m.get_gyarto() == "meta");
    assert(m.get_eladva() == 0);
    assert(m.get_operacios_rendszer() == "android");
#endif
}

/*
 * Szoftver osztaly - beepit metodus
 */
void test_szoftver_beepit() {
#if defined TEST_SZOFTVER_BEEPIT && !defined TEST_BIRO
    class Szoftver2: public Szoftver {
    public: Szoftver2(const string& gyarto, const string& operaciosRendszer): Szoftver(gyarto, operaciosRendszer) {}
        void beepit(int mac_id, string *szoftverlista) override {}
    };

    Szoftver2 m("meta", "ios");
    m.beepit(1, nullptr);
#endif
}

/*
 * Alkalmazas osztaly - adattagok, getterek, konstruktorok
 */
void test_alkalmazas() {
#if defined TEST_ALKALMAZAS && !defined TEST_BIRO
    Alkalmazas m;
    assert(m.get_gyarto().empty());
    assert(m.get_eladva() == 0);
    assert(m.get_operacios_rendszer().empty());
    assert(m.is_beepitett());
    assert(!m.is_engedelyek());

    Alkalmazas m2("asd", "ios", false, true);
    assert(m2.get_gyarto() == "asd");
    assert(m2.get_eladva() == 0);
    assert(m2.get_operacios_rendszer() == "ios");
    assert(!m2.is_beepitett());
    assert(m2.is_engedelyek());
#endif
}

/*
 * Alkalmazas osztaly - beepit metodus
 */
void test_alkalmazas_beepit() {
#if defined TEST_ALKALMAZAS_BEEPIT && !defined TEST_BIRO
    Alkalmazas m2("asd", "ios", false, true);
    string s = "valami";
    m2.beepit(10, &s);
    assert(s == "valami;asd");
    assert(m2.is_beepitett());
#endif
}

/*
 * Alkalmazas osztaly - plusz egyenlo
 */
void test_alkalmazas_plusz_egyenlo() {
#if defined TEST_ALKALMAZAS_PLUSZ_EGYENLO && !defined TEST_BIRO
    Alkalmazas a;
    a += 7u;
    a += 2u;
    assert(a.get_eladva() == 9);
#endif
}

/*
 * Alkalmazas osztaly - plusz operator
 */
void test_alkalmazas_plusz() {
#if defined TEST_ALKALMAZAS_PLUSZ && !defined TEST_BIRO
    unsigned a = 7;
    Alkalmazas m("asd", "ios", false, true);
    m += 8u;
    unsigned res = a + m;
    assert(res == 15);
#endif
}

/*
 * Telefon osztaly - adattagok, konstruktor
 */
void test_telefon() {
#if defined TEST_TELEFON && !defined TEST_BIRO
    class Telefon2: Telefon {
        public: Telefon2(const string& gyarto, const string& operaciosRendszer, unsigned int maxAlkalmazasok): Telefon(gyarto, operaciosRendszer, maxAlkalmazasok) {}

        void test() {
            assert(gyarto == "samsung");
            assert(operacios_rendszer == "android");
            assert(max_alkalmazasok == 5);
            assert(aktualis_alkalmazasok == 0);
            assert(alkalmazasok != nullptr);
        }
    };

    Telefon2 k("samsung", "android", 5);
    k.test();
#endif
}

/*
 * Telefon osztaly - copy konstruktor
 */
void test_telefon_copy_konstruktor() {
#if defined TEST_TELEFON_COPY_KONSTRUKTOR && !defined TEST_BIRO
    class Telefon2: public Telefon {
    public: Telefon2(const string& gyarto, const string& operaciosRendszer, unsigned int maxAlkalmazasok): Telefon(gyarto, operaciosRendszer, maxAlkalmazasok) {}

        void test() {
            assert(aktualis_alkalmazasok == 1);
            assert(alkalmazasok[0].get_gyarto() == "asd");
        }
    };

    Telefon2 k("samsung", "android", 5);
    Alkalmazas m("asd", "android", false, false); m += 10000;
    k += m;

    Telefon2 k2(k);
    k2.test();
#endif
}

/*
 * Telefon osztaly - ertekadas operator
 */
void test_telefon_ertekadas_operator() {
#if defined TEST_TELEFON_ERTEKADAS_OPERATOR && !defined TEST_BIRO
    class Telefon2: public Telefon {
    public: Telefon2(const string& gyarto, const string& operaciosRendszer, unsigned int maxAlkalmazasok): Telefon(gyarto, operaciosRendszer, maxAlkalmazasok) {}

        void test() {
            assert(aktualis_alkalmazasok == 1);
            assert(alkalmazasok[0].get_gyarto() == "asd");
        }
    };

    Telefon2 k("samsung", "android", 5);
    Alkalmazas m("asd", "android", false, false); m += 10000;
    k += m;

    Telefon2 k2("huawei", "android", 1);
    k2 = k;

    k2.test();
#endif
}

/*
 * Telefon osztaly - plusz plusz
 */
void test_telefon_plusz_plusz() {
#if defined TEST_TELEFON_PLUSZ_PLUSZ && !defined TEST_BIRO
    class Telefon2: public Telefon {
    public: Telefon2(const string& gyarto, const string& operaciosRendszer, unsigned int maxAlkalmazasok): Telefon(gyarto, operaciosRendszer, maxAlkalmazasok) {}

        void test() {
            assert(aktualis_alkalmazasok == 1);
            assert(max_alkalmazasok == 20);
            assert(alkalmazasok[0].get_gyarto() == "asd");
        }
    };

    Telefon2 k("apple", "ios", 10);
    Alkalmazas m("asd", "ios", false, false); m += 10000;

    k += m;

    ++k;

    k.test();
#endif
}

/*
 * Telefon osztaly - plusz egyenlo
 */
void test_telefon_plusz_egyenlo() {
#if defined TEST_TELEFON_PLUSZ_EGYENLO && !defined TEST_BIRO
    class Telefon2: public Telefon {
    public: Telefon2(const string& gyarto, const string& operaciosRendszer, unsigned int maxAlkalmazasok): Telefon(gyarto, operaciosRendszer, maxAlkalmazasok) {}

        void test() {
            assert(aktualis_alkalmazasok == 1);
            assert(alkalmazasok[0].get_gyarto() == "asd");
        }
    };

    Telefon2 k("apple", "ios", 10);
    Alkalmazas m("asd", "ios", false, false); m += 10000;

    k += m;

    k.test();
#endif
}

/*
 * Telefon osztaly - string konverzio
 */
void test_telefon_string_konverzio() {
#if defined TEST_TELEFON_STRING_KONVERZIO && !defined TEST_BIRO
    Telefon k("microsoft", "windows 12", 4);
    assert(((string) k) == "microsoft;windows 12");
#endif
}

/*
 * Telefon osztaly - tombindex operator
 */
void test_telefon_tombindex_operator() {
#if defined TEST_TELEFON_TOMBINDEX_OPERATOR && !defined TEST_BIRO
    Telefon k("Coospace", "BBB", 4);
    Alkalmazas m("asd", "BBB", false, true); m += 10000;
    Alkalmazas m2("asda", "BBB", true, false); m2 += 10000;

    k += m;
    k += m2;

    assert(k["asd"].is_engedelyek());
#endif
}

/*
 * HuieTelefon osztaly - adattagok, konstruktor
 */
void test_huie_telefon() {
#if defined TEST_HUIE_TELEFON && !defined TEST_BIRO
    class Telefon2: public HuieTelefon {
    public: Telefon2(const string& gyarto, const string& operaciosRendszer, unsigned int maxAlkalmazasok, int megjelenesEve): HuieTelefon(gyarto, operaciosRendszer, maxAlkalmazasok, megjelenesEve) {}
        void test() {
            assert(gyarto == "apple");
            assert(operacios_rendszer == "ios");
            assert(max_alkalmazasok == 3);
            assert(aktualis_alkalmazasok == 0);
        }
    };

    Telefon2 k("apple", "ios", 3, 2022);
    k.test();
#endif
}

/*
 * HuieTelefon osztaly - plusz egyenlo
 */
void test_huei_telefon_plusz_egyenlo() {
#if defined TEST_HUIE_TELEFON_PLUSZ_EGYENLO && !defined TEST_BIRO
    class Telefon2: public HuieTelefon {
    public: Telefon2(const string& gyarto, const string& operaciosRendszer, unsigned int maxAlkalmazasok, int megjelenesEve): HuieTelefon(gyarto, operaciosRendszer, maxAlkalmazasok, megjelenesEve) {}
        void test() {
            assert(alkalmazasok[0].get_gyarto().empty());
            assert(aktualis_alkalmazasok == 0);
        }
    };

    Telefon2 k("apple", "ios", 3, 2022);
    Alkalmazas m("Gogel", "BBB", false, false); m += 10000;

    k += m;

    k.test();
#endif
}

int main(){
    test_alkalmazas_hiba();

    test_szoftver();
    test_szoftver_beepit();

    test_alkalmazas();
    test_alkalmazas_beepit();
    test_alkalmazas_plusz_egyenlo();
    test_alkalmazas_plusz();

    test_telefon();
    test_telefon_copy_konstruktor();
    test_telefon_ertekadas_operator();
    test_telefon_plusz_plusz();
    test_telefon_plusz_egyenlo();
    test_telefon_string_konverzio();
    test_telefon_tombindex_operator();

    test_huie_telefon();
    test_huei_telefon_plusz_egyenlo();
}

#endif
