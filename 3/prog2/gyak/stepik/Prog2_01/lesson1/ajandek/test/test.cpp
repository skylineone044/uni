#include <gtest/gtest.h>

#define main main_0
#include "../src/ajandek.cpp"
#undef main

#include "../../tools.cpp"

void hullamvasut(int a, int b);

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    KIIR(kibontas("Lajos", 12, 24))
    ASSERT_STREQ(readstring(file, str), "OPEN");
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    KIIR(kibontas("Lajos", 12, 25))
    ASSERT_STREQ(readstring(file, str), "OPEN");
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    KIIR(kibontas("Lajos", 11, 24))
    ASSERT_STREQ(readstring(file, str), "DON'T OPEN");
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    KIIR(kibontas("Lajos", 12, 26))
    ASSERT_STREQ(readstring(file, str), "OPEN");
}

TEST(Test, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    KIIR(kibontas("Lajos", 12, 22))
    ASSERT_STREQ(readstring(file, str), "DON'T OPEN");
}

TEST(Test, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    KIIR(kibontas("Lajoska", 12, 25))
    ASSERT_STREQ(readstring(file, str), "DON'T OPEN");
}

TEST(Test, 07) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    KIIR(kibontas("Ferenc", 12, 24))
    ASSERT_STREQ(readstring(file, str), "DON'T OPEN");
}
