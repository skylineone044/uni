#include <gtest/gtest.h>

#define main main_0
#include "../src/akcio.cpp"
#undef main

#include "../../tools.cpp"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t[] = {30, 55, 22};
    int res[] = {27, 49, 19};
    int len = 3;

    akcio(t, len);
    ASSERT_ARRAY_EQ(t, res, len);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t[] = {100, 502, 241, 100, 5, 10, 50};
    int res[] = {90, 451, 216, 90, 4, 9, 45};
    int len = 7;

    akcio(t, len);
    ASSERT_ARRAY_EQ(t, res, len);
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t[] = {0};
    int res[] = {0};
    int len = 1;

    akcio(t, len);
    ASSERT_ARRAY_EQ(t, res, len);
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t[] = {100, 50, 2121, 40000, 4212, 535, 10, 0};
    int res[] = {90, 45, 1908, 36000, 3790, 481, 9, 0};
    int len = 8;

    akcio(t, len);
    ASSERT_ARRAY_EQ(t, res, len);
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22};
    int res[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 18, 19};
    int len = 22;

    akcio(t, len);
    ASSERT_ARRAY_EQ(t, res, len);
}
