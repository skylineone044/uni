#include <gtest/gtest.h>

#define main main_0
#include "../src/alvas.cpp"
#undef main

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    aktualisEv = 2022;
    ASSERT_EQ(aktualisEv, 2022);

    aktualisEv = 2025;
    ASSERT_EQ(aktualisEv, 2025);

    aktualisEv = 2050.77;
    ASSERT_EQ(aktualisEv, 2050);
}