#include <iostream>
#include <iomanip>

int main() {
    double osszeg = 0;
    int counter = 0;

    int szam = 0;

    while (true) {
        std::cin >> szam;

        if (szam == 0) {
            break;
        }

        osszeg += szam;
        counter++;
    }

    std::cout << std::fixed << std::setprecision(2) << osszeg / counter;
    return 0;
}