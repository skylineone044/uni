#include <iostream>

using namespace std;

struct Disk {
    string felirat;
    bool okosDisk;
};

struct Shelf {
    int sorszam;
    Disk** diskek;
    int ferohely;
};

bool diskeketFelhelyez(Shelf* shelves[], Disk* disks[]) {
    bool currentDiskType = false;
    int shelf_id = 0;
    int slot_id = 0;

    for (int i = 0; i < 2; ++i) {
        for (int j = 0; disks[j] != nullptr; j++) { // go through the disks

            if (disks[j]->okosDisk == currentDiskType) {
                if (shelves[shelf_id] == nullptr) { // if true then van meg disk, de nincs tobb shelf
                    return false;
                }
                shelves[shelf_id]->diskek[slot_id] = disks[j];
                slot_id++;

                if (slot_id == shelves[shelf_id]->ferohely) {
                    shelf_id++;
                    slot_id = 0;
                }
            }
        }

        currentDiskType = true;
    }

    return true;
}