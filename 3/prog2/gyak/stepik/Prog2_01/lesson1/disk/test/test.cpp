#include <gtest/gtest.h>

#define main main_0
#include "../src/disk.cpp"
#undef main

Disk d1 = {"asd", false};
Disk d2 = {"vbad", false};
Disk d3 = {"bjk", false};
Disk d4 = {"mbdfn", false};
Disk d5 = {"sdjnfb", false};
Disk d6 = {"amsjkn", false};
Disk d7 = {"mkdvnsd", false};
Disk d8 = {"adjvnk", true};
Disk d9 = {"jkldbns", true};
Disk d10 = {"mbjkdfn", true};
Disk d11 = {"sjkdvbs", true};
Disk d12 = {"mjkdlbn", true};
Disk d13 = {"kldnv", true};
Disk d14 = {"oifgnp", true};

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Shelf s1 = {1, new Disk*[3] {nullptr, nullptr, nullptr}, 3};
    Shelf s2 = {2, new Disk*[4] {nullptr, nullptr, nullptr, nullptr}, 4};
    Shelf s3 = {3, new Disk*[2] {nullptr, nullptr}, 2};

    Shelf* shelves[] = {&s1, &s2, &s3, nullptr};
    Disk* disks[] = {&d1, &d2, &d3, nullptr};

    bool ret = diskeketFelhelyez(shelves, disks);

    ASSERT_EQ(s1.diskek[0], &d1);
    ASSERT_EQ(s1.diskek[1], &d2);
    ASSERT_EQ(s1.diskek[2], &d3);

    ASSERT_EQ(s2.diskek[0], nullptr);
    ASSERT_EQ(s2.diskek[1], nullptr);
    ASSERT_EQ(s2.diskek[2], nullptr);
    ASSERT_EQ(s2.diskek[3], nullptr);

    ASSERT_EQ(s3.diskek[0], nullptr);
    ASSERT_EQ(s3.diskek[1], nullptr);

    ASSERT_EQ(ret, true);

    delete[] s1.diskek;
    delete[] s2.diskek;
    delete[] s3.diskek;
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Shelf s1 = {1, new Disk*[3] {nullptr, nullptr, nullptr}, 3};
    Shelf s2 = {2, new Disk*[4] {nullptr, nullptr, nullptr, nullptr}, 4};
    Shelf s3 = {3, new Disk*[2] {nullptr, nullptr}, 2};

    Shelf* shelves[] = {&s1, &s2, &s3, nullptr};
    Disk* disks[] = {&d1, &d2, &d3, &d4, &d5, &d6, &d7, nullptr};

    bool ret = diskeketFelhelyez(shelves, disks);

    ASSERT_EQ(s1.diskek[0], &d1);
    ASSERT_EQ(s1.diskek[1], &d2);
    ASSERT_EQ(s1.diskek[2], &d3);

    ASSERT_EQ(s2.diskek[0], &d4);
    ASSERT_EQ(s2.diskek[1], &d5);
    ASSERT_EQ(s2.diskek[2], &d6);
    ASSERT_EQ(s2.diskek[3], &d7);

    ASSERT_EQ(s3.diskek[0], nullptr);
    ASSERT_EQ(s3.diskek[1], nullptr);

    ASSERT_EQ(ret, true);

    delete[] s1.diskek;
    delete[] s2.diskek;
    delete[] s3.diskek;
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Shelf s1 = {1, new Disk*[3] {nullptr, nullptr, nullptr}, 3};
    Shelf s2 = {2, new Disk*[4] {nullptr, nullptr, nullptr, nullptr}, 4};
    Shelf s3 = {3, new Disk*[2] {nullptr, nullptr}, 2};

    Shelf* shelves[] = {&s1, &s2, &s3, nullptr};
    Disk* disks[] = {&d9, &d1, &d2, &d8, &d3, &d4, &d5, &d6, &d7, nullptr};

    bool ret = diskeketFelhelyez(shelves, disks);

    ASSERT_EQ(s1.diskek[0], &d1);
    ASSERT_EQ(s1.diskek[1], &d2);
    ASSERT_EQ(s1.diskek[2], &d3);

    ASSERT_EQ(s2.diskek[0], &d4);
    ASSERT_EQ(s2.diskek[1], &d5);
    ASSERT_EQ(s2.diskek[2], &d6);
    ASSERT_EQ(s2.diskek[3], &d7);

    ASSERT_EQ(s3.diskek[0], &d9);
    ASSERT_EQ(s3.diskek[1], &d8);

    ASSERT_EQ(ret, true);

    delete[] s1.diskek;
    delete[] s2.diskek;
    delete[] s3.diskek;
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Shelf s1 = {1, new Disk*[3] {nullptr, nullptr, nullptr}, 3};
    Shelf s2 = {2, new Disk*[4] {nullptr, nullptr, nullptr, nullptr}, 4};
    Shelf s3 = {3, new Disk*[2] {nullptr, nullptr}, 2};

    Shelf* shelves[] = {&s1, &s2, &s3, nullptr};
    Disk* disks[] = {&d9, &d1, &d10, &d2, &d8, &d3, &d4, &d5, &d6, &d7, nullptr};

    bool ret = diskeketFelhelyez(shelves, disks);

    ASSERT_EQ(s1.diskek[0], &d1);
    ASSERT_EQ(s1.diskek[1], &d2);
    ASSERT_EQ(s1.diskek[2], &d3);

    ASSERT_EQ(s2.diskek[0], &d4);
    ASSERT_EQ(s2.diskek[1], &d5);
    ASSERT_EQ(s2.diskek[2], &d6);
    ASSERT_EQ(s2.diskek[3], &d7);

    ASSERT_EQ(s3.diskek[0], &d9);
    ASSERT_EQ(s3.diskek[1], &d10);

    ASSERT_EQ(ret, false);

    delete[] s1.diskek;
    delete[] s2.diskek;
    delete[] s3.diskek;
}

TEST(Test, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Shelf s1 = {1, new Disk*[2] {nullptr, nullptr}, 2};
    Shelf s2 = {2, new Disk*[3] {nullptr, nullptr, nullptr}, 3};

    Shelf* shelves[] = {&s1, &s2, nullptr};
    Disk* disks[] = {&d8, &d9, &d10, nullptr};

    bool ret = diskeketFelhelyez(shelves, disks);

    ASSERT_EQ(s1.diskek[0], &d8);
    ASSERT_EQ(s1.diskek[1], &d9);

    ASSERT_EQ(s2.diskek[0], &d10);
    ASSERT_EQ(s2.diskek[1], nullptr);
    ASSERT_EQ(s2.diskek[2], nullptr);

    ASSERT_EQ(ret, true);

    delete[] s1.diskek;
    delete[] s2.diskek;
}

TEST(Test, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Shelf s1 = {1, new Disk*[2] {nullptr, nullptr}, 2};

    Shelf* shelves[] = {&s1, nullptr};
    Disk* disks[] = {&d8, &d9, &d10, nullptr};

    bool ret = diskeketFelhelyez(shelves, disks);

    ASSERT_EQ(s1.diskek[0], &d8);
    ASSERT_EQ(s1.diskek[1], &d9);

    ASSERT_EQ(ret, false);

    delete[] s1.diskek;
}

TEST(Test, 07) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Shelf s1 = {1, new Disk*[2] {nullptr, nullptr}, 2};
    Shelf s2 = {2, new Disk*[3] {nullptr, nullptr, nullptr}, 3};

    Shelf* shelves[] = {&s1, &s2, nullptr};
    Disk* disks[] = {&d1, &d2, &d3, &d4, &d5, &d6, nullptr};

    bool ret = diskeketFelhelyez(shelves, disks);

    ASSERT_EQ(s1.diskek[0], &d1);
    ASSERT_EQ(s1.diskek[1], &d2);

    ASSERT_EQ(s2.diskek[0], &d3);
    ASSERT_EQ(s2.diskek[1], &d4);
    ASSERT_EQ(s2.diskek[2], &d5);

    ASSERT_EQ(ret, false);

    delete[] s1.diskek;
    delete[] s2.diskek;
}

TEST(Test, 08) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Shelf s1 = {1, new Disk*[2] {nullptr, nullptr}, 2};
    Shelf s2 = {2, new Disk*[3] {nullptr, nullptr, nullptr}, 3};

    Shelf* shelves[] = {&s1, &s2, nullptr};
    Disk* disks[] = {nullptr};

    bool ret = diskeketFelhelyez(shelves, disks);

    ASSERT_EQ(s1.diskek[0], nullptr);
    ASSERT_EQ(s1.diskek[1], nullptr);

    ASSERT_EQ(s2.diskek[0], nullptr);
    ASSERT_EQ(s2.diskek[1], nullptr);
    ASSERT_EQ(s2.diskek[2], nullptr);

    ASSERT_EQ(ret, true);

    delete[] s1.diskek;
    delete[] s2.diskek;
}

TEST(Test, 09) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Shelf* shelves[] = {nullptr};
    Disk* disks[] = {nullptr};

    bool ret = diskeketFelhelyez(shelves, disks);

    ASSERT_EQ(ret, true);
}

TEST(Test, 10) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Shelf* shelves[] = {nullptr};
    Disk* disks[] = {&d1, nullptr};

    bool ret = diskeketFelhelyez(shelves, disks);

    ASSERT_EQ(ret, false);
}