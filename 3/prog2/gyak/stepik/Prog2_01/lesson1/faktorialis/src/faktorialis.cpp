int faktorialis(int szam) {
    int eredmeny = 1;
    for (int i = 1; i <= szam; ++i) {
        eredmeny *= i;
    }
    return eredmeny;
}