#include <gtest/gtest.h>

#define main main_0
#include "../src/hiba.cpp"
#undef main

#include "../../tools.cpp"

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char res[100];
    IO("", log("cica"), res)
    ASSERT_STREQ("", res);
    ASSERT_STREQ("cica\n", error_string);
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char res[100];
    IO("", log("itt vannak a lovacskak"), res)
    ASSERT_STREQ("", res);
    ASSERT_STREQ("itt vannak a lovacskak\n", error_string);
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char res[100];
    IO("", log("a csibek nem esznek tulipanokat"), res)
    ASSERT_STREQ("", res);
    ASSERT_STREQ("a csibek nem esznek tulipanokat\n", error_string);
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char res[100];
    IO("", log("az operacios rendszer elkeszult"), res)
    ASSERT_STREQ("", res);
    ASSERT_STREQ("az operacios rendszer elkeszult\n", error_string);
}

TEST(Test, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char res[100];
    IO("", log("a mehek szomoruak"), res)
    ASSERT_STREQ("", res);
    ASSERT_STREQ("a mehek szomoruak\n", error_string);
}

TEST(Test, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char res[100];
    IO("", log("a bekak brekegnek"), res)
    ASSERT_STREQ("", res);
    ASSERT_STREQ("a bekak brekegnek\n", error_string);
}