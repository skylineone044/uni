#include <gtest/gtest.h>

#define main main_0
#include "../src/homerseklet.cpp"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t[] = {4, 5};
    ASSERT_EQ(hideg(t, 2), 0);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t[] = {-5};
    ASSERT_EQ(hideg(t, 1), 1);
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t[] = {1, -5};
    ASSERT_EQ(hideg(t, 0), 0);
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t[] = {1, 1, 1, -1, 1, -1, 1, 1};
    ASSERT_EQ(hideg(t, 8), 2);
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t[] = {7, -5, 5};
    ASSERT_EQ(hideg(t, 3), 1);
}

TEST(Teszt, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t[] = {5, 0, -5, -1, 1, 2, 15, 30, -2, 0};
    ASSERT_EQ(hideg(t, 10), 3);
}

TEST(Teszt, 07) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t[] = {0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1};
    ASSERT_EQ(hideg(t, 8), 0);
}

TEST(Teszt, 08) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t[] = {-15, -30, -44, -46, -55, -58};
    ASSERT_EQ(hideg(t, 6), 6);
}