#include <climits>
struct Joslat {
    int honap;
    int nap;
    int homerseklet;
};

Joslat* legmelegebb(Joslat* joslatok[]) {
    int i = 0;

    int legnagyobb_homerseklet = INT_MIN;
    Joslat* legmelegebb_nap = nullptr;
    while (joslatok[i] != nullptr) {
        if (joslatok[i]->homerseklet > legnagyobb_homerseklet) {
            legnagyobb_homerseklet = joslatok[i]->homerseklet;
            legmelegebb_nap = joslatok[i];
        }

        i++;
    }
    return legmelegebb_nap;
}