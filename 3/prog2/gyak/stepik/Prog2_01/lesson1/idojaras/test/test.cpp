#include <gtest/gtest.h>

#define main main_0
#include "../src/idojaras.cpp"
#undef main

Joslat j1 = {9, 7, 20};
Joslat j2 = {9, 8, 21};
Joslat j3 = {9, 9, 17};
Joslat j4 = {9, 10, 19};
Joslat j5 = {5, 7, 25};
Joslat j6 = {1, 2, -3};
Joslat j7 = {2, 14, -2};
Joslat j8 = {4, 2, 1};

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Joslat* joslatok[] = {&j1, &j2, nullptr};
    Joslat* megoldas = &j2;

    Joslat* res = legmelegebb(joslatok);
    ASSERT_EQ(res, megoldas);
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Joslat* joslatok[] = {&j1, &j2, &j3, &j4, &j5, &j6, nullptr};
    Joslat* megoldas = &j5;

    Joslat* res = legmelegebb(joslatok);
    ASSERT_EQ(res, megoldas);
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Joslat* joslatok[] = {&j6, nullptr};
    Joslat* megoldas = &j6;

    Joslat* res = legmelegebb(joslatok);
    ASSERT_EQ(res, megoldas);
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Joslat* joslatok[] = {&j6, &j5, &j7, nullptr};
    Joslat* megoldas = &j5;

    Joslat* res = legmelegebb(joslatok);
    ASSERT_EQ(res, megoldas);
}

TEST(Test, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Joslat* joslatok[] = {nullptr};
    Joslat* megoldas = nullptr;

    Joslat* res = legmelegebb(joslatok);
    ASSERT_EQ(res, megoldas);
}

TEST(Test, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Joslat* joslatok[] = {&j6, &j7, &j8, nullptr};
    Joslat* megoldas = &j8;

    Joslat* res = legmelegebb(joslatok);
    ASSERT_EQ(res, megoldas);
}

TEST(Test, 07) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Joslat* joslatok[] = {&j1, &j2, &j3, &j4, &j5, &j6, &j7, &j8, nullptr};
    Joslat* megoldas = &j5;

    Joslat* res = legmelegebb(joslatok);
    ASSERT_EQ(res, megoldas);
}