#include <gtest/gtest.h>

#define main main_0
#include "../src/kedvezmeny.cpp"
#undef main

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_FLOAT_EQ(bevasarlas(10), 9);
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_FLOAT_EQ(bevasarlas(7.73f), 6.957f);
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_FLOAT_EQ(bevasarlas(6362.22f), 5725.998f);
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_FLOAT_EQ(bevasarlas(0.01f), 0.009f);
}