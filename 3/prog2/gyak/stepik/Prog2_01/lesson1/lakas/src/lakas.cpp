struct Szoba {
    double x;
    double y;
};

struct Lakas {
    Szoba szobak[100];
    int szobaSzam;
};

double terulet(Lakas lakas) {
    double eredmeny = 0;
    for (int i = 0; i < lakas.szobaSzam; ++i) {
        eredmeny += lakas.szobak[i].x * lakas.szobak[i].y;
    }
    return eredmeny;
}

Lakas nagyobb(Lakas l1, Lakas l2) {
    if (terulet(l1) >= terulet(l2) ) {
        return l1;
    } else return l2;
}
