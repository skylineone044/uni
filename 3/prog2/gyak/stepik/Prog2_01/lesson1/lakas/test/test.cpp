#include <gtest/gtest.h>

#define main main_0
#include "../src/lakas.cpp"
#undef main

Szoba s1 = {5, 7}; // 35
Szoba s2 = {4.4, 6.3}; // 27.72
Szoba s3 = {6.2, 6.2}; // 38.44
Szoba s4 = {6.5, 4.9}; // 31.85
Szoba s5 = {10.2, 2.2}; // 22.44
Szoba s6 = {1.2, 1.6}; // 1.92
Szoba s7 = {2.3, 1.5}; // 3.45

bool operator==(const Szoba& sz1, const Szoba& sz2) {
    return sz1.x == sz2.x && sz1.y == sz2.y;
}

bool operator!=(const Szoba& sz1, const Szoba& sz2) {
    return !operator==(sz1, sz2);
}

bool operator==(const Lakas& l1, const Lakas& l2) {
    if (l1.szobaSzam != l2.szobaSzam) {
        return false;
    }

    for (int i = 0; i < l1.szobaSzam; i++) {
        if (l1.szobak[i] != l2.szobak[i]) {
            return false;
        }
    }

    return true;
}

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Lakas l1 = {{s1, s2, s3}, 3};
    Lakas l2 = {{s4, s5, s6, s7}, 4};

    ASSERT_EQ(l1, nagyobb(l1, l2));
    ASSERT_EQ(l1, nagyobb(l2, l1));
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Lakas l1 = {{s1, s6, s5}, 3};
    Lakas l2 = {{s1, s3}, 2};

    ASSERT_EQ(l2, nagyobb(l1, l2));
    ASSERT_EQ(l2, nagyobb(l2, l1));
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Lakas l1 = {{s3}, 1};
    Lakas l2 = {{s5, s6, s6, s6, s6, s7, s6}, 7};

    ASSERT_EQ(l1, nagyobb(l1, l2));
    ASSERT_EQ(l1, nagyobb(l2, l1));
}