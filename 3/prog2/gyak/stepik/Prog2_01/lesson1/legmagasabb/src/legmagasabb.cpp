#include <climits>
#include <algorithm>

int legmagasabb(const int* magassagok, int db) {
    return *std::max_element(magassagok, magassagok + db);
}