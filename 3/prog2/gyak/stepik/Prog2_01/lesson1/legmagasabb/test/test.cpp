#include <gtest/gtest.h>

#define main main_0
#include "../src/legmagasabb.cpp"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t[] = {120, 125, 130, 135};
    int db = 4;
    int res = 135;

    ASSERT_EQ(legmagasabb(t, db), res);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t[] = {-4, 5, 10, 105, -200, 0, -20};
    int db = 7;
    int res = 105;

    ASSERT_EQ(legmagasabb(t, db), res);
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t[] = {170};
    int db = 1;
    int res = 170;

    ASSERT_EQ(legmagasabb(t, db), res);
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t[] = {120, -155, 110, -200};
    int db = 4;
    int res = 120;

    ASSERT_EQ(legmagasabb(t, db), res);
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t[] = {-15, -30, -55, -14, -6, -80, -22, -40, -202, -152, -5, -6, -5, -5};
    int db = 14;
    int res = -5;

    ASSERT_EQ(legmagasabb(t, db), res);
}

TEST(Teszt, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int t[] = {-2100000000, -2104643230, -2141704521, -2123344410};
    int db = 4;
    int res = -2100000000;

    ASSERT_EQ(legmagasabb(t, db), res);
}
