#include <gtest/gtest.h>
#include <cstring>

#define main main_0
#include "../src/patkany.cpp"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Patkany h;
    strcpy(h.nev, "Veronika");
    strcpy(h.szin, "nem emlekszem");
    h.tudHarmonikazni = true;

    ASSERT_STREQ(h.nev, "Veronika");
    ASSERT_STREQ(h.szin, "nem emlekszem");
    ASSERT_EQ(h.tudHarmonikazni, true);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Patkany h;
    strcpy(h.nev, "Andras");
    strcpy(h.szin, "barna");
    h.tudHarmonikazni = false;

    ASSERT_STREQ(h.nev, "Andras");
    ASSERT_STREQ(h.szin, "barna");
    ASSERT_EQ(h.tudHarmonikazni, false);

    h.tudHarmonikazni = 67;
    ASSERT_EQ(h.tudHarmonikazni, true);
}
