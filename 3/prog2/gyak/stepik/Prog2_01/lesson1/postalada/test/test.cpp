#include <gtest/gtest.h>
#include <cstring>

#define main main_0
#include "../src/postalada.cpp"
#undef main

Level a1 = {"gyorgy", "anett", 2411};
Level a2 = {"karoly", "peter", 5232};
Level a3 = {"natasa", "balazs", 52335};
Level a4 = {"anna", "petra", 5232};

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Postalada p;
    strcpy(p.szin, "piros");
    p.levelek[0] = a1;
    p.levelek[1] = a2;
    p.levelek[2] = a3;

    ASSERT_STREQ(p.szin, "piros");
    ASSERT_EQ(p.levelek[0].datum, 2411);
    ASSERT_STREQ(p.levelek[1].felado, "karoly");
    ASSERT_STREQ(p.levelek[2].cimzett, "balazs");
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Postalada p;
    strcpy(p.szin, "zold");
    p.levelek[0] = a3;
    p.levelek[1] = a4;

    ASSERT_STREQ(p.szin, "zold");
    ASSERT_EQ(p.levelek[0].datum, 52335);
    ASSERT_STREQ(p.levelek[0].felado, "natasa");
    ASSERT_STREQ(p.levelek[1].cimzett, "petra");
}