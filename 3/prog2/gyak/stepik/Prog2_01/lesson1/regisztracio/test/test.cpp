#include <gtest/gtest.h>

#define main main_0
#include "../src/regisztracio.cpp"
#undef main

#include "../../tools.cpp"

TEST(Test, visszateresi_ertek) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(main_0(), 0);
}

TEST(Test, sorvege_jel) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    KIIR(main_0())
    char megoldas[100];
    int index = 0;
    do {
        fscanf(file, "%c", &megoldas[index]);
        index++;
    } while (!feof(file));

    megoldas[index-1] = 0;

    ASSERT_STREQ("A jelszo tul gyenge!\n", megoldas);
}