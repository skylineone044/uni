#include <gtest/gtest.h>

#define main main_0
#include "../src/sebessegkorlat.cpp"
#undef main

#include "../../tools.cpp"

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char res[100];
    KIIR(kijelez(100))
    ASSERT_STREQ("Maximalis sebesseg: 100\n", readstring(file, res));
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char res[100];
    KIIR(kijelez(50))
    ASSERT_STREQ("Maximalis sebesseg: 50\n", readstring(file, res));
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char res[100];
    KIIR(kijelez(130))
    ASSERT_STREQ("Maximalis sebesseg: 130\n", readstring(file, res));
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char res[100];
    KIIR(kijelez(20))
    ASSERT_STREQ("Maximalis sebesseg: 20\n", readstring(file, res));
}