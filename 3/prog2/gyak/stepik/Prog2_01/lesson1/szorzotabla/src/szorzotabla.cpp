#include <iostream>
#include <iomanip>

int main() {
    int meret;
    std::cin >> meret;

    // max szam szelesseg
    int negyzet = meret * meret;
    std::string negyzet_str = std::to_string(negyzet);
    int maxLen = negyzet_str.length();

    if (maxLen != 1) {
        maxLen++;
    }

    for (int i = 1; i <= meret; ++i) {
        for (int j = 1; j <= meret; ++j) {
            std::cout << std::setfill(' ') << std::setw( (j == 1 ? maxLen-1 :maxLen) ) << i*j;
        }
        std::cout << "\n";
    }
    return 0;
}