#include <iostream>
#include <iomanip>

void video(int ora, int perc, int render_perc) {
    int osszperc = 0;
    osszperc += ora * 60;
    osszperc += perc;
    osszperc += render_perc;
    osszperc %= 1440;

    int veg_ora = osszperc / 60;
    int veg_perc = osszperc % 60;
    std::cout << "Varhatoan kesz: " << std::setfill('0') << std::setw(2) << veg_ora << ":" << std::setfill('0') << std::setw(2) << veg_perc;
}