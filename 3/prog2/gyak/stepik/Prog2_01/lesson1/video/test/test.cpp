#include <gtest/gtest.h>

#define main main_0
#include "../src/video.cpp"
#undef main

#include "../../tools.cpp"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", video(15, 20, 22), str)
    ASSERT_STREQ(str, "Varhatoan kesz: 15:42");
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", video(8, 50, 80), str)
    ASSERT_STREQ(str, "Varhatoan kesz: 10:10");
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", video(5, 20, 60), str)
    ASSERT_STREQ(str, "Varhatoan kesz: 06:20");
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", video(3, 10, 50), str)
    ASSERT_STREQ(str, "Varhatoan kesz: 04:00");
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", video(23, 50, 10), str)
    ASSERT_STREQ(str, "Varhatoan kesz: 00:00");
}

TEST(Teszt, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", video(20, 20, 240), str)
    ASSERT_STREQ(str, "Varhatoan kesz: 00:20");
}

TEST(Teszt, 07) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", video(1, 1, 1), str)
    ASSERT_STREQ(str, "Varhatoan kesz: 01:02");
}

TEST(Teszt, 08) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", video(23, 50, 720), str)
    ASSERT_STREQ(str, "Varhatoan kesz: 11:50");
}

TEST(Teszt, 09) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", video(5, 20, 1441), str)
    ASSERT_STREQ(str, "Varhatoan kesz: 05:21");
}

TEST(HosszuRender, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", video(12, 0, 1934588200), str)
    ASSERT_STREQ(str, "Varhatoan kesz: 12:40");
}

TEST(HosszuRender, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", video(14, 11, 1660988722), str)
    ASSERT_STREQ(str, "Varhatoan kesz: 23:33");
}

TEST(HosszuRender, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[50000];

    CHECK_RUNNING_TIME(
    IO("", for (int aa = 0; aa < 2000; aa++) video(14, 11, 1660988722), str),
    0.5f
    );
}