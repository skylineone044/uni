#include <string>

using namespace std;

struct Ember {
    string n;
    unsigned int magassag;
};

bool operator<(const Ember& e1, const Ember& e2) {
    return e1.magassag < e2.magassag;
}

bool operator>(const Ember& e1, const Ember& e2) {
    return e1.magassag > e2.magassag;
}