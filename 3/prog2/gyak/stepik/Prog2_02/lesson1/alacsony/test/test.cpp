#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/alacsony.cpp"
#undef main

Ember e1 = {"ajnk", 155};
Ember e2 = {"nisdn", 160};
Ember e3 = {"nvaikn", 186};
Ember e4 = {"mjnsdvn", 173};
Ember e5 = {"maiovn", 186};

TEST(Teszt, kisebb) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_TRUE(e1 < e2);
    ASSERT_TRUE(e1 < e3);
    ASSERT_TRUE(e1 < e4);
    ASSERT_FALSE(e2 < e1);
    ASSERT_TRUE(e2 < e3);
    ASSERT_TRUE(e2 < e4);
    ASSERT_FALSE(e3 < e1);
    ASSERT_FALSE(e3 < e2);
    ASSERT_FALSE(e3 < e4);
    ASSERT_FALSE(e3 < e5);
    ASSERT_TRUE(e4 < e5);
    ASSERT_FALSE(e4 < e2);
    ASSERT_FALSE(e5 < e3);
}

TEST(Teszt, nagyobb) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_FALSE(e1 > e2);
    ASSERT_FALSE(e1 > e3);
    ASSERT_FALSE(e1 > e4);
    ASSERT_TRUE(e2 > e1);
    ASSERT_FALSE(e2 > e3);
    ASSERT_FALSE(e2 > e4);
    ASSERT_TRUE(e3 > e1);
    ASSERT_TRUE(e3 > e2);
    ASSERT_TRUE(e3 > e4);
    ASSERT_FALSE(e3 > e5);
    ASSERT_FALSE(e4 > e5);
    ASSERT_TRUE(e4 > e2);
    ASSERT_FALSE(e5 > e3);
}