#include <string>
#include <iostream>

using namespace std;

bool vasarlas(string termekek[], int darab, int max_korlat) {
    int counter = 0;
    for (int i = 0; i < darab; ++i) {
        counter += stoi( termekek[i].substr(0, termekek[i].find(" ")) );
    }
    std::cout << "counter: " << counter << " \n";
    std::cout << "maxKorlat: " << max_korlat << " \n";
    return max_korlat >= counter;
}