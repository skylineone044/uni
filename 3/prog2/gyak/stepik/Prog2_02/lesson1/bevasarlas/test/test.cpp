#include <gtest/gtest.h>

#define main main_0
#include "../src/bevasarlas.cpp"
#undef main

bool vasarlas(string termekek[], int darab, int max_korlat);

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string termekek[] = {"4 cipo", "3 karfiol"};
    int db = 2;
    int korlat = 6;
    ASSERT_FALSE(vasarlas(termekek, db, korlat));
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string termekek[] = {"4 cipo", "3 karfiol"};
    int db = 2;
    int korlat = 7;
    ASSERT_TRUE(vasarlas(termekek, db, korlat));
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string termekek[] = {"4 cipo", "3 karfiol"};
    int db = 2;
    int korlat = 13;
    ASSERT_TRUE(vasarlas(termekek, db, korlat));
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string termekek[] = {"3 cseresznye", "5 kalacs", "1 szamitogep", "2 papir", "5 tapir", "100 zsebkendo"};
    int db = 6;
    int korlat = 116;
    ASSERT_TRUE(vasarlas(termekek, db, korlat));
}

TEST(Test, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string termekek[] = {"3 cseresznye", "5 kalacs", "1 szamitogep", "2 papir", "5 tapir", "100 zsebkendo"};
    int db = 6;
    int korlat = 115;
    ASSERT_FALSE(vasarlas(termekek, db, korlat));
}

TEST(Test, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string termekek[] = {"0 cseresznye", "0 kalacs", "0 szamitogep", "0 papir", "1 tapir", "0 zsebkendo"};
    int db = 6;
    int korlat = 1;
    ASSERT_TRUE(vasarlas(termekek, db, korlat));
}

TEST(Test, 07) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string termekek[] = {"0 cseresznye", "0 kalacs", "0 szamitogep", "0 papir", "1 tapir", "0 zsebkendo"};
    int db = 6;
    int korlat = 0;
    ASSERT_FALSE(vasarlas(termekek, db, korlat));
}