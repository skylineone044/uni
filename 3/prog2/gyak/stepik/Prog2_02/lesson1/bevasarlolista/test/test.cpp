#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/bevasarlolista.cpp"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    BevasarloLista b = {"LIDD", ""};

    ASSERT_EQ(b.lista, "");

    b += "tej";
    ASSERT_EQ(b.lista, "tej");

    b += "fuzet";
    ASSERT_EQ(b.lista, "tej, fuzet");

    b += "dinnye";
    ASSERT_EQ(b.lista, "tej, fuzet, dinnye");

    b += "holapat";
    ASSERT_EQ(b.lista, "tej, fuzet, dinnye, holapat");
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    BevasarloLista b = {"LIDD", ""};

    ASSERT_EQ(b.lista, "");

    b += "aPhone";
    ASSERT_EQ(b.lista, "aPhone");

    b += "32 kisparna";
    ASSERT_EQ(b.lista, "aPhone, 32 kisparna");

    b += "asvanyviz";
    ASSERT_EQ(b.lista, "aPhone, 32 kisparna, asvanyviz");

    b += "asvanyviz";
    ASSERT_EQ(b.lista, "aPhone, 32 kisparna, asvanyviz, asvanyviz");
}