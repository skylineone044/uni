struct Lada {
    int aktualis_meret = 0;
    int* tartalom = new int[0];
};

void operator+=(Lada& lada, int targy) {
    lada.aktualis_meret++;
    int* placeholder = lada.tartalom;
    lada.tartalom = new int[lada.aktualis_meret];

    for (int i = 0; i < lada.aktualis_meret; ++i) {
        if (i == lada.aktualis_meret-1) {
            lada.tartalom[i] = targy;
        } else {
            lada.tartalom[i] = placeholder[i];
        }
    }
}

void operator+=(Lada& lada1, Lada& lada2) {
    if (&lada1 == &lada2) {
        return;
    }
    int* placeholder = lada1.tartalom;
    lada1.tartalom = new int[lada1.aktualis_meret + lada2.aktualis_meret];

    for (int i = 0; i < lada1.aktualis_meret; ++i) {
        lada1.tartalom[i] = placeholder[i];
    }

    for (int i = 0; i < lada2.aktualis_meret; ++i) {
        lada1.tartalom[lada1.aktualis_meret + i] = lada2.tartalom[i];
    }

    lada1.aktualis_meret += lada2.aktualis_meret;
}