#include <gtest/gtest.h>

#define main main_0
#include "../src/egyesites.cpp"
#undef main

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Lada l1;
    Lada l2;

    ASSERT_EQ(l1.aktualis_meret, 0);
    ASSERT_EQ(l2.aktualis_meret, 0);
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int* elozo = nullptr;

    Lada l1;
    l1 += 5;

    ASSERT_EQ(l1.aktualis_meret, 1);
    ASSERT_EQ(l1.tartalom[0], 5);
    ASSERT_NE(l1.tartalom, elozo);

    elozo = l1.tartalom;

    l1 += 5;

    ASSERT_EQ(l1.aktualis_meret, 2);
    ASSERT_EQ(l1.tartalom[0], 5);
    ASSERT_EQ(l1.tartalom[1], 5);
    ASSERT_NE(l1.tartalom, elozo);
    elozo = l1.tartalom;

    l1 += 2;

    ASSERT_EQ(l1.aktualis_meret, 3);
    ASSERT_EQ(l1.tartalom[0], 5);
    ASSERT_EQ(l1.tartalom[1], 5);
    ASSERT_EQ(l1.tartalom[2], 2);
    ASSERT_NE(l1.tartalom, elozo);

    Lada l2;
    elozo = l2.tartalom;
    l2 += -10;

    ASSERT_EQ(l2.aktualis_meret, 1);
    ASSERT_EQ(l2.tartalom[0], -10);
    ASSERT_NE(l2.tartalom, elozo);
    elozo = l2.tartalom;

    l2 += 1;
    ASSERT_EQ(l2.aktualis_meret, 2);
    ASSERT_EQ(l2.tartalom[0], -10);
    ASSERT_EQ(l2.tartalom[1], 1);
    ASSERT_NE(l2.tartalom, elozo);

    delete[] l1.tartalom;
    delete[] l2.tartalom;
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int* elozoA;
    int* elozoB;

    Lada l1;
    l1 += 5;
    l1 += 7;

    Lada l2;
    l2 += 2;
    l2 += 3;

    elozoA = l1.tartalom;
    elozoB = l2.tartalom;

    l1 += l2;

    ASSERT_EQ(l1.aktualis_meret, 4);
    ASSERT_EQ(l2.aktualis_meret, 2);

    ASSERT_EQ(l1.tartalom[0], 5);
    ASSERT_EQ(l1.tartalom[1], 7);
    ASSERT_EQ(l1.tartalom[2], 2);
    ASSERT_EQ(l1.tartalom[3], 3);

    ASSERT_EQ(l2.tartalom[0], 2);
    ASSERT_EQ(l2.tartalom[1], 3);

    ASSERT_NE(l1.tartalom, elozoA);
    ASSERT_EQ(l2.tartalom, elozoB);

    Lada l3;
    l3 += 7;
    l3 += 2;
    l3 += 0;

    elozoA = l3.tartalom;
    elozoB = l1.tartalom;
    l3 += l1;

    ASSERT_EQ(l3.aktualis_meret, 7);
    ASSERT_EQ(l1.aktualis_meret, 4);

    ASSERT_EQ(l3.tartalom[0], 7);
    ASSERT_EQ(l3.tartalom[1], 2);
    ASSERT_EQ(l3.tartalom[2], 0);
    ASSERT_EQ(l3.tartalom[3], 5);
    ASSERT_EQ(l3.tartalom[4], 7);
    ASSERT_EQ(l3.tartalom[5], 2);
    ASSERT_EQ(l3.tartalom[6], 3);

    ASSERT_EQ(l1.tartalom[0], 5);
    ASSERT_EQ(l1.tartalom[1], 7);
    ASSERT_EQ(l1.tartalom[2], 2);
    ASSERT_EQ(l1.tartalom[3], 3);

    ASSERT_NE(l3.tartalom, elozoA);
    ASSERT_EQ(l1.tartalom, elozoB);

    Lada l4;
    l4 += 75234;

    elozoA = l3.tartalom;
    elozoB = l4.tartalom;

    l3 += l4;

    ASSERT_EQ(l3.aktualis_meret, 8);
    ASSERT_EQ(l4.aktualis_meret, 1);

    ASSERT_EQ(l3.tartalom[0], 7);
    ASSERT_EQ(l3.tartalom[1], 2);
    ASSERT_EQ(l3.tartalom[2], 0);
    ASSERT_EQ(l3.tartalom[3], 5);
    ASSERT_EQ(l3.tartalom[4], 7);
    ASSERT_EQ(l3.tartalom[5], 2);
    ASSERT_EQ(l3.tartalom[6], 3);
    ASSERT_EQ(l3.tartalom[7], 75234);

    ASSERT_EQ(l4.tartalom[0], 75234);

    ASSERT_NE(l3.tartalom, elozoA);
    ASSERT_EQ(l4.tartalom, elozoB);

    delete[] l1.tartalom;
    delete[] l2.tartalom;
    delete[] l3.tartalom;
    delete[] l4.tartalom;
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Lada l1;
    Lada l2;

    l2 += 5;
    l1 += l2;

    ASSERT_EQ(l1.aktualis_meret, 1);
    ASSERT_EQ(l2.aktualis_meret, 1);

    ASSERT_EQ(l1.tartalom[0], 5);
    ASSERT_EQ(l2.tartalom[0], 5);

    delete[] l1.tartalom;
    delete[] l2.tartalom;
}

TEST(Test, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Lada l1;
    l1 += 5;
    l1 += 7;

    l1 += l1;

    ASSERT_EQ(l1.aktualis_meret, 2);

    ASSERT_EQ(l1.tartalom[0], 5);
    ASSERT_EQ(l1.tartalom[1], 7);
}

TEST(Test, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Lada l1;
    l1 += 5;

    Lada l2;
    l2 += 5;

    l1 += l2;

    ASSERT_EQ(l1.aktualis_meret, 2);
    ASSERT_EQ(l2.aktualis_meret, 1);

    ASSERT_EQ(l1.tartalom[0], 5);
    ASSERT_EQ(l1.tartalom[1], 5);
    ASSERT_EQ(l2.tartalom[0], 5);
}