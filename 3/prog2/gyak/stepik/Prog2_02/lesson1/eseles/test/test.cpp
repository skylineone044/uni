#include <gtest/gtest.h>

#include <string>

using namespace std;

#define main main_0
#include "../src/eseles.cpp"
#undef main

string operator&(string& str2, string& str1);

void teszt(string a, string b, string res) {
    int indexA = 0;
    int indexB = 0;
    for (int i = 0; i < res.length(); i++) {
        bool successA = false;
        bool successB = false;
        for (int ia = indexA; ia < a.length(); ia++) {
            if (a[ia] == res[i]) {
                indexA = ia + 1;
                successA = true;
                break;
            }
        }

        for (int ib = indexB; ib < b.length(); ib++) {
            if (b[ib] == res[i]) {
                indexB = ib + 1;
                successB = true;
                break;
            }
        }

        if (!successA || !successB) {
            FAIL();
        }
    }
}

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string a = "babaabaab";
    string b = "abbababa";
    int hossz = 6;
    string res = a & b;
    ASSERT_EQ(hossz, res.length());
    teszt(a, b, res);
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string a = "macska";
    string b = "kutya";
    int hossz = 2;
    string res = a & b;
    ASSERT_EQ(hossz, res.length());
    teszt(a, b, res);
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string a = "aaaaabbababbbbababbbbababbbbabababbbabab";
    string b = "bbb";
    int hossz = 3;
    string res = a & b;
    ASSERT_EQ(hossz, res.length());
    teszt(a, b, res);
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string a = "bbb";
    string b = "aaaaabbababbbbababbbbababbbbabababbbabab";
    int hossz = 3;
    string res = a & b;
    ASSERT_EQ(hossz, res.length());
    teszt(a, b, res);
}

TEST(Test, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string a = "bbbabbbabbbbabbbabbbabb";
    string b = "aaaaabbababbbbababbbbababbbbabababbbabab";
    int hossz = 23;
    string res = a & b;
    ASSERT_EQ(hossz, res.length());
    teszt(a, b, res);
}

TEST(Test, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string a = "a kiscica felmaszott a fara";
    string b = "a macska szeret aludni";
    int hossz = 10;
    string res = a & b;
    ASSERT_EQ(hossz, res.length());
    teszt(a, b, res);
}

TEST(Test, 07) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string a = "ezegykonnyufeladat";
    string b = "nemnehezafeladat";
    int hossz = 9;
    string res = a & b;
    ASSERT_EQ(hossz, res.length());
    teszt(a, b, res);
}

TEST(Test, 08) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string a = "abbbbbbbbbbbb";
    string b = "baaaaaaaa";
    int hossz = 1;
    string res = a & b;
    ASSERT_EQ(hossz, res.length());
    teszt(a, b, res);
}

TEST(Test, 09) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string a = "bbbbbbbbbbbbbbbbbbbba";
    string b = "a";
    int hossz = 1;
    string res = a & b;
    ASSERT_EQ(hossz, res.length());
    teszt(a, b, res);
}

TEST(Test, 10) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string a = "aaaaaaaaaaaaaaaaa";
    string b = "bbbbbbbbbbbb";
    int hossz = 0;
    string res = a & b;
    ASSERT_EQ(hossz, res.length());
    teszt(a, b, res);
}

TEST(Test, 11) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string a = "aaaaaaaaaaaaaaaaaaaaaaaaaasaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
    string b = "a";
    int hossz = 1;
    string res = a & b;
    ASSERT_EQ(hossz, res.length());
    teszt(a, b, res);
}

TEST(Test, 12) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string a = "aaaaaba";
    string b = "bbbaaaaaa";
    int hossz = 6;
    string res = a & b;
    ASSERT_EQ(hossz, res.length());
    teszt(a, b, res);
}

TEST(Test, 13) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string a = "bbbaaaaaa";
    string b = "aaaaaba";
    int hossz = 6;
    string res = a & b;
    ASSERT_EQ(hossz, res.length());
    teszt(a, b, res);
}

TEST(Test, 14) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string a = "asvnkaivuaibgqwe";
    string b = "asvnkaivuaibgqwe";
    int hossz = 16;
    string res = a & b;
    ASSERT_EQ(hossz, res.length());
    teszt(a, b, res);
}

TEST(Test, 15) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string a = "aabca";
    string b = "baaca";
    int hossz = 4;
    string res = a & b;
    ASSERT_EQ(hossz, res.length());
    teszt(a, b, res);
}

TEST(Test, 16) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string a = "aabbaab";
    string b = "bbab";
    int hossz = 4;
    string res = a & b;
    ASSERT_EQ(hossz, res.length());
    teszt(a, b, res);
}

TEST(Test, 17) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string a = "abccabc";
    string b = "bcaac";
    int hossz = 4;
    string res = a & b;
    ASSERT_EQ(hossz, res.length());
    teszt(a, b, res);
}