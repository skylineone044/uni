struct Gyakorlat {
    int max_letszam;
    int aktualis_letszam;
};

Gyakorlat operator&(Gyakorlat gy1, Gyakorlat gy2) {
    Gyakorlat gyuj;
    gyuj.max_letszam = gy1.max_letszam > gy2.max_letszam ? gy1.max_letszam : gy2.max_letszam;
    gyuj.aktualis_letszam = gy1.aktualis_letszam + gy2.aktualis_letszam;
    return gyuj;
}