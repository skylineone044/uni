#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/gyakorlat.cpp"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Gyakorlat gy1 = {30, 12};
    Gyakorlat gy2 = {20, 11};

    Gyakorlat gy3 = gy1 & gy2;

    ASSERT_EQ(gy1.max_letszam, 30);
    ASSERT_EQ(gy1.aktualis_letszam, 12);
    ASSERT_EQ(gy2.max_letszam, 20);
    ASSERT_EQ(gy2.aktualis_letszam, 11);

    ASSERT_EQ(gy3.max_letszam, 30);
    ASSERT_EQ(gy3.aktualis_letszam, 23);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Gyakorlat gy1 = {50, 5};
    Gyakorlat gy2 = {10, 5};

    Gyakorlat gy3 = gy1 & gy2;

    ASSERT_EQ(gy3.max_letszam, 50);
    ASSERT_EQ(gy3.aktualis_letszam, 10);
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Gyakorlat gy1 = {5, 5};
    Gyakorlat gy2 = {10, 8};

    Gyakorlat gy3 = gy1 & gy2;

    ASSERT_EQ(gy3.max_letszam, 10);
    ASSERT_EQ(gy3.aktualis_letszam, 13);
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Gyakorlat gy1 = {30, 0};
    Gyakorlat gy2 = {10, 10};

    Gyakorlat gy3 = gy1 & gy2;

    ASSERT_EQ(gy3.max_letszam, 30);
    ASSERT_EQ(gy3.aktualis_letszam, 10);
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Gyakorlat gy1 = {20, 7};
    Gyakorlat gy2 = {20, 5};

    Gyakorlat gy3 = gy1 & gy2;

    ASSERT_EQ(gy3.max_letszam, 20);
    ASSERT_EQ(gy3.aktualis_letszam, 12);
}

TEST(Teszt, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Gyakorlat gy1 = {100, 99};
    Gyakorlat gy2 = {2, 1};

    Gyakorlat gy3 = gy1 & gy2;

    ASSERT_EQ(gy3.max_letszam, 100);
    ASSERT_EQ(gy3.aktualis_letszam, 100);
}