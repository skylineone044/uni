#include <iostream>

using namespace std;

int main() {
    string szamString;
    cin >> szamString;

    if ('0' <= szamString[0] && szamString[0] <= '9') {
        if (!('0' <= szamString[1] && szamString[1] <= '9')) {
            switch (szamString[0]) {
                case '0':
                    cout << "nulla";
                    break;
                case '1':
                    cout << "egy";
                    break;
                case '2':
                    cout << "ketto";
                    break;
                case '3':
                    cout << "harom";
                    break;
                case '4':
                    cout << "negy";
                    break;
                case '5':
                    cout << "ot";
                    break;
                case '6':
                    cout << "hat";
                    break;
                case '7':
                    cout << "het";
                    break;
                case '8':
                    cout << "nyolc";
                    break;
                case '9':
                    cout << "kilenc";
                    break;
            }
            return 0;
        }
    }
    cerr << "hibas szamjegy!";
    return 0;
}