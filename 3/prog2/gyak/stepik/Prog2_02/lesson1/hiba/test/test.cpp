#include <gtest/gtest.h>

#define main main_0
#include "../src/hiba.cpp"
#undef main

#include "../../tools.cpp"

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("5", main_0(), str);
    ASSERT_STREQ(str, "ot");
    ASSERT_STREQ(error_string, "");
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("160", main_0(), str);
    ASSERT_STREQ(str, "");
    ASSERT_STREQ(error_string, "hibas szamjegy!");
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("2", main_0(), str);
    ASSERT_STREQ(str, "ketto");
    ASSERT_STREQ(error_string, "");
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("10", main_0(), str);
    ASSERT_STREQ(str, "");
    ASSERT_STREQ(error_string, "hibas szamjegy!");
}

TEST(Test, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("1", main_0(), str);
    ASSERT_STREQ(str, "egy");
    ASSERT_STREQ(error_string, "");
}

TEST(Test, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("6", main_0(), str);
    ASSERT_STREQ(str, "hat");
    ASSERT_STREQ(error_string, "");
}

TEST(Test, 07) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("4", main_0(), str);
    ASSERT_STREQ(str, "negy");
    ASSERT_STREQ(error_string, "");
}

TEST(Test, 08) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("3", main_0(), str);
    ASSERT_STREQ(str, "harom");
    ASSERT_STREQ(error_string, "");
}

TEST(Test, 09) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("9", main_0(), str);
    ASSERT_STREQ(str, "kilenc");
    ASSERT_STREQ(error_string, "");
}

TEST(Test, 10) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("8", main_0(), str);
    ASSERT_STREQ(str, "nyolc");
    ASSERT_STREQ(error_string, "");
}

TEST(Test, 11) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("0", main_0(), str);
    ASSERT_STREQ(str, "nulla");
    ASSERT_STREQ(error_string, "");
}

TEST(Test, 12) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("7", main_0(), str);
    ASSERT_STREQ(str, "het");
    ASSERT_STREQ(error_string, "");
}