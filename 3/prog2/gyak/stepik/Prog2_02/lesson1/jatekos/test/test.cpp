#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/jatekos.cpp"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Jatekos j1 = {"asd", 3};
    Jatekos j2 = {"sdga", 5};

    j1 >>= 5;
    ASSERT_EQ(j1.szint, 5);

    j1 >>= 8;
    ASSERT_EQ(j1.szint, 8);

    j1 >>= 15;
    ASSERT_EQ(j1.szint, 15);

    j1 >>= 15;
    ASSERT_EQ(j1.szint, 15);

    j1 >>= 12;
    ASSERT_EQ(j1.szint, 15);

    j1 >>= 1;
    ASSERT_EQ(j1.szint, 15);

    j1 >>= 30;
    ASSERT_EQ(j1.szint, 30);

    j2 >>= 10;
    ASSERT_EQ(j1.szint, 30);
    ASSERT_EQ(j2.szint, 10);
}