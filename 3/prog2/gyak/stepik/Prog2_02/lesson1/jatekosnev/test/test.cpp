#include <gtest/gtest.h>

#define main main_0
#include "../src/jatekosnev.cpp"
#undef main

#include "../../tools.cpp"

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("macska macska", main_0(), str)
    ASSERT_STREQ(str, "egyforma\n");
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("macska MACSKA", main_0(), str)
    ASSERT_STREQ(str, "kulonbozo\n");
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("acskam macska", main_0(), str)
    ASSERT_STREQ(str, "kulonbozo\n");
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("macska macske", main_0(), str)
    ASSERT_STREQ(str, "kulonbozo\n");
}

TEST(Test, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("eeeeef eeeeef", main_0(), str)
    ASSERT_STREQ(str, "egyforma\n");
}