#include <string>
#include <regex>

using namespace std;

int telefon_kereses(string kutak_tartalma[], int kutak_szama) {
    string keresett = "telefon";
    smatch m;
    for (int i = 0; i < kutak_szama; ++i) {
        if (regex_search(kutak_tartalma[i], m, regex("(^| )telefon($| )"))) {
            return i;
        }
    }
    return -1;
}