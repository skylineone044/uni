#include <gtest/gtest.h>

#define main main_0
#include "../src/kutatas.cpp"
#undef main

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s[] = { "telefon", "kuka", "toll" };
    int res = telefon_kereses(s, 3);
    ASSERT_EQ(res, 0);
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s[] = { "telefon kamion", "kuka", "toll" };
    int res = telefon_kereses(s, 3);
    ASSERT_EQ(res, 0);
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s[] = { "ceruza", "alma telefon cseresznye", "toll" };
    int res = telefon_kereses(s, 3);
    ASSERT_EQ(res, 1);
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s[] = { "ceruza", "alma telefon", "toll" };
    int res = telefon_kereses(s, 3);
    ASSERT_EQ(res, 1);
}

TEST(Test, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s[] = { "ceruza", "raketa", "toll", "telefon" };
    int res = telefon_kereses(s, 4);
    ASSERT_EQ(res, 3);
}

TEST(Test, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s[] = { "szamologep", "raketa", "ceruza ceruza telefon matrica ragaszto", "aram" };
    int res = telefon_kereses(s, 4);
    ASSERT_EQ(res, 2);
}

TEST(Test, 07) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s[] = { "szamologep", "raketa", "ceruza ceruza sajt matrica ragaszto", "aram" };
    int res = telefon_kereses(s, 4);
    ASSERT_EQ(res, -1);
}

TEST(Test, 08) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s[] = { "telefontok", "raketa", "ceruza ceruza sajt matrica ragaszto", "aram" };
    int res = telefon_kereses(s, 4);
    ASSERT_EQ(res, -1);
}

TEST(Test, 09) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s[] = { "jatektelefon", "raketa", "ceruza ceruza sajt matrica ragaszto", "aram" };
    int res = telefon_kereses(s, 4);
    ASSERT_EQ(res, -1);
}

TEST(Test, 10) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s[] = { "szamologep telefontok", "raketa", "ceruza ceruza sajt matrica ragaszto", "aram" };
    int res = telefon_kereses(s, 4);
    ASSERT_EQ(res, -1);
}

TEST(Test, 11) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s[] = { "szamologep jatektelefon", "raketa", "ceruza ceruza sajt matrica ragaszto", "aram" };
    int res = telefon_kereses(s, 4);
    ASSERT_EQ(res, -1);
}

TEST(Test, 12) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s[] = { "telefontok jatek", "raketa", "ceruza ceruza sajt matrica ragaszto", "aram" };
    int res = telefon_kereses(s, 4);
    ASSERT_EQ(res, -1);
}

TEST(Test, 13) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s[] = { "jatektelefon matrica", "raketa", "ceruza ceruza sajt matrica ragaszto", "aram" };
    int res = telefon_kereses(s, 4);
    ASSERT_EQ(res, -1);
}

TEST(Test, 14) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s[] = { "okostelefon telefontok telefonkartya", "raketa", "ceruza ceruza sajt matrica ragaszto", "aram" };
    int res = telefon_kereses(s, 4);
    ASSERT_EQ(res, -1);
}

TEST(Test, 15) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s[] = { "telefonkagylo", "raketa", "ceruza ceruza telefonmatrica matrica ragaszto", "kamutelefon" };
    int res = telefon_kereses(s, 4);
    ASSERT_EQ(res, -1);
}

TEST(Test, 16) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s[] = { "telefontok", "raketa", "ceruza ceruza sajt matrica ragaszto", "aram", "kotel telefon harcosmaszk" };
    int res = telefon_kereses(s, 5);
    ASSERT_EQ(res, 4);
}