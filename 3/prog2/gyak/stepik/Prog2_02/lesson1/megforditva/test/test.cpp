#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/megforditva.cpp"
#undef main

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string eredeti = "macska";
    string megforditott = "akscam";

    string eredeti_masolat(eredeti);
    string modositott = -eredeti;
    ASSERT_EQ(modositott, megforditott);
    ASSERT_EQ(eredeti, eredeti_masolat);
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string eredeti = "aaaa";
    string megforditott = "aaaa";

    string eredeti_masolat(eredeti);
    string modositott = -eredeti;
    ASSERT_EQ(modositott, megforditott);
    ASSERT_EQ(eredeti, eredeti_masolat);
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string eredeti = "abcd";
    string megforditott = "dcba";

    string eredeti_masolat(eredeti);
    string modositott = -eredeti;
    ASSERT_EQ(modositott, megforditott);
    ASSERT_EQ(eredeti, eredeti_masolat);
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string eredeti = "abcde";
    string megforditott = "edcba";

    string eredeti_masolat(eredeti);
    string modositott = -eredeti;
    ASSERT_EQ(modositott, megforditott);
    ASSERT_EQ(eredeti, eredeti_masolat);
}

TEST(Test, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string eredeti = "a sas megette a kukacot!";
    string megforditott = "!tocakuk a ettegem sas a";

    string eredeti_masolat(eredeti);
    string modositott = -eredeti;
    ASSERT_EQ(modositott, megforditott);
    ASSERT_EQ(eredeti, eredeti_masolat);
}