#include <string>

struct Monitor {
    std::string marka;
    std::string tipus;
    double atmero;
};

bool operator==(Monitor& m1, Monitor& m2) {
    if (m1.marka == m2.marka &&
        m1.tipus == m2.tipus &&
        m1.atmero == m2.atmero) {
        return true;
    } else {
        return false;
    }
}