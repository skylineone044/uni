#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/monitor.cpp"
#undef main

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Monitor m1 {"LG", "111111", 32.2};
    Monitor m2 {"LG", "111111", 32.3};
    ASSERT_FALSE(m1 == m2);
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Monitor m1 {"LG", "111111", 32.2};
    Monitor m2 {"LG", "111111", 32.2};
    ASSERT_TRUE(m1 == m2);
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Monitor m1 {"LG", "111111", 32.2};
    Monitor m2 {"LG", "111121", 32.2};
    ASSERT_FALSE(m1 == m2);
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Monitor m1 {"LG", "112111", 32.2};
    Monitor m2 {"LG", "111111", 32.2};
    ASSERT_FALSE(m1 == m2);
}

TEST(Test, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Monitor m1 {"LGG", "111111", 85};
    Monitor m2 {"LG", "111111", 85};
    ASSERT_FALSE(m1 == m2);
}

TEST(Test, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Monitor m1 {"LG", "111111", 32.2};
    Monitor m2 {"MMM", "111111", 32.2};
    ASSERT_FALSE(m1 == m2);
}

TEST(Test, 07) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Monitor m1 {"LG", "1111112", 32.2};
    Monitor m2 {"MMM", "111111", 32.2};
    ASSERT_FALSE(m1 == m2);
}

TEST(Test, 08) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Monitor m1 {"LG", "111111", 32.2};
    Monitor m2 {"MMM", "ASMAS", 32.3};
    ASSERT_FALSE(m1 == m2);
}