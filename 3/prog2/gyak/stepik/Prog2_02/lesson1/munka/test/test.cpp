#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/munka.cpp"
#undef main

#include "../../tools.cpp"

Ember e1 = {"Jani", 29};
Ember e2 = {"Otto", 37};
Ember e3 = {"Gabor", 22};
Ember e4 = {"kft", 20};
Ember e5 = {"Fanni", 23};
Ember e6 = {"Eszter", 18};
Ember e7 = {"Monika", 33};

TEST(Eredmeny, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Ember ee1[] = {e1, e2};
    Munkahely m1 = {ee1, 2};

    Ember ee2[] = {e3, e4, e5, e7};
    Munkahely m2 = {ee2, 4};

    ASSERT_TRUE(m2 < m1);
    ASSERT_FALSE(m1 < m2);
    ASSERT_FALSE(m2 > m1);
    ASSERT_TRUE(m1 > m2);
}

TEST(Eredmeny, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Ember ee1[] = {e1};
    Munkahely m1 = {ee1, 1};

    Ember ee2[] = {e7, e2, e5, e4};
    Munkahely m2 = {ee2, 4};

    ASSERT_TRUE(m2 < m1);
    ASSERT_FALSE(m1 < m2);
    ASSERT_FALSE(m2 > m1);
    ASSERT_TRUE(m1 > m2);
}

TEST(Eredmeny, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Ember ee1[] = {e6, e3};
    Munkahely m1 = {ee1, 2};

    Ember ee2[] = {e4};
    Munkahely m2 = {ee2, 1};

    ASSERT_FALSE(m2 < m1);
    ASSERT_FALSE(m1 < m2);
    ASSERT_FALSE(m2 > m1);
    ASSERT_FALSE(m1 > m2);
}

TEST(Eredmeny, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Ember ee1[] = {e2};
    Munkahely m1 = {ee1, 1};

    Ember ee2[] = {e1, e3, e4, e5, e6, e7};
    Munkahely m2 = {ee2, 6};

    ASSERT_TRUE(m2 < m1);
    ASSERT_FALSE(m1 < m2);
    ASSERT_FALSE(m2 > m1);
    ASSERT_TRUE(m1 > m2);
}

TEST(Fajlmeret, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    checkFileSize("munka", "munka.cpp", 300);
}