#include <string>

using namespace std;

string operator+(string s, int shift) {
    string res = s;
    for (int i = 0; i < res.length(); ++i) {
        res[i] += shift;
    }
    return res;
}