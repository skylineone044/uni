#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/noveles.cpp"
#undef main

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string eredeti = "macska";
    int ertek = 2;
    string elvart = "oceumc";

    string eredeti_masolat(eredeti);
    string res = eredeti + ertek;
    ASSERT_EQ(eredeti, eredeti_masolat);
    ASSERT_EQ(res, elvart);
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string eredeti = "aaaaa";
    int ertek = 5;
    string elvart = "fffff";

    string eredeti_masolat(eredeti);
    string res = eredeti + ertek;
    ASSERT_EQ(eredeti, eredeti_masolat);
    ASSERT_EQ(res, elvart);
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string eredeti = "0019";
    int ertek = -2;
    string elvart = "../7";

    string eredeti_masolat(eredeti);
    string res = eredeti + ertek;
    ASSERT_EQ(eredeti, eredeti_masolat);
    ASSERT_EQ(res, elvart);
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string eredeti = "zzaaZ";
    int ertek = 3;
    string elvart = "}}dd]";

    string eredeti_masolat(eredeti);
    string res = eredeti + ertek;
    ASSERT_EQ(eredeti, eredeti_masolat);
    ASSERT_EQ(res, elvart);
}

TEST(Test, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string eredeti = "a b c 0 1 2";
    int ertek = 13;
    string elvart = "n-o-p-=->-?";

    string eredeti_masolat(eredeti);
    string res = eredeti + ertek;
    ASSERT_EQ(eredeti, eredeti_masolat);
    ASSERT_EQ(res, elvart);
}