#include <gtest/gtest.h>

#define main main_0
#include "../src/osszefuz.cpp"
#undef main

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s[] = {"alma", "fa"};
    string res = osszefuzes(s, 2);
    ASSERT_EQ(res, "almafa");
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s[] = {"alma", "fa", "karfiol", "lo"};
    string res = osszefuzes(s, 4);
    ASSERT_EQ(res, "almafakarfiollo");
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s[] = {"tehen", "tehen"};
    string res = osszefuzes(s, 2);
    ASSERT_EQ(res, "tehentehen");
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s[] = {"cseresznye"};
    string res = osszefuzes(s, 1);
    ASSERT_EQ(res, "cseresznye");
}

TEST(Test, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s[] = {"k", "", "a"};
    string res = osszefuzes(s, 3);
    ASSERT_EQ(res, "ka");
}