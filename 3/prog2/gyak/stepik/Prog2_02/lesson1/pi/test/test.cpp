#include <gtest/gtest.h>

#define main main_0
#include "../src/pi.cpp"
#undef main

#include "../../tools.cpp"

TEST(Kiiratas, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", main_0(), str)
    ASSERT_STREQ(str, "3.14159265358979323846\n");
}

TEST(VisszateresiErtek, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", int res = main_0(), str);
    ASSERT_EQ(res, 0);
}