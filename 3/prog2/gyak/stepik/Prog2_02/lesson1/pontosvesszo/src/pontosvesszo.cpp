#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

string tolvajlas(const string& eredeti) {
    string newString = "";
    for (int i = 0; i < eredeti.length(); ++i) {
        if (eredeti[i] != ';') {
            newString += eredeti[i];
        }
    }
    return newString;
}