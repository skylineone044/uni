#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/pontosvesszo.cpp"
#undef main

string tolvajlas(const string& eredeti);

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(tolvajlas("cica"), "cica");
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(tolvajlas(";tehen"), "tehen");
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(tolvajlas("lovacska;"), "lovacska");
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(tolvajlas(";;;hullik a ho;;"), "hullik a ho");
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(tolvajlas("egy;ketto;harom;negy"), "egykettoharomnegy");
}

TEST(Teszt, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(tolvajlas(";;;; ; ;;ez;lesz; az utolso;;;teszteset;;-;15978@;a.hu"), "  ezlesz az utolsoteszteset-15978@a.hu");
}