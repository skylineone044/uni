#include <string>

using namespace std;

int* gyartas(int start, int end) {
    int len = end - start + 1;
    int* tomb = new int[len];
    int j = 0;
    for (int i = start; i <= end; ++i, ++j) {
        tomb[j] = i;
    }
    return tomb;
}