#include <gtest/gtest.h>

#define main main_0
#include "../src/szamok.cpp"
#undef main

#include "../../tools.cpp"

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int start = 5;
    int end = 7;
    int elvart[] = {5, 6, 7};
    int* t = gyartas(start, end);

    ASSERT_ARRAY_EQ(t, elvart, end - start + 1);
    delete[] t;
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int start = -1;
    int end = 3;
    int elvart[] = {-1, 0, 1, 2, 3};
    int* t = gyartas(start, end);

    ASSERT_ARRAY_EQ(t, elvart, end - start + 1);
    delete[] t;
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int start = -4;
    int end = 2;
    int elvart[] = {-4, -3, -2, -1, 0, 1, 2};
    int* t = gyartas(start, end);

    ASSERT_ARRAY_EQ(t, elvart, end - start + 1);
    delete[] t;
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int start = 10;
    int end = 13;
    int elvart[] = {10, 11, 12, 13};
    int* t = gyartas(start, end);

    ASSERT_ARRAY_EQ(t, elvart, end - start + 1);
    delete[] t;
}

TEST(Test, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int start = 3;
    int end = 3;
    int elvart[] = {3};
    int* t = gyartas(start, end);

    ASSERT_ARRAY_EQ(t, elvart, end - start + 1);
    delete[] t;
}