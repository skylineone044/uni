#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/szorzas.cpp"
#undef main

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string str = "macska";
    string def = "macska";
    int db = 3;
    string resOk = "macskamacskamacska";

    string res = db * str;
    ASSERT_EQ(str, def);
    ASSERT_EQ(res, resOk);
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string str = "tehen";
    string def = "tehen";
    int db = 1;
    string resOk = "tehen";

    string res = db * str;
    ASSERT_EQ(str, def);
    ASSERT_EQ(res, resOk);
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string str = "cica";
    string def = "cica";
    int db = 77;
    string resOk = "cicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacicacica";

    string res = db * str;
    ASSERT_EQ(str, def);
    ASSERT_EQ(res, resOk);
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string str = "macska";
    string def = "macska";
    int db = 0;
    string resOk;

    string res = db * str;
    ASSERT_EQ(str, def);
    ASSERT_EQ(res, resOk);
}

TEST(Test, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string str = "a";
    string def = "a";
    int db = 77;
    string resOk = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

    string res = db * str;
    ASSERT_EQ(str, def);
    ASSERT_EQ(res, resOk);
}