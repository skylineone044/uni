#include <gtest/gtest.h>

#define main main_0
#include "../src/tajekoztatas.cpp"
#undef main

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string res = eredmeny(5);
    ASSERT_EQ(res, "Az eredmenyed: 5");
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string res = eredmeny(54);
    ASSERT_EQ(res, "Az eredmenyed: 54");
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string res = eredmeny(104);
    ASSERT_EQ(res, "Az eredmenyed: 104");
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string res = eredmeny(0);
    ASSERT_EQ(res, "Az eredmenyed: 0");
}

TEST(Test, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string res = eredmeny(-4);
    ASSERT_EQ(res, "Az eredmenyed: -4");
}

TEST(Test, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string res = eredmeny(5.8);
    ASSERT_EQ(res, "Az eredmenyed: 5");
}