#include <cfloat>
#include <cmath>
#include <iostream>
#include <climits>

using namespace std;

struct Point {
    float x;
    float y;
};

double dist(Point a, Point b) {
    double x_dist = std::abs(a.x - b.x);
    double y_dist = std::abs(a.y - b.y);

    return std::sqrt(x_dist * x_dist + y_dist * y_dist);
}

Point* operator>>(Point p, Point* points[]) {
    double minDistance = INT_MAX;
    Point* closestPoint = nullptr;

    for (int i = 0; points[i] != nullptr; i++) {
        if (dist(p, *points[i]) < minDistance) {
            minDistance = dist(p, *points[i]);
            closestPoint = points[i];
        }
    }
    return closestPoint;
}