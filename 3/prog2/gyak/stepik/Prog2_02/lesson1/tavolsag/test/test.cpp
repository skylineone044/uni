#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/tavolsag.cpp"
#undef main

#include "../../tools.cpp"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Point p1 = {3, 5};
    Point p2 = {7, 0};
    Point p3 = {6, 4};
    Point p4 = {5, 5};
    Point p5 = {5, 5};
    Point p6 = {1, 0};
    Point p7 = {0, 7};

    Point* pp1[] = {&p1, &p2, &p3, &p4, &p5, &p6, nullptr};
    Point* res = p7 >> pp1;
    ASSERT_EQ(res, &p1);

    Point* pp2[] = {&p2, &p4, &p7, nullptr};
    res = p1 >> pp2;
    ASSERT_EQ(res, &p4);

    res = p3 >> pp2;
    ASSERT_EQ(res, &p4);

    res = p5 >> pp2;
    ASSERT_EQ(res, &p4);

    res = p6 >> pp2;
    ASSERT_EQ(res, &p2);

    Point* pp3[] = {&p2, &p7, &p6, nullptr};

    res = p1 >> pp3;
    ASSERT_EQ(res, &p7);

    res = p3 >> pp3;
    ASSERT_EQ(res, &p2);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Point p1 = {-5, 2};
    Point p2 = {3, 1};
    Point p3 = {-6, -4};
    Point p4 = {5, -2};
    Point p5 = {-2, -2};

    Point* pp1[] = {&p1, &p2, &p3, nullptr};
    Point* res = p4 >> pp1;
    ASSERT_EQ(res, &p2);

    Point* pp2[] = {&p2, &p4, nullptr};

    res = p1 >> pp2;
    ASSERT_EQ(res, &p2);

    res = p3 >> pp2;
    ASSERT_EQ(res, &p2);

    Point* pp3[] = {&p1, &p2, &p3, &p4, nullptr};
    res = p5 >> pp3;
    ASSERT_EQ(res, &p3);

    res = p1 >> pp3;
    ASSERT_EQ(res, &p1);
}
