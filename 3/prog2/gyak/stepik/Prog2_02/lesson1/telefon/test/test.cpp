#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/telefon.cpp"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Telefon t;
    t.tarhely = 50;

    ASSERT_EQ(t.foglalt, 0);

    ASSERT_TRUE(telepit(t, 20));
    ASSERT_EQ(t.foglalt, 20);

    ASSERT_TRUE(telepit(t, 15));
    ASSERT_EQ(t.foglalt, 35);

    ASSERT_FALSE(telepit(t, 20));
    ASSERT_EQ(t.foglalt, 35);

    ASSERT_TRUE(telepit(t, 13));
    ASSERT_EQ(t.foglalt, 48);

    Telefon t2;
    t2.tarhely = 10;
    ASSERT_FALSE(telepit(t, 5));
    ASSERT_EQ(t.foglalt, 48);
    ASSERT_TRUE(telepit(t2, 5));
    ASSERT_EQ(t2.foglalt, 5);
    ASSERT_TRUE(telepit(t2, 5));
    ASSERT_EQ(t2.foglalt, 10);
}