#include <string>

using namespace std;

struct Eredmeny {
    string nev;
    int pontszam;
};

float operator%(Eredmeny e1, int maxPontszam) {
    return ((float)e1.pontszam / (float)maxPontszam) * 100;
}