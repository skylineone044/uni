#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/teljesitmeny.cpp"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Eredmeny e1 = {"asd", 20};
    Eredmeny e2 = {"smndn", 44};
    Eredmeny e3 = {"moabnfd", 100};
    Eredmeny e4 = {"anjnva", -2};

    ASSERT_NEAR(e1 % 20, 100, 0.0001);
    ASSERT_NEAR(e1 % 40, 50, 0.0001);
    ASSERT_NEAR(e1 % 60, 33.333333, 0.0001);
    ASSERT_NEAR(e1 % 22, 90.909090, 0.0001);

    ASSERT_NEAR(e2 % 50, 88, 0.0001);
    ASSERT_NEAR(e2 % 96, 45.833333, 0.0001);

    ASSERT_NEAR(e3 % 100, 100, 0.0001);
    ASSERT_NEAR(e3 % 1000, 10, 0.0001);
    ASSERT_NEAR(e3 % 101, 99.0099009, 0.0001);
    ASSERT_NEAR(e3 % 90, 111.1111111, 0.0001);

    ASSERT_NEAR(e4 % 10, -20, 0.0001);
    ASSERT_NEAR(e4 % 3, -66.66666, 0.0001);
    ASSERT_NEAR(e4 % 100, -2, 0.0001);
}