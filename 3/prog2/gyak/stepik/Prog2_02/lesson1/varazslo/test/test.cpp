#include <gtest/gtest.h>

#define main main_0
#include "../src/varazslo.cpp"
#undef main

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string str = "macska";
    int tomb[] = {5, 2};
    int db = 2;
    string result = "ac";
    string s = valogat(str, tomb, db);
    ASSERT_EQ(s, result);
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string str = "kecske";
    int tomb[] = {1, 2, 1, 2, 2, 0};
    int db = 6;
    string result = "ececck";
    string s = valogat(str, tomb, db);
    ASSERT_EQ(s, result);
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string str = "E";
    int tomb[] = {0, 0, 0, 0};
    int db = 4;
    string result = "EEEE";
    string s = valogat(str, tomb, db);
    ASSERT_EQ(s, result);
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string str = "A macska felmaszott a fara";
    int tomb[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 2, 3, 4, 5, 6, 7};
    int db = 15;
    string result = "A macska macska";
    string s = valogat(str, tomb, db);
    ASSERT_EQ(s, result);
}

TEST(Test, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string str = "ami";
    int tomb[] = {1, 0, 2, 1, 0, 1, 0, 1, 0};
    int db = 9;
    string result = "maimamama";
    string s = valogat(str, tomb, db);
    ASSERT_EQ(s, result);
}

TEST(Test, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string str = "macska";
    int tomb[] = {0, 2, 0, 1, 0, 0};
    int db = 6;
    string result = "mcmamm";
    string s = valogat(str, tomb, db);
    ASSERT_EQ(s, result);
}