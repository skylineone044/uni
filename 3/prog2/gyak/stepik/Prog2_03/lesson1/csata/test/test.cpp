#include <gtest/gtest.h>

#define main main_0
#include "../src/csata.cpp"
#undef main

bool operator==(const Harcos& h1, const Harcos& h2) {
    bool ok = h1.get_eletero() == h2.get_eletero();
    ok = ok && h1.get_sebzes() == h2.get_sebzes();
    return ok;
}

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Harcos h1(100, 10);
    Harcos h2(50, 22);
    Harcos* elvartNyertes = &h2;

    Harcos vh1(-10, 10);
    Harcos vh2(0, 22);

    Harcos* nyertes = gyoztes(&h1, &h2);
    ASSERT_EQ(nyertes, elvartNyertes);
    ASSERT_EQ(h1, vh1);
    ASSERT_EQ(h2, vh2);
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Harcos h1(120, 1);
    Harcos h2(10, 20);
    Harcos* elvartNyertes = &h2;

    Harcos vh1(0, 1);
    Harcos vh2(4, 20);

    Harcos* nyertes = gyoztes(&h1, &h2);
    ASSERT_EQ(nyertes, elvartNyertes);
    ASSERT_EQ(h1, vh1);
    ASSERT_EQ(h2, vh2);
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Harcos h1(22, 5);
    Harcos h2(16, 5);
    Harcos* elvartNyertes = &h1;

    Harcos vh1(2, 5);
    Harcos vh2(-4, 5);

    Harcos* nyertes = gyoztes(&h1, &h2);
    ASSERT_EQ(nyertes, elvartNyertes);
    ASSERT_EQ(h1, vh1);
    ASSERT_EQ(h2, vh2);
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Harcos h1(100, 10);
    Harcos h2(100, 10);
    Harcos* elvartNyertes = nullptr;

    Harcos vh1(0, 10);
    Harcos vh2(0, 10);

    Harcos* nyertes = gyoztes(&h1, &h2);
    ASSERT_EQ(nyertes, elvartNyertes);
    ASSERT_EQ(h1, vh1);
    ASSERT_EQ(h2, vh2);
}

TEST(Test, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Harcos h1(50, 16);
    Harcos h2(200, 4);
    Harcos* elvartNyertes = &h1;

    Harcos vh1(-2, 16);
    Harcos vh2(-8, 4);

    Harcos* nyertes = gyoztes(&h1, &h2);
    ASSERT_EQ(nyertes, elvartNyertes);
    ASSERT_EQ(h1, vh1);
    ASSERT_EQ(h2, vh2);
}

TEST(Test, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Harcos h1(10, 2);
    Harcos h2(20, 1);
    Harcos* elvartNyertes = nullptr;

    Harcos vh1(0, 2);
    Harcos vh2(0, 1);

    Harcos* nyertes = gyoztes(&h1, &h2);
    ASSERT_EQ(nyertes, elvartNyertes);
    ASSERT_EQ(h1, vh1);
    ASSERT_EQ(h2, vh2);
}

TEST(Test, 07) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Harcos h1(50000000, 0);
    Harcos h2(1, 1);
    Harcos* elvartNyertes = &h2;

    Harcos vh1(0, 0);
    Harcos vh2(1, 1);

    Harcos* nyertes = gyoztes(&h1, &h2);
    ASSERT_EQ(nyertes, elvartNyertes);
    ASSERT_EQ(h1, vh1);
    ASSERT_EQ(h2, vh2);
}