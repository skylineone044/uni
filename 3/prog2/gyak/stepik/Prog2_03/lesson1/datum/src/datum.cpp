class Datum {
private:
    int ev;
    int honap;
    int nap;

public:
    int get_ev() const {
        return ev;
    }

    int get_honap() const {
        return honap;
    }

    int get_nap() const {
        return nap;
    }

};