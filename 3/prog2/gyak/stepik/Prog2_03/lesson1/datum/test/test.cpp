#include <gtest/gtest.h>

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    #define private public
    #include "../src/datum.cpp"
    #undef private

    Datum d;
    d.ev = 2000;
    d.honap = 10;
    d.nap = 11;

    ASSERT_EQ(d.get_ev(), 2000);
    ASSERT_EQ(d.get_honap(), 10);
    ASSERT_EQ(d.get_nap(), 11);

    d.ev = 2001;
    d.honap = 12;
    d.nap = 31;

    ASSERT_EQ(d.get_ev(), 2001);
    ASSERT_EQ(d.get_honap(), 12);
    ASSERT_EQ(d.get_nap(), 31);
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    #include "../src/datum.cpp"
    Datum d;
    d.get_ev();
    d.get_honap();
    d.get_nap();
}