#include <gtest/gtest.h>
#include <climits>

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    #include "../src/ember.cpp"

    Ember e;
    e.kor = 10;
    e.nev = "jozsi";
    e.nem = 30;

    ASSERT_EQ(e.kor, 10);
    ASSERT_EQ(e.nev, "jozsi");
    ASSERT_EQ(e.nem, 30);

    Ember e2;
    e2.kor = -1;
    e2.nev = "marika";
    e2.nem = 29;

    ASSERT_EQ(e2.kor, UINT_MAX);
    ASSERT_GE(e2.kor, 0);
    ASSERT_EQ(e2.nev, "marika");
    ASSERT_EQ(e2.nem, 29);
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    #define struct class
    #define public private
    #include "../src/ember.cpp"
    #undef public
    #undef struct

    class E2: public Ember {
    public:
        E2(int vagyon, string lakcim) {
            this->vagyon = vagyon;
            this->lakcim = std::move(lakcim);
        }

        int get_vagyon() {
            return this->vagyon;
        }

        string get_lakcim() {
            return this->lakcim;
        }

    };

    E2 e(5, "otthon");
    ASSERT_EQ(e.get_vagyon(), 5);
    ASSERT_EQ(e.get_lakcim(), "otthon");

    E2 e2(-3, "hazban ott valahol");
    ASSERT_EQ(e2.get_vagyon(), -3);
    ASSERT_EQ(e2.get_lakcim(), "hazban ott valahol");
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    #define private public
    #include "../src/ember.cpp"
    #undef private

    Ember e;
    e.bankkartya_szam = "1120 1120";
    e.betegsegek[77] = "asd";
    e.betegsegek[33] = "vvvvavava";

    ASSERT_EQ(e.bankkartya_szam, "1120 1120");
    ASSERT_EQ(e.betegsegek[33], "vvvvavava");
    ASSERT_EQ(e.betegsegek[77], "asd");

    Ember e2;
    e2.bankkartya_szam = "22221120";
    e2.betegsegek[33] = "vvv";
    e2.betegsegek[99] = "yyy";

    ASSERT_EQ(e2.bankkartya_szam, "22221120");
    ASSERT_EQ(e2.betegsegek[33], "vvv");
    ASSERT_EQ(e2.betegsegek[99], "yyy");
}