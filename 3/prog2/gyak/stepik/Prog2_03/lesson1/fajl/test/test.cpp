#include <gtest/gtest.h>
#include <string>

using namespace std;

#define class struct
#define private public
#define main main_0
#include "../src/fajl.cpp"
#undef main
#undef private
#undef class

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Fajl f1("asd", "mov", "egy cica ket cica");
    ASSERT_EQ(f1.get_nev(), "asd");
    ASSERT_EQ(f1.get_kiterjesztes(), "mov");
    ASSERT_EQ(f1.get_szoveg(), "egy cica ket cica");
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Fajl f2;
    ASSERT_EQ(f2.get_nev(), "file");
    ASSERT_EQ(f2.get_kiterjesztes(), "txt");
    ASSERT_EQ(f2.get_szoveg(), "");
}