#include <string>
using namespace std;

class Forditas {
private:
    string angol;
    string magyar;

public:
    Forditas() {}

    Forditas(const string &angol, const string &magyar) : angol(angol), magyar(magyar) {}

    friend class Szotar;
};

class Szotar {
    int szavak_max;
    int szavak_szama;
    Forditas* szavak;

public:
    Szotar(int max_szavak) : szavak(new Forditas[max_szavak]), szavak_max(max_szavak) {
        szavak_szama = 0;
        for (int i = 0; i < max_szavak; ++i) {
            szavak[i] = Forditas();
        }
    }

    void frissit(string mit, string mire) {
        bool frissitve = false;
        for (int i = 0; i < szavak_max; ++i) {
            if (szavak[i].magyar == mit) {
                szavak[i].angol = mire;
                frissitve = true;
            }
        }
        if (!frissitve) {
            szavak[szavak_szama] = Forditas(mire, mit);
            szavak_szama++;
        }
    }

    string lekerdez(string magyar_szo) {
        for (int i = 0; i < szavak_max; ++i) {
            if (szavak[i].magyar == magyar_szo) {
                return szavak[i].angol;
            }
        }
        return "";
    }

    string tartalom() {
        string res = "";
        for (int i = 0; i < szavak_max; ++i) {
            if (szavak[i].angol != "" && szavak[i].magyar != "") {
                res += szavak[i].magyar + " - " + szavak[i].angol + "\n";
            }
        }
        return res;
    }

    bool hozzaad(string magyar, string angol) {
        if (szavak_szama >= szavak_max) {
            if (lekerdez(magyar) != "") {
                frissit(magyar, angol);
                return true;
            }
            return false;
        } else {
            frissit(magyar, angol);
            return true;
        }
    }

    void kiurit() {
        szavak = new Forditas[szavak_max];
        szavak_szama = 0;
    }

};