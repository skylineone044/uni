#include <gtest/gtest.h>
#include <string>

using namespace std;

#define class struct
#define private public
#define main main_0
#include "../src/forditas.cpp"
#undef main
#undef private
#undef class

string s1_magyar = "macska";
string s1_angol = "cat";

string s2_magyar = "kutya";
string s2_angol = "dog";

string s3_magyar = "kod";
string s3_angol = "fog";

string s4_magyar = "bank";
string s4_angol = "bank";

string s5_magyar = "telefon";
string s5_angol = "phone";

TEST(Forditas, default_konstruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Forditas f;
}

TEST(Forditas, parameteres_konstruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string a = "aaa";
    string b = "bbb";

    Forditas f(a, b);
    ASSERT_EQ(f.angol, "aaa");
    ASSERT_EQ(f.magyar, "bbb");

    string c = "erwet";
    string d = "evvv";

    Forditas f2(c, d);
    ASSERT_EQ(f2.angol, "erwet");
    ASSERT_EQ(f2.magyar, "evvv");
}

TEST(Szotar, konstruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Szotar s1(5);
}

TEST(Szotar, hozzaad) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Szotar s1(4);
    bool siker;

    siker = s1.hozzaad(s1_magyar, s1_angol);
    ASSERT_TRUE(siker);
    ASSERT_EQ(s1.szavak[0].magyar, "macska");
    ASSERT_EQ(s1.szavak[0].angol, "cat");

    siker = s1.hozzaad(s2_magyar, s2_angol);
    ASSERT_TRUE(siker);
    ASSERT_EQ(s1.szavak[0].magyar, "macska");
    ASSERT_EQ(s1.szavak[0].angol, "cat");
    ASSERT_EQ(s1.szavak[1].magyar, "kutya");
    ASSERT_EQ(s1.szavak[1].angol, "dog");

    siker = s1.hozzaad(s4_magyar, s4_angol);
    ASSERT_TRUE(siker);
    ASSERT_EQ(s1.szavak[0].magyar, "macska");
    ASSERT_EQ(s1.szavak[0].angol, "cat");
    ASSERT_EQ(s1.szavak[1].magyar, "kutya");
    ASSERT_EQ(s1.szavak[1].angol, "dog");
    ASSERT_EQ(s1.szavak[2].magyar, "bank");
    ASSERT_EQ(s1.szavak[2].angol, "bank");

    siker = s1.hozzaad(s1_magyar, s2_angol);
    ASSERT_TRUE(siker);
    ASSERT_EQ(s1.szavak[0].magyar, "macska");
    ASSERT_EQ(s1.szavak[0].angol, "dog");
    ASSERT_EQ(s1.szavak[1].magyar, "kutya");
    ASSERT_EQ(s1.szavak[1].angol, "dog");
    ASSERT_EQ(s1.szavak[2].magyar, "bank");
    ASSERT_EQ(s1.szavak[2].angol, "bank");

    ASSERT_NE(s1.szavak[3].magyar, "macska");
    ASSERT_NE(s1.szavak[3].angol, "dog");

    siker = s1.hozzaad(s4_magyar, s1_angol);
    ASSERT_TRUE(siker);
    ASSERT_EQ(s1.szavak[0].magyar, "macska");
    ASSERT_EQ(s1.szavak[0].angol, "dog");
    ASSERT_EQ(s1.szavak[1].magyar, "kutya");
    ASSERT_EQ(s1.szavak[1].angol, "dog");
    ASSERT_EQ(s1.szavak[2].magyar, "bank");
    ASSERT_EQ(s1.szavak[2].angol, "cat");

    ASSERT_NE(s1.szavak[3].magyar, "bank");
    ASSERT_NE(s1.szavak[3].angol, "cat");

    siker = s1.hozzaad(s5_magyar, s5_angol);
    ASSERT_TRUE(siker);
    ASSERT_EQ(s1.szavak[0].magyar, "macska");
    ASSERT_EQ(s1.szavak[0].angol, "dog");
    ASSERT_EQ(s1.szavak[1].magyar, "kutya");
    ASSERT_EQ(s1.szavak[1].angol, "dog");
    ASSERT_EQ(s1.szavak[2].magyar, "bank");
    ASSERT_EQ(s1.szavak[2].angol, "cat");
    ASSERT_EQ(s1.szavak[3].magyar, "telefon");
    ASSERT_EQ(s1.szavak[3].angol, "phone");

    siker = s1.hozzaad(s4_magyar, s5_angol);
    ASSERT_TRUE(siker);
    ASSERT_EQ(s1.szavak[0].magyar, "macska");
    ASSERT_EQ(s1.szavak[0].angol, "dog");
    ASSERT_EQ(s1.szavak[1].magyar, "kutya");
    ASSERT_EQ(s1.szavak[1].angol, "dog");
    ASSERT_EQ(s1.szavak[2].magyar, "bank");
    ASSERT_EQ(s1.szavak[2].angol, "phone");
    ASSERT_EQ(s1.szavak[3].magyar, "telefon");
    ASSERT_EQ(s1.szavak[3].angol, "phone");

    siker = s1.hozzaad(s3_magyar, s3_angol);
    ASSERT_FALSE(siker);
    ASSERT_EQ(s1.szavak[0].magyar, "macska");
    ASSERT_EQ(s1.szavak[0].angol, "dog");
    ASSERT_EQ(s1.szavak[1].magyar, "kutya");
    ASSERT_EQ(s1.szavak[1].angol, "dog");
    ASSERT_EQ(s1.szavak[2].magyar, "bank");
    ASSERT_EQ(s1.szavak[2].angol, "phone");
    ASSERT_EQ(s1.szavak[3].magyar, "telefon");
    ASSERT_EQ(s1.szavak[3].angol, "phone");
}

TEST(Szotar, kiurit) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Szotar s1(4);
    s1.hozzaad(s1_magyar, s1_angol);
    s1.hozzaad(s2_magyar, s2_angol);

    ASSERT_NE(s1.szavak[0].magyar, "kod");
    ASSERT_NE(s1.szavak[0].angol, "fog");

    s1.kiurit();

    ASSERT_NE(s1.szavak[0].magyar, "kod");
    ASSERT_NE(s1.szavak[0].angol, "fog");

    bool siker = s1.hozzaad(s3_magyar, s3_angol);
    ASSERT_TRUE(siker);
    ASSERT_EQ(s1.szavak[0].magyar, "kod");
    ASSERT_EQ(s1.szavak[0].angol, "fog");
}

TEST(Szotar, lekerdez) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Szotar s1(3);
    s1.hozzaad(s1_magyar, s1_angol);
    s1.hozzaad(s2_magyar, s2_angol);
    s1.hozzaad(s3_magyar, s4_angol);

    string str = "cat";
    string s = s1.lekerdez(str);
    ASSERT_EQ(s, "");

    str = "macska";
    s = s1.lekerdez(str);
    ASSERT_EQ(s, "cat");

    str = "bank";
    s = s1.lekerdez(str);
    ASSERT_EQ(s, "");

    str = "kod";
    s = s1.lekerdez(str);
    ASSERT_EQ(s, "bank");
}

TEST(Szotar, frissit) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Szotar s1(4);
    bool siker;

    s1.hozzaad(s1_magyar, s1_angol);
    s1.hozzaad(s2_magyar, s2_angol);
    s1.hozzaad(s4_magyar, s4_angol);
    ASSERT_EQ(s1.szavak[0].magyar, "macska");
    ASSERT_EQ(s1.szavak[0].angol, "cat");
    ASSERT_EQ(s1.szavak[1].magyar, "kutya");
    ASSERT_EQ(s1.szavak[1].angol, "dog");
    ASSERT_EQ(s1.szavak[2].magyar, "bank");
    ASSERT_EQ(s1.szavak[2].angol, "bank");

    string mit = "macska";
    string mire = "catt";
    s1.frissit(mit, mire);
    ASSERT_EQ(s1.szavak[0].magyar, "macska");
    ASSERT_EQ(s1.szavak[0].angol, "catt");
    ASSERT_EQ(s1.szavak[1].magyar, "kutya");
    ASSERT_EQ(s1.szavak[1].angol, "dog");
    ASSERT_EQ(s1.szavak[2].magyar, "bank");
    ASSERT_EQ(s1.szavak[2].angol, "bank");

    mit = "bank";
    mire = "banko";
    s1.frissit(mit, mire);
    ASSERT_EQ(s1.szavak[0].magyar, "macska");
    ASSERT_EQ(s1.szavak[0].angol, "catt");
    ASSERT_EQ(s1.szavak[1].magyar, "kutya");
    ASSERT_EQ(s1.szavak[1].angol, "dog");
    ASSERT_EQ(s1.szavak[2].magyar, "bank");
    ASSERT_EQ(s1.szavak[2].angol, "banko");
    ASSERT_NE(s1.szavak[3].magyar, "door");
    ASSERT_NE(s1.szavak[3].angol, "ajto");

    string mit2 = "door";
    string mire2 = "ajto";
    s1.frissit(mit2, mire2);
    ASSERT_EQ(s1.szavak[0].magyar, "macska");
    ASSERT_EQ(s1.szavak[0].angol, "catt");
    ASSERT_EQ(s1.szavak[1].magyar, "kutya");
    ASSERT_EQ(s1.szavak[1].angol, "dog");
    ASSERT_EQ(s1.szavak[2].magyar, "bank");
    ASSERT_EQ(s1.szavak[2].angol, "banko");
    ASSERT_EQ(s1.szavak[3].magyar, "door");
    ASSERT_EQ(s1.szavak[3].angol, "ajto");
}

TEST(Szotar, tartalom) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Szotar s1(4);
    s1.hozzaad(s1_magyar, s1_angol);
    s1.hozzaad(s2_magyar, s2_angol);

    string res1 = s1.tartalom();
    ASSERT_EQ(res1, "macska - cat\nkutya - dog\n");

    s1.hozzaad(s3_magyar, s3_angol);
    string res2 = s1.tartalom();
    ASSERT_EQ(res2, "macska - cat\nkutya - dog\nkod - fog\n");

    s1.frissit(s1_magyar, s2_angol);
    string res3 = s1.tartalom();
    ASSERT_EQ(res3, "macska - dog\nkutya - dog\nkod - fog\n");
}
