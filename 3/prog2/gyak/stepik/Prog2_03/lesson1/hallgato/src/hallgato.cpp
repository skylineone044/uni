#include <string>

using namespace std;

class Hallgato {
    string nev;
    string neptun_kod;
    string kedvenc_targyak[3];

public:
    Hallgato(const string nev, const string neptunKod, string targy1="", string targy2="", string targy3="") {
        this->nev = nev;
        this->neptun_kod = neptunKod;
        this->kedvenc_targyak[0] = targy1;
        this->kedvenc_targyak[1] = targy2;
        this->kedvenc_targyak[2] = targy3;
    }

    const string& get_nev() const {
        return nev;
    }

    const string& get_neptun_kod() const {
        return neptun_kod;
    }

    const string* get_kedvenc_targyak() const {
        return kedvenc_targyak;
    }

};