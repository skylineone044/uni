#include <gtest/gtest.h>

#define main main_0
#include "../src/hallgato.cpp"
#undef main

string s1 = "aaa";
string s2 = "bbb";
string s3 = "a bbb nagyon jo";
string s4 = "bbb a legjobb";
string s5 = "ccc";

TEST(Test, 2_parameter) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Hallgato h(s1, s2);
    ASSERT_EQ(h.get_nev(), "aaa");
    ASSERT_EQ(h.get_neptun_kod(), "bbb");

    Hallgato h2(s2, s3);
    ASSERT_EQ(h2.get_nev(), "bbb");
    ASSERT_EQ(h2.get_neptun_kod(), "a bbb nagyon jo");
}

TEST(Test, 3_parameter) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Hallgato h(s1, s2, s3);
    ASSERT_EQ(h.get_nev(), "aaa");
    ASSERT_EQ(h.get_neptun_kod(), "bbb");
    ASSERT_EQ(h.get_kedvenc_targyak()[0], "a bbb nagyon jo");

    Hallgato h2(s2, s4, s5);
    ASSERT_EQ(h2.get_nev(), "bbb");
    ASSERT_EQ(h2.get_neptun_kod(), "bbb a legjobb");
    ASSERT_EQ(h2.get_kedvenc_targyak()[0], "ccc");
}

TEST(Test, 4_parameter) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Hallgato h(s5, s2, s3, s4);
    ASSERT_EQ(h.get_nev(), "ccc");
    ASSERT_EQ(h.get_neptun_kod(), "bbb");
    ASSERT_EQ(h.get_kedvenc_targyak()[0], "a bbb nagyon jo");
    ASSERT_EQ(h.get_kedvenc_targyak()[1], "bbb a legjobb");

    Hallgato h2(s2, s4, s1, s5);
    ASSERT_EQ(h2.get_nev(), "bbb");
    ASSERT_EQ(h2.get_neptun_kod(), "bbb a legjobb");
    ASSERT_EQ(h2.get_kedvenc_targyak()[0], "aaa");
    ASSERT_EQ(h2.get_kedvenc_targyak()[1], "ccc");
}

TEST(Test, 5_parameter) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Hallgato h(s3, s1, s5, s4, s2);
    ASSERT_EQ(h.get_nev(), "a bbb nagyon jo");
    ASSERT_EQ(h.get_neptun_kod(), "aaa");
    ASSERT_EQ(h.get_kedvenc_targyak()[0], "ccc");
    ASSERT_EQ(h.get_kedvenc_targyak()[1], "bbb a legjobb");
    ASSERT_EQ(h.get_kedvenc_targyak()[2], "bbb");

    Hallgato h2(s1, s5, s4, s2, s3);
    ASSERT_EQ(h2.get_nev(), "aaa");
    ASSERT_EQ(h2.get_neptun_kod(), "ccc");
    ASSERT_EQ(h2.get_kedvenc_targyak()[0], "bbb a legjobb");
    ASSERT_EQ(h2.get_kedvenc_targyak()[1], "bbb");
    ASSERT_EQ(h2.get_kedvenc_targyak()[2], "a bbb nagyon jo");
}