#include <gtest/gtest.h>
#include <climits>

#define private public
#define class struct
#define main main_0
#include "../src/haz.cpp"
#undef private
#undef class
#undef main


TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Haz h;
    h.meret = 10;
    h.szin = "piros";
    h.hazszam = 140;

    ASSERT_EQ(h.meret, 10);
    ASSERT_EQ(h.szin, "piros");
    ASSERT_EQ(h.hazszam, 140);
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Haz h;
    h.meret = 1;
    h.szin = "zold";
    h.hazszam = 11;

    ASSERT_EQ(h.meret, 1);
    ASSERT_EQ(h.szin, "zold");
    ASSERT_EQ(h.hazszam, 11);

    h.meret--;
    h.meret--;
    h.szin = "kek";
    h.hazszam++;

    ASSERT_EQ(h.meret, UINT_MAX);
    ASSERT_GE(h.meret, 0);
    ASSERT_EQ(h.szin, "kek");
    ASSERT_EQ(h.hazszam, 12);
}