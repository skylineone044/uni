#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/hazepites.cpp"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Haz h1("asd", "asd", 30);
    hazat_felepit(&h1);
    ASSERT_EQ(h1.get_felepitettseg(), 100);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Haz h1("vva", "dbdf", 11);
    for (int i = 0; i < 1; i++) h1.epit();
    hazat_felepit(&h1);
    ASSERT_EQ(h1.get_felepitettseg(), 100);
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Haz h1("wegweg wegvew", "dfav fdadh", 20);
    for (int i = 0; i < 45; i++) h1.epit();
    hazat_felepit(&h1);
    ASSERT_EQ(h1.get_felepitettseg(), 100);
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Haz h1("adfh df", "adfb adfbdf dfab dfab", 44);
    for (int i = 0; i < 100; i++) h1.epit();
    hazat_felepit(&h1);
    ASSERT_EQ(h1.get_felepitettseg(), 100);
}