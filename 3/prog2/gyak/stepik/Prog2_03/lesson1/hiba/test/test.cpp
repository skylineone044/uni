#include <gtest/gtest.h>

#define main main_0
#include "../src/hiba.cpp"
#undef main

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s = "aaaa";
    string s2 = "ba";

    Monitor m(s, 4.2, 5.2);
    ASSERT_EQ(m.get_gyarto(), "aaaa");
    ASSERT_NEAR(m.get_pixel_x(), 4.2, 0.0001);
    ASSERT_NEAR(m.get_pixel_y(), 5.2, 0.0001);

    Monitor m2(s2, 16, 14.2);
    ASSERT_EQ(m2.get_gyarto(), "ba");
    ASSERT_NEAR(m2.get_pixel_x(), 16, 0.0001);
    ASSERT_NEAR(m2.get_pixel_y(), 14.2, 0.0001);
}