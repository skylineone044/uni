#include <gtest/gtest.h>

#define main main_0
#include "../src/idopont.cpp"
#undef main

#include "../../tools.cpp"

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[1000];
    IO("", idopont(2000, 3, 5, 10, 22, 23), str)
    ASSERT_STREQ(str, "2000.3.5. 10:22:23");
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[1000];
    IO("", idopont(2001, 5, 4, 11, 20, 30), str)
    ASSERT_STREQ(str, "2001.5.4. 11:20:30");
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[1000];
    IO("", idopont(2000, 3, 5, 10, 22), str)
    ASSERT_STREQ(str, "2000.3.5. 10:22:0");
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[1000];
    IO("", idopont(2001, 5, 4, 11, 20), str)
    ASSERT_STREQ(str, "2001.5.4. 11:20:0");
}

TEST(Test, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[1000];
    IO("", idopont(2000, 3, 5, 10), str)
    ASSERT_STREQ(str, "2000.3.5. 10:0:0");
}

TEST(Test, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[1000];
    IO("", idopont(2001, 5, 4, 11), str)
    ASSERT_STREQ(str, "2001.5.4. 11:0:0");
}

TEST(Test, 07) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[1000];
    IO("", idopont(2020, 1, 4, 20), str)
    ASSERT_STREQ(str, "2020.1.4. 20:0:0");
}

TEST(Test, 08) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[1000];
    IO("", idopont(2011, 3, 7, 13), str)
    ASSERT_STREQ(str, "2011.3.7. 13:0:0");
}

TEST(Test, 09) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[1000];
    IO("", idopont(2020, 1, 4), str)
    ASSERT_STREQ(str, "2020.1.4. 0:0:0");
}

TEST(Test, 10) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[1000];
    IO("", idopont(2011, 3, 7), str)
    ASSERT_STREQ(str, "2011.3.7. 0:0:0");
}

TEST(Test, 11) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[1000];
    IO("", idopont(2020, 1), str)
    ASSERT_STREQ(str, "2020.1.1. 0:0:0");
}

TEST(Test, 12) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[1000];
    IO("", idopont(2011, 3), str)
    ASSERT_STREQ(str, "2011.3.1. 0:0:0");
}

TEST(Test, 13) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[1000];
    IO("", idopont(2021), str)
    ASSERT_STREQ(str, "2021.1.1. 0:0:0");
}

TEST(Test, 14) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[1000];
    IO("", idopont(1980), str)
    ASSERT_STREQ(str, "1980.1.1. 0:0:0");
}

TEST(Test, 15) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[1000];
    IO("", idopont(), str)
    ASSERT_STREQ(str, "1970.1.1. 0:0:0");
}

TEST(Test, 16) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[1000];
    IO("", idopont(), str)
    ASSERT_STREQ(str, "1970.1.1. 0:0:0");
}