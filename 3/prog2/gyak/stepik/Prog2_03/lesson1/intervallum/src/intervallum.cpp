class Intervallum {
    double also{};
    double felso{};

public:
    Intervallum() {}

    Intervallum(double also, double felso){
        if (also > felso) {
            double tmp = also;
            also = felso;
            felso = tmp;
        }

        this->also = also;
        this->felso = felso;
    }

    double get_also() {
        return also;
    }

    double get_felso() {
        return felso;
    }

    double hossz() {
        return felso - also;
    }


};

// intervallum_krealas fuggveny
Intervallum* intervallum_krealas(double tomb[][2], int meretY) {
    Intervallum* res = new Intervallum[meretY];

    for (int i = 0; i < meretY; ++i) {
        res[i] = Intervallum(tomb[i][0], tomb[i][1]);
    }
    return res;
}
