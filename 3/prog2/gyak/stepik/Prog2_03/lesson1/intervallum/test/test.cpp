#include <gtest/gtest.h>

#define main main_0
#include "../src/intervallum.cpp"
#undef main

void intervallum_egyforma(Intervallum i, const double t[]) {
    double kisebb = t[0] < t[1] ? t[0] : t[1];
    double nagyobb = t[0] < t[1] ? t[1] : t[0];

    ASSERT_EQ(kisebb, i.get_also());
    ASSERT_EQ(nagyobb, i.get_felso());
    ASSERT_EQ(nagyobb - kisebb, i.hossz());
}

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double szamok[][2] = {
            {1, 3}, {5, 4}, {6, 3}
    };
    int db = 3;

    Intervallum* intervallumok = intervallum_krealas(szamok, db);

    for (int i = 0; i < db; i++) {
        intervallum_egyforma(intervallumok[i], szamok[i]);
    }

    delete[] intervallumok;
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double szamok[][2] = {
            {1, 11}, {10, 5}, {5, 6}
    };
    int db = 3;

    Intervallum* intervallumok = intervallum_krealas(szamok, db);

    for (int i = 0; i < db; i++) {
        intervallum_egyforma(intervallumok[i], szamok[i]);
    }

    delete[] intervallumok;
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double szamok[][2] = {
            {4, 4}
    };
    int db = 1;

    Intervallum* intervallumok = intervallum_krealas(szamok, db);

    for (int i = 0; i < db; i++) {
        intervallum_egyforma(intervallumok[i], szamok[i]);
    }

    delete[] intervallumok;
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    double szamok[][2] = {
            {1.2, 3.2}, {4.4, 7.4}, {2.1, 5.2}, {-3.2, -1.1}, {-1.1, -3.2}, {5, 5}, {-2, -2}, {-1, 1}, {1, -1}
    };
    int db = 9;

    Intervallum* intervallumok = intervallum_krealas(szamok, db);

    for (int i = 0; i < db; i++) {
        intervallum_egyforma(intervallumok[i], szamok[i]);
    }

    delete[] intervallumok;
}