#include <gtest/gtest.h>

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
#include "../src/jatek.cpp"

    Jatek j;
    j.nev = "F1 2022";
    j.ar = 59.99;

    ASSERT_EQ(j.nev, "F1 2022");
    ASSERT_NEAR(j.ar, 59.99, 0.0001);

    Jatek j2;
    j2.nev = "A cicak birodalma";
    j2.ar = 125.529;

    ASSERT_EQ(j2.nev, "A cicak birodalma");
    ASSERT_NEAR(j2.ar, 125.529, 0.0001);
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
#define struct class
#define public private
#include "../src/jatek.cpp"
#undef public
#undef struct

    class E2: public Jatek {
    public:
        E2(unsigned long megjelenesi_datum) {
            this->megjelenesi_datum = megjelenesi_datum;
        }

        unsigned long get_megjelenesi_datum() {
            return megjelenesi_datum;
        }

    };

    E2 e(542151);
    ASSERT_EQ(e.get_megjelenesi_datum(), 542151);

    E2 ee(3643063453L);
    ASSERT_EQ(ee.get_megjelenesi_datum(), 3643063453L);
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
#define private public
#include "../src/jatek.cpp"
#undef private

    Jatek e;
    e.forraskod = "asd";
    ASSERT_EQ(e.forraskod, "asd");

    Jatek ee;
    ee.forraskod = "class Cica {};";
    ASSERT_EQ(ee.forraskod, "class Cica {};");
}