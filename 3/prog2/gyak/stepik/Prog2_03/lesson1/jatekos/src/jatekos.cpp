#include <string>

using namespace std;

class Jatekos {
private:
    string nev;
    int szint;

public:
    string get_nev() {
        return nev;
    }

    int get_szint() {
        return szint;
    }

    void set_nev(const string &nev) {
        Jatekos::nev = nev;
    }

    void set_szint(int szint) {
        Jatekos::szint = szint;
    }
};