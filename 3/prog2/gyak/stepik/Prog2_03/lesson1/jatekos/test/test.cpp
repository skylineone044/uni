#include <gtest/gtest.h>

#define private public
#define main main_0
#include "../src/jatekos.cpp"
#undef main
#undef private

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Jatekos j {"jozsi", 52};
    ASSERT_EQ(j.get_nev(), "jozsi");
    ASSERT_EQ(j.get_szint(), 52);

    string s = "marika";
    j.set_nev(s);
    j.set_szint(1);

    ASSERT_EQ(j.get_nev(), "marika");
    ASSERT_EQ(j.get_szint(), 1);

    string ss = "eszter";
    j.set_nev(ss);
    j.set_szint(103);

    ASSERT_EQ(j.get_nev(), "eszter");
    ASSERT_EQ(j.get_szint(), 103);
}