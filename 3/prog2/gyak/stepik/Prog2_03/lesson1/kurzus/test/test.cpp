#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/kurzus.cpp"
#undef main

#include "../../tools.cpp"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", kurzus_meghirdetes("programozas 3", 40), str)
    ASSERT_STREQ(str, "A(z) programozas 3 kurzus 40 fore megnyitasra kerult.");
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", kurzus_meghirdetes("kalkulus 3", 1400), str)
    ASSERT_STREQ(str, "A(z) kalkulus 3 kurzus 1400 fore megnyitasra kerult.");
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", kurzus_meghirdetes("kvantumprogramozas"), str)
    ASSERT_STREQ(str, "A(z) kvantumprogramozas kurzus 20 fore megnyitasra kerult.");
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", kurzus_meghirdetes("harry potter"), str)
    ASSERT_STREQ(str, "A(z) harry potter kurzus 20 fore megnyitasra kerult.");
}