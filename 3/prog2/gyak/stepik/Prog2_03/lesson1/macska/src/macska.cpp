#include <string>

using namespace std;

class Macska {
private:
    string nev;
    unsigned int eletkor;
    string szin;

public:
    Macska(const string &nev, unsigned int eletkor, const string &szin) : nev(nev), eletkor(eletkor), szin(szin) {}

    Macska(const string &nev, const string &szin) : nev(nev), szin(szin), eletkor(0){}

    const string& get_nev() const {
        return nev;
    }

    unsigned int get_eletkor() const {
        return eletkor;
    }

    const string& get_szin() const {
        return szin;
    }

};