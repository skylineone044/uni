#include <gtest/gtest.h>
#include <climits>

#define main main_0
#include "../src/macska.cpp"
#undef main

TEST(Test, 2_parameteres) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string nev = "cirmi";
    string szin = "feher";
    Macska macska(nev, szin);
    ASSERT_EQ(macska.get_nev(), "cirmi");
    ASSERT_EQ(macska.get_szin(), "feher");
    ASSERT_EQ(macska.get_eletkor(), 0);

    nev = "zoldike";
    szin = "zold";
    Macska m2(nev, szin);
    ASSERT_EQ(m2.get_nev(), "zoldike");
    ASSERT_EQ(m2.get_szin(), "zold");
    ASSERT_EQ(m2.get_eletkor(), 0);
}

TEST(Test, 3_parameteres) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string nev = "cirmi2";
    string szin = "piros";
    Macska macska(nev, 5, szin);
    ASSERT_EQ(macska.get_nev(), "cirmi2");
    ASSERT_EQ(macska.get_szin(), "piros");
    ASSERT_EQ(macska.get_eletkor(), 5);

    nev = "zoldike";
    szin = "zold";
    Macska m2(nev, -1,szin);
    ASSERT_EQ(m2.get_nev(), "zoldike");
    ASSERT_EQ(m2.get_szin(), "zold");
    ASSERT_EQ(m2.get_eletkor(), UINT_MAX);
    ASSERT_GT(m2.get_eletkor(), 0);
}