#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/torles.cpp"
#undef main

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s = "Macska";
    s -= 'a';
    ASSERT_EQ(s, "Mcsk");
    s -= 'M';
    ASSERT_EQ(s, "csk");
    s -= 'k';
    ASSERT_EQ(s, "cs");
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s = "Lovacska es tehenecske";
    s -= 'e';
    ASSERT_EQ(s, "Lovacska s thncsk");
    s -= 's';
    ASSERT_EQ(s, "Lovacka  thnck");
    s -= 'q';
    ASSERT_EQ(s, "Lovacka  thnck");
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s = "510 arucikket vasaroltam";
    s -= '1';
    ASSERT_EQ(s, "50 arucikket vasaroltam");
    s -= 'k';
    ASSERT_EQ(s, "50 aruciet vasaroltam");
    s -= ' ';
    ASSERT_EQ(s, "50arucietvasaroltam");
    s -= 'a';
    ASSERT_EQ(s, "50rucietvsroltm");
}

TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    string s;
    s -= '1';
    ASSERT_EQ(s, "");
    s -= 'q';
    ASSERT_EQ(s, "");
    s -= '?';
    ASSERT_EQ(s, "");
    s -= '*';
    ASSERT_EQ(s, "");
}