#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/video.cpp"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Video v1("NEM FOGOD ELHINNI MI TORTENT VELEM", "nem tudom", 30);
    update_video(v1, "MI TORTENT MA VELEM?");

    ASSERT_EQ(v1.get_cim(), "MI TORTENT MA VELEM?");
    ASSERT_EQ(v1.get_leiras(), "nem tudom #shorts");
    ASSERT_EQ(v1.get_hossz(), 30);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Video v1("Uj jatek", "ide nem szoktam semmit irni", 150);
    update_video(v1, "HIHETETLEN EZ A JATEK");

    ASSERT_EQ(v1.get_cim(), "HIHETETLEN EZ A JATEK");
    ASSERT_EQ(v1.get_leiras(), "ide nem szoktam semmit irni #shorts");
    ASSERT_EQ(v1.get_hossz(), 150);
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Video v1("Uj zenem", "nagyon jo zene #music", 15);
    update_video(v1, "kijott az UJ ALBUMOM");

    ASSERT_EQ(v1.get_cim(), "kijott az UJ ALBUMOM");
    ASSERT_EQ(v1.get_leiras(), "nagyon jo zene #music #shorts");
    ASSERT_EQ(v1.get_hossz(), 15);
}