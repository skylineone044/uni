#include <iostream>
#include <string>

using namespace std;

class Zene {
    string eloado;
    string cim;

public:
    Zene(string eloado, string cim): eloado(eloado), cim(cim) {}

    string get_eloado() {
        return eloado;
    }

    string get_cim() {
        return cim;
    }
};

Zene zenedoboz() {
    string eloado;
    string cim;

    cin >> eloado;
    cin >> cim;

    return Zene(eloado, cim);
}