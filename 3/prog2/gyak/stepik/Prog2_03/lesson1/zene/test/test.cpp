#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/zene.cpp"
#undef main

#include "../../tools.cpp"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[1000];
    IO("abc def", Zene z1 = zenedoboz(), str)
    ASSERT_EQ(z1.get_eloado(), "abc");
    ASSERT_EQ(z1.get_cim(), "def");
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[1000];
    IO("NagyVillany AzErdo", Zene z1 = zenedoboz(), str)
    ASSERT_EQ(z1.get_eloado(), "NagyVillany");
    ASSERT_EQ(z1.get_cim(), "AzErdo");
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[1000];
    IO("VargaIren Kisapam", Zene z1 = zenedoboz(), str)
    ASSERT_EQ(z1.get_eloado(), "VargaIren");
    ASSERT_EQ(z1.get_cim(), "Kisapam");
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[1000];
    IO("RickAstley NeverGonnaGiveYouUp", Zene z1 = zenedoboz(), str)
    ASSERT_EQ(z1.get_eloado(), "RickAstley");
    ASSERT_EQ(z1.get_cim(), "NeverGonnaGiveYouUp");
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[1000];
    IO("Mehecske Katonasag", Zene z1 = zenedoboz(), str)
    ASSERT_EQ(z1.get_eloado(), "Mehecske");
    ASSERT_EQ(z1.get_cim(), "Katonasag");
}