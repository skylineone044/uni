#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/zh.cpp"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Eredmeny e(100, 60);
    Hallgato h("ASDFGH", e);
    ASSERT_NEAR(h.get_szazalek(), 60, 0.00001);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Eredmeny e(22, 16);
    Hallgato h("ASDFGH", e);
    ASSERT_NEAR(h.get_szazalek(), 72.727272, 0.00001);
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Eredmeny e(40, 40);
    Hallgato h("ASDFGH", e);
    ASSERT_NEAR(h.get_szazalek(), 100, 0.00001);
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Eredmeny e(30, 28);
    Hallgato h("ASDFGH", e);
    ASSERT_NEAR(h.get_szazalek(), 93.33333333, 0.00001);
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Eredmeny e(48, 42);
    Hallgato h("ASDFGH", e);
    ASSERT_NEAR(h.get_szazalek(), 87.5, 0.00001);
}

TEST(Teszt, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Eredmeny e(97, -2);
    Hallgato h("ASDFGH", e);
    ASSERT_NEAR(h.get_szazalek(), -2.061855, 0.00001);
}

TEST(Teszt, 07) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    class E2: public Eredmeny {
    public:
        E2(unsigned int max, int elert) : Eredmeny(max, elert) {}
    };

    E2 e2(5, 6);

    Hallgato h2("asdasd", e2);
}