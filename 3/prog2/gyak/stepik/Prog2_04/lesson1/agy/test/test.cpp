#include <gtest/gtest.h>

#define private public
#define class struct
#define main main_0
#include "../src/agy.cpp"
#undef main
#undef class
#undef private

#include "../../tools.cpp"

bool operator==(Dimenzio d1, Dimenzio d2) {
    return (abs(d1.x - d2.x) < 0.0001 && abs(d1.y - d2.y) < 0.0001 && abs(d1.z - d2.z) < 0.0001);
}

TEST(harom_parameteres_konstruktor, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];

    Dimenzio d1(4, 5, 6);

    IO("", Agy a(d1, "anyag", 5200), str)
    ASSERT_EQ(d1, a.meret);
    ASSERT_NEAR(5200, a.ar, 0.001);
    ASSERT_EQ("anyag", a.anyag);

    ASSERT_STREQ(str, "Agy letrehozva!\n");
}

TEST(harom_parameteres_konstruktor, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];

    Dimenzio d1(6, 3.3, 2.122);

    IO("", const Agy a(d1, "valami", 11.1), str)
    ASSERT_EQ(d1, a.meret);
    ASSERT_NEAR(11.1, a.ar, 0.0001);
    ASSERT_EQ("valami", a.anyag);

    ASSERT_STREQ(str, "Agy letrehozva!\n");
}

TEST(ot_parameteres_konstruktor, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];

    Dimenzio d(4, 5, 6);

    IO("", Agy a(4, 5, 6, "anyag", 5200), str)
    ASSERT_EQ(d, a.meret);
    ASSERT_NEAR(5200, a.ar, 0.001);
    ASSERT_EQ("anyag", a.anyag);

    ASSERT_STREQ(str, "Agy letrehozva!\nAz agy letrehozasa egy extra lepest igenyelt!\n");
}

TEST(ot_parameteres_konstruktor, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];

    Dimenzio d1(6, 2.2, 1);

    IO("", const Agy a(6, 2.2, 1, "valami", 11.1), str)
    ASSERT_EQ(d1, a.meret);
    ASSERT_NEAR(11.1, a.ar, 0.0001);
    ASSERT_EQ("valami", a.anyag);

    ASSERT_STREQ(str, "Agy letrehozva!\nAz agy letrehozasa egy extra lepest igenyelt!\n");
}