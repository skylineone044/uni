#include <string>

using namespace std;

struct Entry {
    string key;
    string value;
};

class Map {
    Entry entries[10];
    unsigned count = 0;

public:
    Map& operator+=(const Entry& entry) {
        if (count >= 10) {
            return *this;
        }

        entries[count] = entry;
        count++;
        return *this;
    }

    const string operator[](const string key) const {
        for (int i = 0; i < 10; ++i) {
            if (this->entries[i].key == key) {
                return this->entries[i].value;
            }
        }
        return this->entries[0].value;
    }

    string operator[](const string key) {
        for (int i = 0; i < 10; ++i) {
            if (this->entries[i].key == key) {
                return this->entries[i].value;
            }
        }
        return this->entries[0].value;
    }
};