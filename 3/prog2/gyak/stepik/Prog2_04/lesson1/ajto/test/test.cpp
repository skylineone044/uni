#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/ajto.cpp"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Entry e1 {"elso", "ot"};
    Entry e2 {"masodik", "hat"};
    Entry e3 {"harmadik", "het"};
    Entry e4 {"negyedik", "tizenot"};

    Map map;
    ASSERT_EQ(map["cica"], "");

    map += e1;
    ASSERT_EQ(map["0"], "ot");
    ASSERT_EQ(map["cica"], "ot");
    ASSERT_EQ(map["masodik"], "ot");
    ASSERT_EQ(map["elso"], "ot");

    map += e4;
    ASSERT_EQ(map["elso"], "ot");
    ASSERT_EQ(map["masodik"], "ot");
    ASSERT_EQ(map["negyedik"], "tizenot");

    map += e3;
    map += e2;
    ASSERT_EQ(map["elso"], "ot");
    ASSERT_EQ(map["cica"], "ot");
    ASSERT_EQ(map["masodik"], "hat");
    ASSERT_EQ(map["harmadik"], "het");
    ASSERT_EQ(map["negyedik"], "tizenot");
    ASSERT_EQ(map["otodik"], "ot");

    Map m2;
    m2 += e3;
    ASSERT_EQ(m2["cica"], "het");

    const Map m3;
    ASSERT_EQ(m3["cica"], "");
}