#include <string>

using namespace std;

class Bolt {
    string nev;
    string hely;
    mutable double osszbevetel;

public:
    Bolt(const string &nev, const string &hely) : nev(nev), hely(hely), osszbevetel(0) {}

    const void vasarlas(int osszeg) const {
        this->osszbevetel += osszeg;
    }
};