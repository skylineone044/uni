#include <gtest/gtest.h>

#define class struct
#define private public
#define main main_0
#include "../src/bolt.cpp"
#undef main
#undef private
#undef class

#include "../../tools.cpp"

TEST(adattagok, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Bolt b("abc", "def");

    b.nev = "masik";
    ASSERT_EQ(b.nev, "masik");

    b.osszbevetel = 100;
    ASSERT_EQ(b.osszbevetel, 100);

    b.hely = "masikhely";
    ASSERT_EQ(b.hely, "masikhely");

    const Bolt bb("bb", "bbb");
    bb.osszbevetel = 1003;
    ASSERT_EQ(bb.osszbevetel, 1003);
}

TEST(konstruktor, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Bolt b("aaa", "bbb");
    ASSERT_EQ(b.nev, "aaa");
    ASSERT_EQ(b.hely, "bbb");
    ASSERT_EQ(b.osszbevetel, 0);
}

TEST(vasarlas, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Bolt b("aaa", "bbb");
    b.vasarlas(40);
    ASSERT_EQ(b.osszbevetel, 40);

    b.vasarlas(42);
    ASSERT_EQ(b.osszbevetel, 82);

    b.vasarlas(2);
    ASSERT_EQ(b.osszbevetel, 84);

    const Bolt bb("aaa", "bbb");
    bb.vasarlas(11);
    ASSERT_EQ(bb.osszbevetel, 11);

    bb.vasarlas(3);
    ASSERT_EQ(bb.osszbevetel, 14);
}