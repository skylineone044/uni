#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/cica.cpp"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Cica c("asd", 3);
    ASSERT_EQ(c.nyavog(), "MIAU");

    Cica c2("asdasd");
    ASSERT_EQ(c2.nyavog(), "MIAU");
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Cica c("asd", 3);
    ASSERT_EQ(c.nyavog(), "MIAU");

    const Cica c2("asdasd");
    ASSERT_EQ(c2.nyavog(), "MIAU");
}