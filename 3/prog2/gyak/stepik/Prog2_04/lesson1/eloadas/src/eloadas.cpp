#include <string>

using namespace std;

class Eloadas {
    string cim;
    unsigned int hossz;

public:
    Eloadas(const string& cim, unsigned int hossz): cim(cim), hossz(hossz) {}

    Eloadas operator+(Eloadas e2) const {
        Eloadas res = Eloadas(this->cim + " es " + e2.cim, this->hossz + e2.hossz);
        return res;
    }
};