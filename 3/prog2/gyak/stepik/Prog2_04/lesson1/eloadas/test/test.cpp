#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#define private public
#define class struct
#include "../src/eloadas.cpp"
#undef class
#undef private
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Eloadas e1("kalkulus", 30);
    Eloadas e2("kiscicak", 45);
    Eloadas e3 = e1 + e2;

    ASSERT_EQ(e1.cim, "kalkulus");
    ASSERT_EQ(e1.hossz, 30);
    ASSERT_EQ(e2.cim, "kiscicak");
    ASSERT_EQ(e2.hossz, 45);

    ASSERT_EQ(e3.cim, "kalkulus es kiscicak");
    ASSERT_EQ(e3.hossz, 75);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Eloadas e1("tavak", 100);
    Eloadas e2("programozas", 5);
    Eloadas e3 = e1 + e2;

    ASSERT_EQ(e3.cim, "tavak es programozas");
    ASSERT_EQ(e3.hossz, 105);
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Eloadas e1("bogarak", 25);
    Eloadas e2("tortak", 35);
    Eloadas e3 = e1 + e2;

    ASSERT_EQ(e3.cim, "bogarak es tortak");
    ASSERT_EQ(e3.hossz, 60);
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Eloadas e1("idojaras", 20);
    const Eloadas e2("idoules", 20);
    const Eloadas e3 = e1 + e2;

    ASSERT_EQ(e3.cim, "idojaras es idoules");
    ASSERT_EQ(e3.hossz, 40);
}