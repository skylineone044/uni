#include <iostream>

using namespace std;

class Haz {
public:
    string utca;
    int hazszam;
    int meret;

    Haz(const string &utca, int hazszam, int meret) : utca(utca), hazszam(hazszam), meret(meret) {}

};

Haz operator+(const Haz haz, int bovites) {
    return Haz(haz.utca, haz.hazszam, haz.meret + bovites);
}

Haz operator+(Haz h1, Haz h2) {
    if (h1.utca == h2.utca) {
        if (abs(h1.hazszam - h2.hazszam) == 2) {
            return Haz(h1.utca, h1.hazszam, h1.meret + h2.meret);
        }
    }
    return h1;
}
