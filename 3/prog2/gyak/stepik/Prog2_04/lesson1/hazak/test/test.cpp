#include <gtest/gtest.h>

#define class struct
#define private public
#define main main_0
#include "../src/hazak.cpp"
#undef main
#undef private
#undef class

TEST(Konstruktor, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Haz h1("utca", 40, 4);
    const Haz h2("utca2", 41, 7);

    ASSERT_EQ(h1.utca, "utca");
    ASSERT_EQ(h1.hazszam, 40);
    ASSERT_EQ(h1.meret, 4);

    ASSERT_EQ(h2.utca, "utca2");
    ASSERT_EQ(h2.hazszam, 41);
    ASSERT_EQ(h2.meret, 7);
}

TEST(hozzaadas, szam) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Haz h1("asd", 55, 4);
    Haz h2 = h1 + 10;
    ASSERT_EQ(h2.utca, "asd");
    ASSERT_EQ(h2.hazszam, 55);
    ASSERT_EQ(h2.meret, 14);

    Haz h4("nyuszi", 1111, 20);
    Haz h3 = h4 + 222;
    ASSERT_EQ(h3.utca, "nyuszi");
    ASSERT_EQ(h3.hazszam, 1111);
    ASSERT_EQ(h3.meret, 242);
}

TEST(hozzaadas, haz) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Haz h1("utca", 40, 4);
    const Haz h2("utca2", 41, 7);

    Haz h3 = h1 + h2;
    ASSERT_EQ(h3.utca, "utca");
    ASSERT_EQ(h3.hazszam, 40);
    ASSERT_EQ(h3.meret, 4);

    const Haz h4("utca", 41, 6);
    Haz h5 = h1 + h4;
    ASSERT_EQ(h5.utca, "utca");
    ASSERT_EQ(h5.hazszam, 40);
    ASSERT_EQ(h5.meret, 4);

    const Haz h6("utca", 42, 8);
    Haz h7 = h6 + h1;
    ASSERT_EQ(h7.utca, "utca");
    ASSERT_EQ(h7.hazszam, 42);
    ASSERT_EQ(h7.meret, 12);

    Haz h8("valami", 29, 10);
    Haz h9("valami", 31, 22);
    Haz h10 = h8 + h9;
    ASSERT_EQ(h10.utca, "valami");
    ASSERT_EQ(h10.hazszam, 29);
    ASSERT_EQ(h10.meret, 32);

    Haz h11("utca", 44, 200);
    Haz h12 = h11 + h11;
    ASSERT_EQ(h12.utca, "utca");
    ASSERT_EQ(h12.hazszam, 44);
    ASSERT_EQ(h12.meret, 200);
}