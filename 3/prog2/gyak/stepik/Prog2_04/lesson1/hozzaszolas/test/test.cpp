#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#define class struct
#define private public
#include "../src/hozzaszolas.cpp"
#undef private
#undef class
#undef main

#include "../../tools.cpp"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Hozzaszolas h("BBB", "A BBB nagyon jo");
    ASSERT_EQ(h.kiiro, "BBB");
    ASSERT_EQ(h.tartalom, "A BBB nagyon jo");

    ASSERT_FALSE(isConst(h.kiiro));
    ASSERT_FALSE(isConst(h.tartalom));
    ASSERT_FALSE(isConst(h.lajkok));

    const Hozzaszolas h2("asd", "bsd");
    h2.lajkok = 50;
    ASSERT_EQ(h2.lajkok, 50);

    h2.lajkok = 33;
    ASSERT_EQ(h2.lajkok, 33);
}