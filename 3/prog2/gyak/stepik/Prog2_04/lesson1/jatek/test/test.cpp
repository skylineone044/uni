#include <gtest/gtest.h>
#include <cmath>

#define class struct
#define private public
#define main main_0
#include "../src/jatek.cpp"
#undef main
#undef private
#undef class

bool egyforma(const Jatek& j1, const Jatek& j2) {
    return j1.gyarto == j2.gyarto && j1.nev == j2.nev && j1.meret == j2.meret && j1.ar == j2.ar;
}

TEST(Jatek, konstruktorok) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Jatek j;
    Jatek j2("teszt", "valamimajdlesz", 5.55, 10000);
    ASSERT_EQ(j2.gyarto, "teszt");
    ASSERT_EQ(j2.nev, "valamimajdlesz");
    ASSERT_EQ(j2.meret, 5.55);
    ASSERT_EQ(j2.ar, 10000);

    const Jatek j3("masik", "egyeb", 40, 404);
    ASSERT_EQ(j3.gyarto, "masik");
    ASSERT_EQ(j3.nev, "egyeb");
    ASSERT_EQ(j3.meret, 40);
    ASSERT_EQ(j3.ar, 404);
}

TEST(Jatek, learazas_visszaallit) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Jatek j("a", "b", 1, 10000);
    j.learazas(50);
    ASSERT_EQ(j.ar, 5000);
    j.visszaallit();
    ASSERT_EQ(j.ar, 10000);

    Jatek j2("a", "b", 10, 4000);
    j2.learazas(10);
    ASSERT_EQ(j2.ar, 3600);
    j2.visszaallit();
    ASSERT_EQ(j2.ar, 4000);

    Jatek j3("aa", "bb", 30.3, 1220);
    j3.learazas(66);
    ASSERT_EQ(j3.ar, 414);
    j3.visszaallit();
    ASSERT_EQ(j3.ar, 1220);

    Jatek j4("aaa", "bbb", 3.1, 2201);
    j4.learazas(100);
    ASSERT_EQ(j4.ar, 0);
    j4.visszaallit();
    ASSERT_EQ(j4.ar, 2201);
}

TEST(Jatek, egyenloseg) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Jatek j1("aaa", "bbb", 5, 4);
    Jatek j2("aaa", "bbb", 7, 3);
    const Jatek j3("bbb", "aaa", 5, 4);
    const Jatek j4("aaa", "bbb", 7, 3);

    ASSERT_TRUE(j1 == j2);
    ASSERT_FALSE(j1 == j3);
    ASSERT_TRUE(j1 == j4);
    ASSERT_FALSE(j2 == j3);
    ASSERT_TRUE(j2 == j4);
    ASSERT_FALSE(j3 == j4);
    ASSERT_TRUE(j4 == j4);
}

TEST(Jatekkonyvtar, konstruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Jatekkonyvtar k("asd");
    k.jatekok;
    ASSERT_EQ(k.jatekosnev, "asd");

    const Jatekkonyvtar k2("masik");
    ASSERT_EQ(k2.jatekosnev, "masik");
}

TEST(Jatekkonyvtar, hozzaadas) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Jatekkonyvtar k("asd");

    Jatek j("a", "b", 1, 10000);
    Jatek j2("a", "b", 10, 4000);
    const Jatek j3("aa", "bb", 30.3, 1220);
    const Jatek j4("aaa", "bbb", 3.1, 2201);

    k += j;
    ASSERT_TRUE(egyforma(j, k.jatekok[0]));

    k += j3;
    ASSERT_TRUE(egyforma(j, k.jatekok[0]));
    ASSERT_TRUE(egyforma(j3, k.jatekok[1]));

    k += j2;
    ASSERT_TRUE(egyforma(j, k.jatekok[0]));
    ASSERT_TRUE(egyforma(j3, k.jatekok[1]));
    ASSERT_TRUE(egyforma(j2, k.jatekok[2]));

    for (int i = 0; i < 97; i++) {
        Jatek jj("gy" + to_string(i), "nev", 4.2, 1000 * i);
        k += jj;
        ASSERT_TRUE(egyforma(jj, k.jatekok[i+3]));
    }

    for (int i = 0; i < 500000; i++) {
        Jatek jj("gya" + to_string(i), "neva", 4.2, 1000 * i);
        k += jj;
    }
}

TEST(Jatekkonyvtar, torles) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Jatekkonyvtar k("a");

    Jatek j("a", "b", 1, 10000);
    Jatek j2("ac", "b", 10, 4000);
    const Jatek j3("aa", "bb", 30.3, 1220);
    const Jatek j4("aa", "bbb", 3.1, 2201);

    k += j;
    k += j2;
    k += j3;
    k += j4;

    k -= j4;
    ASSERT_TRUE(egyforma(j, k.jatekok[0]));
    ASSERT_TRUE(egyforma(j2, k.jatekok[1]));
    ASSERT_TRUE(egyforma(j3, k.jatekok[2]));

    k += j4;
    k -= j;
    ASSERT_TRUE(egyforma(j2, k.jatekok[0]));
    ASSERT_TRUE(egyforma(j3, k.jatekok[1]));
    ASSERT_TRUE(egyforma(j4, k.jatekok[2]));

    k -= j3;
    ASSERT_TRUE(egyforma(j2, k.jatekok[0]));
    ASSERT_TRUE(egyforma(j4, k.jatekok[1]));

    k -= j;
    ASSERT_TRUE(egyforma(j2, k.jatekok[0]));
    ASSERT_TRUE(egyforma(j4, k.jatekok[1]));
}

TEST(Jatekkonyvtar, indexeles) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Jatekkonyvtar k("a");

    Jatek j("a", "b", 1, 10000);
    Jatek j2("ac", "b", 10, 4000);
    const Jatek j3("aa", "bb", 30.3, 1220);
    const Jatek j4("aa", "bbb", 3.1, 2201);

    k += j;
    k += j3;
    k += j2;
    k += j4;

    ASSERT_TRUE(egyforma(j, k[0]));
    ASSERT_TRUE(egyforma(j3, k[1]));
    ASSERT_TRUE(egyforma(j2, k[2]));
    ASSERT_TRUE(egyforma(j4, k[3]));

    k[0] = j4;
    ASSERT_TRUE(egyforma(j4, k[0]));
    ASSERT_TRUE(egyforma(j3, k[1]));
    ASSERT_TRUE(egyforma(j2, k[2]));
    ASSERT_TRUE(egyforma(j4, k[3]));
}