#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#define private public
#define class struct
#include "../src/karora.cpp"
#undef class
#undef private
#undef main

#include "../../tools.cpp"

TEST(konstruktor, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Karora k("Samsung");
    ASSERT_EQ(k.gyarto, "Samsung");

    Karora k2("Huawei");
    ASSERT_EQ(k2.gyarto, "Huawei");
}

TEST(adattag, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Karora k("asd");
    ASSERT_TRUE(isConst(k.gyarto));

    const Karora k2("bsd");
    ASSERT_TRUE(isConst(k2.gyarto));
}