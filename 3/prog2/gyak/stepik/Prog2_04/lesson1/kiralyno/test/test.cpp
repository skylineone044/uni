#include <gtest/gtest.h>
#include <cmath>

#define class struct
#define private public
#define main main_0
#include "../src/kiralyno.cpp"
#undef main
#undef private
#undef class

bool egyforma(const Cselekedet& cs1, const Cselekedet& cs2) {
    return cs1.josagi_mertek == cs2.josagi_mertek && cs1.megnevezes == cs2.megnevezes;
}

TEST(Cselekedet, konstruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Cselekedet cs("lopas", -3.2);
    ASSERT_EQ(cs.megnevezes, "lopas");
    ASSERT_EQ(cs.josagi_mertek, -3.2);

    Cselekedet cs2("kedvesseg", 1.01);
    ASSERT_EQ(cs2.megnevezes, "kedvesseg");
    ASSERT_EQ(cs2.josagi_mertek, 1.01);
}

TEST(Ember, konstruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Cselekedet cs[] = {
            {"lopas", -5},
            {"gyilkossag", -300},
            {"adakozas", 1.3}
    };

    Ember e("juci", cs);
    ASSERT_EQ(e.nev, "juci");
    ASSERT_TRUE(&e.cselekedetek[0] == &cs[0]);
    ASSERT_TRUE(&e.cselekedetek[1] == &cs[1]);
    ASSERT_TRUE(&e.cselekedetek[2] == &cs[2]);
}

TEST(Ember, mennyorszag) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Cselekedet cs0("", 0);

    Cselekedet cs1("lopas", -5);
    Cselekedet cs2("gyilkossag", -350);
    Cselekedet cs3("gyorshajtas", -1.2);
    Cselekedet cs4("baleset_okozas", -2.75);

    Cselekedet cs5("jotekonysag", 1.3);
    Cselekedet cs6("segitseg_nyujtas", 1.09);
    Cselekedet cs7("kedvesseg", 1.01);
    Cselekedet cs8("talalmany", 10);
    Cselekedet cs9("eletmentes", 4);
    Cselekedet cs10("nemtudom", 1.9);
    Cselekedet cs11("utolso", 7.42);
    Cselekedet cs12("najonem", 9);

    Cselekedet c1[] = {cs1, cs0};
    Ember e1("a", c1);
    ASSERT_FALSE(e1.mennyorszag());

    Cselekedet c2[] = {cs1, cs1, cs0};
    Ember e2("a", c2);
    ASSERT_FALSE(e2.mennyorszag());

    Cselekedet c3[] = {cs3, cs3, cs3, cs3, cs3, cs0};
    Ember e3("a", c3);
    ASSERT_FALSE(e3.mennyorszag());

    Cselekedet c4[] = {cs5, cs0};
    Ember e4("a", c4);
    ASSERT_TRUE(e4.mennyorszag());

    Cselekedet c5[] = {cs6, cs6, cs0};
    Ember e5("a", c5);
    ASSERT_TRUE(e5.mennyorszag());

    Cselekedet c6[] = {cs6, cs6, cs6, cs0};
    Ember e6("a", c6);
    ASSERT_TRUE(e6.mennyorszag());

    Cselekedet c7[] = {cs1, cs8, cs0};
    Ember e7("a", c7);
    ASSERT_FALSE(e7.mennyorszag());

    Cselekedet c8[] = {cs3, cs8, cs0};
    Ember e8("a", c8);
    ASSERT_TRUE(e8.mennyorszag());

    Cselekedet c9[] = {cs3, cs5, cs5, cs3, cs5, cs0};
    Ember e9("a", c9);
    ASSERT_TRUE(e9.mennyorszag());

    Cselekedet c10[] = {cs4, cs3, cs5, cs5, cs3, cs5, cs0};
    Ember e10("a", c10);
    ASSERT_FALSE(e10.mennyorszag());

    Cselekedet c11[] = {cs10, cs4, cs3, cs5, cs5, cs3, cs5, cs9, cs10, cs0};
    Ember e11("a", c11);
    ASSERT_TRUE(e11.mennyorszag());

    Cselekedet c12[] = {cs10, cs4, cs3, cs5, cs8, cs8, cs2, cs5, cs3, cs5, cs9, cs10, cs8, cs8, cs0};
    Ember e12("a", c12);
    ASSERT_FALSE(e12.mennyorszag());

    Cselekedet c13[] = {cs10, cs8, cs4, cs11, cs11, cs3, cs5, cs8, cs8, cs2, cs5, cs3, cs11, cs11, cs5, cs9, cs10, cs8, cs8, cs11, cs0};
    Ember e13("a", c13);
    ASSERT_FALSE(e13.mennyorszag());

    Cselekedet c14[] = {cs10, cs8, cs4, cs11, cs12, cs11, cs3, cs5, cs8, cs8, cs2, cs5, cs3, cs11, cs11, cs5, cs9, cs10, cs8, cs8, cs11, cs0};
    Ember e14("a", c14);
    ASSERT_TRUE(e14.mennyorszag());
}

TEST(mennyorszagba_jut_a_kiralyno, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Cselekedet cs0("", 0);

    Cselekedet cs1("lopas", -5);
    Cselekedet cs2("gyilkossag", -350);
    Cselekedet cs3("gyorshajtas", -1.2);
    Cselekedet cs4("baleset_okozas", -2.75);

    Cselekedet cs5("jotekonysag", 1.3);
    Cselekedet cs6("segitseg_nyujtas", 1.09);
    Cselekedet cs7("kedvesseg", 1.01);
    Cselekedet cs8("talalmany", 10);
    Cselekedet cs9("eletmentes", 4);
    Cselekedet cs10("nemtudom", 1.9);
    Cselekedet cs11("utolso", 7.42);
    Cselekedet cs12("najonem", 9);

    Cselekedet c1[] = {cs1, cs0};
    Ember e1("Leenhurbertindrund alneven", c1);

    Cselekedet c2[] = {cs1, cs1, cs0};
    Ember e2("Leenhurbertindrund kiralyno", c2);

    Cselekedet c3[] = {cs3, cs3, cs3, cs3, cs3, cs0};
    Ember e3("nem Leenhurbertindrund", c3);

    Cselekedet c4[] = {cs5, cs0};
    Ember e4("nemet kiralyno", c4);

    Cselekedet c5[] = {cs6, cs6, cs0};
    Ember e5("kiralyno lenne", c5);

    Cselekedet c6[] = {cs6, cs6, cs6, cs0};
    Ember e6("Marika", c6);

    Cselekedet c7[] = {cs1, cs8, cs0};
    Ember e7("Leenhurbertindrund kiralyno", c7);

    Cselekedet c8[] = {cs3, cs8, cs0};
    Ember e8("Antal", c8);

    Cselekedet c9[] = {cs3, cs5, cs5, cs3, cs5, cs0};
    Ember e9("Sandor", c9);

    Cselekedet c10[] = {cs4, cs3, cs5, cs5, cs3, cs5, cs0};
    Ember e10("Leenhurbertindrund kiralyno", c10);

    Cselekedet c11[] = {cs10, cs4, cs3, cs5, cs5, cs3, cs5, cs9, cs10, cs0};
    Ember e11("Leenhurbertindrund kiralyno", c11);

    Cselekedet c12[] = {cs10, cs4, cs3, cs5, cs8, cs8, cs2, cs5, cs3, cs5, cs9, cs10, cs8, cs8, cs0};
    Ember e12("Gyongyi", c12);

    Cselekedet c13[] = {cs10, cs8, cs4, cs11, cs11, cs3, cs5, cs8, cs8, cs2, cs5, cs3, cs11, cs11, cs5, cs9, cs10, cs8, cs8, cs11, cs0};
    Ember e13("Mokuska", c13);

    Cselekedet c14[] = {cs10, cs8, cs4, cs11, cs12, cs11, cs3, cs5, cs8, cs8, cs2, cs5, cs3, cs11, cs11, cs5, cs9, cs10, cs8, cs8, cs11, cs0};
    Ember e14("Leenhurbertindrund kiralyno", c14);

    Ember ee1[] = {e5, e6, e8, e10};
    ASSERT_FALSE(mennyorszagba_jut_a_kiralyno(ee1, 4));

    Ember ee2[] = {e5, e10, e6, e8};
    ASSERT_FALSE(mennyorszagba_jut_a_kiralyno(ee2, 4));

    Ember ee3[] = {e4, e7, e5, e8};
    ASSERT_FALSE(mennyorszagba_jut_a_kiralyno(ee3, 4));

    Ember ee4[] = {e11, e4, e5, e6};
    ASSERT_TRUE(mennyorszagba_jut_a_kiralyno(ee4, 4));

    Ember ee5[] = {e6, e4, e11, e5};
    ASSERT_TRUE(mennyorszagba_jut_a_kiralyno(ee5, 4));

    Ember ee6[] = {e1, e2, e3, e4, e5};
    ASSERT_FALSE(mennyorszagba_jut_a_kiralyno(ee6, 5));

    Ember ee7[] = {e1, e5, e3, e4, e2};
    ASSERT_FALSE(mennyorszagba_jut_a_kiralyno(ee7, 5));

    Ember ee8[] = {e1, e14, e13, e12, e9};
    ASSERT_TRUE(mennyorszagba_jut_a_kiralyno(ee8, 5));

    Ember ee9[] = {e9, e1, e13, e12, e14};
    ASSERT_TRUE(mennyorszagba_jut_a_kiralyno(ee9, 5));

    Ember ee10[] = {e12, e13, e14};
    ASSERT_TRUE(mennyorszagba_jut_a_kiralyno(ee10, 3));

    Ember ee11[] = {e14};
    ASSERT_TRUE(mennyorszagba_jut_a_kiralyno(ee11, 1));

    Ember ee12[] = {e11};
    ASSERT_TRUE(mennyorszagba_jut_a_kiralyno(ee12, 1));

    Ember ee13[] = {e1, e3, e4, e5, e6, e8, e9, e11, e12, e13};
    ASSERT_TRUE(mennyorszagba_jut_a_kiralyno(ee13, 10));

    Ember ee14[] = {e1, e3, e4, e5, e6, e8, e9, e13, e12, e11};
    ASSERT_TRUE(mennyorszagba_jut_a_kiralyno(ee14, 10));
}