#include <string>

#define KONYV_SZAM 10

using namespace std;

struct Konyv {
    string szerzo;
    string cim;
};

class Konyvespolc {
    Konyv konyvek[KONYV_SZAM];
    unsigned darab = 0;

public:
    Konyvespolc& operator+=(const Konyv& konyv) {
        if (darab >= KONYV_SZAM) {
            return *this;
        }

        konyvek[darab] = konyv;
        darab++;
        return *this;
    }

    const Konyv& operator[](unsigned index) const {
        if (index >= KONYV_SZAM) {
            return konyvek[0];
        }

        return konyvek[index];
    }

    Konyv& operator[](unsigned index) {
        if (index >= KONYV_SZAM) {
            return konyvek[0];
        }

        return konyvek[index];
    }

};