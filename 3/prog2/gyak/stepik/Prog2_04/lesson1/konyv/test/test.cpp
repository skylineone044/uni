#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/konyv.cpp"
#undef main

bool operator==(const Konyv& k1, const Konyv& k2) {
    return k1.cim == k2.cim && k1.szerzo == k2.szerzo;
}

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Konyv k1 = {"asd", "adbf"};
    const Konyv k2 = {"asfacv", "asvk"};
    const Konyv k3 = {"mvoad", "vadof"};

    Konyvespolc p1;
    p1 += k1;

    ASSERT_EQ(p1[0], k1);

    p1 += k2;
    ASSERT_EQ(p1[0], k1);
    ASSERT_EQ(p1[1], k2);

    p1[0] = k3;
    ASSERT_EQ(p1[0], k3);

    const Konyvespolc p2;
    ASSERT_EQ(p2[0], Konyv());
}