#include <cmath>

class Kor{
    const double r;

public:
    Kor(const double r) : r(r) {}

    const double get_r() const {
        return r;
    }

    const double kerulet() const {
        return 2 * r * M_PI;
    }

    const double terulet() const {
        return r * r * M_PI;
    }
};