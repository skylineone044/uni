#include <gtest/gtest.h>
#include <cmath>

#define private public
#define class struct
#define main main_0
#include "../src/kor.cpp"
#undef main
#undef class
#undef private

#include "../../tools.cpp"

TEST(adattag, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Kor k(1);
    ASSERT_TRUE(isConst(k.r));

    const Kor k2(5);
    ASSERT_TRUE(isConst(k2.r));
}

TEST(konstruktor, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Kor k(12);
    ASSERT_EQ(k.r, 12);

    const Kor k2(8.5);
    ASSERT_EQ(k2.r, 8.5);
}

TEST(getter, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Kor k(1);
    ASSERT_NEAR(k.get_r(), 1, 0.0001);

    const Kor k2(5.2);
    ASSERT_NEAR(k2.get_r(), 5.2, 0.0001);

    ASSERT_TRUE(isConst(k.get_r()));
}

TEST(kerulet, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Kor k(1);
    ASSERT_NEAR(k.kerulet(), 6.28318530718, 0.0001);

    const Kor k2(5.2);
    ASSERT_NEAR(k2.kerulet(), 32.6725635973, 0.0001);

    ASSERT_TRUE(isConst(k.kerulet()));
}

TEST(terulet, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Kor k(44.4);
    ASSERT_NEAR(k.terulet(), 6193.21009358, 0.0001);

    const Kor k2(0.14);
    ASSERT_NEAR(k2.terulet(), 0.06157521601, 0.0001);

    ASSERT_TRUE(isConst(k.kerulet()));
}