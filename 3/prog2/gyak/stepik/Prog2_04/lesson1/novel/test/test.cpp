#include <gtest/gtest.h>

#define main main_0
#include "../src/novel.cpp"
#undef main

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int* a = new int;
    *a = 5;

    const int* const eredeti_cim = a;
    novel(a);
    ASSERT_EQ(eredeti_cim, a);
    ASSERT_EQ(*a, 6);
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    int* a = new int;
    *a = 22;

    const int* const eredeti_cim = a;
    novel(a);
    ASSERT_EQ(eredeti_cim, a);
    ASSERT_EQ(*a, 23);
}