#include <string>

using namespace std;

class Parna {
    string minta;
    unsigned int meret;
public:
    Parna() = default;

public:
    Parna(const string& minta, unsigned int meret): minta(minta), meret(meret) {}
};

class Agy {
    string szin;
    Parna parnak[10];

public:
    Agy(const string& szin): szin(szin) {}
};