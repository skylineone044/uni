#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#define private public
#define class struct
#include "../src/parnak.cpp"
#undef class
#undef private
#undef main

TEST(Parna, konstruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Parna p;
    Parna p2("pottyos", 30);

    ASSERT_EQ(p2.minta, "pottyos");
    ASSERT_EQ(p2.meret, 30);
}

TEST(Agy, konstruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Agy a("fekete");

    ASSERT_EQ(a.szin, "fekete");
    a.parnak[0];
}