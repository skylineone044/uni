#include <string>

using namespace std;

class Penztarca {
    string tulajdonos;
    double penzmennyiseg;

public:
    Penztarca(const string &tulajdonos) : tulajdonos(tulajdonos), penzmennyiseg(0) {}

    Penztarca& operator+=(Penztarca& p2) {
        if (p2.tulajdonos == this->tulajdonos) {
            double pm = p2.penzmennyiseg;
            this->penzmennyiseg += pm;
            p2.penzmennyiseg -= pm;
        }
        return *this;
    }

    Penztarca& operator+=(double penz) {
        this->penzmennyiseg += penz;
        return *this;
    }

    Penztarca& operator-=(double penz) {
        if (this->penzmennyiseg >= penz) {
            this->penzmennyiseg -= penz;
        }
        return *this;
    }
};