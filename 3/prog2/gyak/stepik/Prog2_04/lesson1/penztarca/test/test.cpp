#include <gtest/gtest.h>

#define class struct
#define private public
#define main main_0
#include "../src/penztarca.cpp"
#undef main
#undef private
#undef class

using namespace std;

TEST(Konstruktor, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Penztarca p("Jozsi");
    ASSERT_EQ(p.tulajdonos, "Jozsi");
    ASSERT_EQ(p.penzmennyiseg, 0);

    const Penztarca p2("MArika");
    ASSERT_EQ(p2.tulajdonos, "MArika");
    ASSERT_EQ(p2.penzmennyiseg, 0);
}

TEST(muveletek, penztarca_hozzaadasa) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Penztarca p1("Jozsi");
    Penztarca p2("asdas");
    Penztarca p3("Jozsi");
    Penztarca p4("mmbmbb");

    p1.penzmennyiseg = 40;
    p2.penzmennyiseg = 77;
    p3.penzmennyiseg = 100;
    p4.penzmennyiseg = 5;

    p1 += p2;
    ASSERT_EQ(p1.penzmennyiseg, 40);
    ASSERT_EQ(p2.penzmennyiseg, 77);

    p1 += p3;
    ASSERT_EQ(p1.penzmennyiseg, 140);
    ASSERT_EQ(p3.penzmennyiseg, 0);

    p4 += p4;
    ASSERT_EQ(p4.penzmennyiseg, 5);
}

TEST(muveletek, penz_hozzaadasa) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Penztarca p1("Jozsi");

    p1.penzmennyiseg = 40;

    p1 += 5;
    ASSERT_EQ(p1.penzmennyiseg, 45);

    p1 += 2;
    ASSERT_EQ(p1.penzmennyiseg, 47);
}

TEST(muveletek, penz_elvetele) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Penztarca p1("Jozsi");

    p1.penzmennyiseg = 40;

    p1 -= 5;
    ASSERT_EQ(p1.penzmennyiseg, 35);

    p1 -= 2;
    ASSERT_EQ(p1.penzmennyiseg, 33);

    p1 -= 34;
    ASSERT_EQ(p1.penzmennyiseg, 33);

    p1 -= 33;
    ASSERT_EQ(p1.penzmennyiseg, 0);

    p1.penzmennyiseg = 2;

    p1 -= 1;
    ASSERT_EQ(p1.penzmennyiseg, 1);
}