#include <gtest/gtest.h>

#define main main_0
#include "../src/pointer.cpp"
#undef main

#include "../../tools.cpp"

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_FALSE(isConst(pointer));
    ASSERT_TRUE(isConst(*pointer));
}