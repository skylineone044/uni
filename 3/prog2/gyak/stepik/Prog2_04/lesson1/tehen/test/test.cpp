#include <gtest/gtest.h>

#define main main_0
#include "../src/tehen.cpp"
#undef main

#include "../../tools.cpp"

TEST(konstruktor, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Tehen t("piros");
    const Tehen t2("sarga");
}

TEST(getter, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Tehen t("piros");
    ASSERT_EQ(t.get_szin(), "piros");
    ASSERT_TRUE(isConst(t.get_szin()));
}

TEST(getter, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Tehen t2("kek");
    ASSERT_EQ(t2.get_szin(), "kek");
    ASSERT_TRUE(isConst(t2.get_szin()));
}