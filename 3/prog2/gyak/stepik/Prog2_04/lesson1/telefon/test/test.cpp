#include <gtest/gtest.h>

#define class struct
#define private public
#define main main_0
#include "../src/telefon.cpp"
#undef main
#undef private
#undef class

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Alkalmazas a;
    a.nev = "valami";
    a.meret = 40;

    Alkalmazas a2;
    a2.nev = "masik";
    a2.meret = 22.2;

    Telefon t("nemtudom");
    ASSERT_EQ(t.nev, "nemtudom");

    t.alkalmazasok[0] = a;
    t.alkalmazasok[9] = a2;
}