#include <string>

using namespace std;

class Uzenet {
    string szoveg;

public:
    Uzenet& operator+=(char c) {
        this->szoveg += c;
        return *this;
    }

    const string& get_szoveg() const {
        return szoveg;
    }
};