#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#define class struct
#define private public
#include "../src/uzenet.cpp"
#undef private
#undef class
#undef main

TEST(plusz_egyenlo, mukodes) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Uzenet u;
    ASSERT_EQ(u.szoveg, "");

    u += 'a';
    ASSERT_EQ(u.szoveg, "a");

    u += 'l';
    u += 'm';
    u += 'a';
    ASSERT_EQ(u.szoveg, "alma");

    Uzenet u2;
    u2 += 'c';
    u2 += 'i';
    u2 += 'c';
    u2 += 'a';
    u2 += '*';
    u2 += '5';
    ASSERT_EQ(u2.szoveg, "cica*5");
}

TEST(plusz_egyenlo, osszefuzes) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Uzenet u;
    ((((u += 'L') += 'o') += 'v') += 'a') += 'k';
    ASSERT_EQ(u.szoveg, "Lovak");
}