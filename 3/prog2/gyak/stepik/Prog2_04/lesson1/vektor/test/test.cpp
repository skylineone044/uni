#include <gtest/gtest.h>
#include <cmath>

#define main main_0
#include "../src/vektor.cpp"
#undef main

TEST(Konstruktor, 3_parameter) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Vector3 v(5, 4, 2);
    ASSERT_NEAR(v.x, 5, 0.0001);
    ASSERT_NEAR(v.y, 4, 0.0001);
    ASSERT_NEAR(v.z, 2, 0.0001);

    const Vector3 v2(2.2, 4.42, 3.2);
    ASSERT_NEAR(v2.x, 2.2, 0.0001);
    ASSERT_NEAR(v2.y, 4.42, 0.0001);
    ASSERT_NEAR(v2.z, 3.2, 0.0001);
}

TEST(Konstruktor, 2_parameter) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Vector3 v(15, 14);
    ASSERT_NEAR(v.x, 15, 0.0001);
    ASSERT_NEAR(v.y, 14, 0.0001);
    ASSERT_NEAR(v.z, 0, 0.0001);

    const Vector3 v2(32.2, 2.42);
    ASSERT_NEAR(v2.x, 32.2, 0.0001);
    ASSERT_NEAR(v2.y, 2.42, 0.0001);
    ASSERT_NEAR(v2.z, 0, 0.0001);
}

TEST(Konstruktor, 1_parameter) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Vector3 v(3);
    ASSERT_NEAR(v.x, 3, 0.0001);
    ASSERT_NEAR(v.y, 0, 0.0001);
    ASSERT_NEAR(v.z, 0, 0.0001);

    const Vector3 v2(3.28);
    ASSERT_NEAR(v2.x, 3.28, 0.0001);
    ASSERT_NEAR(v2.y, 0, 0.0001);
    ASSERT_NEAR(v2.z, 0, 0.0001);
}

TEST(Konstruktor, 0_parameter) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Vector3 v;
    ASSERT_NEAR(v.x, 0, 0.0001);
    ASSERT_NEAR(v.y, 0, 0.0001);
    ASSERT_NEAR(v.z, 0, 0.0001);

    const Vector3 v2;
    ASSERT_NEAR(v2.x, 0, 0.0001);
    ASSERT_NEAR(v2.y, 0, 0.0001);
    ASSERT_NEAR(v2.z, 0, 0.0001);
}

TEST(Muveletek, osszeadas) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Vector3 v1(5.1, 4.8, 2);
    Vector3 v2(7.2, 4.4, 5.5);

    Vector3 v3 = v1 + v2;
    ASSERT_NEAR(v3.x, 12.3, 0.0001);
    ASSERT_NEAR(v3.y, 9.2, 0.0001);
    ASSERT_NEAR(v3.z, 7.5, 0.0001);

    const Vector3 v4(5.5, 5, 4);
    const Vector3 v5(2, 2, 5.1);
    Vector3 v6 = v4 + v5;
    ASSERT_NEAR(v6.x, 7.5, 0.0001);
    ASSERT_NEAR(v6.y, 7, 0.0001);
    ASSERT_NEAR(v6.z, 9.1, 0.0001);
}

TEST(Muveletek, kivonas) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Vector3 v1(7.2, 3.8, 5.5);
    Vector3 v2(5.2, 4.4, 5.5);

    Vector3 v3 = v1 - v2;
    ASSERT_NEAR(v3.x, 2, 0.0001);
    ASSERT_NEAR(v3.y, -0.6, 0.0001);
    ASSERT_NEAR(v3.z, 0, 0.0001);

    const Vector3 v4(2.5, 5, 4);
    const Vector3 v5(2, 3.1, 5.1);
    Vector3 v6 = v4 - v5;
    ASSERT_NEAR(v6.x, 0.5, 0.0001);
    ASSERT_NEAR(v6.y, 1.9, 0.0001);
    ASSERT_NEAR(v6.z, -1.1, 0.0001);
}

TEST(Muveletek, szorzas) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Vector3 v1(7.2, -3.8, 5.5);

    Vector3 v3 = v1 * 3;
    ASSERT_NEAR(v3.x, 21.6, 0.0001);
    ASSERT_NEAR(v3.y, -11.4, 0.0001);
    ASSERT_NEAR(v3.z, 16.5, 0.0001);

    const Vector3 v4(2.5, 5, -4.1);
    Vector3 v6 = v4 * 2.4;
    ASSERT_NEAR(v6.x, 6, 0.0001);
    ASSERT_NEAR(v6.y, 12, 0.0001);
    ASSERT_NEAR(v6.z, -9.84, 0.0001);

    const Vector3 v7(-5, 3, 1.1);
    Vector3 v8 = v7 * -0.5;
    ASSERT_NEAR(v8.x, 2.5, 0.0001);
    ASSERT_NEAR(v8.y, -1.5, 0.0001);
    ASSERT_NEAR(v8.z, -0.55, 0.0001);
}

TEST(Muveletek, osztas) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Vector3 v1(7.2, -3.8, 5.5);

    Vector3 v3 = v1 / 3;
    ASSERT_NEAR(v3.x, 2.4, 0.0001);
    ASSERT_NEAR(v3.y, -1.2666666, 0.0001);
    ASSERT_NEAR(v3.z, 1.8333333, 0.0001);

    const Vector3 v4(2.5, 5, -4.1);
    Vector3 v6 = v4 / 2.4;
    ASSERT_NEAR(v6.x, 1.04166666, 0.0001);
    ASSERT_NEAR(v6.y, 2.08333333, 0.0001);
    ASSERT_NEAR(v6.z, -1.70833333, 0.0001);

    const Vector3 v7(-5, 3, 1.1);
    Vector3 v8 = v7 / -0.5;
    ASSERT_NEAR(v8.x, 10, 0.0001);
    ASSERT_NEAR(v8.y, -6, 0.0001);
    ASSERT_NEAR(v8.z, -2.2, 0.0001);
}

TEST(Osszehasonlitas, egyenlo) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Vector3 v1(7.2, -3.8, 5.5);
    Vector3 v2(7.2, -3.8, 5.501);
    const Vector3 v3(0.72, -3.8, 5.5);
    Vector3 v4(7.2, -3.7, 5.5);
    const Vector3 v5(7.2, 3.8, -5.5);
    Vector3 v6(7.2, -3.8, 5.501);
    const Vector3 v7(0.72, -3.8, 5.5);

    ASSERT_FALSE(v1 == v2);
    ASSERT_FALSE(v1 == v3);
    ASSERT_FALSE(v1 == v4);
    ASSERT_FALSE(v1 == v5);
    ASSERT_FALSE(v1 == v6);
    ASSERT_FALSE(v1 == v7);
    ASSERT_TRUE(v2 == v6);
    ASSERT_TRUE(v3 == v7);
    ASSERT_TRUE(v5 == v5);
}

TEST(Osszehasonlitas, nemegyenlo) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Vector3 v1(7.2, -3.8, 5.5);
    Vector3 v2(7.2, -3.8, 5.501);
    const Vector3 v3(0.72, -3.8, 5.5);
    Vector3 v4(7.2, -3.7, 5.5);
    const Vector3 v5(7.2, 3.8, -5.5);
    Vector3 v6(7.2, -3.8, 5.501);
    const Vector3 v7(0.72, -3.8, 5.5);

    ASSERT_TRUE(v1 != v2);
    ASSERT_TRUE(v1 != v3);
    ASSERT_TRUE(v1 != v4);
    ASSERT_TRUE(v1 != v5);
    ASSERT_TRUE(v1 != v6);
    ASSERT_TRUE(v1 != v7);
    ASSERT_FALSE(v2 != v6);
    ASSERT_FALSE(v3 != v7);
    ASSERT_FALSE(v5 != v5);
}

TEST(Indexer, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Vector3 v1(5, 6, 2);
    ASSERT_NEAR(v1[0], 5, 0.0001);
    ASSERT_NEAR(v1[1], 6, 0.0001);
    ASSERT_NEAR(v1[2], 2, 0.0001);

    const Vector3 v2(4.2, 5.5, -3.2);
    ASSERT_NEAR(v2[0], 4.2, 0.0001);
    ASSERT_NEAR(v2[1], 5.5, 0.0001);
    ASSERT_NEAR(v2[2], -3.2, 0.0001);
}

TEST(hossz, sqrMagnitude) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Vector3 v1(5.5, 4.4, 3.3);
    const Vector3 v2(7.7, -2.1, 4);

    ASSERT_NEAR(v1.sqrMagnitude(), 60.5, 0.0001);
    ASSERT_NEAR(v2.sqrMagnitude(), 79.7, 0.0001);
}

TEST(hossz, magnitude) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Vector3 v1(5.5, 4.4, 3.3);
    const Vector3 v2(7.7, -2.1, 4);

    ASSERT_NEAR(v1.magnitude(), 7.7781745, 0.0001);
    ASSERT_NEAR(v2.magnitude(), 8.9274856, 0.0001);
}