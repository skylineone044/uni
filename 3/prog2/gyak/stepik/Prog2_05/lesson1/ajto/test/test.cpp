#include <gtest/gtest.h>
#include <string>

using namespace std;

#define class struct
#define private public
#define main main_0
#include "../src/ajto.cpp"
#undef main
#undef private
#undef class

TEST(Mukodes, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Ajto a1(true, 5);
    !a1;
    ASSERT_EQ(a1.irany, false);
    ASSERT_EQ(a1.meret, 5);

    !a1;
    ASSERT_EQ(a1.irany, true);
    ASSERT_EQ(a1.meret, 5);
}

TEST(VisszateresiErtek, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Ajto a1(true, 7);
    Ajto a2 = !a1;
    ASSERT_EQ(a1.irany, false);
    ASSERT_EQ(a1.meret, 7);
    ASSERT_EQ(a2.irany, false);
    ASSERT_EQ(a2.meret, 7);

    Ajto a3 = !a1;

    ASSERT_EQ(a1.irany, true);
    ASSERT_EQ(a1.meret, 7);
    ASSERT_EQ(a3.irany, true);
    ASSERT_EQ(a3.meret, 7);

    ASSERT_EQ(&a1, &!a1);
}