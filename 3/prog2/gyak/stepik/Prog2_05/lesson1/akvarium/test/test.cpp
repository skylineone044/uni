#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/akvarium.cpp"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Akvarium a1(3, 5, 7);
    ASSERT_NEAR((double) a1, 105, 0.0001);

    Akvarium a2(4.4, 7.3, 2.1);
    ASSERT_NEAR((double) a2, 67.452, 0.0001);

    Akvarium a3(1.1, 7.241, 5.55);
    ASSERT_NEAR((double) a3, 44.206305, 0.0001);
}