#include <gtest/gtest.h>

#define main main_0
#include "../src/allando.cpp"
#undef main

#include "../../tools.cpp"

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[1000];
    IO("", int res = main_0(), str)
    ASSERT_STREQ(str, "");
    ASSERT_STREQ(error_string, "HIBA\n");
    ASSERT_EQ(res, 1);
}