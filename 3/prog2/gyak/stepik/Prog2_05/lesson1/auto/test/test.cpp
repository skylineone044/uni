#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/auto.cpp"
#undef main

#include "../../tools.cpp"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Auto a1(5);
    Auto a2(7);

    char str[100];
    IO("", gyorsit(a1, a2), str)
    ASSERT_STREQ(str, "E5T7E7");
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Auto a1(3);
    Auto a2(2);

    char str[100];
    IO("", gyorsit(a1, a2), str)
    ASSERT_STREQ(str, "E3T2E2");
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Auto a1(3);
    Auto a2(2);

    char str[100];
    IO("", gyorsit(a2, a1), str)
    ASSERT_STREQ(str, "E2T3E3");
}