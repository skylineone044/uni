#include <gtest/gtest.h>

#define class struct
#define private public
#define main main_0
#include "../src/blokkok.cpp"
#undef main
#undef class
#undef private

bool egyforma(Blokk b, int x, int y) {
    return b.meret.x == x && b.meret.y == y;
}

TEST(Blokk, osszeadas) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Blokk b1(4, 7);
    const Blokk b2(3, 7);
    const Blokk b3(3, 5);
    const Blokk b4(3, 5);
    const Blokk b5(2, 2);
    const Blokk b6(4, 2);
    const Blokk b7(1, 5);

    Blokk bb1 = b1 + b2;
    ASSERT_TRUE(egyforma(bb1, 7, 7));

    Blokk bb2 = b3 + b7;
    ASSERT_TRUE(egyforma(bb2, 4, 5));

    Blokk bb3 = b2 + b3;
    ASSERT_TRUE(egyforma(bb3, 3, 12));

    Blokk bb4 = b1 + b6;
    ASSERT_TRUE(egyforma(bb4, 4, 9));

    Blokk bb5 = b3 + b4;
    ASSERT_TRUE(egyforma(bb5, 6, 5));

    Blokk bb6 = b7 + b5;
    ASSERT_TRUE(egyforma(bb6, 0, 0));

    Blokk bb7 = b4 + bb1;
    ASSERT_TRUE(egyforma(bb7, 0, 0));
}

TEST(Blokk, kivonas) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Blokk b1(4, 7);
    const Blokk b2(3, 7);
    const Blokk b3(3, 5);
    const Blokk b4(3, 5);
    const Blokk b5(2, 2);
    const Blokk b6(4, 2);
    const Blokk b7(1, 5);

    Blokk bb1 = b1 - b2;
    ASSERT_TRUE(egyforma(bb1, 1, 7));

    Blokk bb2 = b6 - b5;
    ASSERT_TRUE(egyforma(bb2, 2, 2));

    Blokk bb3 = b2 - b3;
    ASSERT_TRUE(egyforma(bb3, 3, 2));

    Blokk bb4 = b4 - bb3;
    ASSERT_TRUE(egyforma(bb4, 3, 3));

    Blokk bb5 = b2 - b1;
    ASSERT_TRUE(egyforma(bb5, 0, 0));

    Blokk bb6 = bb4 - b4;
    ASSERT_TRUE(egyforma(bb6, 0, 0));

    Blokk bb7 = b3 - b4;
    ASSERT_TRUE(egyforma(bb7, 0, 0));

    Blokk e1 = bb3;
    Blokk bb8 = bb3 - e1;
    ASSERT_TRUE(egyforma(bb8, 0, 0));
}