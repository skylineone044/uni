#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/bolygo.cpp"
#undef main

#include "../../tools.cpp"

Bolygo b1 = {"ad", true};
Bolygo b2 = {"sasda", true};
Bolygo b3 = {"afav", false};
Bolygo b4 = {"asfasv", true};
Bolygo b5 = {"anvk", true};
Bolygo b6 = {"aklsnv", true};
Bolygo b7 = {"olakn", false};
Bolygo b8 = {"klan", false};

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Bolygo bolygok[] = {b1, b2, b3, b4};
    int db = 4;
    int elvart = 3;

    int eredmeny = kozetek(bolygok, db);
    ASSERT_EQ(elvart, eredmeny);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Bolygo bolygok[] = {b1, b2, b5, b4, b3, b6, b7, b8};
    int db = 4;
    int elvart = 4;

    int eredmeny = kozetek(bolygok, db);
    ASSERT_EQ(elvart, eredmeny);
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Bolygo bolygok[] = {b1};
    int db = 1;
    int elvart = 1;

    int eredmeny = kozetek(bolygok, db);
    ASSERT_EQ(elvart, eredmeny);
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Bolygo bolygok[] = {b3, b7, b8};
    int db = 3;
    int elvart = 0;

    int eredmeny = kozetek(bolygok, db);
    ASSERT_EQ(elvart, eredmeny);
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Bolygo bolygok[] = {b3, b7, b8};
    int db = 0;
    int elvart = 0;

    int eredmeny = kozetek(bolygok, db);
    ASSERT_EQ(elvart, eredmeny);
}

TEST(Teszt, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Bolygo bolygok[] = {b5, b3, b2, b7};
    int db = 4;
    int elvart = 2;

    int eredmeny = kozetek(bolygok, db);
    ASSERT_EQ(elvart, eredmeny);
}

TEST(Fajlmeret, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    checkFileSize("bolygo", "bolygo.cpp", 150);
}