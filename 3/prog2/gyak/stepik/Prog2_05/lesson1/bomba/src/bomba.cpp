class Bomba {
    mutable int hatralevo_ido;

    void dec() {
        this->hatralevo_ido = this->hatralevo_ido <= 0 ? 0 : this->hatralevo_ido -1;
    }


    const void dec() const {
        this->hatralevo_ido = this->hatralevo_ido <= 0 ? 0 : this->hatralevo_ido -1;
    }

public:
    Bomba(int hatralevoIdo) : hatralevo_ido(hatralevoIdo) {}

    bool felrobbant() {
        return this->hatralevo_ido <= 0;
    }

    const bool felrobbant() const {
        return this->hatralevo_ido <= 0;
    }

    Bomba& operator--() {
        this->dec();
        return *this;
    }

    Bomba operator--(int) {
        Bomba regi = *this;
        this->dec();
        return regi;
    }

    const Bomba& operator--() const {
        this->dec();
        return *this;
    }

    const Bomba operator--(int) const {
        Bomba regi = *this;
        this->dec();
        return regi;
    }
};