#include <gtest/gtest.h>

#define main main_0
#define private public
#define class struct
#include "../src/bomba.cpp"
#undef private
#undef class
#undef main

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Bomba b(7);
    ASSERT_EQ(b.hatralevo_ido, 7);
    ASSERT_FALSE(b.felrobbant());
    Bomba b2 = --b;
    ASSERT_EQ(b.hatralevo_ido, 6);
    ASSERT_EQ(b2.hatralevo_ido, 6);
    ASSERT_FALSE(b.felrobbant());
    const Bomba b3 = --b;
    ASSERT_EQ(b.hatralevo_ido, 5);
    ASSERT_EQ(b3.hatralevo_ido, 5);
    ASSERT_FALSE(b.felrobbant());
    ASSERT_FALSE(b3.felrobbant());
    Bomba b4 = b--;
    ASSERT_EQ(b.hatralevo_ido, 4);
    ASSERT_EQ(b4.hatralevo_ido, 5);
    ASSERT_FALSE(b.felrobbant());
    ASSERT_FALSE(b4.felrobbant());
    Bomba b5 = b--;
    ASSERT_EQ(b.hatralevo_ido, 3);
    ASSERT_EQ(b5.hatralevo_ido, 4);
    ASSERT_FALSE(b.felrobbant());
    ASSERT_FALSE(b5.felrobbant());
    b--;
    ASSERT_FALSE(b.felrobbant());
    b--;
    ASSERT_FALSE(b.felrobbant());
    ASSERT_EQ(b.hatralevo_ido, 1);
    b--;
    ASSERT_EQ(b.hatralevo_ido, 0);
    ASSERT_TRUE(b.felrobbant());
    b--;
    ASSERT_TRUE(b.felrobbant());
    --b;
    ASSERT_TRUE(b.felrobbant());
    --b;
    ASSERT_TRUE(b.felrobbant());
    b--;
    ASSERT_EQ(b.hatralevo_ido, 0);
    ASSERT_TRUE(b.felrobbant());

    const Bomba bbb(4);
    bbb.felrobbant();
    ASSERT_FALSE(bbb.felrobbant());

    Bomba s(5);
    Bomba ss = ((s--)--)--;
    ASSERT_EQ(s.hatralevo_ido, 4);
    ASSERT_EQ(ss.hatralevo_ido, 5);

    Bomba t(4);
    Bomba tt = --(--(--t));
    ASSERT_EQ(t.hatralevo_ido, 1);
    ASSERT_EQ(tt.hatralevo_ido, 1);
}