class Hoember {
    unsigned int meret;

public:
    Hoember(unsigned int meret): meret(meret) {}

    Hoember& operator++(){
        this->meret += 1;
        return *this;
    }

    Hoember& operator--(){
        if (this->meret >= 1) {
            this->meret-= 1;
        }
        return *this;
    }
};