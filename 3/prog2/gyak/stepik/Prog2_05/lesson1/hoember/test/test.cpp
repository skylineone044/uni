#include <gtest/gtest.h>
#include <string>

using namespace std;

#define class struct
#define private public
#define main main_0
#include "../src/hoember.cpp"
#undef main
#undef private
#undef class

TEST(pluszplusz, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Hoember h(5);

    ++h;
    ASSERT_EQ(h.meret, 6);
    ++++++h;
    ASSERT_EQ(h.meret, 9);

    Hoember h2 = ++h;
    ASSERT_EQ(h.meret, 10);
    ASSERT_EQ(h2.meret, 10);
}

TEST(minuszminusz, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Hoember h(5);

    --h;
    ASSERT_EQ(h.meret, 4);
    ------h;
    ASSERT_EQ(h.meret, 1);

    Hoember h2 = --h;
    ASSERT_EQ(h.meret, 0);
    ASSERT_EQ(h2.meret, 0);

    --h;
    ASSERT_EQ(h.meret, 0);

    ------h;
    ASSERT_EQ(h.meret, 0);
}