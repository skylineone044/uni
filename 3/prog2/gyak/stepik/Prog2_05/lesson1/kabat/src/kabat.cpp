#include <iostream>
#include <string>

using namespace std;

class Kabat {
    string marka;
    string szin;
    int ar;

public:
    Kabat(const string& marka, const string& szin, int ar): marka(marka), szin(szin), ar(ar) {}

    friend ostream& operator<<(ostream& os, Kabat k);
};

ostream& operator<<(ostream& os, Kabat k) {
    os << k.marka << " " << k.szin << " " << k.ar;
    return os;
}