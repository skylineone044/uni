#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/kabat.cpp"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    stringstream ss;

    Kabat k1("asd", "valami", 51);
    ss << k1;

    ASSERT_EQ(ss.str(), "asd valami 51");
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    stringstream ss;

    Kabat k1("aaa", "bbb", 51);
    Kabat k2("medve", "kecske", 400);

    ss << k1 << "---" << k2;

    ASSERT_EQ(ss.str(), "aaa bbb 51---medve kecske 400");
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    stringstream ss;

    Kabat k1("negy", "ot", 100);
    ss << k1;

    ASSERT_EQ(ss.str(), "negy ot 100");
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    stringstream ss;

    Kabat k1("asd", "valami", 51);
    Kabat k2("lll", "mmm", 0);
    ss << "egy" << k1 << k2;

    ASSERT_EQ(ss.str(), "egyasd valami 51lll mmm 0");
}