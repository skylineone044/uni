#include <gtest/gtest.h>

#define main main_0
#include "../src/kacsa.cpp"
#undef main

TEST(Test, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Kacsa k1("a", 5, nullptr, nullptr);
    Kacsa k2("b", 5, nullptr, nullptr);
    Kacsa k3("c", 5, &k1, &k2);
    Kacsa k4("d", 5, &k3, nullptr);
    Kacsa k5("e", 5, nullptr, &k1);
    Kacsa k6("f", 5, &k5, &k4);
    Kacsa k7("g", 5, &k2, &k6);
    Kacsa k8("h", 5, &k7, nullptr);
    Kacsa k9("i", 5, nullptr, nullptr);
    Kacsa k10("j", 5, nullptr, nullptr);
    Kacsa k11("k", 5, &k9, &k10);
    const Kacsa k12("l", 5, nullptr, nullptr);
    const Kacsa k13("m", 5, nullptr, &k12);
    const Kacsa k14("n", 5, nullptr, nullptr);
    const Kacsa k15("o", 5, &k8, &k13);

    ASSERT_TRUE(k15 >> k13);
    ASSERT_TRUE(k15 >> k12);
    ASSERT_TRUE(k15 >> k6);
    ASSERT_TRUE(k15 >> k7);
    ASSERT_TRUE(k15 >> k2);
    ASSERT_TRUE(k15 >> k5);
    ASSERT_FALSE(k15 >> k14);
    ASSERT_FALSE(k15 >> k10);
    ASSERT_FALSE(k15 >> k15);

    ASSERT_FALSE(k12 >> k13);
    ASSERT_FALSE(k12 >> k1);
    ASSERT_FALSE(k12 >> k7);
    ASSERT_FALSE(k12 >> k8);

    ASSERT_FALSE(k14 >> k14);
    ASSERT_FALSE(k14 >> k12);
    ASSERT_FALSE(k14 >> k1);

    ASSERT_TRUE(k13 >> k12);
    ASSERT_TRUE(k6 >> k4);
    ASSERT_TRUE(k6 >> k3);
    ASSERT_TRUE(k6 >> k2);
    ASSERT_TRUE(k6 >> k2);
    ASSERT_TRUE(k7 >> k2);

    ASSERT_TRUE(k5 >> k1);
    ASSERT_FALSE(k5 >> k2);

    ASSERT_FALSE(k6 >> k7);
    ASSERT_FALSE(k8 >> k11);
    ASSERT_FALSE(k15 >> k14);
    ASSERT_TRUE(k6 >> k2);
}

TEST(Test, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Kacsa k1("a", 5, nullptr, nullptr);
    const Kacsa k2("a", 5, nullptr, nullptr);
    const Kacsa k3("a", 5, &k1, &k2);
    const Kacsa k4("a", 5, &k3, nullptr);
    const Kacsa k5("a", 5, nullptr, &k1);
    const Kacsa k6("a", 5, &k5, &k4);
    const Kacsa k7("a", 5, &k2, &k6);
    const Kacsa k8("a", 5, &k7, nullptr);
    const Kacsa k9("a", 5, nullptr, nullptr);
    Kacsa k10("a", 5, nullptr, nullptr);
    Kacsa k11("a", 5, &k9, &k10);
    Kacsa k12("a", 5, nullptr, nullptr);
    Kacsa k13("a", 5, nullptr, &k12);
    Kacsa k14("a", 5, nullptr, nullptr);
    Kacsa k15("a", 5, &k8, &k13);

    ASSERT_FALSE(k5 << k3);
    ASSERT_FALSE(k5 << k13);
    ASSERT_FALSE(k5 << k12);
    ASSERT_FALSE(k5 << k10);
    ASSERT_FALSE(k5 << k14);
    ASSERT_FALSE(k5 << k4);

    ASSERT_TRUE(k5 << k6);
    ASSERT_TRUE(k5 << k8);
    ASSERT_TRUE(k5 << k15);

    ASSERT_FALSE(k1 << k2);
    ASSERT_TRUE(k1 << k3);
    ASSERT_TRUE(k1 << k4);
    ASSERT_TRUE(k1 << k6);
    ASSERT_TRUE(k1 << k15);
    ASSERT_FALSE(k15 << k13);

    ASSERT_FALSE(k14 << k14);
    ASSERT_FALSE(k14 << k15);
    ASSERT_FALSE(k14 << k1);
    ASSERT_FALSE(k14 << k9);

    ASSERT_FALSE(k9 << k13);
    ASSERT_FALSE(k9 << k10);
    ASSERT_TRUE(k9 << k11);

    ASSERT_FALSE(k11 << k13);
    ASSERT_FALSE(k11 << k9);
    ASSERT_FALSE(k11 << k1);

    ASSERT_TRUE(k7 << k8);
    ASSERT_TRUE(k7 << k15);
    ASSERT_FALSE(k7 << k7);
    ASSERT_FALSE(k7 << k6);

    ASSERT_TRUE(k4 << k6);
    ASSERT_TRUE(k4 << k15);
    ASSERT_FALSE(k4 << k3);
    ASSERT_FALSE(k4 << k10);
}

TEST(Test, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Kacsa k1("a", 5, nullptr, nullptr);
    const Kacsa k2("a", 5, nullptr, nullptr);
    const Kacsa k3("a", 5, &k1, &k2);
    const Kacsa k4("a", 5, &k3, nullptr);
    const Kacsa k5("a", 5, nullptr, &k1);
    const Kacsa k6("a", 5, &k5, &k4);
    const Kacsa k7("a", 5, &k2, &k6);
    const Kacsa k8("a", 5, &k7, nullptr);
    const Kacsa k9("a", 5, nullptr, nullptr);
    Kacsa k10("a", 5, nullptr, nullptr);
    Kacsa k11("a", 5, &k9, &k10);
    Kacsa k12("a", 5, nullptr, nullptr);
    Kacsa k13("a", 5, nullptr, &k12);
    Kacsa k14("a", 5, nullptr, nullptr);
    Kacsa k15("a", 5, &k8, &k13);

    ASSERT_TRUE(k11 ^ k11);
    ASSERT_FALSE(k11 ^ k10);
    ASSERT_FALSE(k9 ^ k10);

    ASSERT_FALSE(k3 ^ k13);
    ASSERT_FALSE(k1 ^ k2);
    ASSERT_FALSE(k1 ^ k15);
    ASSERT_FALSE(k5 ^ k2);
    ASSERT_FALSE(k5 ^ k14);
    ASSERT_FALSE(k14 ^ k5);
    ASSERT_FALSE(k13 ^ k11);
    ASSERT_FALSE(k15 ^ k1);
    ASSERT_FALSE(k15 ^ k11);

    ASSERT_TRUE(k8 ^ k7);
    ASSERT_TRUE(k8 ^ k5);
    ASSERT_TRUE(k8 ^ k15);
    ASSERT_TRUE(k15 ^ k7);
    ASSERT_TRUE(k5 ^ k3);
    ASSERT_TRUE(k6 ^ k4);
    ASSERT_TRUE(k7 ^ k6);
}


TEST(Test, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Kacsa k1("a", 5, nullptr, nullptr);
    Kacsa k2("a", 5, nullptr, nullptr);
    const Kacsa k3("a", 4, &k1, &k2);
    const Kacsa k4("a", 7, &k3, nullptr);

    ASSERT_FALSE(k1 < k2);
    ASSERT_FALSE(k1 < k1);
    ASSERT_FALSE(k1 > k1);
    ASSERT_FALSE(k1 > k2);

    ASSERT_FALSE(k3 > k4);
    ASSERT_FALSE(k4 < k3);

    ASSERT_TRUE(k3 < k4);
    ASSERT_TRUE(k4 > k3);

    ASSERT_TRUE(k3 < k2);
    ASSERT_TRUE(k2 > k3);
}