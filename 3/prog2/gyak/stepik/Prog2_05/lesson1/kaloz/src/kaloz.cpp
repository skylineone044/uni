class Kaloz {
    unsigned int erosseg = 0;

public:
    Kaloz() = default;

    Kaloz(unsigned int erosseg): erosseg(erosseg) {}

    const unsigned int get_erosseg() const {
        return erosseg;
    }
};

class Hajo {
    Kaloz kalozok[10];
    unsigned darab;

public:
    Hajo(): darab(0) {}

    Hajo& operator+=(const Kaloz& uj) {
        kalozok[darab++] = uj;
        return *this;
    }

    const Kaloz* legjobb_kaloz() {
        if (darab == 0) {
            return nullptr;
        }
        Kaloz a = Kaloz(0);
        Kaloz* legerosebb = &a;
        for (int i = 0; i < darab; ++i) {
            if (this->kalozok[i].get_erosseg() > legerosebb->get_erosseg()) {
                legerosebb = &kalozok[i];
            }
        }
        for (int i = 0; i < darab; ++i) {
            if (this->kalozok[i].get_erosseg() == legerosebb->get_erosseg() && legerosebb != &this->kalozok[i]) {
                return nullptr;
            }
        }
        if (legerosebb == &a) {
            return nullptr;
        } else {
            return legerosebb;
        }
    }
};