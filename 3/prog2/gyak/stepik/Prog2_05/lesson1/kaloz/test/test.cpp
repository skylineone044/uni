#include <gtest/gtest.h>
#include <string>

using namespace std;

#define class struct
#define private public
#define main main_0
#include "../src/kaloz.cpp"
#undef main
#undef private
#undef class

const Kaloz k1(1);
const Kaloz k2(4);
const Kaloz k3(4);
const Kaloz k4(7);
const Kaloz k5(10);
const Kaloz k6(15);
const Kaloz k7(22);
const Kaloz k8(22);
const Kaloz k9(22);

Hajo& operator,(Hajo& h, const Kaloz& k) {
    h += k;
    return h;
}

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Hajo h1; h1 += k1, k2, k3, k4;
    Kaloz* kalozok = h1.kalozok;
    const Kaloz* eredmeny = h1.legjobb_kaloz();
    ASSERT_EQ(&kalozok[3], eredmeny);
    ASSERT_EQ(eredmeny->erosseg, 7);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Hajo h1; h1 += k1;
    Kaloz* kalozok = h1.kalozok;
    const Kaloz* eredmeny = h1.legjobb_kaloz();
    ASSERT_EQ(&kalozok[0], eredmeny);
    ASSERT_EQ(eredmeny->erosseg, 1);
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Hajo h1; h1 += k2, k3;
    const Kaloz* eredmeny = h1.legjobb_kaloz();
    ASSERT_EQ(nullptr, eredmeny);
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Hajo h1; h1 += k7, k8, k9;
    const Kaloz* eredmeny = h1.legjobb_kaloz();
    ASSERT_EQ(nullptr, eredmeny);
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Hajo h1; h1 += k2, k1, k3;
    const Kaloz* eredmeny = h1.legjobb_kaloz();
    ASSERT_EQ(nullptr, eredmeny);
}

TEST(Teszt, 06) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Hajo h1; h1 += k3, k2, k1;
    const Kaloz* eredmeny = h1.legjobb_kaloz();
    ASSERT_EQ(nullptr, eredmeny);
}

TEST(Teszt, 07) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Hajo h1; h1 += k2, k3, k4, k5, k6;
    Kaloz* kalozok = h1.kalozok;
    const Kaloz* eredmeny = h1.legjobb_kaloz();
    ASSERT_EQ(&kalozok[4], eredmeny);
    ASSERT_EQ(eredmeny->erosseg, 15);
}

TEST(Teszt, 08) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Hajo h1; h1 += k7, k8, k9, k1, k2, k3, k4, k5;
    const Kaloz* eredmeny = h1.legjobb_kaloz();
    ASSERT_EQ(nullptr, eredmeny);
}