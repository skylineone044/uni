class Complex {
    int real;
    int imag;

public:
    Complex(int real, int imag): real(real), imag(imag) {}

    Complex operator-() const {
        return Complex(-this->real, -this->imag);
    }
};