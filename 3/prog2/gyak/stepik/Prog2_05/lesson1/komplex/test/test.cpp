#include <gtest/gtest.h>
#include <string>

using namespace std;

#define class struct
#define private public
#define main main_0
#include "../src/komplex.cpp"
#undef main
#undef private
#undef class

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Complex c1(5, 7);
    Complex c2 = -c1;

    ASSERT_EQ(c1.real, 5);
    ASSERT_EQ(c1.imag, 7);
    ASSERT_EQ(c2.real, -5);
    ASSERT_EQ(c2.imag, -7);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Complex c1(0, 204);
    Complex c2 = -c1;

    ASSERT_EQ(c1.real, 0);
    ASSERT_EQ(c1.imag, 204);
    ASSERT_EQ(c2.real, 0);
    ASSERT_EQ(c2.imag, -204);
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Complex c1(-2, -11);
    Complex c2 = -c1;

    ASSERT_EQ(c1.real, -2);
    ASSERT_EQ(c1.imag, -11);
    ASSERT_EQ(c2.real, 2);
    ASSERT_EQ(c2.imag, 11);
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Complex c1(-103, 511);
    Complex c2 = -c1;

    ASSERT_EQ(c1.real, -103);
    ASSERT_EQ(c1.imag, 511);
    ASSERT_EQ(c2.real, 103);
    ASSERT_EQ(c2.imag, -511);
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Complex c1(0, 0);
    const Complex c2 = -c1;

    ASSERT_EQ(c1.real, 0);
    ASSERT_EQ(c1.imag, 0);
    ASSERT_EQ(c2.real, 0);
    ASSERT_EQ(c2.imag, 0);
}