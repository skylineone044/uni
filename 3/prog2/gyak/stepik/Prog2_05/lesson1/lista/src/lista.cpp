#include <string>

using namespace std;

class Lista {
    string lista;

    Lista& operator+=(string s) {
        if (this->lista.length() != 0) {
            this->lista+=", " + s;
        } else {
            this->lista = s;
        }
        return *this;
    }
    friend string& operator+=(string& s, Lista l);
};

string& operator+=(string& s, Lista l) {
    if (s.length() != 0) {
        if (l.lista.length() > 0) {
            s+=", " + l.lista;
        }
    } else {
        s = l.lista;
    }
    return s;
}