#include <gtest/gtest.h>
#include <string>

using namespace std;

#define class struct
#define private public
#define main main_0
#include "../src/lista.cpp"
#undef main
#undef private
#undef class

TEST(lista_string, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Lista lista;
    lista += "medve";
    ASSERT_EQ(lista.lista, "medve");

    lista += "oroszlan";
    ASSERT_EQ(lista.lista, "medve, oroszlan");

    lista += "kecske";
    ASSERT_EQ(lista.lista, "medve, oroszlan, kecske");
}

TEST(lista_string, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Lista lista;
    lista += "kalkulus";
    ASSERT_EQ(lista.lista, "kalkulus");

    lista += "logika";
    ASSERT_EQ(lista.lista, "kalkulus, logika");

    Lista lista2 = lista += "fonya";
    ASSERT_EQ(lista.lista, "kalkulus, logika, fonya");
    ASSERT_EQ(lista2.lista, "kalkulus, logika, fonya");
}

TEST(string_lista, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Lista lista;
    string s = "cica";

    s += lista;
    ASSERT_EQ(s, "cica");
    ASSERT_EQ(lista.lista, "");

    lista.lista = "kecske";
    s += lista;
    ASSERT_EQ(s, "cica, kecske");
    ASSERT_EQ(lista.lista, "kecske");
}

TEST(string_lista, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Lista lista;
    string s = "lovacska";

    lista.lista = "kecske, tehen";
    s += lista;
    ASSERT_EQ(s, "lovacska, kecske, tehen");
    ASSERT_EQ(lista.lista, "kecske, tehen");

    s = "";
    s += lista;
    ASSERT_EQ(s, "kecske, tehen");
    ASSERT_EQ(lista.lista, "kecske, tehen");

    const Lista l2 = lista;
    s += l2;
    ASSERT_EQ(s, "kecske, tehen, kecske, tehen");
    ASSERT_EQ(l2.lista, "kecske, tehen");
}