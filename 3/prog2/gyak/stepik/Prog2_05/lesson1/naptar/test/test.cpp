#include <gtest/gtest.h>

#define class struct
#define private public
#define main main_0
#include "../src/naptar.cpp"
#undef main
#undef class
#undef private

bool oldal_eq(Naptaroldal n1, Naptaroldal n2) {
    return n1.ev_sorszam == n2.ev_sorszam && n1.honap_sorszam == n2.honap_sorszam && n1.kep_sorszam == n2.kep_sorszam;
}

Naptar create_naptar(int evszam, Naptaroldal* oldalak, int oldal_db) {
    Naptar n(evszam);
    for (int i = 0; i < oldal_db; i++) {
        oldalak[i].ev_sorszam = evszam;
        n += oldalak[i];
    }
    return n;
}

TEST(honapszam, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ(HONAP_SZAM, 12);
}

TEST(Naptaroldal, konstruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Naptaroldal n(5, 7);
    const Naptaroldal n2(6, 2);

    ASSERT_EQ(n.kep_sorszam, 5);
    ASSERT_EQ(n.honap_sorszam, 7);
    ASSERT_EQ(n2.kep_sorszam, 6);
    ASSERT_EQ(n2.honap_sorszam, 2);
}

TEST(Naptaroldal, string_konverzio) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Naptaroldal n1(2, 1);
    Naptaroldal n2(2, 2);
    Naptaroldal n3(2, 3);
    Naptaroldal n4(2, 4);
    const Naptaroldal n5(2, 5);
    Naptaroldal n6(2, 6);
    Naptaroldal n7(2, 7);
    const Naptaroldal n8(2, 8);
    Naptaroldal n9(2, 9);
    Naptaroldal n10(2, 10);
    Naptaroldal n11(2, 11);
    Naptaroldal n12(2, 12);

    ASSERT_EQ((string) n4, "aprilis");
    ASSERT_EQ((string) n1, "januar");
    ASSERT_EQ((string) n2, "februar");
    ASSERT_EQ((string) n5, "majus");
    ASSERT_EQ((string) n10, "oktober");
    ASSERT_EQ((string) n6, "junius");
    ASSERT_EQ((string) n7, "julius");
    ASSERT_EQ((string) n4, "aprilis");
    ASSERT_EQ((string) n11, "november");
    ASSERT_EQ((string) n8, "augusztus");
    ASSERT_EQ((string) n9, "szeptember");
    ASSERT_EQ((string) n3, "marcius");
    ASSERT_EQ((string) n12, "december");
}

TEST(Naptar, konstruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Naptar n(1999);
    ASSERT_EQ(n.evszam, 1999);

    Naptar n2(2022);
    ASSERT_EQ(n2.evszam, 2022);
}

TEST(Naptar, plusz_egyenlo_oldal) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Naptaroldal n0;
    Naptaroldal n1(1313155, 3); n1.ev_sorszam = 2000;
    Naptaroldal n2(5325253, 3); n2.ev_sorszam = 2001;
    Naptaroldal n3(4124111, 3); n3.ev_sorszam = 2000;
    Naptaroldal n4(111111, 12); n4.ev_sorszam = 2000;

    Naptar naptar(2000);
    naptar += n2;
    ASSERT_FALSE(oldal_eq(n2, naptar.oldalak[2]));

    naptar += n1;
    ASSERT_TRUE(oldal_eq(n1, naptar.oldalak[2]));

    naptar += n3;
    ASSERT_TRUE(oldal_eq(n1, naptar.oldalak[2]));

    naptar += n4;
    ASSERT_TRUE(oldal_eq(n1, naptar.oldalak[2]));
    ASSERT_TRUE(oldal_eq(n4, naptar.oldalak[11]));

    (naptar += n4) += n4;
}

TEST(Naptar, plusz_egyenlo_naptar) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Naptaroldal o1a(0, 2); o1a.ev_sorszam = 2000;
    Naptaroldal o1b(0, 4); o1b.ev_sorszam = 2000;
    Naptaroldal o1c(0, 7); o1c.ev_sorszam = 2000;

    Naptaroldal o2a(0, 5); o2a.ev_sorszam = 2000;
    Naptaroldal o2b(0, 12); o2b.ev_sorszam = 2000;

    Naptaroldal o3a(0, 1); o3a.ev_sorszam = 2001;

    Naptaroldal o4a(0, 2); o4a.ev_sorszam = 2020;
    Naptaroldal o4b(0, 4); o4b.ev_sorszam = 2020;
    Naptaroldal o4c(0, 7); o4c.ev_sorszam = 2020;

    Naptaroldal o5a(40, 3); o5a.ev_sorszam = 2020;
    Naptaroldal o5b(2, 7); o5b.ev_sorszam = 2020;


    int ev1 = 2000; int om1 = 3; Naptaroldal o1[] = {o1a, o1b, o1c};
    int ev2 = 2000; int om2 = 2; Naptaroldal o2[] = {o2a, o2b};
    int ev3 = 2001; int om3 = 1; Naptaroldal o3[] = {o3a};
    int ev4 = 2020; int om4 = 3; Naptaroldal o4[] = {o4a, o4b, o4c};
    int ev5 = 2020; int om5 = 2; Naptaroldal o5[] = {o5a, o5b};

    Naptar n1 = create_naptar(ev1, o1, om1);
    const Naptar n2 = create_naptar(ev2, o2, om2);
    Naptar n3 = create_naptar(ev3, o3, om3);
    Naptar n4 = create_naptar(ev4, o4, om4);
    Naptar n5 = create_naptar(ev5, o5, om5);

    n1 += n2;
    ASSERT_TRUE(oldal_eq(n1.oldalak[1], o1a));
    ASSERT_TRUE(oldal_eq(n1.oldalak[3], o1b));
    ASSERT_TRUE(oldal_eq(n1.oldalak[6], o1c));
    ASSERT_TRUE(oldal_eq(n1.oldalak[4], o2a));
    ASSERT_TRUE(oldal_eq(n1.oldalak[11], o2b));

    n1 += n3;
    ASSERT_TRUE(oldal_eq(n1.oldalak[1], o1a));
    ASSERT_TRUE(oldal_eq(n1.oldalak[3], o1b));
    ASSERT_TRUE(oldal_eq(n1.oldalak[6], o1c));
    ASSERT_TRUE(oldal_eq(n1.oldalak[4], o2a));
    ASSERT_TRUE(oldal_eq(n1.oldalak[11], o2b));
    ASSERT_FALSE(oldal_eq(n1.oldalak[0], o3a));

    n4 += n5;
    ASSERT_TRUE(oldal_eq(n4.oldalak[1], o4a));
    ASSERT_TRUE(oldal_eq(n4.oldalak[3], o4b));
    ASSERT_TRUE(oldal_eq(n4.oldalak[6], o4c));
    ASSERT_FALSE(oldal_eq(n4.oldalak[2], o5a));
    ASSERT_FALSE(oldal_eq(n4.oldalak[2], o5b));
    ASSERT_FALSE(oldal_eq(n4.oldalak[3], o5a));
    ASSERT_FALSE(oldal_eq(n4.oldalak[3], o5b));

    (n4 += n5) += n2;
}

TEST(Naptar, minusz_egyenlo) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Naptaroldal o1a(0, 2); o1a.ev_sorszam = 2000;
    Naptaroldal o1b(0, 4); o1b.ev_sorszam = 2000;
    Naptaroldal o1c(0, 7); o1c.ev_sorszam = 2000;

    int ev1 = 2000; int om1 = 3; Naptaroldal o1[] = {o1a, o1b, o1c};

    Naptar n1 = create_naptar(ev1, o1, om1);

    n1 -= 12;
    ASSERT_TRUE(oldal_eq(n1.oldalak[1], o1a));
    ASSERT_TRUE(oldal_eq(n1.oldalak[3], o1b));
    ASSERT_TRUE(oldal_eq(n1.oldalak[6], o1c));

    n1 -= 1;
    ASSERT_TRUE(oldal_eq(n1.oldalak[1], o1a));
    ASSERT_TRUE(oldal_eq(n1.oldalak[3], o1b));
    ASSERT_TRUE(oldal_eq(n1.oldalak[6], o1c));

    n1 -= 0;
    ASSERT_TRUE(oldal_eq(n1.oldalak[1], o1a));
    ASSERT_TRUE(oldal_eq(n1.oldalak[3], o1b));
    ASSERT_TRUE(oldal_eq(n1.oldalak[6], o1c));

    n1 -= 100000000;
    ASSERT_TRUE(oldal_eq(n1.oldalak[1], o1a));
    ASSERT_TRUE(oldal_eq(n1.oldalak[3], o1b));
    ASSERT_TRUE(oldal_eq(n1.oldalak[6], o1c));

    n1 -= 2;
    ASSERT_FALSE(oldal_eq(n1.oldalak[1], o1a));
    ASSERT_TRUE(oldal_eq(n1.oldalak[3], o1b));
    ASSERT_TRUE(oldal_eq(n1.oldalak[6], o1c));

    n1 -= 2;
    ASSERT_FALSE(oldal_eq(n1.oldalak[1], o1a));
    ASSERT_TRUE(oldal_eq(n1.oldalak[3], o1b));
    ASSERT_TRUE(oldal_eq(n1.oldalak[6], o1c));

    n1 -= 4;
    ASSERT_FALSE(oldal_eq(n1.oldalak[1], o1a));
    ASSERT_FALSE(oldal_eq(n1.oldalak[3], o1b));
    ASSERT_TRUE(oldal_eq(n1.oldalak[6], o1c));

    ((n1 -= 7) -= 5) -= 3;
}

TEST(Naptar, tombindex_operator) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Naptaroldal o1a(0, 3); o1a.ev_sorszam = 2000;
    Naptaroldal o1b(0, 5); o1b.ev_sorszam = 2000;
    Naptaroldal o1c(0, 8); o1c.ev_sorszam = 2000;

    int ev1 = 2000; int om1 = 3; Naptaroldal o1[] = {o1a, o1b, o1c};

    Naptar n1 = create_naptar(ev1, o1, om1);

    ASSERT_TRUE(oldal_eq(n1[3], o1a));
    ASSERT_TRUE(oldal_eq(n1[5], o1b));
    ASSERT_TRUE(oldal_eq(n1[8], o1c));
}

TEST(Naptar, plusz_plusz) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Naptaroldal o1a(0, 3); o1a.ev_sorszam = 2000;
    Naptaroldal o1b(0, 5); o1b.ev_sorszam = 2000;
    Naptaroldal o1c(0, 8); o1c.ev_sorszam = 2000;

    int ev1 = 2000; int om1 = 3; Naptaroldal o1[] = {o1a, o1b, o1c};

    Naptar n1 = create_naptar(ev1, o1, om1);
    n1++;
    ++n1;

    o1a.ev_sorszam = 2002;
    o1b.ev_sorszam = 2002;
    o1c.ev_sorszam = 2002;

    ASSERT_EQ(n1.evszam, 2002);
    ASSERT_TRUE(oldal_eq(n1[3], o1a));
    ASSERT_TRUE(oldal_eq(n1[5], o1b));
    ASSERT_TRUE(oldal_eq(n1[8], o1c));

    Naptar n2 = ++n1;

    o1a.ev_sorszam = 2003;
    o1b.ev_sorszam = 2003;
    o1c.ev_sorszam = 2003;

    ASSERT_EQ(n1.evszam, 2003);
    ASSERT_TRUE(oldal_eq(n1[3], o1a));
    ASSERT_TRUE(oldal_eq(n1[5], o1b));
    ASSERT_TRUE(oldal_eq(n1[8], o1c));


    ASSERT_EQ(n2.evszam, 2003);
    ASSERT_TRUE(oldal_eq(n2[3], o1a));
    ASSERT_TRUE(oldal_eq(n2[5], o1b));
    ASSERT_TRUE(oldal_eq(n2[8], o1c));

    Naptar n3 = n1++;
    ASSERT_EQ(n3.evszam, 2003);
    ASSERT_TRUE(oldal_eq(n3[3], o1a));
    ASSERT_TRUE(oldal_eq(n3[5], o1b));
    ASSERT_TRUE(oldal_eq(n3[8], o1c));

    (n1++)++;
    ++(++n1);
}

TEST(Naptar, minusz_minusz) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Naptaroldal o1a(0, 3); o1a.ev_sorszam = 2000;
    Naptaroldal o1b(0, 5); o1b.ev_sorszam = 2000;
    Naptaroldal o1c(0, 8); o1c.ev_sorszam = 2000;

    int ev1 = 2000; int om1 = 3; Naptaroldal o1[] = {o1a, o1b, o1c};

    Naptar n1 = create_naptar(ev1, o1, om1);
    n1--;
    --n1;

    o1a.ev_sorszam = 1998;
    o1b.ev_sorszam = 1998;
    o1c.ev_sorszam = 1998;

    ASSERT_EQ(n1.evszam, 1998);
    ASSERT_TRUE(oldal_eq(n1[3], o1a));
    ASSERT_TRUE(oldal_eq(n1[5], o1b));
    ASSERT_TRUE(oldal_eq(n1[8], o1c));

    Naptar n2 = --n1;

    o1a.ev_sorszam = 1997;
    o1b.ev_sorszam = 1997;
    o1c.ev_sorszam = 1997;

    ASSERT_EQ(n1.evszam, 1997);
    ASSERT_TRUE(oldal_eq(n1[3], o1a));
    ASSERT_TRUE(oldal_eq(n1[5], o1b));
    ASSERT_TRUE(oldal_eq(n1[8], o1c));


    ASSERT_EQ(n2.evszam, 1997);
    ASSERT_TRUE(oldal_eq(n2[3], o1a));
    ASSERT_TRUE(oldal_eq(n2[5], o1b));
    ASSERT_TRUE(oldal_eq(n2[8], o1c));

    Naptar n3 = n1--;
    ASSERT_EQ(n3.evszam, 1997);
    ASSERT_TRUE(oldal_eq(n3[3], o1a));
    ASSERT_TRUE(oldal_eq(n3[5], o1b));
    ASSERT_TRUE(oldal_eq(n3[8], o1c));

    (n1--)--;
    --(--n1);
}

TEST(Naptar, egesz_konverzio) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ASSERT_EQ((unsigned) Naptar(2021), 365);
    ASSERT_EQ((unsigned) Naptar(2020), 366);
    ASSERT_EQ((unsigned) Naptar(2012), 366);
    ASSERT_EQ((unsigned) Naptar(2001), 365);
    ASSERT_EQ((unsigned) Naptar(2100), 365);
    ASSERT_EQ((unsigned) Naptar(1900), 365);
    ASSERT_EQ((unsigned) Naptar(1400), 365);
    ASSERT_EQ((unsigned) Naptar(1404), 366);
    ASSERT_EQ((unsigned) Naptar(2000), 366);
    ASSERT_EQ((unsigned) Naptar(2400), 366);
    ASSERT_EQ((unsigned) Naptar(1600), 366);
    ASSERT_EQ((unsigned) Naptar(1700), 365);
}

TEST(Naptar, bool_konverzio) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Naptaroldal o1a(0, 1); o1a.ev_sorszam = 2000;
    Naptaroldal o1b(0, 2); o1b.ev_sorszam = 2000;
    Naptaroldal o1c(0, 3); o1c.ev_sorszam = 2000;
    Naptaroldal o1d(0, 4); o1d.ev_sorszam = 2000;
    Naptaroldal o1e(0, 5); o1e.ev_sorszam = 2000;
    Naptaroldal o1f(0, 6); o1f.ev_sorszam = 2000;
    Naptaroldal o1h(0, 7); o1h.ev_sorszam = 2000;
    Naptaroldal o1i(0, 8); o1i.ev_sorszam = 2000;
    Naptaroldal o1j(0, 9); o1j.ev_sorszam = 2000;
    Naptaroldal o1k(0, 10); o1k.ev_sorszam = 2000;
    Naptaroldal o1l(0, 11); o1l.ev_sorszam = 2000;
    Naptaroldal o1m(0, 12); o1m.ev_sorszam = 2000;

    int ev1 = 2000;

    int om1 = 3;Naptaroldal o1[] = {o1a, o1b, o1c};
    int om2 = 4;Naptaroldal o2[] = {o1c, o1f, o1h, o1l};
    int om3 = 12;Naptaroldal o3[] = {o1a, o1b, o1c, o1d, o1e, o1f, o1h, o1i, o1j, o1k, o1l, o1m};

    Naptar n1 = create_naptar(ev1, o1, om1);
    Naptar n2 = create_naptar(ev1, o2, om2);
    Naptar n3 = create_naptar(ev1, o3, om3);

    ASSERT_FALSE((bool) n1);
    ASSERT_FALSE((bool) n2);
    ASSERT_TRUE((bool) n3);
}
