#include <gtest/gtest.h>
#include <string>

using namespace std;

#define class struct
#define private public
#define main main_0
#include "../src/parkolo.cpp"
#undef main
#undef private
#undef class

const Auto a0;
const Auto a1("AAA-111", "Mazda");
const Auto a2("HAT-789", "Suzuki");
const Auto a3("CIC-123", "Toyota");
const Auto a4("KOT-567", "Aston Martin");

bool operator==(const Auto& aa1, const Auto& aa2) {
    return aa1.rendszam == aa2.rendszam && aa1.tipus == aa2.tipus;
}

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Parkolo p1;
    p1.autok[0] = a1;
    p1.autok[1] = a2;
    p1.autok[2] = a3;
    p1.autok[3] = a4;

    const Auto* autok = p1.get_autok();
    ASSERT_EQ(autok[0], a1);
    ASSERT_EQ(autok[1], a2);
    ASSERT_EQ(autok[2], a3);
    ASSERT_EQ(autok[3], a4);
    ASSERT_EQ(autok[4], a0);
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Parkolo p1;

    const Auto* autok = p1.get_autok();
    ASSERT_EQ(autok[0], a0);
    ASSERT_EQ(autok[1], a0);
    ASSERT_EQ(autok[2], a0);
    ASSERT_EQ(autok[3], a0);
    ASSERT_EQ(autok[4], a0);
    ASSERT_EQ(autok[5], a0);
    ASSERT_EQ(autok[6], a0);
    ASSERT_EQ(autok[7], a0);
    ASSERT_EQ(autok[8], a0);
    ASSERT_EQ(autok[9], a0);

    ASSERT_EQ(autok, p1.autok);
}