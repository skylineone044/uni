#include <string>

using namespace std;

class Szoba {
    mutable string vendeg;
    unsigned int sorszam;
    unsigned int meret;

public:
    Szoba() = default;

    Szoba(const string &vendeg, unsigned int sorszam, unsigned int meret) :
        vendeg(vendeg), sorszam(sorszam), meret(meret) {}

    friend class Emelet;
};

class Emelet {
    mutable Szoba szobak[4];
    int szobak_szama = 0;
    int emelet_szam;

public:
    Emelet(int emeletSzam) : emelet_szam(emeletSzam) {}

    Emelet& operator+=(Szoba sz) {
        this->szobak[szobak_szama] = sz;
        this->szobak_szama++;
        return *this;
    }

    string& operator[](int index) {
        return this->szobak[index].vendeg;
    }

    const string operator[](int index) const {
        return this->szobak[index].vendeg;
    }
};