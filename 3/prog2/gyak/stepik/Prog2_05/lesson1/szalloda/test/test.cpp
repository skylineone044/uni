#include <gtest/gtest.h>

#define class struct
#define private public
#define main main_0
#include "../src/szalloda.cpp"
#undef main
#undef private
#undef public

bool egyforma(const Szoba& s1, const Szoba& s2) {
    return s1.vendeg == s2.vendeg && s1.meret == s2.meret && s1.sorszam == s2.sorszam;
}

TEST(Emelet, konstruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Emelet e(5);
    ASSERT_EQ(e.emelet_szam, 5);

    Emelet ee(7);
    ASSERT_EQ(ee.emelet_szam, 7);
}

TEST(Emelet, hozzaadas) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Szoba s1("Marika", 1, 30);
    Szoba s2("Jani", 2, 33);
    const Szoba s3("Peti", 3, 33);
    Szoba s4("Jutka", 4, 40);
    Szoba s5("Otto", 5, 10);

    Emelet e(6);
    e += s1;
    ASSERT_TRUE(egyforma(e.szobak[0], s1));

    e += s2;
    ASSERT_TRUE(egyforma(e.szobak[0], s1));
    ASSERT_TRUE(egyforma(e.szobak[1], s2));

    e += s3;
    ASSERT_TRUE(egyforma(e.szobak[0], s1));
    ASSERT_TRUE(egyforma(e.szobak[1], s2));
    ASSERT_TRUE(egyforma(e.szobak[2], s3));

    e += s5;
    ASSERT_TRUE(egyforma(e.szobak[0], s1));
    ASSERT_TRUE(egyforma(e.szobak[1], s2));
    ASSERT_TRUE(egyforma(e.szobak[2], s3));
    ASSERT_TRUE(egyforma(e.szobak[3], s5));

    for (int i = 0; i < 10000000; i++) {
        e += s4;
    }

    Emelet e2(5);
    (e2 += s1) += s4;
    ASSERT_TRUE(egyforma(e2.szobak[0], s1));
    ASSERT_TRUE(egyforma(e2.szobak[1], s4));
}

TEST(Emelet, indexeles) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Szoba s1("Marika", 1, 30);
    Szoba s2("Jani", 2, 33);
    const Szoba s3("Peti", 3, 33);
    Szoba s4("Jutka", 4, 40);
    Szoba s5("Otto", 5, 10);

    Emelet e(1);
    e += s1;
    e += s2;
    e += s3;
    e += s4;

    ASSERT_EQ(e[0], "Marika");
    ASSERT_EQ(e[1], "Jani");
    ASSERT_EQ(e[2], "Peti");
    ASSERT_EQ(e[3], "Jutka");
    ASSERT_EQ(e[-1], "Marika");

    Emelet e2(2);
    e2 += s5;
    e2 += s4;

    ASSERT_EQ(e2[0], "Otto");
    ASSERT_EQ(e2[1], "Jutka");
    ASSERT_EQ(e2[2], "Otto");
    ASSERT_EQ(e2[3], "Otto");
    ASSERT_EQ(e2[50000000], "Otto");

    Emelet e3(3);
    e3 += s1;
    ASSERT_EQ(e3[0], "Marika");
    e3[0] = "Bendeguz";
    ASSERT_EQ(e3[0], "Bendeguz");

    e3 += s2;
    e3 += s3;
    ASSERT_EQ(e3[2], "Peti");
    e3[2] = "Eszter";
    ASSERT_EQ(e3[2], "Eszter");

    const Emelet e4 = e3;
    ASSERT_EQ(e4[0], "Bendeguz");
}