#include <gtest/gtest.h>

#define main main_0
#include "../src/telefon.cpp"
#undef main

TEST(Plusz_egyenlo, helyes_karakter) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Telefon t;
    t += '1';
    (t += '0') += '3';
    ((t += '5') += '7') += '9';
    string res = t();
    ASSERT_EQ(res, "103579");
    string res2 = t();
    ASSERT_EQ(res2, "");
}

TEST(Plusz_egyenlo, helyes_szam) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Telefon t;
    t += 9;
    (t += 2) += 5;
    ((t += 7) += 5) += 0;
    string res = t();
    ASSERT_EQ(res, "925750");
    string res2 = t();
    ASSERT_EQ(res2, "");
}

TEST(Plusz_egyenlo, helytelen_karakter) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Telefon t;
    t += '1';
    (t += 'G') += '#';
    ((t += 'b') += '?') += '9';
    t += 'a';
    string res = t();
    ASSERT_EQ(res, "19");
    string res2 = t();
    ASSERT_EQ(res2, "");
}

TEST(Plusz_egyenlo, helytelen_szam) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Telefon t;
    t += 10;
    (t += -4) += 5;
    ((t += 0) += 361231) += 0;
    string res = t();
    ASSERT_EQ(res, "500");
    string res2 = t();
    ASSERT_EQ(res2, "");
}

TEST(minusz_minusz, helyes) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Telefon t;
    t += 6;
    t += 5;
    t += 2;
    Telefon u1 = t--;
    t--;
    t += 2;
    t += 4;
    Telefon uj1 = t--;
    t += 2;
    t += 6;
    t += 0;
    Telefon u2 = --t;
    t += 7;
    t += 7;
    Telefon uj2 = --t;
    t += 5;
    t += 6;
    t--;
    --t;

    uj1 += 3;
    --u2;

    string res = t();
    ASSERT_EQ(res, "62267");

    string res2 = uj1();
    ASSERT_EQ(res2, "6243");

    string res3 = uj2();
    ASSERT_EQ(res3, "62267");

    string res4 = u1();
    ASSERT_EQ(res4, "652");

    string res5 = u2();
    ASSERT_EQ(res5, "622");
}

TEST(minusz_minusz, helytelen) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Telefon t;
    t += 6;
    t += 5;
    t += 2;
    t--;
    t--;
    t--;

    string res = t();
    ASSERT_EQ(res, "");

    t += 6;
    t += 2;
    for (int i = 0; i < 1000000; i++) {
        t--;
    }

    string res2 = t();
    ASSERT_EQ(res2, "");

    t += 7;
    string res3 = t();
    ASSERT_EQ(res3, "7");

    t += 2;
    t += 5;
    --t;
    --t;
    --t;
    --t;
    string res4 = t();
    ASSERT_EQ(res4, "");

    t += 7;
    for (int i = 0; i < 1000000; i++) {
        --t;
    }

    string res5 = t();
    ASSERT_EQ(res5, "");
}

TEST(minusz_egyenlo, helyes_parameter) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Telefon t;
    t += 6;
    t += 5;
    t += 4;
    t += 2;
    t += '5';
    t += 6;

    t -= 2;

    string res = t();
    ASSERT_EQ(res, "6542");

    t += 5;
    t += 2;
    t += 7;
    t += 4;
    t += 0;

    t -= 4;
    string res2 = t();
    ASSERT_EQ(res2, "5");
}

TEST(minusz_egyenlo, helytelen_parameter) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Telefon t;
    t += 6;
    t += 5;
    t += 4;
    t += 2;
    t += '5';
    t += 6;

    t -= 130;

    string res = t();
    ASSERT_EQ(res, "");

    t += 5;
    t += 2;
    t += 7;
    t += 4;
    t += 0;

    t -= 2100000000;
    string res2 = t();
    ASSERT_EQ(res2, "");

    t += 3;
    t -= 2100000;
    t += 5;
    string res3 = t();
    ASSERT_EQ(res3, "5");
}

TEST(nullazas, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Telefon t;
    t += 7;
    t += 2;

    Telefon t2 = ~t;
    ASSERT_EQ(t(), "");
    ASSERT_EQ(t2(), "");

    for (int i = 0; i < 10000; i++) {
        t += 2;
    }
    ~t;
    ASSERT_EQ(t(), "");

    t += 2;
    ASSERT_EQ(t(), "2");
}