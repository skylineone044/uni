#include <gtest/gtest.h>
#include <string>

using namespace std;

#define class struct
#define private public
#define main main_0
#include "../src/telefonszam.cpp"
#undef main
#undef private
#undef class

TEST(Noveles, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Weboldal w;
    w++;
    ASSERT_EQ(w.telefonszam, 1000001);
    w++;
    ASSERT_EQ(w.telefonszam, 1000002);
}

TEST(VisszateresiErtek, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Weboldal w;

    Weboldal w2 = w++;
    ASSERT_EQ(w.telefonszam, 1000001);
    ASSERT_EQ(w2.telefonszam, 1000000);

    Weboldal w3 = w++;
    ASSERT_EQ(w.telefonszam, 1000002);
    ASSERT_EQ(w3.telefonszam, 1000001);

    ASSERT_EQ(w++.telefonszam, 1000002);
    ASSERT_EQ(w.telefonszam, 1000003);
}