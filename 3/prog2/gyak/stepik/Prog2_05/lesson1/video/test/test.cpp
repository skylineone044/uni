#include <gtest/gtest.h>

#define class struct
#define private public
#define main main_0
#include "../src/video.cpp"
#undef main
#undef private
#undef class

TEST(Video, konstruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Video v("asd", "aaaaaa");
    ASSERT_EQ(v.cim, "asd");
    ASSERT_EQ(v.leiras, "aaaaaa");
    ASSERT_EQ(v.like, 0);
    ASSERT_EQ(v.dislike, 0);

    const Video v2("ddd", "cac");
    ASSERT_EQ(v2.cim, "ddd");
    ASSERT_EQ(v2.leiras, "cac");
    ASSERT_EQ(v2.like, 0);
    ASSERT_EQ(v2.dislike, 0);
}

TEST(Video, double_konverzio) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Video v("a", "a");
    v.like = 34;
    v.dislike = 66;
    ASSERT_NEAR(0.34, (double) v, 0.00001);

    v.like = 44;
    v.dislike = 44;
    ASSERT_NEAR(0.5, (double) v, 0.00001);

    v.like = 1000;
    v.dislike = 1;
    ASSERT_NEAR(0.999, (double) v, 0.00001);

    v.like = 0;
    v.dislike = 1;
    ASSERT_NEAR(0, (double) v, 0.00001);

    v.like = 42;
    v.dislike = 21;
    ASSERT_NEAR(0.66666, (double) v, 0.00001);
}

TEST(Video, unsigned_int_konverzio) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Video v("a", "A");
    v.like = 42;
    v.dislike = 33;
    ASSERT_EQ(42, (unsigned int) v);

    v.like = 777;
    v.dislike = 2;
    ASSERT_EQ(777, (unsigned int) v);

    const Video v2("a", "a");
    ASSERT_EQ(0, (unsigned int) v2);
}

TEST(Video, string_konverzio) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Video v("aaa", "bbb");
    ASSERT_EQ("aaa;bbb", (string) v);

    v.cim = "a";
    v.leiras = "qqq";
    ASSERT_EQ("a;qqq", (string) v);

    v.cim = "ava sa as";
    v.leiras = "ww af dsd s 532";
    ASSERT_EQ("ava sa as;ww af dsd s 532", (string) v);

    v.cim = "";
    v.leiras = "";
    ASSERT_EQ(";", (string) v);
}