#include <gtest/gtest.h>

#define class struct
#define private public
#define main main_0
#include "../src/weboldal.cpp"
#undef main
#undef private
#undef class

TEST(Test, Konstruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Weboldal o("asd");
    ASSERT_EQ(o.latogatok_szama, 0);
    ASSERT_EQ(o.url_cim, "asd");

    const Weboldal o2("aaa");
    ASSERT_EQ(o2.latogatok_szama, 0);
    ASSERT_EQ(o2.url_cim, "aaa");

    Weboldal o3("ccc", 30);
    ASSERT_EQ(o3.latogatok_szama, 30);
    ASSERT_EQ(o3.url_cim, "ccc");

    const Weboldal o4("qqq", 44);
    ASSERT_EQ(o4.latogatok_szama, 44);
    ASSERT_EQ(o4.url_cim, "qqq");
}

TEST(Test, operator_plusplus_prefix) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Weboldal o("asd");
    Weboldal o2 = ++o;
    ASSERT_EQ(o.url_cim, "asd");
    ASSERT_EQ(o.latogatok_szama, 1);
    ASSERT_EQ(o2.url_cim, "asd");
    ASSERT_EQ(o2.latogatok_szama, 1);

    Weboldal o3("fdff", 4);
    Weboldal o4 = ++o3;
    ASSERT_EQ(o3.url_cim, "fdff");
    ASSERT_EQ(o3.latogatok_szama, 5);
    ASSERT_EQ(o4.url_cim, "fdff");
    ASSERT_EQ(o4.latogatok_szama, 5);

    Weboldal o5("a", 5);
    ++(++(++o5));
    ASSERT_EQ(o5.latogatok_szama, 8);
}

TEST(Test, operator_plusplus_postfix) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Weboldal o("asd");
    Weboldal o2 = o++;
    ASSERT_EQ(o.url_cim, "asd");
    ASSERT_EQ(o.latogatok_szama, 1);
    ASSERT_EQ(o2.url_cim, "asd");
    ASSERT_EQ(o2.latogatok_szama, 0);

    Weboldal o3("fdff", 4);
    Weboldal o4 = o3++;
    ASSERT_EQ(o3.url_cim, "fdff");
    ASSERT_EQ(o3.latogatok_szama, 5);
    ASSERT_EQ(o4.url_cim, "fdff");
    ASSERT_EQ(o4.latogatok_szama, 4);

    Weboldal o5("a", 2);
    ((o5++)++)++;
    ASSERT_EQ(o5.latogatok_szama, 3);
}

TEST(Test, operator_felkialtojel) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Weboldal o("asd");
    bool res = !o;
    ASSERT_TRUE(res);
    ASSERT_EQ(o.latogatok_szama, 0);

    Weboldal o2("asd", 444);
    bool res2 = !o2;
    ASSERT_TRUE(res2);
    ASSERT_EQ(o2.latogatok_szama, 0);
}