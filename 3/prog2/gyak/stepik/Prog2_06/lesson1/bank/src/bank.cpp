#include <string>
#include <iostream>

using namespace std;

class Bankjegy {
    unsigned cimlet;
    unsigned ev;

public:
    Bankjegy(): cimlet(0), ev(0) {}
    Bankjegy(unsigned int cimlet, unsigned int ev): cimlet(cimlet), ev(ev) {}
    ~Bankjegy() { cout << "B"; }
};

class Penztarca {
    string tulajdonos;

    Bankjegy* bankjegyek;
    unsigned bankjegy_szam;

public:
    Penztarca(): tulajdonos(""), bankjegy_szam(0), bankjegyek(new Bankjegy[0]) {}
    Penztarca(const string& tulajdonos): tulajdonos(tulajdonos), bankjegy_szam(0), bankjegyek(new Bankjegy[0]) {}

    Penztarca& operator+=(const Bankjegy& bankjegy) {
        Bankjegy* ujak = new Bankjegy[bankjegy_szam + 1];
        for (int i = 0; i < bankjegy_szam; i++) {
            ujak[i] = bankjegyek[i];
        }

        ujak[bankjegy_szam] = bankjegy;
        bankjegy_szam++;

        delete[] bankjegyek;

        bankjegyek = ujak;

        return *this;
    }

    ~Penztarca() {
        delete[] bankjegyek;
    }

};

class Szef {
    string kod;

    Penztarca* penztarcak;
    unsigned penztarca_szam;

public:
    Szef(const string& kod): kod(kod), penztarca_szam(0), penztarcak(new Penztarca[10]) {}

    Szef& operator+=(const Penztarca& penztarca) {
        if (penztarca_szam < 10) {
            penztarcak[penztarca_szam++] = penztarca;
        }

        return *this;
    }

    virtual ~Szef() {
        delete[] penztarcak;
    }

};