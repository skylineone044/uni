#include <gtest/gtest.h>
#include <string>

using namespace std;

#define class struct
#define private public
#define main main_0
#include "../src/bank.cpp"
#undef main
#undef private
#undef class

const Bankjegy b1(1000, 1999);
const Bankjegy b2(5000, 2003);
const Bankjegy b3(10000, 2005);
const Bankjegy b4(20000, 2002);
const Bankjegy b5(20000, 2003);
const Bankjegy b6(1000, 2020);
const Bankjegy b7(500, 2017);

bool operator==(const Bankjegy& b1, const Bankjegy& b2) {
    return b1.cimlet == b2.cimlet && b1.ev == b2.ev;
}

bool operator==(const Penztarca& p1, const Penztarca& p2) {
    if (p1.bankjegy_szam != p2.bankjegy_szam || p1.tulajdonos != p2.tulajdonos) {
        return false;
    }

    for (int i = 0; i < p1.bankjegy_szam; i++) {
        if (!(p1.bankjegyek[i] == p2.bankjegyek[i])) {
            return false;
        }
    }

    return true;
}

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Penztarca p1("Marika");
    p1 += b1;
    p1 += b2;
    p1 += b3;

    Penztarca p2("Janos");
    p2 += b4;
    p2 += b5;

    Penztarca p3("Anett");
    p3 += b6;
    p3 += b7;

    Penztarca p4;

    Szef szef("1111");
    szef += p1;
    szef += p2;
    szef += p3;
    szef += p4;

    Szef szef2(szef);
    ASSERT_EQ(szef2.kod, "1111");
    ASSERT_EQ(szef2.penztarca_szam, 4);
    ASSERT_EQ(szef2.penztarcak[0], p1);
    ASSERT_EQ(szef2.penztarcak[1], p2);
    ASSERT_EQ(szef2.penztarcak[2], p3);
    ASSERT_EQ(szef2.penztarcak[3], p4);

    szef2.penztarcak[0].tulajdonos = "Gabor";
    ASSERT_EQ(szef.penztarcak[0].tulajdonos, "Marika");

    szef2.penztarcak[0].bankjegyek[0].ev = 2022;
    ASSERT_EQ(szef.penztarcak[0].bankjegyek[0].ev, 1999);

    szef2.penztarcak[2].bankjegyek[1].cimlet = 10000;
    ASSERT_EQ(szef.penztarcak[2].bankjegyek[1].cimlet, 500);

    szef2.penztarcak[3] += b1;
    szef2 += p1;
    ASSERT_EQ(szef.penztarca_szam, 4);
    ASSERT_EQ(szef2.penztarca_szam, 5);
    ASSERT_EQ(szef2.penztarcak[3].bankjegyek[0], b1);

    ASSERT_EQ(szef2.penztarcak[3].bankjegy_szam, 1);
    ASSERT_EQ(szef.penztarcak[3].bankjegy_szam, 0);
}