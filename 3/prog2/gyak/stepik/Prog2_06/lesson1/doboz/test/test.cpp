#include <gtest/gtest.h>

#define main main_0
#define class struct
#define private public
#include "../src/doboz.cpp"
#undef class
#undef private
#undef main

#include "../../tools.cpp"

Doboz d0;
Doboz da1("cipokanal alaku villa");
Doboz da2("kabat");
Doboz da3("medveriaszto uborka");
Doboz da4("parnahuzat");
Doboz da5("szamologep");
Doboz da6("kalkulus peldatar");
Doboz da7("katalogus detektor");
Doboz da8("vezetekes telefon kabel");
Doboz da9("dorges kabel");
Doboz da10("esoallo toll");
Doboz da11("felhuzhato zokni");
Doboz da12("billentyukkel rendelkezo billentyuzet");
Doboz da13("kerek parna");

bool operator==(const Ajandek& a1, const Ajandek& a2) {
    return a1.leiras == a2.leiras;
}

bool operator==(const Doboz& d1, const Doboz& d2) {
    if (d1.dobozok == nullptr && d2.dobozok == nullptr) {
        if ((d1.ajandek == nullptr && d2.ajandek != nullptr) || (d1.ajandek != nullptr && d2.ajandek == nullptr)) {
            return false;
        }

        if (d1.ajandek == nullptr && d2.ajandek == nullptr) {
            return true;
        }

        return *d1.ajandek == *d2.ajandek;
    } else if ((d1.dobozok == nullptr && d2.dobozok != nullptr) || (d1.dobozok != nullptr && d2.dobozok == nullptr)) {
        return false;
    }

    if (d1.max_dobozszam != d2.max_dobozszam) {
        return false;
    }
    
    for (int i = 0; i < d1.max_dobozszam; i++) {
        if (!(d1.dobozok[i] == d2.dobozok[i])) {
            return false;
        }
    }

    return true;
}

bool operator!=(const Doboz& d1, const Doboz& d2) {
    return !(d1 == d2);
}

TEST(Ajandek_osztaly, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Ajandek* j = new Ajandek("egerfogo");
    char str[100];
    IO("", delete j, str)
    ASSERT_STREQ(str, "egerfogo");
}

TEST(Ajandek_osztaly, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Ajandek* j = new Ajandek("nyomtatoba valo papir");
    char str[100];
    IO("", delete j, str)
    ASSERT_STREQ(str, "nyomtatoba valo papir");
}

TEST(parameteres_konstruktor, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Doboz doboz(5);
    ASSERT_NE(doboz.dobozok, nullptr);
    ASSERT_EQ(doboz.ajandek, nullptr);
}

TEST(parameteres_konstruktor, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Doboz doboz("cicajatek");
    ASSERT_EQ(doboz.dobozok, nullptr);
    ASSERT_EQ(doboz.ajandek->leiras, "cicajatek");
}

TEST(copy_konstruktor, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    // ez lesz a fo doboz
    Doboz* dd1 = new Doboz(3);

    // mellek dobozok eloallitasa
    Doboz dd0(1);
    dd0 += da1;

    Doboz dd2(2);
    dd2 += da6;
    dd2 += da5;

    // mellek dobozok hozzaadasa
    *dd1 += dd0;
    *dd1 += dd2;
    *dd1 += da2;

    // modositas
    char str[200];
    IO("", Doboz last = *dd1, str);

    dd1->max_dobozszam = 511;
    dd1->dobozok[0].max_dobozszam = 77;
    dd1->dobozok[2].ajandek->leiras = "...";

    ASSERT_EQ(last.max_dobozszam, 3);
    ASSERT_EQ(last.ajandek, nullptr);
    ASSERT_EQ(last.dobozok[0], dd0);
    ASSERT_EQ(last.dobozok[1], dd2);
    ASSERT_EQ(last.dobozok[2], da2);

    delete dd1;
}

TEST(copy_konstruktor, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    // ez lesz a fo doboz
    Doboz* dd1 = new Doboz(10);

    // mellek dobozok eloallitasa
    Doboz dd2(3);
    Doboz dd3(5);
    Doboz dd4(3);
    Doboz dd5(4);
    Doboz dd6(3);
    Doboz dd7(2);
    Doboz dd8(1);
    Doboz dd9(4);
    Doboz dd10(1);
    Doboz dd11(2);
    Doboz dd12(2);
    Doboz dd13(1);
    Doboz dd14(1);
    Doboz dd15(7);
    Doboz dd16(1);
    Doboz dd17(1);

    // mellek dobozok feltoltese
    dd7 += da3;
    dd7 += da4;
    dd5 += dd7;
    dd5 += da12;
    dd5 += da5;
    dd5 += da11;

    dd8 += dd10;
    dd6 += dd8;

    dd2 += dd5;
    dd2 += dd6;

    dd11 += da6;
    dd3 += dd11;
    dd3 += da7;
    dd3 += da8;
    dd3 += da9;
    dd3 += da10;

    dd12 += dd14;
    dd12 += dd13;

    dd16 += dd17;

    dd4 += dd12;
    dd4 += dd15;
    dd4 += dd16;

    // mellek dobozok hozzaadasa
    *dd1 += da13;
    *dd1 += dd2;
    *dd1 += dd3;
    *dd1 += dd4;
    *dd1 += dd9;
    *dd1 += da2;
    *dd1 += da1;

    char str[200];
    IO("", Doboz last = *dd1, str)
    Doboz last2 = dd3;

    dd1->max_dobozszam = 511;
    dd1->dobozok[0].max_dobozszam = 77;
    dd1->dobozok[5].ajandek->leiras = "...";

    ASSERT_EQ(last.max_dobozszam, 10);
    ASSERT_EQ(last.ajandek, nullptr);
    ASSERT_EQ(last.dobozok[0], da13);
    ASSERT_EQ(last.dobozok[1], dd2);
    ASSERT_EQ(last.dobozok[2], dd3);
    ASSERT_EQ(last.dobozok[3], dd4);
    ASSERT_EQ(last.dobozok[4], dd9);
    ASSERT_EQ(last.dobozok[5], da2);
    ASSERT_EQ(last.dobozok[6], da1);

    ASSERT_EQ(last.dobozok[6], da1);
    da1.ajandek->leiras = "cipokanal alaku kes";
    ASSERT_NE(last.dobozok[6], da1);
    da1.ajandek->leiras = "cipokanal alaku villa";

    dd3.dobozok[1].ajandek->leiras = "???";
    dd3.dobozok[0].dobozok[0].ajandek->leiras = "ures";

    ASSERT_EQ(last2.dobozok[0], dd11);
    ASSERT_EQ(last2.dobozok[1], da7);
    ASSERT_EQ(last2.dobozok[2], da8);
    ASSERT_EQ(last2.dobozok[3], da9);
    ASSERT_EQ(last2.dobozok[4], da10);

    ASSERT_STREQ(str, "");

    Doboz ddd(da1);
    ASSERT_EQ(ddd.ajandek->leiras, "cipokanal alaku villa");
}

TEST(ertekadas_operator, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    // ez lesz a fo doboz
    Doboz* dd1 = new Doboz(3);

    // mellek dobozok eloallitasa
    Doboz dd0(1);
    dd0 += da1;

    Doboz dd2(2);
    dd2 += da6;
    dd2 += da5;

    // mellek dobozok hozzaadasa
    *dd1 += dd0;
    *dd1 += dd2;
    *dd1 += da2;

    // modositas
    char str[200];
    Doboz last(3);
    last += dd0;
    last += da3;
    IO("", last = *dd1, str);

    dd1->max_dobozszam = 511;
    dd1->dobozok[0].max_dobozszam = 77;
    dd1->dobozok[2].ajandek->leiras = "...";

    ASSERT_EQ(last.max_dobozszam, 3);
    ASSERT_EQ(last.ajandek, nullptr);
    ASSERT_EQ(last.dobozok[0], dd0);
    ASSERT_EQ(last.dobozok[1], dd2);
    ASSERT_EQ(last.dobozok[2], da2);

    ASSERT_STREQ(str, "medveriaszto uborkacipokanal alaku villa");

    delete dd1;
}

TEST(ertekadas_operator, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    // ez lesz a fo doboz
    Doboz* dd1 = new Doboz(10);

    // mellek dobozok eloallitasa
    Doboz dd2(3);
    Doboz dd3(5);
    Doboz dd4(3);
    Doboz dd5(4);
    Doboz dd6(3);
    Doboz dd7(2);
    Doboz dd8(1);
    Doboz dd9(4);
    Doboz dd10(1);
    Doboz dd11(2);
    Doboz dd12(2);
    Doboz dd13(1);
    Doboz dd14(1);
    Doboz dd15(7);
    Doboz dd16(1);
    Doboz dd17(1);

    // mellek dobozok feltoltese
    dd7 += da3;
    dd7 += da4;
    dd5 += dd7;
    dd5 += da12;
    dd5 += da5;
    dd5 += da11;

    dd8 += dd10;
    dd6 += dd8;

    dd2 += dd5;
    dd2 += dd6;

    dd11 += da6;
    dd3 += dd11;
    dd3 += da7;
    dd3 += da8;
    dd3 += da9;
    dd3 += da10;

    dd12 += dd14;
    dd12 += dd13;

    dd16 += dd17;

    dd4 += dd12;
    dd4 += dd15;
    dd4 += dd16;

    // mellek dobozok hozzaadasa
    *dd1 += da13;
    *dd1 += dd2;
    *dd1 += dd3;
    *dd1 += dd4;
    *dd1 += dd9;
    *dd1 += da2;
    *dd1 += da1;

    char str[200];
    Doboz last(5);
    last += da10;
    last += dd3;
    last += da2;
    last += da7;

    IO("", last = *dd1, str)
    Doboz last2 = dd3;

    dd1->max_dobozszam = 511;
    dd1->dobozok[0].max_dobozszam = 77;
    dd1->dobozok[5].ajandek->leiras = "...";

    ASSERT_EQ(last.max_dobozszam, 10);
    ASSERT_EQ(last.ajandek, nullptr);
    ASSERT_EQ(last.dobozok[0], da13);
    ASSERT_EQ(last.dobozok[1], dd2);
    ASSERT_EQ(last.dobozok[2], dd3);
    ASSERT_EQ(last.dobozok[3], dd4);
    ASSERT_EQ(last.dobozok[4], dd9);
    ASSERT_EQ(last.dobozok[5], da2);
    ASSERT_EQ(last.dobozok[6], da1);

    ASSERT_EQ(last.dobozok[6], da1);
    da1.ajandek->leiras = "cipokanal alaku kes";
    ASSERT_NE(last.dobozok[6], da1);
    da1.ajandek->leiras = "cipokanal alaku villa";

    dd3.dobozok[1].ajandek->leiras = "???";
    dd3.dobozok[0].dobozok[0].ajandek->leiras = "ures";

    ASSERT_EQ(last2.dobozok[0], dd11);
    ASSERT_EQ(last2.dobozok[1], da7);
    ASSERT_EQ(last2.dobozok[2], da8);
    ASSERT_EQ(last2.dobozok[3], da9);
    ASSERT_EQ(last2.dobozok[4], da10);

    ASSERT_STREQ(str, "katalogus detektorkabatesoallo tolldorges kabelvezetekes telefon kabelkatalogus detektorkalkulus peldataresoallo toll");
}

TEST(ertekadas_operator, 03) {
    Doboz ddd("elefantfurdeto");
    Doboz dd1(2);

    char str[200];
    IO("", ddd = dd1, str);
    ASSERT_STREQ(str, "elefantfurdeto");
}

TEST(ertekadas_operator, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    // ez lesz a fo doboz
    Doboz* dd1 = new Doboz(3);

    // mellek dobozok eloallitasa
    Doboz dd0(1);
    dd0 += da1;

    Doboz dd2(2);
    dd2 += da6;
    dd2 += da5;

    // mellek dobozok hozzaadasa
    *dd1 += dd0;
    *dd1 += dd2;
    *dd1 += da2;

    // modositas
    char str[200];
    Doboz last(515);
    last += dd0;
    last += da3;
    IO("", last = last, str);

    ASSERT_EQ(last.max_dobozszam, 515);
    ASSERT_EQ(last.ajandek, nullptr);
    ASSERT_EQ(last.dobozok[0], dd0);
    ASSERT_EQ(last.dobozok[1], da3);
    ASSERT_EQ(last.dobozok[2], d0);
    ASSERT_EQ(last.dobozok[3], d0);
    ASSERT_EQ(last.dobozok[233], d0);

    ASSERT_STREQ(str, "");

    delete dd1;
}

TEST(hozzaadas, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    // ez lesz a fo doboz
    Doboz* dd1 = new Doboz(3);

    // mellek dobozok eloallitasa
    Doboz dd0(1);
    dd0 += da1;

    Doboz dd2(2);
    dd2 += da6;
    dd2 += da5;

    // mellek dobozok hozzaadasa
    *dd1 += dd0;
    *dd1 += dd2;
    *dd1 += da2;
    
    // modositas

    ASSERT_EQ(dd1->dobozok[0], dd0);
    ASSERT_EQ(dd1->dobozok[1], dd2);
    ASSERT_EQ(dd1->dobozok[2], da2);
    
    dd0.dobozok[0].ajandek->leiras = "medvecsapda";
    ASSERT_NE(dd1->dobozok[0], dd0);

    delete dd1;
}

TEST(hozzaadas, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    // ez lesz a fo doboz
    Doboz* dd1 = new Doboz(4);

    // mellek dobozok eloallitasa
    Doboz dd10(2);
    dd10 += da13;

    Doboz dd0(1);
    dd0 += da1;

    Doboz dd2(3);
    dd2 += dd10;
    dd2 += da6;
    dd2 += da5;

    // mellek dobozok hozzaadasa
    *dd1 += dd0;
    *dd1 += dd2;
    *dd1 += da2;

    // modositas

    ASSERT_EQ(dd1->dobozok[0], dd0);
    ASSERT_EQ(dd1->dobozok[1], dd2);
    ASSERT_EQ(dd1->dobozok[2], da2);
    ASSERT_EQ(dd1->dobozok[3], d0);

    dd0.dobozok[0].ajandek->leiras = "kalkulus peldatar";
    ASSERT_NE(dd1->dobozok[0], dd0);

    delete dd1;
}

TEST(dobozSzam, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    // ez lesz a fo doboz
    Doboz* dd1 = new Doboz(3);

    // mellek dobozok eloallitasa
    Doboz dd0(1);
    dd0 += da1;

    Doboz dd2(2);
    dd2 += da6;
    dd2 += da5;

    // mellek dobozok hozzaadasa
    *dd1 += dd0;
    *dd1 += dd2;
    *dd1 += da2;

    ASSERT_EQ(dd1->dobozSzam(), 6);
    ASSERT_EQ(dd0.dobozSzam(), 1);
    ASSERT_EQ(dd2.dobozSzam(), 2);
    ASSERT_EQ(da2.dobozSzam(), 0);
    ASSERT_EQ(dd1->dobozok[2].dobozSzam(), 0);

    delete dd1;
}

TEST(dobozSzam, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    // ez lesz a fo doboz
    Doboz* dd1 = new Doboz(10);

    // mellek dobozok eloallitasa
    Doboz dd2(3);
    Doboz dd3(5);
    Doboz dd4(3);
    Doboz dd5(4);
    Doboz dd6(3);
    Doboz dd7(2);
    Doboz dd8(1);
    Doboz dd9(4);
    Doboz dd10(1);
    Doboz dd11(2);
    Doboz dd12(2);
    Doboz dd13(1);
    Doboz dd14(1);
    Doboz dd15(7);
    Doboz dd16(1);
    Doboz dd17(1);

    // mellek dobozok feltoltese
    dd7 += da3;
    dd7 += da4;
    dd5 += dd7;
    dd5 += da12;
    dd5 += da5;
    dd5 += da11;

    dd8 += dd10;
    dd6 += dd8;

    dd2 += dd5;
    dd2 += dd6;

    dd11 += da6;
    dd3 += dd11;
    dd3 += da7;
    dd3 += da8;
    dd3 += da9;
    dd3 += da10;

    dd12 += dd14;
    dd12 += dd13;

    dd16 += dd17;

    dd4 += dd12;
    dd4 += dd15;
    dd4 += dd16;

    // mellek dobozok hozzaadasa
    *dd1 += da13;
    *dd1 += dd2;
    *dd1 += dd3;
    *dd1 += dd4;
    *dd1 += dd9;
    *dd1 += da2;
    *dd1 += da1;

    ASSERT_EQ(dd1->dobozSzam(), 29);

    ASSERT_EQ(dd2.dobozSzam(), 10);
    ASSERT_EQ(dd3.dobozSzam(), 6);
    ASSERT_EQ(dd4.dobozSzam(), 6);

    ASSERT_EQ(dd5.dobozSzam(), 6);
    ASSERT_EQ(dd6.dobozSzam(), 2);
    ASSERT_EQ(dd11.dobozSzam(), 1);
    ASSERT_EQ(dd15.dobozSzam(), 0);

    ASSERT_EQ(dd9.dobozSzam(), 0);

    delete dd1;
}

TEST(ajandekLista, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    // ez lesz a fo doboz
    Doboz* dd1 = new Doboz(3);

    // mellek dobozok eloallitasa
    Doboz dd0(1);
    dd0 += da1;

    Doboz dd2(2);
    dd2 += da6;
    dd2 += da5;

    // mellek dobozok hozzaadasa
    *dd1 += dd0;
    *dd1 += dd2;
    *dd1 += da2;

    ASSERT_EQ(dd1->ajandekLista(), "cipokanal alaku villa, kalkulus peldatar, szamologep, kabat");
    ASSERT_EQ(dd0.ajandekLista(), "cipokanal alaku villa");
    ASSERT_EQ(dd1->dobozok[2].ajandekLista(), "kabat");

    delete dd1;
}

TEST(ajandekLista, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    // ez lesz a fo doboz
    Doboz* dd1 = new Doboz(10);

    // mellek dobozok eloallitasa
    Doboz dd2(3);
    Doboz dd3(5);
    Doboz dd4(3);
    Doboz dd5(5);
    Doboz dd6(3);
    Doboz dd7(2);
    Doboz dd8(1);
    Doboz dd9(4);
    Doboz dd10(1);
    Doboz dd11(2);
    Doboz dd12(2);
    Doboz dd13(1);
    Doboz dd14(1);
    Doboz dd15(7);
    Doboz dd16(1);
    Doboz dd17(1);

    // mellek dobozok feltoltese
    dd7 += da3;
    dd7 += da4;
    dd5 += dd7;
    dd5 += da12;
    dd5 += da5;
    dd5 += da11;

    dd8 += dd10;
    dd6 += dd8;

    dd2 += dd5;
    dd2 += dd6;

    dd11 += da6;
    dd3 += dd11;
    dd3 += da7;
    dd3 += da8;
    dd3 += da9;
    dd3 += da10;

    dd12 += dd14;
    dd12 += dd13;

    dd16 += dd17;

    dd4 += dd12;
    dd4 += dd15;
    dd4 += dd16;

    // mellek dobozok hozzaadasa
    *dd1 += da13;
    *dd1 += dd2;
    *dd1 += dd3;
    *dd1 += dd4;
    *dd1 += dd9;
    *dd1 += da2;
    *dd1 += da1;

    ASSERT_EQ(dd1->ajandekLista(), "kerek parna, medveriaszto uborka, parnahuzat, billentyukkel rendelkezo billentyuzet, szamologep, felhuzhato zokni, kalkulus peldatar, katalogus detektor, vezetekes telefon kabel, dorges kabel, esoallo toll, kabat, cipokanal alaku villa");

    ASSERT_EQ(dd2.ajandekLista(), "medveriaszto uborka, parnahuzat, billentyukkel rendelkezo billentyuzet, szamologep, felhuzhato zokni");
    ASSERT_EQ(dd5.ajandekLista(), "medveriaszto uborka, parnahuzat, billentyukkel rendelkezo billentyuzet, szamologep, felhuzhato zokni");
    ASSERT_EQ(dd7.ajandekLista(), "medveriaszto uborka, parnahuzat");
    ASSERT_EQ(dd6.ajandekLista(), "");

    ASSERT_EQ(dd3.ajandekLista(), "kalkulus peldatar, katalogus detektor, vezetekes telefon kabel, dorges kabel, esoallo toll");
    ASSERT_EQ(dd11.ajandekLista(), "kalkulus peldatar");

    ASSERT_EQ(dd4.ajandekLista(), "");

    delete dd1;
}

TEST(ajandekotElhelyez, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Doboz dd1(3);

    bool res = dd1.ajandekotElhelyez("toll");
    ASSERT_NE(dd1.ajandek, nullptr);
    ASSERT_EQ(dd1.ajandek->leiras, "toll");
    ASSERT_EQ(dd1.dobozok, nullptr);
    ASSERT_EQ(dd1.max_dobozszam, 0);

    dd1 += da1;
    dd1 += da2;
    ASSERT_NE(dd1.ajandek, nullptr);
    ASSERT_EQ(dd1.ajandek->leiras, "toll");
    ASSERT_EQ(dd1.dobozok, nullptr);
    ASSERT_EQ(dd1.max_dobozszam, 0);

    ASSERT_EQ(res, true);

    Doboz dd2(2);
    dd2 += da1;
    res = dd2.ajandekotElhelyez("telefontok");

    ASSERT_NE(dd2.dobozok, nullptr);
    ASSERT_EQ(dd2.ajandek, nullptr);
    ASSERT_EQ(dd2.max_dobozszam, 2);
    ASSERT_EQ(dd2.dobozok[0], da1);
    ASSERT_EQ(dd2.dobozok[1], d0);

    ASSERT_EQ(res, false);
}

TEST(ajandekotElhelyez, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Doboz dd1("eger");
    bool res = dd1.ajandekotElhelyez("masik");
    ASSERT_EQ(dd1.ajandek->leiras, "eger");
    ASSERT_EQ(dd1.dobozok, nullptr);
    ASSERT_EQ(dd1.max_dobozszam, 0);

    dd1 += da1;
    ASSERT_EQ(dd1.ajandek->leiras, "eger");
    ASSERT_EQ(dd1.dobozok, nullptr);
    ASSERT_EQ(dd1.max_dobozszam, 0);

    ASSERT_EQ(res, false);
}

TEST(destruktor, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    // ez lesz a fo doboz
    Doboz* dd1 = new Doboz(3);

    // mellek dobozok eloallitasa
    Doboz dd0(1);
    dd0 += da1;

    Doboz dd2(2);
    dd2 += da6;
    dd2 += da5;

    // mellek dobozok hozzaadasa
    *dd1 += dd0;
    *dd1 += dd2;
    *dd1 += da2;

    char str[200];

    IO("", delete dd1, str)

    ASSERT_STREQ(str, "kabatszamologepkalkulus peldatarcipokanal alaku villa");
}

TEST(destruktor, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    // ez lesz a fo doboz
    Doboz* dd1 = new Doboz(10);

    // mellek dobozok eloallitasa
    Doboz dd2(3);
    Doboz dd3(5);
    Doboz dd4(3);
    Doboz dd5(4);
    Doboz dd6(3);
    Doboz dd7(2);
    Doboz dd8(1);
    Doboz dd9(4);
    Doboz dd10(1);
    Doboz dd11(2);
    Doboz dd12(2);
    Doboz dd13(1);
    Doboz dd14(1);
    Doboz dd15(7);
    Doboz dd16(1);
    Doboz dd17(1);

    // mellek dobozok feltoltese
    dd7 += da3;
    dd7 += da4;
    dd5 += dd7;
    dd5 += da12;
    dd5 += da5;
    dd5 += da11;

    dd8 += dd10;
    dd6 += dd8;

    dd2 += dd5;
    dd2 += dd6;

    dd11 += da6;
    dd3 += dd11;
    dd3 += da7;
    dd3 += da8;
    dd3 += da9;
    dd3 += da10;

    dd12 += dd14;
    dd12 += dd13;

    dd16 += dd17;

    dd4 += dd12;
    dd4 += dd15;
    dd4 += dd16;

    // mellek dobozok hozzaadasa
    *dd1 += da13;
    *dd1 += dd2;
    *dd1 += dd3;
    *dd1 += dd4;
    *dd1 += dd9;
    *dd1 += da2;
    *dd1 += da1;

    char str[500];

    IO("", delete dd1, str);

    ASSERT_STREQ(str, "cipokanal alaku villakabatesoallo tolldorges kabelvezetekes telefon kabelkatalogus detektorkalkulus peldatarfelhuzhato zokniszamologepbillentyukkel rendelkezo billentyuzetparnahuzatmedveriaszto uborkakerek parna");
}

TEST(destruktor, virtualis) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    class MM: public Doboz {
    public:
        MM(unsigned int maximalisAjandekDarab) : Doboz(maximalisAjandekDarab) {}
        ~MM() { cout << "M"; }
    };

    Doboz* d = new MM(3);
    *d += da1;
    *d += da2;

    // teszt
    char str[200];
    IO("", delete d, str)

    ASSERT_STREQ(str, "Mkabatcipokanal alaku villa");
}