#include <iostream>
#include <string>

using namespace std;

struct Fa {
    string tipus;
    int magassag;

    Fa(): tipus(""), magassag(1) {}
    Fa(const string &tipus, int magassag) : tipus(tipus), magassag(magassag) {}
    ~Fa() { cout << tipus << magassag; }
};

class Erdo {
    Fa* fak;
    unsigned int aktualis_darab;
    unsigned int maximalis_darab;

public:
    Erdo& operator+=(const Fa& fa) {
        if (aktualis_darab != maximalis_darab) {
            fak[aktualis_darab] = fa;
            aktualis_darab++;
        }

        return *this;
    }

    Erdo(unsigned int maximalisDarab) : maximalis_darab(maximalisDarab) {
        if (maximalisDarab > 0) {
            this->fak = new Fa[maximalisDarab];
        } else {
            this->fak = nullptr;
        }
    }

    virtual ~Erdo() {
        if (this->fak != nullptr) {
            delete[] this->fak;
        }
    }
};