#include <gtest/gtest.h>

#define main main_0
#define delete printf("A");delete
#define class struct
#define private public
#include "../src/erdo.cpp"
#undef private
#undef class
#undef delete
#undef main

#include "../../tools.cpp"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Erdo* e = new Erdo(2);

    char str[200];
    IO("", delete e, str)
    ASSERT_STREQ(str, "A11");
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Erdo* e = new Erdo(4);

    Fa f1("a", 3);
    Fa f2("b", 4);
    *e += f1;
    *e += f2;

    char str[200];
    IO("", delete e, str)
    ASSERT_STREQ(str, "A11b4a3");
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Erdo* e = new Erdo(1);

    Fa f1("a", 3);
    *e += f1;

    char str[200];
    IO("", delete e, str)
    ASSERT_STREQ(str, "Aa3");
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Erdo* e = new Erdo(0);

    char str[200];
    IO("", delete e, str)
    ASSERT_STREQ(str, "A");
}

TEST(Teszt, virtualis_destruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    class Erdo2: public Erdo {
    public:
        Erdo2(unsigned int maximalisDarab) : Erdo(maximalisDarab) {}
        ~Erdo2() { cout << "Q"; }
    };

    Erdo* e = new Erdo2(0);

    char str[200];
    IO("", delete e, str)
    ASSERT_STREQ(str, "QA");
}