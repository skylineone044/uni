#include <gtest/gtest.h>

#define main main_0
#include "../src/fajlkezeles.cpp"
#undef main

#include "../../tools.cpp"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Fajl f1("macska.txt");
    f1 << "cica";

    FILE* f = fopen("macska.txt", "r");
    char str[200];
    readstring(f, str);

    ASSERT_STREQ(str, "");
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Fajl* f1 = new Fajl("macska.txt");
    (*f1) << "macska";

    FILE* f = fopen("macska.txt", "r");
    char str[200];
    readstring(f, str);
    fclose(f);

    ASSERT_STREQ(str, "");

    delete f1;

    f = fopen("macska.txt", "r");
    readstring(f, str);
    fclose(f);

    ASSERT_STREQ(str, "macska");
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Fajl* f1 = new Fajl("macska.txt");

    (*f1) << "macska";
    (*f1) << 5;
    (*f1) << 3.33;
    (*f1) << " ot";

    delete f1;

    FILE* f = fopen("macska.txt", "r");
    char str[200];
    readstring(f, str);

    ASSERT_STREQ(str, "macska53.33 ot");
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Fajl* f1 = new Fajl("macska.txt");

    (*f1) << "egy" << 2 << 3.0 << "negy" << 5;

    delete f1;

    FILE* f = fopen("macska.txt", "r");
    char str[200];
    readstring(f, str);

    ASSERT_STREQ(str, "egy23.00negy5");
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Fajl* f1 = new Fajl("mascka.txt");

    (*f1) << "a";

    delete f1;

    FILE* f = fopen("mascka.txt", "r");
    char str[200];
    readstring(f, str);

    ASSERT_STREQ(str, "a");
}

TEST(Teszt, virtualis_destruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    class Fajl2: public Fajl {
    public:
        Fajl2(const string &fajlnev) : Fajl(fajlnev) {}
        ~Fajl2() {cout << "OK";}
    };

    Fajl* f1 = new Fajl2("mascka.txt");

    char ss[10];
    IO("", (*f1) << "a"; delete f1, ss)



    FILE* fff = fopen("mascka.txt", "r");
    char str[200];
    readstring(fff, str);

    ASSERT_STREQ(str, "a");
    ASSERT_STREQ(ss, "OK");
}