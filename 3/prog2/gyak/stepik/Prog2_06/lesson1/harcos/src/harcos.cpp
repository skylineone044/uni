#include <iostream>

using namespace std;

class Harcos {
    unsigned int eletero;

public:
    Harcos(unsigned int eletero) : eletero(eletero) {
        cout << "Letrejott a harcos!" << endl;
    }

    virtual ~Harcos() {
        this->eletero = 0;
        cout << "Elesett a harcos!" << endl;
    }

};