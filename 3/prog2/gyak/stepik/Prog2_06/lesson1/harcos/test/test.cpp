#include <gtest/gtest.h>

#define main main_0
#define private public
#define class struct
#define cout cout << eletero << " "
#include "../src/harcos.cpp"
#undef cout
#undef private
#undef class
#undef main

#include "../../tools.cpp"

class Masikharcos: public Harcos {
public:
    Masikharcos(unsigned int eletero) : Harcos(eletero) {}

    ~Masikharcos() {
        cout << "OK" << endl;
    }
};

TEST(Konstruktor, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", Harcos h(5), str)
    ASSERT_STREQ(str, "5 Letrejott a harcos!\n");
    ASSERT_EQ(h.eletero, 5);
}

TEST(Konstruktor, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", Harcos h(7), str)
    ASSERT_STREQ(str, "7 Letrejott a harcos!\n");
    ASSERT_EQ(h.eletero, 7);
}

TEST(Destruktor, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", Harcos* h = new Harcos(10); delete h, str)
    ASSERT_STREQ(str, "10 Letrejott a harcos!\n0 Elesett a harcos!\n");
}

TEST(Destruktor, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", Harcos* h = new Harcos(414); delete h, str)
    ASSERT_STREQ(str, "414 Letrejott a harcos!\n0 Elesett a harcos!\n");
}

TEST(Destruktor, virtualissag) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("", Harcos* h = new Masikharcos(414); delete h, str)
    ASSERT_STREQ(str, "414 Letrejott a harcos!\nOK\n0 Elesett a harcos!\n");
}