#include <iostream>
#include <string>

using namespace std;

class Kaloz {
    string nev;
    int ero;

public:
    Kaloz(): nev(""), ero(0) {}
    Kaloz(const string& nev, int ero): nev(nev), ero(ero) {}
    ~Kaloz() { cout << "K"; }
};

class Hajo {
    unsigned fedelzet_meret;
    unsigned kalozok_szama;

    Kaloz* kalozok;

public:
    Hajo(unsigned fedelzet_meret): fedelzet_meret(fedelzet_meret), kalozok_szama(0), kalozok(new Kaloz[10]) {}

    Hajo& operator=(const Hajo& h) {
        if (this == &h) {
            return *this;
        }
        this->fedelzet_meret = h.fedelzet_meret;
        this->kalozok_szama = h.kalozok_szama;

        delete[] this->kalozok;
        this->kalozok = new Kaloz[10];
        for (int i = 0; i < 10; ++i) {
            this->kalozok[i] = h.kalozok[i];
        }
        return *this;
    }

    ~Hajo() {
        delete[] this->kalozok;
    }
};