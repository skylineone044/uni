#include <gtest/gtest.h>
#include <string>

using namespace std;

#define class struct
#define private public
#define main main_0
#include "../src/kaloz.cpp"
#undef main
#undef private
#undef class

#include "../../tools.cpp"

bool operator==(const Kaloz& k1, const Kaloz& k2) {
    return k1.nev == k2.nev && k1.ero == k2.ero;
}

TEST(Ertekadas_operator, mukodes) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Kaloz k;
    Kaloz k0("jkndfb", 3);
    Kaloz k1("jklsbn", 7);
    Kaloz k2("jklvndjk", -2);

    Hajo h1(47);
    h1.kalozok[0] = k0;
    h1.kalozok[1] = k1;
    h1.kalozok[2] = k2;
    h1.kalozok_szama = 3;

    Hajo h2(667);
    h2.kalozok[0] = k2;
    h2.kalozok_szama = 1;

    char str[100];
    IO("", h2 = h1, str);
    ASSERT_STREQ(str, "KKKKKKKKKK");

    ASSERT_EQ(h2.fedelzet_meret, 47);
    ASSERT_EQ(h2.kalozok_szama, 3);
    ASSERT_EQ(k0, h2.kalozok[0]);
    ASSERT_EQ(k1, h2.kalozok[1]);
    ASSERT_EQ(k2, h2.kalozok[2]);
    ASSERT_EQ(k, h2.kalozok[3]);

    h2.kalozok[0] = k1;
    ASSERT_EQ(h1.kalozok[0], k0);
}

TEST(Ertekadas_operator, specialis_eset) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Kaloz k;
    Kaloz k0("jkndfb", 3);
    Kaloz k1("jklsbn", 7);
    Kaloz k2("jklvndjk", -2);

    Hajo h1(47);
    h1.kalozok[0] = k0;
    h1.kalozok[1] = k1;
    h1.kalozok[2] = k2;
    h1.kalozok_szama = 3;

    char str[100];
    IO("", h1 = h1, str);
    ASSERT_STREQ(str, "");

    ASSERT_EQ(h1.fedelzet_meret, 47);
    ASSERT_EQ(h1.kalozok_szama, 3);
    ASSERT_EQ(k0, h1.kalozok[0]);
    ASSERT_EQ(k1, h1.kalozok[1]);
    ASSERT_EQ(k2, h1.kalozok[2]);
    ASSERT_EQ(k, h1.kalozok[3]);
}