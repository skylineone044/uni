#include <string>
#include <iostream>

using namespace std;

class Disz {
    string forma;
    string szin;
    float meret;

public:
    Disz(): forma(""), szin(""), meret(0) { cout << "B"; }
    Disz(const string& forma, const string& szin, float meret): forma(forma), szin(szin), meret(meret) {}
    ~Disz() { cout << "D"; }
};

class Karacsonyfa {
    string fajta;
    unsigned magassag;

    Disz* diszek;
    unsigned diszek_szama;

public:
    Karacsonyfa(const string& fajta, unsigned int magassag): fajta(fajta), magassag(magassag), diszek_szama(0), diszek(new Disz[0]) {}

    Karacsonyfa& operator+=(const Disz& disz) {
        Disz* ujDiszek = new Disz[this->diszek_szama + 1];

        if (this->diszek != nullptr) {
            for (int i = 0; i < this->diszek_szama; ++i) {
                ujDiszek[i] = this->diszek[i];
            }
        }

        ujDiszek[this->diszek_szama] = disz;
        this->diszek_szama++;

        delete[] this->diszek;
        this->diszek = ujDiszek;

        return *this;
    }
};