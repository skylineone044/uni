#include <gtest/gtest.h>
#include <string>

using namespace std;

#define class struct
#define private public
#define main main_0
#include "../src/karacsonyfa.cpp"
#undef main
#undef private
#undef class

#include "../../tools.cpp"

bool operator==(const Disz& d1, const Disz& d2) {
    return abs(d1.meret - d2.meret) < 0.001f && d1.szin == d2.szin && d1.forma == d2.forma;
}

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Disz d1("gomb", "piros", 3);
    const Disz d2("gomb", "kek", 5);
    const Disz d3("kacsa", "sarga", 7);

    char str[200];
    IO("",
        Karacsonyfa k("luc", 202);
        cout << "A";
        k += d1;
        cout << "C";
        k += d2;
        cout << "E";
        k += d3,
        str)

    ASSERT_EQ(k.diszek[0], d1);
    ASSERT_EQ(k.diszek[1], d2);
    ASSERT_EQ(k.diszek[2], d3);
    ASSERT_EQ(k.diszek_szama, 3);

    ASSERT_STREQ(str, "ABCBBDEBBBDD");
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Disz d1("kocka", "piros", 7);
    const Disz d2("medve", "kek", 2);

    char str[200];
    IO("",
       Karacsonyfa k("luc", 202);
               cout << "A";
               k += d1;
               cout << "C";
               k += d2,
       str)

    ASSERT_EQ(k.diszek[0], d1);
    ASSERT_EQ(k.diszek[1], d2);
    ASSERT_EQ(k.diszek_szama, 2);

    ASSERT_STREQ(str, "ABCBBD");
}