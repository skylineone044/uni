#include <iostream>
#include <string>

using namespace std;

class Kavics {
    unsigned kiterjedes;
    string szin;
    bool ertekes;

public:
    Kavics(unsigned int kiterjedes, const string& szin, bool ertekes): kiterjedes(kiterjedes), szin(szin), ertekes(ertekes) {}

    Kavics(const Kavics& k1) {
        this->kiterjedes = k1.kiterjedes;
        this->szin = k1.szin;
        this->ertekes = k1.ertekes;
        cout << "uj kavics" << endl;
    }

};