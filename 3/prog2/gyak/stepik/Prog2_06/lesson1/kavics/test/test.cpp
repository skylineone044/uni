#include <gtest/gtest.h>
#include <string>

using namespace std;

#define class struct
#define private public
#define main main_0
#include "../src/kavics.cpp"
#undef main
#undef class
#undef private

#include "../../tools.cpp"

TEST(Copy_konstruktor, masolas) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Kavics k(5, "zold", true);
    const Kavics k2(7, "sarga", false);

    Kavics k3(k);
    Kavics k4(k2);

    ASSERT_EQ(k3.kiterjedes, 5);
    ASSERT_EQ(k3.szin, "zold");
    ASSERT_EQ(k3.ertekes, true);

    ASSERT_EQ(k4.kiterjedes, 7);
    ASSERT_EQ(k4.szin, "sarga");
    ASSERT_EQ(k4.ertekes, false);
}

TEST(Copy_konstruktor, kiiras) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Kavics k(7, "piros", true);

    char str[100];
    IO("", Kavics k2(k), str);
    ASSERT_STREQ(str, "uj kavics\n");
}