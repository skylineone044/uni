#include <gtest/gtest.h>

#define main main_0
#define private public
#define class struct
#include "../src/macska.cpp"
#undef class
#undef private
#undef main

#include "../../tools.cpp"

Jatek j0;
Jatek j1(3, false);
Jatek j2(5, true);
Jatek j3(2, true);
Jatek j4(5, false);
Jatek j5(4, true);
Jatek j6(5, true);

bool operator==(const Jatek& j1, const Jatek& j2) {
    return j1.csorog == j2.csorog && j1.tipus == j2.tipus;
}

TEST(jatek_osztaly, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Jatek* j = new Jatek(3, true);
    char str[100];
    IO("", delete j, str)
    ASSERT_STREQ(str, "J31");
}

TEST(jatek_osztaly, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Jatek* j = new Jatek(22, false);
    char str[100];
    IO("", delete j, str)
    ASSERT_STREQ(str, "J220");
}

TEST(hozzaadas_operator, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Macska* m = new Macska(3);
    *m += j1;
    *m += j2;
    *m += j3;

    ASSERT_EQ(m->jatekok[0], j1);
    ASSERT_EQ(m->jatekok[1], j2);
    ASSERT_EQ(m->jatekok[2], j3);

    delete m;
}

TEST(hozzaadas_operator, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Macska* m = new Macska(3);
    *m += j4;
    *m += j3;

    ASSERT_EQ(m->jatekok[0], j4);
    ASSERT_EQ(m->jatekok[1], j3);
    ASSERT_EQ(m->jatekok[2], j0);

    delete m;
}

TEST(copy_konstruktor, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Macska* m = new Macska(4);
    *m += j4;
    *m += j3;

    // masolas
    Macska m2 = *m;

    // modositas
    m->jatekok[0].tipus = 1000;
    m->jatekok[1].csorog = false;

    ASSERT_EQ(m2.jatekok[0], j4);
    ASSERT_EQ(m2.jatekok[1], j3);
    ASSERT_EQ(m2.jatekok[2], j0);

    delete m;
}

TEST(copy_konstruktor, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Macska* m = new Macska(2);

    // masolas
    Macska m2 = *m;

    ASSERT_EQ(m2.jatekok[0], j0);
    ASSERT_EQ(m2.jatekok[1], j0);

    delete m;
}

TEST(ertekadas_operator, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Macska* m = new Macska(3);
    *m += j4;
    *m += j3;

    Macska m2(4);
    m2 += j1;

    // teszt
    char str[200];
    IO("", m2 = *m, str)

    // modositas
    m->jatekok[1].tipus = 30;
    m->jatekok[0].csorog = true;

    ASSERT_EQ(m2.jatekok[0], j4);
    ASSERT_EQ(m2.jatekok[1], j3);
    ASSERT_EQ(m2.jatekok[2], j0);

    delete m;

    ASSERT_STREQ(str, "J00J00J00J30");
}

TEST(ertekadas_operator, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Macska* m = new Macska(4);
    *m += j5;
    *m += j2;
    *m += j1;

    Macska m2(1);
    m2 += j1;

    // teszt
    char str[200];
    IO("", m2 = *m, str)

    // modositas
    m->jatekok[2].tipus = 331;
    m->jatekok[1] = j6;
    m->jatekok[0].csorog = true;

    ASSERT_EQ(m2.jatekok[0], j5);
    ASSERT_EQ(m2.jatekok[1], j2);
    ASSERT_EQ(m2.jatekok[2], j1);

    delete m;

    ASSERT_STREQ(str, "J30");
}

TEST(ertekadas_operator, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Macska* m = new Macska(10);
    *m += j5;
    *m += j2;
    *m += j1;

    Macska m2(3);
    m2 += j1;
    m2 += j5;

    // teszt
    char str[200];
    IO("", m2 = m2, str)

    // modositas
    m->jatekok[2].tipus = 331;
    m->jatekok[1] = j6;
    m->jatekok[0].csorog = true;

    ASSERT_EQ(m2.jatekok[0], j1);
    ASSERT_EQ(m2.jatekok[1], j5);
    ASSERT_EQ(m2.jatekok[2], j0);

    delete m;

    ASSERT_STREQ(str, "");

    const Macska mm1(3);
    m2 = mm1;
}

TEST(destruktor, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Macska* m = new Macska(5);
    *m += j5;
    *m += j2;
    *m += j1;

    // teszt
    char str[200];
    IO("", delete m, str)

    ASSERT_STREQ(str, "J00J00J30J51J41");
}

TEST(destruktor, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Macska* m = new Macska(4);
    *m += j6;
    *m += j3;

    // teszt
    char str[200];
    IO("", delete m, str)

    ASSERT_STREQ(str, "J00J00J21J51");
}

TEST(destruktor, virtualis) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    class MM: public Macska {
    public:
        MM(unsigned int maximalisJatekDarab) : Macska(maximalisJatekDarab) {}
        virtual ~MM() { cout << "M"; }
    };

    Macska* m = new MM(3);
    *m += j1;
    *m += j4;

    // teszt
    char str[200];
    IO("", delete m, str)

    ASSERT_STREQ(str, "MJ00J50J30");
}