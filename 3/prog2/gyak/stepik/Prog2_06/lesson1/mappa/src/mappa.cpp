#include <iostream>
#include <string>

using namespace std;

class Fajl {
    string nev;
    string tartalom;

public:
    Fajl() = default;
    Fajl(const string& nev, const string& tartalom): nev(nev), tartalom(tartalom) {}
    ~Fajl() { cout << "DEL"; }
};

class Mappa {
    string nev;

    Fajl* fajlok;
    unsigned fajl_mennyiseg;

public:
    Mappa(const string& nev): nev(nev), fajl_mennyiseg(2), fajlok(new Fajl[2]) {}

    virtual ~Mappa() {
        delete[] this->fajlok;
    }
};