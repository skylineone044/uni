#include <gtest/gtest.h>
#include <string>

using namespace std;

#define main main_0
#include "../src/mappa.cpp"
#undef main

#include "../../tools.cpp"

TEST(Destruktor, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Mappa* mappa = new Mappa("macska");

    char str[100];
    IO("", delete mappa, str);
    ASSERT_STREQ(str, "DELDEL");
}

TEST(Destruktor, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Mappa* mappa = new Mappa("macska");
    Mappa* mappa2 = new Mappa("macska");

    char str[100];
    IO("", delete mappa; delete mappa2, str);
    ASSERT_STREQ(str, "DELDELDELDEL");
}

TEST(Destruktor, virtualis) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    class Mappa2: public Mappa {
    public:
        Mappa2(): Mappa("asd") {}
        ~Mappa2() { cout << "Q"; }
    };

    Mappa* m = new Mappa2();

    char str[100];
    IO("", delete m, str)
    ASSERT_STREQ(str, "QDELDEL");
}