#include <iostream>
#include <string>

using namespace std;

struct Doboz {
    int azonosito_szam;
    int meret;

public:
    Doboz(): azonosito_szam(0), meret(0) {}
    Doboz(int azonositoSzam, int meret) : azonosito_szam(azonositoSzam), meret(meret) {}
    virtual ~Doboz() { cout << azonosito_szam << meret; }
};

struct Ember {
    string nev;
    unsigned int szuletesi_ev;

public:
    Ember(const string &nev, unsigned int szuletesiEv) : nev(nev), szuletesi_ev(szuletesiEv) {}
    virtual ~Ember() { cout << nev << szuletesi_ev; }
};

class Raktar {
    string lokacio;
    Ember* tulajdonos;
    Doboz* dobozok;
    unsigned int max_dobozok;
    unsigned int aktualis_dobozszam;

public:
    Raktar(const string &lokacio, unsigned int max_dobozok, const string& tulajdonos_nev, unsigned int tulajdonos_szuletesi_ev) :
        lokacio(lokacio), max_dobozok(max_dobozok), aktualis_dobozszam(0) {
        tulajdonos = new Ember(tulajdonos_nev, tulajdonos_szuletesi_ev);
        dobozok = new Doboz[max_dobozok];
    }

    Raktar& operator+=(const Doboz& d) {
        if (aktualis_dobozszam < max_dobozok) {
            dobozok[aktualis_dobozszam] = d;
            aktualis_dobozszam++;
        }

        return *this;
    }

    // megoldas

    virtual ~Raktar() {
        delete tulajdonos;
        delete[] dobozok;
    }

};