#include <gtest/gtest.h>

#define main main_0
#define class struct
#define private public
#include "../src/raktar.cpp"
#undef private
#undef class
#undef main

#include "../../tools.cpp"

Doboz d1 = {11111, 1};
Doboz d2 = {11112, 2};
Doboz d3 = {11113, 2};
Doboz d0;

bool operator==(const Doboz& d1, const Doboz& d2) {
    return d1.meret == d2.meret && d1.azonosito_szam == d2.azonosito_szam;
}


TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Raktar* r1 = new Raktar("hely", 3, "jozsi", 2000);
    *r1 += d1;
    *r1 += d2;


    Raktar* r2 = new Raktar(*r1);
    r1->lokacio = "masik";
    r1->max_dobozok = 7;
    r1->tulajdonos->nev = "marika";
    r1->tulajdonos->szuletesi_ev = 1999;

    *r1 += d3;
    r1->dobozok[0].azonosito_szam = 1;
    r1->dobozok[1].meret = 10;

    ASSERT_EQ(r2->lokacio, "hely");
    ASSERT_EQ(r2->max_dobozok, 3);
    ASSERT_EQ(r2->aktualis_dobozszam, 2);
    ASSERT_EQ(r2->tulajdonos->nev, "jozsi");
    ASSERT_EQ(r2->tulajdonos->szuletesi_ev, 2000);
    ASSERT_NE(r1->dobozok, r2->dobozok);

    ASSERT_EQ(r2->dobozok[0], d1);
    ASSERT_EQ(r2->dobozok[1], d2);
    ASSERT_EQ(r2->dobozok[2], d0);

    char str[200];
    IO("", delete r2; delete r1, str);
    ASSERT_STREQ(str, "jozsi200000111122111111marika1999111132111121011");
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Raktar* r1 = new Raktar("masik", 2, "anett", 1988);
    *r1 += d3;
    *r1 += d2;


    Raktar* r2 = new Raktar(*r1);
    r1->lokacio = "egyik";
    r1->max_dobozok = 4;
    r1->tulajdonos->nev = "janos";
    r1->tulajdonos->szuletesi_ev = 1997;

    r1->dobozok[0].azonosito_szam = 33;
    r1->dobozok[1].meret = 31;

    ASSERT_EQ(r2->lokacio, "masik");
    ASSERT_EQ(r2->max_dobozok, 2);
    ASSERT_EQ(r2->aktualis_dobozszam, 2);
    ASSERT_EQ(r2->tulajdonos->nev, "anett");
    ASSERT_EQ(r2->tulajdonos->szuletesi_ev, 1988);
    ASSERT_NE(r1->dobozok, r2->dobozok);

    ASSERT_EQ(r2->dobozok[0], d3);
    ASSERT_EQ(r2->dobozok[1], d2);

    char str[200];
    IO("", delete r2; delete r1, str);
    ASSERT_STREQ(str, "anett1988111122111132janos19971111231332");
}