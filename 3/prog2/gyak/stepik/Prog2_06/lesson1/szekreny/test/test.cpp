#include <gtest/gtest.h>

#define main main_0
#define private public
#define class struct
#include "../src/szekreny.cpp"
#undef class
#undef private
#undef main

#include "../../tools.cpp"

Vallfa j0;
Vallfa j1("piros", 3);
Vallfa j2("kek", 5);
Vallfa j3("fekete", 2);
Vallfa j4("barna", 3);
Vallfa j5("zold", 1);
Vallfa j6("fekete", 3);

bool operator==(const Vallfa& j1, const Vallfa& j2) {
    return j1.szin == j2.szin && j1.meret == j2.meret;
}

TEST(Vallfa_osztaly, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Vallfa* j = new Vallfa("piros", 2);
    char str[100];
    IO("", delete j, str)
    ASSERT_STREQ(str, "Vpiros2");
}

TEST(Vallfa_osztaly, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Vallfa* j = new Vallfa("oros", 3);
    char str[100];
    IO("", delete j, str)
    ASSERT_STREQ(str, "Voros3");
}

TEST(hozzaadas_operator, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Szekreny* m = new Szekreny(3);
    *m += j1;
    *m += j2;
    *m += j3;

    ASSERT_EQ(m->vallfak[0], j1);
    ASSERT_EQ(m->vallfak[1], j2);
    ASSERT_EQ(m->vallfak[2], j3);

    delete m;
}

TEST(hozzaadas_operator, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Szekreny* m = new Szekreny(3);
    *m += j4;
    *m += j3;

    ASSERT_EQ(m->vallfak[0], j4);
    ASSERT_EQ(m->vallfak[1], j3);
    ASSERT_EQ(m->vallfak[2], j0);

    delete m;
}

TEST(hozzaadas_operator, 03) {
    // itt még nézzük a tömb méretének módosítását, felszabadítását is!
    Szekreny* m = new Szekreny(3);
    Vallfa* regivallfak = m->vallfak;
    *m += j1;
    ASSERT_EQ(regivallfak, m->vallfak);

    *m += j2;
    ASSERT_EQ(regivallfak, m->vallfak);

    *m += j3;
    ASSERT_EQ(regivallfak, m->vallfak);

    ASSERT_EQ(m->vallfak[0], j1);
    ASSERT_EQ(m->vallfak[1], j2);
    ASSERT_EQ(m->vallfak[2], j3);

    *m += j4;

    ASSERT_EQ(m->vallfak[0], j1);
    ASSERT_EQ(m->vallfak[1], j2);
    ASSERT_EQ(m->vallfak[2], j3);
    ASSERT_EQ(m->vallfak[3], j4);

    ASSERT_NE(m->vallfak, regivallfak);

    Vallfa* ujvallfak = m->vallfak;

    *m += j5;
    ASSERT_EQ(ujvallfak, m->vallfak);

    *m += j6;
    ASSERT_EQ(ujvallfak, m->vallfak);

    ASSERT_EQ(m->vallfak[0], j1);
    ASSERT_EQ(m->vallfak[1], j2);
    ASSERT_EQ(m->vallfak[2], j3);
    ASSERT_EQ(m->vallfak[3], j4);
    ASSERT_EQ(m->vallfak[4], j5);
    ASSERT_EQ(m->vallfak[5], j6);

    char str[200];
    IO("", *m += j2, str);
    ASSERT_NE(ujvallfak, m->vallfak);
    ASSERT_NE(regivallfak, m->vallfak);

    ASSERT_EQ(m->vallfak[0], j1);
    ASSERT_EQ(m->vallfak[1], j2);
    ASSERT_EQ(m->vallfak[2], j3);
    ASSERT_EQ(m->vallfak[3], j4);
    ASSERT_EQ(m->vallfak[4], j5);
    ASSERT_EQ(m->vallfak[5], j6);
    ASSERT_EQ(m->vallfak[6], j2);

    ASSERT_STREQ(str, "Vfekete3Vzold1Vbarna3Vfekete2Vkek5Vpiros3");

    delete m;
}

TEST(copy_konstruktor, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Szekreny* m = new Szekreny(4);
    *m += j4;
    *m += j3;

    // masolas
    Szekreny m2 = *m;

    // modositas
    m->vallfak[0].szin = "rozsaszin";
    m->vallfak[1].meret = 100;

    ASSERT_EQ(m2.vallfak[0], j4);
    ASSERT_EQ(m2.vallfak[1], j3);
    ASSERT_EQ(m2.vallfak[2], j0);

    delete m;
}

TEST(copy_konstruktor, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Szekreny* m = new Szekreny(2);

    // masolas
    Szekreny m2 = *m;

    ASSERT_EQ(m2.vallfak[0], j0);
    ASSERT_EQ(m2.vallfak[1], j0);

    delete m;
}

TEST(ertekadas_operator, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Szekreny* m = new Szekreny(3);
    *m += j4;
    *m += j3;

    Szekreny m2(4);
    m2 += j1;

    // teszt
    char str[200];
    IO("", m2 = *m, str)

    // modositas
    m->vallfak[1].meret = 30;
    m->vallfak[0].szin = "feher";

    ASSERT_EQ(m2.vallfak[0], j4);
    ASSERT_EQ(m2.vallfak[1], j3);
    ASSERT_EQ(m2.vallfak[2], j0);

    delete m;

    ASSERT_STREQ(str, "Vfekete10Vfekete10Vfekete10Vpiros3");
}

TEST(ertekadas_operator, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Szekreny* m = new Szekreny(4);
    *m += j5;
    *m += j2;
    *m += j1;

    Szekreny m2(1);
    m2 += j1;

    // teszt
    char str[200];
    IO("", m2 = *m, str)

    // modositas
    m->vallfak[2].meret = 331;
    m->vallfak[1] = j6;
    m->vallfak[0].szin = "sarga";

    ASSERT_EQ(m2.vallfak[0], j5);
    ASSERT_EQ(m2.vallfak[1], j2);
    ASSERT_EQ(m2.vallfak[2], j1);

    delete m;

    ASSERT_STREQ(str, "Vpiros3");
}

TEST(ertekadas_operator, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Szekreny* m = new Szekreny(10);
    *m += j5;
    *m += j2;
    *m += j1;

    Szekreny m2(3);
    m2 += j1;
    m2 += j5;

    // teszt
    char str[200];
    IO("", m2 = m2, str)

    // modositas
    m->vallfak[2].szin = "kek";
    m->vallfak[1] = j6;
    m->vallfak[0].meret = 11;

    ASSERT_EQ(m2.vallfak[0], j1);
    ASSERT_EQ(m2.vallfak[1], j5);
    ASSERT_EQ(m2.vallfak[2], j0);

    delete m;

    ASSERT_STREQ(str, "");

    const Szekreny mm1(3);
    m2 = mm1;
}

TEST(destruktor, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Szekreny* m = new Szekreny(5);
    *m += j5;
    *m += j2;
    *m += j1;

    // teszt
    char str[200];
    IO("", delete m, str)

    ASSERT_STREQ(str, "Vfekete10Vfekete10Vpiros3Vkek5Vzold1");
}

TEST(destruktor, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Szekreny* m = new Szekreny(4);
    *m += j6;
    *m += j3;

    // teszt
    char str[200];
    IO("", delete m, str)

    ASSERT_STREQ(str, "Vfekete10Vfekete10Vfekete2Vfekete3");
}

TEST(destruktor, virtualis) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    class MM: public Szekreny {
    public:
        MM(unsigned int maximalisVallfaDarab) : Szekreny(maximalisVallfaDarab) {}
        virtual ~MM() { cout << "M"; }
    };

    Szekreny* m = new MM(3);
    *m += j1;
    *m += j4;

    // teszt
    char str[200];
    IO("", delete m, str)

    ASSERT_STREQ(str, "MVfekete10Vbarna3Vpiros3");
}