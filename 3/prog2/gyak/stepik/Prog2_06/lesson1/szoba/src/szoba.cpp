#include <iostream>
#include <string>

using namespace std;

struct Butor {
    int tipus;
    string szin;
    int x;
    int y;
    int z;

public:
    Butor(int tipus, const string &szin, int x, int y, int z) : tipus(tipus), szin(szin), x(x), y(y), z(z) {}
    Butor(): tipus(1), szin(""), x(0), y(0), z(0) {}
    virtual ~Butor() { cout << 'b' << x << y << z; }
};

struct Szonyeg {
    string szin;
    int x;
    int y;

    Szonyeg(const string &szin, int x, int y) : szin(szin), x(x), y(y) {}

    virtual ~Szonyeg() { cout << "s" << x << y; }
};

class Szoba {
    Butor* butorok;
    Szonyeg* szonyeg;
    unsigned int aktualis_butorszam;
    unsigned int max_butorszam;

public:
    Szoba(unsigned int maxButorszam) : max_butorszam(maxButorszam), aktualis_butorszam(0), szonyeg(nullptr) {
        butorok = new Butor[maxButorszam];
    }

    void szonyegCsere(const string& szin, int x, int y) {
        delete szonyeg;
        szonyeg = new Szonyeg(szin, x, y);
    }

    Szoba& operator+=(const Butor& butor) {
        if (aktualis_butorszam < max_butorszam) {
            butorok[aktualis_butorszam] = butor;
            aktualis_butorszam++;
        }

        return *this;
    }

    virtual ~Szoba() {
        delete szonyeg;
        delete[] butorok;
    }

    // megoldas

};