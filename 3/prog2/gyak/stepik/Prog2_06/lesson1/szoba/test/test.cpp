#include <gtest/gtest.h>

#define main main_0
#define class struct
#define private public
#include "../src/szoba.cpp"
#undef class
#undef private
#undef main

#include "../../tools.cpp"

Butor b0;
Butor b1(1, "a", 1, 2, 3);
Butor b2(2, "a", 2, 1, 2);
Butor b3(1, "b", 2, 2, 2);
Butor b4(3, "a", 1, 3, 3);

bool operator==(const Butor& bb1, const Butor& bb2) {
    return bb1.x == bb2.x && bb1.y == bb2.y && bb1.z == bb2.z && bb1.szin == bb2.szin && bb1.tipus == bb2.tipus;
}

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Szoba* r1 = new Szoba(4);
    *r1 += b1;
    *r1 += b2;
    *r1 += b3;
    r1->szonyegCsere("kek", 1, 2);


    Szoba* r2 = new Szoba(2);
    *r2 += b4;
    *r2 = *r1;

    r1->aktualis_butorszam = 2;
    r1->max_butorszam = 5;
    r1->szonyeg->szin = "sarga";
    r1->szonyeg->x = 3;
    r1->szonyeg->y = 4;

    *r1 += b4;
    r1->butorok[0].tipus = 30;
    r1->butorok[1].x = 100;

    ASSERT_EQ(r2->aktualis_butorszam, 3);
    ASSERT_EQ(r2->max_butorszam, 4);
    ASSERT_EQ(r2->szonyeg->szin, "kek");
    ASSERT_EQ(r2->szonyeg->x, 1);
    ASSERT_EQ(r2->szonyeg->y, 2);

    ASSERT_NE(r1->szonyeg, r2->szonyeg);
    ASSERT_NE(r1->butorok, r2->butorok);

    ASSERT_EQ(r2->butorok[0], b1);
    ASSERT_EQ(r2->butorok[1], b2);
    ASSERT_EQ(r2->butorok[2], b3);
    ASSERT_EQ(r2->butorok[3], b0);

    delete r1;
    delete r2;
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Szoba* r1 = new Szoba(4);
    *r1 += b1;
    *r1 += b2;
    *r1 += b3;
    r1->szonyegCsere("barna", 1, 2);


    Szoba* r2 = new Szoba(2);

    char str[100];
    *r2 += b4;
    IO("", *r2 = *r1, str)



    r1->aktualis_butorszam = 1;
    r1->max_butorszam = 5;
    r1->szonyeg->szin = "piros";
    r1->szonyeg->x = 3;
    r1->szonyeg->y = 6;

    *r1 += b4;
    r1->butorok[0].tipus = 30;
    r1->butorok[1].x = 100;

    ASSERT_EQ(r2->aktualis_butorszam, 3);
    ASSERT_EQ(r2->max_butorszam, 4);
    ASSERT_EQ(r2->szonyeg->szin, "barna");
    ASSERT_EQ(r2->szonyeg->x, 1);
    ASSERT_EQ(r2->szonyeg->y, 2);

    ASSERT_NE(r1->szonyeg, r2->szonyeg);
    ASSERT_NE(r1->butorok, r2->butorok);

    ASSERT_EQ(r2->butorok[0], b1);
    ASSERT_EQ(r2->butorok[1], b2);
    ASSERT_EQ(r2->butorok[2], b3);
    ASSERT_EQ(r2->butorok[3], b0);

    ASSERT_STREQ(str, "b000b133");

    delete r1;
    delete r2;
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Szoba* r1 = new Szoba(4);
    *r1 += b1;
    *r1 += b2;
    *r1 += b3;
    r1->szonyegCsere("barna", 1, 2);


    Szoba* r2 = new Szoba(2);

    char str[100];
    *r2 += b4;
    r2->szonyegCsere("rozsaszin", 10, 20);

    IO("", *r2 = *r1, str)



    r1->aktualis_butorszam = 1;
    r1->max_butorszam = 5;
    r1->szonyeg->szin = "piros";
    r1->szonyeg->x = 3;
    r1->szonyeg->y = 6;

    *r1 += b4;
    r1->butorok[0].tipus = 30;
    r1->butorok[1].x = 100;

    ASSERT_EQ(r2->aktualis_butorszam, 3);
    ASSERT_EQ(r2->max_butorszam, 4);
    ASSERT_EQ(r2->szonyeg->szin, "barna");
    ASSERT_EQ(r2->szonyeg->x, 1);
    ASSERT_EQ(r2->szonyeg->y, 2);

    ASSERT_NE(r1->szonyeg, r2->szonyeg);
    ASSERT_NE(r1->butorok, r2->butorok);

    ASSERT_EQ(r2->butorok[0], b1);
    ASSERT_EQ(r2->butorok[1], b2);
    ASSERT_EQ(r2->butorok[2], b3);
    ASSERT_EQ(r2->butorok[3], b0);

    if (strcmp(str, "s1020b000b133") != 0 && strcmp(str, "b000b133s1020") != 0) {
        FAIL();
    }

    delete r1;
    delete r2;
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Szoba* r1 = new Szoba(4);
    *r1 += b1;
    *r1 += b2;
    *r1 += b3;


    Szoba* r2 = new Szoba(2);

    char str[100];
    *r2 += b4;
    r2->szonyegCsere("rozsaszin", 10, 20);

    IO("", *r2 = *r1, str)

    *r1 += b4;

    ASSERT_NE(r1->butorok, r2->butorok);

    ASSERT_EQ(r2->szonyeg, nullptr);

    if (strcmp(str, "s1020b000b133") != 0 && strcmp(str, "b000b133s1020") != 0) {
        FAIL();
    }

    delete r1;
    delete r2;
}

TEST(Teszt, 05) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Szoba* r1 = new Szoba(4);
    *r1 += b1;
    *r1 += b2;
    *r1 += b3;


    Szoba* r2 = new Szoba(2);

    char str[100];
    *r2 += b4;

    r2->butorok[0].z = 4;

    IO("", *r2 = *r1, str)

    *r1 += b4;

    ASSERT_NE(r1->butorok, r2->butorok);

    ASSERT_EQ(r2->szonyeg, nullptr);

    if (strcmp(str, "b000b134") != 0) {
        FAIL();
    }

    delete r1;
    delete r2;
}