#include <iostream>
#include <string>

class Tegla {
    int allapot;

public:
    Tegla() = default;
    Tegla(int allapot): allapot(allapot) {}
    ~Tegla() { std::cout << "A"; }
};

class Talicska {
    unsigned meret;
    unsigned aktualis_teglak_szama;
    Tegla* teglak;

public:
    Talicska(unsigned int meret): meret(meret), aktualis_teglak_szama(0) {
        this->teglak = new Tegla[meret];
    }

    virtual ~Talicska() {
        delete[] teglak;
    }
};