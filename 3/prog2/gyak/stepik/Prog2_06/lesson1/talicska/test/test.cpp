#include <gtest/gtest.h>
#include <string>

using namespace std;

#define class struct
#define private public
#define main main_0
#include "../src/talicska.cpp"
#undef main
#undef private
#undef class

#include "../../tools.cpp"

TEST(Konstruktor, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Talicska* talicska = new Talicska(4);

    char str[200];
    IO("", delete talicska, str);
    ASSERT_STREQ(str, "AAAA");
}

TEST(Konstruktor, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Talicska* talicska = new Talicska(10);

    char str[200];
    IO("", delete talicska, str);
    ASSERT_STREQ(str, "AAAAAAAAAA");
}

TEST(Konstruktor, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Talicska* talicska = new Talicska(1);

    char str[200];
    IO("", delete talicska, str);
    ASSERT_STREQ(str, "A");
}

TEST(Konstruktor, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Talicska* talicska = new Talicska(3);
    talicska->teglak[0] = Tegla(7);
    talicska->teglak[1] = Tegla(7);
    talicska->teglak[2] = Tegla(7);

    char str[200];
    IO("", delete talicska, str);
    ASSERT_STREQ(str, "AAA");
}