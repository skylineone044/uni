#include <iostream>
#include <string>

using namespace std;

class Var {
    string nev;
    unsigned int meret;

public:
    Var(): nev(""), meret(1) {}
    Var(const string &nev, unsigned int meret) : nev(nev), meret(meret) {}
    virtual ~Var() { cout << "1"; }
};

Var* varepites(string nev, int meret) {
    return new Var(nev, meret);
}

Var* varepites(int meret) {
    return new Var[meret];
}
