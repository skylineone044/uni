#include <gtest/gtest.h>

#define main main_0
#define class struct
#define private public
#include "../src/varfoglalas.cpp"
#undef private
#undef class
#undef main

#include "../../tools.cpp"

bool operator==(const Var& v1, const Var& v2) {
    return v1.nev == v2.nev && v1.meret == v2.meret;
}

Var defaultVar;

TEST(Egy_var, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Var* v1 = varepites("macskavar", 5);
    ASSERT_EQ(v1->nev, "macskavar");
    ASSERT_EQ(v1->meret, 5);

    char str[10];
    IO("", delete v1, str);
    ASSERT_STREQ(str, "1");
}

TEST(Egy_var, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Var* v1 = varepites("masikvar", 5);
    ASSERT_EQ(v1->nev, "masikvar");
    ASSERT_EQ(v1->meret, 5);

    char str[10];
    IO("", delete v1, str);
    ASSERT_STREQ(str, "1");
}

TEST(Tobb_var, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Var* v1 = varepites(4);
    ASSERT_EQ(defaultVar, v1[0]);
    ASSERT_EQ(defaultVar, v1[1]);
    ASSERT_EQ(defaultVar, v1[2]);
    ASSERT_EQ(defaultVar, v1[3]);

    char str[10];
    IO("", delete[] v1, str);
    ASSERT_STREQ(str, "1111");
}

TEST(Tobb_var, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Var* v1 = varepites(23);
    for (int i = 0; i < 23; i++) {
        ASSERT_EQ(defaultVar, v1[i]);
    }

    char str[50];
    IO("", delete[] v1, str);
    ASSERT_STREQ(str, "11111111111111111111111");
}

TEST(Tobb_var, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Var* v1 = varepites(1);
    ASSERT_EQ(defaultVar, v1[0]);

    char str[10];
    IO("", delete[] v1, str);
    ASSERT_STREQ(str, "1");
}

TEST(Tobb_var, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Var* v1 = varepites(0);

    char str[10];
    IO("", delete[] v1, str);
    ASSERT_STREQ(str, "");
}