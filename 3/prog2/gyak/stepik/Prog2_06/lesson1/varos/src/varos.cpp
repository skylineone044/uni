#include <string>

using namespace std;

class Haz {
    unsigned alapterulet;
    bool felujitando;

public:
    Haz(): alapterulet(0), felujitando(false) {};
    Haz(unsigned int alapterulet, bool felujitando): alapterulet(alapterulet), felujitando(felujitando) {}
};

class Varos {
    string nev;

    Haz* hazak;
    unsigned aktualis_hazak;
    unsigned max_hazak;

public:
    Varos(const string& nev, unsigned int max_hazak): nev(nev), max_hazak(max_hazak), aktualis_hazak(0) {
        hazak = new Haz[max_hazak];
    }

    Varos(const Varos& v) {
        this->nev = v.nev;
        this->aktualis_hazak = v.aktualis_hazak;
        this->max_hazak = v.max_hazak;

        this->hazak = new Haz[this->max_hazak];

        for (int i = 0; i < aktualis_hazak; ++i) {
            this->hazak[i] = v.hazak[i];
        }
    }

    virtual ~Varos() {
        delete[] hazak;
    }
};