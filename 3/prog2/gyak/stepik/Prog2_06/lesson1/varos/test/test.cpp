#include <gtest/gtest.h>
#include <string>

using namespace std;

#define private public
#define class struct
#define main main_0
#include "../src/varos.cpp"
#undef main
#undef class
#undef private

bool operator==(const Haz& h1, const Haz& h2) {
    return h1.alapterulet == h2.alapterulet && h1.felujitando == h2.felujitando;
}

TEST(CopyKonstruktor, egyszeru_adattagok) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Varos v1("Szeged", 10);
    Varos v2(v1);
    ASSERT_EQ(v2.nev, "Szeged");
    ASSERT_EQ(v2.max_hazak, 10);
    ASSERT_EQ(v2.aktualis_hazak, 0);

    Varos v3("Pecs", 15);
    Varos v4(v3);
    ASSERT_EQ(v4.nev, "Pecs");
    ASSERT_EQ(v4.max_hazak, 15);
    ASSERT_EQ(v4.aktualis_hazak, 0);
}

TEST(CopyKonstruktor, dinamikus_memoria) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Haz h0;
    const Haz h1(70, true);
    const Haz h2(44, false);
    const Haz h3(93, true);
    const Haz h4(45, true);

    Varos v1("Szeged", 10);
    v1.hazak[0] = h1;
    v1.hazak[1] = h2;
    v1.aktualis_hazak = 2;

    Varos v2(v1);
    ASSERT_EQ(h1, v2.hazak[0]);
    ASSERT_EQ(h2, v2.hazak[1]);
    ASSERT_EQ(h0, v2.hazak[2]);

    v2.hazak[0] = h3;
    ASSERT_EQ(h1, v1.hazak[0]);
}