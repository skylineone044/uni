#include <iostream>
#include <string>

using namespace std;

class Virag {
    string nev;
    int meret;

public:
    Virag(): nev(""), meret(1) {}
    Virag(const string &nev, int meret) : nev(nev), meret(meret) {}
    virtual ~Virag() { cout << nev << meret; }
};

void viragElpusztul(Virag* v) {
    delete v;
    v = nullptr;
 }

void viragmezoElpusztul(Virag vv[]) {
    delete[] vv;
    vv = nullptr;
}