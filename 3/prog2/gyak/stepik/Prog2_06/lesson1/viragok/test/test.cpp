#include <gtest/gtest.h>

#define main main_0
#define class struct
#define private public
#define delete printf("A");delete
#include "../src/virag.cpp"
#undef delete
#undef private
#undef class
#undef main

#include "../../tools.cpp"

TEST(viragElpusztul, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Virag* v = new Virag("tulipan", 5);

    char str[100];
    IO("", viragElpusztul(v), str);
    if (strcmp(str, "Atulipan5") != 0) {
        FAIL();
    }
}

TEST(viragElpusztul, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Virag* v = new Virag("narcisz", 15);

    char str[100];
    IO("", viragElpusztul(v), str);
    if (strcmp(str, "Anarcisz15") != 0) {
        FAIL();
    }
}

TEST(viragmezoElpusztul, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Virag* v = new Virag[10];
    v[2].nev = "a";
    v[5].nev = "b";
    v[3].meret = 6;
    v[8].meret = 11;

    char str[100];
    IO("", viragmezoElpusztul(v), str)
    if (strcmp(str, "A11111b116a111") != 0) {
        FAIL();
    }
}

TEST(viragmezoElpusztul, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Virag* v = new Virag[2];
    v[0].nev = "aaaa";
    v[0].meret = 66;
    v[1].nev = "bbb";
    v[1].meret = 5;

    char str[100];
    IO("", viragmezoElpusztul(v), str)
    if (strcmp(str, "Abbb5aaaa66") != 0) {
        FAIL();
    }
}

TEST(viragmezoElpusztul, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Virag* v = new Virag[0];

    char str[100];
    IO("", viragmezoElpusztul(v), str)
    if (strcmp(str, "A") != 0) {
        FAIL();
    }
}
