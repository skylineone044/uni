#include <gtest/gtest.h>

#define main main_0
#define private public
#define protected public
#define class struct
#include "../src/butor.cpp"
#undef class
#undef protected
#undef private
#undef main

#include "../../tools.cpp"

bool operator==(const Vector3& v1, const Vector3& v2) {
    return v1.x == v2.x && v1.y == v2.y && v1.z == v2.z;
}

TEST(Vector3, konstruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Vector3 v(1, 2, 3.2);
    ASSERT_NEAR(v.x, 1, 0.0001);
    ASSERT_NEAR(v.y, 2, 0.0001);
    ASSERT_NEAR(v.z, 3.2, 0.0001);

    Vector3 v2(2.23, 4.252, 0.1341);
    ASSERT_NEAR(v2.x, 2.23, 0.0001);
    ASSERT_NEAR(v2.y, 4.252, 0.0001);
    ASSERT_NEAR(v2.z, 0.1341, 0.0001);
}

TEST(Butor, konstruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Vector3 v(4, 2.2, 3.3);
    Butor b(v, "piros");
    ASSERT_EQ(b.meret, v);
    ASSERT_EQ(b.szin, "piros");

    Vector3 v2(1, 0.2, 2.5);
    Butor b2(v2, "zold");
    ASSERT_EQ(b2.meret, v2);
    ASSERT_EQ(b2.szin, "zold");
}

TEST(Butor, atfest) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Vector3 v(4, 2.2, 3.3);
    Butor b(v, "piros");
    b.atfest("lila");
    ASSERT_EQ(b.szin, "lila");

    b.atfest("kek");
    ASSERT_EQ(b.szin, "kek");
    ASSERT_EQ(b.meret, v);
}

TEST(Komod, oroklodes) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Vector3 v(4,4,4);
    Butor* b = new Komod(v, "kek", 4);
    ASSERT_TRUE(isConst(b->meret));

    b->atfest("zold");
    ASSERT_EQ(b->szin, "zold");

    b->atfest("sarga");
    ASSERT_EQ(b->szin, "sarga");

    delete b;
}

TEST(Komod, konstruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Vector3 v(4, 2.2, 3.3);
    Komod k(v, "zold", 4);

    ASSERT_EQ(k.fiokok_szama, 4);
    ASSERT_EQ(k.meret, v);
    ASSERT_EQ(k.szin, "zold");

    const Vector3 v2(14, 22.2, 34.223);
    Komod k2(v2, "sarga", 2);

    ASSERT_EQ(k2.fiokok_szama, 2);
    ASSERT_EQ(k2.meret, v2);
    ASSERT_EQ(k2.szin, "sarga");
}

TEST(Komod, fiok_magassag) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Vector3 v(4, 2.2, 3.3);
    Komod k(v, "zold", 4);
    ASSERT_NEAR(k.fiok_magassag(), 0.55, 0.00001);

    const Vector3 v2(4, 8.8, 3.3);
    Komod k2(v2, "zold", 6);
    ASSERT_NEAR(k2.fiok_magassag(), 1.4666666666, 0.00001);

    const Vector3 v3(4, 4, 3.3);
    Komod k3(v3, "zold", 4);
    ASSERT_NEAR(k3.fiok_magassag(), 1, 0.00001);
}