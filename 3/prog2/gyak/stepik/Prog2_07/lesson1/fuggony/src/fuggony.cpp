#include <iostream>
#include <string>

using namespace std;

class Gep {
protected:
    int energia_igeny;
    int meret;

public:
    Gep(int energiaIgeny, int meret) : energia_igeny(energiaIgeny), meret(meret) {}
    virtual ~Gep() = default;
};

class Fuggony {
protected:
    string anyag;
    string szin;
    int meret;

public:
    Fuggony(const string &anyag, const string &szin, int meret) : anyag(anyag), szin(szin), meret(meret) {}
    virtual ~Fuggony() = default;
};

class AutomataFuggony: public Fuggony, public Gep {
public:
    AutomataFuggony(int energiaIgeny, int meret1, const string &anyag, const string &szin, int meret) : Fuggony(anyag,
                                                                                                                szin,
                                                                                                                meret),
                                                                                                        Gep(energiaIgeny,
                                                                                                            meret1) {}

    void fejleszt()  {
        this->Gep::meret++;
        this->energia_igeny++;
    }

    void nagyobbit() {
        this->Fuggony::meret++;
    }
};