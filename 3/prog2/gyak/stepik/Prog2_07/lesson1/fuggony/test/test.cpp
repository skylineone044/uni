#include <gtest/gtest.h>

#define main main_0
#define protected public
#include "../src/fuggony.cpp"
#undef protected
#undef main

TEST(Gep, konstruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Gep g(5, 10);
    ASSERT_EQ(g.energia_igeny, 5);
    ASSERT_EQ(g.meret, 10);

    Gep g2(25, 2);
    ASSERT_EQ(g2.energia_igeny, 25);
    ASSERT_EQ(g2.meret, 2);
}

TEST(Fuggony, konstruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Fuggony f("valami", "sarga", 5);
    ASSERT_EQ(f.anyag, "valami");
    ASSERT_EQ(f.szin, "sarga");
    ASSERT_EQ(f.meret, 5);

    Fuggony f2("BBB", "ccc", 33);
    ASSERT_EQ(f2.anyag, "BBB");
    ASSERT_EQ(f2.szin, "ccc");
    ASSERT_EQ(f2.meret, 33);
}

TEST(AutomataFuggony, oroklodes) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Fuggony* f = new AutomataFuggony(2, 2, "", "", 2);
    Gep* g = new Gep(2, 2);
    ASSERT_NE(f, nullptr);
    ASSERT_NE(g, nullptr);
    delete f;
    delete g;
}

TEST(AutomataFuggony, konstruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    AutomataFuggony* f = new AutomataFuggony(10, 20, "selyem", "piros", 22);
    ASSERT_EQ(f->energia_igeny, 10);
    ASSERT_EQ(f->Gep::meret, 20);
    ASSERT_EQ(f->anyag, "selyem");
    ASSERT_EQ(f->szin, "piros");
    ASSERT_EQ(f->Fuggony::meret, 22);
    delete f;

    AutomataFuggony* g = new AutomataFuggony(11, 21, "selyem+", "piros++", 10);
    ASSERT_EQ(g->energia_igeny, 11);
    ASSERT_EQ(g->Gep::meret, 21);
    ASSERT_EQ(g->anyag, "selyem+");
    ASSERT_EQ(g->szin, "piros++");
    ASSERT_EQ(g->Fuggony::meret, 10);
    delete g;
}

TEST(AutomataFuggony, fejlesztEsNagyobbit) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    AutomataFuggony *f = new AutomataFuggony(10, 20, "selyem", "piros", 22);

    ASSERT_EQ(f->Gep::meret, 20);
    ASSERT_EQ(f->Fuggony::meret, 22);

    f->fejleszt();
    ASSERT_EQ(f->Gep::meret, 21);
    ASSERT_EQ(f->Fuggony::meret, 22);

    f->fejleszt();
    ASSERT_EQ(f->Gep::meret, 22);
    ASSERT_EQ(f->Fuggony::meret, 22);

    f->nagyobbit();
    ASSERT_EQ(f->Gep::meret, 22);
    ASSERT_EQ(f->Fuggony::meret, 23);

    f->nagyobbit();
    ASSERT_EQ(f->Gep::meret, 22);
    ASSERT_EQ(f->Fuggony::meret, 24);

    f->fejleszt();
    ASSERT_EQ(f->Gep::meret, 23);
    ASSERT_EQ(f->Fuggony::meret, 24);

    delete f;
}