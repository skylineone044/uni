#include <iostream>
#include <string>

using namespace std;

class Iroeszkoz {
    string szin;
    bool bekapcsolva;

public:
    Iroeszkoz(const string &szin, bool bekapcsolva = false) : szin(szin), bekapcsolva(bekapcsolva) {}

    void bekapcsol() {
        bekapcsolva = true;
    }

    void kikapcsol() {
        bekapcsolva = false;
    }

};

class Toll: public Iroeszkoz {
    double vastagsag;

public:
    Toll(const string &szin, bool bekapcsolva, double vastagsag) : Iroeszkoz(szin, bekapcsolva), vastagsag(vastagsag) {}

    void iras(const string& szoveg) {
        Toll::bekapcsol();
        cout << szoveg << endl;
    }
};