#include <gtest/gtest.h>

#define main main_0
#define class struct
#define private public
#include "../src/iroeszkoz.cpp"
#undef private
#undef class
#undef main

#include "../../tools.cpp"

TEST(Konstruktor, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Iroeszkoz* i = new Toll("rozsaszin", false, 5.55);
    ASSERT_EQ(i->bekapcsolva, false);
    ASSERT_EQ(i->szin, "rozsaszin");
    delete i;

    Toll t("piros", true, 3.12);
    ASSERT_EQ(t.szin, "piros");
    ASSERT_EQ(t.bekapcsolva, true);
    ASSERT_EQ(t.vastagsag, 3.12);
}

TEST(iras, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Toll t("kek", false, 7.777);
    char str[100];
    IO("", t.iras("cica lovacska tehenecske gombocska"), str);
    ASSERT_EQ(t.szin, "kek");
    ASSERT_EQ(t.bekapcsolva, true);
    ASSERT_NEAR(t.vastagsag, 7.777, 0.001);
    ASSERT_STREQ(str, "cica lovacska tehenecske gombocska\n");
}

TEST(iras, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Toll t("sarga", true, 15.5);
    char str[100];
    IO("", t.iras("a kenyer tart minket eletben"), str);
    ASSERT_EQ(t.szin, "sarga");
    ASSERT_EQ(t.bekapcsolva, true);
    ASSERT_NEAR(t.vastagsag, 15.5, 0.001);
    ASSERT_STREQ(str, "a kenyer tart minket eletben\n");
}