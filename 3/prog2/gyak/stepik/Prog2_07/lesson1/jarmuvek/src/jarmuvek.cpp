#include <iostream>
#include <string>

using namespace std;

class Jarmu {
private:
    string megnevezes;

public:
    Jarmu(const string &megnevezes) : megnevezes(megnevezes) {}

    const string &get_megnevezes() const {
        return megnevezes;
    }
};

class Busz: public Jarmu {
public:
    Busz() : Jarmu("busz") {}
};

class Villamos: public Jarmu {
public:
    Villamos() : Jarmu("villamos") {}

};