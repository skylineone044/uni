#include <gtest/gtest.h>

#define main main_0
#include "../src/jarmuvek.cpp"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Jarmu* busz = new Busz;
    Jarmu* villamos = new Villamos;

    ASSERT_EQ(busz->get_megnevezes(), "busz");
    ASSERT_EQ(villamos->get_megnevezes(), "villamos");

    delete busz;
    delete villamos;

    Jarmu j("asd");
    ASSERT_EQ(j.get_megnevezes(), "asd");
}