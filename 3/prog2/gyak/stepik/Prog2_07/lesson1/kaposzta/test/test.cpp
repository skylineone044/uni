#include <gtest/gtest.h>
#include <string>

using namespace std;

#define protected public
#define struct class
#define main main_0
#include "../src/kaposzta.cpp"
#undef main
#undef struct
#undef protected

#include "../../tools.cpp"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Kaposzta k(7);
    ASSERT_EQ(k.meret, 7);

    char str[1000];
    GETSOURCE("kaposzta", "kaposzta.cpp", str)
    string s = str;

    if (s.find("protected") == string::npos) {
        cout << endl << "Az adattagot ne erhesse el barki, csak a kaposzta osztaly es annak gyerekosztalyai!" << endl << endl;
        FAIL();
    }
}