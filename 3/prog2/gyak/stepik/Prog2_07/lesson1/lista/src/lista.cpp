#include <iostream>

class Lista {
    int* szamok;
    int darabszam;

public:
    Lista(): darabszam(0), szamok(nullptr) {}

    virtual Lista& operator+=(int ertek) {
        int* uj_szamok = new int[darabszam + 1];

        if (szamok != nullptr) {
            for (int i = 0; i < darabszam; i++) {
                uj_szamok[i] = szamok[i];
            }
        }

        delete[] szamok;
        szamok = uj_szamok;

        szamok[darabszam] = ertek;
        darabszam++;

        return *this;
    }

    Lista(const Lista& masik) {
        darabszam = masik.darabszam;
        szamok = new int[darabszam];
        for (int i = 0; i < darabszam; i++) {
            szamok[i] = masik.szamok[i];
        }
    }

    Lista& operator=(const Lista& masik) {
        if (this == &masik) {
            return *this;
        }

        delete[] szamok;

        darabszam = masik.darabszam;
        szamok = new int[darabszam];
        for (int i = 0; i < darabszam; i++) {
            szamok[i] = masik.szamok[i];
        }

        return *this;
    }

    virtual ~Lista() {
        delete[] szamok;
    }

    virtual Lista& operator,(int i) {
        operator+=(i);
        return *this;
    }

    const int* get_szamok() const {
        return szamok;
    }

};

class ParosLista: public Lista {
public:
    ParosLista() : Lista() {}

    ParosLista& operator+=(int ertek) override {
        if (ertek % 2 == 0) {
            Lista::operator+=(ertek);
        }
        return *this;
    }

    ParosLista& operator,(int i) override {
        *this += i;
        return *this;
    }
};