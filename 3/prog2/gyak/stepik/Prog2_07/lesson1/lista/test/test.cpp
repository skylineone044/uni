#include <gtest/gtest.h>

#define main main_0
#include "../src/lista.cpp"
#undef main

#include "../../tools.cpp"

TEST(Lista, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Lista l;
    l += 7, 4, 4, 3, 3, 2;
    (l += 3) += 2;
    l , 2;
    const int* szamok = l.get_szamok();
    int elvart[] = {7, 4, 4, 3, 3, 2, 3, 2, 2};
    ASSERT_ARRAY_EQ(szamok, elvart, 9)
}

TEST(Lista, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Lista l;
    l += 1, 4, -1, -4, 2, 6, 4, 2, 2, 2;
    ((l += 7) += 7) += 22;
    l , 2 , 3;
    const int* szamok = l.get_szamok();
    int elvart[] = {1, 4, -1, -4, 2, 6, 4, 2, 2, 2, 7, 7, 22, 2, 3};
    ASSERT_ARRAY_EQ(szamok, elvart, 15)
}

TEST(ParosLista, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ParosLista l;
    l += 7, 4, 4, 3, 3, 2;
    (l += 3) += 2;
    l , 2;
    const int* szamok = l.get_szamok();
    int elvart[] = {4, 4, 2, 2, 2};
    ASSERT_ARRAY_EQ(szamok, elvart, 5)
}

TEST(ParosLista, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    ParosLista l;
    l += 1, 4, -1, -4, 2, 6, 4, 2, 2, 2;
    ((l += 7) += 7) += 22;
    l , 2 , 3;
    const int* szamok = l.get_szamok();
    int elvart[] = {4, -4, 2, 6, 4, 2, 2, 2, 22, 2};
    ASSERT_ARRAY_EQ(szamok, elvart, 10)
}

TEST(ParosLista, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Lista* l = new ParosLista;
    *l += 5, 5, -2, -7, 2120, 6, 1111, 22222221, 222222, 0;
    ((*l += 6) += 7) += 19;
    *l , 2 , 3;
    const int* szamok = l->get_szamok();
    int elvart[] = {-2, 2120, 6, 222222, 0, 6, 2};
    ASSERT_ARRAY_EQ(szamok, elvart, 7)
    delete l;
}

TEST(ParosLista, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Lista* l = new ParosLista;
    *l += 1, 1, 1, 1, 2;
    ((*l += 2) += 4) += 1;
    *l , -10, 11;
    const int* szamok = l->get_szamok();
    int elvart[] = {2, 2, 4, -10};
    ASSERT_ARRAY_EQ(szamok, elvart, 4)
    delete l;
}