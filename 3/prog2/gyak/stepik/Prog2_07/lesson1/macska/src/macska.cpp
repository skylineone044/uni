#include <iostream>
#include <string>

using namespace std;

class Macska {
    const string nev;
    unsigned int eletkor;
    unsigned int nyavogas_szam;

public:
    Macska(const string &nev, unsigned int eletkor) : nev(nev), eletkor(eletkor), nyavogas_szam(0) {}

    virtual string nyavog() {
        this->nyavogas_szam++;
        return "miau";
    }

};

class Urmacska: public Macska {
    bool urhajoban;

public:
    Urmacska(const string &nev, unsigned int eletkor) : Macska(nev, eletkor), urhajoban(false) {}

    string nyavog() override {
        Macska::nyavog();
        return "miiaauu";
    }

    void megfontoltReakcio() {
        if (this->urhajoban) {
            cout << Macska::nyavog();
        } else {
            cout << nyavog();
        }
    }
};