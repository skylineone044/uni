#include <gtest/gtest.h>

#define main main_0
#define class struct
#define private public
#include "../src/macska.cpp"
#undef private
#undef class
#undef main

#include "../../tools.cpp"

TEST(Macska, nyavog) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Macska m("cica", 7);
    string res = m.nyavog();
    ASSERT_EQ(m.nyavogas_szam, 1);
    ASSERT_EQ(res, "miau");

    m.nyavog();
    m.nyavog();
    m.nyavog();
    ASSERT_EQ(m.nyavogas_szam, 4);
}

TEST(Urmacska, konstruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Urmacska u("BBBMacska", 44);
    ASSERT_EQ(u.nev, "BBBMacska");
    ASSERT_EQ(u.eletkor, 44);
    ASSERT_EQ(u.nyavogas_szam, 0);
    ASSERT_EQ(u.urhajoban, false);

    Urmacska u2("BBB-BBBMacska", 1);
    ASSERT_EQ(u2.nev, "BBB-BBBMacska");
    ASSERT_EQ(u2.eletkor, 1);
    ASSERT_EQ(u2.nyavogas_szam, 0);
    ASSERT_EQ(u2.urhajoban, false);
}

TEST(Urmacska, nyavog) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Urmacska u("BBB", 40);
    string res = u.nyavog();
    ASSERT_EQ(res, "miiaauu");
    ASSERT_EQ(u.nyavogas_szam, 1);

    for (int i = 0; i < 13030; i++) {
        u.nyavog();
    }

    ASSERT_EQ(u.nyavogas_szam, 13031);

    Macska* m = new Urmacska("AAA", 1);
    res = m->nyavog();
    ASSERT_EQ(res, "miiaauu");
    ASSERT_EQ(m->nyavogas_szam, 1);
}

TEST(Urmacska, megfontoltReakcio_1) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Urmacska u("CICA", 3);
    char str[100];
    IO("", u.megfontoltReakcio(), str)
    ASSERT_STREQ(str, "miiaauu");
    ASSERT_EQ(u.nyavogas_szam, 1);
}

TEST(Urmacska, megfontoltReakcio_2) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Urmacska u("CICA", 3);
    char str[100];
    u.urhajoban = true;
    IO("", u.megfontoltReakcio(), str)
    ASSERT_STREQ(str, "miau");
    ASSERT_EQ(u.nyavogas_szam, 1);
}

TEST(Urmacska, megfontoltReakcio_3) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Urmacska u("CICA", 3);
    char str[200];

    IO("",
        ASSERT_EQ(u.nyavogas_szam, 0);
        u.urhajoban = false;
        u.megfontoltReakcio();
        ASSERT_EQ(u.nyavogas_szam, 1);
        u.megfontoltReakcio();
        ASSERT_EQ(u.nyavogas_szam, 2);
        u.urhajoban = true;
        u.megfontoltReakcio();
        ASSERT_EQ(u.nyavogas_szam, 3);
        u.urhajoban = false;
        u.megfontoltReakcio();
        ASSERT_EQ(u.nyavogas_szam, 4), str)
    ASSERT_STREQ(str, "miiaauumiiaauumiaumiiaauu");
}