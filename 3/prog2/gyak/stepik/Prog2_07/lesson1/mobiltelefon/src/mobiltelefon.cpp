#include <iostream>
#include <string>

using namespace std;

class Mobiltelefon {
protected:
    int hangero;

public:
    Mobiltelefon(int hangero) : hangero(hangero) {}

    virtual string csorog() const {
        return "[" + to_string(hangero) + "] bip-bip bip-bip bip-bip";
    }
};

class Okostelefon: public Mobiltelefon {
    string operacios_rendszer;

public:
    Okostelefon(int hangero, const string &operaciosRendszer):
        Mobiltelefon(hangero), operacios_rendszer(operaciosRendszer) {}

    string csorog() const override {
        return "[" + to_string(hangero) + "] trallalalala huhuhu trallalalala huhuhu";
    }

};

void csorget(Mobiltelefon melyiket) {
    if (melyiket)
    melyiket.csorog();
}
