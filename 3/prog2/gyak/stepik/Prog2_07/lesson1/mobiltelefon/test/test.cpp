#include <gtest/gtest.h>

#define main main_0
#include "../src/mobiltelefon.cpp"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Mobiltelefon* t = new Mobiltelefon(3);
    ASSERT_EQ(t->csorog(), "[3] bip-bip bip-bip bip-bip");
    ASSERT_EQ(csorget(*t), "[3] bip-bip bip-bip bip-bip");
    delete t;
}

TEST(Teszt, 02) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Okostelefon* t = new Okostelefon(6, "android");
    ASSERT_EQ(t->csorog(), "[6] trallalalala huhuhu trallalalala huhuhu");
    ASSERT_EQ(csorget(*t), "[6] trallalalala huhuhu trallalalala huhuhu");
    delete t;
}

TEST(Teszt, 03) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Mobiltelefon* t = new Okostelefon(22, "android");
    ASSERT_EQ(t->csorog(), "[22] trallalalala huhuhu trallalalala huhuhu");
    ASSERT_EQ(csorget(*t), "[22] trallalalala huhuhu trallalalala huhuhu");
    delete t;
}

TEST(Teszt, 04) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    const Okostelefon* t = new Okostelefon(0, "android");
    ASSERT_EQ(t->csorog(), "[0] trallalalala huhuhu trallalalala huhuhu");

    const Mobiltelefon* t2 = new Mobiltelefon(419);
    ASSERT_EQ(t2->csorog(), "[419] bip-bip bip-bip bip-bip");

    delete t;
}