#include <gtest/gtest.h>

using namespace std;

#define main main_0
#include "../src/orak.cpp"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Ora* a = new Faliora;
    Ora* b = new Karora;
    Ora* c = new Ajtoora;

    ASSERT_NE(a, nullptr);
    ASSERT_NE(b, nullptr);
    ASSERT_NE(c, nullptr);
}