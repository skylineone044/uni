#include <iostream>
#include <string>

using namespace std;

class Penz {
    string tulajdonos;
    int mennyiseg;

public:
    Penz(const string &tulajdonos, int mennyiseg) : tulajdonos(tulajdonos), mennyiseg(mennyiseg) {}

};

class TitkosTaller: public Penz {
    int titkossagi_mertek;

public:
    TitkosTaller(const string &tulajdonos, int mennyiseg, int titkossagiMertek) : Penz(tulajdonos, mennyiseg),
                                                                                  titkossagi_mertek(titkossagiMertek) {}
};