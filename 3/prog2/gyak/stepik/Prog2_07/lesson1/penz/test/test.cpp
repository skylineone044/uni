#include <gtest/gtest.h>

#define main main_0
#define class struct
#define private public
#include "../src/penz.cpp"
#undef private
#undef class
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    TitkosTaller tt("asd", 5, 7);
    ASSERT_EQ(tt.tulajdonos, "asd");
    ASSERT_EQ(tt.mennyiseg, 5);
    ASSERT_EQ(tt.titkossagi_mertek, 7);

    TitkosTaller ttt("bbb", 7, 10);
    ASSERT_EQ(ttt.tulajdonos, "bbb");
    ASSERT_EQ(ttt.mennyiseg, 7);
    ASSERT_EQ(ttt.titkossagi_mertek, 10);

    Penz* p = new TitkosTaller("abab", 6, 8);
    ASSERT_EQ(p->tulajdonos, "abab");
    ASSERT_EQ(p->mennyiseg, 6);
    delete p;
}