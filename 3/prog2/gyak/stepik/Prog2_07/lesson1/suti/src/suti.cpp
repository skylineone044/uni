#include <iostream>
#include <string>

using namespace std;

class Suti {
    string nev;
    bool finom;

public:
    Suti(const string& nev, bool finom): nev(nev), finom(finom) {
        cout << "megsult!" << endl;
    }
};

class Mezeskalacs: Suti {
    string forma;
    
public:
    Mezeskalacs(bool finom, const string& forma): Suti("mezeskalacs", finom), forma(forma) {}
};