#include <gtest/gtest.h>
#include <string>

using namespace std;

#define private public
#define class struct
#define main main_0
#include "../src/suti.cpp"
#undef main
#undef class
#undef private

#include "../../tools.cpp"

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    char str[100];
    IO("",
        Suti s("asd", true);
        cout << "*";
        Mezeskalacs cs(true, "kerek");
        cout << "**",
       str);

    ASSERT_EQ(cs.nev, "mezeskalacs");
    ASSERT_EQ(cs.finom, true);
    ASSERT_EQ(cs.forma, "kerek");

    ASSERT_STREQ(str, "megsult!\n*megsult!\n**");
}