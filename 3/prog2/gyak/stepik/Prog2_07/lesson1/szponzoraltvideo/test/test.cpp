#include <gtest/gtest.h>

#define main main_0
#define private public
#define protected public
#define class struct
#include "../src/szponzoralt.cpp"
#undef class
#undef protected
#undef private
#undef main

bool operator==(const Tartalomgyarto& t1, const Tartalomgyarto& t2) {
    return t1.penz == t2.penz && t1.nev == t2.nev;
}

TEST(Tartalomgyarto, hozzaadas) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Tartalomgyarto t("Marika");
    ASSERT_EQ(t.nev, "Marika");
    ASSERT_EQ(t.penz, 0);

    t += 77;
    ASSERT_EQ(t.penz, 77);

    (t += 5) += 2;
    ASSERT_EQ(t.penz, 84);

    Tartalomgyarto t2("Marika");
    t2 += 2;
    ASSERT_EQ(t2.penz, 2);
}

TEST(Video, konstruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Tartalomgyarto f("Jozska");
    Video v("ASD", "masik ASD", f, 33);

    ASSERT_EQ(v.cim, "ASD");
    ASSERT_EQ(v.leiras, "masik ASD");
    ASSERT_EQ(v.hossz, 33);
    ASSERT_EQ(v.like, 0);
    ASSERT_EQ(v.dislike, 0);
    ASSERT_EQ(v.feltolto, f);
}

TEST(Video, bevetelszerzes) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Tartalomgyarto f("Jozska");
    Video v("ASD", "masik ASD", f, 33);

    ++v;
    ASSERT_EQ(v.like, 0);
    ASSERT_EQ(v.dislike, 0);
    ASSERT_EQ(v.feltolto.penz, 1);

    ++v;
    ASSERT_EQ(v.like, 0);
    ASSERT_EQ(v.dislike, 0);
    ASSERT_EQ(v.feltolto.penz, 2);

    ++(++v);
    ASSERT_EQ(v.like, 0);
    ASSERT_EQ(v.dislike, 0);
    ASSERT_EQ(v.feltolto.penz, 4);
}

TEST(Video, like_dislike) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Tartalomgyarto f("Jozska");
    Video v("ASD", "masik ASD", f, 33);

    v++;
    ASSERT_EQ(v.like, 1);
    ASSERT_EQ(v.dislike, 0);
    ASSERT_EQ(v.feltolto.penz, 1);

    v++;
    ASSERT_EQ(v.like, 2);
    ASSERT_EQ(v.dislike, 0);
    ASSERT_EQ(v.feltolto.penz, 2);

    ++v;

    v++;
    ASSERT_EQ(v.like, 3);
    ASSERT_EQ(v.dislike, 0);
    ASSERT_EQ(v.feltolto.penz, 4);

    v--;
    ASSERT_EQ(v.like, 3);
    ASSERT_EQ(v.dislike, 1);
    ASSERT_EQ(v.feltolto.penz, 5);

    ++v;
    v--;
    ASSERT_EQ(v.like, 3);
    ASSERT_EQ(v.dislike, 2);
    ASSERT_EQ(v.feltolto.penz, 7);

    v++;
    ASSERT_EQ(v.like, 4);
    ASSERT_EQ(v.dislike, 2);
    ASSERT_EQ(v.feltolto.penz, 8);

    (((v++)--)--)++;
}

TEST(Reklam, konstruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Reklam r("Nagy doboz kft", 50);
    ASSERT_EQ(r.reklamozo, "Nagy doboz kft");
    ASSERT_EQ(r.megjelenitesenkenti_koltseg, 50);

    Reklam r2("No doboz bt", 22.2);
    ASSERT_EQ(r2.reklamozo, "No doboz bt");
    ASSERT_EQ(r2.megjelenitesenkenti_koltseg, 22.2);
}

TEST(SzponzoraltVideo, oroklodes) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Tartalomgyarto t("a");
    Video* v = new SzponzoraltVideo("a", "a", t, 1, "a", 1, false, 1);
    Reklam* r = new SzponzoraltVideo("a", "a", t, 1, "a", 1, false, 1);

    ASSERT_NE(v, nullptr);
    ASSERT_NE(r, nullptr);

    delete v;
    delete r;
}

TEST(SzponzoraltVideo, konstruktor) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Tartalomgyarto t("Gabor");
    SzponzoraltVideo* v = new SzponzoraltVideo("A", "B", t, 11, "aa", 10, true, 50);
    ASSERT_EQ(v->cim, "A");
    ASSERT_EQ(v->leiras, "B");
    ASSERT_EQ(v->feltolto, t);
    ASSERT_EQ(v->hossz, 11);
    ASSERT_EQ(v->reklamozo, "aa");
    ASSERT_NEAR(v->megjelenitesenkenti_koltseg, 10, 0.001);
    ASSERT_EQ(v->egyedi, true);
    ASSERT_EQ(v->reszesedes, 50);
    delete v;

    Tartalomgyarto t2("Anna");
    SzponzoraltVideo* v2 = new SzponzoraltVideo("AA", "BBB", t2, 30, "bbb", 40.2, false, 33);
    ASSERT_EQ(v->cim, "AA");
    ASSERT_EQ(v->leiras, "BBB");
    ASSERT_EQ(v->feltolto, t2);
    ASSERT_EQ(v->hossz, 30);
    ASSERT_EQ(v->reklamozo, "bbb");
    ASSERT_NEAR(v->megjelenitesenkenti_koltseg, 40.2, 0.001);
    ASSERT_EQ(v->egyedi, false);
    ASSERT_EQ(v->reszesedes, 33);
    delete v2;
}

TEST(SzponzoraltVideo, bevetelszerzes) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Tartalomgyarto t("Gabor");
    Video* v = new SzponzoraltVideo("A", "B", t, 11, "aa", 12, true, 50);
    ASSERT_EQ(v->like, 0);
    ASSERT_EQ(v->dislike, 0);
    ASSERT_EQ(v->feltolto.penz, 0);

    ++(*v);
    ASSERT_EQ(v->like, 0);
    ASSERT_EQ(v->dislike, 0);
    ASSERT_EQ(v->feltolto.penz, 6);

    ++(*v);
    ASSERT_EQ(v->like, 0);
    ASSERT_EQ(v->dislike, 0);
    ASSERT_EQ(v->feltolto.penz, 12);

    (*v)++;
    ASSERT_EQ(v->like, 1);
    ASSERT_EQ(v->dislike, 0);
    ASSERT_EQ(v->feltolto.penz, 18);

    (*v)++;
    ASSERT_EQ(v->like, 2);
    ASSERT_EQ(v->dislike, 0);
    ASSERT_EQ(v->feltolto.penz, 24);

    (*v)--;
    ASSERT_EQ(v->like, 2);
    ASSERT_EQ(v->dislike, 1);
    ASSERT_EQ(v->feltolto.penz, 30);

    (*v)--;
    ASSERT_EQ(v->like, 2);
    ASSERT_EQ(v->dislike, 2);
    ASSERT_EQ(v->feltolto.penz, 36);

    (*v)++;
    ASSERT_EQ(v->like, 3);
    ASSERT_EQ(v->dislike, 2);
    ASSERT_EQ(v->feltolto.penz, 42);

    SzponzoraltVideo s("A", "B", t, 11, "aa", 10, true, 79);
    ++s;
    ASSERT_EQ(s.like, 0);
    ASSERT_EQ(s.dislike, 0);
    ASSERT_EQ(s.feltolto.penz, 7);

    ++s;
    ASSERT_EQ(s.like, 0);
    ASSERT_EQ(s.dislike, 0);
    ASSERT_EQ(s.feltolto.penz, 14);

    s--;
    ASSERT_EQ(s.like, 0);
    ASSERT_EQ(s.dislike, 1);
    ASSERT_EQ(s.feltolto.penz, 21);

    ++s;
    ASSERT_EQ(s.like, 0);
    ASSERT_EQ(s.dislike, 1);
    ASSERT_EQ(s.feltolto.penz, 28);

    s++;
    ASSERT_EQ(s.like, 1);
    ASSERT_EQ(s.dislike, 1);
    ASSERT_EQ(s.feltolto.penz, 35);

    s++;
    ASSERT_EQ(s.like, 2);
    ASSERT_EQ(s.dislike, 1);
    ASSERT_EQ(s.feltolto.penz, 42);

    s--;
    ASSERT_EQ(s.like, 2);
    ASSERT_EQ(s.dislike, 2);
    ASSERT_EQ(s.feltolto.penz, 49);

    ++(((s++)--)++)++;
}