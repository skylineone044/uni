#include <gtest/gtest.h>

#define main main_0
#include "../src/szurospajzs.cpp"
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Pajzs* p = nullptr;
    Fegyver* f = nullptr;
    SzurosPajzs* szp = nullptr;
    SzurosPajzs szp2;

    Pajzs* p2 = new SzurosPajzs;
    Fegyver* f2 = new SzurosPajzs;

    ASSERT_EQ(p, nullptr);
    ASSERT_EQ(f, nullptr);
    ASSERT_EQ(szp, nullptr);
    ASSERT_NE(&szp2, nullptr);
    ASSERT_NE(p2, nullptr);
    ASSERT_NE(f2, nullptr);
}