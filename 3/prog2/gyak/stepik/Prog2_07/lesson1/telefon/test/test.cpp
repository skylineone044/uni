#include <gtest/gtest.h>

#define main main_0
#define class struct
#define private public
#include "../src/telefon.cpp"
#undef class
#undef private
#undef main

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    Telefon t1 {};
    t1.kijelzo_meret = 0;
    t1.ar = 0;

    Telefon* t2 = new Okostelefon;
    t2->kijelzo_meret = 412412;
    t2->ar = 124124;

    auto t3 = (Okostelefon*) t2;
    ASSERT_EQ(t3->kijelzo_meret, 412412);
    ASSERT_EQ(t3->ar, 124124);

    t2->ar = 444;
    ASSERT_EQ(t3->ar, 444);
}