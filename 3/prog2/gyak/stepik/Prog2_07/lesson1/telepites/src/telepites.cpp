#include <iostream>
#include <string>

using namespace std;

struct Alkalmazas {
    string nev;
    bool biztonsagos;
};

class Telefon {
    string alkalmazasok;

public:
    Telefon() = default;

    virtual bool telepit(const Alkalmazas& alkalmazas) {
        alkalmazasok += alkalmazas.nev + ", ";
        return true;
    }
};

class BiztonsagosTelefon: public Telefon {
public:
    bool telepit(const Alkalmazas& alkalmazas) override {
        if (alkalmazas.biztonsagos) {
            Telefon::telepit(alkalmazas);
            return true;
        } else {
            return false;
        }
    }
};