#include <gtest/gtest.h>
#include <string>

using namespace std;

#define private public
#define class struct
#define main main_0
#include "../src/telepites.cpp"
#undef main
#undef class
#undef private

const Alkalmazas a1 = {"asd", false};
const Alkalmazas a2 = {"ajnv", true};
const Alkalmazas a3 = {"njbk", false};

TEST(Teszt, 01) { // NOLINT(cert-err58-cpp) suppress for initialization static field in generated class
    BiztonsagosTelefon bt;
    bool res = bt.telepit(a1);
    ASSERT_EQ(res, false);
    ASSERT_EQ(bt.alkalmazasok, "");

    res = bt.telepit(a2);
    ASSERT_EQ(res, true);
    ASSERT_EQ(bt.alkalmazasok, "ajnv, ");

    res = bt.telepit(a3);
    ASSERT_EQ(res, false);
    ASSERT_EQ(bt.alkalmazasok, "ajnv, ");
}