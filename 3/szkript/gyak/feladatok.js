// classok js ben, CservZ gyak

class Mappa {
    constructor(nev, meret=0) {
        this.nev = nev;
        this.meret = meret;
        this.tartalom = [];
    }

    get meret() {
        return this._meret;
    }

    set meret(ertek) {
        this._meret = (typeof ertek === "number" && ertek >= 0 ? ertek : 0);
    }

    allomanyFelvesz(nev, meret) {
        if (this.tartalom.includes(nev)) {
            console.log("Hiba! Mar letezo allomany!");
        } else {
            this.tartalom.push(nev);
            this.meret += meret;
        }
    }

    info() {
        return `${this.nev} mappa, ${this.meret} bajt, ${this.tartalom.length} allomany`;
    }
}

class MemesMappa extends Mappa {
    constructor(nev, meret=420) {
        super(nev, meret);
    }

    allomanyFelvesz(nev, meret) {
        if (nev.toLocaleString().endsWith(".txt") ||  nev.toLocaleString().endsWith(".png") || nev.toLocaleString().endsWith(".mp4")) {
            super.allomanyFelvesz(nev, meret);
        }
    }

    memeketTorol() {
        this.meret = 0;
        this.tartalom = [];
    }
}

const m1 = new Mappa("Letoltesek", 1024);
const m2 = new Mappa("Cuccok");

console.log(m2.meret);
m2.meret = 2048;

m2.allomanyFelvesz("macska.txt", 100);
m2.allomanyFelvesz("valami.pdf", 1000);
m2.allomanyFelvesz("macska.txt", 200);

console.log(m1.info());
console.log(m2.info());

const mm = new MemesMappa("Dank Memes");
mm.allomanyFelvesz("rick.roll.mp4", 10000);
mm.allomanyFelvesz("rick.roll.jpg", 10000);
mm.allomanyFelvesz(",macska.txt", 100);
mm.allomanyFelvesz("rick.roll.mp4", 10000);
mm.memeketTorol()

console.log(mm)
console.log(mm instanceof Mappa)