# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 5. feladat
also = int(input("Az intervallum also vegpontja: "))
felso = int(input("Az intervallum felso vegpontja:"))

eredmeny = 0

for i in range(also, felso + 1):
    if i % 2 == 0:
        eredmeny += i

print(f"A(z) [{also}; {felso}] intervallumba eso paros szamok osszege: {eredmeny}")
