# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 7. feladat
def prime(szam):
    prim = True

    for i in range(2, szam // 2 + 1):
        if szam % i == 0:
            prim = False
            break
    return prim


szam = int(input("Add meg a csoki gyartasi sorszamat: "))

if prime(szam):
    print("Gratulalok, nyertel!")
else:
    print("Sajnos nem nyert!")
