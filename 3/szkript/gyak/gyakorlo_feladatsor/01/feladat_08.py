# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 8. feladat
pontszam = int(input("A pontszamod: "))

if 0 <= pontszam <= 49:
    eredmeny = "Elégtelen (1)"
elif 50 <= pontszam <= 62:
    eredmeny = "Elégséges (2)"
elif 63 <= pontszam <= 75:
    eredmeny = "Közepes (3)"
elif 76 <= pontszam <= 88:
    eredmeny = "Jó (4)"
elif 89 <= pontszam <= 100:
    eredmeny = "Jeles (5)"
else:
    eredmeny = False

if eredmeny:
    print(f"Az erdemjegyed: {eredmeny}.")
else:
    print("Ervenytelen ertek!")
