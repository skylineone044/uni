# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 10. feladat
magassag = int(input("Add meg, hogy milyen magas legyen a piramis: "))
talpszelesseg = 1 + (magassag - 1) * 2

for i in range(1, magassag + 1):
    print(((talpszelesseg) // 2 - i + 1) * " " + i * "*", end="")
    if i > 1:
        print((i-1) * "*")
    else:
        print()
