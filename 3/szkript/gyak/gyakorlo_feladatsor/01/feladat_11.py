# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 11. feladat
a = float(input("Elso szam: "))
b = float(input("Masodik szam: "))
muvelet = input("Muvelet: ")

if muvelet in ("+", "-", "*", "/"):
    if muvelet == "/" and b == 0:
        print("Ejnye, nullaval nem osztunk!")
    else:
        # feltetelezem te arra szamitottal, hogy egy adag if-elif komboval lesz
        # megoldva, de eszemble jutott hogy van eval, es ez igy egy kicsit
        # fun-abb nak tunt                               :)
        print(f"Az eredmeny: {round(eval(f'{a} {muvelet} {b}'), 2)}")
else:
    print("Hibas muveleti jel!")
