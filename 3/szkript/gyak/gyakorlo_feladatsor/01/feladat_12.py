# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 12. feladat
jegyekSzama = int(input("Hallgato jegyeinek szama: "))
jegyek = []

print("A jegyek:")
for i in range(jegyekSzama):
    jegyek.append(int(input()))

atlag = sum(jegyek) / jegyekSzama
print(f"A jegyek atlaga: {atlag}")
if atlag >= 4.0:
    print("A hallgato osztondijra jogosult!")
else:
    print("A hallgato nem jogosult osztondijra!")
