# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 14. feladat
elso_ora = int(input("Elso idopont (ora): "))
elso_perc = int(input("Elso idopont (perc): "))
masodid_ora = int(input("Masodik idopont (ora): "))
masodik_perc = int(input("Masodik idopont (perc): "))

elso_osszperc = elso_ora * 60 + elso_perc
masodik_osszperc = masodid_ora * 60 + masodik_perc

diff_perc = abs(elso_osszperc - masodik_osszperc)

print(f"A ket idopont kozott {diff_perc // 60} ora es {diff_perc % 60} perc telt el.")
