# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 15. feladat
bulizok = int(input("A hazibuliban resztvevok szama: "))
rendorok = int(input("A rendorok szama: "))

elkaptak = 0

rendor_elkap = 1

for i in range(rendorok):
    elkaptak += rendor_elkap
    rendor_elkap += 1

if elkaptak == 0:
    print("Minden bulizo megmenekult!")
elif elkaptak >= bulizok:
    print("Ajaj, mindenkit elkaptak!")
else:
    print(f"{elkaptak} bulizot elkaptak, {bulizok - elkaptak} pedig elmenekult.")
