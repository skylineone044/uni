# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 16. feladat
from random import randint

MAX = 1000
MIN = 1

probak = 20
randomSzam = randint(MIN, MAX + 1)

print(
    f"Gondoltam egy szamra {MIN} és {MAX} kozott, talald ki, hogy melyikre! Eletek szama: {probak}"
)
while probak > 0:
    guess = int(input("Tipp: "))
    if guess == randomSzam:
        print(
            f"""--------------------------------
Gratulalok, nyertel!
A gondolt szam valoban {randomSzam} volt.
Megmaradt eletek: {probak}
 """
        )
        break

    if guess < randomSzam:
        print("Nagyobb")
    elif guess > randomSzam:
        print("Kisebb")

    probak -= 1

else:
    print(
        f"""--------------------------------
Sajnos nem nyertel!
A gondolt szam {randomSzam} volt."""
    )
