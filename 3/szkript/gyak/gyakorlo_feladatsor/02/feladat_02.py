# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 2. feladat


def fibo(n):
    N = 0
    N_1 = 1
    N_2 = 0

    # nagy szamok eseten ez gyorsabb, mert nem kerul tobbszor kiszamitasra n-1
    # es n-2 kulon agakon, hanem minden csak egyszer
    for i in range(n - 1):
        N = N_1 + N_2
        N_2 = N_1
        N_1 = N

    return N


if __name__ == "__main__":
    print(fibo(10))
    print(fibo(42))
