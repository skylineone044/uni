# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 3. feladat
def kuba(felhnev):
    if felhnev[-1] == ".":
        felhnev = felhnev[:-1]
    else:
        felhnev += "."
    return felhnev


if __name__ == "__main__":
    print(kuba("abc"))
    print(kuba("abc."))
