# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 4. feladat
def kirakhato(szo):
    used = []
    for letter in szo:
        if letter.lower() in used:
            return False
        else:
            used.append(letter.lower())
    return True


if __name__ == "__main__":
    print(kirakhato("bean"))
    print(kirakhato("BeaN"))
    print(kirakhato("beanbean"))
    print(kirakhato("BeaNbeAn"))
