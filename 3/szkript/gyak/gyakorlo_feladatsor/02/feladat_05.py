# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 5. feladat
def armstrong_szam(n):
    szam = 0
    szam_s = str(n)
    for digit in szam_s:
        szam += int(digit) ** len(szam_s)

    return szam == n


if __name__ == "__main__":
    print(armstrong_szam(153))
    print(armstrong_szam(1999))
    print(armstrong_szam(8208))
