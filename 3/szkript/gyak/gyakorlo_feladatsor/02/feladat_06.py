# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 6. feladat
def jelszo_erosseg(jelszo):
    erosseg = 1

    erosseg += 1 if len(jelszo) >= 5 else 0
    erosseg += 1 if len(jelszo) >= 8 else 0

    for letter in jelszo:
        if letter in ("_", "-", "."):
            erosseg += 2

    if "jelszo" in jelszo or "123" in jelszo or len(jelszo) == 0:
        erosseg = 0

    return erosseg


if __name__ == "__main__":
    print(jelszo_erosseg("hazi_macska_4_life"))     # a megoldasban nem tudom hogy jon ki a 10, en akarhogy szamoltam manualisan mindig 9 jott ki
    print(jelszo_erosseg("ez1feltorhetetlenjelszo"))
