# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 7. feladat
def maganhangzot_torol(szoveg):
    res = ""
    for letter in szoveg:
        if letter.lower() not in ("a", "e", "i", "o", "u"):
            res += letter

    return res


if __name__ == "__main__":
    print(
        maganhangzot_torol(
            "Iden Java szigeten voltunk nyaralni. Nem is tudtam, hogy elneveztek egy helyet egy programozasi nyelvrol."
        )
    )
