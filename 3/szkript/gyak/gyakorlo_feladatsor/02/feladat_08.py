# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 8. feladat
def kodol(msg, n, c):
    res = ""

    for letter in msg:
        res += letter
        res += n * c

    return res


if __name__ == "__main__":
    print(kodol("Pizza delben?", 3, "x"))
