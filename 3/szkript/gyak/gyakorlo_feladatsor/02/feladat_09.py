# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 9. feladat
def dekodol(coded_msg, n):
    res = ""

    for letter in coded_msg[:: n + 1]:
        res += letter

    return res


if __name__ == "__main__":
    print(dekodol("Pxxxixxxzxxxzxxxaxxx xxxdxxxexxxlxxxbxxxexxxnxxx?xxx", 3))
