# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 10. feladat
def elmozdulas(szoveg):
    jobb = 0
    fel = 0

    for letter in szoveg:
        if letter == "J":
            jobb += 1
        elif letter == "B":
            jobb -= 1
        elif letter == "F":
            fel += 1
        elif letter == "L":
            fel -= 1

    res = ""
    if jobb == 0 and fel == 0:
        return "Nem mentunk sehova"

    if jobb != 0:
        res += f"{abs(jobb)} lepes {'jobbra' if jobb > 0 else 'balra'}"

    if fel != 0:
        plus = f"{abs(fel)} lepes {'fel' if fel > 0 else 'le'}"
        if res == "":
            res += plus
        else:
            res += ", " + plus

    return res


if __name__ == "__main__":
    print(elmozdulas("JJFBFFFFFFBBBL"))
    print(elmozdulas("FBLLLJLLJ"))
    print(elmozdulas("FFF"))
    print(elmozdulas("FFLLBBJJ"))
