# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 11. feladat
def palindrom(szoveg):
    res = ""
    for letter in szoveg:
        if letter not in (" ", ".", "!", "?", ","):
            res += letter.lower()

    return res == res[::-1]


if __name__ == "__main__":
    print(palindrom("Indul a gorog aludni."))
    print(palindrom("kecske"))
