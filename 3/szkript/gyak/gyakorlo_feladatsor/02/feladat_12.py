# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 12. feladat
def palindromszam(szam):
    return str(szam) == str(szam)[::-1]


if __name__ == "__main__":
    print(palindromszam(123454321))
    print(palindromszam(2020))
