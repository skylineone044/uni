# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 13. feladat
def idegesseg_detektor(szoveg):
    if len(szoveg) == 0:
        return None

    counter = 0
    for letter in szoveg:
        if letter in "!ABCDEFGHIJKLMNOPQRSTUVWXYZ":
            counter += 1

    return counter / len(szoveg) > 0.5


if __name__ == "__main__":
    print(
        idegesseg_detektor(
            "Hello! 3 darab AUCHANOS ZSEMLET cserelnek SURGOSEN kedd esti PROG2 gyakorlatra."
        )
    )
    print(
        idegesseg_detektor(
            # kedves ferenc XD
            "KEDVES FERENC! Az EN VELEMENYEM pedig az, hogy a FELADAT KESZITOJE KIFOGYOTT az ERTELMES peldamondatokbol!!!!!!"
        )
    )
    print(idegesseg_detektor(""))
