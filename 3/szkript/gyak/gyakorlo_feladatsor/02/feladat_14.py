# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 14. feladat
def mocking_spongebob(szoveg):
    res = ""
    for i, letter in enumerate(szoveg):
        if i % 2 == 0:
            res += letter.lower()
        else:
            res += letter.upper()

    return res


if __name__ == "__main__":
    print(mocking_spongebob("A Szkriptnyelvek meg konnyu targynak szamit."))
