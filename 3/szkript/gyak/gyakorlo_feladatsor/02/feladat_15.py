# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 15. feladat
def tomorit(szoveg):
    szoveg += " "
    res = ""
    last = ""
    counter = 0

    # ez nem a leg elegansabb, de este van, es ez mukodik, ugyhogy jovanazugy'
    for i, letter in enumerate(szoveg):
        if szoveg[i - 1] == letter:
            counter += 1
        elif szoveg[i - 1] != letter and counter != 0:
            res = res[:-1] + f"{counter+1}{szoveg[i - 1]}"
            counter = 0
            res += szoveg[i]
        else:
            res += letter

    return res[:-1]


if __name__ == "__main__":
    print(tomorit("Hahooooo! Van itt valaki???"))
