# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 1. feladat
def konyveket_rendez(konyv_cimek):
    konyv_cimek.sort()
    return konyv_cimek[::-1]

if __name__ == "__main__":
    print(konyveket_rendez(['Vajak I', 'Allatfarm', 'Harry Potter es a bolcsek kove', 'A hobbit', 'Szamitogep Architekturak']))
