# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 2. feladat
def akcios_ar(eredeti_arak, learazas):
    return [int(ar*(1-(learazas*.01))) for ar in eredeti_arak]

if __name__ == "__main__":
    print(akcios_ar([5000, 12000, 10000, 29000, 47000], 30.0))
