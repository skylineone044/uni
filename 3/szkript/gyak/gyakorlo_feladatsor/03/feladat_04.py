# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 4. feladat
def leghosszabb_szo(szoveg):
    if len(szoveg) == 0:
        return "HIBA!"

    # leghosszabb = max(szoveg.split(), key=len) # python is magic, had no idea this is a built in thing

    # de gondolom ez lenne az elvart megoldas:
    leghosszabb = ""
    leghosszabb_hossz = 0
    for word in szoveg.split():
        if len(word) > len(leghosszabb):
            leghosszabb = word
            leghosszabb_hossz = len(leghosszabb)
    return leghosszabb


if __name__ == "__main__":
    print(leghosszabb_szo("Szia uram! Mondd mar meg, hogy hany ora van!"))
    print(leghosszabb_szo(""))
