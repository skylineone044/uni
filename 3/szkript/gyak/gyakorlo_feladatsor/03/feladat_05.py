# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 5. feladat
def gyenge_jelszavak(jelszavak):
    weaklings = []

    for jelszo in jelszavak:
        if len(jelszo) < 5 or "jelszo" in jelszo.lower() or "123" in jelszo:
            weaklings.append(jelszo)
        elif not any(char.isdigit() for char in jelszo):
            weaklings.append(jelszo)
    return weaklings


if __name__ == "__main__":
    print(
        gyenge_jelszavak(
            [
                "Root",
                "Kekw2000",
                "H0sszuJelszoG0esBrrr",
                "Admin123456",
                "sub2Pewdiepie",
                "asdqwe",
                "K1L0",
                "beanbeanbean",
            ]
        )
    )
