# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 6. feladat
def egyedi_szavak_szama(szoveg):
    szavak = set()
    for szo in szoveg.split():
        szo = szo.lower()
        if szo[-1] in ".,?!":
            szo = szo[:-1]
        szavak.add(szo)
    return len(szavak)


if __name__ == "__main__":
    print(egyedi_szavak_szama("A macska, vagy mas neven a hazi macska kisebb termetu husevo emlos, amely a macskafelek csaladjaba tartozik."))
