# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 7. feladat
def felvaltva(szoveg):
    if len(szoveg.split()) < 2:
        return "HIBA!"

    words = []
    for i, word in enumerate(szoveg.split()):
        words.append(word.upper() if i % 2 == 0 else word.lower())
    return " ".join(words)


if __name__ == "__main__":
    print(
        felvaltva(
            "Tajekoztatjuk utasainkat, hogy a Szeged felol erkezo szemelyvonat varhatoan otven percet kesik."
        )
    )
    print(felvaltva("sajt"))
