# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 8. feladat
def sorozat(szamok):
    szamtani = False
    mertani = False

    if len(szamok) < 3:
        return "HIBA!"
    # szamtani
    kulombsegek = set()
    for i in range(len(szamok) - 1):
        kulombsegek.add(szamok[i] - szamok[i + 1])

    if len(kulombsegek) == 1:
        szamtani = True

    hanyadosok = set()
    for i in range(len(szamok) - 1):
        hanyadosok.add(szamok[i] / szamok[i + 1])

    if len(hanyadosok) == 1:
        mertani = True

    if szamtani and mertani:
        return "A sorozat szamtani es mertani sorozat is egyben."
    elif szamtani and not mertani:
        return "A sorozat szamtani sorozat."
    elif not szamtani and mertani:
        return "A sorozat mertani sorozat."
    elif not szamtani and not mertani:
        return "A sorozat se nem szamtani, se nem mertani sorozat."


if __name__ == "__main__":
    print(sorozat([10, 20]))
    print(sorozat([2, 3, 5, 7, 11, 13]))
    print(sorozat([42, 42, 42]))
    print(sorozat([9, 5, 1, -3, -7]))
    print(sorozat([2, 4, 8, 16, 32, 64, 128, 256, 512, 1024]))
