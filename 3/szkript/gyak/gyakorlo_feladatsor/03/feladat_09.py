# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 9. feladat
def kasszahoz_rendel(szoveg):
    parosak = []
    paratlanok = []

    for szam in szoveg.split(";"):
        szam = int(szam)
        if szam % 2 == 0:
            parosak.append(szam)
        else:
            paratlanok.append(szam)
    parosak.sort()
    paratlanok.sort()
    return [parosak, paratlanok]


if __name__ == "__main__":
    print(kasszahoz_rendel("42;38;45;40;41;39;44;43;46"))
    print(kasszahoz_rendel("12;16;14"))
