# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 10. feladat
def matrix_osszead(A, B):
    try:
        for i, sor in enumerate(A):
            for j, item in enumerate(sor):
                A[i][j] += B[i][j]
        return A
    except:
        return []


if __name__ == "__main__":
    print(matrix_osszead([[7, 1, 3], [4, 0, 2]], [[2, 5, 5], [8, 6, 0]]))
    print(matrix_osszead([[5, 1], [3, 2], [8, 6]], [[4, 8], [2, 7]]))
