# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 11. feladat
def szoveget_elemez(szoveg):
    res = {"betu": 0, "szamjegy": 0, "egyeb": 0}
    for betu in szoveg:
        if betu.upper() in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
            res["betu"] += 1
        elif betu in "0123456789":
            res["szamjegy"] += 1
        else:
            res["egyeb"] += 1
    return res


if __name__ == "__main__":
    print(szoveget_elemez("Python4Life!!!"))
    print(szoveget_elemez("12345"))
