# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 12. feladat
def kurzuskodot_csoportosit(kodok):
    if kodok == "":
        return {}

    res = {"infos": [], "matekos": [], "szabval": []}
    for kod in kodok.split(";"):
        if kod[0] == "I":
            res["infos"].append(kod)
        if kod[0] == "M":
            res["matekos"].append(kod)
        if kod[0] == "X":
            res["szabval"].append(kod)
    return res


if __name__ == "__main__":
    print(kurzuskodot_csoportosit("IB370G;MBNXK114E;MBNXK114G;XA0021-GTK-MM1;IB370E"))
    print(kurzuskodot_csoportosit("ITN714G;IB402g;XN0011-01317;IB202g"))
    print(kurzuskodot_csoportosit(""))
