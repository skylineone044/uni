# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 13. feladat
def statisztika(fileok):
    res = {}
    for file in fileok:
        ext = file.split(".")[-1].lower()
        res[ext] = res[ext] + 1 if res.get(ext) is not None else 1
    return res


if __name__ == "__main__":
    print(
        statisztika(
            [
                "feladat.py",
                "Bolygo.java",
                "HELLOFRIENDS.MP4",
                "TEST.PY",
                "biro.gib.maxpont.py",
                "russian-driving-fails.mp4",
            ]
        )
    )
