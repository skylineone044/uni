# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 14. feladat
def vegeredmeny(eredmenyek):
    res = {}

    for eredmeny in eredmenyek:
        for k, v in eredmeny.items():
            res[k] = res[k] + v if res.get(k) is not None else v
    return res


if __name__ == "__main__":
    print(
        vegeredmeny(
            [
                {"shronk": 400, "Tamas": 200, "adam": 800, "Wolf": 500, "Karoly": 70},
                {"Tamas": 0, "Wolf": 0, "Karoly": 200, "shronk": 0, "adam": 100},
                {"Wolf": 600, "adam": 400, "Karoly": 500, "shronk": 200, "Tamas": 300},
                {"Tamas": 500, "Wolf": 100, "Karoly": 0, "shronk": 600, "adam": 200},
                {"adam": 100, "Wolf": 500, "shronk": 0, "Tamas": 300, "Karoly": 100},
            ]
        )
    )
