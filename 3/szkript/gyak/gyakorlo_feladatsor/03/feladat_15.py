# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 15. feladat
def szoveges_vegeredmeny(eredmenyek):
    darabok = []
    for nev, pontok in eredmenyek.items():
        darabok.append(f"{nev}: {pontok} pont")

    return ", ".join(darabok)


if __name__ == "__main__":
    print(
        szoveges_vegeredmeny(
            {"shronk": 1200, "Tamas": 1300, "adam": 1600, "Wolf": 1700, "Karoly": 870}
        )
    )
