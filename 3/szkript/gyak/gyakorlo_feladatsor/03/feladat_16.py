# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

# 16. feladat
def gyoztes(eredmeny):
    maxnev = ""
    maxpont = -1

    for nev, pont in eredmeny.items():
        if pont > maxpont:
            maxpont = pont
            maxnev = nev

    return maxnev

if __name__ == "__main__":
    print(gyoztes({'shronk': 1200, 'Tamas': 1300, 'adam': 1600, 'Wolf': 1700, 'Karoly': 870}))
