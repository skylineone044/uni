# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794

#         _
#        \`*-.
#         )  _`-.
#        .  : `. .
#        : _   '  \
#        ; *` _.   `*-._
#        `-.-'          `-.
#          ;       `       `.
#          :.       .        \
#          . \  .   :   .-'   .
#          '  `+.;  ;  '      :
#          :  '  |    ;       ;-.
#          ; '   : :`-:     _.`* ;
# [kat] .*' /  .*' ; .*`- +'  `*'
#       `*-*   `*-*  `*-*'

from typing import Type


class BorospinceException(Exception):
    def __init__(self, hibauzenet):
        super().__init__(hibauzenet)


class Bor:
    def __init__(self, _fajta, _evjarat, _alkoholtartalom=12.5):
        self._fajta = _fajta
        self._evjarat = _evjarat
        self.alkoholtartalom = _alkoholtartalom

    @property
    def fajta(self):
        return self._fajta

    @fajta.setter
    def fajta(self, ujertek):
        self._fajta = ujertek

    @property
    def evjarat(self):
        return self._evjarat

    @evjarat.setter
    def evjarat(self, ujertek):
        self._evjarat = ujertek

    @property
    def alkoholtartalom(self):
        return self._alkoholtartalom

    @alkoholtartalom.setter
    def alkoholtartalom(self, ujertek):
        if (
            isinstance(ujertek, int) or isinstance(ujertek, float)
        ) and 0 <= ujertek <= 100:
            # self._alkoholtartalom = min(max(ujertek, 0), 100)
            self._alkoholtartalom = ujertek
        else:
            raise BorospinceException("Nem megfelelo alkoholtartalom!")

    def __str__(self):
        return f"{self.fajta} (evjarat: {self.evjarat}), melynek alkoholtartalma: {self.alkoholtartalom}%"

    def __eq__(self, other):
        if isinstance(other, Bor):
            return (
                self.fajta.lower() == other.fajta.lower()
                and self.evjarat == other.evjarat
                and self.alkoholtartalom == other.alkoholtartalom
            )
        else:
            return False


class Szekreny:
    def __init__(self):
        self.borok = []

    def get_bor(self, index):
        if 0 <= index < len(self.borok):
            return self.borok[index]
        else:
            raise BorospinceException("Nem letezo index!")

    def __iadd__(self, bor):
        if isinstance(bor, Bor):
            self.borok.append(bor)
            return self
        else:
            raise TypeError("Nem bor!")

    def __add__(self, other):
        if isinstance(other, Szekreny):
            for bor in other.borok:
                self += bor
            return self
        else:
            raise TypeError("Nem szekreny!")

    def atlag_alkoholtartalom(self):
        if len(self.borok) > 0:
            ossz = 0
            for bor in self.borok:
                ossz += bor.alkoholtartalom
            return ossz / len(self.borok)
        else:
            raise BorospinceException("Ures a szekreny!")

    def statisztika(self):
        res = {}
        if len(self.borok) > 0:
            for bor in self.borok:
                res[bor.fajta.lower()] = (
                    1
                    if res.get(bor.fajta.lower()) is None
                    else res[bor.fajta.lower()] + 1
                )
            return res
        else:
            return {}

    def megisszak(self, ezt_megisszak):
        if isinstance(ezt_megisszak, Bor):
            torolve = False
            for i, bor in enumerate(self.borok):
                if bor == ezt_megisszak:
                    del self.borok[i]
                    torolve = True
            if not torolve:
                raise BorospinceException("Bor nem talalhato!")
        else:
            raise TypeError("Nem bor!")

    def __str__(self):
        if len(self.borok) == 0:
            return "Ez egy ures szekreny."
        else:
            res = []
            for borfajta, darab in self.statisztika().items():
                res.append(f"{darab} {borfajta}")
            return ", ".join(res)

if __name__ == "__main__":
    bor1 = Bor('Tokaji aszu', 2017, 13.5)
    bor2 = Bor('Gyanus kinezetu kannasbor', 2010)
    bor3 = Bor('ToKaJi AsZu', 2015, 13.8)
    bor4 = Bor('Chardonnay', 2019, 13.0)

    bor2.fajta = 'Egri bikaver'
    bor2.evjarat = 2013
    bor2.alkoholtartalom = 12.0
    print(f'{bor2.fajta}, {bor2.evjarat}, {bor2.alkoholtartalom}')  # 'Egri bikaver, 2013, 12.0'

    print(bor1)                  # 'Tokaji aszu (evjarat: 2017), melynek alkoholtartalma: 13.5%'
    print(bor1 == Bor('TOKAJI ASZU', 2017, 13.5))   # True
    print(bor1 == bor2)                             # False
    print(bor1 == 'Hibas tipusu parameter!')        # False

    szekreny1 = Szekreny()
    szekreny2 = Szekreny()

    szekreny1 += bor1
    szekreny1 += bor2
    szekreny1 += bor3
    szekreny2 += bor4

    szekreny3 = szekreny1 + szekreny2

    print(szekreny3.get_bor(0))                   # 'Tokaji aszu (evjarat: 2017), melynek alkoholtartalma: 13.5%'
    print(szekreny3.get_bor(3))                   # 'Chardonnay (evjarat: 2019), melynek alkoholtartalma: 13.0%'
    print(szekreny3.atlag_alkoholtartalom())      # 13.075

    szekreny2.megisszak(bor4)
    print(szekreny2.statisztika())                # {}
    print(szekreny3.statisztika())                # {'tokaji aszu': 2, 'egri bikaver': 1, 'chardonnay': 1}

    print(szekreny2)                              # 'Ez egy ures szekreny.'
    print(szekreny3)                              # '2 tokaji aszu, 1 egri bikaver, 1 chardonnay'
