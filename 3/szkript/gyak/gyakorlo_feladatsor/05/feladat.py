# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794


def beolvas():
    with open("playlist.csv", "r") as playlist_csv:
        return [
            {
                "eloado": items[0],
                "cim": items[1],
                "mufaj": items[2],
                "hossz": int(items[3]),
            }
            for items in [line.split(";") for line in playlist_csv.readlines()]
        ]


def teljes_hossz(playlist):
    sum_len = 0
    for song in playlist:
        sum_len += song["hossz"]

    with open("02_hossz.txt", "w") as hossz_txt:
        hossz_txt.write(
            f"A lejatszasi lista hossza: {sum_len // 60} perc, {sum_len % 60} masodperc"
        )


def leghosszabb_rockzene(playlist):
    leghosszabb = 0
    leghosszabb_cim = ""
    for song in playlist:
        if song["hossz"] > leghosszabb and song["mufaj"] == "rock":
            leghosszabb = song["hossz"]
            leghosszabb_cim = song["cim"]

    with open("03_leghosszabb_rock.txt", "w") as rock_txt:
        rock_txt.write(leghosszabb_cim)


def leggyakoribb_mufaj(playlist):
    mufajcount = {}

    for song in playlist:
        mufajcount[song["mufaj"]] = (
            mufajcount[song["mufaj"]] + 1 if song["mufaj"] in mufajcount.keys() else 1
        )

    leggyakoribb = ""
    leggyakoribb_gy = 0

    for mufaj, count in mufajcount.items():
        if count > leggyakoribb_gy:
            leggyakoribb = mufaj
            leggyakoribb_gy = count

    with open("04_kedvenc_mufaj.txt", "w") as kedvenc_txt:
        kedvenc_txt.write(leggyakoribb.upper())


def zeneket_csoportosit(playlist):
    eloadok = {}

    for song in playlist:
        eloadok[song["eloado"]] = (
            eloadok[song["eloado"]] + song["hossz"]
            if song["eloado"] in eloadok.keys()
            else song["hossz"]
        )

    with open("05_osszegzes.txt", "w") as osszegzes_txt:
        for eloado, ossz_hossz in sorted(eloadok.items()):
            osszegzes_txt.write(f"{eloado} - osszesen {ossz_hossz} masodpercnyi zene\n")


def zeneket_listaz(playlist, eloado):
    with open(f"06_{eloado.lower().replace(' ', '_')}_dalok.txt", "w") as out_file:
        out_file.write(
            "".join(
                [
                    f"{song['cim']};{song['mufaj']};{song['hossz']}\n"
                    for song in playlist
                    if song["eloado"].lower() == eloado.lower()
                ]
            )
            or "Nincs ilyen eloado a lejatszasi listaban!"
        )


def zeneket_torol(playlist, eloadok):
    with open("07_torolt.txt", "w") as torolt_txt:
        torolt_txt.write(
            "".join(
                [
                    f"{song['eloado']};{song['cim']};{song['mufaj']};{song['hossz']}\n"
                    for song in playlist
                    if song["eloado"] not in eloadok
                    # btw did you know I love python's ternary operator and list comprehensions?
                ]
            )
        )


if __name__ == "__main__":
    playlist = beolvas()
    # print("\n".join([str(list) for list in playlist]))
    teljes_hossz(playlist)
    leghosszabb_rockzene(playlist)
    leggyakoribb_mufaj(playlist)
    zeneket_csoportosit(playlist)
    zeneket_listaz(playlist, "POWERWOLF")
    zeneket_listaz(playlist, "Imagine Dragons")
    zeneket_listaz(playlist, "Taylor Swift")
    zeneket_torol(playlist, ["Imagine Dragons", "Rick Astley", "Powerwolf"])
