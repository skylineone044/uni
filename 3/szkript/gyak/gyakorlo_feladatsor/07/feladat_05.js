// Nev: Tokodi Mate
// Neptun: BCSJ6F
// h: h052794

function kobosszeg(also, felso) {
    let sum = 0;
    for (let i = also; i <= felso; i++) {
        sum += i**3;
    }
    return sum;
}

console.log(kobosszeg(1, 3));
console.log(kobosszeg(10, 100));
