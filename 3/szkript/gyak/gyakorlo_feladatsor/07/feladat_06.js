// Nev: Tokodi Mate
// Neptun: BCSJ6F
// h: h052794


function kobosszeg(also, felso) {
    let sum = 0;
    for (let i = also; i <= felso; i++) {
        sum += i**3;
    }
    return sum;
}

function kobosszegetMeghiv(also, felso, fgv) {
    if (Number.isInteger(also) && Number.isInteger(felso) && typeof fgv == "function") {
        return fgv(Math.min(also, felso), Math.max(also, felso));
    }
}

console.log(kobosszegetMeghiv(100, 10, kobosszeg))
