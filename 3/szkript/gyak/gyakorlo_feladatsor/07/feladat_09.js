// Nev: Tokodi Mate
// Neptun: BCSJ6F
// h: h052794

function cenzura(uzenet) {
    return uzenet.replaceAll(/[aeiouAEIOU]/gi, "*");
}

console.log(cenzura('He, miert igy jelennek meg az uzeneteim?'));
console.log(cenzura('He, miert igy jelennek meg az uzeneteim?') === 'H*, m**rt *gy j*l*nn*k m*g *z *z*n*t**m?');

console.log(cenzura('Elhagyom ezt a csoportot, mert serto az elmult tobb mint 10 eves Discordos tapasztalatom es multammal szemben.'));
console.log(cenzura('Elhagyom ezt a csoportot, mert serto az elmult tobb mint 10 eves Discordos tapasztalatom es multammal szemben.') === '*lh*gy*m *zt * cs*p*rt*t, m*rt s*rt* *z *lm*lt t*bb m*nt 10 *v*s D*sc*rd*s t*p*szt*l*t*m *s m*lt*mm*l sz*mb*n.');
