// Nev: Tokodi Mate
// Neptun: BCSJ6F
// h: h052794

function konvertal(hex_kod) {
    return `rgb(${parseInt(Number("0x" + hex_kod.slice(1, 3)))}, ${parseInt(Number("0x" + hex_kod.slice(3, 5)))}, ${parseInt(Number("0x" + hex_kod.slice(5, 7)))})`;
}

console.log(konvertal('#FF0077'))
console.log(konvertal('#06C2E9'))
