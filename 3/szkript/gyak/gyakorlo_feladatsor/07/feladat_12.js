// Nev: Tokodi Mate
// Neptun: BCSJ6F
// h: h052794

function python(szoveg, index) {
    if (index >= 0) {
        return szoveg[index];
    } else {
        return szoveg[szoveg.length + index];
    }
}

console.log(python('Lehet-e a Pythonos indexelest JavaScriptben szimulalni?', 0))
console.log(python('Lehet-e a Pythonos indexelest JavaScriptben szimulalni?', -1))
console.log(python('Lehet-e a Pythonos indexelest JavaScriptben szimulalni?', -45))
console.log(python('Lehet-e a Pythonos indexelest JavaScriptben szimulalni?', -2000))
