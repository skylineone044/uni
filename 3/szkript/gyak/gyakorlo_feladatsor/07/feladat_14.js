// Nev: Tokodi Mate
// Neptun: BCSJ6F
// h: h052794

function haromszog(a, b, c) {
    if (a < 0 || b < 0 || c < 0 || !(a + b >= c && a + c >= b && b + c >= a)) {
        return "HIBA! Nem letezo haromszog!";
    } else {
        k = a + b + c;
        s = k / 2;
        return `Kerulet: ${Math.round(k * 100) / 100} cm, terulet: ${ Math.round(Math.sqrt( s * (s - a) * (s - b) * (s - c)) * 100) / 100 } cm2`;
    }
}

console.log(haromszog(3, -1, 3))
console.log(haromszog(7, 4, 2))
console.log(haromszog(5, 4, 7))
