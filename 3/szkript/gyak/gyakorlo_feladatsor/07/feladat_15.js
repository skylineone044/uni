// Nev: Tokodi Mate
// Neptun: BCSJ6F
// h: h052794

function leghosszabbNyeroszeria(eredmenyStr) {
    let leghosszabb = 0;
    let counter = 0;

    for (let i = 0; i < eredmenyStr.length; i++) {
        if (eredmenyStr[i] === "1") {
            counter++;
        } else {
            if (counter > leghosszabb) {
                leghosszabb = counter;
            }
            counter = 0;
        }
    }
    return leghosszabb;
}

console.log(leghosszabbNyeroszeria('0000100111000000110000101'))
console.log(leghosszabbNyeroszeria('1101001111010000011110011'))
console.log(leghosszabbNyeroszeria('0000000000000000000000000'))
