// Nev: Tokodi Mate
// Neptun: BCSJ6F
// h: h052794

// 2. feladat

function stringeketOsszefuz(tomb) {
    return tomb.filter(elem => typeof elem === "string").join("");
}

console.log(stringeketOsszefuz(['Java', null, [], 'Script', 123, '4', 'life', undefined]))
