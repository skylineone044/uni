// Nev: Tokodi Mate
// Neptun: BCSJ6F
// h: h052794

// 3. feladat
function szepTomb(tomb) {
    if (!(tomb instanceof Array)) {
        return false;
    }

    return tomb.filter(elem => typeof elem === typeof tomb[0]).length === tomb.length;
}

console.log(szepTomb(['alma', 'korte', 'szilva', 'barack', 'palinka']))
console.log(szepTomb([10, 20, 30, 40, '50', 60, 70]))
console.log(szepTomb([]))
console.log(szepTomb('YEE HAW!'))
