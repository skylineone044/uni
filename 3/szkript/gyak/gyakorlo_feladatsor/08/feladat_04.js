// Nev: Tokodi Mate
// Neptun: BCSJ6F
// h: h052794

// 4. feladat
function hatvanyoz(tomb, kitevo) {
    if (tomb instanceof Array && typeof kitevo === "number") {
        return tomb.map(elem => elem**kitevo);
    } else {
        return [];
    }
}

console.log(hatvanyoz([1, 4, 7, 2, 5], 3))
console.log(hatvanyoz('buvarszivattyu', 5))
