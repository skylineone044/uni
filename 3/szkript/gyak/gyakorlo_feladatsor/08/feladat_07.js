// Nev: Tokodi Mate
// Neptun: BCSJ6F
// h: h052794

// 7. feladat
function gepeles(gepelnek) {
    if (!(typeof gepelnek === "string") || gepelnek.length === 0) {
        return "HIBA!";
    }

    gepelnek_array = gepelnek.split(";").map(gepelo => gepelo.trim())

    if (gepelnek_array.length === 1) {
        return `${gepelnek_array[0]} eppen gepel...`;
    } else if (gepelnek_array.length === 2) {
        return `${gepelnek_array[0]} es ${gepelnek_array[1]} eppen gepel...`;
    } else {
        return `${gepelnek_array[0]}, ${gepelnek_array[1]} es ${gepelnek_array.length - 2} tovabbi felhasznalo eppen gepel...`;
    }
}

console.log(gepeles('SuTi'))
console.log(gepeles('szte4k;catman6     ;    Sziklas  ;          Aerosol;cservZenberg'))
console.log(gepeles(""))
