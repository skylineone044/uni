// Nev: Tokodi Mate
// Neptun: BCSJ6F
// h: h052794

// 9. feladat
function emailCimetGeneral(teljes_nev, szul_ev=1970) {
    return teljes_nev.split(" ").map(nev_resz => nev_resz.slice(0, 3).toLowerCase()).join("") + szul_ev.toString()[szul_ev.toString().length-1] + "@duck.com";
}

console.log(emailCimetGeneral('Richard Paul Astley', 1966))
console.log(emailCimetGeneral('Richard Paul Astley', 1966) === 'ricpauast6@duck.com')
console.log(emailCimetGeneral('Citad Ella'))
console.log(emailCimetGeneral('Citad Ella') === 'citell0@duck.com')
console.log(emailCimetGeneral('Pablo Diego Jose Francisco de Paula Juan Nepomuceno Maria de los Remedios Cipriano de la Santisima Trinidad Ruiz y Picasso', 1881))
console.log(emailCimetGeneral('Pablo Diego Jose Francisco de Paula Juan Nepomuceno Maria de los Remedios Cipriano de la Santisima Trinidad Ruiz y Picasso', 1881) === 'pabdiejosfradepaujuanepmardelosremcipdelasantriruiypic1@duck.com')
