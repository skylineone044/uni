// Nev: Tokodi Mate
// Neptun: BCSJ6F
// h: h052794

// 10. feladat
function egyszamjatek(szamok) {
    let voltmar = {};
    for (let i = 0; i < szamok.length; i++) {
        voltmar[szamok[i]] = (Object.keys(voltmar).includes(szamok[i].toString()) ? voltmar[szamok[i]] + 1 : 1);
    }
    egyszeresek = Object.keys(voltmar).map(szam => [szam, voltmar[szam]]).filter(elem => elem[1] === 1).map(elem => elem[0]).sort((a, b) => b - a); // jajj de szép lett
    return (egyszeresek[0] === undefined ? -1 : egyszeresek[0]);
}

console.log(egyszamjatek([100, 42, 92, 100, 81, 99, 100, 1, 99, 100]))
console.log(egyszamjatek([70, 90, 90, 80, 70, 80]))
