// Nev: Tokodi Mate
// Neptun: BCSJ6F
// h: h052794

// 11. feladat
function fajlokatRendszerez(fileok) {
    return [
        fileok.filter(filenev => filenev.toLowerCase().endsWith(".py")),
        fileok.filter(filenev => filenev.toLowerCase().endsWith(".js")),
        fileok.filter(filenev => !filenev.toLowerCase().endsWith(".js") && !filenev.toLowerCase().endsWith(".py"))
    ];
}

console.log(fajlokatRendszerez(['gyak1.py', 'GYAKORLO.JS', 'feladat.pdf', 'elso.zh.py', 'riport.txt']))
console.log(fajlokatRendszerez(['index.js', 'functions.strings.js', 'INDEX.HTML', 'test01.js']))
