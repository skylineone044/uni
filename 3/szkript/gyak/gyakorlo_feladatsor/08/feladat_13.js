// Nev: Tokodi Mate
// Neptun: BCSJ6F
// h: h052794

// 13. feladat
function szamokatCsoportosit(szamok) {
    return {
        pozitiv: [szamok.split(";").filter(szam => szam > 0)],
        nulla: [szamok.split(";").filter(szam => szam === 0)],
        negativ: [szamok.split(";").filter(szam => szam < 0)]
    };
}

console.log(szamokatCsoportosit('7;-4;-1;0;2;5;-8'))
console.log(szamokatCsoportosit('-1;-2;-3;-4;-5'))
