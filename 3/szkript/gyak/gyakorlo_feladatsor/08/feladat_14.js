// Nev: Tokodi Mate
// Neptun: BCSJ6F
// h: h052794

// 14. feladat
function statisztika(pontszamok) {
    if (!(pontszamok instanceof Array) || pontszamok.length < 3) {
        return {};
    }
    sorted = pontszamok.sort((a, b) => a - b);
    return {
        atlag: pontszamok.reduce((pont, ossz) => pont + ossz) / pontszamok.length,
        median: (sorted.length % 2 === 0 ? (sorted[Math.floor(sorted.length / 2)-1] + sorted[Math.floor(sorted.length / 2)]) / 2 : sorted[Math.floor(sorted.length / 2)]),
        terjedelem: sorted[sorted.length - 1] - sorted[0]
    };
}

console.log(statisztika([29, 48, 0, 36, 20]))
console.log(statisztika([42, 35, 23, 50, 46, 25, 50, 47, 18, 38]))
console.log(statisztika([38, 45]))
