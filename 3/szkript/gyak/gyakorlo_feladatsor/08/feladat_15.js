// Nev: Tokodi Mate
// Neptun: BCSJ6F
// h: h052794

// 15. feladat
function valaszokatOsszesit(tantargyak) {
    let sum = {};

    for (let tantargy of tantargyak) {
        sum[tantargy.toLowerCase()] = (Object.keys(sum).includes(tantargy.toLowerCase()) ? sum[tantargy.toLowerCase()] + 1 : 1);
    }
    return sum;
}

console.log(valaszokatOsszesit([
    'Indiszkret Matematika', 'Kalkulus III', 'Tavolito es valosagos szamitasok',
    'kalkulus iii', 'tavolito es valosagos szamitasok',                                           // Ezek a targyak XD
    'TAVOLITO ES VALOSAGOS SZAMITASOK', 'kalkulus iii', 'Kalkulus III',                           //    10/10 would laugh again
    'Indiszkret Matematika', 'tAvOlItO Es vAlOsAgOs sZaMiTaSoK',
    'Kaveautomatak es informalis nyelvek', 'tavolito es valosagos szamitasok'
]))
