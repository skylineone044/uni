// Nev: Tokodi Mate
// Neptun: BCSJ6F
// h: h052794

// 16. feladat
function legnehezebbTargy(targyak) {
    return Object.keys(targyak).map(targy => [targy, targyak[targy]]).sort((a, b) => b[1] - a[1])[0][0];
}

console.log(legnehezebbTargy({
    'indiszkret matematika': 2,
    'kalkulus iii': 4,
    'tavolito es valosagos szamitasok': 5,
    'kaveautomatak es informalis nyelvek': 1
}))
