// Nev: Tokodi Mate
// Neptun: BCSJ6F
// h: h052794

// 17. feladat
function szavazatKulonbseg(targyak) {
    let szavazat_szamok = Object.values(targyak).sort((a, b) => a - b);
    return szavazat_szamok[szavazat_szamok.length - 1] - szavazat_szamok[0];
}

console.log(szavazatKulonbseg({
    'indiszkret matematika': 2,
    'kalkulus iii': 4,
    'tavolito es valosagos szamitasok': 5,
    'kaveautomatak es informalis nyelvek': 1
}))
