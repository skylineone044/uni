# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794


def szekem(id):
    print(f"id: {id}")
    sor = "S"
    oldal = "O"
    szam = "SZ"

    # a szek sora
    sor = (id // 14) + (0 if id % 14 == 0 else 1)

    # melyik oldaon van a szek (bal / jobb)
    oldalszzam = (id % 14)
    # print(oldalszzam)
    oldal = "bal" if oldalszzam > 7 or oldalszzam == 0 else "jobb"

    # az adott oldalon bentrol kifele hanyadik szek
    hetbol_maradek = (id % 7)
    if oldal == "jobb":
        if hetbol_maradek == 0:
            szam = 1
        else:
            szam = (7-hetbol_maradek) + 1
    else:
        if hetbol_maradek == 0:
            szam = 7
        else:
            szam = hetbol_maradek

    return f"{sor}. sor, {oldal} {szam}. szek"


if __name__ == "__main__":
    print(szekem(1))
    print(szekem(7))
    print(szekem(8))
    print(szekem(14))
    print(szekem(15))
    print(szekem(21))
    print(szekem(28))
    print(szekem(50))
    print(szekem(71))
    print(szekem(77))
    print(szekem(92))
    print(szekem(98))
    print(szekem(624))
    print(szekem(630))
