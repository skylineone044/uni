# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794


def nyertes_korok(adam_csapata, ellenfel_csappata):
    if (
        len(adam_csapata) != len(ellenfel_csappata)
        or len(adam_csapata) == 0
        or len(ellenfel_csappata) == 0
    ):
        return -1
    couner = 0

    for i in range(len(adam_csapata)):
        if adam_csapata[i] > ellenfel_csappata[i]:
            couner += 1

    return couner


# if __name__ == "__main__":
#     print(nyertes_korok([30, 50, 10, 80, 100, 40], [60, 20, 10, 20, 30, 20]))
#     print(nyertes_korok([70, 40, 50, 80, 0], [10, 90, 100, 20]))
