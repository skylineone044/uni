# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794


def hogolyo_csata(lista):
    sum = {}
    for csata in lista:
        for nev, adat in csata.items():
            if nev not in sum.keys():
                sum[nev] = {}
                sum[nev]["eldobott_hogolyok"] = (
                    adat["eldobott_hogolyok"]
                    if adat.get("eldobott_hogolyok") is not None
                    else 0
                )
                sum[nev]["talalt"] = (
                    adat["talalt"] if adat.get("talalt") is not None else 0
                )
                sum[nev]["fejtalalat"] = (
                    adat["fejtalalat"] if adat.get("fejtalalat") is not None else 0
                )
            else:
                sum[nev]["eldobott_hogolyok"] += (
                    adat["eldobott_hogolyok"]
                    if adat.get("eldobott_hogolyok") is not None
                    else 0
                )
                sum[nev]["talalt"] += (
                    adat["talalt"] if adat.get("talalt") is not None else 0
                )
                sum[nev]["fejtalalat"] += (
                    adat["fejtalalat"] if adat.get("fejtalalat") is not None else 0
                )

    return sum


def hogolyo_pontossag(csata_sum_res):
    res = {}
    for nev, adat in csata_sum_res.items():
        res[nev] = csata_sum_res[nev]
        res[nev]["pontossag"] = (
            adat["talalt"] / adat["eldobott_hogolyok"]
            if adat["eldobott_hogolyok"] != 0
            else 0
        )

    return res


def hogolyo_piros_lap(csata_sum_pontossagokall):
    piros_lapot_kapnak = []
    for nev, adat in csata_sum_pontossagokall.items():
        if adat["pontossag"] >= 0.7:
            if adat["fejtalalat"] / adat["talalt"] >= 0.5:
                piros_lapot_kapnak.append(nev)

    return piros_lapot_kapnak


# if __name__ == "__main__":
#     minimal_input = [
#         {
#             "Tamas": {"eldobott_hogolyok": 4, "talalt": 1},
#             "Ferenc": {"eldobott_hogolyok": 16, "talalt": 6, "fejtalalat": 1},
#             "Csaba": {
#                 "eldobott_hogolyok": 28,
#             },
#         },
#         {
#             "Tamas": {"eldobott_hogolyok": 2, "talalt": 2},
#             "Ferenc": {"eldobott_hogolyok": 3, "talalt": 2, "fejtalalat": 1},
#             "Csaba": {"eldobott_hogolyok": 4, "talalt": 2, "fejtalalat": 1},
#         },
#     ]
#     minimal_input_2 = {
#         "Geza": {
#             "eldobott_hogolyok": 14,
#             "talalt": 4,
#             "fejtalalat": 0,
#             "pontossag": 0.2857142857142857,
#         },
#         "Lajos": {
#             "eldobott_hogolyok": 45,
#             "talalt": 36,
#             "fejtalalat": 22,
#             "pontossag": 0.8,
#         },
#         "Jozsef": {
#             "eldobott_hogolyok": 37,
#             "talalt": 29,
#             "fejtalalat": 15,
#             "pontossag": 0.7837837837837838,
#         },
#     }
#     print(hogolyo_csata(minimal_input))
#     print(hogolyo_pontossag(hogolyo_csata(minimal_input)))
#     print(hogolyo_piros_lap(minimal_input_2))
