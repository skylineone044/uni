# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794


class Palack:
    def __init__(self, ital, max_urtartalom, _jelenlegi_urtartalom=1):
        self.ital = ital
        self.max_urtartalom = max_urtartalom
        self._jelenlegi_urtartalom = _jelenlegi_urtartalom

    @property
    def jelenlegi_urtartalom(self):
        return self._jelenlegi_urtartalom

    @jelenlegi_urtartalom.setter
    def jelenlegi_urtartalom(self, uj_mennyiseg):
        self._jelenlegi_urtartalom = min(self.max_urtartalom, uj_mennyiseg)
        if self.jelenlegi_urtartalom == 0:
            self.ital = None

    def suly(self):
        return self.max_urtartalom / 35 + self.jelenlegi_urtartalom

    def __str__(self):
        return f"Palack, benne levo ital: {self.ital}, jelenleg {self.jelenlegi_urtartalom} ml van benne, maximum {self.max_urtartalom} ml fer bele."

    def __eq__(self, other):
        if isinstance(other, Palack):
            return self.__dict__ == other.__dict__
        else:
            return False

    def __iadd__(self, other):
        if isinstance(other, Palack):
            if self.ital != other.ital and other.ital is not None:
                if self.jelenlegi_urtartalom > 0 and other.jelenlegi_urtartalom > 0:
                    self.ital = "keverek"
                elif self.jelenlegi_urtartalom == 0 and self.ital is None:
                    self.ital = other.ital

            self.jelenlegi_urtartalom = (
                self.jelenlegi_urtartalom + other.jelenlegi_urtartalom
            )
        return self


class VisszavalthatoPalack(Palack):
    def __init__(self, ital, max_urtartalom, _jelenlegi_urtartalom=1, palackdij=25):
        super().__init__(
            ital, max_urtartalom, _jelenlegi_urtartalom=_jelenlegi_urtartalom
        )
        self.palackdij = palackdij

    def __str__(self):
        return f"Visszavalthato{super().__str__()}"


class Rekesz:
    def __init__(self, max_teherbiras):
        self.max_teherbiras = max_teherbiras
        self.palackok = []

    def suly(self):
        osszSuly = 0
        for palack in self.palackok:
            osszSuly += palack.suly()

        return osszSuly

    def uj_palack(self, palack):
        if self.suly() + palack.suly() <= self.max_teherbiras:
            self.palackok.append(palack)

    def osszes_penz(self):
        osszes_penz = 0
        for palack in self.palackok:
            if isinstance(palack, VisszavalthatoPalack):
                osszes_penz += palack.palackdij
        return osszes_penz


if __name__ == "__main__":
    # p1 = Palack("viz", 3)
    # p2 = Palack("tej", 4)
    # p1 += p2
    # p1 += p2
    # p1 += p2
    # print(p1)

    p1 = Palack(None, 3, 0)
    p2 = Palack("viz", 4, 6)
    p1 += p2
    p1 += p2
    print(p1)
