# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794


def is_disarium(szam):
    szam_string = str(szam)

    res = 0
    for i, szamjegy in enumerate(szam_string):
        res += int(szamjegy) ** (i + 1)
    return res == szam


def letter_combinations(numbers):
    buttons = {
        "2": "abc",
        "3": "def",
        "4": "ghi",
        "5": "jkl",
        "6": "mno",
        "7": "pqrs",
        "8": "tuv",
        "9": "wxzy",
    }

    if len(numbers) == 1:
        return list(buttons[numbers])
    else:
        elozo = letter_combinations(numbers[:-1])
        res = []
        for i, item in enumerate(elozo):
            for j, szam in enumerate(buttons[numbers[-1]]):
                # print(f"adding {item + szam}")
                res.append(item + szam)
        # print(res)
        return res


class Savanyusag:
    def __init__(self, minoseget_megorzi, nyitva, *elemek):
        self.minoseget_megorzi = minoseget_megorzi
        self.nyitva = nyitva
        self.elemek = list(elemek)
        self._tipus = "csalamade" if len(set(elemek)) != 1 else elemek[0]

    @property
    def tipus(self):
        return self._tipus

    @tipus.setter
    def tipus(self, newval):
        if newval in self.elemek:
            self._tipus = newval

    def szavatos(self, ev, honap, nap):
        return (
            ev >= self.minoseget_megorzi[0]
            and (honap >= self.minoseget_megorzi[1] or ev > self.minoseget_megorzi[0])
            and (
                nap >= self.minoseget_megorzi[2]
                or (honap > self.minoseget_megorzi[1] or ev > self.minoseget_megorzi[0])
            )
        )

    def fedel_csavar(self):
        self.nyitva = not self.nyitva

    def __iadd__(self, other):
        if isinstance(other, Savanyusag):
            if not self.nyitva:
                raise Exception("A savanyusag fedele zarva van!")
            elif not other.nyitva:
                raise Exception("A masik savanyusag fedele zarva van!")
            else:
                self.elemek += other.elemek
                self.minoseget_megorzi = (
                    # TODO ez tuti nnem jo
                    min(self.minoseget_megorzi[0], other.minoseget_megorzi[0]),
                    min(self.minoseget_megorzi[1], other.minoseget_megorzi[1]),
                    min(self.minoseget_megorzi[2], other.minoseget_megorzi[2]),
                )
                self.tipus = (
                    "csalamade" if len(set(self.elemek)) != 1 else self.elemek[0]
                )
        return self

    def __str__(self):
        return f"Savanyitott {tipus}, aminek a fedele {'nyitva' if self.nyitva else 'zarva'}"

    def __imul__(self, szorzo):
        uj = []
        for elem in self.elemek:
            for i in range(szorzo):
                uj.append(elem)

        self.elemek = uj
        return self

    def __eq__(self, other):
        return (
            isinstance(other, Savanyusag) and
            self.minoseget_megorzi == other.minoseget_megorzi
            and self.nyitva == other.nyitva
            and sorted(self.elemek) == sorted(other.elemek)
            and self.tipus == other.tipus
        )


if __name__ == "__main__":
    print(is_disarium(175), is_disarium(42))
    print(letter_combinations("532"))
