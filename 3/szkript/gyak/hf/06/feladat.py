# Nev: Tokodi Mate
# Neptun: BCSJ6F
# h: h052794


def legnagyobb_stadion(file_path):
    out_file_path = "legnagyobb.txt"

    legnagyobb_nev = ""
    legnagyobb_varos = ""
    legnagyobb_ferohely = 0

    with open(file_path, "r", encoding="utf-8") as file:
        for stadion in [line.split(",") for line in file.readlines()[1:]]:
            # print(stadion)
            nev = stadion[3].strip()
            varos = stadion[2].strip()
            ferohely = int(stadion[4])
            if ferohely > legnagyobb_ferohely:
                legnagyobb_nev = nev
                legnagyobb_varos = varos
                legnagyobb_ferohely = ferohely

    with open(out_file_path, "w", encoding="utf-8") as outfile:
        outfile.write(
            f"{legnagyobb_nev if legnagyobb_nev else 'Nincs'} ({legnagyobb_varos if legnagyobb_nev else 'Nincs'})"
        )


def osszes_arena(file_path):
    out_file_path = "arena_park.csv"

    res = ""

    with open(file_path, "r", encoding="utf-8") as file:
        for stadion in [line.split(",") for line in file.readlines()[1:]]:
            # print(stadion)

            nev = stadion[3].strip()
            varos = stadion[2].strip()
            ferohely = int(stadion[4])
            orszag = stadion[7].strip()

            if nev.endswith("Arena"):
                res += ",".join((nev, varos, orszag, str(ferohely > 50000))) + "\n"

    with open(out_file_path, "w", encoding="utf-8") as outfile:
        fejlec = "Stadium,City,Country,Big\n"
        outfile.write(fejlec)
        outfile.write(res)


def osszes_park(file_path):
    out_file_path = "arena_park.csv"

    res = ""

    with open(file_path, "r", encoding="utf-8") as file:
        for stadion in [line.split(",") for line in file.readlines()[1:]]:
            # print(stadion)

            nev = stadion[3].strip()
            varos = stadion[2].strip()
            ferohely = int(stadion[4])
            orszag = stadion[7].strip()

            if nev.endswith("Park"):
                res += ",".join((nev, varos, orszag, str(ferohely > 20000))) + "\n"

    with open(out_file_path, "a", encoding="utf-8") as outfile:
        outfile.write(res)


def varosok_szama(file_path, *orszagok):
    out_file_path = "varosok.txt"

    if len(orszagok) == 0:
        raise Exception("Nincs megadva egy orszag sem!")

    varosok = {}

    with open(file_path, "r", encoding="utf-8") as file:
        for stadion in [line.split(",") for line in file.readlines()[1:]]:
            # print(stadion)

            varos = stadion[2].strip()
            orszag = stadion[7].strip()

            if orszag in varosok.keys():
                varosok[orszag].add(varos)
            else:
                varosok[orszag] = set()
                varosok[orszag].add(varos)

    res = ""
    for orszag in orszagok:
        res += orszag + " varosai:\n"
        try:
            res += "\t" + "\t".join(varos + "\n" for varos in sorted(list(varosok[orszag])))
        except KeyError:
            res += ""
        res += "-" * 10 + "\n"

    with open(out_file_path, "w", encoding="utf-8") as outfile:
        outfile.write(res)


if __name__ == "__main__":
    legnagyobb_stadion("stadium.csv")
    osszes_arena("stadium.csv")
    osszes_park("stadium.csv")
    varosok_szama("stadium.csv", "Germany", "Spain", "Hungary")
