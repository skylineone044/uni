// Nev: Tokodi Mate
// Neptun: BCSJ6F
// h: h052794

function matek(input) {
    if (typeof input === "undefined") {
        return 0;
    } else if (typeof input === "string") {
        return 1;
    } else if (typeof input === "number") {
        if (Number.isInteger(input)) {
            if (input % 2 === 0) {
                return input ** input;
            } else {
                return (input - 1) * (input - 1);
            }
        }
    }
    return null;

}

// console.log(matek())
// console.log(matek("szoveg"))
// console.log(matek(12))
// console.log(matek(13))
// console.log(matek(12.5))

