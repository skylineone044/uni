// Nev: Tokodi Mate
// Neptun: BCSJ6F
// h: h052794

function clear(tomb) {
    if (tomb === undefined) {
        return 0;
    }
    res = [];

    for (elem of tomb) {
        if (typeof elem === "number") {
            res.push(elem);
        } else if (typeof elem === "string") {
            if (elem.length >= 1)  {
                res.push(elem.slice(0, 2).toUpperCase())
            }
        }
    }
    return res.reverse();
}

// console.log(clear([2,5,3,'heyho',null,7]))
// console.log(clear([undefined]))
