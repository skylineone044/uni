function call(callback, szoveg) {
    if (typeof callback !== "function" || typeof szoveg !== "string") {
        return null;
    }
    if (szoveg.split(" ").length < 2) {
        return undefined;
    }
    return callback(szoveg.split(" "));
}
function wild(words) { return 'Szavak szama: ' + words.length; }
function lastWord(words) { return words[words.length - 1]; }
console.log(call(wild, 'JavaScript minta ZH'));          // 'Szavak szama: 3'
console.log(call(lastWord, 'JavaScript minta ZH'));      // 'ZH'
console.log(call('Hibas tipus', 'JavaScript minta ZH')); // null
console.log(call(wild, 'JavaScript'));                   // undefined

function beastOfGevaudan(n, m, koords) {
    if (!(typeof n === "number" && typeof m === "number" && typeof koords === "string")) {
        return [];
    }

    let res = []
    let koords_d = []
    for (koord of koords.split(" ")) {
        koords_d.push([Number(koord.split(";")[0]), Number(koord.split(";")[1])])
    }
    for (let sorszam = 0; sorszam < n; sorszam++) {
        res.push([])
        for (let oszlopszam = 0; oszlopszam < m; oszlopszam++) {
            let belerakta = false;
            for (let koord of koords_d) {
                if (sorszam === koord[0] && oszlopszam === koord[1]) {
                    res[sorszam].push("X")
                    belerakta = true;
                }
            }
            if (!belerakta) {
                res[sorszam].push("-")
            }
        }
    }

    return res;
}

console.log(beastOfGevaudan(2, 3, '1;0 0;2 1;2'))
console.log(beastOfGevaudan(3, 5, '2;0 0;3 2;4 10;1 2;3 0;0 2;1'))
console.log(beastOfGevaudan('Hibas tipus', 3, '1;0 0;2 1;2'))

class Album {
    constructor(cim="untitled") {
        this.cim = cim;
        this.hossz = 0;
        this.zenek = [];
    }

    get cim() {
        return this._cim;
    }

    set cim(val) {
        if (typeof val === "string" && val.length >= 3) {
            this._cim = val;
        } else {
            this._cim = "Unknown";
        }
    }

    zenetFelvesz(zene) {
        if (typeof zene === "object" && typeof zene["zeneCim"] !== undefined && zene["zeneHossz"] !== undefined) {
            this.zenek.push(zene["zeneCim"]);
            this.hossz += zene["zeneHossz"];
        }
    }

    albumHossz() {
        return `${Math.floor(this.hossz / 60)} perc, ${this.hossz % 60} masodperc`
    }

    info() {
        return `${this.cim} album, ${this.zenek.length} darab zenevel`
    }

    osszehasonlit(masik) {
        return JSON.stringify(this) === JSON.stringify(masik);
    }

    randomZene() {
        if (this.zenek.length === 0) {
            return "Ures album!";
        }
        return this.zenek[Math.floor(Math.random() * (this.zenek.length))]
    }
}

const album = new Album('Call of the Wild');
album.cim = 'The Sacrament of Sin';
console.log(album.cim);                 // 'The Sacrament of Sin'
album.zenetFelvesz({zeneCim: 'Incense & Iron', zeneHossz: 238});
album.zenetFelvesz({zeneCim: 'Killers With The Cross', zeneHossz: 250});
album.zenetFelvesz({zeneCim: 'Fire & Forgive'});
console.log(album.info());              // 'The Sacrament of Sin album, 2 darabzenevel'
console.log(album.albumHossz());        // '8 perc, 8 masodperc'
console.log(album.randomZene());        // 'Incense & Iron' VAGY 'Killers WithThe Cross'
const masikAlbum = new Album('The Sacrament of Sin');
masikAlbum.hossz = 488;
masikAlbum.zenek = ['Incense & Iron', 'Killers With The Cross'];
console.log(album.osszehasonlit(masikAlbum));   // true