# A gyakorlat elején megnéztük, hogy hogyan lehet Python szkripteket létrehozni és parancssorból futtatni.
# Vegyük az alábbi egyszerű, 2 kiíratásból álló Python programot!

print("Hello friends!")
print("Welcome to my szkriptnyelvek gyakorlat!")

# Nyissuk meg a terminált abban a mappában, ahol a fájl megtalálható, majd itt a `python 01_szkriptek_futtatasa.py`
# (vagy szükség esetén a `python3 01_szkriptek_futtatasa.py`) paranccsal futtassuk le azt!
