# Egy kicsit továbbfejlesztettük az előző gyakorlaton megírt életkor meghatározó programunkat azzal, hogy az aktuális
# évet nem egy beégetett adatként tároltuk, hanem ezt a `datetime` modul segítségével kértük le.

# Először is be kellett húznunk a `datetime` modult.

import datetime

# Ezt követően az aktuális évet lekérhettük a modul felhasználásával: vettük a `datetime` modulban lévő `datetime`
# objektumot, ennek a `now()` metódusával lekértük az aktuális dátumot és időpontot, majd ebből vettük az évet (`year`).

aktualis_ev = datetime.datetime.now().year

# A program többi része már ismerős, ezt már az előző gyakorlaton is megírtuk.

szuletesi_ev = int(input("Melyik évben születtél? "))
eletkor = aktualis_ev - szuletesi_ev
print(str(eletkor) + " éves vagy.")

# A programot úgy is elkészíthettük volna, hogy nem húzzuk be a teljes `datetime` modult, hanem csupán a `datetime`
# objektumot importáljuk be a modul elemei közül. Ekkor az importálás szintaxisa a következő lenne:
# `from datetime import datetime` (ahol az első `datetime` a modul neve, a második `datetime` pedig a beimportálandó
# objektum neve). Ebben az esetben az aktuális év lekérdezése a következő: `aktualis_ev = datetime.now().year`.

# MEGJEGYZÉS: A modulok nem képezik a gyakorlat anyagát, így sem ZH-kban, sem házi feladatokban nem lesznek számonkérve!
