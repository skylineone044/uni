# Vegyük az előzőleg elkészített, életkor meghatározó programunkat!

import datetime

aktualis_ev = datetime.datetime.now().year
szuletesi_ev = int(input("Melyik évben születtél? "))
eletkor = aktualis_ev - szuletesi_ev
print(str(eletkor) + " éves vagy.")

# Döntsük el az `eletkor` változó értékének függvényében, hogy a felhasználó nagykorú-e vagy sem! Ehhez a szelekciós
# vezérlést használjuk, amelynek kulcsszavai: `if`, `else`, `elif` (utóbbi az `else if` Pythonos megfelelője).

# Fontos, hogy Pythonban nem a C-ből ismerős kapcsos zárójeleket használjuk utasításblokkok megadására. Itt úgy
# készíthetünk utasításblokkot, hogy a vezérlési szerkezetünk fejléce után teszünk egy kettőspontot, és azokat az
# utasításokat, amelyek a vezérlési szerkezeteinkhez tartoznak, beljebb indentáljuk (beljebb igazítjuk). Fontos, hogy
# az indentálással konzisztensek legyünk, azaz mindenhol ugyanannyi helyközt használjunk az indentáláskor (pl. 4 darab
# szóközt), mert ellenkező esetben IndentationError hibaüzenetet kapunk!

# Logikai operátorok: and (logikai ÉS), or (logikai VAGY), not (logikai NEM)

if eletkor < 1 or eletkor > 123:        # Pythonban a feltételek körül lévő zárójelek szabadon elhagyhatók.
    print("Érvénytelen életkor!")
elif eletkor < 18:
    print("Kiskorú vagy.")
    korkulonbseg = 18 - eletkor
    print(korkulonbseg, "év múlva leszel nagykorú.")
elif 20 <= eletkor <= 29:               # Itt `eletkor >= 20 and eletkor <= 29` is lehetne, csak az kevésbé Pythonos.
    print("Huszonéves vagy.")
else:
    print("Nagykorú vagy.")

if not eletkor % 2 == 0:
    print("Az életkorod páratlan.")

print("Ez az utasítás az if-en kívül van, így ez mindig le fog futni.")
