# Pythonban az ismétléses vezérlés megvalósításának egyik módja a while-ciklus. Ez ugyanúgy működik, mint C-ben.

# Írassuk ki az egész számokat 1-től 10-ig while-ciklus használatával!

i = 0

while i < 10:
    i += 1              # Ne felejtsük el, hogy Pythonban az `i++` nem működik!
    print(i)

print()

# Oldjuk meg ugyanezt úgy, hogy végtelen ciklusból indulunk ki! Érjük el azt, hogy a páratlan számok ne legyenek kiírva!

j = 0

while True:             # Végtelen ciklust legegyszerűbben így csinálhatunk.
    j += 1

    if j > 10:          # A `break` utasításssal megszakíthatjuk a ciklus futását.
        break

    if j % 2 == 1:      # A `continue` utasítással a következő iterációra ugorhatunk ("kihagyjuk a páratlan számokat").
        continue

    print(j)
