# Pythonban a while-ciklus mellett a for-ciklust is használhatjuk ismétléses vezérlés megvalósítására.

# C-ben a for-ciklus az valahogy így nézett ki: `for (int i = 0; i < n; i++)`, tehát mentünk valamilyen számtól
# valamilyen számig, valamekkora lépésközzel. Ezt hívtuk számlálásos ismétléses vezérlésnek.

# Pythonban a for-ciklus egy picit másképp viselkedik: itt listaszerű for-ciklus van (ezt pár nyelv foreach-nek hívja).
# A listaszerű for-ciklus mindig egy elemsorozat bejárását valósítja meg: a ciklusváltozó rendre felveszi a sorozat
# elemeinek az értékét. Nézzük az alábbi két példát!

for betu in "sajt":         # Itt a `betu` változó rendre felveszi a szöveg karaktereinek az értékét.
    print(betu)

print()

for elem in [10, 20, 30]:   # Itt az `elem` változó rendre felveszi a "tömb" (valójában lista) elemeinek az értékét.
    print(elem)

print()

# Hogyan használhatjuk a Python listaszerű for-ciklusát a C-szerű for-ciklus szimulálására?
# Használjuk a `range()` függvényt! Ez legyárt nekünk egy egész számokból álló elemsorozatot, aminek az
# elemein végig tudunk iterálni.

# A `range()` függvényt többféleképpen is paraméterezhetjük:
#   - `range(n)`: legyártja a [0; n) intervallumba eső egész számokat
#   - `range(a, b)`: legyártja az [a; b) intervallumba eső egész számokat
#   - `range(a, b, k)`: legyártja az [a; b) intervallumba eső egész számokat k lépésközzel.
#
# Fontos észrevenni, hogy a `range()` függvényben a szám, ameddig megyünk, sosem lesz benne a generált sorozatban!

print("0 és 10 közötti egész számok:")

for i in range(11):
    print(i)

print()
print("1 és 10 közötti egész számok:")

for i in range(1, 11):
    print(i)

print()
print("1 és 10 közötti egész számok, 2-es lépésközzel:")

for i in range(1, 11, 2):
    print(i)

# Ha csökkenő sorrendben akarjuk legyártani a számokat (pl. 10-től 1-ig), akkor a `range()` függvény harmadik
# paramétereként megadott lépésközt egy negatív értékre kell állítanunk.

print()
print("10 és 1 közötti egész számok:")

for i in range(10, 0, -1):
    print(i)
