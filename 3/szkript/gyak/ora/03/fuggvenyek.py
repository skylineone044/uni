# Pythonban a függvénydefiníció szintaxisa viszonylag egyszerű. Az alábbiakban definiálunk két függvényt: egy
# paraméter nélküli `hello` függvényt, illetve egy 3 paraméteres `info` függvényt.

def hello():
    print("Aloha!")


# Pythonban a Javából ismerős function overloading nem létezik! Ha Pythonban egy függvényt eltérő paraméterszámmal
# szeretnénk használni, akkor használjuk a default függvényparamétereket! Az alábbi függvény esetén az `eletkor` és
# `foglalkozas` paramétereknek nem kötelező értéket adnunk a függvényhíváskor (opcionális paraméterek). Ha a híváskor
# nem adjuk meg ezen két paraméter értékét, akkor a függvénydefiníciókor megadott alapértékeket veszik fel.

def info(nev, eletkor=30, foglalkozas="munkanélküli"):
    """
    A felhasználó információit visszaadó függvény.
    :param nev: A felhasználó neve.
    :param eletkor: A felhasználó életkora (opcionális).
    :param foglalkozas: A felhasználó foglalkozása (opcionális).
    :return: Egy szöveg, amely a felhasználó információit tartalmazza.
    """

    # A függvényeink működését dokumentálhatjuk a fentihez hasonló docstringek segítségével. A docstring vázának
    # legenerálásához kattintsunk PyCharm-ban a függvény nevére, nyomjunk egy Alt+Entert, majd válasszuk ki a felugró
    # menüből az "Insert documentation string stub" opciót!

    # Az `isinstance()` beépített függvénnyel ellenőrizhetjük, hogy a paraméterek megfelelő típusúak-e.

    if isinstance(nev, str) and isinstance(eletkor, int) and isinstance(foglalkozas, str):
        # Készítünk egy szöveget a paraméterek alapján. Itt használhatnánk (stringre való konvertálással egybekötött)
        # összefűzéseket is, viszont az f-stringek használatával leegyszerűsödik a stringek megadása.

        return f"{nev} egy {eletkor} éves {foglalkozas}."

    # Ez a kiíratás akkor fog lefutni, ha valamelyik paraméter típusa nem megfelelő.
    print("[HIBA] Nem megfelelő típusú paraméter!")


# Amíg a C programok futtatásakor legelőször a `main()`-függvény utasításai kerültek végrehajtásra, addig a Python
# programoknak nincs hagyományos belépési pontja. A belépési pontot viszont lehet szimulálni az alábbi `if`-fel. Az
# `if` után megadott feltétel pontosan akkor lesz igaz, ha közvetlenül ezt a `fuggvenyek.py` nevű fájlt futtatjuk.
# Ha viszont a `valami.py` fájlt futtatjuk, ami egy modulként ágyazza be a `fuggvenyek.py`-t, akkor az `if` után
# megadott feltétel hamisra értékelődik ki, így az `if`-en belüli utasítások nem futnak le.

if __name__ == '__main__':
    hello()

    # A default paraméterek miatt az `info` függvényt 1, 2, illetve 3 paraméterrel is meghívhatjuk.

    print(info("Sanyi"))
    print(info("Jóska", 25))
    print(info("Béla", 40, "programozó"))

    # A típusellenőrzés is működik. Mivel hibás típus esetén nem ad vissza a függvény semmit, ezért itt `None` lesz a
    # visszatérési érték (ez a más nyelvekből ismerős `null` ("üres érték") Pythonos megfelelője).

    print(info("Pista", "macska", "macska"))

    # Ha több opcionális paraméterünk van, akkor olyat is csinálhatunk, hogy a függvényhíváskor expliciten megmondjuk,
    # hogy melyik paraméternek adunk értéket. Most itt a középső `eletkor` paramétert "kihagyjuk".

    print(info("Feri", foglalkozas="vonatkerék pumpáló"))
