# Pythonban az `str` a beépített szöveges adattípus, a string. A stringeket általában aposztrófok vagy idézőjelek
# között adjuk meg. A két megadási mód között lényegi különbség nincs.

s1 = 'Kecske'
s2 = "sajt"

# Három idézőjel között több soros stringeket is megadhatunk. Ezt a három idézőjeles szintaxist dokumentációs
# célokból íródott kommentek esetén is használhatjuk (lásd: `fuggvenyek.py`).

s3 = """Első sor
Második sor"""

print(f"Több soros szöveg:\n{s3}")

# Stringeket összefűzni a `+` operátorral tudunk.

osszefuzott = s1 + s2
print(f"Az összefűzött szöveg: {osszefuzott}")
print()

# Egy szöveg elején és végén lévő helyközöket a `strip()` metódussal távolíthatjuk el.

szoveg = "  Kecskesajtok      "
szoveg = szoveg.strip()
print(f"A helyközök nélküli szöveg: {szoveg}")

# Egy string hosszát (a stringben található karakterek számát) a `len()` függvénnyel kérdezhetjük le.

print(f"A szövegben található karakterek száma: {len(szoveg)}")

# A stringek karaktereinek indexelése Pythonban is 0-tól kezdődik.

print(f"A legelső karakter: {szoveg[0]}")

# Pythonban negatív indexeket is megadhatunk, ekkor a string végétől kezdünk el számolni (-1 lesz az utolsó karakter
# indexe, -2 az utolsó előtti karakteré és így tovább).

print(f"Az utolsó karakter: {szoveg[-1]}")      # Persze `szoveg[len(szoveg) - 1]` is működne.

# Sőt az intervallumos indexeléssel akár egy részstringet is le tudunk kérdezni. Ekkor az index helyén 3 számot
# adhatunk meg kettősponttal elválasztva: `mettől:meddig:lépésköz`. A `meddig` indexű karakter már nem lesz benne
# a kihasított részstringben. Nézzünk néhány példát intervallumos indexelésre!

print(f"1-5. indexű karakterek, 2-es lépésközzel: {szoveg[1:5:2]}")
print(f"1-5. indexű karakterek: {szoveg[1:5]}")     # Ha `lépésköz`-t elhagyjuk, akkor 1-esével lépkedünk.
print(f"Az első 5 karakter: {szoveg[:5]}")          # Ha `mettől`-t elhagyjuk, akkor a legelső karaktertől indulunk.
print(f"Az 1. indexű karaktertől: {szoveg[1:]}")    # Ha `meddig`-et elhagyjuk, akkor a legutolsó karakterig megyünk.
print(f"A teljes szöveg: {szoveg[:]}")              # Mindhárom érték elhagyásával a teljes stringet kapjuk vissza.

# Pythonban egy stringet egyszerűen megfordíthatunk az intervallumos indexeléssel. A `mettől` és `meddig` értékeket
# elhagyjuk (hiszen a teljes string kell), a `lépésköz`-t pedig `-1`-re állítjuk (visszafele lépkedünk egyesével).

print(f"A szöveg megfordítva: {szoveg[::-1]}")
print()

# Pythonban a string egy immutable adattípus. Ez azt jelenti, hogy egy string karaktereit nem tudjuk módosítani!

# szoveg[0] = "F"                       # Ez hibát eredményez!
szoveg = "F" + szoveg[1:]               # Viszont ezzel a trükkel megoldható az első karakter "megváltoztatása".
print(f"Az új szövegünk: {szoveg}")

# Nézzünk meg néhány fontosabb stringkezelő függvényt!

print(f"A szöveg csupa kisbetűkkel: {szoveg.lower()}")
print(f"A szöveg csupa nagybetűkkel: {szoveg.upper()}")
print(f"A szöveg a 'Fecske' stringgel kezdődik-e: {szoveg.startswith('Fecske')}")
print(f"A szöveg a 'sajtok' stringre végződik-e: {szoveg.endswith('sajtok')}")

if "sajt" in szoveg:                                                   # Részstring előfordulásának ellenőrzése.
    print("A 'sajt' string szerepel a szövegben!")

print(f"A 'sajt' string {szoveg.count('sajt')} alkalommal szerepel a szövegben.")
print(f"Az 'e' betűk lecserélése 'a'-ra: {szoveg.replace('e', 'a')}")  # 1 darab betű helyett stringeket is megadhatunk.
print()

# A múlt héten láttunk példát a stringek karaktereinek bejárására. Ismételjük át ezt is!

print("A szöveg karakterei:")

for karakter in szoveg:
    print(karakter)
