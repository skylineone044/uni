# Pythonban a dictionary (szótár) adatszerkezettel kulcs-érték párok sorozatát tárolhatjuk el. Az ilyen, kulcs-érték
# párok tárolására alkalmas adatszerkezetet más nyelvek asszociatív tömbnek vagy map-nek is nevezik.
#    - A dictionary-ben minden kulcs pontosan 1-szer fordul elő. Az értékek között lehet ismétlődés.
#    - A kulcs típusa általában string, az érték típusa tetszőleges lehet.

# Dictionary-k létrehozása.

szemely = {"nev": "Ferenc", "eletkor": 20, "kedves": True}  # A kulcs-érték párokat vesszővel elválasztva soroljuk fel.
ures = {}                                                   # Üres (0 elemű) dictionary.

print("Egy személyt reprezentáló dictionary:", szemely)
print("Egy üres dictionary:", ures)
print("--------------------------------------------------------------------------------------------")

# Egy dictionary adott kulcsához tartozó értékének lekérdezése.

print("A személy neve:", szemely["nev"])                    # 1. módszer: hagyományos indexelés.
print("A személy neve:", szemely.get("nev"))                # 2. módszer: `get()`-metódus használata.

# A különbség a hagyományos indexelés és a `get()`-metódus között, hogy ha a kérdéses kulcs nem szerepel a
# dictionary-ben, akkor a hagyományos indexelés hibát eredményez, míg a `get()` metódus ekkor `None`-nal tér vissza.

# print("A személy foglalkozása:", szemely["foglalkozas"])      # HIBA!!! A program futása megáll!
print("A személy foglalkozása:", szemely.get("foglalkozas"))    # `None`-t kapunk vissza, és a program fut tovább.

# Az `in` operátorral lekérdezhetjük, hogy egy adott kulcs szerepel-e a dictionary-ben.

if "foglalkozas" in szemely:
    print("Ismerjük az emberünk foglalkozását!")

if "eletkor" in szemely:
    print("Ismerjük az emberünk életkorát!")

print("--------------------------------------------------------------------------------------------")

# Egy dictionary tartalmának módosítása.

szemely["eletkor"] = 30                                     # Meglévő kulcshoz tartozó érték módosítása.
print(szemely)

szemely["foglalkozas"] = "szomorúfűz vigasztaló"            # Új kulcs-érték pár beszúrása a dictionary-be.
print(szemely)

del szemely["kedves"]                                       # Adott kulcsnak és az ahhoz tartozó értéknek törlése.
print(szemely)
print("--------------------------------------------------------------------------------------------")

# Egy dictionary tartalmának bejárása:
#    - Bejárhatjuk külön a kulcsokat (lásd: gyakorlati jegyzet).
#    - Bejárhatjuk külön az értékeket (lásd: gyakorlati jegyzet).
#    - Mi most a kulcs-érték párok együttes bejárására nézünk példát ("két legyet egy csapással").

print("A dictionary kulcs-érték párjai:")

for kulcs, ertek in szemely.items():
    print(f"{kulcs} értéke: {ertek}")
