# Pythonban nincs hagyományos tömb adatszerkezet. Több elem tárolására a listákat használhatjuk. A listák dinamikus
# méretűek, így a létrehozásukkor nem kell megadni a méretüket, és "menet közben" is tetszőlegesen lehet listaelemeket
# beszúrni és törölni. Egy kis érdekesség, hogy a dinamikusan típusosság miatt egy listában akár eltérő típusú
# elemeket is tárolhatunk, viszont a gyakorlati életben célszerű azonos típusú elemeket tárolni a listáinkban.

# Egy függvény, amely megduplázza a paraméterben kapott listában található számokat. Ezt majd később használjuk fel.

def duplaz(lista):
    for i in range(len(lista)):
        lista[i] *= 2

    return lista


# Listák létrehozása.

lista1 = [10, 20, 30, 40, 50]
lista2 = []                                     # Üres (0 elemű) lista.
lista3 = [42, "kecske", True]                   # Egy listában akár eltérő típusú elemek is tárolhatók.

print("Az első lista:", lista1)
print("A második lista:", lista2)
print("A harmadik lista:", lista3)
print("--------------------------------------------------------------------------------------------")

# A listák hosszát (azaz a listában tárolt elemek számát) a múlt órán tanult `len()` függvénnyel kérhetjük le.

print("Az első lista hossza:", len(lista1))

# Az alábbiakban a listaelemek indexelésére láthatunk néhány példát.

print("Az első listaelem:", lista1[0])          # Természetesen itt is 0-tól kezdődik az indexelés.
print("Az utolsó listaelem:", lista1[-1])       # A stringeknél tanult negatív indexelés itt is működik.
print("Egy részlista:", lista1[1:4:2])          # A stringeknél tanult intervallumos indexelés itt is működik.
print("Másolat a listáról:", lista1[:])         # A lista lemásolása.
print("A lista megfordítva:", lista1[::-1])     # A lista megfordítása.

# A stringekkel ellentétben a listák módosíthatók. Tehát felülírhatjuk egy adott indexen lévő listaelem értékét.

lista1[0] = 42
print("A módosított lista:", lista1)
print("--------------------------------------------------------------------------------------------")

# A `+` operátorral lehetőségünk van listák összefűzésére.

lista4 = [7, 4, 1] + [2, 5]
print("Az összefűzés után kapott lista:", lista4)

# Fontos megjegyezni, hogy Pythonban a listák referencia szerint működnek! Tehát ha egy függvénynek átadunk egy listát
# paraméterül, akkor az EREDETI listát adjuk át, nem egy másolatot. Megfigyelhető, hogy emiatt a `duplaz()` függvény az
# eredeti listánk tartalmát is módosítja.

# print(duplaz(lista4))                         # [14, 8, 2, 4, 10]
# print(lista4)                                 # [14, 8, 2, 4, 10] (az eredeti lista elemei is megduplázódtak!)

# Ha nem szeretnénk, hogy az eredeti lista elemeit módosítsa a függvény, akkor adjunk át egy másolatot a listánkról!

print("A duplázás után:", duplaz(lista4[:]))    # A `lista4[:]` azt jelenti, hogy a `lista4` egy másolatát adjuk át.
print("Az eredeti lista:", lista4)              # Mivel másolatot adtunk át, ezért az eredeti lista nem módosult.
print("--------------------------------------------------------------------------------------------")

# Az `in` operátorral egyszerűen ellenőrizhetjük, hogy egy adott elem szerepel-e a listánkban.

if 5 in lista4:
    print("Van ötös a listában!")

# Nézzünk meg néhány hasznosabb listakezelő függvényt! Vegyük észre, hogy ezek a függvények közvetlenül az eredeti
# listát módosítják, és nem térnek vissza semmivel (tehát emiatt például `lista4 = lista4.append(1)` helytelen lenne!

lista4.append(1)                                # Elem beszúrása a lista végére.
print("A lista a beszúrás után:", lista4)
lista4.insert(0, 1)                             # Elem beszúrása a lista 0. indexére (azaz az elejére).
print("A lista a beszúrás után:", lista4)

while 1 in lista4:                              # Elem összes előfordulásának törlése a listából.
    lista4.remove(1)                            # (A sima `remove()` csak a LEGELSŐ előfordulást törli.)

print("A lista az 1-esek törlése után:", lista4)

lista4.sort()                                   # A lista elemeinek rendezése.
print("A rendezett lista:", lista4)
print("--------------------------------------------------------------------------------------------")

# Egy szöveget a `split()` függvénnyel feldarabolhatunk adott karakterek mentén. Ekkor a feldarabolás utáni elemeket
# egy listában kapjuk vissza. Így például egyszerűen lekérhetjük egy szöveg szavait.

szoveg = "A Szkriptnyelvek még könnyű tárgynak számít"
szavak = szoveg.split(" ")                      # Szöveg feldarabolása szóköz karakterek mentén.
print("A szöveg szavai egy listában:", szavak)

# FELADAT: Rakjuk bele az 5 karakternél hosszabb szavakat egy új listába, csupa nagybetűkkel!

# MEGOLDÁS: Végigmegyünk a szavakon, és az 5 karakternél hosszabb szavakat belepakolgatjuk egy kezdetben üres listába.

hosszu_szavak = []

for szo in szavak:
    if len(szo) > 5:
        hosszu_szavak.append(szo.upper())

# A fenti feladat egyszerűbben is megoldható a "list comprehension" használatával. Ennek a szintaxisa:
# `[x for x in eredeti_lista if feltetel]`, ahol az `eredeti_lista` azon `x` elemeit beleválogatjuk egy új listába,
# amelyek eleget tesznek a `feltetel`-nek.

hosszu_szavak = [szo.upper() for szo in szavak if len(szo) > 5]

# A `join()` metódussal egy lista elemeit egyesíthetjük egy szöveggé.

eredmeny_szoveg = " ".join(hosszu_szavak)       # A `hosszu_szavak` lista elemeit szóközök mentén egyesítjük.
print("Az 5 karakternél hosszabb szavak, csupa nagybetűkkel:", eredmeny_szoveg)
print("--------------------------------------------------------------------------------------------")

# Természetesen létrehozhatunk többdimenziós listákat is. Ezek olyan listák, amelyeknek az elemei is listák.

matrix = [[1, 2], [3, 4], [5, 6]]               # Egy 2-dimenziós (2D) lista létrehozása.
print("Egy 3x2-es mátrix:")

for sor in matrix:                              # Egy dupla for-ciklussal egyszerűen kiírathatunk egy 2D listát.
    for oszlop in sor:
        print(oszlop, end=" ")
    print()
