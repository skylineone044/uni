# FELADAT: Tekintsük az alábbi dalszöveget! Egy dictionary segítségével készítsünk statisztikát arról, hogy a szövegben
# melyik szó hányszor fordul elő! A szavak vizsgálatakor a kis- és nagybetűket ne különböztessük meg (tehát például
# a `You` és a `you` ugyanazon szó)!

dalszoveg = """We're no strangers to love
You know the rules and so do I
A full commitment's what I'm thinking of
You wouldn't get this from any other guy
I just wanna tell you how I'm feeling
Gotta make you understand"""

# MEGOLDÁS: Készítsünk egy dictionary-t, amiben a kulcsok a dalszövegben található szavak lesznek, a
# kulcsokhoz tartozó értékek pedig azt mondják meg, hogy az adott szó hányszor fordul elő a szövegben!

szavak = dalszoveg.split()          # A szavak megkapásához feldaraboljuk a dalszöveget helyközök mentén.
stat = {}                           # Ebben a dictionary-ben fogjuk elkészíteni a szavakkal kapcsolatos statisztikát.

for szo in szavak:                  # Végigmegyünk a szavakon...
    szo = szo.lower()               # ...a kis- és nagybetűket nem különböztetjük meg a szóban

    if szo not in stat:             # ...ha a szó még nincs benne a dictionary-ben, belerakjuk 1-es előfordulási számmal
        stat[szo] = 1
    else:                           # ...ha a szó már benne van a dictionary-ben, növeljük a hozzá tartozó előfordulást.
        stat[szo] += 1

print(stat)
