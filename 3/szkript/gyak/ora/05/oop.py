# Ezen az órán a Pythonban történő objektumorientált programozás (röviden OOP) került terítékre. Készítettünk egy
# végletekig leegyszerűsített Markmyprofessor klónt: volt egy `Oktato` osztályunk, ami egy oktatót reprezentált,
# akiről eltároltuk a nevét, a kurzusainak számát és az értékeléseit (1.0 és 5.0 közötti valós számok).

# Az osztály neve után, zárójelek között megadhatjuk, hogy az osztályunk melyik osztályból öröklődik. Ha az `object`
# ősosztályból öröklődünk, akkor Python 3-ban az osztály neve utáni `(object)` szabadon elhagyható.

class Oktato(object):
    # Érdekes módon Pythonban a konstruktorban szoktuk létrehozni az adattagokat, és ezzel együtt ugyanitt végezzük azok
    # inicializálását is. Pythonban az `__init__` nevű metódust tekintjük konstruktornak. Pythonban minden
    # metódusnak kötelezően rendelkeznie kell egy első paraméterrel, amit általában `self`-nek hívunk, és ezzel
    # tudunk az aktuális objektumra hivatkozni (ez a Javából ismerős `this` Pythonos megfelelője).

    def __init__(self, nev, kurzusszam=0):  # A `kurzusszam` paraméter megadását opcionálissá tesszük.
        self._nev = nev                     # Konvenció: az alulvonással kezdődő adattagokat nem publikusnak szánjuk.
        self.kurzusszam = kurzusszam        # Meghívjuk a `kurzusszam` settert (ez ellenőrzi a paraméter értékét).
        self.ertekelesek = []

    # A nem publikus adattagokhoz készíthetünk getter és setter metódusokat. Pythonban ezeket általában property-kkel
    # valósítjuk meg. A metódusok fölé írt dekorátorok "kidíszítik" a metódusainkat mindenféle speciális tulajdonsággal.
    # A property-k segítségével úgy használhatjuk a gettert és a settert, mintha egy publikus adattaggal dolgoznánk.

    @property                               # Getter property készítése a `_kurzusszam` adattaghoz.
    def kurzusszam(self):
        return self._kurzusszam

    @kurzusszam.setter                      # Setter property készítése a `_kurzusszam` adattaghoz.
    def kurzusszam(self, ertek):
        if isinstance(ertek, int) and ertek >= 0:   # A kapott paraméter típusának és értékének ellenőrzése.
            self._kurzusszam = ertek
        else:
            print("[HIBA] A kurzusszám értéke nem megfelelő!")
            self._kurzusszam = 0

    # Az `__str__` metódus felüldefiniálásával megmondhatjuk, hogy mi jelenjen meg a konzolon, ha az osztály egy
    # példányát egy `print()`-tel kiíratjuk a képernyőre (objektum szöveggé alakításáért felelő metódus).

    def __str__(self):
        return f"{self._nev} (kurzusok száma: {self._kurzusszam})"

    # Az operator overloading segítségével megmondhatjuk, hogy a hagyományos operátorok hogyan működjenek az osztály
    # példányaira. Ehhez felül kell definiálnunk az operátornak megfelelő metódust.

    def __add__(self, masik):               # A `+` operátor felüldefiniálása.
        # Ha két oktatót adunk össze, akkor ennek az eredménye legyen egy olyan oktató, akinek a kurzusszáma
        # megegyezik a két összeadott oktató kurzusszámainak összegével!

        if isinstance(masik, Oktato):
            uj_oktato = Oktato("Új Oktató", self.kurzusszam + masik.kurzusszam)
            return uj_oktato

        # Ha egy oktatót összeadunk egy egész számmal, akkor ennek az eredménye legyen egy olyan oktató, akinek a
        # kurzusszáma megegyezik az oktató kurzusszámának és az egész számnak az összegével!

        if isinstance(masik, int):
            uj_oktato = Oktato("Új Oktató", self.kurzusszam + masik)
            return uj_oktato

        # Ide még jöhetne valami hibakezelés...

    def __iadd__(self, masik):              # A `+=` operátor felüldefiniálása.
        # Ha egy oktatóhoz `+=`-vel hozzáadunk egy 1.0 és 5.0 közötti valós számot, akkor szúrjuk be ezt a számot
        # az oktató `ertekelesek` listájának végére!

        if isinstance(masik, float) and 1.0 <= masik <= 5.0:
            # Konvenció: A `+`, `-`, `*`, `/` operátorok esetén egy teljesen új objektumot adunk vissza, míg a `+=`,
            # `-=`, `*=`, `/=` operátorok esetén az aktuális objektumot módosítjuk, és ezzel is térünk vissza.

            self.ertekelesek.append(masik)
            return self

        # Ide még jöhetne valami hibakezelés...

    def __eq__(self, masik):                # Az `==` operátor felüldefiniálása.
        if not isinstance(masik, Oktato):   # Ha nem egy másik oktató a paraméter, akkor hamissal térünk vissza.
            return False

        # Ha a paraméter egy másik oktató, akkor adjuk vissza, hogy a két oktató adattagjai rendre megegyeznek-e!

        # Természetesen egyesével is összehasonlítgathatnánk az adattagok értékeit, viszont egy egyszerű "life hack",
        # ha az objektumok `__dict__` adattagjával csinálunk egy dictionary-t mindkét objektumból, ami adattagnév-
        # érték párokat fog tartalmazni, és ezt a két dictionary-t hasonlítjuk össze, hogy megegyezik-e.

        return self.__dict__ == masik.__dict__

    # Egy metódus, ami visszaadja az oktató értékeléseinek átlagát.

    def atlag_ertekeles(self):
        if len(self.ertekelesek) == 0:      # Lekezeljük a nullával osztás hibalehetőséget.
            return 0

        return sum(self.ertekelesek) / len(self.ertekelesek)    # A `sum()` függvény visszaadja a listaelemek összegét.


# A főprogram...

if __name__ == '__main__':
    # Az `Oktato` osztály példányosítása (konstruktor-hívások).

    okt1 = Oktato("Koaxk Ábel", 2)          # A metódusok meghívásakor a `self`-en kívüli paramétereket kell átadnunk.
    okt2 = Oktato("Riz Ottó")               # A második konstruktor-paraméter (`kurzusszam`) elhagyható.
    okt3 = Oktato("Koaxk Ábel", 2)

    # Getter és setter property hívása.

    okt2.kurzusszam = 3                     # Setter hívás (olyan, mintha egy publikus adattagnak adnánk értéket).
    print(okt2.kurzusszam)                  # Getter hívás (olyan, mintha egy publikus adattag értékét kérnénk le).

    # Operator overloading tesztelése.

    okt4 = okt1 + okt2                      # A `+` operátor alkalmazása két oktatóra.
    okt5 = okt1 + 5                         # A `+` operátor alkalmazása egy oktatóra és egy egész számra.

    okt1 += 3.0                             # Értékelések felvétele a `+=` operátorral.
    okt1 += 5.0
    okt1 += 4.6

    okt3 += 3.0
    okt3 += 5.0
    okt3 += 4.6

    print(okt1 == "macska")                 # Az `==` operátor működésének tesztelése.
    print(okt1 == okt2)
    print(okt1 == okt3)
    print(okt1 == okt1)

    # Objektumok kiíratása (ekkor az `__str__` által visszaadott szöveg fog kiíródni a konzolra).

    print(okt1)
    print(okt2)
    print(okt3)
    print(okt4)
    print(okt5)

    # Egy `Oktato` objektum dictionary-ként való kiíratása.

    print(okt1.__dict__)
    print(okt3.__dict__)

    # Az `atlag_ertekeles()` metódus tesztelése.

    print("1. oktató átlaga:", okt1.atlag_ertekeles())
    print("2. oktató átlaga:", okt2.atlag_ertekeles())
