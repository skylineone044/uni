# ======================================================================================================================
# FELADAT: Olvassuk be a be.txt fájl tartalmát, amely oktatók adatait tartalmazza! Példányosítsunk ez alapján `Oktato`
# objektumokat, majd írassuk ki egy kimeneti fájlba, hogy melyik oktató rendelkezik a legjobb átlagértékeléssel!
# ======================================================================================================================

from oktato_teljes import Oktato                    # Beimportáljuk az `oktato.py` fájlban szereplő `Oktato` osztályt.

oktatok = []                                        # Ebben a listában fogjuk majd tárolni a lepéldányosított oktatókat.

# Megnyitjuk a be.txt fájlt olvasásra, UTF-8 karakterkódolással. A `with` utasítással használhatjuk a Python kontextus-
# kezelő mechanizmusát, ami gondoskodik arról, hogy minden esetben megfelelően legyen lezárva a megnyitott fájl (még
# akkor is, ha a fájlkezelés során kivétel dobódik).

with open("be.txt", "r", encoding="utf-8") as file:     # A megnyitott fájlra `file`-ként tudunk hivatkozni.
    # A fájl összes sorának beolvasása egy listába. Figyelem: a `readlines()` függvény megőrzi a sorvége jeleket!

    sorok = file.readlines()

    # A fájl elején egy fejlécsor szerepel, amit nem szeretnénk feldolgozni (mi csak az oktatók adataira vagyunk
    # kíváncsiak). Ezt a fejlécsort elhagyhatjuk, ha a sorok bejárásakor a második (azaz 1. indexű) sortól indulunk.

    for sor in sorok[1:]:
        sor = sor.strip()               # Sorvége jelek eltávolítása az adott sorból.
        darabok = sor.split(";")        # Feldaraboljuk a sort pontosvesszők mentén.

        nev = darabok[0]                # Lementjük a feldarabolás után kapott értékeket egy-egy változóba.
        kurzusszam = int(darabok[1])    # A darabolás utáni adatok alapból stringek, szükség esetén átkonvertáljuk őket.
        ertekelesek = darabok[2].split(" ")

        okt = Oktato(nev, kurzusszam)   # Készítünk egy új `Oktato` objektumot a konstruktor-paraméterek átadásával.

        for ertekles in ertekelesek:    # Minden értékelést felveszünk az oktatóhoz a `+=` operátor segítségével.
            okt += float(ertekles)      # Az értékelést alapból stringként kapjuk, ezért át kell azt konvertálni.

        # print(okt.__dict__)           # Akár ki is írathajuk a létrehozott oktatók adatait a konzolra.
        oktatok.append(okt)             # Minden oktatót beszúrunk az `oktatok` listába.

# Végrehajtunk egy maximumkeresést: meghatározzuk a legjobb átlagértékeléssel rendelkező oktató nevét.

max_atlag = -1                          # Ebben a változóban tároljuk majd a legjobb átlagot.
legjobb_nev = ""                        # Ebben a változóban tároljuk majd a legjobb átlaggal rendelkező oktató nevét.

for oktato in oktatok:                          # Végigmegyünk az oktatókon...
    atlag = round(oktato.atlag_ertekeles(), 2)  # ...kiszámítjuk az oktató átlagát (2 tizedesjegyre kerekítve)...

    if atlag > max_atlag:                       # ...ha az oktató átlaga jobb, mint az eddigi legjobb átlag...
        max_atlag = atlag                       # ...frissítjük a `max_atlag` és `legjobb_nev` változók értékét.
        legjobb_nev = oktato.nev

# Megnyitunk egy ki.txt nevű fájlt írásra, és beleírjuk a legjobb átlaggal rendelkező oktató nevét és átlagát.

with open("ki.txt", "w", encoding="utf-8") as file:
    file.write(f"{legjobb_nev} átlaga: {max_atlag}\n")   # Egy fájlba a `write()` metódussal írhatunk egy szöveget.
    print("A ki.txt-be belekerült a legjobb átlaggal rendelkező oktató...")
