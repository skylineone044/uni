class Oktato(object):
    def __init__(self, nev, kurzusszam=0):
        self.nev = nev
        self.kurzusszam = kurzusszam
        self.ertekelesek = []

    @property
    def kurzusszam(self):
        return self._kurzusszam

    @kurzusszam.setter
    def kurzusszam(self, ertek):
        if isinstance(ertek, int) and ertek >= 0:
            self._kurzusszam = ertek
        else:
            print("[HIBA] A kurzusszám értéke nem megfelelő!")
            self._kurzusszam = 0

    def __str__(self):
        return f"{self.nev} (kurzusok száma: {self._kurzusszam})"

    def __add__(self, masik):
        if isinstance(masik, Oktato):
            uj_oktato = Oktato("Új Oktató", self.kurzusszam + masik.kurzusszam)
            return uj_oktato

        if isinstance(masik, int):
            uj_oktato = Oktato("Új Oktató", self.kurzusszam + masik)
            return uj_oktato

        # Itt dobhatnánk egy kivételt...

    def __iadd__(self, masik):
        if isinstance(masik, float) and 1.0 <= masik <= 5.0:
            self.ertekelesek.append(masik)
            return self

        # Itt dobhatnánk egy kivételt...

    def __eq__(self, masik):
        if not isinstance(masik, Oktato):
            return False

        return self.__dict__ == masik.__dict__

    def atlag_ertekeles(self):
        if len(self.ertekelesek) == 0:
            return 0

        return sum(self.ertekelesek) / len(self.ertekelesek)


if __name__ == '__main__':
    okt1 = Oktato("Koaxk Ábel", 2)
    okt2 = Oktato("Riz Ottó", 0)

    okt2.kurzusszam = 3
    print(okt2.kurzusszam)

    okt3 = okt1 + okt2
    okt3 += 5.0
    okt3 += 4.5

    print(okt3)
    print(okt3.__dict__)
