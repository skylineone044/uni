# ======================================================================================================================
# FELADAT: Egészítsük ki a múlt órán írt `Oktato` osztályunkat kivételek dobásával és kivételkezeléssel!
# ======================================================================================================================

# Pythonban akár saját kivételosztályokat is írhatunk. A saját kivételosztályainkat egy már meglévő kivételosztályból
# kell örököltetnünk. Az alábbiakban létrehozunk egy kivételosztályt, amely az `Exception`-ből öröklődik.

class HibasErtekelesException(Exception):
    def __init__(self, hibas_ertek):                            # Konstruktor készítése.
        super().__init__(f"Hibás értékelés: {hibas_ertek}")     # Ősosztály (`Exception`) konstruktorának meghívása.


class Oktato(object):
    def __init__(self, nev, kurzusszam=0):
        self.nev = nev
        self.kurzusszam = kurzusszam
        self.ertekelesek = []

    @property
    def kurzusszam(self):
        return self._kurzusszam

    @kurzusszam.setter
    def kurzusszam(self, ertek):
        if isinstance(ertek, int) and ertek >= 0:
            self._kurzusszam = ertek
        else:
            # Ha a setter paramétere nem egy nemnegatív egész szám, akkor dobunk egy `Exception` típusú kivételt.
            raise Exception(f"Hibás kurzusszám: {ertek}")

    def __str__(self):
        return f"{self.nev} (kurzusok száma: {self._kurzusszam})"

    def __add__(self, masik):
        if isinstance(masik, Oktato):
            uj_oktato = Oktato("Új Oktató", self.kurzusszam + masik.kurzusszam)
            return uj_oktato

        if isinstance(masik, int):
            uj_oktato = Oktato("Új Oktató", self.kurzusszam + masik)
            return uj_oktato

        # Ha a paraméter típusa nem megfelelő, akkor dobunk egy `TypeError` típusú kivételt.
        raise TypeError(f"Hibás típus: {type(masik)}")

    def __iadd__(self, masik):
        if isinstance(masik, float) and 1.0 <= masik <= 5.0:
            self.ertekelesek.append(masik)
            return self

        # Ha a paraméter nem egy 1.0 és 5.0 közötti valós szám, dobunk egy `HibasErtekelesException` típusú kivételt.
        raise HibasErtekelesException(masik)

    def __eq__(self, masik):
        if not isinstance(masik, Oktato):
            return False

        return self.__dict__ == masik.__dict__

    def atlag_ertekeles(self):
        if len(self.ertekelesek) == 0:
            return 0

        return sum(self.ertekelesek) / len(self.ertekelesek)


if __name__ == '__main__':
    # Pythonban az alábbi 3 utasítást használhatjuk kivételkezelésre:
    # - `try`: az ehhez tartozó blokkba kerülnek azok a kódsorok, amelyekről azt feltételezzük, hogy kivételt dobhatnak
    # - `except`: ennek a segítségével kezelhetünk le különféle kivételeket (ez lényegében a Javából ismerős `catch`)
    # - `finally`: az ehhez tartozó blokkban szereplő utasítások minden esetben lefutnak (kivételdobás esetén is).

    try:
        okt1 = Oktato("Koaxk Ábel", 2)
        okt2 = Oktato("Riz Ottó", 0)

        # okt2.kurzusszam = -3          # Ez a sor egy `Exception` típusú kivétel eldobását eredményezi!
        okt2.kurzusszam = 3
        print(okt2.kurzusszam)

        # okt3 = okt1 + "macska"        # Ez a sor egy `TypeError` típusú kivétel eldobását eredményezi!
        okt3 = okt1 + okt2
        okt3 += "macska"                # Ez a sor egy `HibasErtekelesException` típusú kivétel eldobását eredményezi!
        okt3 += 5.0
        okt3 += 4.5

        print(okt3)
        print(okt3.__dict__)
    except TypeError as te:                     # `TypeError` típusú kivétel lekezelése (`te`-ként hivatkozhatunk rá).
        print("TypeError!", te)
    except HibasErtekelesException as hee:      # `HibasErtekelesException` típusú kivétel lekezelése.
        print("HibasErtekelesException!", hee)
    except Exception:                           # `Exception` típusú kivétel lekezelése.
        print("Nem várt hiba!")
    finally:
        print("Ez mindig lefut.")
