/*
* A múlt alkalommal egy HTML dokumentumba ágyaztuk be a JavaScript kódjainkat, és végső soron a böngészőnkkel futtattuk
* le azt. A Node.js futtatókörnyezet telepítése után (lásd: gyakorlati jegyzet) akár parancssorból is le tudjuk
* futtatni a JavaScript kódjainkat.
*
* Telepítsük a Node.js-t a számítógépünkre! A telepítést követően futtassuk le ezt az egyszerű JavaScript példakódot:
* adjuk ki terminálban a `node 01_futtatas.js` parancsot a projekt mappáján belül! WebStorm használata esetén a
* Jobb Klikk -> Run '01_futtatas.js' opcióval is lefuttathatjuk az alábbi szkriptet.
*/

console.log("- Hogyan köszönnek egymásnak a német kenyerek?");
console.log("- Gluten Tag!");
