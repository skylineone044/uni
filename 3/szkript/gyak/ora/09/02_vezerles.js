/* =========================== A JavaScript vezérlési szerkezetei (vázlatos áttekintés). =========================== */

/*
* Szelekciós vezérlés utasításai: if, else if, else, switch
*   - Összehasonlító operátorok: ==, !=, ===, !==, <, <=, >, >=
*   - Logikai operátorok: &&, ||, !
*/

let szam = 42;

if (szam < 0) {
    console.log("A szám negatív.");
} else if (szam === 0) {
    console.log("A szám nulla.");
} else {
    console.log("A szám pozitív.");
}

let hetNapja = 6;

if (hetNapja >= 1 && hetNapja <= 7)
    console.log("A megadott nap a hétnek egy érvényes napja.");

if (hetNapja === 6 || hetNapja === 7)
    console.log("A megadott nap hétvégére esik.");

switch (hetNapja) {
    case 1: console.log("Hétfő."); break;
    case 2: console.log("Kedd."); break;
    case 3: console.log("Szerda."); break;
    case 4: console.log("Csütörtök."); break;
    case 5: console.log("Péntek."); break;
    case 6: console.log("Szombat."); break;
    case 7: console.log("Vasárnap."); break;
    default: console.log("Nincs ilyen nap!");
}

/*
* Ismétléses vezérlés utasításai: while, do... while, for
*/

console.log("--------------------------------------------");

let i = 1;

while (i <= 5) {
    console.log(i);
    i++;
}

console.log("--------------------------------------------");

let j = 1;

do {
    console.log(j);
    j++;
} while (j <= 5);

console.log("--------------------------------------------");

for (let i = 1; i <= 5; i++) {
    console.log(i);
}
