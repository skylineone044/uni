/*
* Egy `szovegTobbszoroz` nevű függvény definiálása, amely egymás után fűzi az első paraméterben kapott szöveget
* annyiszor, mint amennyi a második paraméterben kapott szám, majd visszatér az így kapott eredménnyel.
* A második, `szam` paramétert nem kötelező megadni. Ha nem adjuk meg, akkor az értéke 1 lesz (default paraméterérték).
*/

function szovegTobbszoroz(szoveg, szam=1) {
    // Hibakezelés: nem megfelelő típusú paraméter (persze lehetne még mindenféle egyéb hibakezelést is végezni...).

    if (typeof szoveg !== "string" || typeof szam !== "number") {   // Típusellenőrzés a `typeof` kulcsszóval.
        console.log("[HIBA] Valamelyik paraméter típusa nem megfelelő!");
        return;
    }

    // Ha a paraméterek típusa megfelelő, akkor elvégezzük az első paraméterben kapott szöveg többszörözését.

    let eredmeny = "";

    for (let i = 0; i < szam; i++)          // A `szoveg`-et `szam`-szor egymás után írjuk.
        eredmeny = eredmeny + szoveg;

    return eredmeny;                        // Visszatérünk az így kapott eredménnyel.
}

console.log(szovegTobbszoroz("sajt", 3));
console.log(szovegTobbszoroz("sajt"));              // A `szam` paraméter az alapértelmezett 1 értéket veszi fel.
console.log(szovegTobbszoroz("sajt", "sajt")); // A hibakezelés fut le. Itt a visszatérési érték `undefined`.
console.log(szovegTobbszoroz());                          // A nem megadott paraméter értéke `undefined` lesz.

/*
* A callback függvények olyan függvények, amelyeket egy másik függvénynek adunk át paraméterül. Egy ilyen callback
* függvényt természetesen meg is tudunk hívni abban a függvényben, ami paraméterül kapja azt.
*/

function orul() {
    console.log("JUHÉÉÉ!");
}

function oraraMegy(milyenOra, fuggveny) {       // A `fuggveny` paraméter egy callback függvény lesz.
    // Ha Szkriptnyelvek órára megyünk, és a második paraméterben ténylegesen egy függvényt kapunk (a függvények típusa
    // "function" lesz), akkor meghívjuk a paraméterben kapott callback függvényt.

    if (milyenOra === "Szkriptnyelvek" && typeof fuggveny === "function")
        fuggveny();
}

oraraMegy("Szkriptnyelvek", orul);      // A callback függvényünk az `orul` függvény lesz.
oraraMegy("Alkalmazott statisztika", orul);

// Olyat is csinálhatunk, hogy a callback függvényünket nem definiáljuk külön a `function` kulcsszóval, hanem "menet
// közben" hozzuk létre, egy úgynevezett anonim függvényként.

oraraMegy("Szkriptnyelvek", function() {
    console.log("Éppen Szkriptnyelvek órára megyünk. Hurrá!");
});

// Az ECMAScript6 (ES6) szabványban bevezették az anonim függvényeknek egy tömörebb megadási módját, az úgynevezett
// arrow function-öket. Ezeknek a szintaxisa: `(függvényparaméterek) => { függvénytörzs }`.

oraraMegy("Szkriptnyelvek", () => {     // Ez az előző függvényhívással ekvivalens.
    console.log("Éppen Szkriptnyelvek órára megyünk. Hurrá!");
});

// MEGJEGYZÉS: callback függvényekre fogunk "értelmesebb" példákat is nézni a későbbiek során.
