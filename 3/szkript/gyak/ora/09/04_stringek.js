/* === Stringek megadása: aposztrófokkal vagy idézőjelekkel. === */

let s1 = 'almas';
let s2 = "pite";

/* === Stringek összefűzése: `+` operátorral. === */

let szoveg = s1 + s2;
console.log("Az összefűzött szöveg:", szoveg);

/* === Stringek hosszának lekérdezése: `length` property-vel (ez egy "adattag" a String osztályon belül). === */

console.log("A szövegben szereplő karakterek száma:", szoveg.length);

/* === Stringek karaktereinek indexelése. === */

console.log("A legelső karakter:", szoveg[0]);      // Itt is 0-tól kezdődően indexeljük a stringek karaktereit.
console.log("A legutolsó karakter:", szoveg[szoveg.length - 1]);
console.log("A -1-edik karakter:", szoveg[-1]);     // A negatív indexelés JavaScriptben nem működik!

// A string itt is immutable (tehát nem lehet a karaktereit módosítani)!

szoveg[0] = 'A';
console.log("Immutable tulajdonság:", szoveg);      // A módosításnak nem lesz hatása!

console.log("----------------------------------------------------------------");

/* === Stringek karaktereinek bejárása. ==== */

// A stringek karaktereit bejárhatjuk hagyományos módon, a string-indexeken végigmenve.

for (let i = 0; i < szoveg.length; i++)
    console.log(szoveg[i]);

console.log("----------------------------------------------------------------");

// A `for... of` szintaxissal a stringek karaktereit "listaszerűen" is bejárhatjuk.

for (let karakter of szoveg)            // Itt `karakter` rendre felveszi a stringben szereplő karakterek értékeit.
    console.log(karakter);

console.log("----------------------------------------------------------------");

// === Fontosabb stringkezelő függvények. ===

// Részstring lekérdezése (ha nem adjuk meg a 2. paraméter értékét, akkor a string végéig megyünk).
console.log("Az 5. indextől kezdve 4 darab karakter:", szoveg.substr(5, 4));
console.log("Az 5-7. indexű karakterek:", szoveg.substring(5, 7));
console.log("A string karakterei az 5. indextől kezdve (1. módszer):", szoveg.substr(5));
console.log("A string karakterei az 5. indextől kezdve (2. módszer):", szoveg.substring(5));

// Kisbetűsítés, nagybetűsítés.
console.log("\nCsupa kisbetűssé alakítás:", szoveg.toLowerCase());
console.log("Csupa nagybetűssé alakítás:", szoveg.toUpperCase());

// Adott stringgel való kezdődés és adott stringre való végződés vizsgálata.
console.log("\nA szöveg 'alma'-val kezdődik:", szoveg.startsWith("alma"));
console.log("A szöveg 'pite'-re végződik:", szoveg.endsWith("pite"));

// Részstring előfordulásának ellenőrzése. Ha a részstring szerepel a stringen belül, akkor az `indexOf()` megadja az
// első előfordulásának kezdőindexét. Ha nem szerepel, akkor az `indexOf()` -1-gyel tér vissza.
console.log("\nA szövegben szerepel a 'mas' string:", szoveg.includes("mas"));
console.log("A 'mas' string előfordulásának kezdőindexe:", szoveg.indexOf("mas"));
console.log("Az 'asd' string előfordulásának kezdőindexe:", szoveg.indexOf("asd"));

// Részstring cseréje. PROBLÉMA: a sima `replace()` függvény csak a LEGELSŐ előforudálást cseréli le!
console.log("\nA szövegben található 'a' betűk lecserélése 'x' betűkre:");
console.log("HIBÁS:", szoveg.replace("a", "x"));

// Az ECMAScript2021 (ES2021) szabványban bevezetett `replaceAll()` függvény az ÖSSZES előfordulást lecseréli.
console.log("HELYES:", szoveg.replaceAll("a", "x"));

// Amíg nem volt `replaceAll()`, addig egyéb alternatívákat használtunk. Például a `replace()` első paramétereként
// megadható egy reguláris kifejezés. Az `/a/g` azt jelenti, hogy a stringben szereplő 'a' betűket cseréljük le! A 'g'
// a 'global' rövidítése, ezzel mondjuk meg, hogy az összes 'a' betűt le akarjuk cserélni a stringben.
console.log("HELYES:", szoveg.replace(/a/g, "x"));

// Egy másik, klasszikus módszer: indítunk egy ciklust, ami addig fut, amíg előfordul a szövegben az adott részstring
// (most az 'a' betű), és minden iterációban kidobunk egy előfordulást a sima `replace()` függvénnyel.

while (szoveg.includes("a"))
    szoveg = szoveg.replace("a", "x");

console.log("HELYES:", szoveg);

// A `trim()` függvénnyel eltávolíthatjuk a szöveg elején és végén szereplő helyközöket.

szoveg = "    almas pite          ";
szoveg = szoveg.trim();
console.log("\nA helyközök nélküli szöveg:", szoveg);

// FONTOS MEGJEGYZÉS: Pythonhoz hasonló módon a JavaScript stringkezelő függvényei se módosítják az eredeti szöveget,
// hanem csupán visszatérnek egy új, módosított szöveggel. Ezért tehát például a `szoveg.trim()` hatására a `szoveg`
// változó tartalma nem módosul, csak akkor, ha értékül adjuk a `trim()` által visszaadott szöveget a változónak
// (ezt csináltuk a fenti példában).

// === A stringek egy elegáns megadási módja: template stringek. ===

// JavaScriptben a template stringekkel nagyon egyszerűen megadhatunk olyan szövegeket, amelyekbe kifejezések (pl.
// változók) értékét szeretnénk behelyettesíteni. A template stringeket backtick-ek (AltGr + 7) közé írjuk, a
// behelyettesítendő értékeket pedig ${} segítségével adjuk meg.

console.log(`Template string példa: ${szoveg}`);
