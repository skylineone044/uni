/* ========================== A JavaScript Math objektumának néhány hasznosabb függvénye. ========================== */

console.log("Valós osztás: " + (5 / 2));
console.log("Egészosztás (lefelé kerekítés): " + Math.floor(5 / 2));
console.log("Felfelé kerekítés: " + Math.ceil(5 / 2));
console.log("Hagyományos kerekítés: " + Math.round(5 / 2));

console.log("Abszolútérték: " + Math.abs(-42));
console.log("Hatványozás: " + Math.pow(2, 10));         // Ezzel ekvivalens: 2 ** 10
console.log("Négyzetgyökvonás: " + Math.sqrt(36));

console.log("Minimum: " + Math.min(7, 4, 1, 2, 5));
console.log("Maximum: " + Math.max(7, 4, 1, 2, 5));

console.log("[0; 1) intervallumba tartozó randomszám: " + Math.random());
console.log("[1; 10] intervallumba tartozó random egész szám: " + Math.floor(Math.random() * 10 + 1));
