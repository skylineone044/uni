// FELADAT: Írj egy `modusz` függvényt, amely egy számokból álló tömböt vár paraméterben, és visszatér a tömbben
// szereplő számok móduszával (azaz a leggyakrabban előforduló számmal)! Ha a függvény nem egy tömböt kap paraméterben,
// akkor a visszatérési érték legyen a -99 szám!

function modusz(szamok) {
    if (!(szamok instanceof Array))              // Hibakezelés: nem tömb típusú paraméter.
        return -99;

    // Ötlet: Számoljuk meg, hogy melyik szám hányszor fordul elő a tömbben! Ezt egy object segítségével könnyedén meg
    // tudjuk tenni: a property-k lesznek a számok, a property-khez tartozó értékek pedig azt fogják megmondani, hogy
    // az adott szám hányszor fordul elő az inputban.

    const stat = {};                            // Ebben az object-ben fogjuk megszámolni, hogy ki hányszor fordul elő.

    for (let szam of szamok) {                  // A tömbben szereplő számok bejárása.
        // Fontos, hogy mivel itt a `szam` változóban tárolt értékre vagyunk kíváncsiak (az aktuális számra), ezért
        // mindenképpen a szögletes zárójeles szintaxist kell használni az object property-jeire való hivatkozáskor! Ha
        // a pont operátort használnánk (`stat.szam`), akkor hibásan egy `szam` nevű property-t keresnénk az object-ben.

        if (stat[szam] === undefined)           // Ha az adott számot még nem láttuk korábban...
            stat[szam] = 1;                     // ...beszúrjuk az object-be 1-es előfordulási értékkel.
        else                                    // Ha már láttuk korábban az adott számot...
            stat[szam]++;                       // ...megnöveljük a hozzá tartozó előfordulás értéket.
    }

    // A módusz meghatározásához lényegében meg kell keresnünk a `stat` object legnagyobb értékéhez tartozó property-jét
    // (tehát a legtöbbször előforduló számot). Ez egy egyszerű maximumkereséssel megoldható.

    let maxElofordulas = -1;                    // Ebben a változóban fogjuk tárolni, hogy a módusz hányszor fordul elő.
    let modusz = -1;                            // Ebben a változóban fogjuk tárolni a móduszt (a leggyakoribb számot).

    for (let [szam, elofordulas] of Object.entries(stat)) {     // Az object property-érték párjainak bejárása.
        if (elofordulas > maxElofordulas) {    // Ha egy szám többször fordult elő, mint az eddigi módusz...
            maxElofordulas = elofordulas;      // ...frissítjük a két változónk értékét ennek megfelelően.
            modusz = szam;
        }
    }

    // A függvény végén visszatérünk a módusszal. Fontos, hogy a móduszt alapból stringként kapjuk meg (mert az
    // object property-jei alapból stringek), ezért azt számmá kell konvertálnunk.

    return Number(modusz);
}

console.log(modusz([1, 2, 3, 1, 4, 3, 5, 3, 5]));
console.log(modusz("Hibás típus!"));
