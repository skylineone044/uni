// === Object létrehozása (property-érték párokból áll). ===

const programozasiNyelv = {nev: "JavaScript", kiadasEve: 1995, logikus: false};
console.log(programozasiNyelv);

// === Adott property-hez tartozó érték lekérdezése. ===

console.log(programozasiNyelv["nev"]);
console.log(programozasiNyelv.nev);

const propertyNeve = "kiadasEve";

console.log(programozasiNyelv[propertyNeve]);
console.log(programozasiNyelv.propertyNeve);                // NEM JÓ!!!
console.log("-------------------------------------------------------------------------------");

// === Property előfordulásának ellenőrzése. ===

if (programozasiNyelv["kiadasEve"] !== undefined)
    console.log("A kiadasEve property szerepel az objektumban.");

console.log("-------------------------------------------------------------------------------");

// === Property-hez tartozó érték módosítása. ===

programozasiNyelv["logikus"] = true;
console.log(programozasiNyelv);

// === Új property-érték pár beszúrása. ===

programozasiNyelv["keszito"] = "Brendan Eich";
console.log(programozasiNyelv);

// === Property-érték pár törlése. ===

delete programozasiNyelv["kiadasEve"];
console.log(programozasiNyelv);
console.log("-------------------------------------------------------------------------------");

// === Object bejárása. ===

for (let [prop, ertek] of Object.entries(programozasiNyelv))
    console.log(prop, "értéke:", ertek);
