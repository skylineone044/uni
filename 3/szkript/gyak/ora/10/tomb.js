// === Tömbök létrehozása. ===

const tomb1 = [7, 4, 1, 2, 5, 9];
const tomb2 = ["macska", 3.14, true, undefined];

// FELADAT: Ellenőrizzük, hogy a `tomb1` változó egy tömböt tárol-e!

// TODO

// === Tömbök hosszának lekérdezése. ===

console.log("tomb1 hossza:", tomb1.length);

// === Tömbök indexelése. ===

console.log(tomb1);
console.log("A legelső tömbelem:", tomb1[0]);
console.log("Az utolsó tömbelem:", tomb1[tomb1.length - 1]);
console.log(tomb1[-1]);                         // Ez JavaScriptben nem működik!

tomb1[0] = 8;                                   // Ez így okés, mert a tömb egy mutable típus.
console.log(tomb1);
console.log("----------------------------------------------------------------------");

// === Tömbök bejárása. ===

for (let i = 0; i < tomb1.length; i++)
    console.log(tomb1[i]);

console.log("----------------------------------------------------------------------");

for (let elem of tomb1)
    console.log(elem);

console.log("----------------------------------------------------------------------");

// FELADAT: Írassuk ki, hogy a `tomb1` tömbben lévő számok közül melyik a legnagyobb!

// TODO

// === Referencia szerinti működés. ===

const nullaz = (tomb) => {
  for (let i = 0; i < tomb.length; i++)
      tomb[i] = 0;

  return tomb;
};

console.log(nullaz(tomb1));
console.log(tomb1);
console.log("----------------------------------------------------------------------");

const tomb3 = [1, 2, 3, 4, 5];

// FELADAT: Duplázzuk meg a `tomb3` tömbben szereplő számokat! A duplázott értékeket adjuk vissza egy új tömbben!

// TODO

// FELADAT: Válogassuk ki a `tomb3` tömbben szereplő értékek közül a páros számokat egy új tömbbe!

// TODO

// FELADAT: Számoljuk ki a `tomb3` tömbben szereplő számok összegét!

// TODO

console.log("----------------------------------------------------------------------");

// === Stringekből tömbök, tömbökből stringek. ===

let szoveg = "    Never gonna give you up            ";
szoveg = szoveg.trim();                                     // String elején és végén lévő helyközök eltávolítása.
console.log("A whitespace-ek nélküli szöveg:", szoveg);

const szavak = szoveg.split(" ");                   // String -> tömb
console.log("A szöveg szavai:", szavak);

const nagybetusSzavak = szavak.map(szo => szo.toUpperCase());
console.log("A szavak csupa nagybetűkkel:", nagybetusSzavak);

const nagybetusSzoveg = nagybetusSzavak.join(" ");          // Tömb -> string
console.log(nagybetusSzoveg);
console.log("----------------------------------------------------------------------");

// === Tömbkezelő függvények. ===

const tomb4 = [7, 4, 1, 2, 5];
console.log(tomb4);

tomb4.pop();                    // Tömb végén lévő elem törlése.
console.log(tomb4);
tomb4.push(8);                  // Elem beszúrása a tömb végére.
console.log(tomb4);

tomb4.shift();                  // Tömb elején lévő elem törlése.
console.log(tomb4);
tomb4.unshift(10);        // Elem beszúrása a tömb elejére.
console.log(tomb4);

const resztomb = tomb4.slice(1, 4); // Résztömb lekérése.
console.log("Az 1-4. indexek közötti tömbelemek:", resztomb);

console.log(tomb4.includes(4));     // Tömbelem előfordulásának ellenőrzése (1. módszer).
console.log(tomb4.indexOf(4));      // Tömbelem előfordulásának ellenőrzése (2. módszer).

tomb4.sort();                       // Tömbelemek rendezése (JÓ???).
console.log("A rendezett tömb:", tomb4);
console.log("----------------------------------------------------------------------");

// === Kétdimenziós tömbök létrehozása és bejárása. ===

const matrix = [[1, 2, 3], [4, 5, 6]];

for (let i = 0; i < matrix.length; i++)
    for (let j = 0; j < matrix[0].length; j++)
        console.log(matrix[i][j]);
