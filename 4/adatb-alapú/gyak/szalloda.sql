
create table Szoba(
  szobaszam NUMBER(3) PRIMARY KEY NOT NULL,
  agyak_szama NUMBER(1),
  erkelyes CHAR(1),
  kategoria VARCHAR(10)
);

create table Szemely(
  szigsz CHAR(8) PRIMARY KEY NOT NULL,
  nev VARCHAR2(100),
  szuletesiDatum DATE,
  telepules VARCHAR2(30),
  utca VARCHAR2(30)
);

create table Foglalas(
 szigsz CHAR(8),
 szobaszam NUMBER(3),
 erkezes DATE,
 tavozas DATE,
 PRIMARY KEY(szigsz,szobaszam,erkezes),
 FOREIGN KEY(szigsz) REFERENCES SZEMELY(szigsz),
 FOREIGN KEY(szobaszam) REFERENCES SZOBA(szobaszam)
);
 
ALTER SESSION SET NLS_DATE_FORMAT = 'YYYY-MM-DD';
 
INSERT into SZEMELY values('145789PA',	'K�roly B�la',	'1982-02-12', 'P�cs', 'F� u. 6');
INSERT into SZEMELY values('122789QA',	'T�th Val�ria',	'1985-05-25', 'Budapest', 'Kossuth u. 2');
INSERT into SZEMELY values('165790SV',	'P�l Botond',	'1985-05-25', 'Szeged', 'H�d u. 31');
INSERT into SZEMELY values('235690BV',	'Miksz�th P�ter','1975-01-10', 'Szeksz�rd', 'Vir�g u. 10');
INSERT into SZEMELY values('165222SQ',	'Kiss Ilona', '1980-10-11', 'Budapest', 'Pet�fi u. 33');
INSERT into SZEMELY values('252223TA',	'T�th Alad�r',	'1962-04-06', 'P�cs', 'K� u. 2');
INSERT into SZEMELY values('423456PQ',	'Szak�cs Piroska', '1977-10-16', 'Szombathely', 'Pipacs u. 27');
INSERT into SZEMELY values('188856BQ',	'L. Nagy Ferenc', '1957-10-30', 'Eger', 'V�r u. 17');
INSERT into SZEMELY values('333321HA',	'E�tv�s Tiham�r', '1961-07-10', 'Szeged', 'Retek u. 4');
INSERT into SZEMELY values('555555AC',	'Kiss Cec�lia', '1969-09-28', 'Szeged', 'F�s� u. 9');
INSERT into SZEMELY values('242354ST',	'Dr. Feh�r Botond', '1959-11-11', 'Debrecen', 'Iskola u. 33');

INSERT into SZOBA values(101, 1, 'N', 'standard');
INSERT into SZOBA values(102, 1, 'N', 'standard'); 
INSERT into SZOBA values(103, 2, 'N', 'standard');
INSERT into SZOBA values(104, 3, 'I', 'standard'); 
INSERT into SZOBA values(105, 2, 'I', 'standard');
INSERT into SZOBA values(106, 1, 'I', 'superior');
INSERT into SZOBA values(107, 2, 'I', 'superior');
INSERT into SZOBA values(108, 1, 'I', 'superior');
INSERT into SZOBA values(109, 3, 'N', 'superior');
INSERT into SZOBA values(110, 4, 'I', 'csal�di');
INSERT into SZOBA values(111, 4, 'I', 'csal�di');
INSERT into SZOBA values(201, 3, 'I', 'standard');
INSERT into SZOBA values(202, 4, 'N', 'csal�di'); 
INSERT into SZOBA values(203, 1, 'N', 'standard'); 
INSERT into SZOBA values(204, 2, 'I', 'standard');
INSERT into SZOBA values(205, 2, 'I', 'superior');
INSERT into SZOBA values(206, 3, 'I', 'superior');
INSERT into SZOBA values(207, 3, 'N', 'standard');
INSERT into SZOBA values(208, 2, 'I', 'luxus');
INSERT into SZOBA values(209, 1, 'I', 'luxus');
INSERT into SZOBA values(210, 2, 'I', 'luxus');
INSERT into SZOBA values(301, 1, 'N', 'standard');
INSERT into SZOBA values(302, 2, 'I', 'standard');
INSERT into SZOBA values(303, 1, 'I', 'superior');
INSERT into SZOBA values(304, 2, 'I', 'superior');
INSERT into SZOBA values(305, 5, 'I', 'csal�di');
INSERT into SZOBA values(306, 4, 'I', 'csal�di');
INSERT into SZOBA values(307, 2, 'I', 'superior');
INSERT into SZOBA values(308, 1, 'I', 'luxus');
INSERT into SZOBA values(309, 2, 'I', 'luxus');

INSERT into FOGLALAS values('242354ST', 101, '2016-02-22', '2016-02-24');
INSERT into FOGLALAS values('145789PA', 101, '2016-05-30', '2016-06-02');
INSERT into FOGLALAS values('252223TA', 102, '2016-06-01', '2016-06-04');
INSERT into FOGLALAS values('235690BV', 103, '2016-06-01', '2016-06-08');
INSERT into FOGLALAS values('165790SV', 105, '2016-06-20', '2017-06-25');
INSERT into FOGLALAS values('165790SV', 102, '2017-01-10', '2017-01-12');
INSERT into FOGLALAS values('333321HA', 101, '2017-02-22', '2017-02-25');
INSERT into FOGLALAS values('333321HA', 301, '2017-04-16', '2017-04-19');
INSERT into FOGLALAS values('145789PA', 101, '2017-10-30', '2017-11-02');
INSERT into FOGLALAS values('145789PA', 102, '2017-12-10', '2018-12-13');
INSERT into FOGLALAS values('555555AC', 102, '2017-04-02', '2017-04-07');
INSERT into FOGLALAS values('333321HA', 310, '2017-09-10', '2017-09-15');
INSERT into FOGLALAS values('145789PA', 101, '2018-02-20', '2018-02-22');
INSERT into FOGLALAS values('122789QA', 106, '2018-02-25', '2018-03-02');
INSERT into FOGLALAS values('165790SV', 106, '2017-10-25', '2017-10-31');
INSERT into FOGLALAS values('165790SV', 108, '2017-01-05', '2017-01-11');
INSERT into FOGLALAS values('333321HA', 105, '2017-07-26', '2017-07-29');
INSERT into FOGLALAS values('235690BV', 110, '2018-03-05', '2018-03-11');
INSERT into FOGLALAS values('165222SQ', 203, '2018-03-06', '2018-03-09');
INSERT into FOGLALAS values('252223TA', 205, '2018-03-05', '2018-03-09');
INSERT into FOGLALAS values('423456PQ', 210, '2017-05-10', '2018-05-13');
INSERT into FOGLALAS values('188856BQ', 307, '2017-10-02', '2017-10-08');
INSERT into FOGLALAS values('423456PQ', 210, '2018-03-02', '2018-03-07');
INSERT into FOGLALAS values('145789PA', 102, '2018-03-21', '2018-03-24');
INSERT into FOGLALAS values('188856BQ', 206, '2018-03-19', '2018-03-25');
INSERT into FOGLALAS values('333321HA', 204, '2018-03-12', '2018-03-16');
INSERT into FOGLALAS values('242354ST', 305, '2018-03-14', '2018-03-19');
INSERT into FOGLALAS values('242354ST', 306, '2018-03-14', '2018-03-19');
INSERT into FOGLALAS values('555555AC', 208, '2018-03-02', '2018-03-05');
INSERT into FOGLALAS values('252223TA', 102, '2018-04-21', '2016-04-25');
INSERT into FOGLALAS values('188856BQ', 309, '2018-05-10', '2018-05-15');