package hu.alkfejl.controller;

import hu.alkfejl.App;
import hu.alkfejl.dao.DAOImpl;
import hu.alkfejl.model.Order;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.util.ResourceBundle;

public class AddOrder implements Initializable {
    @FXML private TextField name;
    @FXML private ComboBox<String> title;
    @FXML private ComboBox<Integer> portion;
    @FXML private DatePicker delivery;
    @FXML private ToggleGroup payment;

    public void onCreateOrder(ActionEvent actionEvent) {
        String name = this.name.getText();
        String title = this.title.getValue();
        int portion = this.portion.getValue();
        var delivery = this.delivery.getValue();
        String payment = ((RadioButton)this.payment.getSelectedToggle()).getText();

        var order = new DAOImpl().save(new Order(name, title, portion, delivery, payment));
        onCancel(actionEvent);
    }

    public void onCancel(ActionEvent actionEvent) {
        App.loadFXML("/fxml/main.fxml");
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // title
        String[] titles = { "Grand Moff", "Captain", "Clone", "Others"};
        title.setItems(FXCollections.observableArrayList(titles));
        title.getSelectionModel().select(0);

        // portion
        Integer[] portions = { 1, 2, 3, 4, 5, 6};
        portion.setItems(FXCollections.observableArrayList(portions));
        portion.getSelectionModel().select(0);
    }
}
