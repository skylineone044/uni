package hu.alkfejl.dao;

import hu.alkfejl.model.Order;

import java.util.List;

public interface DAO {
    List<Order> getOrders();
    Order save(Order order);
}
