package hu.alkfejl.dao;

import hu.alkfejl.config.ConfigurationHelper;
import hu.alkfejl.model.Order;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DAOImpl implements DAO {
    @Override
    public List<Order> getOrders() {
        String path = ConfigurationHelper.getValue("db.url");
        List<Order> result = new ArrayList<>();

        try(Connection c = DriverManager.getConnection(path);
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM TEST")) {

            while(rs.next()) {
                result.add(new Order(
                    rs.getInt("ID"),
                    rs.getString("CUSTOMER"),
                    rs.getString("TITLE"),
                    rs.getInt("PORTION"),
                    LocalDate.parse(rs.getString("DELIVERY")),
                    rs.getString("PAYMENT")));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return result;
    }

/*
    create table if not exists TEST (
      ID integer primary key AUTOINCREMENT,
      CUSTOMER   text not null,
      TITLE      text not null,
      PORTION    integer not null,
      DELIVERY   text not null,
      PAYMENT    text not null
    );
 */

    @Override
    public Order save(Order order) {
        String path = ConfigurationHelper.getValue("db.url");
        String sqlCommand = "INSERT INTO TEST (CUSTOMER, TITLE, PORTION, DELIVERY, PAYMENT) VALUES (?,?,?,?,?)";
        try (Connection c = DriverManager.getConnection(path);
             PreparedStatement stmt = c.prepareStatement(sqlCommand, Statement.RETURN_GENERATED_KEYS)) {


            stmt.setString(1, order.getCustomer());
            stmt.setString(2, order.getTitle());
            stmt.setInt(3, order.getPortion());
            stmt.setString(4, order.getDelivery().toString());
            stmt.setString(5, order.getPayment());

            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                return null;
            }

            ResultSet genKeys = stmt.getGeneratedKeys();
            if (genKeys.next())
                order.setID(genKeys.getInt(1));

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }

        return order;
    }
}
