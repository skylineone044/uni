package hu.alkfejl;

import hu.alkfejl.config.ConfigurationHelper;
import hu.alkfejl.dao.DaoImpl;
import hu.alkfejl.model.Rendeles;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * JavaFX App
 */
public class App extends Application implements Initializable {

    private static Stage stage;

    @FXML private TableView<Rendeles> table;
    @FXML private TableColumn<Rendeles, String> t_nev;
    @FXML private TableColumn<Rendeles, String> t_adag;
    @FXML private TableColumn<Rendeles, String> t_szallitas;
    @FXML private TableColumn<Rendeles, String> t_fizeto;

    public static FXMLLoader loadFXML(String fxml){

        FXMLLoader loader = new FXMLLoader(App.class.getResource(fxml));
        Scene scene = null;
        try {
            Parent root = loader.load();
            scene = new Scene(root);
        } catch (IOException e) {
            e.printStackTrace();
        }
        stage.setScene(scene);
        return loader;
    }

    public void onUjRendeles() {
        try {
            FXMLLoader fxmlLoader = App.loadFXML(("/fxml/uj_rendeles.fxml"));
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void onClose() {
        Platform.exit();
    }

    @Override
    public void start(Stage stage) throws IOException {
        App.stage = stage;
        App.loadFXML("/fxml/main.fxml");
        stage.show();

        System.out.println(ConfigurationHelper.getValue("db.url"));
    }

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        t_nev.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getTitle() + " " + c.getValue().getNev()));
        t_adag.setCellValueFactory(c -> new SimpleStringProperty(Integer.toString(c.getValue().getAdag())));
        t_szallitas.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getSzallitas_ideje().toString()));
        t_fizeto.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getFizetoeszkoz()));

        try {
            table.getItems().setAll(new DaoImpl().getOrders());
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
    }
}