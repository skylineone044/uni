package hu.alkfejl.controller;

import hu.alkfejl.App;
import hu.alkfejl.dao.Dao;
import hu.alkfejl.dao.DaoImpl;
import hu.alkfejl.model.Rendeles;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class UjRendelesController implements Initializable {
    @FXML private TextField nev_tf;
    @FXML private ComboBox titulus_cb;
    @FXML private ComboBox adag_cb;
    @FXML private DatePicker szallitas_ido_date;
    @FXML private RadioButton fiz_cred_radio;
    @FXML private RadioButton fiz_wupi_radio;

    public void onMegse() {
        App.loadFXML("/fxml/main.fxml");
    }

    public void onRendeles() {
        Dao dao = new DaoImpl();
        dao.save(new Rendeles(nev_tf.getText(),
                titulus_cb.getValue().toString(),
                Integer.parseInt(adag_cb.getValue().toString()),
                LocalDate.parse(szallitas_ido_date.getValue().toString()),
                ((fiz_cred_radio.isPressed() && !fiz_wupi_radio.isPressed()) ? "Credit" : "Wupiupi")));
        App.loadFXML("/fxml/main.fxml");
    }

    public void onCredSelect() {
        this.fiz_cred_radio.setSelected(true);
        this.fiz_wupi_radio.setSelected(false);
    }

    public void onWupiSelect() {
        this.fiz_cred_radio.setSelected(false);
        this.fiz_wupi_radio.setSelected(true);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        titulus_cb.getItems().addAll("Grand Moff", "Captain", "Clone", "Others");
        titulus_cb.getSelectionModel().selectFirst();
        adag_cb.getItems().addAll("1", "2", "3","4", "5", "6");
        adag_cb.getSelectionModel().selectFirst();
    }
}
