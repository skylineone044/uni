package hu.alkfejl.dao;

import hu.alkfejl.model.Rendeles;

import java.util.List;

public interface Dao {
    List<Rendeles> getOrders();
    Rendeles save(Rendeles r);

}
