package hu.alkfejl.dao;

import hu.alkfejl.config.ConfigurationHelper;
import hu.alkfejl.model.Rendeles;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DaoImpl implements Dao{
    @Override
    public List<Rendeles> getOrders() {
        String db_path = ConfigurationHelper.getValue("db.url");
        List<Rendeles> res = new ArrayList<>();

        String sql_select = "SELECT id, nev, title, adag, szallitas_ido, fizetoeszkoz FROM Rendelesek";
        try (Connection c = DriverManager.getConnection("jdbc:sqlite:" + db_path);
             Statement s = c.createStatement();
             ResultSet r = s.executeQuery(sql_select)) {

            while (r.next()) {
                res.add(new Rendeles(
                        r.getInt("id"),
                        r.getString("nev"),
                        r.getString("title"),
                        r.getInt("adag"),
                        r.getDate("szallitas_ido").toLocalDate(),
                        r.getString("fizetoeszkoz")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return res;


    }

    @Override
    public Rendeles save(Rendeles r) {
        String db_path = ConfigurationHelper.getValue("db.url");
        String sql_insert = "INSERT INTO Rendelesek (nev, title, adag, szallitas_ido, fizetoeszkoz) VALUES (?,?,?,?,?)";
        try (Connection c = DriverManager.getConnection(db_path);
                PreparedStatement ps = c.prepareStatement(sql_insert, Statement.RETURN_GENERATED_KEYS)) {

            int i = 1;
            ps.setString(i++, r.getNev());
            ps.setString(i++, r.getTitle());
            ps.setInt(i++, r.getAdag());
            ps.setDate(i++, Date.valueOf(r.getSzallitas_ideje()));
            ps.setString(i++, r.getFizetoeszkoz());

            int affectedrows = ps.executeUpdate();
            if (affectedrows == 0) {
                return null;
            }

            ResultSet genkeys = ps.getGeneratedKeys();
            if (genkeys.next()) {
                r.setId(genkeys.getInt(1));
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return r;
    }
}
