package hu.alkfejl.model;

import java.security.InvalidParameterException;
import java.time.LocalDate;

public class Rendeles {
    int id;
    String nev;
    String title;
    int adag;
    LocalDate szallitas_ideje;
    String fizetoeszkoz;

    public Rendeles(int id, String nev, String title, int adag, LocalDate szallitas_ideje, String fizetoeszkoz) {
        this.id = id;
        this.nev = nev;
        this.title = title;
        this.adag = adag;
        this.szallitas_ideje = szallitas_ideje;
        this.fizetoeszkoz = fizetoeszkoz;
    }

    public Rendeles(String nev, String title, int adag, LocalDate szallitas_ideje, String fizetoeszkoz) {
        this.nev = nev;
        this.title = title;
        this.adag = adag;
        this.szallitas_ideje = szallitas_ideje;
        this.fizetoeszkoz = fizetoeszkoz;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNev() {
        return nev;
    }

    public void setNev(String nev) {
        this.nev = nev;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getAdag() {
        return adag;
    }

    public void setAdag(int adag) {
        if (adag < 1 || adag > 6) {
            throw new InvalidParameterException("adag csak 1-6 lehet!");
        }
        this.adag = adag;
    }

    public LocalDate getSzallitas_ideje() {
        return szallitas_ideje;
    }

    public void setSzallitas_ideje(LocalDate szallitas_ideje) {
        this.szallitas_ideje = szallitas_ideje;
    }

    public String getFizetoeszkoz() {
        return fizetoeszkoz;
    }

    public void setFizetoeszkoz(String fizetoeszkoz) {
        this.fizetoeszkoz = fizetoeszkoz;
    }
}
