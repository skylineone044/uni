import dao.ChampionshipDAO;
import jakarta.servlet.annotation.WebServlet;
import model.Race;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

@WebServlet("/DeleteController")
public class DeleteController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ChampionshipDAO dao = ChampionshipDAO.getInstance();
        Race r = new Race();
        r.setID(Integer.parseInt(req.getParameter("ID")));
        dao.delete(r);

        req.getRequestDispatcher("index.jsp").forward(req, resp);
    }
}
