import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.ChampionshipDAO;
import jakarta.servlet.annotation.WebServlet;

import java.io.IOException;

@WebServlet("/IndexController")
public class IndexController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ChampionshipDAO dao = ChampionshipDAO.getInstance();

        boolean needspastraces = "on".equals(req.getParameter("needspastraces"));
        req.setAttribute("races", dao.findAll(needspastraces));
        req.setAttribute("regiekis", needspastraces ? "checked" : "");
//        req.setAttribute("debug", req.getParameter("needspastraces"));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
