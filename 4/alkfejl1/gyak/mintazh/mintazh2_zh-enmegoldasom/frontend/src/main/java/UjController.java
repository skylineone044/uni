import dao.ChampionshipDAO;
import jakarta.servlet.annotation.WebServlet;
import model.Race;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

@WebServlet("/UjController")
public class UjController extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ChampionshipDAO dao = ChampionshipDAO.getInstance();

        try {
            Race r = new Race();
            r.setName(req.getParameter("name"));
            r.setCountry(req.getParameter("country"));
            r.setDate(LocalDate.parse(req.getParameter("date")));
            if (!("".equals(r.getName()) || "".equals(r.getCountry()))) {
                dao.save(r);
            }
        } catch (Exception ignore){
        }

//        req.getRequestDispatcher("index.jsp").forward(req, resp);
        resp.sendRedirect("index.jsp");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("isDisabled", "disabled");
    }
}
