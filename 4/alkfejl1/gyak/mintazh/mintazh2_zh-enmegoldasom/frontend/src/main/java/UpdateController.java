import dao.ChampionshipDAO;
import jakarta.servlet.annotation.WebServlet;
import model.Race;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

@WebServlet("/UpdateController")
public class UpdateController extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ChampionshipDAO dao = ChampionshipDAO.getInstance();

        try {
            Race r = new Race();
            if (req.getParameter("ID") != null && !req.getParameter("ID").isEmpty()) {
                r.setID(Integer.parseInt(req.getParameter("ID")));
                r.setName(req.getParameter("name"));
                r.setCountry(req.getParameter("country"));
                r.setDate(LocalDate.parse(req.getParameter("date")));
                r.setGuests(Integer.parseInt(req.getParameter("guests")));
                if (!("".equals(r.getName()) || "".equals(r.getCountry()) )) {
                    dao.save(r);
                }
            }
        } catch (Exception ignore) {
        }
//        req.getRequestDispatcher("index.jsp").forward(req, resp);
        resp.sendRedirect("index.jsp");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ChampionshipDAO dao = ChampionshipDAO.getInstance();

        Race r = dao.findAll(true).stream().filter(race -> race.getID() == Integer.parseInt(req.getParameter("ID"))).findFirst().orElse(null);
        if (r == null) {
            req.getRequestDispatcher("index.jsp").forward(req, resp);
        } else {
            req.setAttribute("race", r);
            req.getRequestDispatcher("update.jsp").forward(req, resp);
        }


    }
}
