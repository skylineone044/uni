<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<html>
<body>
<h2>Races</h2>
<jsp:include page="/IndexController" />

<table>
    <thead>
        <tr>
<%--                <th> ID </th>--%>
            <th> Name </th>
            <th> Country </th>
            <th> Date </th>
            <th> Guests </th>
            <th> Actions </th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="race" items="${requestScope.races}">
            <tr>
<%--                    <td>${race.ID}</td>--%>
                <td>${race.name}</td>
                <td>${race.country}</td>
                <td>${race.date}</td>
                <td>${race.guests}</td>
                <td>
                    <a href="${pageContext.request.contextPath}/UpdateController?ID=${race.ID}">Update</a>
                    <a href="${pageContext.request.contextPath}/DeleteController?ID=${race.ID}">Delete</a>
                </td>
            </tr>
        </c:forEach>
    </tbody>
</table>
<form method="get" action="index.jsp">
    <label for="needspastraces">Regieket is</label>
    <input name="needspastraces" id="needspastraces" type="checkbox" ${requestScope.regiekis}/>
    <button type="submit" value="OK">OK</button>
</form>
${requestScope.debug}
<a href="uj.jsp" >Uj</a>
</body>
</html>
