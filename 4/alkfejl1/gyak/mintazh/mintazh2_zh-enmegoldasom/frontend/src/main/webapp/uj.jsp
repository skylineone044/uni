<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<html>
<body>
<h2> Add new race</h2>
<jsp:include page="/UjController" />

<form method="post" action="UjController">
    <input type="hidden" value="0" id="ID" name="ID">

    <label for="name">Name</label>
    <input type="text" id="name" name="name"><br>

    <label for="country">Country</label>
    <input type="text" id="country" name="country"><br>

    <label for="date">Date</label>
    <input type="date" id="date" name="date"><br>

    <label for="guests">Guests</label>
    <input type="text" id="guests" name="guests" ${requestScope.isDisabled}><br>

    <button type="submit" value="OK">OK</button>
</form>
</body>
</html>
