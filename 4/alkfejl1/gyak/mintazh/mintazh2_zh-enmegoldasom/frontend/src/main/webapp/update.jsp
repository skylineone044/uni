<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<html>
<body>
<h2>Update a race</h2>
<%--<jsp:include page="/UpdateController" />--%>

<form method="post" action="UpdateController">
    <input type="hidden" value="${requestScope.race.ID}" id="ID" name="ID">

    <label for="name">Name</label>
    <input type="text" id="name" name="name" value="${requestScope.race.name}"><br>

    <label for="country">Country</label>
    <input type="text" id="country" name="country" value="${requestScope.race.country}"><br>

    <label for="date">Date</label>
    <input type="date" id="date" name="date" value="${requestScope.race.date}"><br>

    <label for="guests">Guests</label>
    <input type="text" id="guests" name="guests" ${requestScope.isDisabled} value="${requestScope.race.guests}"><br>

    <button type="submit" value="OK">OK</button>
</form>
</body>
</html>
