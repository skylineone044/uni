package hu.alkfejl.dao;

import hu.alkfejl.model.Contact;

import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import static hu.alkfejl.dao.SqlUtil.*;

public class ContactDAOImpl implements ContactDAO {

    private Properties props;
    private String connectionURL;


    public ContactDAOImpl() {

        props = new Properties();

        try {
            props.load(getClass().getResourceAsStream("/application.properties"));
            connectionURL = props.getProperty("db.url");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Contact> findAll() {
        List<Contact> result = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(connectionURL);
             Statement stmt =  conn.createStatement();
             ResultSet rs = stmt.executeQuery(SELECT_ALL_CONTACTS)){

            while(rs.next()){
                Contact contact = new Contact();
                contact.setId(rs.getInt("id"));
                contact.setName(rs.getString("name"));
                contact.setEmail(rs.getString("email"));
                contact.setAddress(rs.getString("address"));
                contact.setDateOfBirth(LocalDate.parse(rs.getString("dateofBirth")));
                contact.setCompany(rs.getString("company"));
                contact.setPosition(rs.getString("position"));

                result.add(contact);
            }

        }catch (SQLException e){
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public Contact save(Contact contact) {
        return null;
    }

    @Override
    public void delete(Contact contact) {

    }
}
