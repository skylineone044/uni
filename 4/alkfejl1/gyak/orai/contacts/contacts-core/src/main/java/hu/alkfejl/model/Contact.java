package hu.alkfejl.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

public class Contact implements Serializable {

    private int id;
    private String name;
    private String email;
    private List<Phone>phones;
    private String address;
    private LocalDate dateOfBirth;
    private String company;
    private String position;

    public Contact() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phones=" + phones +
                ", address='" + address + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", company='" + company + '\'' +
                ", position='" + position + '\'' +
                '}';
    }
}
