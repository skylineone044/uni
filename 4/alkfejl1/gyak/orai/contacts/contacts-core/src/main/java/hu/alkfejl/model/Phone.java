package hu.alkfejl.model;

import java.io.Serializable;

public class Phone implements Serializable {

    public enum PhoneType{
        HOME("Home"),
        WORK("Work"),
        MOBILE("Mobile"),
        UNKNOWN("Unknown");

        private final String value;

        PhoneType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return getValue();
        }
    }

    private int id;
    private String number;
    private PhoneType type;

    public Phone() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public PhoneType getType() {
        return type;
    }

    public void setType(PhoneType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", type=" + type +
                '}';
    }
}
