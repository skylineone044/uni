package hu.alkfejl;


import hu.alkfejl.dao.ContactDAO;
import hu.alkfejl.dao.ContactDAOImpl;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;


/**
 * JavaFX App
 */
public class App extends Application {

    @Override
    public void start(Stage stage) {
        ContactDAO dao = new ContactDAOImpl();
        StringBuilder result = new StringBuilder();
        dao.findAll().forEach(x -> {
            result.append(x)
                  .append(System.lineSeparator());
        });

        var scene = new Scene(new StackPane(new Label(result.toString())), 1024, 768);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}