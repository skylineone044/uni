package hu.alkfejl.model;

import lombok.*;

import java.io.Serializable;

@ToString
@NoArgsConstructor
@Getter @Setter
public class Phone implements Serializable {

    @ToString
    @AllArgsConstructor(access = AccessLevel.PUBLIC)
    @Getter
    public enum PhoneType{
        HOME("Home"),
        WORK("Work"),
        MOBILE("Mobile"),
        UNKNOWN("Unknown");

        private final String value;
    }
    private int id;
    private String number;
    private PhoneType type;
    private int contactId;

}
