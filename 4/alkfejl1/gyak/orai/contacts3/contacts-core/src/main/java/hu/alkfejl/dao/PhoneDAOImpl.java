package hu.alkfejl.dao;

import hu.alkfejl.model.Contact;
import hu.alkfejl.model.Phone;
import hu.alkfejl.util.ConnectionUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static hu.alkfejl.util.SqlUtil.*;

public class PhoneDAOImpl implements PhoneDAO{

    private String connectionURL;

    public PhoneDAOImpl() {
        connectionURL = ConnectionUtil.getValue("db.url");
    }

    @Override
    public List<Phone> findAllByContactId(Contact contact) {
        return findAllByContactId(contact.getId());
    }

    @Override
    public List<Phone> findAllByContactId(int contactID) {
        List<Phone> result = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(connectionURL);
            PreparedStatement stmt = conn.prepareStatement(SELECT_PHONES_BY_CONTACT)
        ){

            stmt.setInt(1, contactID);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                Phone phone = new Phone();
                phone.setId(rs.getInt("id"));
                phone.setNumber(rs.getString("number"));
                int ord = rs.getInt("phoneType");

                Optional<Phone.PhoneType> type = Arrays.stream(Phone.PhoneType.values()).
                                                        filter(t -> t.ordinal() == ord).
                                                        findAny();
                phone.setType(type.orElse(Phone.PhoneType.UNKNOWN));

                result.add(phone);

            }

        }catch(SQLException e){
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public Phone save(Phone phone, int contactId) {
        try(Connection conn = DriverManager.getConnection(connectionURL);
            PreparedStatement stmt = phone.getId() <= 0 ? conn.prepareStatement(INSERT_PHONE, Statement.RETURN_GENERATED_KEYS) :
                    conn.prepareStatement(UPDATE_PHONE);
        ){


            if(phone.getId() > 0) {
                stmt.setInt(3,phone.getId());
            }else{
                stmt.setInt(3, contactId);
            }
            stmt.setString(1,phone.getNumber());
            stmt.setInt(2, phone.getType().ordinal());


            int affectedRows = stmt.executeUpdate();
            if(affectedRows == 0){
                return null;
            }

            if(phone.getId() <= 0){//insert
                ResultSet genKeys = stmt.getGeneratedKeys();
                if(genKeys.next()){
                    phone.setId(genKeys.getInt(1));
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return phone;
    }

    @Override
    public void delete(Phone phone) {
        try(Connection conn = DriverManager.getConnection(connectionURL);
            PreparedStatement stmt = conn.prepareStatement(DELETE_PHONE)
        ){
            stmt.setInt(1,phone.getId());
            stmt.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public void deleteAll(int contactId) {
        findAllByContactId(contactId).forEach(this::delete);
    }
}
