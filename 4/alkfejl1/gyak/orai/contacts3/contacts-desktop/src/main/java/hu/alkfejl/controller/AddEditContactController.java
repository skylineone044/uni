package hu.alkfejl.controller;

import hu.alkfejl.App;
import hu.alkfejl.dao.ContactDAO;
import hu.alkfejl.dao.ContactDAOImpl;
import hu.alkfejl.dao.PhoneDAO;
import hu.alkfejl.dao.PhoneDAOImpl;
import hu.alkfejl.model.Contact;
import hu.alkfejl.model.Phone;
import javafx.beans.binding.StringBinding;
import javafx.beans.binding.When;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;


public class AddEditContactController implements Initializable {
    private Contact contact;
    private PhoneDAO phoneDao = new PhoneDAOImpl();
    private ContactDAO contactDAO = new ContactDAOImpl();

    @FXML
    private TextField name;
    @FXML
    private TextField email;
    @FXML
    private TextField address;
    @FXML
    private TextField company;
    @FXML
    private TextField position;
    @FXML
    private ListView<Phone> numbers;
    @FXML
    private DatePicker birth;


    public void setContact(Contact c) {
        this.contact = c;
        List<Phone> phones = phoneDao.findAllByContactId(c);
        contact.setPhones(phones);

        name.setText(contact.getName());
        email.setText(contact.getEmail());
        address.setText(contact.getAddress());
        company.setText(contact.getCompany());
        position.setText(contact.getPosition());
        birth.valueProperty().set(contact.getDateOfBirth());
        numbers.getItems().setAll(phones);
    }

    @FXML
    public void onCancel(){
        App.loadFXML("/fxml/main_window.fxml");
    }

    @FXML
    public void onSave(){
        contact.setName(name.getText());
        contact.setEmail(email.getText());
        contact.setAddress(address.getText());
        contact.setCompany(company.getText());
        contact.setPosition(position.getText());
        contact.setDateOfBirth(birth.getValue());

        Contact c = contactDAO.save(contact);
        phoneDao.deleteAll(c.getId());
        contact.getPhones().forEach(phone -> {
            // Ez azért kell, mert ilyenkor  a save egy update-et csinálna (ha id>0), de az ID nincs benne a db-ben
            // tehát be kell állítani 0-ra, különben az összes, a contacthoz tartozó számot törli a vérbe.
            phone.setId(0);
            phoneDao.save(phone,c.getId());
        });
        App.loadFXML("/fxml/main_window.fxml");
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        numbers.setCellFactory(param ->{
            ListCell<Phone> cell = new ListCell<>();
            ContextMenu contextMenu = new ContextMenu();

            MenuItem edit = new MenuItem("Edit");
            MenuItem delete = new MenuItem("Delete");

            contextMenu.getItems().addAll(edit, delete);

            edit.setOnAction(event ->{
                Phone item = cell.getItem();
                showPhoneDialog(item);
            });

            delete.setOnAction(event ->{
                Phone item = cell.getItem();
                contact.getPhones().remove(item);
                //Hogy property nélkül is eltűnjön a listából, a ListView-ból is törölni kell
                numbers.getItems().remove(item);


            });

            StringBinding cellText = new When(cell.itemProperty().isNotNull()).then(cell.itemProperty().asString()).otherwise("");
            cell.textProperty().bind(cellText);

            cell.emptyProperty().addListener((observableValue, isEmpty, isNowEmpty) -> {
                if(isNowEmpty){
                    cell.setContextMenu(null);
                }else{
                    cell.setContextMenu(contextMenu);
                }
            });

            return cell;
        });
    }

    @FXML
    public void addNewPhone(){
        showPhoneDialog();
    }

    private void showPhoneDialog(Phone phone) {
        Stage stage = new Stage();
        //kikattintás ellen
        stage.initModality(Modality.APPLICATION_MODAL);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/add_edit_phone.fxml"));

        try {
            Parent root = loader.load();
            AddEditPhoneController controller = loader.getController();
            controller.init(stage, phone, contact);
            stage.setScene(new Scene(root));
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showPhoneDialog(){
        Phone phone = new Phone();
        phone.setId(0);
        showPhoneDialog(phone);
    }
}
