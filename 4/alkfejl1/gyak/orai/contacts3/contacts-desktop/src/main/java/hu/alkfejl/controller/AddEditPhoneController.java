package hu.alkfejl.controller;

import hu.alkfejl.model.Contact;
import hu.alkfejl.model.Phone;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class AddEditPhoneController implements Initializable {

    private Stage stage;
    private Phone phone;
    private Contact contact;


    public void init(Stage stage,Phone phone, Contact contact) {
        this.stage = stage;
        this.phone = phone;
        this.contact = contact;
        number.setText(phone.getNumber());
        Phone.PhoneType currType = phone.getType();
        type.setValue(currType == null ? Phone.PhoneType.HOME : currType);
    }

    @FXML
    private TextField number;

    @FXML
    private ComboBox<Phone.PhoneType> type;

    @FXML
    public void onCancel(){
        stage.close();
    }


    @FXML
    public void onSave(){
        phone.setNumber(number.getText());
        phone.setType(type.getValue());
        if(phone.getId() == 0){
            contact.getPhones().add(phone);
        }
        stage.close();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        type.getItems().setAll(Phone.PhoneType.values());
    }
}
