package hu.alkfejl.controller;

import hu.alkfejl.App;
import hu.alkfejl.dao.ContactDAO;
import hu.alkfejl.dao.ContactDAOImpl;
import hu.alkfejl.model.Contact;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;

import java.net.URL;
import java.util.ResourceBundle;

public class MainWindowController implements Initializable {

    ContactDAO dao = new ContactDAOImpl();

    @FXML
    private TableView<Contact> contactTable;

    @FXML
    private TableColumn<Contact, String> nameColumn;

    @FXML
    private TableColumn<Contact, String> emailColumn;

    @FXML
    private TableColumn<Contact, Void> actionsColumn;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        refreshTable();
        nameColumn.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getName()));
        emailColumn.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getEmail()));

        actionsColumn.setCellFactory(param -> new TableCell<>(){
                private final Button delete = new Button("delete");
                private final Button edit = new Button("edit");

                {
                    delete.setOnAction(event -> {
                        Contact c = getTableRow().getItem();
                        deleteContact(c);
                        refreshTable();
                    });

                    edit.setOnAction(event -> {
                        Contact c  = getTableRow().getItem();
                        editContact(c);
                        refreshTable();
                    });
                }

            @Override
            protected void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                //ha nincs adat, nem kell gomb se.
                if(empty){
                    setGraphic(null);
                }else{//ha igen, kell...
                    HBox hb = new HBox();
                    hb.getChildren().addAll(edit, delete);
                    hb.setSpacing(10.0);
                    setGraphic(hb);
                }
            }
        });
    }

    @FXML
    public void onExit(){
        Platform.exit();
    }

    @FXML
    public void onAddNewContact(){
        Contact c = new Contact();
        c.setId(0);
        editContact(c);
    }

    private void deleteContact(Contact c){
        Alert confirmationAlert = new Alert(Alert.AlertType.CONFIRMATION,"Are you 🍺?",ButtonType.YES,ButtonType.NO);
        confirmationAlert.showAndWait().ifPresent(buttonType -> {
            if(buttonType.equals(ButtonType.YES)){
                dao.delete(c);
            }
        });
    }

    private void editContact(Contact c){
        FXMLLoader loader = App.loadFXML("/fxml/add_edit_contact.fxml");
        AddEditContactController controller = loader.getController();
        controller.setContact(c);
    }

    private void refreshTable(){
        contactTable.getItems().setAll(dao.findAll());
    }
}
