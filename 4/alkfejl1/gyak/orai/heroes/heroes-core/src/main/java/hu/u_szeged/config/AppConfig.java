package hu.u_szeged.config;

import java.io.IOException;
import java.util.Properties;

/**
 * Useful for getting the properties defined in application.properties file.
 */
public class AppConfig {
    private static final Properties props = new Properties();

    static {
        try {
            props.load(Thread.currentThread()
                    .getContextClassLoader()
                    .getResourceAsStream("application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets all the properties defined in application.properties.
     * @return - properties of our application
     */
    public static Properties getProps() {
        return props;
    }

    /**
     * Gets a specified key's value in application.properties.
     * @param key - the name of the key
     * @return - the value of prop defined by the passed key
     */
    public static String getValue(String key) {
        return props.getProperty(key);
    }

    public static String getDbUrl() {
        return props.getProperty("db.url");
    }
}
