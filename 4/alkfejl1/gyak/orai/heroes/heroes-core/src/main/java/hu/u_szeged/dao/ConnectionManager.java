package hu.u_szeged.dao;

import hu.u_szeged.config.AppConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Helps to connect to the db specified in application.properties.
 */
public class ConnectionManager {
    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(AppConfig.getDbUrl());
    }
}
