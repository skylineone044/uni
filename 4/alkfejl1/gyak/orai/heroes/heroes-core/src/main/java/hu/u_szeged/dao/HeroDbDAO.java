package hu.u_szeged.dao;

import hu.u_szeged.config.AppConfig;
import hu.u_szeged.models.Hero;

import java.io.File;
import java.sql.*;
import java.util.*;

public class HeroDbDAO implements IHeroDAO {
    public static final String tableId = "heroes";

    // constructors
    public HeroDbDAO() {
        String createQuery = "CREATE TABLE IF NOT EXISTS %s (".formatted(tableId) +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "name TEXT NOT NULL, " + "race TEXT NOT NULL, " +
                "defense INTEGER NOT NULL, " +
                "strength INTEGER NOT NULL, " +
                "intelligence INTEGER NOT NULL, " +
                "dexterity INTEGER NOT NULL" +
                ");";

        String dbPath = AppConfig.getDbUrl();
        String path = dbPath.replace("jdbc:sqlite:", "");
        File file = new File(path);
        File parent = new File(file.getParent());
        if (!parent.exists()) {
            boolean created = parent.mkdirs();
            if (!created) System.err.println("Could not create database directory!");
        }

        try (Connection connection = ConnectionManager.getConnection()) {
            Statement stmt = connection.createStatement();
            stmt.execute(createQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // interface method implementations
    public List<Hero> selectAll() {
        try (Connection connection = ConnectionManager.getConnection()) {
            Statement stmt = connection.createStatement();
            ResultSet res = stmt.executeQuery("SELECT * FROM %s;".formatted(tableId));

            List<Hero> heroes = new ArrayList<>();

            while (res.next()) {
                int id = res.getInt("id");
                String name = res.getString("name");
                String race = res.getString("race");
                int defense = res.getInt("defense");
                int strength = res.getInt("strength");
                int intelligence = res.getInt("intelligence");
                int dexterity = res.getInt("dexterity");
                heroes.add(new Hero(id, name, race, defense, strength, intelligence, dexterity));
            }

            return heroes;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Hero selectOneById(int id) {
        try (Connection connection = ConnectionManager.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM %s WHERE id=?;".formatted(tableId));
            stmt.setMaxRows(1);
            stmt.setInt(1, id);
            ResultSet res = stmt.executeQuery();
            stmt.clearParameters();
            res.next();

            String name = res.getString("name");
            String race = res.getString("race");
            int defense = res.getInt("defense");
            int strength = res.getInt("strength");
            int intelligence = res.getInt("intelligence");
            int dexterity = res.getInt("dexterity");
            return new Hero(id, name, race, defense, strength, intelligence, dexterity);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean deleteOneById(int id) {
        try (Connection connection = ConnectionManager.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE * FROM %s WHERE id=?;".formatted(tableId));
            stmt.setMaxRows(1);
            stmt.setInt(1, id);
            stmt.executeUpdate();
            stmt.clearParameters();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean insertOne(Hero hero) {
        try (Connection connection = ConnectionManager.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO %s".formatted(tableId) +
                            "(name, race, defense, strength, intelligence, dexterity)" +
                            "VALUES (?, ?, ?, ?, ?, ?);");
            stmt.setString(1, hero.getName());
            stmt.setString(2, hero.getRace());
            stmt.setInt(3, hero.getDefense());
            stmt.setInt(4, hero.getStrength());
            stmt.setInt(5, hero.getIntelligence());
            stmt.setInt(6, hero.getDexterity());
            stmt.executeUpdate();
            stmt.clearParameters();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean updateOneById(int id, Hero hero) {
        try (Connection connection = ConnectionManager.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE %s".formatted(tableId) +
                            "SET name = ?, race = ?, defense = ?, strength = ?, intelligence = ?, dexterity = ?" +
                            "WHERE id = ?;");
            stmt.setString(1, hero.getName());
            stmt.setString(2, hero.getRace());
            stmt.setInt(3, hero.getDefense());
            stmt.setInt(4, hero.getStrength());
            stmt.setInt(5, hero.getIntelligence());
            stmt.setInt(6, hero.getDexterity());
            stmt.setInt(7, id);
            stmt.executeUpdate();
            stmt.clearParameters();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
