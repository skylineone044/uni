package hu.u_szeged.dao;

import hu.u_szeged.models.Hero;

import java.util.List;

// TODO: implement methods for memory dao
public class HeroMemoryDAO implements IHeroDAO {
    public List<Hero> selectAll() {
        throw new UnsupportedOperationException();
    }

    public Hero selectOneById(int id) {
        throw new UnsupportedOperationException();
    }

    public boolean deleteOneById(int id) {
        throw new UnsupportedOperationException();
    }

    public boolean insertOne(Hero hero) {
        throw new UnsupportedOperationException();
    }

    public boolean updateOneById(int id, Hero hero) {
        throw new UnsupportedOperationException();
    }
}
