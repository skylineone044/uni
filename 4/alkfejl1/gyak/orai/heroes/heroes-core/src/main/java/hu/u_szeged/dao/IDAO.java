package hu.u_szeged.dao;

import java.util.List;

public interface IDAO<T> {
    List<T> selectAll();
    T selectOneById(int id);
    boolean deleteOneById(int id);
    boolean insertOne(T hero);
    boolean updateOneById(int id, T hero);
}
