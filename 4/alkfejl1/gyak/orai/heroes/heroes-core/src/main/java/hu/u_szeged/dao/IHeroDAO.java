package hu.u_szeged.dao;

import hu.u_szeged.models.Hero;

import java.util.List;

/**
 * Interface for our Data Access Object.
 * Use it to interface with database implementations.
 */
public interface IHeroDAO extends IDAO<Hero> {
    List<Hero> selectAll();
    Hero selectOneById(int id);
    boolean deleteOneById(int id);
    boolean insertOne(Hero hero);
    boolean updateOneById(int id, Hero hero);
}
