package hu.u_szeged.models;

/**
 * Model class for storing hero objects.
 * These objects will be stored in our db.
 */
public class Hero {
    // props
    private Integer id;
    private String name;
    private String race;
    private Integer defense;
    private Integer strength;
    private Integer intelligence;
    private Integer dexterity;

    // constructors
    public Hero() {}

    public Hero(String name, String race, Integer defense, Integer strength, Integer intelligence, Integer dexterity) {
        this.name = name;
        this.race = race;
        this.defense = defense;
        this.strength = strength;
        this.intelligence = intelligence;
        this.dexterity = dexterity;
    }

    public Hero(Integer id, String name, String race, Integer defense, Integer strength, Integer intelligence, Integer dexterity) {
        this.id = id;
        this.name = name;
        this.race = race;
        this.defense = defense;
        this.strength = strength;
        this.intelligence = intelligence;
        this.dexterity = dexterity;
    }

    // public getters and setters
    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDefense() {
        return defense;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public Integer getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public Integer getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public Integer getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }
}
