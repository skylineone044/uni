module heroes.core {
    requires java.sql;

    exports hu.u_szeged.models;
    exports hu.u_szeged.dao;
}