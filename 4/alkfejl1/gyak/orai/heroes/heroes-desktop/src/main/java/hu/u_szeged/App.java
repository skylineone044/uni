package hu.u_szeged;

import hu.u_szeged.factories.HeroControllerFactory;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Entry point of our application.
 */
public class App extends Application {
    private static Stage stage;
    private static Scene scene;

    @Override
    public void start(Stage stage) throws Exception {
        App.stage = stage;
        scene = new Scene(loadFXML("base"));
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Helps to load fxml resource files.
     * @param fxml - file to load from resources, without the .fxml postfix
     * @return - loaded fxml file as a Parent Node
     * @throws IOException - if there is an error on loading the file
     */
    public static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setControllerFactory(new HeroControllerFactory());
        loader.setLocation(App.class.getResource(fxml + ".fxml"));
        return loader.load();
    }

    public static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    public static void setSize(double width, double height) {
        if (stage != null) {
            stage.setWidth(width);
            stage.setHeight(height);
        }
    }

    public static void setMinSize(double width, double height) {
        if (stage != null) {
            stage.setMinWidth(width);
            stage.setMinHeight(height);
        }
    }

    public static void setSizeAndMinSize(double width, double height) {
        setSize(width, height);
        setMinSize(width, height);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
