package hu.u_szeged.controllers;

import hu.u_szeged.App;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.Pane;

import java.io.IOException;

import static hu.u_szeged.App.loadFXML;

public class BaseController {
    @FXML
    private MenuBar menuBar;
    @FXML
    private Pane rootPane;

    private void setRoot(String fxml) throws IOException {
        Pane root = (Pane) loadFXML(fxml);
        double width = root.getPrefWidth();
        double height = root.getPrefHeight() + menuBar.getPrefHeight() + 40;
        rootPane.getChildren().setAll(root);
        App.setSizeAndMinSize(width, height);
    }

    @FXML
    private void initialize() throws IOException {
        setRoot("heroAdd");
    }

    @FXML
    private void onCloseMenuItemAction() {
        Platform.exit();
    }

    @FXML
    private void switchToAddHeroes() throws IOException {
        setRoot("heroAdd");
    }

    @FXML
    private void switchToListHeroes() throws IOException {
        setRoot("heroList");
    }
}
