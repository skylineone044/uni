package hu.u_szeged.controllers;

import hu.u_szeged.dao.IDAO;
import hu.u_szeged.models.Hero;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;

public class HeroAddController {
    private final IDAO<Hero> dao;

    @FXML
    private TextField nameTextField;
    @FXML
    private ChoiceBox<String> raceChoiceBox;
    @FXML
    private Spinner<Integer> defenseSpinner;
    @FXML
    private Spinner<Integer> strengthSpinner;
    @FXML
    private Spinner<Integer> intelligenceSpinner;
    @FXML
    private Spinner<Integer> dexteritySpinner;

    public HeroAddController(IDAO<Hero> dao) {
        this.dao = dao;
    }

    private boolean checkRequiredHeroData() {
        return nameTextField.getText().isEmpty() || raceChoiceBox.getSelectionModel().isEmpty();
    }

    private Hero getHeroData() {
        return new Hero(nameTextField.getText(),
                raceChoiceBox.getValue(),
                defenseSpinner.getValue(),
                strengthSpinner.getValue(),
                intelligenceSpinner.getValue(),
                dexteritySpinner.getValue());
    }

    private void resetHeroData() {
        nameTextField.setText("");
        raceChoiceBox.getSelectionModel().clearSelection();
        defenseSpinner.getValueFactory().setValue(0);
        strengthSpinner.getValueFactory().setValue(0);
        intelligenceSpinner.getValueFactory().setValue(0);
        dexteritySpinner.getValueFactory().setValue(0);
    }

    @FXML
    private void onHeroSaveAction() {
        if (checkRequiredHeroData()) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Name and Race should be filled!");
            alert.show();
            return;
        }
        Hero hero = getHeroData();
        dao.insertOne(hero);
    }

    @FXML
    private void onHeroSaveAndClearAction() {
        if (checkRequiredHeroData()) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Name and Race should be filled!");
            alert.show();
            return;
        }
        Hero hero = getHeroData();
        dao.insertOne(hero);
        resetHeroData();
    }

    @FXML
    private void onHeroClearAction() {
        resetHeroData();
    }
}
