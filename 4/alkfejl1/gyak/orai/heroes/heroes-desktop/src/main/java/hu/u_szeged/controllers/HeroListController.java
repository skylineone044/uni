package hu.u_szeged.controllers;

import hu.u_szeged.dao.IDAO;
import hu.u_szeged.models.Hero;
import hu.u_szeged.utils.FXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;

public class HeroListController {
    private final IDAO<Hero> dao;

    @FXML
    private TableView<Hero> heroTableView;

    public HeroListController(IDAO<Hero> dao) {
        this.dao = dao;
    }

    private void buildRowContext() {
        heroTableView.setRowFactory(table -> {;
            TableRow<Hero> row = new TableRow<>();
            MenuItem editItem = new MenuItem("Edit");
            MenuItem deleteItem = new MenuItem("Delete");
            editItem.setOnAction(event -> this.rowEditEventHandler(event, row));
            deleteItem.setOnAction(event -> this.rowDeleteEventHandler(event, row));
            ContextMenu contextMenu = new ContextMenu();
            contextMenu.getItems().setAll(editItem, deleteItem);
            row.setContextMenu(contextMenu);
            return row;
        });
    }

    /*
    TODO: implement hero edit feature using the methods provided.
    Note: reuse heroAdd.fxml
     */
    private void rowEditEventHandler(ActionEvent actionEvent, TableRow<Hero> row) {
        throw new UnsupportedOperationException();
    }

    /*
    TODO: implement hero delete feature using the methods provided.
     */
    private void rowDeleteEventHandler(ActionEvent actionEvent, TableRow<Hero> row) {
        throw new UnsupportedOperationException();
    }

    @FXML
    private void initialize() {
        FXUtils.buildTable(heroTableView, dao.selectAll());
        buildRowContext();
    }
}
