package hu.u_szeged.factories;

import hu.u_szeged.dao.IDAO;
import javafx.util.Callback;

import java.lang.reflect.InvocationTargetException;

/**
 * Factory class for javafx controllers.
 * Provides two types of factory methods:
 * - parameterless instance factory
 * - one parameter instance factory, where the parameter required is the DAO used by the controller
 */
public abstract class ControllerFactory<T extends IDAO<?>> implements Callback<Class<?>, Object> {
    private final Class<T> builder;

    protected ControllerFactory(Class<T> factoryClass) {
        builder = factoryClass;
    }

    @Override
    public Object call(Class<?> clazz) {
        try {
            return clazz.getConstructor(IDAO.class).newInstance(builder.getConstructor().newInstance());
        } catch (InstantiationException |
                IllegalAccessException |
                InvocationTargetException |
                NoSuchMethodException e0) {
            try {
                return clazz.getConstructor().newInstance();
            } catch (InstantiationException |
                    IllegalAccessException |
                    InvocationTargetException |
                    NoSuchMethodException e1) {
                e1.printStackTrace();
            }
        }
        return null;
    }
}
