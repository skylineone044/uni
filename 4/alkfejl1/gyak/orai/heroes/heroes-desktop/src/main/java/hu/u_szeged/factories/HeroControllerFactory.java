package hu.u_szeged.factories;

import hu.u_szeged.dao.HeroDbDAO;

public class HeroControllerFactory extends ControllerFactory<HeroDbDAO> {
    public HeroControllerFactory() {
        super(HeroDbDAO.class);
    }
}
