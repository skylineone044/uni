package hu.u_szeged.utils;

import hu.u_szeged.models.Hero;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

public class FXUtils {
    public static <T> void buildTable(TableView<T> table, List<T> data) {
        ObservableList<T> oData = FXCollections.observableArrayList(data);
        try {
            List<String> fieldNames = Arrays.stream(Hero.class.getDeclaredFields()).map(Field::getName).toList();
            table.getColumns().clear();
            table.getColumns().addAll(
                    fieldNames.stream().map(
                            name -> {
                                TableColumn<T, ?> col = new TableColumn<>(name);
                                col.setCellValueFactory(new PropertyValueFactory<>(name));
                                return col;
                            }).toList());
            table.setItems(oData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
