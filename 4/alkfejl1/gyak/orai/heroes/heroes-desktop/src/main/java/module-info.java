module heroes.desktop {
    requires javafx.fxml;
    requires javafx.controls;
    requires heroes.core;

    opens hu.u_szeged to javafx.fxml;
    opens hu.u_szeged.controllers to javafx.fxml;

    exports hu.u_szeged;
    exports hu.u_szeged.controllers;
}