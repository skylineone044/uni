package hu.u_szeged.configs;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Collectors;

public class AppConfig {
    private static final Properties props = new Properties();

    static {
        try {
            props.load(Thread.currentThread()
                    .getContextClassLoader()
                    .getResourceAsStream("application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getProperty(String key) {
        return props.getProperty(key);
    }

    public static Map<String, Class<?>> getDbTables() {
        return props.keySet()
                .stream().map(key -> ((String) key))
                .filter(key -> key.contains("db.tables."))
                .map(key -> {
                    try {
                        return new Object[]{
                                key.replace("db.tables.", ""),
                                Class.forName(getProperty(key))
                        };
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(data -> (String) data[0], data -> (Class<?>) data[1]));
    }

    public static String getDbUrl() {
        return getProperty("db.url");
    }
}
