package hu.u_szeged.configs;

import com.j256.ormlite.jdbc.JdbcConnectionSource;

import java.io.File;
import java.sql.SQLException;

public class ConnectionManager {
    public static JdbcConnectionSource getConnection() throws SQLException {
        String dbPath = "jdbc:sqlite:" + new File(AppConfig.getDbUrl()).getAbsolutePath();
        return new JdbcConnectionSource(dbPath);
    }
}
