package hu.u_szeged.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@DatabaseTable(tableName = "planets")
@ToString
public class Planet {
    @Getter
    @DatabaseField(id = true)
    private Integer id;
    @Getter
    @Setter
    @NonNull
    @DatabaseField(canBeNull = false)
    private String name;
    @Getter
    @Setter
    @NonNull
    @DatabaseField(canBeNull = false)
    private String occupation;
    @Getter
    @Setter
    @NonNull
    @DatabaseField(canBeNull = false)
    private int population;
    @Getter
    @Setter
    @NonNull
    @DatabaseField(canBeNull = false)
    private double galacticGravityRate;

    public Planet(int id, @NonNull Planet planet) {
        this(id, planet.name, planet.occupation, planet.population, planet.galacticGravityRate);
    }
}
