package hu.u_szeged.utils.db;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.table.TableUtils;
import hu.u_szeged.configs.ConnectionManager;
import hu.u_szeged.models.Planet;

import java.sql.SQLException;

public class DbDefaultRecords {
    public static void addDefaultPlanets() {
        try (JdbcConnectionSource connectionSource = ConnectionManager.getConnection()) {
            try {
                TableUtils.createTableIfNotExists(connectionSource, Planet.class);
                Dao<Planet, Integer> dao = DaoManager.createDao(connectionSource, Planet.class);
                dao.createIfNotExists(new Planet(1, "Hoth", "Rebel Alliance", 89513, 0.97));
                dao.createIfNotExists(new Planet(2, "Endor", "Galactic Empire", 125439, 0.87));
                dao.createIfNotExists(new Planet(3, "Geonosis", "Separatist Alliance", 52219459, 1.13));
                dao.createIfNotExists(new Planet(4, "Coruscant", "Galactic Republic", 84123351, 1.0));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
