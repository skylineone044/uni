package hu.u_szeged.utils.db;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.table.TableUtils;
import hu.u_szeged.configs.AppConfig;
import hu.u_szeged.configs.ConnectionManager;

import java.sql.SQLException;
import java.util.Map;

public class DbInitializer {
    public static void init() {
        try (JdbcConnectionSource connectionSource = ConnectionManager.getConnection()) {
            Map<String, Class<?>> dbTables = AppConfig.getDbTables();

            dbTables.forEach((table, clazz) -> {
                try {
                    Dao<?, ?> dao = DaoManager.createDao(connectionSource, clazz);
                    if (!dao.isTableExists()) {
                        TableUtils.createTableIfNotExists(connectionSource, clazz);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
