module core {
    requires java.sql;
    requires lombok;
    requires ormlite.jdbc;

    opens hu.u_szeged.models to ormlite.jdbc;

    exports hu.u_szeged.configs;
    exports hu.u_szeged.models;
    exports hu.u_szeged.utils.db;
}