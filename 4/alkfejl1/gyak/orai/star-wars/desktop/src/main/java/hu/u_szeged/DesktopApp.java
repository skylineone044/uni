package hu.u_szeged;

import hu.u_szeged.utils.db.DbDefaultRecords;
import hu.u_szeged.utils.db.DbInitializer;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;

public class DesktopApp extends Application {
    private static Stage stage;
    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        DesktopApp.stage = stage;
        scene = new Scene(loadFXML("planetList"));
        stage.setScene(scene);
        stage.show();
    }

    public static void setRoot(String fxml) throws IOException {
        Pane pane = (Pane) loadFXML(fxml);
        setMinSize(pane.getPrefWidth(), pane.getPrefHeight());
        setSize(pane.getPrefWidth(), pane.getPrefHeight());
        scene.setRoot(loadFXML(fxml));
    }

    public static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(DesktopApp.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void setSize(double width, double height) {
        stage.setWidth(width);
        stage.setHeight(height);
    }

    public static void setMinSize(double width, double height) {
        stage.setMinWidth(width);
        stage.setMinHeight(height);
    }

    public static void main(String[] args) {
        DbInitializer.init();
        DbDefaultRecords.addDefaultPlanets();
        launch();
    }
}