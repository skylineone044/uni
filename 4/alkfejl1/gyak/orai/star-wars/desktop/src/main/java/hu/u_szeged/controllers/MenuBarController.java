package hu.u_szeged.controllers;

import hu.u_szeged.DesktopApp;
import javafx.application.Platform;
import javafx.fxml.FXML;

import java.io.IOException;

public class MenuBarController {
    @FXML
    private void onCloseMenuItemAction() {
        Platform.exit();
    }

    @FXML
    private void onAddPlanetMenuItemAction() throws IOException {
        DesktopApp.setRoot("planetAdd");
    }

    @FXML
    private void onListPlanetMenuItemAction() throws IOException {
        DesktopApp.setRoot("planetList");
    }
}
