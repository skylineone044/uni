package hu.u_szeged.controllers;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import hu.u_szeged.configs.ConnectionManager;
import hu.u_szeged.models.Planet;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;

import java.sql.SQLException;

public class PlanetAddController {
    private final Dao<Planet, Integer> dao;

    @FXML
    private TextField nameTextField;
    @FXML
    private ComboBox<String> occupationChoiceBox;
    @FXML
    private Spinner<Integer> populationSpinner;
    @FXML
    private Spinner<Double> galacticGravityRateSpinner;

    public PlanetAddController() throws SQLException {
        dao = DaoManager.createDao(ConnectionManager.getConnection(), Planet.class);
    }

    private Planet getPlanet() {
        String name = nameTextField.getText();
        String occupation = occupationChoiceBox.getSelectionModel().getSelectedItem();
        int population = populationSpinner.getValue();
        double galacticGravityRate = galacticGravityRateSpinner.getValue();
        return new Planet(name, occupation, population, galacticGravityRate);
    }

    private void resetState() {
        nameTextField.clear();
        occupationChoiceBox.getSelectionModel().clearSelection();
        populationSpinner.getValueFactory().setValue(0);
        populationSpinner.getEditor().setText("0");
        galacticGravityRateSpinner.getValueFactory().setValue(1.0);
        galacticGravityRateSpinner.getEditor().setText("1.0");
    }

    @FXML
    private void onSaveAction() {
        try {
            dao.create(getPlanet());
            resetState();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void onResetAction() {
        this.resetState();
    }
}
