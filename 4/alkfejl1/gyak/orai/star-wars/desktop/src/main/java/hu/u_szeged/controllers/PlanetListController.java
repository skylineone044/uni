package hu.u_szeged.controllers;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import hu.u_szeged.configs.ConnectionManager;
import hu.u_szeged.models.Planet;
import hu.u_szeged.utils.fx.TableViewUtils;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;

import java.sql.SQLException;

public class PlanetListController {
    private final Dao<Planet, Integer> dao;

    public PlanetListController() throws SQLException {
        dao = DaoManager.createDao(ConnectionManager.getConnection(), Planet.class);
    }

    @FXML
    private TableView<Planet> planetTableView;

    @FXML
    private void initialize() throws SQLException {
        TableViewUtils.populateTable(planetTableView, dao.queryForAll(), Planet.class);
    }
}
