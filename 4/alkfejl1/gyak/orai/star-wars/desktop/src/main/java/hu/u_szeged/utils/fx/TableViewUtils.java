package hu.u_szeged.utils.fx;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

public class TableViewUtils {
    public static <T> TableView<T> populateTable(TableView<T> table, List<T> data, Class<?> clazz) {
        ObservableList<T> oData = FXCollections.observableArrayList(data);
        List<String> fieldNames = Arrays.stream(clazz.getDeclaredFields())
                .map(Field::getName)
                .toList();

        table.getColumns().clear();
        table.getColumns().addAll(fieldNames.stream().map(colName -> {
            TableColumn<T, ?> col = new TableColumn<>(colName);
            col.setCellValueFactory(new PropertyValueFactory<>(colName));
            return col;
        }).toList());

        table.setItems(oData);
        return table;
    }
}
