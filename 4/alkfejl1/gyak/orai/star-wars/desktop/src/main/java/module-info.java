module hu.u_szeged {
    requires core;
    requires javafx.controls;
    requires javafx.fxml;
    requires ormlite.jdbc;
    requires java.sql;

    opens hu.u_szeged to javafx.fxml;
    opens hu.u_szeged.controllers to javafx.fxml;

    exports hu.u_szeged;
    exports hu.u_szeged.controllers;
}
