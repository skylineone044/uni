<h1>
Star Wars Planetary
</h1>

___

<h2>
Modules
</h2>

- **core:** Contains packages used by _desktop & webapp_.
  Mostly consists of utility methods, configurations and model files.
- **desktop:** JavaFX application containing fxml views and controllers.
- **web:** Java Spring Boot application with jsp as view technology.

___

<h2>
Usage
</h2>
For plugins to work, you will need to install the application.

> mvnw clean install

To execute JavaFX application simply run from preferred IDE or alternatively, run:

> ./mvnw -f desktop/pom.xml javafx:run

To execute Spring Boot application, run:

> ./mvnw -f web/pom.xml spring-boot:run

If modifications are made to _core module_ AND you are using plugins to run the app,
you need to reinstall the app first, for changes to be made visible in the _core module_.

<h2>
Configs
</h2>

Application configurations are found under the standard _${module}/src/main/resources/application.properties_ file.

You may need to modify _db.url_ or the default **_sw.db_** will be created on the fly, relative to you run environment.

___

Any improvement or suggestion made to this project, is appreciated!