package hu.u_szeged;

import hu.u_szeged.utils.db.DbDefaultRecords;
import hu.u_szeged.utils.db.DbInitializer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class WebApp extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(WebApp.class);
    }

    public static void main(String[] args) {
        DbInitializer.init();
        DbDefaultRecords.addDefaultPlanets();
        SpringApplication.run(WebApp.class, args);
    }
}
