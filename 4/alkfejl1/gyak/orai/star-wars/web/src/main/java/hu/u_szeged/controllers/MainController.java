package hu.u_szeged.controllers;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import hu.u_szeged.configs.ConnectionManager;
import hu.u_szeged.models.Planet;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.sql.SQLException;

@Controller
public class MainController {
    private final Dao<Planet, Integer> dao;

    public MainController() throws SQLException {
        dao = DaoManager.createDao(ConnectionManager.getConnection(), Planet.class);
    }

    @GetMapping(value = {"/", "/index"})
    public ModelAndView index() {
        return new ModelAndView("index", "message", "Hello There!");
    }
}
