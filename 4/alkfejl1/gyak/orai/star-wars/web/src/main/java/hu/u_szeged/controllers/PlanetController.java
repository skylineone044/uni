package hu.u_szeged.controllers;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import hu.u_szeged.configs.ConnectionManager;
import hu.u_szeged.models.Planet;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.sql.SQLException;

@Controller
@RequestMapping("/planets")
public class PlanetController {
    private final Dao<Planet, Integer> dao;

    public PlanetController() throws SQLException {
        this.dao = DaoManager.createDao(ConnectionManager.getConnection(), Planet.class);
    }

    @GetMapping(value = {"/add"})
    public ModelAndView viewPlanetAdd() {
        return new ModelAndView("planetAdd", "planet", new Planet());
    }

    @PostMapping(value = "/add")
    private ModelAndView createPlanet(@ModelAttribute("planet") Planet planet) {
        ModelAndView mav = new ModelAndView("planetAdd");
        mav.addObject("planet", new Planet());
        try {
            dao.create(planet);
            mav.addObject("addPlanetSuccess", true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mav;
    }

    @GetMapping(value = {"/list"})
    public String viewPlanetList(Model model) throws SQLException {
        model.addAttribute("planets", dao.queryForAll());
        return "planetList";
    }
}
