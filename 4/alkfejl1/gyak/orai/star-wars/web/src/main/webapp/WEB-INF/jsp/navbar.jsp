<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Star Wars Planets</title>
    <link
      rel="stylesheet"
      type="text/css"
      href="${pageContext.request.contextPath}/css/style.css"
    />
  </head>
  <body>
    <div class="star-wars navbar">
      <button
        onclick="location.href='${pageContext.request.contextPath}/planets/add'"
        class="star-wars button"
      >
        Add Planet
      </button>
      <button
        onclick="location.href='${pageContext.request.contextPath}/planets/list'"
        class="star-wars button"
      >
        List Planet
      </button>
    </div>
  </body>
</html>
