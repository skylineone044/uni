<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="navbar.jsp" %>

<!DOCTYPE html>
<html>
  <head>
    <title>Add Planet</title>
  </head>
  <body>
    <c:url var="addPlanetUrl" value="/planets/add" />

    <div class="star-wars form-container">
      <form:form action="${addPlanetUrl}" method="post" modelAttribute="planet" class="star-wars form">
        <form:label path="name" class="star-wars form-label">Name</form:label>
        <br />
        <form:input type="text" path="name" class="star-wars form-input" />
        <br /><br />
        <form:label path="occupation" class="star-wars form-label">Occupation</form:label>
        <br />
        <form:select path="occupation" class="star-wars form-input">
          <form:option value="Galactic Republic">Galactic Republic</form:option>
          <form:option value="Separatist Alliance">Separatist Alliance</form:option>
          <form:option value="Rebel Alliance">Rebel Alliance</form:option>
          <form:option value="Galactic Empire">Galactic Empire</form:option>
          <form:option value="Resistance">Resistance</form:option>
          <form:option value="First Order">First Order</form:option>
        </form:select>
        <br /><br />
        <form:label path="population" class="star-wars form-label">Population</form:label>
        <br />
        <form:input type="number" step="1" path="population" class="star-wars form-input" />
        <br /><br />
        <form:label path="galacticGravityRate" class="star-wars form-label">Galactic Gravity Rate</form:label>
        <br />
        <form:input type="number" step="0.001" path="galacticGravityRate" class="star-wars form-input" />
        <br /><br />
        <input type="submit" value="Add Planet" />
      </form:form>
    </div>

    <c:if test="${addPlanetSuccess}">
      <div class="star-wars message-container">Successfully Added Planet</div>
    </c:if>
  </body>
</html>
