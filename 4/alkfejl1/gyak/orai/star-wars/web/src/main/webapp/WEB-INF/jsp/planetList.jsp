<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@include file="navbar.jsp" %>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Planet List</title>
    <link
      rel="stylesheet"
      type="text/css"
      href="${pageContext.request.contextPath}/css/style.css"
    />
  </head>
  <body>
    <div class="star-wars table-container">
      <table class="star-wars table">
        <tr>
          <th>Id</th>
          <th>Name</th>
          <th>Occupation</th>
          <th>Population</th>
          <th>Galactic Gravity Rate</th>
        </tr>
        <c:forEach items="${planets}" var="planet">
          <tr>
            <td>${planet.id}</td>
            <td>${planet.name}</td>
            <td>${planet.occupation}</td>
            <td>${planet.population}</td>
            <td>${planet.galacticGravityRate}</td>
          </tr>
        </c:forEach>
      </table>
    </div>
  </body>
</html>
