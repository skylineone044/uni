package hu.alkfejl;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/KosarServlet")
public class KosarServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF8");
        PrintWriter out = response.getWriter();
        String parNev = request.getParameter("nev");
        String parAru = request.getParameter("aru");
        String parDb = request.getParameter("db");

        if (parNev == null || parNev.isEmpty() || parAru == null || parAru.isEmpty() || parDb == null || parDb.isEmpty()) {
            response.sendRedirect("index.jsp");
        }

        Cookie nk = new Cookie("nev", parNev);
        Cookie ak = new Cookie("aru", parAru);
        Cookie dbk = new Cookie("db", parDb);
        response.addCookie(nk);
        response.addCookie(ak);
        response.addCookie(dbk);


        out.println(
                "<html>\n" +
                        "<head><title>Bevásárló lista</title></head>\n" +
                        "<body>\n" +
                        parNev + " bevásárló listája" +
                        "<ul>\n" +
                        "<li>" + parDb + " darab " + parAru + "\n" +
                        "</ul>\n" +
                        "<b>Összesen: " + parDb + "</b><br/>\n" +
                        "<form action = \"VasarolServlet\" method = \"GET\">\n" +
                        "    <table>\n" +
                        "        <tr>\n" +
                        "            <td><input type = \"submit\" name = \"OK\"/></td>\n" +
                        "        </tr>\n" +
                        "    </table>\n" +
                        "</form>\n" +
                        "</body>" +
                        "</html>"
        );
    }

}