package hu.alkfejl;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/VasarolServlet")
public class VasarolServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF8");
        PrintWriter out = response.getWriter();

        String nev = "--";
        String aru = "--";
        String db = "--";

        Cookie[] c = request.getCookies();
        for (int i = 0; i < c.length; i++) {
            if (c[i].getName().equals("nev")) {
                nev = c[i].getValue();
            } else if (c[i].getName().equals("aru")) {
                aru = c[i].getValue();
            } else if (c[i].getName().startsWith("db")) {
                db = c[i].getValue();
            }
        }

        out.println(
                "<html>\n" +
                        "<head><title>Sikeres vasarlas</title></head>\n" +
                        "<body>\n" +
                        nev + " vett " + db + " darab " + aru + "\n" +
                        "</body>" +
                        "</html>"
        );
    }

}