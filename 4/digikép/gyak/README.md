# please use a venv, with python 3.9.9

## OpenCV has some builtin QT things

It may or may not conflict with the system's QT verson
If it does indeed conflict, and e.g. cv2.imshow does not create a window and
show the image you need to build OpenCV from source
you can use pip for this, use this command:

```shell
pip install opencv-contrib-python==4.5.5.62 --no-binary :all:
```

This directory should also be in a python venv with python 3.9.9 and opencv 4.5.5.x
Also: There's a pyocv-requirements.txt, to install it all use:

```shell
pip install -r pyocv-requirements.txt
```
