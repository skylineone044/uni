import cv2
import numpy as np

IMG = np.ndarray((400, 640, 3), np.uint8)
IMG.fill(255)
PREVIEW = IMG.copy()

LEFT_MB_LAST_ACTION = cv2.EVENT_LBUTTONUP
CURRENT_COLOR = (0, 0, 255)
CURRENT_RADIUS = 10
CURRENT_SHAPE = "circle"

LAST_MOUSE_POS = (0, 0)

FILENAME = "drawing.png"
ALLOW_CLICK_TO_DRAW = True


def draw_shape(surface, center, radius, color):
    global CURRENT_SHAPE

    if CURRENT_SHAPE == "circle":
        cv2.circle(surface, center, radius, color, -1)
    elif CURRENT_SHAPE == "square":
        top_left_corner = center[0] - radius, center[1] - radius
        bottom_right_corner = center[0] + radius, center[1] + radius
        cv2.rectangle(surface, top_left_corner, bottom_right_corner, color, cv2.FILLED)


def mouse_event(event, x, y, flags, param):
    global LEFT_MB_LAST_ACTION
    global IMG
    global PREVIEW
    global LAST_MOUSE_POS
    LAST_MOUSE_POS = (x, y)
    # print("drawing")
    draw_shape(PREVIEW, (x, y), CURRENT_RADIUS, CURRENT_COLOR)

    if event in (cv2.EVENT_LBUTTONDOWN, cv2.EVENT_LBUTTONUP):
        LEFT_MB_LAST_ACTION = event

    if (event == cv2.EVENT_MOUSEMOVE or ALLOW_CLICK_TO_DRAW) and \
            LEFT_MB_LAST_ACTION == cv2.EVENT_LBUTTONDOWN:
        draw_shape(IMG, (x, y), CURRENT_RADIUS, CURRENT_COLOR)

    cv2.imshow('image', PREVIEW)
    PREVIEW = IMG.copy()


cv2.imshow('image', PREVIEW)
cv2.setMouseCallback('image', mouse_event)

while True:
    key = cv2.waitKey(0)
    #print(f"{key=}")
    if key in (ord('q'), 27, -1):  # 27 is the ESC key, -1 is when the window is closed with the X
        cv2.destroyAllWindows()
        break

    if key == ord('t'):
        IMG.fill(255)
        # print("crealed the screen")
    if key == ord('s'):
        cv2.imwrite(FILENAME, IMG)
        print(f"saved image to {FILENAME}")
    if key == ord('l'):
        new_image = cv2.imread(FILENAME, flags=cv2.IMREAD_COLOR)
        if new_image is None:
            print(f"could not load {FILENAME}")
        else:
            IMG = new_image
            print(f"loaded {FILENAME}")

    if key in (ord('+'), 171): # 171 is numpad + key
        CURRENT_RADIUS = min(max(5, CURRENT_RADIUS + 5), 100)
        # print(f"+{CURRENT_RADIUS=}")
    if key in (ord('-'), 173): # 173 is numbad - key
        CURRENT_RADIUS = min(max(5, CURRENT_RADIUS - 5), 100)
        # print(f"-{CURRENT_RADIUS=}")

    if key == ord('r'):
        CURRENT_COLOR = (0, 0, 255)
    if key == ord('g'):
        CURRENT_COLOR = (0, 255, 0)
    if key == ord('b'):
        CURRENT_COLOR = (255, 0, 0)
    if key == ord('k'):
        CURRENT_COLOR = (0, 0, 0)
    if key == ord('w'):
        CURRENT_COLOR = (255, 255, 255)

    if key == ord('c'):
        CURRENT_SHAPE = "circle"
    if key == ord('v'):
        CURRENT_SHAPE = "square"

    PREVIEW = IMG.copy()
    draw_shape(PREVIEW, LAST_MOUSE_POS, CURRENT_RADIUS, CURRENT_COLOR)
    cv2.imshow('image', PREVIEW)
    PREVIEW = IMG.copy()
