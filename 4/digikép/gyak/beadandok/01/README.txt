Az alapból megadottak felett, a "c" és "v" gombokkal lehet kör és négyzet
rajzolása között váltani

Az "l" billentyű beolvassa a "drawing.png" nevű képet

A forráskódban van opció a pontok rajzolásának ki és bekacsolására:
    ha az "ALLOW_CLICK_TO_DRAW" értéke True, akkor rajzoláskor csak a bal
    egérgomb lenyomása elég a rajzoláshoz, hogy lehessen gyorsan pontokat
    rajzolni; amennyiben viszont a "ALLOW_CLICK_TO_DRAW" értéke False, akkor
    a feladatleíráshoz hűbben viselkesik az alkalmazás, és csak akkor kezd el
    rajzolni, ha az egér elmozdult, és le van nyomva a bal egérgomb
