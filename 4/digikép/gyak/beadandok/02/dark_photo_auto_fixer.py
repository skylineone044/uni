# a file elejen vannak globalis valtozok, azzal lehet allitani a prametereit a kulonbozo javitasi modszereknek, de a
# feladatnak megfelelve, ezek mar elfogadhato eredmenyt biztosito ertekre lettek allitva, a felhasznalonek ezeket nem
# kell hogy atallitsa
#
# a 3 fele javitasi modszer, a sajat fuggvenyeibe lettek delegalva, hogy atlathatobb legyen
# a feladat fo resze a lab_way() fuggvenyben van megvalositva
#
# az oran mondta, hogy a lepesekhez kulon kulon is kell egy egy kepet generalni szoval
# sok kep lesz megnyitva egymas tetejere, lehet arrabb kell nehanyat tenni, hogy mind latszodjon

import cv2
import numpy as np

BRIGHTNESS = 260
CONTRAST = 120
GAMMA = 0.33

IMG = cv2.imread("street_dark.jpg", cv2.IMREAD_COLOR)


# cv2.imshow("img", IMG)

def lab_way(img):
    # konverzio LAB szinterbe
    img_lab = cv2.cvtColor(img, cv2.COLOR_BGR2Lab)
    img_ch_l, img_ch_a, img_ch_b = cv2.split(img_lab)

    cv2.imshow("lab way: 1: lab szinter felbontva: L csatorna", img_ch_l)
    cv2.imshow("lab way: 1: lab szinter felbontva: a csatorna", img_ch_a)
    cv2.imshow("lab way: 1: lab szinter felbontva: b csatorna", img_ch_b)
    cv2.imwrite("street_dark_labway_inprogress_1_lab_szinter_felbontva_L_csatorna.jpg", img_ch_l)
    cv2.imwrite("street_dark_labway_inprogress_1_lab_szinter_felbontva_a_csatorna.jpg", img_ch_a)
    cv2.imwrite("street_dark_labway_inprogress_1_lab_szinter_felbontva_b_csatorna.jpg", img_ch_b)

    # hisztogramkiegyenlites az L csatornan
    img_ch_l_historgram_eq = cv2.equalizeHist(img_ch_l)

    cv2.imshow("lab way: 2: hisztogram kiegyenlitve", img_ch_l_historgram_eq)
    cv2.imwrite("street_dark_labway_inprogress_2_hisztogram_kiegyenlitve.jpg", img_ch_l_historgram_eq)

    # kontraszt szethuzas a hisztogramkiegyenlitett kepmatrixon
    threshold_low = 150
    threshold_high = 255
    img_ch_l_historgram_eq[img_ch_l_historgram_eq > threshold_high] = threshold_high
    img_ch_l_historgram_eq[img_ch_l_historgram_eq < threshold_low] = threshold_low
    img_ch_l_heq_hstr = cv2.normalize(img_ch_l_historgram_eq, None, 0, 255, norm_type=cv2.NORM_MINMAX,
                                      dtype=cv2.CV_8UC1)

    cv2.imshow("lab way: 3: kontraszt szethuzva", img_ch_l_historgram_eq)
    cv2.imwrite("street_dark_labway_inprogress_3_kontraszt_szethuzva.jpg", img_ch_l_heq_hstr)

    # a es b csatornak felszorzasa 1.2-vel
    img_ch_a = np.uint8(np.clip(cv2.multiply(img_ch_a, 1.2, dtype=cv2.CV_32S), 0, 255))
    img_ch_b = np.uint8(np.clip(cv2.multiply(img_ch_b, 1.2, dtype=cv2.CV_32S), 0, 255))

    cv2.imshow("lab way: 4: a szorozva 1.2-vel", img_ch_a)
    cv2.imshow("lab way: 4: b szorozva 1.2-vel", img_ch_b)
    cv2.imwrite("street_dark_labway_inprogress_4_a_szorozva_1_2_vel.jpg", img_ch_a)
    cv2.imwrite("street_dark_labway_inprogress_4_b_szorozva_1_2_vel.jpg", img_ch_b)

    # csatornak osszefuzese, es BGR formatumba valo visszaalakitasa
    img_merged = cv2.cvtColor(cv2.merge((img_ch_l_heq_hstr, img_ch_a, img_ch_b)), cv2.COLOR_Lab2BGR)
    cv2.imshow("lab way: 5: osszefuzott", img_merged)
    cv2.imwrite("street_dark_labway_javitott.jpg", img_merged)


def brightness_contrast(img):  # loptama a jegyzetbol, hogy ossze birjam hasonlitani
    x = np.arange(0, 256, 1)
    factor = (259 * (CONTRAST + 255)) / (255 * (259 - CONTRAST))
    lut = np.uint8(np.clip(BRIGHTNESS + factor * (np.float32(x) - 128.0) + 128, 0, 255))
    result_image = cv2.LUT(img, lut)

    cv2.imshow("brightness-constrast way", result_image)
    cv2.imwrite("street_dark_javitott_brightness_contrast.jpg", result_image)


def gamma(img):
    def create_gamma_lut(gamma):  # loptama a jegyzetbol, hogy ossze birjam hasonlitani
        """Gamma paraméter értéknek megfelelő 256 elemű keresőtábla generálása.
        A hatványozás miatt először [0, 1] tartományra kell konvertálni, majd utána vissza [0, 255] közé.
        """
        lut = np.arange(0, 256, 1, np.float32)
        lut = lut / 255.0
        lut = lut ** gamma
        lut = np.uint8(lut * 255.0)
        return lut

    result_image = cv2.LUT(img, create_gamma_lut(GAMMA), None)
    cv2.imshow('gamma way', result_image)
    cv2.imwrite("street_dark_javitott_gamma.jpg", result_image)


lab_way(IMG)
brightness_contrast(IMG)
gamma(IMG)
cv2.waitKey(0)
