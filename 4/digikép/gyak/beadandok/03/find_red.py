import cv2
import numpy as np

IMG = cv2.imread("car_numberplate_rs.jpg", cv2.IMREAD_COLOR)
cv2.imshow("eredeti", IMG)

def morfologiai_szures(img):
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
    print(f"{kernel=}")
    dilated_1 = cv2.dilate(img, kernel, iterations=1)
    eroded_1 = cv2.erode(dilated_1, kernel, iterations=1)
    eroded_2 = cv2.erode(eroded_1, kernel, iterations=1)
    dilated_2 = cv2.dilate(eroded_2, kernel, iterations=1)
    return dilated_2


# Végezzen dilatáció, erózió, erózió, dilatáció lépéssorozatot a színes bemeneti képen 7x7 méretű, téglalap alakú strukturáló elemmel!
morf_szurve = morfologiai_szures(IMG)
cv2.imshow("morfologiai szurve", morf_szurve)

# Hajtson végre Gauss simítást a képen 5x5 méretben, 4-es szórással!
gauss_simitva = cv2.GaussianBlur(morf_szurve, (5, 5), sigmaX=4.0, sigmaY=4.0)
cv2.imshow("gauss simitva", gauss_simitva)

# Bontsa fel a képet különálló csatornákra! Állítson elő egy olyan képet, amely a zöld és a kék csatornák maximum értékeit tartalmazza!
B, G, R = cv2.split(gauss_simitva)
GB_max = np.maximum(G, B)
cv2.imshow("zold es kek maximuma", GB_max)

# Vonja ki a vörös csatorna képből az előző zöld-kék maximum eredményt! Figyeljen arra, hogy az eredmény típusa megfelelő legyen (negatív egész értékek is előfordulhatnak)!
sub = cv2.subtract(R, GB_max)
cv2.imshow("kivonva", sub)

# Küszöbölje a különbség képet 50 értéknél! (Ez azt jelenti, hogy azokat a képpontokat tartjuk meg, ahol a vörös legalább 50 értékkel magasabb a zöld és kék maximumánál.)
thresh = sub.copy()
_, thresh = cv2.threshold(thresh, 50, 255, cv2.THRESH_BINARY)
cv2.imshow("kuszobolve", thresh)

# Nyerje ki a szegmentált területek határvonalát! A körvonalat vetítse vörös színnel az eredeti kép szürkeárnyalatos változatára!
thresh_gauss_simitva = cv2.GaussianBlur(thresh, (5, 5), sigmaX=4.0, sigmaY=4.0)
edges = cv2.Canny(thresh_gauss_simitva, 100, 200, None, 5, True)
cv2.imshow("hatarvonal", edges)

grayscale = cv2.cvtColor(IMG, cv2.COLOR_BGR2GRAY)
grayscale_with_red_outline = cv2.cvtColor(grayscale, cv2.COLOR_GRAY2BGR)
grayscale_with_red_outline[edges == 255] = (0, 0, 255)
cv2.imshow("szurkearnyalatos, piros korvonallal", grayscale_with_red_outline)

cv2.imwrite("result.png", grayscale_with_red_outline)
cv2.waitKey(0)