import math
import numpy as np
import cv2


# Ez a segítő osztály az alábbi címről származik:
# https://stackoverflow.com/questions/45531074/how-to-merge-lines-after-houghlinesp
# pontosabban: https://stackoverflow.com/a/70318827

# célja a cv2.HoughLinesP eredményeként kapott egymáshoz közel lévő és közel azonos irányú, vagy egymáson fekvő
# egyenesek öszevonása

class HoughBundler:
    def __init__(self, min_distance=5, min_angle=2):
        self.min_distance = min_distance
        self.min_angle = min_angle

    def get_orientation(self, line):
        """kiszémolja a vonal meredekségét / "irányát"
        https://en.wikipedia.org/wiki/Atan2
        """
        orientation = math.atan2(abs((line[3] - line[1])), abs((line[2] - line[0])))
        return math.degrees(orientation)

    def check_is_line_different(self, line_1, groups, min_distance_to_merge, min_angle_to_merge):
        """Kiszámolja, hogy a vonalak a csoporokban eléggé közel vannak-e egymáshoz, hogy egy vonallá össze legyenek vonva
        """
        for group in groups:
            for line_2 in group:
                if self.get_distance(line_2, line_1) < min_distance_to_merge:
                    orientation_1 = self.get_orientation(line_1)
                    orientation_2 = self.get_orientation(line_2)
                    if abs(orientation_1 - orientation_2) < min_angle_to_merge:
                        group.append(line_1)
                        return False
        return True

    def distance_point_to_line(self, point, line):
        """kiszámolja egy pont és egy vonal távolságát
        http://local.wasp.uwa.edu.au/~pbourke/geometry/pointline/source.vba
        """
        px, py = point
        x1, y1, x2, y2 = line

        def line_magnitude(x1, y1, x2, y2):
            """Kiszámolja a vonal hosszát"""
            line_magnitude = math.sqrt(math.pow((x2 - x1), 2) + math.pow((y2 - y1), 2))
            return line_magnitude

        lmag = line_magnitude(x1, y1, x2, y2)
        if lmag < 0.00000001:
            distance_point_to_line = 9999
            return distance_point_to_line

        u1 = (((px - x1) * (x2 - x1)) + ((py - y1) * (y2 - y1)))
        u = u1 / (lmag * lmag)

        if (u < 0.00001) or (u > 1):
            # // closest point does not fall within the line segment, take the shorter distance
            # // to an endpoint
            ix = line_magnitude(px, py, x1, y1)
            iy = line_magnitude(px, py, x2, y2)
            if ix > iy:
                distance_point_to_line = iy
            else:
                distance_point_to_line = ix
        else:
            # Intersecting point is on the line, use the formula
            ix = x1 + u * (x2 - x1)
            iy = y1 + u * (y2 - y1)
            distance_point_to_line = line_magnitude(px, py, ix, iy)

        return distance_point_to_line

    def get_distance(self, a_line, b_line):
        """Megadja két vonal között az összes lehetséges vonal - pont távolság közül a legkisebbet
        """
        dist1 = self.distance_point_to_line(a_line[:2], b_line)
        dist2 = self.distance_point_to_line(a_line[2:], b_line)
        dist3 = self.distance_point_to_line(b_line[:2], a_line)
        dist4 = self.distance_point_to_line(b_line[2:], a_line)

        return min(dist1, dist2, dist3, dist4)

    def merge_lines_into_groups(self, lines):
        """Csoportosítja az egymáshoz közel lévő, hasonló vonalakat"""
        groups = []  # all lines groups are here
        # first line will create new group every time
        groups.append([lines[0]])
        # if line is different from existing gropus, create a new group
        for line_new in lines[1:]:
            if self.check_is_line_different(line_new, groups, self.min_distance, self.min_angle):
                groups.append([line_new])

        return groups

    def merge_line_segments(self, lines):
        """Egy vonal csoportból állít elő egy vonalat: a koordináták rendezése utáni legszélső értékekből"""
        orientation = self.get_orientation(lines[0])

        if (len(lines) == 1):
            return np.block([[lines[0][:2], lines[0][2:]]])

        points = []
        for line in lines:
            points.append(line[:2])
            points.append(line[2:])
        if 45 < orientation <= 90:
            # sort by y
            points = sorted(points, key=lambda point: point[1])
        else:
            # sort by x
            points = sorted(points, key=lambda point: point[0])

        return np.block([[points[0], points[-1]]])

    def process_lines(self, lines):
        """Fő függvény a cv2.HoughLinesP() kimenetének összevonására
        OpenCV 3 -hoz
        lines -- cv.HoughLinesP() kimenet
        """
        lines_horizontal = []
        lines_vertical = []

        for line_i in [l[0] for l in lines]:
            orientation = self.get_orientation(line_i)
            # if vertical
            if 45 < orientation <= 90:
                lines_vertical.append(line_i)
            else:
                lines_horizontal.append(line_i)

        lines_vertical = sorted(lines_vertical, key=lambda line: line[1])
        lines_horizontal = sorted(lines_horizontal, key=lambda line: line[0])
        merged_lines_all = []

        # for each cluster in vertical and horizantal lines leave only one line
        for i in [lines_horizontal, lines_vertical]:
            if len(i) > 0:
                groups = self.merge_lines_into_groups(i)
                merged_lines = []
                for group in groups:
                    merged_lines.append(self.merge_line_segments(group))
                merged_lines_all.extend(merged_lines)

        return np.asarray(merged_lines_all)


def apply_mask(matrix, mask, fill_value):
    masked = np.ma.array(matrix, mask=mask, fill_value=fill_value)
    return masked.filled()


def apply_threshold(matrix, low_value, high_value):
    low_mask = matrix < low_value
    matrix = apply_mask(matrix, low_mask, low_value)

    high_mask = matrix > high_value
    matrix = apply_mask(matrix, high_mask, high_value)

    return matrix


# ez a peldatarbol van: 05_04
def simple_cb_hist(img, percent):
    """adaptiv hisztogramszethuzas"""
    half_percent = percent / 200.0

    channel = img

    hist_gray = cv2.calcHist([channel], [0], None, [256], [0, 256])
    height, width = channel.shape
    vec_size = width * height

    sum = 0
    idx = 0
    sum_stop = vec_size * half_percent
    # print(sum_stop)
    while sum <= sum_stop:
        sum += hist_gray[idx]
        idx += 1

    low_val = idx - 1
    # print("Lowval: ", low_val)

    sum_stop = vec_size * (1.0 - half_percent)
    # print(sum_stop)
    while sum <= sum_stop:
        sum += hist_gray[idx]
        idx += 1

    high_val = idx - 1
    # print("Highval: ", high_val)
    # saturate below the low percentile and above the high percentile
    thresholded = apply_threshold(channel, low_val, high_val)
    # scale the channel
    normalized = cv2.normalize(thresholded, thresholded.copy(), 0, 255, cv2.NORM_MINMAX)

    return normalized


def add_additive_noise(img, amount):
    noise = np.zeros(img.shape[:2], np.int16)
    cv2.randn(noise, 0.0, amount)
    h, s, v = cv2.split(cv2.cvtColor(img, cv2.COLOR_BGR2HSV))
    v_noise = cv2.add(v, noise, dtype=cv2.CV_8UC1)
    return cv2.cvtColor(cv2.merge((h, s, v_noise)), cv2.COLOR_HSV2BGR)


def add_salt_pepper_noise(img, amount):
    def add_point_noise(img_in, percentage, value):
        noise = np.copy(img_in)
        n = int(img_in.shape[0] * img_in.shape[1] * percentage)
        print(n)

        for k in range(1, n):
            i = np.random.randint(0, img_in.shape[1])
            j = np.random.randint(0, img_in.shape[0])

            if img_in.ndim == 2:
                noise[j, i] = value

            if img_in.ndim == 3:
                noise[j, i] = [value, value, value]

        return noise

    n = add_point_noise(img, (amount // 2) / 100, 255)  # Só
    n2 = add_point_noise(n, (amount // 2) / 100, 0)  # Bors

    return n2
