# A beolvasott kepet az alabbi IMG valtozo beallitasaval lehet kivalasztani
# A palcikak vegso szama a terminalba kerul kiiratasra
# komplex alakzatok felismerese nincsen megvalositva
# van tobb elokeszito mod is, az alapertelmezett amikor a szint, es az intenzitast is figyelembe veszem
# az openCV kezelofeluleten levo "Target HUE" csuszkaval lehet kivalsztani milyen szinu palcikakat akarunk keresni
# amennyiben a csuszka erteke 0, akkor kizarolag intenzitas alapu kereses fog tortenni

import math

import cv2
import numpy as np
import helper

# IMG = cv2.imread("tesztkepek/palcika1.jpg", cv2.IMREAD_COLOR)
IMG = cv2.imread("tesztkepek/palcika2.jpg", cv2.IMREAD_COLOR)
# IMG = cv2.imread("tesztkepek/xl_square-toothpicks.jpg", cv2.IMREAD_COLOR)
# IMG = cv2.imread("tesztkepek/xl_round-toothpicks.jpg", cv2.IMREAD_COLOR)
# IMG = cv2.imread("tesztkepek/colorful-ice-cream-sticks-isolated.jpg", cv2.IMREAD_COLOR)

# cv2.imshow("original image", IMG)

SHOW_INTERMEDIATE_IMAGES = False

HIGHLIGHT_COLOR = (255, 255, 255)
HIGHLIGHT_WIDTH = 2
HIGHLIGHT_DOT_SIZE = 6

MAX_SAT = 75
MIN_VAL = 125

HUE_SEARCH = True
HUE_SIMILARITY_TH = 50
TARGET_HUE = 26
HSV_SIMILARITY_TH_UPPER = (TARGET_HUE + HUE_SIMILARITY_TH // 2, 255, 255)
HSV_SIMILARITY_TH_LOWER = (TARGET_HUE - HUE_SIMILARITY_TH // 2, 5, 5)

HIST_STRETCH_WIDTH = 30

BRIGHTNESS = -40
CONTRAST = 40

TARGET_LINE_LENGTH = 0
LINE_LENGTH_TOLERANCE = 30

cv2.namedWindow("Lines")


def morfologiai_szures(img):
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
    eroded_1 = cv2.erode(img, kernel, iterations=1)
    dilated_1 = cv2.dilate(eroded_1, kernel, iterations=1)
    dilated_2 = cv2.dilate(dilated_1, kernel, iterations=1)
    eroded_2 = cv2.erode(dilated_2, kernel, iterations=1)
    return eroded_2


def brightness_contrast(img):
    """az oran tanult fenyerosseg-kontraszt korrekciot hajtja vegre"""
    x = np.arange(0, 256, 1)
    factor = (259 * (CONTRAST + 255)) / (255 * (259 - CONTRAST))
    lut = np.uint8(np.clip(BRIGHTNESS + factor * (np.float32(x) - 128.0) + 128, 0, 255))
    result_image = cv2.LUT(img, lut)
    # cv2.imshow("br co", result_image)
    return result_image


def hist_stretch(img, th_lower, th_upper):
    """az oran tanult hisztogram szethuzast hajtja vegre"""
    img[img > th_upper] = min(255, th_upper)
    img[img < th_lower] = max(0, th_lower)
    hist_stretch = cv2.normalize(img, None, 0, 255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8UC1)
    return hist_stretch


def thres(image, threshold):
    """szetvalasztja a kep ertekeit a threshold korul, ami kisebb azt 0-ra, ami nagybb azt 255-re allitja"""
    img = image.copy()
    img[img > threshold] = 255
    img[img < threshold] = 0
    return img


def preprocess_brightness(img):
    """a kep vilagossagan alapulo elofeldolgozas, nem tud szuneket kivalogatni"""
    print(f"preprocessing imgae...")
    blurred = cv2.GaussianBlur(img, (7, 7), sigmaX=0, sigmaY=0)
    blurred = cv2.GaussianBlur(blurred, (7, 7), sigmaX=0, sigmaY=0)
    br_co = brightness_contrast(blurred)
    h, s, v = cv2.split(cv2.cvtColor(br_co, cv2.COLOR_BGR2HSV))

    avg_brightness = v.mean()

    bc = brightness_contrast(v)
    if SHOW_INTERMEDIATE_IMAGES:
        cv2.imshow("preprocessed brightness contrast", bc)

    thr = thres(bc, avg_brightness)

    dark_background = np.sum(thr == 0) > np.sum(thr == 255)
    if not dark_background:
        thr = 255 - thr

    if SHOW_INTERMEDIATE_IMAGES:
        cv2.imshow("preprocessed image: brightness", thr)

    return thr


def preprocess_hue(image):
    """a keppontok szinen alapulo elofeldolgozas"""
    print(f"preprocessing imgae...")
    img = image.copy()
    blurred = cv2.GaussianBlur(img, (7, 7), sigmaX=0, sigmaY=0)
    # blurred = cv2.GaussianBlur(blurred, (7, 7), sigmaX=0, sigmaY=0)
    hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)

    mask = cv2.inRange(hsv, HSV_SIMILARITY_TH_LOWER, HSV_SIMILARITY_TH_UPPER)

    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))

    eroded_1 = np.uint8(cv2.erode(mask, kernel, iterations=1))
    blurred1 = cv2.GaussianBlur(eroded_1, (7, 7), sigmaX=0, sigmaY=0)

    szurve = morfologiai_szures(blurred1)
    thresh = np.zeros((img.shape[0], img.shape[1], 1), dtype=np.uint8)
    thresh[szurve >= MIN_VAL] = 255

    if SHOW_INTERMEDIATE_IMAGES:
        cv2.imshow("preprocessed image: hue", thresh)
    # cv2.imshow("preprocessed mask", mask)

    return thresh


def preprocess_combined_hue_brightness(image):
    """keppontok szinet, egy vilagossagat is figyelembe vevo elofeldolgozas, kepes szinekre szurni"""
    print(f"preprocessing imgae...")
    img = image.copy()
    hue_mask = preprocess_hue(img)

    blurred = cv2.GaussianBlur(img, (7, 7), sigmaX=0, sigmaY=0)
    blurred = cv2.GaussianBlur(blurred, (7, 7), sigmaX=0, sigmaY=0)
    hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)

    avg_brightness = v.mean()

    bc = brightness_contrast(v)
    # cv2.imshow("preprocessed image: bc", bc)
    # stre = hist_stretch(bc, avg_brightness - HIST_STRETCH_WIDTH, avg_brightness + HIST_STRETCH_WIDTH)
    stre = helper.simple_cb_hist(bc, HIST_STRETCH_WIDTH)
    stre = thres(stre, avg_brightness)
    if SHOW_INTERMEDIATE_IMAGES:
        cv2.imshow("preprocessed image: stre", stre)

    stre = morfologiai_szures(stre)
    stre = cv2.GaussianBlur(stre, (7, 7), sigmaX=0, sigmaY=0)
    stre = cv2.GaussianBlur(stre, (7, 7), sigmaX=0, sigmaY=0)
    stre = thres(stre, avg_brightness)

    if SHOW_INTERMEDIATE_IMAGES:
        cv2.imshow("preprocessed image: stre", stre)

    dark_background = np.sum(stre == 0) > np.sum(stre == 255)
    if not dark_background:
        stre = 255 - stre

    if np.sum(hue_mask == 0) < np.sum(hue_mask == 255):
        # too much area is selected, it is probably not made of stick, but of the bg
        hue_mask[hue_mask == 255] = 0

    if SHOW_INTERMEDIATE_IMAGES:
        cv2.imshow("preprocessed image: stre", stre)

    mixed = cv2.bitwise_and(stre, hue_mask)
    mixed = thres(mixed, avg_brightness)

    morf = morfologiai_szures(mixed)

    if SHOW_INTERMEDIATE_IMAGES:
        cv2.imshow("preprocessed image: hue + brightness", morf)
    # cv2.imshow("preprocessed image: stre", stre)
    return mixed


def get_skeleton(img):
    """a palcikak kozepvonalat megtaltalja, egy egy pizel valtagon kijeloli"""
    print(f"getting skeleton...")
    sk = cv2.ximgproc.thinning(img, thinningType=cv2.ximgproc.THINNING_ZHANGSUEN)

    if SHOW_INTERMEDIATE_IMAGES:
        cv2.imshow("skeleton", sk)
    return sk


def get_hough_lines(img):
    """az egy pixel vastak kozepvonalakbol megtalalja az egyeneseket, es pontparokkent visszaadja"""
    print(f"getting hough lines...")

    min_line_len = min(img.shape[0:2]) * 1 / 6
    max_gap = min(img.shape[0:2]) * 1 / 13
    th = int(min(img.shape[0:2]) * 1 / 20)

    # detect lines
    rho = 1  # distance resolution in pixels of the Hough grid
    theta = np.pi / 180  # angular resolution in radians of the Hough grid
    # threshold = 20  # minimum number of votes (intersections in Hough grid cell)
    threshold = th  # minimum number of votes (intersections in Hough grid cell)
    # min_line_length = 50  # minimum number of pixels making up a line
    min_line_length = min_line_len  # minimum number of pixels making up a line
    # max_line_gap = 60  # maximum gap in pixels between connectable line segments
    max_line_gap = max_gap  # maximum gap in pixels between connectable line segments

    # Run Hough on edge detected image
    # Output "lines" is an array containing endpoints of detected line segments
    lines = cv2.HoughLinesP(img, rho, theta, threshold, np.array([]),
                            min_line_length, max_line_gap)
    if lines is None:
        return []
    else:
        return lines


def merge_close_lines(lines):
    """az egymashoz kozel levo es hasonlo iranyu vonalakat osszevonja a helper.HoughBundler segitsegevel"""
    print(f"merging lines...")

    dist = min(IMG.shape[0:2]) // 75
    # print(f"{IMG.shape[0:2]=} {dist=}")
    bundler = helper.HoughBundler(min_distance=dist, min_angle=6)
    lines_bundled = bundler.process_lines(lines)

    return lines_bundled


def line_len(line):
    """egy vonal hossza"""
    # [[x1, y1, x2, y2]]
    # print(f"{line=}")
    return math.sqrt((line[0][2] - line[0][0]) ** 2 + (line[0][3] - line[0][1]) ** 2)


def filter_lines(lines, target_length):
    """kivalogatja azokat a vonalakat, amelyek a megfleleo hosszusagi tatomanyba tartoznak"""
    filtered = []

    for line in lines:
        if abs(line_len(line) - target_length) <= LINE_LENGTH_TOLERANCE:
            filtered.append(line)

    return filtered


def get_lines(img, overlay_base):
    """megkeresi, es felfesti a vonalakat a palcikakra"""
    print(f"getting lines...")
    h_lines = get_hough_lines(get_skeleton(img))
    # print(f"{h_lines=}")

    lines = merge_close_lines(h_lines)
    if TARGET_LINE_LENGTH > 0:
        lines = filter_lines(lines, TARGET_LINE_LENGTH)

    # eloszor egy mask kepre felrrajzoljuk, hogy a vegleges kepen hol kell kiszinezni
    line_image = np.zeros((img.shape[0], img.shape[1]), dtype=np.uint8)
    for line in lines:
        line = line[0]
        # print(f"{line=}")
        a, b = (line[0], line[1]), (line[2], line[3])
        # print(f"{a=}, {b=}")
        cv2.line(line_image, a, b, 255, HIGHLIGHT_WIDTH)
        cv2.circle(line_image, a, HIGHLIGHT_DOT_SIZE, HIGHLIGHT_COLOR)
        cv2.circle(line_image, b, HIGHLIGHT_DOT_SIZE, HIGHLIGHT_COLOR)

    if SHOW_INTERMEDIATE_IMAGES:
        cv2.imshow("lines", line_image)

    # eldonti, hogy az eredeti kepre, vagy egy zajterhelt kepre (overlay_base) kell felrajzolni a maszkot a megfeleo
    # szinnel
    if overlay_base is None:
        overlaid = IMG.copy()
    else:
        overlaid = overlay_base.copy()
    overlaid[line_image > 200] = HIGHLIGHT_COLOR
    cv2.imshow("Lines", overlaid)

    return lines


def prep(img):
    if HUE_SEARCH:
        return preprocess_combined_hue_brightness(img)
    else:
        return preprocess_brightness(img)


def run(img=None):
    """megfelelo parameterekkel elinditja a rajzolast: ha van zajterheles, akkor a zajterhelt kepre lesznek
    felrajzolva a vonalak, ha  nincs, akkor az eredeti kep masolatasra """
    ln = []
    if img is None:
        ln = get_lines(prep(IMG), IMG)
    else:
        ln = get_lines(prep(img), img)
    print(f"There are {len(ln)} toothpicks in this image")


def on_trackbar_line_len(val):
    """Szűrés a kép rövidebb oldalához képest százalákosan megadott hosszüságü vonalakra"""
    global TARGET_LINE_LENGTH
    if val == 0:
        TARGET_LINE_LENGTH = 0
        # print(f"{TARGET_LINE_LENGTH=}")
    else:
        TARGET_LINE_LENGTH = min(IMG.shape[0:2]) * (val / 100)
        # print(f"{TARGET_LINE_LENGTH=}")
    run()


def on_trackbar_show_intermediates(val):
    """ki lehet valasztani, hogy a debugolasra hasznalt lepesenkenti kepeket is meg szeretnenk-e jeleniteni"""
    global SHOW_INTERMEDIATE_IMAGES
    if val == 0:
        SHOW_INTERMEDIATE_IMAGES = False
    else:
        SHOW_INTERMEDIATE_IMAGES = True
    run()


def on_trackbar_change_hue(val):
    """kivalaszthatjuk a keresett siznt"""
    global TARGET_HUE, HSV_SIMILARITY_TH_LOWER, HSV_SIMILARITY_TH_UPPER, HUE_SEARCH
    if val == 0:
        HUE_SEARCH = False
        print(f"{HUE_SEARCH=}")
    else:
        HUE_SEARCH = True
        TARGET_HUE = val - 1
        HSV_SIMILARITY_TH_UPPER = (TARGET_HUE + HUE_SIMILARITY_TH // 2, 255, 255)
        HSV_SIMILARITY_TH_LOWER = (TARGET_HUE - HUE_SIMILARITY_TH // 2, 5, 5)
        print(f"{HUE_SEARCH=}")
    run()


def on_trackbar_hist_stretch(val):
    """debugolashoz hasznalt, kivalaszthatjuk az adaptiv hisztogramszethuzas merteket"""
    global HIST_STRETCH_WIDTH
    HIST_STRETCH_WIDTH = val
    run()


def on_trackbar_add_additive_noise(val):
    """kivalaszthatjuk az additiv zaj merteket"""
    run(helper.add_additive_noise(IMG, val))
    cv2.setTrackbarPos("salt-pepper noise", "Lines", 0)


def on_trackbar_add_salt_pepper_noise(val):
    """kivalaszthatjuk a so-bors zaj merteket"""
    run(helper.add_salt_pepper_noise(IMG, val))
    cv2.setTrackbarPos("additive noise", "Lines", 0)


cv2.createTrackbar("Line Length filter", "Lines", 0, 100,
                   on_trackbar_line_len)  # 0 means the lenght filetering is turned off
cv2.createTrackbar("Show intermediate images", "Lines", 0, 1, on_trackbar_show_intermediates)
cv2.createTrackbar("Target Hue", "Lines", 0, 181, on_trackbar_change_hue)

cv2.createTrackbar("additive noise", "Lines", 0, 200, on_trackbar_add_additive_noise)
cv2.createTrackbar("salt-pepper noise", "Lines", 0, 100, on_trackbar_add_salt_pepper_noise)

# cv2.createTrackbar("Hist stetch", "Lines", 0, 100, on_trackbar_hist_stretch)

# on_trackbar_line_len(0)
# on_trackbar_show_intermediates(SHOW_INTERMEDIATE_IMAGES)
on_trackbar_change_hue(TARGET_HUE)
cv2.setTrackbarPos("Target Hue", "Lines", TARGET_HUE)
# on_trackbar_hist_stretch(HIST_STRETCH_WIDTH)

cv2.waitKey(0)
