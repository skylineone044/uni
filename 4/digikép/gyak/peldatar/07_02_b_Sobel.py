
import cv2
import numpy as np

MAGN_THRESH_PERCENT = 0.2

img = cv2.imread('OpenCV-logo.png', cv2.IMREAD_GRAYSCALE)
# img = cv2.imread('GolyoAlszik_rs.jpg', cv2.IMREAD_GRAYSCALE)
# img = cv2.imread('PalPant_800.jpg', cv2.IMREAD_GRAYSCALE)
# img = cv2.imread('SeaCliffBridge_3_800.jpg', cv2.IMREAD_GRAYSCALE)


# Kép normalizálása és megjelenítése
def display_image(window, image):
    disp = cv2.normalize(image, None, 0, 255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8UC1)
    cv2.imshow(window, disp)


# ksize = -1   # Scharr kernel
ksize = 3    # 3x3 Sobel
# ksize = 5    # 5x5 Sobel

Ix = cv2.Sobel(img, cv2.CV_32FC1, 1, 0, None, ksize)
display_image('Ix', Ix)

Iy = cv2.Sobel(img, cv2.CV_32FC1, 0, 1, None, ksize)
display_image('Iy', Iy)

Imagn = cv2.magnitude(Ix, Iy)
display_image('Gradient magnitude', Imagn)

magn_th = np.amax(Imagn) * MAGN_THRESH_PERCENT
print('magn_th =', magn_th)
_, ImagnTh = cv2.threshold(Imagn, magn_th, 1.0, cv2.THRESH_BINARY)
display_image('Thresholded gradient magnitude', ImagnTh)

cv2.waitKey(0)
cv2.destroyAllWindows()
