
import cv2
import numpy as np

def onParam1Trackbar(x):
    print("=============================")
    print('param1 csúszka pozíció:', x)


def onParam2Trackbar(x):
    print("=============================")
    print('param2 csúszka pozíció:', x)
    param = (x - 50) / 100
    print('Átalakított paraméter érték: {}'.format(param) )


# im = cv2.imread('OpenCV-logo.png', cv2.IMREAD_COLOR)
im = np.ndarray((200, 640, 3), np.uint8)
im.fill(192)
cv2.imshow('window', im)

cv2.createTrackbar('param1', 'window', 5, 10, onParam1Trackbar)
cv2.createTrackbar('param2', 'window', 50, 100, onParam2Trackbar)

cv2.waitKey(0)

cv2.setTrackbarMin('param2', 'window', 20)
cv2.setTrackbarMax('param2', 'window', 50)
cv2.setTrackbarPos('param2', 'window', 30)
pos = cv2.getTrackbarPos('param2', 'window')
print('Trackbar pozíció:', pos)

cv2.waitKey(0)
cv2.destroyAllWindows()
