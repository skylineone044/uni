//This file was generated from (Academic) UPPAAL 4.1.26-1 (rev. 7BCF30B7363A9518), February 2022

/*

*/
A[] not deadlock

/*

*/
E<> wire_assembler.output_inventory == 5

/*

*/
E<> wire_assembler.output_inventory > 5

/*

*/
E<> wire_assembler.input_inventory == 5 && wire_assembler.output_inventory == 5

/*

*/
E<> wire_assembler.input_inventory > 1

/*

*/
E<> wire_assembler.input_inventory == 2

/*

*/
E<> wire_assembler.input_inventory == 3

/*

*/
E<> copper_inserter.inventory > 1

/*

*/
E<> copper_inserter.inventory < 0

/*

*/
wire_assembler.output_inventory == 0 --> wire_assembler.output_inventory == 5

/*

*/
copper_inserter.inserting --> wire_extractor.extracting

/*

*/
(wire_assembler.working and copper_inserter.inserting) --> wire_extractor.extracting

/*

*/
copper_inserter.inserting --> wire_assembler.working

/*

*/
wire_assembler.working --> wire_assembler.waiting_for_extraction

/*

*/
copper_inserter.waiting_for_free_space --> copper_inserter.inserting

/*

*/
wire_assembler.waiting_for_copper_plate --> wire_assembler.working

/*

*/
E<> wire_assembler.working

/*

*/
E<> (wire_assembler.waiting_for_copper_plate && wire_assembler.input_inventory > 0 && not (copper_inserter.inserting || copper_inserter.waiting_for_copper_plate))

/*

*/
//NO_QUERY
