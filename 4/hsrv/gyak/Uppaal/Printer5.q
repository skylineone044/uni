//This file was generated from (Academic) UPPAAL 4.0.11 (rev. 4492), February 2010

/*
Nincs holtpont a rendszerben.
*/
A[] not deadlock

/*
Lehet hogy mindk\u00e9t user v\u00e1r.
*/
E<> (UserA.Wait && UserB.Wait)

/*
K\u00f6lcs\u00f6n\u00f6s kiz\u00e1r\u00e1s: egyszerre nem nyomtatnak.
*/
A[] not (UserA.Print && UserB.Print)

/*
El\u0151fordulhat, hogy UserA tud nyomtatni.
*/
E<> UserA.Print 

/*
El\u0151fordulhat, hogy UserB tud nyomtatni.
*/
E<> UserB.Print

/*
A vez\u00e9rl\u0151 nem t\u00f6k\u00e9letes: B ki tudja \u00e9heztetni A-t, azaz att\u00f3l, hogy UserA.Wait \u00e1llaptba ker\u00fclt, nem biztos, hogy ki tudja nyomatatni amit szeretne.
*/
UserA.Wait  --> UserA.Print

/*
M\u00e1sk\u00e9pp fogalmazva az el\u0151z\u0151: el\u0151fordulhat, hogy A soha nem tud nyomtatni.
*/
A<> UserA.Print

/*
De az\u00e9rt vamelyik biztosan tud el\u0151bb-ut\u00f3bb nyomtatni.
*/
A<> (UserA.Print || UserB.Print)
