//This file was generated from (Academic) UPPAAL 4.0.11 (rev. 4492), February 2010

/*
Nincs holtpont a rendszerben.
*/
A[] not deadlock

/*
Nem lehet hogy mindk\u00e9t user v\u00e1r.
*/
E<> (UserA.Wait && UserB.Wait)

/*
K\u00f6lcs\u00f6n\u00f6s kiz\u00e1r\u00e1s: egyszerre nem nyomtatnak.
*/
A[] not (UserA.Print && UserB.Print)

/*
El\u0151fordulhat, hogy UserA tud nyomtatni.
*/
E<> UserA.Print 

/*
Lehet, hogy UserB tud nyomtatni.
*/
E<> UserB.Print

/*

*/
UserB.Wait --> UserB.Print

/*
Nyomtat\u00e1si k\u00e9r\u00e9s teljes\u00edtve lesz.
*/
UserA.Wait  --> UserA.Print

/*
M\u00e1sk\u00e9pp fogalmazva az el\u0151z\u0151: el\u0151fordulhat, hogy A soha nem tud nyomtatni.
*/
A<> UserA.Print

/*
Vamelyik biztosan tud el\u0151bb-ut\u00f3bb nyomtatni.
*/
A<> (UserA.Print || UserB.Print)
