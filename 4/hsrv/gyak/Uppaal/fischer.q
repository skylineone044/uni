//This file was generated from (Academic) UPPAAL 4.0.11 (rev. 4492), February 2010

/*
Fischer's mutual exclusion protocol.
*/
//NO_QUERY

/*
Mutex requirement.
*/
A[] forall (i:id_t) forall (j:id_t) P(i).cs && P(j).cs imply i == j

/*
The system is deadlock free.
*/
A[] not deadlock

/*

*/
P(1).req --> P(1).cs

/*
Whenever P(1) requests access to the critical section it will eventually enter the wait state.
*/
P(1).req --> P(1).wait

/*

*/
//NO_QUERY
