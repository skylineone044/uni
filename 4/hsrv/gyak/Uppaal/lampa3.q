//This file was generated from (Academic) UPPAAL 4.0.11 (rev. 4492), February 2010

/*
A rendszer egyetlen fut\u00e1sa sor\u00e1n sincs holtpont.
*/
A[] not deadlock\


/*
Van olyan fut\u00e1s, mely holtponthoz vezet (szerencs\u00e9re ez nem igaz.)
*/
E<> deadlock\


/*
Valamikor vil\u00e1gos lesz a l\u00e1mpa.
*/
E<> lampa.VILAGOS

/*
A lampa mindig kikapcsolva maradhat.
*/
E[] lampa.KI

/*
Ha a l\u00e1mpa VIL\u00c1GOS, akkor el\u00f6bb ut\u00f3bb ki is kapcsol.
*/
lampa.VILAGOS --> lampa.KI

/*
Ha a l\u00e1mpa VIL\u00c1GOS, akkor el\u00f6bb ut\u00f3bb megnyomj\u00e1k a gombot \u00e9s x=0 lesz. (Ez nem igaz.)
*/
lampa.VILAGOS --> x==0

/*
Ha a l\u00e1mpa VIL\u00c1GOS, akkor el\u00f6bb ut\u00f3bb kikapcsol vagy megnyomj\u00e1k a gombot \u00e9s x=0 lesz.
*/
lampa.VILAGOS --> ( lampa.KI or x==0) 
