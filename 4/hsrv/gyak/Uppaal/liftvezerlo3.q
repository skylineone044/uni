//This file was generated from (Academic) UPPAAL 4.0.11 (rev. 4492), February 2010

/*

*/
Vezerlo.Free0 --> Vezerlo.Free2 

/*

*/
A[] (Vezerlo.Free0 or Vezerlo.On0) imply !(door(1).O or door(2).O)

/*

*/
E<>deadlock

/*

*/
A[] !deadlock

/*

*/
A[] Kabin.E0 imply (Vezerlo.Free0 or Vezerlo.On0)
