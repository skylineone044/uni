//This file was generated from (Academic) UPPAAL 4.0.11 (rev. 4492), February 2010

/*

*/
vezerlo.Free0 --> vezerlo.Free2 

/*

*/
A[] (vezerlo.Free0 or vezerlo.On0) imply !(door(1).O or door(2).O)

/*

*/
A[] !deadlock

/*

*/
z==0 --> z==1

/*

*/
vezerlo.Free0 --> vezerlo.Free1

/*

*/
A[] kabin.E0 imply (vezerlo.Free0 or vezerlo.On0)
