"Smalltalk GNU interpreter - gst"
"A terminálba beírva, hogy gst kapunk egy prompt-ot, ide írhatjuk a Smalltalk értelmezőnek szánt üzeneteinket."
"Kilépni a CTRL+D-vel tudunk."
"Van fájl mód is, pl. gst gyak01.st - ekkor a gyak01.st fájlban lévő parancsok fognak sorra végrehajtódni."
"Amennyiben nem szeretnék a sok debug üzenetet is látni, akkor a -q kapcsolóval indítsuk a gst-t."


"Kommentezési lehetőség"

'Hello World!' printNl!

"Egy String típusú objektumnak küldjük el a printNl üzenetet."
"A String osztály az Object osztálytól örökli a printNl üzenetet (hasonlóan, mint pl. Java esetén az Object.toString())."
"Van minden osztályhoz egy táblázat, ami összerendeli az üzeneteket és a hozzájuk tartozó futtatandó kódot."

'Hello World!' displayNl!   "Nem rak aposztrófot."
                            "Van sortörés mentes display és print üzenet is."

Transcript show: 'Hello World!'.    "Egyéb kiíratási lehetőségek."
Transcript cr.                      "cr = carriage return, azaz sortörés."


2 printNl!              "Egy Integer-nek megy a printNl üzenet."
5 + 2!                  "Az 5-nek elküldjük a + üzenetet a 2 argumentummal."
5 + 2 printNl!          "Elég fura eredményt kapunk (konzolra kiíródik, hogy 2."
(5 + 2) printNl!        "'Javítás.'"
(2 + 3 * 4) printNl!    "További érdekes eredmény."
(5 + 2 negated) printNl!


"Üzenetek fajtái:"
"Unary: <objektum> <üzenet>, pl. odd, even, negated, asString, print, displayNl, sqrt"
2 even displayNl!   "objektum: 2, üzenet even, majd objektum: true, üzenet displayNl"

"Binary: <objektum> <operator> <objektum>, pl. aritmetikai műveletek (lásd fentiek)"

"Kulcsszavas: több paraméteres függvények helyettesítésére"
2 max: (3 max: (4 max: 5)) displayNl!
(2 + 3 max: 3 + 4) printNl!

"A fenti érdekes eredményeket az üzenet típusok között fennálló precedencia szabályok okozzák."
"Mégpedig: unary > binary > kulcsszavas, azaz egy kifejezésben először a unary üzenet(ek), majd a binary üzenet(ek) legvégül pedig a kulcsszavas üzenet(ek) kerülnek végrehajtásra."
"Az azonos precedenciájú üzenetek esetében - amennyiben nincsen zárójelezés - balról jobbra történik a kiértékelés."

"Egyéb Integer üzenetek:"
5 odd.
4 even.
5 negated.
3 factorial.
5 squared.
5 raisedTo: 3.
12 gcd: 20.
60 lcm: 18.
2 + 3 min: 3 + 4.
2 + 3 max: 3 + 4.
(5//2).    "Egész osztás hányadosa"
(5\\2).    "Egész osztás maradéka"
