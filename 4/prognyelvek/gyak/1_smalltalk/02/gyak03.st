"Objektumok összehasonlítása"
x := 'hehe'.
y := 'hehe'.
(x = y) printNl.  "igaz, ha x és y egyenlő, tehát a tartalmuk azonos"
(x ~= y) printNl. "igaz, ha x és y nem egyenlő, tehát a tartalmuk különbözik"
(x == y) printNl. "igaz, ha x és y azonos, tehát ugyanaz az objektum"
(x ~~ y) printNl. "igaz, ha x és y különböző, tehát nem ugyanaz az objektum"


"-----------"
"Collections"
"minden collection megengedi, hogy a tárolt elemek különböző típusúak legyenek, tehát pl. egy halmazban lehet egyszerre szöveg és szám is"
"Array"
|a|
a := Array new: 20.       "20 elemű tömb létrehozása"
a displayNl.
(a at: 1) displayNl.      "az első elemet írja ki, ami most 'nil', a tömböket 1-től indexeljük "
a at: 1 put: 99.          "legyen 99 az első helyen"
a at: 2 put: 1.           "legyen 1 a második helyen"
(a at: 1) displayNl.
((a at: 1) + 1) displayNl.
(a at:1) + (a at:2) printNl!
a size printNl.           "a tömb mérete"
(a at: 21) printNl.       "tömbtúlindexelés, az interpreter szól"
#(1 $B 'string') printNl. "tömbliterál (nem lehet módosítani)"

"-----------"


"Set"
a := Set new. "üres halmaz létrehozása"
a printNl.    "kiírás"
a add: 5. a add: 7. a add: 'foo'.
a add: 9; add: 10; add: 'bar'.              "A ';' működése: az üzenetet annak az objektumnak küldi el, aminek a legutolsót küldte. Spórolás hosszú változóneveknél..."
a add:5; add: 5; add: 5; add: 5; yourself.  "A yourself üzenet egyszerűen visszaadja azt az objektumot, aminek elküldték, azért lehet hasznos, mert az add az argumentumát adja vissza"
a printNl.
a remove: 5.               "Az 5-ös kivétele"
a printNl.
a includes: 5.             "Tartalmazás vizsgálata -> true vagy false"
a includes: 7.

a addAll: #(4 5 66)        "kollekció összes elemének hozzáadása a halmazhoz"
a printNl.

"Példa halmaz feltöltésére ciklussal"
|x| x:= Set new.
1 to: 100 do: [ :i | x add: i ].
x printNl.

"-----------"
"Bag - multiset, azaz egy elem többször is előfordulhat a halmazban"
|b|
b := Bag new.
b add: 'Hello'.
b add: 'World' withOccurrences: 3. "a 'World' legyen benne a halmazban háromszor"
b printNl.

"-----------"
"Dictionary - asszociatív tömb (map), azaz kulcs-érték párok listája"
Smalltalk at: #d put: 0!      "emlékezzünk vissza, hogy hogyan hoztuk létre a globális változókat"
d := Dictionary new.
d at: 'One' put: 1.           "a kulcs és az érték is tetszőleges objektum lehet"
d at: 'Two' put: 2.
d at: 1 put: 'One'.
d at: 2 put: 'Two'.
d printNl.
(d at: 1) printNl.            "nézzük meg milyen érték van eltárolva az 1-es kulcshoz"
(d at: 'Two') printNl.

"-----------"
"Műveletek kollekciókkal"
(#(1 3 5) collect: [:x | x + 2] ) printNl.       "minden elemre végrehajtja a műveletet"
( #(1 2 3 4 5) select: [:x | x odd] ) printNl.   "válogatás, pl.: páratlan számokat (odd)"

|sum| sum := 0.
#(1 2 3) do: [:x | sum := sum + x].              "~foreach"
sum printNl.
!
