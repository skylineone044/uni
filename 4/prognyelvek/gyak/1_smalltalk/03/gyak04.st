"Egy fájl beolvasása és a karakterek kiíratása egy másik fájlba, úgy hogy mindent nagybetűsítünk."
input := FileStream open: 'be.txt' mode: FileStream read.
output := FileStream open: 'ki.txt' mode: FileStream write.
input do: [:char|
  output nextPut: (char asUppercase).
].
input close.
output close.

"Itt soronként olvasunk -> nextLine üzenet"
input := FileStream open: 'be.txt' mode: FileStream read.
[(line := input nextLine) notNil] whileTrue: [
  line asUppercase displayNl.
].
input close.

"-------------------------"
'Hello' class printNl.                "típus lekérdezése (a Java-s getClass() megfelelője)"
('Hello' isMemberOf: String) printNl. "típus vizsgálata (kb. mint az instanceof Java-ban)"

"-------------------------"
"Tört osztály"
Object subclass: Tort [ "az Object lesz az ős"
  |szamlalo nevezo|     "adattagok definiálása"

  init: sz init: n [    "kettő paraméteres inicializáló üzenet (a konstruktorban használjuk)"
    szamlalo := sz.     "a számláló adattag beállítása az első paraméter alapján"
    nevezo := n.
    self egyszerusit.   "a biztonság kedvéért kapásból csinálunk egy egyszerűsítést"
    ^self.              "visszaadjuk a felinicializált objektumot (self == this Java-ban)"
  ]

  szamlalo [            "a számláló getter üzenete, nincs paraméter"
    ^szamlalo.          "csak annyit csinál, hogy visszadja a megfelelő adattagot"
  ]

  nevezo [              "a nevező getter üzenete"
    ^nevezo.
  ]

  egyszerusit [
    |lnko|
    lnko := szamlalo gcd: nevezo. "legnagyobb közös osztó"
    szamlalo := szamlalo // lnko.
    nevezo := nevezo // lnko.
  ]

  displayNl [              "egy kamu 'toString()'"
    szamlalo display.
    $/ display.
    nevezo displayNl.
  ]

  "bináris üzenetek"
  + masik [
    |eredm sz n|
    sz := (szamlalo * masik nevezo) + (nevezo * masik szamlalo).
    n := nevezo * masik nevezo.
    eredm := Tort new: sz new: n.
    eredm egyszerusit.
    ^eredm.
  ]

  - masik [
    |eredm sz n|
    sz := (szamlalo * masik nevezo) - (nevezo * masik szamlalo).
    n := nevezo * masik nevezo.
    eredm := Tort new: sz new: n.
    eredm egyszerusit.
    ^eredm.
  ]

  * masik [
    |eredm sz n|
    sz := szamlalo * masik szamlalo.
    n := nevezo * masik nevezo.
    eredm := Tort new: sz new: n.
    eredm egyszerusit.
    ^eredm.
  ]

  / masik [
    |eredm sz n|
    sz := szamlalo * masik nevezo.
    n := nevezo * masik szamlalo.
    eredm := Tort new: sz new: n.
    eredm egyszerusit.
    ^eredm.
  ]

  printOn: stream [             "valódi 'toString()', ezt hívja meg a print(Nl)/display(Nl) a megfelelő paraméterrel (=a stream ahová ki kell írni)"
    szamlalo printOn: stream.
    $/ displayOn: stream.       "azért displayOn, hogy a $ ne legyen benne a kiírásban (print-tel benne lenne)"
    nevezo printOn: stream.
  ]

]

"konstruktor"
Tort class extend [        "az osztályt egészítjük ki azzal, hogy hogyan kell belőle egy példányt létrehozni"
  new: sz new: n [
    |obj|
    obj := super new.      "létrehozunk egy 'üres' objektumot az ős konstruktorának meghívásával"
    obj init: sz init: n.  "inicializáljuk az új objektumot"
    ^obj.                  "visszaadjuk a friss, ropogós objektumot"
  ]
]

"példa a Tort osztály használatára"
|r1 r2|
r1 := Tort new: 2 new: 3.
r2 := Tort new: 3 new: 4.
(r1 + r2) printNl.
(r1 - r2) printNl.
(r1 * r2) printNl.
(r1 / r2) printNl.
!

"------------------------"
"(beépített) osztály kiterjesztése (saját) üzenettel: factorial"
SmallInteger extend [
  factorial [
    |product x|
    product := x := 1.
    [self >= x] whileTrue: [
      product := product * x.
      x := x + 1.
    ].
    ^product.
  ]
]

"példa az új üzenet használatára"
5 factorial printNl.
