{-
  Lusta kiértékelés:					
	 duplaz (inc 9)
	 (inc 9) + (inc 9)
	 (9 + 1) + (9 + 1)
	 10 + 10
	 20

  Mohó kiértékelés:
 	 duplaz (inc 9)
	 duplaz (9 + 1)
	 duplaz 10
	 10 + 10
	 20

  Interpreter indítása: hugs
  Fájl betöltés: :load Haskell-1.hs
  Fájl újratöltése: :reaload Haskell-1.hs
  Kilépés: :quit

  Típusok: 
   Int (+,-,*,div,mod,^)
   Float (+,-,/,*,sqrt,log,sin..)
   Bool (True,False,not, &&, ||)
   Char ('A')
   String ("Hello")
   Lista típus
   Felhasználó által definiált

   Relációs operátorok:
	==, /=, >,>=,<,<=

Általános függvénydefiníció:
függvényazonosító :: <arg1 típusa> -> <arg2 típusa> ... -> <visszatérési típus>
függvényazonosító arg1 arg2 ... = <kifejezések>
A visszatérési értéket a <kifejezések> kiértékelése határozza meg
-}

duplaz :: Int -> Int
duplaz x = x + x

add :: Int -> Int -> Int
add x y = x + y


-- Esetvizsgálat az if kifejezéssel
factorial :: Int -> Int
factorial n = 
  if n == 0  
	then 1 
	else n * (factorial (n-1))
 

-- Esetvizsgálat õrökkel (Boolean Guards)
factorial2 :: Int -> Int
factorial2 n 
  | n == 0 = 1
  | n>0 = n * (factorial (n-1))


-- Esetviszgálat a case kifejezéssel
factorial4 :: Int -> Int
factorial4 n = 
  case n of
  0 -> 1
  n -> n * (factorial (n-1))


-- Esetvizsgálat mintaillesztéssel
factorial3 :: Int -> Int
factorial3 0 = 1
factorial3 n = n * (factorial (n-1))


-- További példák
size :: Int
size = 13 + 72

square :: Int -> Int
square n = n * n

length :: Int -> Int -> Int
length x y = y * square x + size

exOR :: Bool -> Bool -> Bool
exOR True x = not x
exOR False x = x

xmax :: Int -> Int -> Int
xmax x y 
  | x >= y = x
  | otherwise = y

zmax :: Int -> Int -> Int
zmax x y = 
  if x > y
  then x 
  else y

fibonacci :: Int -> Int
fibonacci x
 | x == 0 = 0
 | x == 1 = 1
 | x > 1 = fibonacci (x-2) + fibonacci (x-1)

xminmax :: Int -> Int -> (Int,Int)
xminmax x y
 | x > y = (x,y)
 | otherwise = (y,x)
 

--Típusdefiníció
type Pair = (Int,Int)

minmax :: Int -> Int -> Pair
minmax x y = (min x y,max x y)


--Egy újabb típusdefiníció
type StrFlt = (Char,Float)

which :: StrFlt -> StrFlt -> Char
which (s1,f1) (s2,f2)
 | f1 < f2 = s1
 | otherwise = s2
 
second :: Pair -> Int
second (x,y) = y