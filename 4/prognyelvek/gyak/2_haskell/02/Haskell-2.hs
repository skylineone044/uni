-- Lokális függvénydefiníció

-- Előre: let ... in 
kerulet1 :: Int -> Int -> Int
kerulet1 a b = 
  let dupla x = x + x
  in dupla a + dupla b -- ez a visszatérési érték
		  
-- Utólag: ... where ...
kerulet2 :: Int -> Int -> Int
kerulet2 a b = dupla a + dupla b --  ez a visszatérési érték
  where dupla x = x + x

-- másodfokú egyenlet megoldása
roots :: (Float, Float, Float) -> (Float, Float)
roots (a,b,c) = 
  if d<0 
  then error "negativ diszkriminans"
  else (x1, x2)
  where
		x1 = (e + sqrt  d) / (2 * a)
		x2 = (e - sqrt d) / (2 * a)
		d = b * b - 4 * a * c
		e = (-b)

-- lista elemszáma 
size :: [Int] -> Int  -- input: [1..10], [0,2..20]
size [] = 0
size (h:t) = 1 + size t

-- lista elemeinek összege 
summ :: [Int] -> Int 
summ [] = 0
summ (h:t) = h + summ t

-- pozitív egész számok a listában
pos :: [Int] -> Int
pos [] = 0
pos (h:t) = 
  if h > 0
	then 1 + pos t
	else pos t

-- lista megfordítása
rev :: [Int] -> [Int]
rev [] = []
rev (h:t) = rev t ++ [h] -- h-t listába kell tenni

-- lista első n db elemének kiválogatása 
elson :: [Int] -> Int -> [Int]       
elson [] x = []
elson _ 0 = []                       -- _ tetszőleges elemre illeszkedik
elson (h:t) n = h : elson t (n-1) 

-- lista elmeinek átlaga
atlag1 :: [Float] -> Float
atlag1 lista = (osszeg lista) / (hossz lista)
	where
		osszeg [] = 0
		osszeg (h:t) = h + osszeg t
		hossz [] = 0
		hossz (_:t) = 1 + hossz t

-- néhány, már létező beépített függvény listákra: filter, foldr1, zip