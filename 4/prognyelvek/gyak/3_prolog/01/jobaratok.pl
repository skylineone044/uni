/*
Tenyek (ismert allitasok)
+
Szabalyok (eddigi ismeretekbol tovabbi tenyek generalasa)
= model (tudashalmaz)

Kerdeseket lehet feltenni
*/

%ez egy komment
/*
ez egy
tobbsoros
komment
*/


%TENYEK
szereplo(joey). %joey egy szereplo
szereplo(ross). %kis kezdobetuvel (ross) konstans, nagy kezdobetuvel (Ross) valtozo
szereplo(rachel). %fontos a vegen levo pont
szereplo(phoebe).
szereplo(monica).
szereplo(chandler).

kedveli(ross,rachel). %ross kedveli rachelt
kedveli(rachel,ross).
kedveli(monica,chandler).
kedveli(chandler,monica).
kedveli(chandler,phoebe).
kedveli(phoebe,joey).
kedveli(monica,joey).
kedveli(joey,rachel).

/*
%Prolog inditasa: prolog

%BETOLTES
consult(fajlnev). %fajlnev kiterjesztes nelkul


%KERDESEK
szereplo(joey). %joey szereplo-e

szereplo(bela). %bela szereplo-e

kedveli(ross,rachel). %kedveli-e ross rachelt?

kedveli(ross,Kit). %kit kedvel ross?

kedveli(Ki,rachel). %ki kedveli rachelt?

kedveli(rachel,_). %kedvel-e valakit rachel?

kedveli(_,rachel). %kedveli-e valaki rachelt?


%SZABALYOK
% , jelentese: es
% ; jelentese: vagy
% not jelentese: nem

kapcsolat(X,Y) :-
  kedveli(X,Y),
  kedveli(Y,X).

baratsag(X,Y) :-
  kedveli(X,Y);
  kedveli(Y,X).

%ugyanaz maskeppen megfogalmazva:
%baratsag(X,Y) :-
%  kedveli(X,Y).
%baratsag(X,Y) :-
%  kedveli(Y,X).


%KERDESEK  
baratsag(joey,rachel). % lehet-e baratsag joey es rachel kozott?

kapcsolat(joey,rachel). % lehet-e kapcsolat joey es rachel kozott?

baratsag(joey,Valaki). % joey kivel lehet barat?

kapcsolat(joey,Valaki). % joey kivel lehet kapcsolatba?

baratsag(rachel,_). % lehet-e valaki rachel baratja?

kapcsolat(rachel,_). % lehet-e valaki kapcsolatban rachellel?


%Tipikus hibak
%- nincs fajl betoltve
%- nincs pont a vegen
%- felesleges szokoz
%- a konstans neve ragozott formaban
%- kis-nagy betu
*/