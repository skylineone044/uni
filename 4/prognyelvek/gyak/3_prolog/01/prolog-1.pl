% egysoros komment

/* több
soros
komment
*/

% A Prolog programok három alapvető dologból épülnek fel: tények, szabályok és kérdések.
% A tények és szabályok halmaza = tudásbázis, vagy más néven tudáshalmaz = Prolog program.

% Példa 1
% Az egyszerű klózokat nevezzük tényeknek. Ezekkel írjuk le az ismert (igaz) dolgokat, állapotokat.
% Mia, Jody, és Yolanda nők, ezt tényekkel írjuk le.
woman(mia).
woman(jody).
woman(yolanda).
% Jody léggitározik, ez is egy tény lesz.
playsAirGuitar(jody).
% És az is egy tény lesz, hogy egy partin vagyunk.
party.
% A fenti tényekben a kisbetűs változónak látszó dolgok (mia, jody, yolanda) a konstansok.
% A változók nagybetűvel vagy alsóvonással kezdődnek (lásd később).

% Miután megírtuk a tudásbázist, elindítjuk a Prolog-ot méghozzá úgy, hogy a terminálba beírjuk, hogy prolog, majd ENTER.
% Tegyük fel, hogy a tudásbázisunk a pelda.pl fájlban van és a prolog parancsot abban a mappában adtuk ki, ahol ez a fájl van.
% Ekkor, miután a prolog elindult, a consult(pelda). paranccsal tudjuk betölteni a tudásbázisunkat, majd a következő kérdéseket tudjuk feltenni:
% (A ?- csak a prolog konzolt szimbilizálja, ezt a részt nem kell beírni!!!)
% ?- woman(mia). % válasz: true (vagy yes Prolog verziótól függően)
% ?- playsAirGuitar(jody). % válasz: true
% ?- playsAirGuitar(mia). % válasz: false (vagy no Prolog verziótól függően)
% ?- playsAirGuitar(vincent). % válasz: false  (nincs infonk vincent-rõl)
% ?- tatooed(jody). % hiba (nincs ilyen tényünk)


% Példa 2
% Az összetett klózokat (predikátumokat) szabályoknak nevezzük. Ezekkel a valamilyen feltétel(ek)től függő dolgokat írjuk le.
% A szabályokat "head :- body" formában kell felírni.
% A :- operátor bal oldalán a szabály fej része, a jobb oldalán pedig a szabály törzse áll.
% Amennyiben a szabály törzse igaz, akkor a fej része is igaz.
% (modus ponens: ha van egy "head :- body" szabály, akkor a Prolog az ismert body alapján meg tudja mondani a head-et is)
% Tehát, a :- operátor egy fejre állított (jobbról balra olvassuk ki) if kifejezésnek feleltethető meg.
% Egy tény egyébként nem más, mint egy olyan szabály, aminek nincs semmi a jobb oldalán (üres a törzse).
happy(yolanda).
listens2Music(mia).
listens2Music(yolanda) :- happy(yolanda). % Yolanda zenét hallgat, ha boldog.
playsAirGuitar(yolanda) :- listens2Music(yolanda). % Yolanda léggitározik, ha zenét hallgat.
playsAirGuitar(mia) :- listens2Music(mia).

% Eldöntendő (igaz/hamis) kérdések:
% ?- playsAirGuitar(mia). % válasz: true (modus ponens)
% ?- playsAirGuitar(yolanda). % válasz: true (modus ponens 2 lépésben)
% Kielégítendő (ki kell következtetni azokat az értékeket, amelyek a kifejezést igazzá teszik) kérdések:
% ?- happy(Ki). % válasz: Ki = yolanda (A Ki ebben az esetben egy változó. Ennek a helyére keressük az értékeket, amitől a kérdés igaz lesz.)
% ?- playsAirGuitar(Ki). % válasz: Ki = yolanda és Ki = mia (Több lehetséges válasz van, ha mindre kíváncsiak vagyunk, akkor a ;-t nyomogassuk miután a Prolog kiírta az aktuális választ.)


% Példa 3
happy(vincent).
listens2Music(butch).

% A logikai konjukciót (és művelet) vesszõvel írjuk le.
% Vincent léggitározik, ha zenét hallgat és boldog is.
playsAirGuitar(vincent) :- listens2Music(vincent), happy(vincent).

% A logikai diszjunkciót (vagy műveletet) az adott predikátum több sorba írásával, vagy pontosvesszővel írjuk le.
% Butch léggitározik, ha boldog, vagy ha zenét hallgat.
playsAirGuitar(butch) :- happy(butch).
playsAirGuitar(butch) :- listens2Music(butch).
% Lehetne ez is:
playsAirGuitar(butch) :- happy(butch); listens2Music(butch).

% Kérdések:
% ?- playsAirGuitar(vincent). % válasz: false
% ?- playsAirGuitar(butch). % válasz: true
