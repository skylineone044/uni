% Lista elemeinek kiírása
kiir([]).
kiir([E]) :- write(E).
kiir([E|L]) :- write(E), write(','), kiir(L).

% Határozzuk meg egy lista első elemét!
% pl.: elso([1,2,3],X). -> X = 1
elso([H|_],H).

% Határozzuk meg egy lista utolsó elemét!
utolso([H],H).
utolso([_|T], X) :- utolso(T,X).

% Határozzuk meg egy lista elemszámát!
len([],0).
len([_|T],N) :- len(T,N1), N is N1 + 1.

% Összegezzük egy egész számokból álló lista elemeit!
sum([],0).
sum([H|T],N) :- sum(T,N1), N is N1 + H.

% Határozuk meg, hogy egy elem eleme-e a listának!
member([E|_],E).
member([_|T],X) :- member(T,X).

% Adott egy lista és egy elem. Töröljük ki az elemet a listából!
del([H|T],H,T).
del([H|T],X,[H|L]) :- del(T,X,L).

% Fűzzünk össze két listát!
% append([a,b,c], [d,e,f], X).   ->   X = [a, b, c, d, e, f]
append([],L,L).
append([X|L1],L2,[X|L3]) :- append(L1,L2,L3).

% Fordítsunk meg egy listát!
% rev([aa,bb,cc,dd],X).   ->    X = [dd, cc, bb, aa]
rev([],[]).
rev([H|T],L) :- rev(T,L1), append(L1,[H],L).

/*
Írj egy olyan relacio predikátumot, mely két egész számokból álló lista közötti
relációt fejez ki. Két lista akkor van relációban egymással, ha a hosszuk megegyezik,
és az első lista minden nem negatív elemének kétszerese szerepel a második lista ugyanazon pozíciójában!
Pl.: relacio([2,-1,3],[4,5,6]). –ra: True.
*/
rel([],[]).
rel([H1|T1],[_|T2]) :- H1 < 0, rel(T1,T2).
rel([H1|T1],[H2|T2]) :- H1 >= 0, H2 is H1 + H1, rel(T1,T2).
