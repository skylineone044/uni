{-----------------------------------------------------------------}
{- A kommentezett rész után készítsd el a 2. feladat megoldását! -}
{-----------------------------------------------------------------}

nedik :: [a] -> Int -> [a] -- a 'tipus valtozo' megengedi, hogy tetszoleges tipusu listan dolgozzunk, mindenhol ahol a szerepel oda csak ugyan az a tipus pl. Int kerülhet, igy lehet majd pl. [Int] -> Int -> [Int], vagy [Char] -> Int -> [Char] a konkret fejlec
nedik [] n = [] -- n lehetne placeholder: _, mert nem hasznaljuk a jobb oldalon, nem lenyeg, hogy az ures listabol hanyadik elemet szeretnenk
nedik (h:t) 0 = [h] -- itt t lehetne placeholder, mert megvan a keresett elem (az elso, azaz h), igy a t erteke mar nem szamit
nedik (h:t) n = nedik t (n - 1)

--nedik [1..10] 5



{-----------------------------------------------------------------}
{- A kommentezett rész után készítsd el a 3. feladat megoldását! -}
{-----------------------------------------------------------------}

type Komplex = (Float,Float)

kivon :: Komplex -> Komplex -> Komplex -- fejlecben a tipus neve: Komplex kell
kivon (r1,i1) (r2,i2) = (r1-r2,i1-i2) -- torzsben a type jobb oldalan levo kifejezesre: (Float,Float) illeszkedo resz kell: (r1,i1) es (r2,i2)

--kivon (3.14,8.9) (1.23,4.56)

abszolutertek :: Komplex -> Float
abszolutertek (r,i) = sqrt (r * r + i * i) -- lehetne sqrt (r ^ 2 + i ^ 2)

--absz (1.25, 5.33)
