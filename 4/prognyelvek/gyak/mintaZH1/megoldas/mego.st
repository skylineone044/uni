"----------------------------------------------------------------"
" A kommentezett rész után készítsd el az 1. feladat megoldását! "
"----------------------------------------------------------------"

Object subclass: Intervallum [ "erre a reszre jonnek az objektum peldanyra vonatkozo dolgok"
	|a b| "adattagok"

	init: x init: y [
		a := x.
		b := y.
		^self. "opcionalis"
	]

	a [ ^a. ] "a getter"
	b [ ^b. ] "b getter"

	a: uj_a [ a := uj_a. ] "a feladat nem keri, de igy nezne ki az a setter"

	printNl [ "[a, b] forma, a vegen sortoressel"
		$[ display.
		a display.
		', ' display.
		b display.
		$] displayNl.
	]

	hossz [
		^(b-a)+1.
	]

	* masik [
		|also felso e|
		"itt a 'masik a' es 'masik b' reszben az 'a' es 'b' a getter-ek meghivasa"
		also := (a * masik a) min: (a * masik b) min: (b * masik b) min: (b * masik a).
		felso := (a * masik a) max: (a * masik b) max: (b * masik b) max: (b * masik a).
		e := Intervallum new: also new: felso. "az eredmenybol egy uj Intervallum objektumot csinalunk"
		^e. "es ezt az objektumot adjuk vissza"
	]

].

Intervallum class extend [ "erre a reszre pedig az osztalyra vonatkozo dolgok"
	new: a new: b [ "konstruktor"
		|obj|
		obj := super new.
		obj init: a init: b.
		^obj.
	]
].

s := Set new.

1 to: 50 do: [:i|
	x := Intervallum new: (i negated) new: (i * i). "i negated helyett lehetne -i, i * i helyett pedig i squared"
	s add: x.
].

sum := 0.

s do: [:inter|
	sum := sum + inter hossz.
].

sum printNl.
