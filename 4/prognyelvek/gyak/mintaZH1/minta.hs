{-----------------------------------------------------------------}
{- A kommentezett rész után készítsd el a 2. feladat megoldását! -}
{-----------------------------------------------------------------}
n_edik :: [a] -> Int -> [a]
n_edik [] n = []
n_edik (h:t) 0 = [h]
n_edik (h:t) n = n_edik t (n-1)

-- n_edik [1, 2, 4, 5, 6, 7, 9, 2, 3] 6



{-----------------------------------------------------------------}
{- A kommentezett rész után készítsd el a 3. feladat megoldását! -}
{-----------------------------------------------------------------}
type Komplex = (Float, Float)

kivon :: Komplex -> Komplex -> Komplex
kivon (r1, i1) (r2, i2) = ((r1 - r2), (i1 - i2))

abszolutertek :: Komplex -> Float
abszolutertek (r, i) = sqrt(r * r + i * i)

-- kivon (3.14, 15) (2, 100)
-- abszolutertek (3.14, 15)

