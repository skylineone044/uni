"----------------------------------------------------------------"
" A kommentezett rész után készítsd el az 1. feladat megoldását! "
"----------------------------------------------------------------"

Object subclass: Intervallum [
    |a b|

    init: also init: felso [
        a := also.
        b := felso.
        ^self.
    ]

    a [
        ^a.
    ]

    b [
        ^b.
    ]

    printNl [
        '[' display.
        a display.
        ', ' display.
        b display.
        ']' displayNl.
    ]

    hossz [
        ^(b - a)
    ]

    * masik [
        |ac ad bc bd uj mini maxi|
        ac := self a * masik a.
        ad := self a * masik b.
        bc := self b * masik a.
        bd := self b * masik b.

        mini := (((ac min: ad) min: bc) min: bd).
        maxi := (((ac max: ad) max: bc) max: bd).

        uj := Intervallum  new: mini new: maxi.
        ^uj.
    ]
]

Intervallum class extend [
    new: also new: felso [
        |obj|
        obj := super new.
        obj init: also init: felso.
        ^obj.
    ]
]

" |a b|                           "
" a := Intervallum new: 1 new: 3. "
" b := Intervallum new: 2 new: 6. "
" a printNl.                      "
" b printNl.                      "
" (a * b) printNl.                "

|inters| inters := Set new.
1 to: 50 do: [ :i |
    inters add: (Intervallum new: i negated new: i squared)
].

(inters size) printNl.
inters printNl.

sum := 0.
inters do: [ :inter |
    sum := sum + inter hossz.
].

sum printNl.

