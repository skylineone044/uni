duplaz :: Int -> Int
duplaz x = x + x

add :: Int -> Int -> Int
add x y = x + y


factorial :: Int -> Int
factorial n = 
  if n == 0  
  then 1 
  else n * (factorial (n-1))
 

factorial2 :: Int -> Int
factorial2 n 
  | n == 0 = 1
  | n>0 = n * (factorial (n-1))


factorial4 :: Int -> Int
factorial4 n = 
  case n of
  0 -> 1
  n -> n * (factorial (n-1))


factorial3 :: Int -> Int
factorial3 0 = 1
factorial3 n = n * (factorial (n-1))


size :: Int
size = 13 + 72

square :: Int -> Int
square n = n * n

length :: Int -> Int -> Int
length x y = y * square x + size

exOR :: Bool -> Bool -> Bool
exOR True x = not x
exOR False x = x

xmax :: Int -> Int -> Int
xmax x y 
  | x >= y = x
  | otherwise = y

zmax :: Int -> Int -> Int
zmax x y = 
  if x > y
  then x 
  else y

fibonacci :: Int -> Int
fibonacci x
 | x == 0 = 0
 | x == 1 = 1
 | x > 1 = fibonacci (x-2) + fibonacci (x-1)

xminmax :: Int -> Int -> (Int,Int)
xminmax x y
 | x > y = (x,y)
 | otherwise = (y,x)
 

type Pair = (Int,Int)

minmax :: Int -> Int -> Pair
minmax x y = (min x y,max x y)


type StrFlt = (Char,Float)

which :: StrFlt -> StrFlt -> Char
which (s1,f1) (s2,f2)
 | f1 < f2 = s1
 | otherwise = s2
 
second :: Pair -> Int
second (x,y) = y


kerulet1 :: Int -> Int -> Int
kerulet1 a b = 
  let dupla x = x + x
  in dupla a + dupla b
      
kerulet2 :: Int -> Int -> Int
kerulet2 a b = dupla a + dupla b
  where dupla x = x + x

roots :: (Float, Float, Float) -> (Float, Float)
roots (a,b,c) = 
  if d<0 
  then error "negativ diszkriminans"
  else (x1, x2)
  where
    x1 = (e + sqrt  d) / (2 * a)
    x2 = (e - sqrt d) / (2 * a)
    d = b * b - 4 * a * c
    e = (-b)

size :: [Int] -> Int 
size [] = 0
size (h:t) = 1 + size t

summ :: [Int] -> Int 
summ [] = 0
summ (h:t) = h + summ t

pos :: [Int] -> Int
pos [] = 0
pos (h:t) = 
  if h > 0
  then 1 + pos t
  else pos t

rev :: [Int] -> [Int]
rev [] = []
rev (h:t) = rev t ++ [h]

elson :: [Int] -> Int -> [Int]       
elson [] x = []
elson _ 0 = []                      
elson (h:t) n = h : elson t (n-1) 

atlag1 :: [Float] -> Float
atlag1 lista = (osszeg lista) / (hossz lista)
  where
    osszeg [] = 0
    osszeg (h:t) = h + osszeg t
    hossz [] = 0
    hossz (_:t) = 1 + hossz t


polisize :: [a] -> Int
polisize [] = 0
polisize (h:t) = 1 + polisize t

type Pair = (Int, Int)

osszead :: Pair -> Pair -> Pair
osszead (a,b) (c,d) = (a+c, b+d)


data Szin = Fekete | Feher | Lila | Zold | Sarga

data Hetnapja = Hetfo | Kedd | Szerda | Csutortok | Pentek | Szombat | Vasarnap

hetvege :: Hetnapja -> Bool
hetvege Szombat = True
hetvege Vasarnap = True
hetvege _ = False

type Oldal = Float
type Sugar = Float
data Sikidom = Teglalap Oldal Oldal | Negyzet Oldal | Kor Sugar

terulet :: Sikidom -> Float
terulet (Teglalap a b) = a*b
terulet (Negyzet a) = a*a
terulet (Kor a) = a*a*3.14

kerulet :: Sikidom -> Float
kerulet (Teglalap a b) = 2*(a+b)
kerulet (Negyzet a) = 4*a
kerulet (Kor a) = 2*a*3.14

data BinTree a = NIL | Leaf a | Branch a (BinTree a) (BinTree a)

inorder::BinTree a -> [a]
inorder NIL = []
inorder (Leaf x) = [x]
inorder (Branch x l r) = inorder l ++ [x] ++ inorder r

depth::BinTree a -> Int
depth NIL = 0
depth (Leaf x) = 1
depth (Branch x l r) = max (depth l) (depth r) + 1

data Tree = Nil | Node Int Tree Tree

bejar:: Tree -> [Int]
bejar Nil = []
bejar (Node x bal jobb) = [x] ++ (bejar bal) ++ (bejar jobb)
