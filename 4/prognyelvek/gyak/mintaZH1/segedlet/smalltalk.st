'Hello World!' printNl!
'Hello World!' displayNl!
Transcript show: 'Hello World!'.
Transcript cr.


2 printNl!
5 + 2!
5 + 2 printNl!
(5 + 2) printNl!
(2 + 3 * 4) printNl!
(5 + 2 negated) printNl!
2 even displayNl!
2 max: (3 max: (4 max: 5)) displayNl!
(2 + 3 max: 3 + 4) printNl!


|x y z|
x := 200.
x displayNl.
x := y := z := 300
x displayNl.

Smalltalk at: #x put: 500!
x displayNl.

x := stdin nextLine.
x printNl.

Character space.
Character eof.
Character nl.
$a asUppercase.
$A asLowercase.
$a isLetter.
$1 isDigit.
$1 digitValue.
$a isSeparator.
$a asciiValue.

5 odd.
4 even.
5 negated.
3 factorial.
5 squared.
5 raisedTo: 3.
12 gcd: 20.
60 lcm: 18.
2 + 3 min: 3 + 4.
2 + 3 max: 3 + 4.
(5//2).
(5\\2).
97 asCharacter.

x := '1000' asInteger.

[5. 3*2. 'lol'] value!
[5. 3*2. 'lol'] value printNl!
[5 printNl. (3*2) printNl. 'lol' printNl] value!

[:a :b :c | (a + b + c) printNl] value: 2 value: 3 value: 4!

|k| k := 2.
k > 0 ifTrue: ['k pozitiv' printNl] ifFalse: ['k negativ' printNl]!

|x| x := 2.
[x < 20] whileTrue: [x printNl. x := x + 2]!

3 timesRepeat: ['Hello' printNl]!

1 to: 5 by: 3 do: [:x | x printNl]!

5 to: 1 by: -1 do: [:i | i printNl]!
(1 to: 5 by: 3) printNl!


x := 'hehe'.
y := 'hehe'.
(x = y) printNl.
(x ~= y) printNl.
(x == y) printNl.
(x ~~ y) printNl.


|a|
a := Array new: 20.
a displayNl.
(a at: 1) displayNl.
a at: 1 put: 99.
a at: 2 put: 1.
(a at: 1) displayNl.
((a at: 1) + 1) displayNl.
(a at:1) + (a at:2) printNl!
a size printNl.
(a at: 21) printNl.
#(1 $B 'string') printNl.


a := Set new.
a printNl.
a add: 5. a add: 7. a add: 'foo'.
a add: 9; add: 10; add: 'bar'.
a add:5; add: 5; add: 5; add: 5; yourself.
a printNl.
a remove: 5.
a printNl.
a includes: 5.
a includes: 7.

a addAll: #(4 5 66)
a printNl.

|x| x:= Set new.
1 to: 100 do: [ :i | x add: i ].
x printNl.

|b|
b := Bag new.
b add: 'Hello'.
b add: 'World' withOccurrences: 3.
b printNl.

Smalltalk at: #d put: 0!
d := Dictionary new.
d at: 'One' put: 1.
d at: 'Two' put: 2.
d at: 1 put: 'One'.
d at: 2 put: 'Two'.
d printNl.
(d at: 1) printNl.
(d at: 'Two') printNl.

(#(1 3 5) collect: [:x | x + 2] ) printNl.
( #(1 2 3 4 5) select: [:x | x odd] ) printNl.

|sum| sum := 0.
#(1 2 3) do: [:x | sum := sum + x].
sum printNl.


input := FileStream open: 'be.txt' mode: FileStream read.
output := FileStream open: 'ki.txt' mode: FileStream write.
input do: [:char|
  output nextPut: (char asUppercase).
].
input close.
output close.

input := FileStream open: 'be.txt' mode: FileStream read.
[(line := input nextLine) notNil] whileTrue: [
  line asUppercase displayNl.
].
input close.

'Hello' class printNl.
('Hello' isMemberOf: String) printNl.

Object subclass: Tort [
  |szamlalo nevezo|

  init: sz init: n [
    szamlalo := sz.
    nevezo := n.
    self egyszerusit.
    ^self.
  ]

  szamlalo [
    ^szamlalo.
  ]

  nevezo [
    ^nevezo.
  ]

  egyszerusit [
    |lnko|
    lnko := szamlalo gcd: nevezo.
    szamlalo := szamlalo // lnko.
    nevezo := nevezo // lnko.
  ]

  displayNl [
    szamlalo display.
    $/ display.
    nevezo displayNl.
  ]

  + masik [
    |eredm sz n|
    sz := (szamlalo * masik nevezo) + (nevezo * masik szamlalo).
    n := nevezo * masik nevezo.
    eredm := Tort new: sz new: n.
    eredm egyszerusit.
    ^eredm.
  ]

  - masik [
    |eredm sz n|
    sz := (szamlalo * masik nevezo) - (nevezo * masik szamlalo).
    n := nevezo * masik nevezo.
    eredm := Tort new: sz new: n.
    eredm egyszerusit.
    ^eredm.
  ]

  * masik [
    |eredm sz n|
    sz := szamlalo * masik szamlalo.
    n := nevezo * masik nevezo.
    eredm := Tort new: sz new: n.
    eredm egyszerusit.
    ^eredm.
  ]

  / masik [
    |eredm sz n|
    sz := szamlalo * masik nevezo.
    n := nevezo * masik szamlalo.
    eredm := Tort new: sz new: n.
    eredm egyszerusit.
    ^eredm.
  ]

  printOn: stream [
    szamlalo printOn: stream.
    $/ displayOn: stream.
    nevezo printOn: stream.
  ]

]

Tort class extend [
  new: sz new: n [
    |obj|
    obj := super new.
    obj init: sz init: n.
    ^obj.
  ]
]

|r1 r2|
r1 := Tort new: 2 new: 3.
r2 := Tort new: 3 new: 4.
(r1 + r2) printNl.
(r1 - r2) printNl.
(r1 * r2) printNl.
(r1 / r2) printNl.

SmallInteger extend [
  factorial [
    |product x|
    product := x := 1.
    [self >= x] whileTrue: [
      product := product * x.
      x := x + 1.
    ].
    ^product.
  ]
]

5 factorial printNl.
