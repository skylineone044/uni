/*****************************************************************
* A kommentezett rész után készítsd el az 1. feladat megoldását! *
*****************************************************************/
konyvtar(x).
konyvtar(y).
konyvtar(z).
fajl(a).
fajl(b).
fajl(c).
tartalmaz(x, y).
tartalmaz(x, a).
tartalmaz(y, b).
tartalmaz(y, z).
tartalmaz(y, c).

lista(Mit, Allomany) :- tartalmaz(Mit, Allomany).
lista(Mit, Allomany) :- tartalmaz(Mit, Konyvtar), lista(Konyvtar, Allomany).

% a) tartalmaz(z, a).
% b) tartalmaz(y, F), fajl(F).
% c) lista(x, Valami).




/****************************************************************
* A kommentezett rész után készítsd el a 2. feladat megoldását! *
****************************************************************/
relacio([], []).
relacio([0|T1], [_|T2]) :- relacio(T1, T2).
relacio([H1|T1], [H2|T2]) :- H1 =\= 0, H2 is H1 * 2, relacio(T1, T2).



