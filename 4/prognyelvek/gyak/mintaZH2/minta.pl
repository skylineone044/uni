/*****************************************************************
* A kommentezett rész után készítsd el az 1. feladat megoldását! *
*****************************************************************/
konyvtar(x).
konyvtar(y).
konyvtar(z).
fajl(a).
fajl(b).
fajl(c).
tartalmaz(x, y).
tartalmaz(x, a).
tartalmaz(y, b).
tartalmaz(y, z).
tartalmaz(y, c).

listaz(Mit, Allomany) :- tartalmaz(Mit, Allomany).
listaz(Mit, Allomany) :- tartalmaz(Mit, Konyvtar), listaz(Konyvtar, Allomany).

% a
% tartalmaz(z, a).

% b
% tartalmaz(y, X), fajl(X).

% c
% listaz(x, Valami).



/****************************************************************
* A kommentezett rész után készítsd el a 2. feladat megoldását! *
****************************************************************/
relacio([], []).
relacio([0|T1], [H2|T2]) :- relacio(T1, T2).
relacio([H1|T1], [H2|T2]) :- H1 =\= 0, H2 is 2*H1, relacio(T1, T2).



