woman(mia).
woman(jody).
woman(yolanda).
playsAirGuitar(jody).
party.

happy(yolanda).
listens2Music(mia).
listens2Music(yolanda) :- happy(yolanda). playsAirGuitar(yolanda) :- listens2Music(yolanda). playsAirGuitar(mia) :- listens2Music(mia).

happy(vincent).
listens2Music(butch).

playsAirGuitar(vincent) :- listens2Music(vincent), happy(vincent).

playsAirGuitar(butch) :- happy(butch).
playsAirGuitar(butch) :- listens2Music(butch).
playsAirGuitar(butch) :- happy(butch); listens2Music(butch).

nagyobb(A,B,A) :- A > B.
nagyobb(A,B,B) :- A =< B.

kiir([]).
kiir([E]) :- write(E).
kiir([E|L]) :- write(E), write(','), kiir(L).

elso([H|_],H).

utolso([H],H).
utolso([_|T], X) :- utolso(T,X).

len([],0).
len([_|T],N) :- len(T,N1), N is N1 + 1.

sum([],0).
sum([H|T],N) :- sum(T,N1), N is N1 + H.

member([E|_],E).
member([_|T],X) :- member(T,X).

del([H|T],H,T).
del([H|T],X,[H|L]) :- del(T,X,L).

append([],L,L).
append([X|L1],L2,[X|L3]) :- append(L1,L2,L3).

rev([],[]).
rev([H|T],L) :- rev(T,L1), append(L1,[H],L).

rel([],[]).
rel([H1|T1],[_|T2]) :- H1 < 0, rel(T1,T2).
rel([H1|T1],[H2|T2]) :- H1 >= 0, H2 is H1 + H1, rel(T1,T2).
