
"Változók"
"Lokális változók - csak a szakaszon/kontextuson belül él ('!' vs. '.')"
|x y z|            "Deklaráció"
x := 200.          "Egyszeres értékadás. nincs utána pont"
x displayNl.
x := y := z := 300."Többszörös értékadás"
x displayNl.
!

"Globális változó"
Smalltalk at: #x put: 500!
x displayNl.
"-----------------------------"

"Beolvasás"
x := stdin nextLine.   "Stringként olvas be"
x printNl.             "ezért itt aposztrófok közé rakja"
"-----------------------------"

"Karakter üzenetek"
Character space.
Character eof.
Character nl.
$a asUppercase.  "$a = az 'a' mint karakter"
$A asLowercase.
$a isLetter.
$1 isDigit.
$1 digitValue.
$a isSeparator.
$a asciiValue.
!
"-----------------------------"

"Integer üzenetek"
5 odd.
4 even.
5 negated.
3 factorial.
5 squared.
5 raisedTo: 3.
12 gcd: 20.
60 lcm: 18.
2 + 3 min: 3 + 4.
2 + 3 max: 3 + 4.
97 asCharacter.
"-----------------------------"

"Kasztolás, konverzió
Kasztolás nincs, csak konverzió
vált1 := vált2 asXXX.
pl:
"
x := '1000' asInteger.
"-----------------------------"

"Blokkok"
"paraméter nélküli"
[5. 3*2. 'lol'] value!  "blokk kiértékelése"
[5. 3*2. 'lol'] value printNl!  ""
[5 printNl. (3*2) printNl. 'lol' printNl] value!

"paraméteres"
[:a :b :c | (a + b + c) printNl] value: 2 value: 3 value: 4!
"-----------------------------"

"Feltételek"
|k| k := 2.
k > 0 ifTrue: ['k pozitiv' printNl] ifFalse: ['k negativ' printNl]!
"-----------------------------"

"Kezdőfeltételes ciklus (while)"
|x| x := 2.
[x < 20] whileTrue: [x printNl. x := x + 2]!
"-----------------------------"

"Számlálásos ciklus"
3 timesRepeat: ['Hello' printNl]!
"-----------------------------"

"for"
1 to: 5 by: 3 do: [:x | x printNl]! "a by: elhagyható, ilyenkor a +1 lesz a lépésköz"

"csökkenő számlálás:"
5 to: 1 by: -1 do: [:i | i printNl]!
(1 to: 5 by: 3) printNl!
"-----------------------------"
