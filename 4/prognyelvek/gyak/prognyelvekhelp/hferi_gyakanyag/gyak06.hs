-- Lokális függvénydefiníció

-- Előre: let ... in …
ker1 :: Int -> Int -> Int
ker1 a b = let dupla x = x + x
		  in dupla a + dupla b -- ez a visszatérési érték
		  
-- Utólag: ... where ...
ker2 :: Int -> Int -> Int
ker2 a b = dupla a + dupla b -- ez a visszatérési érték
		  where dupla x = x + x
		  

-- másodfokú egyenlet megoldása
roots :: (Float, Float, Float) -> (Float, Float)
roots (a,b,c) = if d<0 
				 then error "negativ diszkriminans"
				 else (x1, x2)
				   where
						x1 = (e + sqrt d) / (2 * a)
						x2 = (e - sqrt d) / (2 * a)
						d = b * b - 4 * a * c
						e = (-b)

						
-- lista elemszáma 
size :: [Int] -> Int
size [] = 0
size (h:t) = 1 + size t

-- lista elemeinek összege 
summ :: [Int] -> Int -- input: [1..10]
summ [] = 0
summ (h:t) = h + summ t

-- pozitív egész számok száma a listában 
pos :: [Int] -> Int
pos [] = 0
pos (h:t) = if h > 0
	then 1 + pos t
	else pos t
	
-- lista megfordítása 
rev :: [Int] -> [Int]
rev [] = []
rev (h:t) = rev t ++ [h] -- h-t listába kell tenni

-- lista első n db elemének kiválogatása 
elson :: [Int] -> Int -> [Int]       -- input: [1..], [1,2..]
elson [] x = []
elson _ 0 = []                       -- _ tetszőleges valami
elson (h:t) n = [h] ++ elson t (n-1) -- h : elson... is jó

-- lista elmeinek átlaga
atlag1 :: [Float] -> Float
atlag1 ls = (ossz ls) / (hossz ls)
	where
		ossz [] = 0
		ossz (h:t) = h + ossz t
		hossz [] = 0
		hossz (_:t) = 1 + hossz t
		


