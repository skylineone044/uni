-- Egyéni típusok --

-- típusszinonímák
type Pair = (Int, Int)

osszead :: Pair -> Pair -> Pair
osszead (a,b) (c,d) = (a+c, b+d)

-- valódi típusok
--data Bool = False | True

data Color = Fekete | Feher | Lila | Zold | Sarga

data Hetnapja = Hetfo | Kedd | Szerda | Csutortok | Pentek | Szombat | Vasarnap

hetvege :: Hetnapja -> Bool
hetvege Szombat = True
hetvege Vasarnap = True
hetvege _ = False

-- polimorf típusok
polisize :: [a] -> Int
polisize [] = 0
polisize (h:t) = 1 + polisize t

-- Sikidom típust definiálása, ami egy négyzet, egy téglalap vagy egy kör 
-- (valós típusú) adatait tárolja. 
type Oldal = Float
type Sugar = Float
data Sikidom = Teglalap Oldal Oldal | Negyzet Oldal | Kor Sugar

terulet :: Sikidom -> Float
terulet (Teglalap a b) = a*b
terulet (Negyzet a) = a*a
terulet (Kor a) = a*a*3.14

kerulet :: Sikidom -> Float
kerulet (Teglalap a b) = 2*(a+b)
kerulet (Negyzet a) = 4*a
kerulet (Kor a) = 2*a*3.14

-- bináris fa típus, ami a belső pontjaiban és a leveleiben is 'a' típusú értékek tárolására képes
-- példainput: ( Branch 6 (Leaf 4) (Branch 8 (Leaf 7) (Leaf 9)) )
data BinTree a = NIL | Leaf a | Branch a (BinTree a) (BinTree a)

preorder :: BinTree a -> [a]
preorder NIL = []
preorder (Leaf x) = [x]
preorder (Branch x l r) = [x] ++ preorder l ++ preorder r

inorder :: BinTree a -> [a]
inorder NIL = []
inorder (Leaf x) = [x]
inorder (Branch x l r) = inorder l ++ [x] ++ inorder r

depth :: BinTree a -> Int
depth NIL = 0
depth (Leaf x) = 1
depth (Branch x l r) = max (depth l) (depth r) + 1

-- egy egyszerűbb fa, amely Int-eket tárol a csúcsiban, és bejárása
-- példainput: ( Node 4 (Node (-3) (Node 1 Nil Nil) Nil) (Node 2 Nil Nil) )
data Tree = Nil | Node Int Tree Tree

bejarT :: Tree -> [Int]
bejarT Nil = []
bejarT (Node x bal jobb) = [x] ++ (bejarT bal) ++ (bejarT jobb)

{- további anyag (a Michnay jegyzetből):
     - beépített listafeldolgozó függvények
	 - függvények kompozíciója
	 - függvény paramétere függvény -}
