/*prolog multi-line
komment
*/

%There are only three basic constructs in Prolog: facts, rules, and queries
%facts and rules = tudáshalmaz = prolog program

%ex_1
%Mia, Jody, and Yolanda are women
%woman(mia).
%woman(jody).
%woman(yolanda).
%%Jody plays air guitar
%playsAirGuitar(jody).
%%egy partin vagyunk.
%party.

% ?- woman(mia).   -> yes  (explicit fact)
% ?- playsAirGuitar(jody). -> yes
% ?- playsAirGuitar(mia).  -> false
% ?- playsAirGuitar(vincent).  -> false  (nincs infonk vincent-rõl)
% ?- tatooed(jody).   -> error (nincs ilyen állításunk)



%ex_2
%happy(yolanda).
%listens2Music(mia).
%listens2Music(yolanda):- happy(yolanda). % Yolanda listens to music if she is happy
%playsAirGuitar(mia):- listens2Music(mia).
%playsAirGuitar(yolanda):- listens2Music(yolanda). % Yolanda plays air guitar if she listens to music

% azaz :- should be read as “if”, or “is implied by
% lhs = head of the rule     rhs = rule body
% if the body of the rule is true, then the head of the rule is true too
% modus ponens: there is a rule "head  :-  body" ekkor a prolog az ismert body alapján meg tudja mondani a head-et is.

% ?- playsAirGuitar(mia). -> yes  (modus ponens).
% ?- playsAirGuitar(yolanda).  (modus ponens 2 lépésben).

% The facts (2 db) and rules (3 darab) contained in a knowledge base are called clauses.
% predikátumok: listens2Music, happy (egyszerû klóz), playsAirGuitar
% Egy tény nem más mint egy olyan szabály aminek nincs semmi a jobb oldalán (üres törzs)



%ex_3
happy(vincent).
listens2Music(butch).

% logikai konjukció a vesszõvel: Vincent plays air guitar if he listens to music and he is happy
playsAirGuitar(vincent):-
   listens2Music(vincent),
   happy(vincent).

% multiple rules with the same head is a way of expressing logical disjunction 	 
playsAirGuitar(butch):-
   happy(butch).
playsAirGuitar(butch):-
   listens2Music(butch).
% de helyette lehetne ez is:
/*
playsAirGuitar(butch):-
   happy(butch);
   listens2Music(butch).
*/	 

% ?- playsAirGuitar(vincent).  -> no
% ?- playsAirGuitar(butch).  -> yes	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 