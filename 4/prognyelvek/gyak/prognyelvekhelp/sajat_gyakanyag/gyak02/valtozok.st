'hello' displayNl.

"Lokális változók"
"Legközelebbi felkiáltójelig (blokk vége) használható"
"A definíciókat (pipeline-ok közöztt) nem kell kiírni"
|x y|
x := 5.
y := 7.

x displayNl.
y displayNl.

"A zisten objektum"
"Smalltalk objektum: dictionary, minden ide töltődik"

Smalltalk at: #dzs put: 'asdf'! "dzs globális változó lesz"

dzs := 3.

dzs displayNl!
