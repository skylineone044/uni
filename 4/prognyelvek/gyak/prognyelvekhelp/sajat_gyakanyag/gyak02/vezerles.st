|a|

"beolvasás: a nextLine stringeket olvas"
a := stdin nextLine.

"konvertálás integerré"
a := a asInteger.

"másképp: a := stdin nextLine asInteger."

"feltételes vezérlés"
(a > 0) ifTrue: [
    'pozitiv' displayNl.
] ifFalse: [
    '0 vagy negativ' displayNl.
].

"while ciklus"
[a > 0] whileTrue: [
    'xd' displayNl.
    a := a - 1s.
].

"csúnya forciklus"
4 timesRepeat: [
    'lol' displayNl.
].

"szép forciklus"
1 to: 5 do: [:i| "ciklusváltozó, paraméteres blokk"
    i displayNl.
].

"enhanced forciklus"
10 to: 1 by: -2 do: [:i|
    i displayNl.
].