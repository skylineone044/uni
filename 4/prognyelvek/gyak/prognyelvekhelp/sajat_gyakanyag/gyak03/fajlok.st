"Karakterstatisztika"
|input stat|
input := FileStream open: 'be.txt' mode: (FileStream read).

stat:= Bag new.

input do: [:ch|
    stat add: ch.
].

stat displayNl.
