"Típus vizsgálat"
42 class displayNl.

('asd' isMemberOf: String) displayNl.

"Sajat osztaly letrehozasa"
Object subclass: Tort [
    |szamlalo nevezo|

    "Inicializalo uzenet --- NEM KIBASZOTT KONSTRUKTOR :'("
    init: sz init: n [
        szamlalo := sz.
        nevezo := n.
        "Inicializalo uzenet visszaadja az aktualis objektumot"
        ^self.
    ]

    "Szamlalo getter"
    szamlalo [
        ^szamlalo.
    ]

    "Nevezo getter"
    nevezo [
        ^nevezo.
    ]

    "Szorzas muvelet"
    * masik [
        |sz n tort|
        sz := szamlalo * (masik szamlalo).
        n := nevezo * (masik nevezo).
        tort := Tort new: sz new: n.
        ^tort.
    ]

    "toString metodus"
    printOn: stream [
        $( displayOn: stream.
        szamlalo printOn: stream.
        $/ displayOn: stream.
        nevezo printOn: stream.
        $) printOn: stream.
    ]
].

"Konstruktor"
Tort class extend [
    new: sz new: n [
        |obj|
        "Os konstruktor hivasa"
        obj := super new.
        "Inicializalas"
        obj init: sz init: n.
        "Return"
        ^obj.
    ]
].

|t1 t2|
t1 := Tort new: 2 new: 3.
t2 := Tort new: 3 new: 5.

(t1 * t2) displayNl.