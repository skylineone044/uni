"Tomb"
'Tombok: ' displayNl.

|a|
a := Array new: 5.
"1-tol indexel :("
a at: 1 put: 'asd'.

a at: 3 put: 42.

(a at: 1) displayNl.
(a at: 1) printNl.
a size displayNl.
a displayNl!

"Halmaz"
'Halmazok: ' displayNl.

|s|
s := Set new.
s add: 1.
s add: 'asd'.
s add: $x.
s remove: 1.
(s includes: $x) displayNl.
s displayNl!

"Multihalmaz"
'Szatyor: ' displayNl.

|b|
b := Bag new.
b add: 1.
b add: $x withOccurrences: 3.
b remove: $x. "Csak egy elemet vesz ki, 2 tovabbi marad"
b displayNl!

"Map / asszociativ tomb"
'Map: ' displayNl.

|d|
d := Dictionary new.
d at: 1 put: 'egy'.
d at: 'egy' put: 1.

(d at: 'egy') displayNl.
d displayNl.