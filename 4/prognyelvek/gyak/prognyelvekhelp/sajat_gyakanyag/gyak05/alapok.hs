{-
    deklaráció:
    fvnév::param1Típus->param2Típus->...->kimenetTípus
    kimenet: utolsó argumentum

    definíció:
    fvnév param1 param2 = returnValue
-}

add::Int->Int->Int
add x y = x + y

duplaz::Int->Int
duplaz 5 = 1
duplaz x = x + x

-- If (feltételes kifejezés)
factorial::Int->Int
factorial x = if x == 0
    then 1
    else x * (factorial(x-1))

-- Boolean guards
factorial2::Int->Int
factorial2 x | x == 0 = 1
             | x > 0 = x * (factorial2(x-1))

-- Case
factorial3::Int->Int
factorial3 x = case x of
    0 -> 1
    x -> x * (factorial3(x-1))

-- Mintaillesztés
factorial4::Int->Int
factorial4 0 = 1
factorial4 x = x * (factorial4(x-1))

size::Int
size = 13 + 72 -- == 85
{-
    size    == 85
    size -5 == 80
    size +3 == 88
-}

square::Int->Int
square x = x*x

exOR::Bool->Bool->Bool
exOR True y = not(y)
exOR False y = y