xminmax::Int->Int->(Int,Int)
xminmax x y 
    | x > y = (x, y)
    | otherwise = (y, x)

-- Tipusdefinicio
type Pair = (Int,Int)

minmax::Int->Int->Pair
minmax x y = (min x y, max x y)

-- Random bs
foo::Int->Int
foo x = if x == 0 then 0
        else x*x + foo(x-1)


{-
    LISTAK
    (h:t) head-tail felbontas
    pl.:
    [0, 1, 2, 3]
        h: 0
        t: [1, 2, 3]
    [2]
        h: 2
        t: []
    konkatenacio: ++
    osszevonas:   :
-}

{-
    [a]: Polimorf lista
    rekurziv length fgv
-}
size::[a]->Int
size [] = 0
size (h:t) = 1 + size(t)

{-
    Num a=>[a]: Tipusmegszoritas, a lista csak numerikus ertekeket tartalmazhat
-}
xsum::Num a=>[a]->a
xsum [] = 0
xsum (h:t) = h + sum(t)