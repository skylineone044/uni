{-
    Vektorműveletek
    (1,2) + (3,4) = (4,6)
-}

--add :: (Int, Int) -> (Int, Int) -> (Int, Int)
--add (a, b) (c, d) = (a+c, b+d)

-- Tipusdefiniciok (tipusszinonima)
type Pair = (Int, Int)
type Egesz = Int
add :: Pair -> Pair -> Pair
add (a, b) (c, d) = (a+c, b+d)


{-
    Sikidomok
    data: adatstruktura, alternativakat kell megadni
    bizonyos tipus milyen alternativ ertekeket vehet fel
-}

data Sikidom = 
    Teglalap Float Float |
    Negyzet Float |
    Kor Float

-- Kerulet
ker :: Sikidom -> Float
ker (Teglalap a b) = 2*(a+b)
ker (Negyzet a) = 4*a
ker (Kor r) = 2*r*3.1415