-- Generikusan típusolt fák
data Tree t =
    Leaf t |
    Node t (Tree t) (Tree t)

-- Preorder bejaras
preorder :: Tree a -> [a]
preorder (Leaf x) = [x]
preorder (Node x left right) = [x] ++ preorder left ++ preorder right 
-- Meghivasa: (Node 2 (Leaf 1) (Node 3 (Leaf 4) (Leaf 5)))