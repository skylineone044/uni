-- Listak osszege
lsum :: [Float] -> Float
lsum [] = 0
lsum (h:t) = h + (lsum t)

-- Listak hossza
hossz :: [Float] -> Int
hossz [] = 0
hossz (h:t) = 1 + (hossz t)

-- Atlagszamitas
-- Hossz szamitas kompakt modon
atlag :: [Float] -> Float
atlag l = (lsum l) / (hossz l)
    where 
        hossz [] = 0
        hossz(h:t) = 1 + hossz t

{-
    Task: szedjük ki egy listából az első N darab számot
-}

elson :: Int -> [Int] -> [Int]
elson _ [] = []
elson 0 _ = []
elson n (h:t) = [h] ++ elson (n-1) t