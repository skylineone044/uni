%Tények:
%predikátum(változó).

woman(mia).

%Szabályok: :- (akkor X ha Y == X :- Y)
hasPeriod(mia) :- woman(mia).

%trace utasítás: mi hív mit

%EDDIG: "tény" akkor, ha "tény"
%vesszővel elválasztott tények: ÉS
%pontosvesszővel elválasztott tények: VAGY


szulo(p, cs).
szulo(e, p).
szulo(j, cs).
szulo(g, e).

nagyszulo(X, Y):-szulo(X, Z), szulo(Z, Y).