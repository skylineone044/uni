% elem törlése listából
del([H|T], H, T).
del([H|T], E, [H|T2]) :- del(T, E, T2).

% két lista összefűzése
append([], [], []).
append([], L, L).
append(L, [], L).
append([H|L1], L2, [H|L3]) :- append(L1, L2, L3).