% CYKA listák BLYAT
% LISTÁÁÁÁÁÁÁK OwO

% Listák kinézete: [1, 2, 3] [a, b, c]
% gáz: nincs vezérlési szerkezet :( csak igaz-hamis és rekurzió
% van head-tail felbontás tho

kiir([]). % megállási feltétel, ofc azonosan igaz
kiir([E]) :- write(E).
kiir([H|T]) :- write(H), write(', '), kiir(T).

% első elem keresése
% elso([H|T], E) :- H = E. ez így szép, de van szebb megoldás is:
elso([H|T], H).

%utolsó elem keresése
utolso([E], E).
utolso([H|T], E) :- utolso(T, E).

% szabályhalmaz, ami egészekből álló lista elemeit szummázza
sum([], 0). % nincs visszatérési érték, szabályok vannak
sum([H|T], Sum) :- sum(T, SumT), Sum is H + SumT.

% listának eleme-e egy adott érték
eleme([E], E).
eleme([H|T], E) :- H = E ; eleme(T, E).

% valami ZH feladat

relacio([], []).
relacio([H1, T1], [H2, T2]):- H1 < 0, relacio(T1, T2).
relacio([H1, T1], [H2, T2]):- H1 >= 0, H2 is H1 + H1,  relacio(T1, T2).