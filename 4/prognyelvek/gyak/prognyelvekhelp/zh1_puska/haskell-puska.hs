-- Lista kezelés: lista elmeinek átlaga
atlag :: [Float] -> Float
atlag ls = (ossz ls) / (hossz ls)
  where
    ossz [] = 0
    ossz (h:t) = h + ossz t
    hossz [] = 0
    hossz (_:t) = 1 + hossz t

-- Polimorf típusok/függvények: tetszőleges típusú elemekből álló lista hossza
polisize :: [a] -> Int
polisize [] = 0
polisize (h:t) = 1 + polisize t

-- Típusdefiníció: String és Float típusokból álló pár
type StrFlt = (String,Float)

-- Olyan függvény, amely a fenti pár második elemét vizsgálja és visszaadja a pár első elemét
which :: StrFlt -> StrFlt -> String
which (s1,f1) (s2,f2)
  | f1 < f2 = s1
  | otherwise = s2

-- Valódi típusok
data Sikidom = Teglalap Float Float | Negyzet Float | Kor Sugar

kerulet :: Sikidom -> Float
kerulet (Teglalap a b) = 2 * (a + b)
kerulet (Negyzet a) = 4 * a
kerulet (Kor a) = 2 * a * 3.14
