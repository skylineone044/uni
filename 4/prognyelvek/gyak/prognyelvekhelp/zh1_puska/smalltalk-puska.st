"Vezérlési szerkezetek"
"Feltételes elágazás"
|k| k := 2.
k > 0 ifTrue: ['k pozitiv' printNl] ifFalse: ['k negativ' printNl]!

"Kezdőfeltételes ciklus (while)"
|x| x := 2.
[x < 20] whileTrue: [x printNl. x := x + 2]!

"Ismétlés"
3 timesRepeat: ['Hello' printNl]!

"Számlálásos vezérlés"
1 to: 5 by: 3 do: [:x | x printNl]! "a by: elhagyható, ilyenkor a +1 lesz a lépésköz"


"Collections"
"minden collection megengedi, hogy a tárolt elemek különböző típusúak legyenek,"
"tehát pl. egy halmazban lehet egyszerre szöveg és szám is"

"Array"
|a|
a := Array new: 20.       "20 elemű tömb létrehozása"
a displayNl.
(a at: 1) displayNl.      "az első elemet írja ki, ami most 'nil', a tömböket 1-től indexeljük "
a at: 1 put: 99.          "legyen 99 az első helyen"
a at: 2 put: 1.           "legyen 1 a második helyen"
(a at: 1) displayNl.
((a at: 1) + 1) displayNl.
(a at:1) + (a at:2) printNl!
a size printNl.           "a tömb mérete"
(a at: 21) printNl.       "tömbtúlindexelés, az interpreter szól"

"Set"
a := Set new.             "üres halmaz létrehozása"
a printNl.
a add: 5. a add: 7. a add: 'foo'.
a add: 9; add: 10; add: 'bar'.              "A ';' működése: az üzenetet annak az objektumnak küldi el, aminek a legutolsót küldte. Spórolás hosszú változóneveknél..."
a printNl.
a remove: 5.               "Az 5-ös kivétele"
a printNl.
a includes: 5.             "Tartalmazás vizsgálata -> true vagy false"
a includes: 7.
a addAll: #(4 5 66)        "kollekció összes elemének hozzáadása a halmazhoz"
a printNl.

"Bag - multiset, azaz egy elem többször is előfordulhat a halmazban"
|b|
b := Bag new.
b add: 'Hello'.
b add: 'World' withOccurrences: 3. "a 'World' legyen benne a halmazban háromszor"
b printNl.

"Dictionary - asszociatív tömb (map), azaz kulcs-érték párok listája"
d := Dictionary new.
d at: 'One' put: 1.           "a kulcs és az érték is tetszőleges objektum lehet"
d at: 'Two' put: 2.
d at: 1 put: 'One'.
d at: 2 put: 'Two'.
d printNl.
(d at: 1) printNl.            "nézzük meg milyen érték van eltárolva az 1-es kulcshoz"
(d at: 'Two') printNl.


"Osztályok"
"Tört osztály (részlet)"
Object subclass: Tort [ "az Object lesz az ős"
  |szamlalo nevezo|     "adattagok definiálása"

  init: sz init: n [    "kettő paraméteres kulcsszavas üzenet"
    szamlalo := sz.     "a számláló adattag beállítása az első paraméter alapján"
    nevezo := n.
    self egyszerusit.   "a biztonság kedvéért kapásból csinálunk egy egyszerűsítést"
    ^self.              "visszaadjuk a felinicializált objektumot (self == this Java-ban)"
  ]

  egyszerusit [         "unáris üzenet"
    |lnko|
    lnko := szamlalo gcd: nevezo.
    szamlalo := szamlalo // lnko.
    nevezo := nevezo // lnko.
  ]

  * masik [             "bináris üzenet"
    |eredm sz n|
    sz := szamlalo * masik szamlalo.
    n := nevezo * masik nevezo.
    eredm := Tort new: sz new: n.
    eredm egyszerusit.
    ^eredm.
  ]

]

Tort class extend [        "az osztályt egészítjük ki (osztály változó, konstruktor, stb.)"
  valtozo := "ertek".      "osztály változó"

  setValtozo: ujErtek [    "egy paraméteres kulcsszavas osztály üzenet"
    valtozo := ujErtek.    "osztály változó beállítása a paraméter alapján"
  ]
]

"Példa a Tort osztály használatára"
|t1 t2|
t1 := Tort new: 2 new: 3.
t2 := Tort new: 3 new: 4.
(t1 * t2)!
