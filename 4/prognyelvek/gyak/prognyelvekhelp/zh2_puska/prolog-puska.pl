% Határozzuk meg, hogy egy elem benne van-e az adott listában!
member([E|_],E).
member([_|T],X) :- member(T,X).

% Maradék képzés (147-nek 3-mal és 5-tel vett maradéka lesz M1 és M2 értéke)
M1 is mod(147,3). % --> M1 = 0
M2 is mod(147,5). % --> M2 = 2
