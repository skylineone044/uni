#!/bin/bash

# Execution: source env.sh

RED='\033[0;31m'
NC='\033[0m'

echo -e "${RED}Irinyi Kabinet WEB-DEVELOPMENT FRAMEWORKS course environment creator${NC}"
echo -e "${RED}Installing Angular CLI ...${NC}"
npm install -g @angular/cli

echo -e "${RED}Cloning NVM and setting up environment variables...${NC}"
export NVM_DIR="$HOME/.nvm" && (
  git clone https://github.com/nvm-sh/nvm.git "$NVM_DIR"
  cd "$NVM_DIR"
  git checkout `git describe --abbrev=0 --tags --match "v[0-9]*" $(git rev-list --tags --max-count=1)`
) && \. "$NVM_DIR/nvm.sh"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

echo -e "${RED}Installing NodeJS 12.22.1${NC}"
nvm install 12.22.1

echo -e "${RED}Checking current NodeJS version${NC}"
node -v
