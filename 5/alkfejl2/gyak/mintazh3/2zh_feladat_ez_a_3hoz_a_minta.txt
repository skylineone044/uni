Roxfort Boszorkány és Varázslókezelő

Roxfortban is szükséges nyilvántartani az iskolába felvett diákokat és az ott dolgozó személyzetet. A csak erre a célra felállított "Nyilvántartásért Felelős Operatív Bizottság" megkérte az IB570G-2 hallgatóit, hogy készítsék el ezt az alkalmazást. A verseny végén a legjobb lesz a Roxfort hivatalos alkalmazása.

A feladatkiírás részletesen
    Egy varázslókezelő program elkészítése C# nyelven, WinForms környezetben, mely követi az MVC modellt,
    Az alkalmazás egy, már használt SQLite adatbázishoz csatlakozzon, melynek kezelését biztosítja,
    Az adatbázis sémáját tartalmazó pergament a négy alapító elégette, így csak a "hogwarts.txt" adatbázis fájl szolgálhat információval.

Az alkalmazásnak a következő funkcionalitásokat kell támogatnia.

Varázslók listázása
    A sikeres csatlakozás után listázd ki az összes rekordot az adatbázisból,
    Egy ComboBox segítségével ki lehet választani, hogy mely ház varázslóit listázzuk, majd a kiválasztás hatására történjen meg a művelet
    A ComboBox-ban található lehetőségeket az adatbázisban található házakat reprezentálják (Ne legyen beégetve.),
    Továbbá legyen egy "Mind" ("All") opció is, ami az összes elemet listázza,
    A lista egy DataGridView-be jelenjen meg,
    Az ID ne látszódjon.

Varázsló hozzáadása

A hozzáadás egy új ablakban jelenjen meg, melyet menün keresztül lehet elérni.
    Egy új varázsló érkezésénél a Teszlek Süveg beosztja őt egy házba véletlenszerűen,
    A beosztás eredményét egy MessageBox segítségével tudasd a felhasználóval,
    Amennyiben egy dolgozót rögzítünk, akkor a teszleg süveg a "Stuff" kategóriába sorolja be fixen,
    A házak előre definiátak az adatbázisban.

A varázslók felvételénél az adatbázisban található séma minden -- nem automatikusan generált -- elemét ki kell tölteni.

Varázslók módosítása
    A listában egy varázslóra kattintva módosítható legyen.
    A hozzáadás albakot újrafelhasználva.
    A DataGridView-ban ne lehessen csak úgy átírni az értékeket.
    Módosítható elem: név, magasság.

Egyéb

Továbbá, annak érdekében, hogy más varázslóiskolák is használni tudják az alkalmazást, az adatbázis elérési útvonalát a felhasználóra bízza. Legyen egy beviteli mező a főoldalon, melybe be lehet másolni az elérési útvonalat és mellette egy "Csatlakozás" gomb. A listázás, hozzáadás és módosítás funkcionalitás csak akkor működjön, ha már a felhasználó sikeresen csatlakozott egy adatbázishoz.

Egy kezdetleges "terv" megtalálható a kurzus dokumentumai között, melyet az "Nyilvántartásért Felelős Operatív Bizottság" készített, "02-ZH-UI" fül alatt. Nem kötelező ezt követni, a lényeg, hogy minden funkcionalitás meglegyen.

Mit kell leadni tömörítve? (Struktúra következik)
    Solution.sln
    Project Mappa/Project.csproj, project fájlok!
    Project Mappából az obj és a bin mappák törölendők leadás előtt.
