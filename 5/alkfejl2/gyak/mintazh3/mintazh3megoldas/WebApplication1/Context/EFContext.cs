﻿using Microsoft.EntityFrameworkCore;
using WebApplication1.Models;

namespace WebApplication1.Context
{
    public class EFContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=.//DB//hogwarts.db");
        }
        public DbSet<WebApplication1.Models.Wizard> Wizards { get; set; }
        public DbSet<WebApplication1.Models.House> Houses { get; set; }
    }
}
