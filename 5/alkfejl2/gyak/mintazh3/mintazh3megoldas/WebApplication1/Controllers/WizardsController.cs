﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Context;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class WizardsController : Controller
    {
        private readonly EFContext _context;

        public WizardsController(EFContext context)
        {
            _context = context;
        }

        // GET: Wizards
        public async Task<IActionResult> Index()
        {
            var eFContext = _context.Wizards.Include(w => w.ReferencedHouse);
            return View(await eFContext.ToListAsync());
        }

        // GET: Wizards/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Wizards == null)
            {
                return NotFound();
            }

            var wizard = await _context.Wizards
                .Include(w => w.ReferencedHouse)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (wizard == null)
            {
                return NotFound();
            }

            return View(wizard);
        }

        // GET: Wizards/Create
        public IActionResult Create()
        {
            ViewData["HouseId"] = new SelectList(_context.Set<House>(), "ID", "Name");
            return View();
        }

        // POST: Wizards/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,Height,PureBlood,HouseId")] Wizard wizard)
        {
            if (ModelState.IsValid)
            {
                _context.Add(wizard);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["HouseId"] = new SelectList(_context.Set<House>(), "ID", "Name", wizard.HouseId);
            return View(wizard);
        }

        // GET: Wizards/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Wizards == null)
            {
                return NotFound();
            }

            var wizard = await _context.Wizards.FindAsync(id);
            if (wizard == null)
            {
                return NotFound();
            }
            ViewData["HouseId"] = new SelectList(_context.Set<House>(), "ID", "Name", wizard.HouseId);
            return View(wizard);
        }

        // POST: Wizards/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,Height,PureBlood,HouseId")] Wizard wizard)
        {
            if (id != wizard.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(wizard);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WizardExists(wizard.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["HouseId"] = new SelectList(_context.Set<House>(), "ID", "Name", wizard.HouseId);
            return View(wizard);
        }

        // GET: Wizards/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Wizards == null)
            {
                return NotFound();
            }

            var wizard = await _context.Wizards
                .Include(w => w.ReferencedHouse)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (wizard == null)
            {
                return NotFound();
            }

            return View(wizard);
        }

        // POST: Wizards/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Wizards == null)
            {
                return Problem("Entity set 'EFContext.Wizard'  is null.");
            }
            var wizard = await _context.Wizards.FindAsync(id);
            if (wizard != null)
            {
                _context.Wizards.Remove(wizard);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool WizardExists(int id)
        {
          return _context.Wizards.Any(e => e.ID == id);
        }
    }
}
