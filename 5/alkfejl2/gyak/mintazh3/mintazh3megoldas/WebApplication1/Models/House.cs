﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class House
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public String Name { get; set; }
    }
}
