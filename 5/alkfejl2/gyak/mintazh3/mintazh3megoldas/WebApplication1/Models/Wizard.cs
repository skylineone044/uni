﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Linq;

namespace WebApplication1.Models
{
    public class Wizard
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public String Name  { get; set; }
        [Required]
        public int Height  { get; set; }
        [Required]
        public bool PureBlood  { get; set; }
        [Required]
        [Display(Name = "House")]
        [ForeignKey("ReferencedHouse")]
        public int HouseId { get; set; }
        public virtual House ReferencedHouse { get; set; }
    }
}
