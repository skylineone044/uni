﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                try
                {
                    string[] input = Console.ReadLine().Split(' ');
                    if (input[0] == "+")
                    {
                        Console.WriteLine(Int64.Parse(input[1]) + Int64.Parse(input[2]));
                    }
                    else if (input[0] == "-")
                    {
                        Console.WriteLine(Int64.Parse(input[1]) - Int64.Parse(input[2]));
                    }
                    else if (input[0] == "*")
                    {
                        Console.WriteLine(Int64.Parse(input[1]) * Int64.Parse(input[2]));
                    }
                    else if (input[0] == "/")
                    {
                        Console.WriteLine(Int64.Parse(input[1]) / Int64.Parse(input[2]));
                    }
                    else if (input[0] == "exit")
                    {
                        return;
                    }
                    else
                    {
                        throw new Exception("rossz muvelet");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Rossz formatum, helyes pl. + 5 4");
                }
            }

        }
    }
}
