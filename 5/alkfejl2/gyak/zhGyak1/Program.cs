﻿using CsvHelper;
using CsvHelper.Configuration;
using System.Globalization;
using System.Text;

namespace zhGyak1
{    class Program
    {
        public static void Main(string[] args)
        {
            var records = LoadDataset(args[0]);


            var task1 = records
                .GroupBy(x => x.City)
                .Select(grop => new
                {
                    City = grop.Key,
                    Count = grop.Count()
                })
                .OrderByDescending(grop => grop.Count);
            /*
            foreach (var record in task1)
            {
                Console.WriteLine(record);
            }*/

            var tarsk2 = records
                .Where(r => r.DurationInSeconds >= 120 && r.DateTime >= new DateTime(2000, 1, 1, 12, 0, 0))
                .OrderByDescending(r => r.DurationInSeconds);
            /*
            foreach (var record in tarsk2)
            {
                Console.WriteLine(record);
            }*/

            var task3 = records
                .Where(r => r.Country != String.Empty)
                .GroupBy(r => r.Country)
                .OrderByDescending(r => r.Count())
                .Take(5)
                .Select(g => new {
                    Count = g.Count(),
                    Country = g.Key
                });
            /*
            foreach (var record in task3)
            {
                Console.WriteLine(record);
            }*/

            var task4 = records
                .Where(r => r.DateTime >= new DateTime(2000, 1, 1) && r.DateTime <= new DateTime(2010, 12, 17))
                .OrderBy(r => r.DateTime);
            /*
            foreach (var record in task4)
            {
                Console.WriteLine(record);
            }*/

            var task5 = records
                .Where(r => r.Shape != string.Empty)
                .GroupBy(records => records.Shape)
                .Select(g => new
                {
                    Shape = g.Key,
                    Count = g.Count()
                });
            /*
            foreach (var record in task5)
            {
                Console.WriteLine(record);
            }*/
            
        }



        private static IEnumerable<Ufo> LoadDataset(string csv_path)
        {
            if (!File.Exists(csv_path))
            {
                Console.WriteLine("Rossz path!");
                Environment.Exit(1);
            }

            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                Encoding = Encoding.UTF8,
                Delimiter = ","
            };

            using var reader = new StreamReader(csv_path);
            using var csvReader = new CsvReader(reader, config);

            var records = new List<Ufo>();

            csvReader.Read();
            csvReader.ReadHeader();

            while (csvReader.Read())
            {
                string datetime_str = csvReader.GetField<string>("DateTime");
                if (!DateTime.TryParse(datetime_str, out DateTime datetime))
                {
                    Console.WriteLine("Wrong date format");
                    continue;
                }

                string latitude_str = csvReader.GetField<string>("Latitude");
                if (!Double.TryParse(latitude_str, out double latitude))
                {
                    Console.WriteLine("Wrong latitude format");
                    continue;
                }

                string longitude_str = csvReader.GetField<string>("Longitude");
                if (!Double.TryParse(longitude_str, out double longitude))
                {
                    Console.WriteLine("Wrong latitude format");
                    continue;
                }

                var record = new Ufo
                {
                    DateTime = datetime,
                    City = csvReader.GetField<string>("City"),
                    State = csvReader.GetField<string>("State"),
                    Country = csvReader.GetField<string>("Country"),
                    Shape = csvReader.GetField<string>("Shape"),
                    DurationInSeconds = csvReader.GetField<double>("DurationInSeconds"),
                    Latitude = latitude,
                    Longitude = longitude,
                };

                records.Add(record);
            }
            return records;
        }
    }
}
