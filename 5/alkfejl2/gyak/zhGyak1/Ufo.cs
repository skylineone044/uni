﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zhGyak1
{
    internal class Ufo
    {
        // DateTime,City,State,Country,Shape,DurationInSeconds,Latitude,Longitude
        // 1949-10-10 20:30,san marcos, tx, us, cylinder,2700,29.8830556,-97.9411111
        public DateTime DateTime;
        public string City;
        public string State;
        public string Country;
        public string Shape;
        public double DurationInSeconds;
        public double Latitude;
        public double Longitude;

        public override string ToString()
        {
            return $"Sighting: {DateTime} - {City}, {State}, {Country}: {Shape}, {DurationInSeconds}; {Longitude}, {Latitude}";
        }
    }
}
