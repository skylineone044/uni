﻿using Microsoft.Data.Sqlite;
using System.ComponentModel;
using WinFormsApp1.DAO;
using WinFormsApp1.Model;

namespace Heroes.DAO
{
    public class ADODao : IDAO
    {
        private string connectionString = @"Data Source=../../../DB/utazas.db";

        public void Add(Utazas utazas)
        {
            using var connection = new SqliteConnection(connectionString);
            connection.Open();

            var command = connection.CreateCommand();
            command.CommandText = "insert into Items"
                + " (Name, Category, Country, Description, Priority) values"
                + " (@name, @category, @country, @description, @priority)";

            command.Parameters.Add("name", SqliteType.Text).Value = utazas.Name;
            command.Parameters.Add("category", SqliteType.Text).Value = utazas.Category;
            command.Parameters.Add("country", SqliteType.Integer).Value = utazas.Country;
            command.Parameters.Add("description", SqliteType.Text).Value = utazas.Description;
            command.Parameters.Add("priority", SqliteType.Integer).Value = utazas.Priority;

            command.ExecuteNonQuery();
        }

        public List<string> GetCategories()
        {
            var categories = new List<string>();

            using var connection = new SqliteConnection(connectionString);
            connection.Open();

            var command = connection.CreateCommand();
            command.CommandText = "select Category from Items";

            var reader = command.ExecuteReader();
            while (reader.Read())
            {
                categories.Add(reader.GetString(reader.GetOrdinal("Category")));
            }

            return categories;
        }

        /*public IEnumerable<Utazas> Get()
        {
            var utazasok = new List<Utazas>();

            using var connection = new SqliteConnection(connectionString);
            connection.Open();

            var command = connection.CreateCommand();
            command.CommandText = "select * from Items order by Priority desc";
            
            var reader = command.ExecuteReader();
            while (reader.Read())
            {
                //reader.GetOrdinal("oszlopnev")

                var utazas = new Utazas
                {
                    ID = reader.GetInt32(reader.GetOrdinal("ID")),
                    Name = reader.GetString(reader.GetOrdinal("Name")),
                    Category = reader.GetString(reader.GetOrdinal("Category")),
                    Country = reader.GetString(reader.GetOrdinal("Country")),
                    Description = reader.GetString(reader.GetOrdinal("Description")),
                    Priority = reader.GetInt32(reader.GetOrdinal("Priority")),
                };
                utazasok.Add(utazas);
            }

            return utazasok;
        }*/

        public IEnumerable<Utazas> Get(string category)
        {
            var utazasok = new List<Utazas>();

            using var connection = new SqliteConnection(connectionString);
            connection.Open();

            var command = connection.CreateCommand();

            if (category == string.Empty)
            {
                command.CommandText = "select * from Items order by Priority desc";
            }
            else
            {
                command.CommandText = "select * from Items where Category=@categ order by Priority desc";
                command.Parameters.Add("categ", SqliteType.Text).Value = category;
            }
            var reader = command.ExecuteReader();
            while (reader.Read())
            {
                //reader.GetOrdinal("oszlopnev")

                var utazas = new Utazas
                {
                    ID = reader.GetInt32(reader.GetOrdinal("ID")),
                    Name = reader.GetString(reader.GetOrdinal("Name")),
                    Category = reader.GetString(reader.GetOrdinal("Category")),
                    Country = reader.GetString(reader.GetOrdinal("Country")),
                    Description = reader.GetString(reader.GetOrdinal("Description")),
                    Priority = reader.GetInt32(reader.GetOrdinal("Priority")),
                };
                utazasok.Add(utazas);
            }

            return utazasok;
        }
    }
}
