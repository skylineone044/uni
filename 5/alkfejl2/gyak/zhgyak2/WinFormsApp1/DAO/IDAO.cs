﻿using WinFormsApp1.Model;

namespace WinFormsApp1.DAO
{
    public interface IDAO
    {
        void Add(Utazas hero);

        List<string> GetCategories();
        IEnumerable<Utazas> Get(string category);
    }
}
