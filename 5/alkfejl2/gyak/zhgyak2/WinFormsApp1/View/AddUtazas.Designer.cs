﻿namespace WinFormsApp1.View
{
    partial class AddUtazas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private TextBox NevTB;
        private TextBox KategoriaTB;
        private TextBox OrszagTB;
        private TextBox LeirasTB;
        private NumericUpDown PrioritasNUD;
        private Button OKBTN;
        private Button CancelBTN;
    }
}