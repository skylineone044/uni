﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinFormsApp1.DAO;

namespace WinFormsApp1.View
{
    public partial class AddUtazas : Form
    {
        IDAO dao;
        public AddUtazas(IDAO dao)
        {
            this.dao = dao;
            InitializeComponent();
        }

        private void OnSaveUtazas(object sender, EventArgs e)
        {
            dao.Add(new Model.Utazas
            {
                Name = NevTB.Text,
                Category = KategoriaTB.Text,
                Country = OrszagTB.Text,
                Description = LeirasTB.Text,
                Priority = (int)PrioritasNUD.Value
            });
            this.DialogResult = DialogResult.OK;
        }

        private void OnCancelBTNClick(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.NevTB = new System.Windows.Forms.TextBox();
            this.KategoriaTB = new System.Windows.Forms.TextBox();
            this.OrszagTB = new System.Windows.Forms.TextBox();
            this.LeirasTB = new System.Windows.Forms.TextBox();
            this.PrioritasNUD = new System.Windows.Forms.NumericUpDown();
            this.OKBTN = new System.Windows.Forms.Button();
            this.CancelBTN = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.PrioritasNUD)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(52, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nev";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Kategoria";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Orszag";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(43, 159);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Leiras";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(37, 200);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Priritas";
            // 
            // NevTB
            // 
            this.NevTB.Location = new System.Drawing.Point(86, 36);
            this.NevTB.Name = "NevTB";
            this.NevTB.Size = new System.Drawing.Size(218, 23);
            this.NevTB.TabIndex = 5;
            // 
            // KategoriaTB
            // 
            this.KategoriaTB.Location = new System.Drawing.Point(86, 73);
            this.KategoriaTB.Name = "KategoriaTB";
            this.KategoriaTB.Size = new System.Drawing.Size(218, 23);
            this.KategoriaTB.TabIndex = 6;
            // 
            // OrszagTB
            // 
            this.OrszagTB.Location = new System.Drawing.Point(86, 114);
            this.OrszagTB.Name = "OrszagTB";
            this.OrszagTB.Size = new System.Drawing.Size(218, 23);
            this.OrszagTB.TabIndex = 7;
            // 
            // LeirasTB
            // 
            this.LeirasTB.Location = new System.Drawing.Point(86, 156);
            this.LeirasTB.Name = "LeirasTB";
            this.LeirasTB.Size = new System.Drawing.Size(218, 23);
            this.LeirasTB.TabIndex = 8;
            // 
            // PrioritasNUD
            // 
            this.PrioritasNUD.Location = new System.Drawing.Point(86, 198);
            this.PrioritasNUD.Name = "PrioritasNUD";
            this.PrioritasNUD.Size = new System.Drawing.Size(120, 23);
            this.PrioritasNUD.TabIndex = 9;
            // 
            // OKBTN
            // 
            this.OKBTN.Location = new System.Drawing.Point(268, 253);
            this.OKBTN.Name = "OKBTN";
            this.OKBTN.Size = new System.Drawing.Size(75, 23);
            this.OKBTN.TabIndex = 10;
            this.OKBTN.Text = "OK";
            this.OKBTN.UseVisualStyleBackColor = true;
            this.OKBTN.Click += new System.EventHandler(this.OnSaveUtazas);
            // 
            // CancelBTN
            // 
            this.CancelBTN.Location = new System.Drawing.Point(23, 253);
            this.CancelBTN.Name = "CancelBTN";
            this.CancelBTN.Size = new System.Drawing.Size(75, 23);
            this.CancelBTN.TabIndex = 11;
            this.CancelBTN.Text = "Cancel";
            this.CancelBTN.UseVisualStyleBackColor = true;
            this.CancelBTN.Click += new System.EventHandler(this.OnCancelBTNClick);
            // 
            // AddUtazas
            // 
            this.ClientSize = new System.Drawing.Size(355, 289);
            this.Controls.Add(this.CancelBTN);
            this.Controls.Add(this.OKBTN);
            this.Controls.Add(this.PrioritasNUD);
            this.Controls.Add(this.LeirasTB);
            this.Controls.Add(this.OrszagTB);
            this.Controls.Add(this.KategoriaTB);
            this.Controls.Add(this.NevTB);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AddUtazas";
            this.Text = "Add Utazas";
            ((System.ComponentModel.ISupportInitialize)(this.PrioritasNUD)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}
