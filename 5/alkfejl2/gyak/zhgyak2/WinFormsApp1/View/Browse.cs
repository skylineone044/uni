﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinFormsApp1.DAO;

namespace WinFormsApp1.View
{
    public partial class Browse : Form
    {
        IDAO dao;
        public Browse(IDAO dao)
        {
            this.dao = dao;
            InitializeComponent();
        }

        private void OnAddUtazas(object sender, EventArgs e)
        {
            var window = new AddUtazas(this.dao);
            window.ShowDialog();
        }

        private void OnListazasClick(object sender, EventArgs e)
        {
            var utazasok = dao.Get(this.CategoryCB.Text);

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = utazasok;
            dataGridView1.Visible = true;
        }

        private void OnCategoryCBClick(object sender, EventArgs e)
        {
            var categories = dao.GetCategories();
            categories.Add("");
            this.CategoryCB.DataSource = null;
            this.CategoryCB.DataSource = categories;
        }
    }
}
