CREATE TABLE Houses(
 ID integer not null PRIMARY key AUTOINCREMENT,
 Name text not null
);

CREATE TABLE Wizards(
 ID integer not null PRIMARY key AUTOINCREMENT,
 Name text not null,
 Height integer not null,
 PureBlood integer not null CHECK (PureBlood IN (0,1)),
 HouseId integer not null,
 FOREIGN KEY(HouseId)
 References Houses(ID),
 UNIQUE(Name)
);