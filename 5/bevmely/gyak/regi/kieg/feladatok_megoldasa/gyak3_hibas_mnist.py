#%%

# 3. Keressük meg, hogy mely képeken hibázott a háló!

print(test_images.shape)
print(mnist.test.images.shape)

input_fn_test_few = tf.estimator.inputs.numpy_input_fn(
    x={'images': mnist.test.images}, batch_size=batch_size, shuffle=False)
# Use the model to predict the images class
preds = list(model.predict(input_fn_test_few))

#%%

# Collecting falsely labeled entity indexes (indices)

dir(mnist.test)
mnist.test.labels

#mistakes = mistakes = np.zeros(1)
mistakes = []

for i in range(len(preds)):
    if preds[i] != mnist.test.labels[i]:
        mistakes = np.append(mistakes, i)        

import numpy as np
import tensorflow as tf

#%%    

# Creating empty array and changing\appending its values

mistakes = np.zeros(1)
print(mistakes)
# mistakes[0] = 1
# print(mistakes)
# mistakes = np.append(mistakes, 2)
# print(mistakes)
# mistakes.shape


#%%

# Predict wrong images
n_images = mistakes[:4]
mistakes = np.array(mistakes, dtype='int32')
# Get images from test set
test_images = mnist.test.images
# Prepare the input data
input_fn_test_few = tf.estimator.inputs.numpy_input_fn(
    x={'images': test_images}, shuffle=False)
# Use the model to predict the images class
preds = list(model.predict(input_fn_test_few))

# Display
for i in range(4):
    plt.imshow(np.reshape(test_images[mistakes[i]], [28, 28]), cmap='gray')
    plt.show()
    print("Model prediction:", preds[mistakes[i]])
    print("Label:", mnist.test.labels[mistakes[i]])

#%%