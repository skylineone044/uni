1.
gyok: 0..gyok K
gyoknegyzet: 1..K
plus: 3..kb gyok K koruli valameddig
szoval O(gyok K + K + gyok K), ami O(K)
K erteket ha tarolni akarunk, akkor log K bit fog kelleni hozza = n, szval verdict: O(n) tar
2.
itt ez rekurziv, szval az (f argjai + x) * call stack magassaga kellene

bejovo tomb n bites, hossza max n lehet (ha csupa 1 bites dolog van benne)
x az max n bites lehet, mert tombelem erteke (ha 1 db n bites, max meretu tombelem van)
A az valami szam, ami hasonlithato az x hez, az is n bit
i az tombindex, szval a 0..T.length kozott valami, szval max log n biten elfer

a rekurzio addig megy, ameddig i le nem er 0ra, mert A lassabban fog felerni x ig, minthogy i leerni, mert az A-t 1esevel novlejuk, de az i-bol 1-1 bitet csapunk le mindig az osztassal
az i pedig log i osztas utan er le, ha mind a log n mennyisegu bitje elfogyott
szoval O((log n) * n + n + log n), ami = O(n * log n) tar3.
itt az input egy szam, amekkora helyet kell foglalni
es a foglalt tomb foglalja csak a helyet ami minket erdekel
ahogy a 1 bittel nagyobb, ugy duplaakkora az erteke, egy az ertekenek megfelelve, dupla akkora hely lesz foglalva
2^n = A
szoval a tarigeny O(A * int_merete) = O((2^n) * int_merete) ahol int_merete nem lesz nagyobb mint 2^n, mert az inteket osszeadassal kapjuk, amivel max duplazni birjuk az intete, azaz 1 bittel novelni.
verdict: O(2^n)

3.
itt A az n meretu, es megadja, hogy mekkora tomb legyen foglalva
szoval A ertekenek megfelelo szamu int lesz foglalva, szoval O((2^n) * n), mert 2^n darab n meretu int lesz foglalva, de ordo miatt a verdict: O(2^n)

4.
itt nincs lokalis valtozo, csak a 2 parameter, ami a rekurzio mitatt szamit
a call stack x=A magas lesz, mert szelsoseges esetben A az egy nagy szam, a B=y meg 1, szoval egyesevel levonogatunk addig, ameddig valamelyik 0 nem lesz (mert van egy olyan ag, ahol megcsereli, az argokat, hogy x mindig a nagyobb legyen)
az n az meg log A + log B
szval O(argok bitjei * call stack melyseg) = O(n * 2^n), mert a call stack 2^n mely, lehet, mert pl x az n-1 bit, erteke 2^(n-1), y meg 1 bit (erteke 1), akkor kb 2^(n-1) kivonas fog kelleni, mire leerunk 0ra
szval verdict: O(2^n)

5.
f(n) = 2^n, mert az exponencialis idobe belefer a gyok tar
mert ea diasor page 43
mert a space(f(n))  <= time(k^f(n)), ahol mondjuk k lehet 2, f(n) meg mondjuk n, akkor space(n) <= time (2^n), es azt is tudjuk, hogy a gyok n belefer az n be

6.
igen, mert a log^2 n tarban lefuto algoritmus az a polinom tarban lefuto algoritmusok kozze tartozik, aminek a reszhalmaza a polinom idoben lefuto algoritmusok halmaza ( P <= PSPACE ) (imagine <= az a gorbe halmazos valtozat)
fokszam barmi lehet, nincs mibol becsulni
