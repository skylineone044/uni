///skyline,Tokodi.Mate@stud.u-szeged.hu

import java.util.*;

import com.sun.source.tree.BreakTree;
import game.quoridor.MoveAction;
import game.quoridor.QuoridorGame;
import game.quoridor.QuoridorPlayer;
import game.quoridor.WallAction;
import game.quoridor.players.DummyPlayer;
import game.quoridor.utils.PlaceObject;
import game.quoridor.utils.QuoridorAction;
import game.quoridor.utils.WallObject;

public class Agent extends QuoridorPlayer {
    private final GameBoard gameBoard;
    private final int enemy_color;

    private final int start_row;
    public final static boolean DEBUG = false;

    /**
     * Az agens
     *
     * @param i      sor
     * @param j      oszlop
     * @param color  szin
     * @param random random egyed
     */
    public Agent(int i, int j, int color, Random random) {
        super(i, j, color, random);
        enemy_color = 1 - color;
        this.start_row = i;
        this.gameBoard = new GameBoard(new DummyPlayer(i, j, color, random), new InternalBlockRandomPlayer(GameBoard.GameBoard_size - i, j, enemy_color, random), color);
    }

    /**
     * Ha az Agent.DEBUG igaz, kiirja a parameterben kapott String-et a kepernyore
     *
     * @param s A szoveg amit ki kell irni
     */
    public static void debugPrint(String s) {
        if (Agent.DEBUG) {
            System.out.println(s);
        }
    }

    /**
     * Ezzel kerdezi meg a keretrendszer az agenstol, hogy o mit lep
     *
     * @param prevAction     the last action of the enemy player or null iff no previous action available.
     * @param remainingTimes the overall remaining times of the players in the game
     * @return a lepes / fal lerakas objektumot
     */
    @Override
    public QuoridorAction getAction(QuoridorAction prevAction, long[] remainingTimes) {
        if (prevAction != null) {
            debugPrint(prevAction.toString());
        } else {
            debugPrint("no previous action");
        }
        debugPrint(Arrays.toString(remainingTimes));

        if (prevAction instanceof WallAction action) {
            gameBoard.addWall(new WallObject(action.i, action.j, action.horizontal));
        } else if (prevAction instanceof MoveAction action) {
            gameBoard.movePlayer(enemy_color, new PlaceObject(action.to_i, action.to_j));
        }

        PlaceObject next_step = this.gameBoard.findNextStepForShortesPathToOtherside(new PlaceObject(this.i, this.j));
        for (int k = 0; k < 2; k++) {
            if (Math.abs(gameBoard.initialPlayerPositions[k].j - this.j) == 1 && this.i == gameBoard.initialPlayerPositions[k].i) {
                if ((this.j == 3 && next_step.j == 5) || this.j == 5 && next_step.j == 3) {
                    return new MoveAction(i, j, next_step.i, 4);
                }
            }
        }
        MoveAction action = new MoveAction(i, j, next_step.i, next_step.j);
        return action;
    }

    /**
     * A palya
     */
    public class GameBoard {
        public static int GameBoard_size = 9;
        private final List<WallObject> walls = new ArrayList<>();
        private final QuoridorPlayer[] players = new QuoridorPlayer[2];
        private final int my_color;
        private final List<PlaceObject> possible_goal_positions;
        public final PlaceObject initialPlayerPositions[];

        private AStarNode[][] aStarNodeTileMap;

        /**
         * Elkesziti a palyat, ami tartalmazza a jatekosokat, falaktat, mezoket
         *
         * @param player1  az egyik jatekos
         * @param player2  a masik jatekos
         * @param my_color az agens szine
         */
        public GameBoard(QuoridorPlayer player1, QuoridorPlayer player2, int my_color) {
            this.players[player1.color] = player1;
            this.players[player2.color] = player2;
            this.my_color = my_color;

            // kijelolom a lehetseges celpontokat: ha felul kezd az agens akkor a lenti sor mezoi, ha lent kezt, akkor a felso sor mezoi
            debugPrint("Possible goals: ");
            this.possible_goal_positions = new ArrayList<>();
            for (int k = 0; k < GameBoard_size; k++) {
                possible_goal_positions.add(new PlaceObject(GameBoard_size - (this.players[my_color].i == 0 ? 1 : GameBoard_size), k));
                debugPrint(this.possible_goal_positions.get(k).toString());
            }
            // a belso palya reprezentacio
            this.aStarNodeTileMap = new AStarNode[GameBoard_size][GameBoard_size];
            for (int i = 0; i < GameBoard_size; i++) {
                for (int j = 0; j < GameBoard_size; j++) {
                    this.aStarNodeTileMap[i][j] = new AStarNode(new PlaceObject(i, j), null, this);
                }
            }
            // ezeken a poziciokon kezdenek a jatekosok
            this.initialPlayerPositions = new PlaceObject[2];
            this.initialPlayerPositions[0] = new PlaceObject(0, 4);
            this.initialPlayerPositions[1] = new PlaceObject(8, 4);
        }

        /**
         * Hozzaad egy falat a palyahoz
         *
         * @param wall a fal
         */
        public void addWall(WallObject wall) {
            this.walls.add(wall);
        }

        /**
         * Elmozgatja az egyik jatekost a megfeleo hleyre
         *
         * @param color   a jatekos szine
         * @param new_pos az uj pozicio
         */
        public void movePlayer(int color, PlaceObject new_pos) {
            this.players[color].i = new_pos.i;
            this.players[color].j = new_pos.j;
        }

        /**
         * Ellenorzi, hogy ervenyes-e az adott lepes
         *
         * @param from honnal lepunk
         * @param to   hova lepunk
         * @return hogy valid-e a lepes
         */
        public boolean canMoveTo(PlaceObject from, PlaceObject to) {
            return QuoridorGame.checkCandidateMove(from, to, walls, this.players);
        }

        /**
         * Megkeresi A*-al, hogy merre kell egyet lepni a masik oldal legkozelebbi mezojenek eleresehez
         *
         * @param start honnan induljon a kereses
         * @return a pozicio ahova lepni kell
         */
        public PlaceObject findNextStepForShortesPathToOtherside(PlaceObject start) {
            double min_distance = Double.MAX_VALUE;
            PlaceObject best_next_position = null;
            // vegigprobaljuk az osszes nem foglalt mezot a legfelso / legalso sorban
            for (PlaceObject possible_goal_pos : this.possible_goal_positions) {
                if (!QuoridorGame.isPlayerOn(this.players, possible_goal_pos)) {
                    try {
                        AStarNode aStarTarget = A_star(start, possible_goal_pos);
                        int dist = aStarTarget.stepsToGetHere;
                        if (dist < min_distance) {
                            min_distance = dist;
                            best_next_position = getNextAStarStep(aStarTarget);
                        }
                    } catch (Exception e) {
                        debugPrint("No path found to " + possible_goal_pos);
                        // do nothing
                    }
                }
            }
//            best_next_position = getNextAStarStep(A_star(start, this.possible_goal_positions.get(0)));
            debugPrint("next best step: " + best_next_position);
            if (this.canMoveTo(new PlaceObject(this.players[my_color].i, this.players[my_color].j), best_next_position)) {
                return best_next_position;
            } else {
                debugPrint("Invalid move detected!");
                debugPrint("Im at: " + new PlaceObject(this.players[my_color].i, this.players[my_color].j));
            }
            return best_next_position;
        }

        /**
         * Egy kifejni az A* altal visszaadott AStarNode-bol, hogy merre kell elindulni, ahhoz hogy az A* altal megtalalt celba erjunk
         *
         * @param final_node az A* altal megtalalt celpont
         * @return A mezo ahova lepni kell
         */
        private PlaceObject getNextAStarStep(AStarNode final_node) {
//            debugPrint("Getting path to " + final_node);
//            return (final_node.comeFrom == null ? final_node.position : getNextAStarStep(final_node.comeFrom));
            AStarNode node = final_node.copy();
            PlaceObject prev_p = final_node.position;
            while (node.comeFrom != null) {
                debugPrint("step: " + node.toString());
                prev_p = node.position;
                node = node.comeFrom;
            }
            return prev_p;
        }

        /**
         * A* heurisztika: legvonalban vett tavolsag a pitagorasz tetel segitsegevel
         *
         * @param p    honnan
         * @param goal hova
         * @return tavolsag
         */
        public static double A_star_heuristic(PlaceObject p, PlaceObject goal) {
            return Math.sqrt(Math.pow(Math.abs(p.i - goal.i), 2) + Math.pow(Math.abs(p.j - goal.j), 2));
        }

        /**
         * A* algoritmus
         *
         * @param start honnan kezdje a keresest
         * @param goal  mi a celpont
         * @return a megtalalt celpntot
         * @throws Exception ha nem talalta meg a celpontot
         */
        public AStarNode A_star(PlaceObject start, PlaceObject goal) throws Exception {
//            this.resetAStarNodeTileMapParents();
            PriorityQueue<AStarNode> openSet = new PriorityQueue<>(
                    GameBoard_size * GameBoard_size,
                    (o1, o2) -> (int) Math.floor(o1.estimatedDistanceToTarget(goal) - o2.estimatedDistanceToTarget(goal)));
            PriorityQueue<AStarNode> closedSet = new PriorityQueue<>(
                    GameBoard_size * GameBoard_size,
                    (o1, o2) -> (int) Math.floor(o1.estimatedDistanceToTarget(goal) - o2.estimatedDistanceToTarget(goal)));
            openSet.add(new AStarNode(start, null, this));
            debugPrint("Finding path to: " + goal);

            while (!openSet.isEmpty()) {
                AStarNode current_node = openSet.peek();
                assert current_node != null;
                debugPrint("Looking at : " + current_node);
                if (current_node.position.i == goal.i && current_node.position.j == goal.j) {
                    debugPrint("Found path to target");
                    return current_node;
                }
                for (AStarNode neighbour : current_node.getNeighbourNodes()) {
                    debugPrint("Looking at neighbour: " + neighbour);
                    if (!closedSet.contains(neighbour) && !openSet.contains(neighbour)) {
                        debugPrint("Putting neighbour " + neighbour + " into the open set");
                        neighbour.comeFrom = current_node;
                        neighbour.stepsToGetHere = current_node.stepsToGetHere + 1;
                        openSet.add(neighbour);
                    } else {
                        debugPrint("neighbour is already in the " + (openSet.contains(neighbour) ? "open " : "") + (closedSet.contains(neighbour) ? " closed" : "") + " set");
                        if (current_node.stepsToGetHere + 1 < neighbour.stepsToGetHere) {
                            debugPrint("found better path to neighbour: through this node");
                            neighbour.comeFrom = current_node;
                            neighbour.stepsToGetHere = current_node.stepsToGetHere + 1;
                            if (closedSet.contains(neighbour)) {
                                closedSet.remove(neighbour);
                                openSet.add(neighbour);
                            }
                        }
                    }
                }
                openSet.remove(current_node);
                closedSet.add(current_node);
            }
            debugPrint(openSet.toString());
            debugPrint(closedSet.toString());
            debugPrint("Failure");
            throw new Exception("NO PATH FOUND");
//            return new AStarNode(null, null, null);
        }

        public List<WallObject> getWalls() {
            return walls;
        }

        public QuoridorPlayer[] getPlayers() {
            return players;
        }
    }

    /**
     * Az A* algoritmusban hasznalt Node
     */
    public class AStarNode {
        public final PlaceObject position;
        public AStarNode comeFrom;
        public int stepsToGetHere;

        private GameBoard gameBoard;

        //        public AStarNode() { }

        /**
         * Letrehoz egy uj AstarNode-ot
         *
         * @param position  a node pozicionja
         * @param comeFrom  honnan ertuk el ezt a Node-ot
         * @param gameBoard a gameBoard peldany
         */
        public AStarNode(PlaceObject position, AStarNode comeFrom, GameBoard gameBoard) {
            this.position = position;
            this.comeFrom = comeFrom;
            this.gameBoard = gameBoard;
        }

        /**
         * Letrehoz egy uj AstarNode-ot
         *
         * @param position       a node pozicionja
         * @param comeFrom       honnan ertuk el ezt a Node-ot
         * @param gameBoard      a gameBoard peldany
         * @param stepsToGetHere ahany lepessel ertuk el ezet a Node-ot
         */
        public AStarNode(PlaceObject position, AStarNode comeFrom, GameBoard gameBoard, int stepsToGetHere) {
            this.position = position;
            this.comeFrom = comeFrom;
            this.gameBoard = gameBoard;
            this.stepsToGetHere = stepsToGetHere;
        }

        /**
         * A teljes becsult tav a start-tol a celpontba
         *
         * @param target a celpont
         * @return A teljes becsult tavot a start-tol a celpontba
         */
        public double estimatedDistanceToTarget(PlaceObject target) {
            return this.stepsToGetHere + GameBoard.A_star_heuristic(this.position, target);
        }

        /**
         * Visszaadja azokat a Node-okat, amelyekbe el jehet jutni a jelenlegi Node-bol
         *
         * @return
         */
        public List<AStarNode> getNeighbourNodes() {
            List<AStarNode> neighbour_nodes = new ArrayList<>();
            for (PlaceObject neighbour_place : this.position.getNeighbors(gameBoard.getWalls(), gameBoard.getPlayers())) {
//                this.gameBoard.aStarNodeTileMap[neighbour_place.i][neighbour_place.j].comeFrom = this;
//                this.gameBoard.aStarNodeTileMap[neighbour_place.i][neighbour_place.j].stepsToGetHere = this.stepsToGetHere + 1;
                neighbour_nodes.add(this.gameBoard.aStarNodeTileMap[neighbour_place.i][neighbour_place.j]);
            }
            debugPrint("Neighbour nodes: " + neighbour_nodes);
            return neighbour_nodes;
        }

        /**
         * Lemasolja ezt a Node-ot
         *
         * @return egy uj Node-ot, melynek parameterei megegyeznek ennek a peldanyeval
         */
        public AStarNode copy() {
            return new AStarNode(new PlaceObject(this.position.i, this.position.j), this.comeFrom, this.gameBoard, this.stepsToGetHere);
        }

        @Override
        public String toString() {
            return "Pos(" + this.position.i + ", " + this.position.j + ") Parent: " + (this.comeFrom == null ? "null" : this.comeFrom.position.i + ", " + this.comeFrom.position.j);
        }
    }

    public class InternalBlockRandomPlayer extends QuoridorPlayer {

        private final List<WallObject> walls = new LinkedList<WallObject>();
        private final QuoridorPlayer[] players = new QuoridorPlayer[2];
        private int numWalls;

        public InternalBlockRandomPlayer(int i, int j, int color, Random random) {
            super(i, j, color, random);
            players[color] = this;
            players[1 - color] = new DummyPlayer((1 - color) * (QuoridorGame.HEIGHT - 1), j, 1 - color, null);
            numWalls = 0;
        }

        @Override
        public QuoridorAction getAction(QuoridorAction prevAction, long[] remainingTimes) {
            if (prevAction instanceof WallAction) {
                WallAction a = (WallAction) prevAction;
                walls.add(new WallObject(a.i, a.j, a.horizontal));
            } else if (prevAction instanceof MoveAction) {
                MoveAction a = (MoveAction) prevAction;
                players[1 - color].i = a.to_i;
                players[1 - color].j = a.to_j;
            }

            int di = (color * (QuoridorGame.HEIGHT - 1)) - players[1 - color].i < 0 ? -1 : 0;
            List<WallObject> wallObjects = new LinkedList<WallObject>();
            wallObjects.add(new WallObject(players[1 - color].i + di, players[1 - color].j - color, true));
            wallObjects.add(new WallObject(players[1 - color].i + di, players[1 - color].j - 1 + color, true));
            wallObjects.add(new WallObject(players[1 - color].i + di, players[1 - color].j - color, false));
            wallObjects.add(new WallObject(players[1 - color].i + di, players[1 - color].j - 1 + color, false));
            for (WallObject wall : wallObjects) {
                if (numWalls < QuoridorGame.MAX_WALLS && QuoridorGame.checkWall(wall, walls, players)) {
                    numWalls++;
                    walls.add(wall);
                    return wall.toWallAction();
                }
            }

            double diff = -QuoridorGame.HEIGHT;
            MoveAction action = null;
            for (PlaceObject step : toPlace().getNeighbors(walls, players)) {
                double d = ((color == 0 ? step.i - i : i - step.i) + 1) / 4.0 + random.nextDouble();
                if (diff < d) {
                    diff = d;
                    action = new MoveAction(i, j, step.i, step.j);
                }
            }
            return action;
        }

    }
}
