#!/bin/bash

GUIMODE="gui"
GUIMODE_VS_AGENT="gui_agent"
AGENT_VS_GUIMODE="agent_gui"
AGENT_VS_BLOCKRANDOM="agent_block_rev"


if [ -z "$1" ]; then
    echo "Running Agent vs BlockRandomPlayer"
    javac -cp game_engine.jar Agent.java
    java -Xmx2G -jar game_engine.jar 0 game.quoridor.QuoridorGame 1234567890 5000 game.quoridor.players.BlockRandomPlayer Agent
elif [ "$1" == $AGENT_VS_BLOCKRANDOM ]; then
    echo "Running  Agent vs HumanPlayer"
    javac -cp game_engine.jar Agent.java
    java -Xmx2G -jar game_engine.jar 0 game.quoridor.QuoridorGame 1234567890 5000 Agent game.quoridor.players.BlockRandomPlayer
elif [ "$1" == $GUIMODE ]; then
    echo "Running BlockRandomPlayer vs HumanPlayer"
    java -Xmx2G -jar game_engine.jar 30 game.quoridor.QuoridorGame 1234567890 5000 game.quoridor.players.HumanPlayer game.quoridor.players.BlockRandomPlayer
elif [ "$1" == $GUIMODE_VS_AGENT ]; then
    echo "Running  HumanPlayer vs Agent"
    javac -cp game_engine.jar Agent.java
    java -Xmx2G -jar game_engine.jar 30 game.quoridor.QuoridorGame 1234567890 5000 game.quoridor.players.HumanPlayer Agent
elif [ "$1" == $AGENT_VS_GUIMODE ]; then
    echo "Running  Agent vs HumanPlayer"
    javac -cp game_engine.jar Agent.java
    java -Xmx2G -jar game_engine.jar 30 game.quoridor.QuoridorGame 1234567890 5000 Agent game.quoridor.players.HumanPlayer
fi
