import json
import math
import time

MAX_CHILDREN_PER_NODE = 3


class Node:
    all_nodes = None

    def __init__(self, node_idx: int, nyeremeny_ertek=None) -> None:
        self.idx: int = node_idx
        self.children_idxs: [int] = []
        self.nyeremeny_ertek: int | str = nyeremeny_ertek

        for i in range(MAX_CHILDREN_PER_NODE):
            self.children_idxs.append(None)

    def vegallapot(self) -> bool:
        """
        A node akkor vegallapot, ha level csucs, azaz nincsen gyereke, ami itt azt jelenti a gyerekei listaban csupa None elem van
        :return: True, ha a node level csucs, False hogyha nem level csucs
        """
        for i in range(MAX_CHILDREN_PER_NODE):
            if self.children_idxs[i] is not None:
                return False
        return True

    def neighbours(self):  # actually, it's the children
        """
        A pszeudokodokban a szomszed szo szerepel, de a vegere rajottem, hogy amugy a node-ok gyerekeirol van szo
        :return: a node gyerek nodejait egy listaban
        """
        child_nodes = [node for node in Node.all_nodes.values() if node.idx in self.children_idxs]
        return child_nodes

    def toJSON(self) -> str:
        return json.dumps(self, default=lambda o: o.__dict__, indent=4)

    def __str__(self) -> str:
        return self.toJSON()

    def __repr__(self):
        return str(self)


def convert_from_str(value: str):
    """
    Atalakit str-bol int-be ha lehet, es valtozatlanul adja vissa az erteket ha nem lehet
    :param value: valamien ertek egy str-ben
    :return: az atalakitott ertek
    """
    try:
        return int(value)
    except ValueError:
        return value


def get_tree(input_strings: [str]) -> Node:
    """
    Boelvassa az inputot, es felepiti a fat a memeoriaban
    :return: a gyoker csucsot
    """
    # input_strings: [str] = "x x x x 3 x 8 x x 6 14 x 5 . . . 12 -7 . . . . 2 . . 4 . . . . . . . . 1 . . . . .".split(
    #     " ")
    # input_strings: [str] = "3 3 2 1 3 12 8 2 4 6 14 1 5 . . . 12 -7 . . . . 2 . . 4 . . . . . . . . 1 . . . . .".split(" ")

    nodes: dict[int, Node] = {}

    for i, node_data in enumerate(input_strings):
        if node_data != ".":
            nodes[i] = Node(i, nyeremeny_ertek=convert_from_str(node_data))

    Node.all_nodes = nodes
    # print(nodes)

    for node_idx, node in nodes.items():
        for node_child_idx in range(MAX_CHILDREN_PER_NODE):
            try:
                node.children_idxs[node_child_idx] = nodes[node_idx * MAX_CHILDREN_PER_NODE + node_child_idx + 1].idx
            except KeyError:
                node.children_idxs[node_child_idx] = None

    return nodes[0]


def max_value(node: Node, alfa: float, beta: float) -> float:
    if node.vegallapot():
        return node.nyeremeny_ertek

    max_val = -math.inf
    for a in node.neighbours():
        max_val = max(max_val, min_value(a, alfa, beta))
        if max_val >= beta:
            return max_val
        alfa = max(max_val, alfa)
    return max_val


def min_value(node: Node, alfa: float, beta: float) -> float:
    if node.vegallapot():
        return node.nyeremeny_ertek

    min_val = math.inf
    for a in node.neighbours():
        min_val = min(min_val, max_value(a, alfa, beta))
        if alfa >= min_val:
            return min_val
        beta = min(min_val, beta)
    return min_val


def main():
    """
    bekeri az inputot
    felepiti a fat
    lefuttatja az algoritmust
    kiirja az eredmenyt
    :return:
    """
    input_strings: [str] = input(f"Kérem adja meg a fát listaként ábrázolva: ").split(" ")

    start_time = time.time()
    root_node: Node = get_tree(input_strings)
    # print(Node.all_nodes)
    res = max_value(root_node, -math.inf, math.inf)
    end_time = time.time()
    print(f"A maximálisan elérhető nyereség: {res}. Futási idő: {end_time - start_time} sec")


if __name__ == "__main__":
    main()
