# Gyakorló feladatsor 1

PHP-Sqlite alapú feladasor. Az SQLi feladat célja a felhasználó
(ABCDEFG neptun kódú hallgató) adatbázisban tárolt jelszavának megszerzése.

A feladatsor dockerként érhető el. A docker utasítás:
`sudo docker run --rm -it -p 127.0.0.1:8000:80/tcp pengoe/sqlite-php-app`.

Az SQlite legfontosabb eltérései a MySQL-hez képest a feladat megoldása szempontjából:

- A metaadatokat (táblák, oszlopok neveit) az sqlite_master táblából lehet lekérdezni
- Pontosvesszőt is kell rakni az utasítás végén
- Egy példa input lehet (ha a beégetett select 1 oszlopot kérdezett le):
  `' UNION SELECT sql FROM sqlite_master;#`
