#include "hash.h"
#include <stdio.h>

extern unsigned produceHash2(unsigned data);

void almostUnusedFunction(){
   unsigned secret0 = produceHash2(SECRET0);
   printf("%08x\n", secret0);
}

void work(char* inp){

  printf("\n==========\n");
  printf(inp);
  printf("\n==========\n");
}

typedef struct {
   unsigned user_numbers[3];
   unsigned user_value;
   unsigned secret1;
   unsigned user_helper;
   char input[256];
} locals;

int main(){

   locals loc;
   loc.user_value = 0;
   loc.user_helper = 777;
   loc.secret1 = produceHash2(SECRET1);

   printf("Provide a string:\n");
   fgets(loc.input, 256, stdin);

   work(loc.input);
   

   printf("How many numbers do you want to store?\n");
   scanf("%d", &loc.user_value);

   if(loc.user_value % 3 == 0 && loc.user_helper - loc.user_value > 20000){
     almostUnusedFunction();
   }

   for(int i=0; i < loc.user_value; ++i){
     printf("%d:\n", i);
     scanf("%x", &loc.user_numbers[i]);
   }

   if(loc.secret1 == 9){
     unsigned secret2 = produceHash2(SECRET2);
     printf("%08x\n", secret2);
   }


   work(loc.input);
  


   return 0;
}
