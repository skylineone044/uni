#include "hash.h"
#include <stdio.h>

extern unsigned produceHash2(unsigned data);

typedef struct {
   char arr[20];
   unsigned var3;
   unsigned var4;
} locals;

typedef struct{
   unsigned var1;
   unsigned secret1;
   unsigned var2;
   char inp[256];
} locals2;

void getSecret0(){
   unsigned secret0 = produceHash2(SECRET0);
   printf("%08x\n", secret0);
}

void getSecret1(){
  locals2 loc;
  loc.var1 = 12;
  loc.var2 = 7;
  loc.secret1 = produceHash2(SECRET1);

  printf("Provide a string:\n");
  fgets(loc.inp, 256, stdin);
  printf("\n==========\n");
  printf(loc.inp);
  printf("\n==========\n");
}

void getSecret2(){
  unsigned secret2 = produceHash2(SECRET2);
   printf("%08x\n", secret2);
}


int main(){

   unsigned char item = 'B';
   unsigned long item2;

   locals loc;
   
   getSecret1();
   
   printf("Provide a number:\n");
   scanf("%ul", &item2);
   item*=item2;
   if(item == 8){
     getSecret0();
   }

   printf("Provide a string again:\n");
   scanf("%s", loc.arr);
   if(loc.var4 == 0x70707070){
    getSecret2();
   }

   return 0;
}
