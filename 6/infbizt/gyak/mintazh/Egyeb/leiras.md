# Egyéb gyakorló feladat

Ez a sérülékeny PHP-MySQL alapú webalkalmazás arra jó, hogy az SQLi mellett a
többi webes sérülékenység megtalálását, kihasználását is gyakorolhatod.
Docker compose-al kell buildeni az image-t. Az alábbi github linken érhető el
a forrás illetve a leírás hozzá: [Mailserver feladat](https://github.com/epengo/beadando_mailserver).

Azt gyanítod, hogy a feleséged, Rosalinda megcsal téged. Ezért eldöntötted, hogy
feltöröd az email kiszolgálóját és megnézed, vannak-e a megcsalást bizonyító
emailjei. Tudod, hogy a felhasználóneve rosalinda. Jelentkezz be a jelszavának
ismerete nélkül! Utána nézz szét az emailjei között! Próbálj találni módot rá,
hogy elolvashatsd a korábban küldött üzeneteket is! Ha rájössz, hogy kivel csal
gondolkozz el rajta hogyan tudnál borsot törni a szemétláda orra alá (pl úgy,
hogy kizárod a fiókjából...) Tudsz felugró ablakot becsempészni, hogy világgá
kürtöld Rosalinda hűtlenségét? Akár úgy is, hogy "maradandó" legyen?

OWASP gyakorló feladat: Mutillidae

A DVWA-hoz hasonló direkt sérülékeny alkalmazás. A metasploitable dockerben érhető el.

```
sudo docker run -it --rm --name metasploitable --hostname metasploitable2 tleemcjr/metasploitable2 bash
docker bash-ben: services.sh
böngészőben: 172.17.0.2/mutillidae
```
