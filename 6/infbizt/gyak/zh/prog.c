#include "hash.h"
#include <stdio.h>

extern unsigned produceHash2(unsigned data);

typedef struct {
   char arr[24];
   unsigned var3;
   unsigned var4;
} locals;

typedef struct{
   unsigned var2;
   unsigned var3;
   unsigned secret1;
   unsigned var1;
   char inp[256];
} locals2;

void getSecret1(){
  locals2 loc;
  loc.var1 = 512;
  loc.var2 = 0xdefe0000;
  loc.var3 = 12048;
  loc.secret1 = produceHash2(SECRET1);

  printf("Provide a string (secret1):\n");
  scanf("\n%255s", loc.inp);
  printf("\n==========\n");
  printf(loc.inp);
  printf("\n==========\n");
}

void getSecret2(){
  unsigned secret2 = produceHash2(SECRET2);
   printf("%02x\n", secret2);
}

void getSecret0(){
   unsigned secret0 = produceHash2(SECRET0);
   printf("%02x\n", secret0);
}


int main(){

   unsigned char item = 'z';
   unsigned long item2;

   locals loc;
   loc.var4 = 0;
   
   printf("Provide a number (secret0):\n");
   scanf("%ul", &item2);
   item-=item2;
   if(item > 254){
     getSecret0();
   }

   getSecret1();

   printf("Provide a string again (secret2):\n");
   scanf("%s", loc.arr);
   if(loc.var3 == 0x61616161 && loc.var4 == 0){
    getSecret2();
   }

   return 0;
}
