Animációk HTML5 Canvas és DOM használatával:

1. Hogyan jelezhetem a böngészőnek, hogy adott időközönként szeretnék meghívni egy általam megadott függvényt?

-setInterval: https://www.w3schools.com/jsref/met_win_setinterval.asp
-setTimeout: https://www.w3schools.com/jsref/met_win_settimeout.asp

2. Miként állíthatom le a ezeket az ismétlődő hívásokat?

-clearInterval: https://www.w3schools.com/jsref/met_win_clearinterval.asp
-clearTimeout: https://www.w3schools.com/jsref/met_win_cleartimeout.asp

3. Van egy függvényem, amit szeretnék meghívni mielőtt a böngészőablak tartalma frissül. (Pl. azért, hogy rajzolhassak
a "canvas"-ra.) Mi a teendő?

-requestAnimationFrame: https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame

4. Eseménykezelőt szeretnék rendelni egy HTML elemhez. Hogyan tehetem meg? 

-addEventListener: https://www.w3schools.com/jsref/met_element_addeventlistener.asp

5. Az eseménykezelőben miből fogom tudni, hogy pontosan mi történt? (Pl. melyik billentyűt használta a user, milyen
koordinátájú pontra kattintott, stb...) 

-KeyboardEvent: https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent
-MouseEvent: https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent
-clientX: https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/clientX (és Y...)
-code: https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/code

6. Mi a teendő, ha a canvas-ra képet szeretnék helyezni rajz helyett?

-drawImage: https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/drawImage

7. A korábbi rajzok ott maradnak a vásznon. Miért?

-clearRect: https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/clearRect

jQuery alapú alkalmazások:

0. Mi kell ahhoz, hogy használhassam a jQuery könyvtárat?

1. Meg tudom-e adni a leggyakrabban előforduló HTML elemeket? Emlékszem-e a HTML alapokra (tag, attribútum, osztály)?

-img https://developer.mozilla.org/en-US/docs/Web/API/HTMLImageElement
-div https://www.w3schools.com/tags/tag_div.ASP (és még sok további...)
-attribute https://www.w3schools.com/html/html_attributes.asp
-class https://www.w3schools.com/html/html_classes.asp

2. Mi a DOM tree? Milyen kapcsolat van a DOM elemek és a jQuery objektumok között?

-What is HTML DOM? https://www.w3schools.com/whatis/whatis_htmldom.asp
-http://api.jquery.com/jquery/

3. Mire jók a jQuery selector-ok, és hogyan használhatom őket? Mit ad vissza a jQuery függvény, ha CSS selector-t kap? Hogyan ellenőrzöm, hogy van-e találat?

-jQuery Selectors https://www.w3schools.com/js/js_jquery_selectors.asp
-http://api.jquery.com/jquery/

4. Hogyan kereshetők elemek egy vagy TÖBB attribútum alapján?

-jQuery Multiple Attribute Selector https://api.jquery.com/multiple-attribute-selector/

5. Van egy jQuery objektumom, ami egy vagy több DOM elemet ábrázol. Szeretnék egy selector-t CSAK ennek az elemnek/elemhalmaznak
a leszármazottaira (ld. DOM tree) alkalmazni. Mi a teendő?

-children https://api.jquery.com/children/
-find https://api.jquery.com/find/

6. Létrehozhatok új jQuery objektumot, nem csak lekérdezés eredményeképp adódhat? Ha igen, hogyan adjam hozzá a dokumentumhoz, vagy
annak valamely eleméhez? Ha már nem kell, miként töröljem? Hogyan frissül ezen műveletek hatására a DOM tree?

-jQuery https://api.jquery.com/jquery/ (Fontos: Creating New Elements)
-append https://api.jquery.com/append/
-empty https://api.jquery.com/empty/
-https://api.jquery.com/remove/

7. Elemek attribútumát szeretném lekérdezni vagy beállítani, hogyan tehetem meg?

-attr https://api.jquery.com/attr/

8. Átállíthatom egy elem stílusát jQuery-vel? Ha bizonyos jellemzőit már meghatároztam statikusan (pl. egy CSS
fájlban), akkor mi fog történni?

-css https://www.w3schools.com/jquery/jquery_css.asp

9. Animáltan szeretném változtatni az elem valamely jellemzőit. Mit változtathatok? Hogyan? Megadhatok-e olyan függvényt, ami
akkor fut, ha végzett az animáció?

-animate https://api.jquery.com/animate/

10. Meg tudom-e változtatni animáltan az elem színét, pozícióját (függőlegesen és vízszintesen), átlátszóságát?

11. Egyszerűen eltüntetni vagy épp láthatóvá tenni kellene animáltan az elemet. Van-e erre egyszerűbb mód mint az "animate" 
használata?

-hide https://api.jquery.com/hide/
-https://api.jquery.com/show/

12. Egy elemhez vagy elemek halmazához eseménykezelőt rendelnék, ahogyan a HTML5-Canvas játékok esetén tettük. Hogy megy ez
a jQuery-vel?

-on https://api.jquery.com/on/
-off https://api.jquery.com/off/
-event https://api.jquery.com/category/events/event-object/ (Fontos: Common Event Properties)

13. Az eseménykezelőben hogyan érhetem el azt az element, amelyen bekövetkezett a kezelő hívását kiváltó esemény?

-Using DOM Elements https://api.jquery.com/jquery/ 

14. Használhatom-e a szokásos DOM időzítőket (pl. setTimeout) a jQuery mellett?
