let GAME_BOARD_STATE = [];
let GAME_BUTTON_SIZE = 110;
let BOARD_SIZE = 5;
let MOVES_NUMBER = 0;
let START_TIME = 0;
let GAME_IN_PROGRESS = true;
let RIPPLE_LENGTH_ms = 500;
let GAME_MODES = {
    preset: "preset", random: "random"
};
let GAME_MODE = GAME_MODES.preset;
let LEADERBOARD = [];
let click_sound_file = "click.opus";
// background music is Melancholy by LEMMiNO (https://www.lemmi.no/music)
let BACKGROUND_MUSIC_FILE = "background_music.opus";
let BACKGROUND_MUSIC_SOUND = new Audio(BACKGROUND_MUSIC_FILE);
BACKGROUND_MUSIC_SOUND.load();
BACKGROUND_MUSIC_SOUND.loop = true;
let BACKGROUND_MUSIC_ENABLED = true;
let RANDOM_ITER_COUNT = BOARD_SIZE * BOARD_SIZE * 10;

const LEVELS = {
    "1": [[false, true, false, true, false], [true, true, true, true, true], [false, true, true, true, false], [true, true, true, true, true], [false, true, false, true, false]],
    "2": [[false, false, false, true, false], [true, true, false, false, true], [false, false, true, false, false], [false, true, true, true, true], [false, false, true, false, true]],
    "3": [[true, false, false, false, false], [false, true, false, false, false], [true, false, true, true, true], [true, true, true, false, false], [true, true, false, false, false]],
    "4": [[true, true, false, true, true], [true, true, false, false, true], [false, false, true, false, true], [false, false, true, false, true], [true, false, false, true, true]],
    "5": [[false, true, false, false, false], [false, true, false, false, true], [false, false, false, false, false], [true, true, false, true, false], [false, true, true, true, true]]
}
let SELECTED_LEVEL = 1;

function animate_options() {
    if (GAME_MODE === GAME_MODES.preset) {
        $("#gamemode_random").animate({
            opacity: 0,
            left: "-=20",
        }, 300);
        $("#gamemode_preset").animate({
            opacity: 1,
            left: "-=20",
        }, 300);
    } else {
        $("#gamemode_random").animate({
            opacity: 1,
            left: "+=20",
        }, 300);
        $("#gamemode_preset").animate({
            opacity: 0,
            left: "+=20",
        }, 300);
    }
}

function update_sound_icon() {
    if (BACKGROUND_MUSIC_ENABLED) {
        $("#music_mute_btn").val("🔊")
    } else {
        $("#music_mute_btn").val("🔇")
    }
}

function toggle_background_music() {
    if (BACKGROUND_MUSIC_ENABLED) {
        BACKGROUND_MUSIC_ENABLED = false;
        BACKGROUND_MUSIC_SOUND.pause();
    } else {
        BACKGROUND_MUSIC_ENABLED = true;
        BACKGROUND_MUSIC_SOUND.play();
    }
    update_sound_icon();
    localStorage.setItem("is_audio_enabled", JSON.stringify(BACKGROUND_MUSIC_ENABLED));
}

function update_leaderboard_list() {
    LEADERBOARD = LEADERBOARD.sort((a, b) => a.steps - b.steps).slice(0, 10);
    let leaderboard_removed_dupes = [];
    let used_names = [];
    for (let i = 0; i < LEADERBOARD.length; i++) {
        if (!used_names.includes(LEADERBOARD[i].name)) {
            leaderboard_removed_dupes.push(LEADERBOARD[i]);
            used_names.push(LEADERBOARD[i].name);
        }
    }
    LEADERBOARD = leaderboard_removed_dupes;

    $("#leaderboard_content").empty();
    for (let item of LEADERBOARD) {
        $(`<tr>
                <td>${item.name}</td>
                <td>${item.steps}</td>
                <td>${item.time}</td>
           </rt>`).appendTo("#leaderboard_content");
    }
}

function add_name_to_leaderboard() {
    if ($("#name_filed").val().toString().length >= 1) {
        LEADERBOARD.push({
            name: $("#name_filed").val().toString(), steps: MOVES_NUMBER, time: $("#elapsed_time").text().toString()
        })
        localStorage.setItem("leaderboard", JSON.stringify(LEADERBOARD));
        $("#name_too_short_warning").css({visibility: "hidden"});
    } else {
        $("#name_too_short_warning").css({visibility: "visible"});
    }
    // console.log(JSON.stringify(LEADERBOARD));
    $("#add_name_leaderboard_modal_btn").css({visibility: "hidden"});
}

function start_level(boardstate = []) {
    GAME_IN_PROGRESS = true;
    $("#win_text").css({visibility: "hidden"})
    $("#game_screen").css({visibility: "visible"})
    GAME_BOARD_STATE = boardstate;
    START_TIME = Date.now();
    reset_moves();
    $("#add_name_leaderboard_modal_btn").css({visibility: "hidden"});
}

function start_preset() {
    console.log(`Start level ${SELECTED_LEVEL}`);
    GAME_BOARD_STATE = JSON.parse(JSON.stringify(LEVELS[`${SELECTED_LEVEL}`]));
    BOARD_SIZE = GAME_BOARD_STATE.length;
    rebuild_board(BOARD_SIZE, GAME_BOARD_STATE);
    if (BACKGROUND_MUSIC_ENABLED) {
        BACKGROUND_MUSIC_SOUND.play();
    }
}


function decrement_level_selector() {
    if (1 <= SELECTED_LEVEL - 1) {
        SELECTED_LEVEL -= 1;
        $("#level_number").text(SELECTED_LEVEL)
    }
    console.log(`Selected level: ${SELECTED_LEVEL}`);
}

function increment_level_selector() {
    if (Object.keys(LEVELS).length >= SELECTED_LEVEL + 1) {
        SELECTED_LEVEL += 1;
        $("#level_number").text(SELECTED_LEVEL)
    }
    console.log(`Selected level: ${SELECTED_LEVEL}`);
}

function switch_game_mode() {
    if (GAME_MODE === GAME_MODES.preset) {
        GAME_MODE = GAME_MODES.random;
        $("#gamemode_preset").css({visibility: "hidden"})
        $("#gamemode_random").css({visibility: "visible"})
        $("#gamemode_selector_btn").val("Random");
    } else if (GAME_MODE === GAME_MODES.random) {
        GAME_MODE = GAME_MODES.preset;
        $("#gamemode_preset").css({visibility: "visible"})
        $("#gamemode_random").css({visibility: "hidden"})
        $("#gamemode_selector_btn").val("Level");
    }
}

function set_game_button_state(i, j, board=GAME_BOARD_STATE) {
    if ((0 <= i && i < board.length) && (0 <= j && j < board.length)) {
        board[i][j] = !board[i][j];
    }
}

function update_board_display() {
    for (let i = 0; i < BOARD_SIZE; i++) {
        for (let j = 0; j < BOARD_SIZE; j++) {
            if (GAME_BOARD_STATE[i][j]) {
                $(`#${i * BOARD_SIZE + j}`).removeClass("state_off").addClass("state_on")
            } else {
                $(`#${i * BOARD_SIZE + j}`).removeClass("state_on").addClass("state_off")
            }
        }
    }
}

function increment_moves() {
    if (GAME_IN_PROGRESS) {
        MOVES_NUMBER += 1;
        $("#movecount").text(`${MOVES_NUMBER}`);
    }
}

function reset_moves() {
    MOVES_NUMBER = 0;
    $("#movecount").text(`0`);
}

function win() {
    if (GAME_IN_PROGRESS) {
        GAME_IN_PROGRESS = false;
        // $("#win_text").style.setProperty("visibility", "visible");
        $("#win_text").css({visibility: "visible"})

        for (let i = 0; i < BOARD_SIZE; i++) {
            for (let j = 0; j < BOARD_SIZE; j++) {
                apply_ripple($(`#${i * BOARD_SIZE + j}`).parent(), i * BOARD_SIZE + j).then(r => {
                });
            }
        }
        $("#add_name_leaderboard_modal_btn").css({visibility: "visible"});
    }
}

function check_win_condition() {
    let is_there_button_on = false;
    for (let i = 0; i < BOARD_SIZE; i++) {
        for (let j = 0; j < BOARD_SIZE; j++) {
            if (GAME_BOARD_STATE[i][j]) {
                is_there_button_on = true;
            }
        }
    }
    if (!is_there_button_on) {
        win();
    }
}

function get_random_is_on(chance) {
    return Math.random() < chance;
}

const sleep = ms => new Promise(r => setTimeout(r, ms));

async function apply_ripple(button, delay = 0) {
    let click_sound = new Audio(click_sound_file);
    let id = `animationholder_${Math.random().toString().slice(2, 100)}`
    let animationholder = $(`<div class="ripple_holder" id="${id}"></div>`);
    animationholder.appendTo(button);
    animationholder.append(click_sound)
    await sleep(delay * 30);
    await click_sound.play();
    animationholder.addClass("animate_ripple");
    await sleep(RIPPLE_LENGTH_ms);
    $(`#${id}`).remove();
}

function click_at(i, j, board=GAME_BOARD_STATE) {
    set_game_button_state(i, j, board);
    set_game_button_state(i - 1, j, board);
    set_game_button_state(i, j - 1, board);
    set_game_button_state(i + 1, j, board);
    set_game_button_state(i, j + 1, board);

}

function game_button_click() {
    if (GAME_IN_PROGRESS) {
        let i = Math.floor($(this).children(".gamebutton").last().attr("id") / BOARD_SIZE);
        let j = $(this).children(".gamebutton").last().attr("id") % BOARD_SIZE;
        console.log(`clicked: ${i} ${j}`);

        click_at(i, j);
        increment_moves();
        update_board_display();
        check_win_condition();
        apply_ripple($(this)).then(r => {});
        // console.log(JSON.stringify(GAME_BOARD_STATE))
    }
}

function solve() {

}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min); // The maximum is exclusive and the minimum is inclusive
}

function create_random_board(boardsize, crate_empty=false) {
    let gameboard = [];
    for (let i = 0; i < boardsize; i++) {
        gameboard.push([]);
    }
    for (let i = 0; i < boardsize; i++) {
        for (let j = 0; j < boardsize; j++) {
            gameboard[i].push(false)
        }
    }

    if(crate_empty) {
        return gameboard;
    }

    for (let i = 0; i < RANDOM_ITER_COUNT; i++) {
        click_at(getRandomInt(0, boardsize), getRandomInt(0, boardsize), gameboard);
    }

    return gameboard;
}

function rebuild_board(boardsize = 5, preset = create_random_board(BOARD_SIZE, true)) {
    start_level(preset);

    let timer = setInterval(function () {
        if (GAME_IN_PROGRESS) {
            let elapsed_time = Date.now() - START_TIME;
            let minutes = Math.floor((elapsed_time % (1000 * 60 * 60)) / (1000 * 60));
            let seconds = Math.floor((elapsed_time % (1000 * 60)) / 1000);

            $("#elapsed_time").text(`${String(minutes).padStart(2, '0')}:${String(seconds).padStart(2, '0')}`);
        }
    }, 1000);

    $("#gameboard").empty();

    for (let i = 0; i < boardsize; i++) {
        for (let j = 0; j < boardsize; j++) {
            // <input type="button" className="gamebutton" id="${i * BOARD_SIZE + j}" value="${i} ${j}">
            let button = $(`<div class="button_holder">
                                <div type="button" class="gamebutton" id="${i * BOARD_SIZE + j}"></div>
                            </div>`);
            apply_ripple(button, i * BOARD_SIZE + j).then(r => {
            });

            button.appendTo($("#gameboard"));
            button.click(game_button_click);
        }
    }
    update_board_display();

    document.getElementById("gameboard").style.width = `${boardsize * GAME_BUTTON_SIZE}px`;
}

$(document).ready(function () {
    let data = JSON.parse(localStorage.getItem("leaderboard"));
    if (data !== null) {
        LEADERBOARD = data;
        localStorage.setItem("leaderboard", JSON.stringify(LEADERBOARD));
    }

    let audiomuted = JSON.parse(localStorage.getItem("is_audio_enabled"));
    if (audiomuted === null) {
        localStorage.setItem("is_audio_enabled", JSON.stringify(BACKGROUND_MUSIC_ENABLED));
    } else {
        BACKGROUND_MUSIC_ENABLED = JSON.parse(localStorage.getItem("is_audio_enabled"))
    }
    update_sound_icon();

    $("#test_game_button").remove();
    BOARD_SIZE = Number($("#board_size").val());

    $("#gamemode_selector_btn").click(switch_game_mode);

    $("#save_options_btn").click(function () {
        BOARD_SIZE = Number($("#board_size").val());
        console.log("Board size changed to: " + BOARD_SIZE);
        rebuild_board(BOARD_SIZE, create_random_board(BOARD_SIZE, false));
    });

    $("#restart_brn").click(function () {
        if (GAME_MODE === GAME_MODES.random) {
            rebuild_board(BOARD_SIZE, create_random_board(BOARD_SIZE, false));
        } else {
            start_preset();
        }
    });

    $("#solve_btn").click(function () {
        solve();
    });

    $("#empty_reset").click(function () {
        rebuild_board(BOARD_SIZE, create_random_board(BOARD_SIZE, true));
    });

    $("#start_preset_btn").click(start_preset);
    $("#prev_btn").click(decrement_level_selector);
    $("#next_btn").click(increment_level_selector);
    $("#add_name_btn").click(add_name_to_leaderboard);
    $("#open_leaderboard").click(update_leaderboard_list);
    $("#music_mute_btn").click(toggle_background_music);
    $("#gamemode_selector_btn").click(animate_options);
});