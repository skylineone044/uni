MAP_SIZE = 10;

$(document).ready(function () {
    for (let i = 0; i < 10; i++) {
        for (let j = 0; j < 10; j++) {
            let mezo = $(`<div id="${i}_${j}" style="width: 50px; height: 50px; background-color: white; margin: 0"></div>`);
            if (i === 0 && j === 0) {
                mezo.css({"background-color": "blue"});
            } else if (i === 9 && j === 9) {
                mezo.css({"background-color": "red"});
            }
            let palya = $("#palya_holder");
            mezo.appendTo(palya);
        }
    }
    posx = 0;
    posy = 0;
    direction = 0;
    a = 10;
    while(a > 0) {
        let direction = Math.floor(Math.random() * 4);
        if (direction === 0) {
            posx = Math.min(Math.max(posx + 1, 0), MAP_SIZE);
        }
        if (direction === 1) {
            posx = Math.min(Math.max(posx - 1, 0), MAP_SIZE);
        }
        if (direction === 2) {
            posx = Math.min(Math.max(posy + 1, 0), MAP_SIZE);
        }
        if (direction === 3) {
            posx = Math.min(Math.max(posx - 1, 0), MAP_SIZE);
        }
        $(`#${posx}_${posy}`).css({"background-color": "blue"});
        a--;
    }
})