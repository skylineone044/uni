# Mentorálás Tartalmi Beszámoló

- *Hallgató neve*: Tokodi Máté
- *Azonosítója*: BCSJ6F / h052794 / skylineone044
- *E-mail címe*: tokodi.mate.24@gmail.com / h052794@stud.u-szeged.hu / skylineone044@gmail.com
- *Kurzuskód*: IB611g-1
- *Gyakorlatvezető neve*: Szatmári Attila
- *Félév során előkerült feladatok és az alkalmazott megoldás vagy workaround*: -
- *Problémás eseteket ki kell emelni, amennyiben voltak*: -
- *Katalógus az összegyűjtött anyagról külön kiemelve a mentor által készített anyagokat*:
    A félév során engem nem keresett meg senki sem, hogy segítséget kérjen, bár
    többször is jeleztem, hogy van lehetőség hozzám jönni git-es kérdésekkel.
    Ettől függetlenül, a Chiron Barlangja oldalra elkészítettem a git-es anyagot:
    [https://chironszte.github.io/cave-of-chiron/_pages/topics/git_usage_and_settings/](https://chironszte.github.io/cave-of-chiron/_pages/topics/git_usage_and_settings/)
    Pull request: [https://github.com/ChironSZTE/cave-of-chiron/pull/21](https://github.com/ChironSZTE/cave-of-chiron/pull/21)
- *Dátum*: 2023. 04. 28.
