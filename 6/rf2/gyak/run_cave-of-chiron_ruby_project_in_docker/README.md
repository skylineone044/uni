# how to run the cave-of-chiron project in docker

- clone the repo
- place the `Dockerfile` and `docker-compose.yml` into the root of the cave-of-chiron project repo directory
- create the container:
    - run `docker build -t cave-of-chiron .` to create the `cave-of-chiron` docker container
- run the container:
    - run `docker-compose up --detach` to run the container in the background
    - run `docker-compose up` to run the container in the foreground, attaching the container's terminal to yours
- to stop the container:
    - if you are attached to the container: hit CTRL+C
    - if you are not attached to the container: `docker stop cave-of-chiron`

