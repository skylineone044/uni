From 26ee7172989928166d42b187c415b16b7d249d3b Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?M=C3=A1t=C3=A9=20Tokodi?= <tokodi.mate.24@gmail.com>
Date: Thu, 23 Mar 2023 15:36:10 +0100
Subject: [PATCH] Add atomic instructions

this adds the required opcode defines, checks, tests and
implementation for x86_64, x86_32, aarch64, ARM32 (v8 THUMB2), ARMv7
---
 sljit_src/sljitLir.c              |  80 +++++++++++++
 sljit_src/sljitLir.h              |  46 +++++++
 sljit_src/sljitNativeARM_32.c     |  97 +++++++++++++++
 sljit_src/sljitNativeARM_64.c     |  99 +++++++++++++++
 sljit_src/sljitNativeARM_T2_32.c  |  92 ++++++++++++++
 sljit_src/sljitNativeX86_common.c | 192 ++++++++++++++++++++++++++++++
 test_src/sljitTest.c              |  97 ++++++++++++++-
 7 files changed, 700 insertions(+), 3 deletions(-)

diff --git a/sljit_src/sljitLir.c b/sljit_src/sljitLir.c
index 390439c..c7e109d 100644
--- a/sljit_src/sljitLir.c
+++ b/sljit_src/sljitLir.c
@@ -1079,6 +1079,23 @@ static const char* call_arg_names[] = {
 	"void", "w", "32", "p", "f64", "f32"
 };
 
+static const char *atomic_op1_names[] = {
+	"atomic_load",
+	"atomic_load_u32",
+	"atomic_load_u16",
+	"atomic_load_u8",
+	"atomic_load32",
+	"atomic_load32_u16",
+	"atomic_load32_u8",
+	"atomic_store",
+	"atomic_store_u32",
+	"atomic_store_u16",
+	"atomic_store_u8",
+	"atomic_store32",
+	"atomic_store32_u16",
+	"atomic_store32_u8"
+};
+
 #endif /* SLJIT_VERBOSE */
 
 /* --------------------------------------------------------------------- */
@@ -1380,6 +1397,69 @@ static SLJIT_INLINE CHECK_RETURN_TYPE check_sljit_emit_op1(struct sljit_compiler
 	CHECK_RETURN_OK;
 }
 
+static SLJIT_INLINE CHECK_RETURN_TYPE check_sljit_emit_atomic_op1(struct sljit_compiler *compiler, sljit_s32 op,
+	sljit_s32 base_reg, sljit_s32 data_reg, sljit_s32 temp_reg)
+{
+	if (SLJIT_UNLIKELY(compiler->skip_checks)) {
+		compiler->skip_checks = 0;
+		CHECK_RETURN_OK;
+	}
+
+#if (defined SLJIT_ARGUMENT_CHECKS && SLJIT_ARGUMENT_CHECKS)
+	CHECK_ARGUMENT(SLJIT_ATOMIC_LOAD <= GET_OPCODE(op) && GET_OPCODE(op) <= SLJIT_ATOMIC_STORE32_U8);
+
+	/* all args must be registers */
+	CHECK_ARGUMENT(SLJIT_R0 <= base_reg && base_reg <= SLJIT_NUMBER_OF_REGISTERS);
+	CHECK_ARGUMENT(SLJIT_R0 <= data_reg && data_reg <= SLJIT_NUMBER_OF_REGISTERS);
+	CHECK_ARGUMENT(SLJIT_R0 <= temp_reg && temp_reg <= SLJIT_NUMBER_OF_REGISTERS);
+
+	switch (GET_OPCODE(op)) {
+		case SLJIT_ATOMIC_LOAD:
+		case SLJIT_ATOMIC_LOAD32:
+		case SLJIT_ATOMIC_LOAD_U8:
+		case SLJIT_ATOMIC_LOAD32_U8:
+		case SLJIT_ATOMIC_LOAD_U16:
+		case SLJIT_ATOMIC_LOAD32_U16:
+		case SLJIT_ATOMIC_LOAD_U32:
+			/* Only SLJIT_32, is allowed. */
+			CHECK_ARGUMENT(!(op & (VARIABLE_FLAG_MASK | SLJIT_SET_Z)));
+			break;
+
+		case SLJIT_ATOMIC_STORE:
+		case SLJIT_ATOMIC_STORE32:
+		case SLJIT_ATOMIC_STORE_U8:
+		case SLJIT_ATOMIC_STORE32_U8:
+		case SLJIT_ATOMIC_STORE_U16:
+		case SLJIT_ATOMIC_STORE32_U16:
+		case SLJIT_ATOMIC_STORE_U32:
+			/* Only SLJIT_32, SLJIT_SET_Z is allowed. */
+			CHECK_ARGUMENT(!(op & VARIABLE_FLAG_MASK));
+			/* SLJIT_SET_Z is required */
+			CHECK_ARGUMENT((op & SLJIT_SET_Z));
+			break;
+		default:
+			SLJIT_UNREACHABLE();
+			break;
+	}
+
+	compiler->last_flags = GET_FLAG_TYPE(op) | (op & (SLJIT_32 | SLJIT_SET_Z));
+#endif
+#if (defined SLJIT_VERBOSE && SLJIT_VERBOSE)
+	if (SLJIT_UNLIKELY(!!compiler->verbose)) {
+		fprintf(compiler->verbose, "  %s%s ",
+				atomic_op1_names[GET_OPCODE(op) - SLJIT_ATOMIC_BASE - 1 + (op & SLJIT_32 ? 3 : 0) + (GET_OPCODE(op) < SLJIT_ATOMIC_STORE ? 0 : 3)],
+				!(op & SLJIT_SET_Z) ? "" : ".z");
+		sljit_verbose_reg(compiler, base_reg);
+		fprintf(compiler->verbose, ", ");
+		sljit_verbose_reg(compiler, data_reg);
+		fprintf(compiler->verbose, ", ");
+		sljit_verbose_reg(compiler, temp_reg);
+		fprintf(compiler->verbose, "\n");
+	}
+#endif
+	CHECK_RETURN_OK;
+}
+
 static SLJIT_INLINE CHECK_RETURN_TYPE check_sljit_emit_op2(struct sljit_compiler *compiler, sljit_s32 op, sljit_s32 unset,
 	sljit_s32 dst, sljit_sw dstw,
 	sljit_s32 src1, sljit_sw src1w,
diff --git a/sljit_src/sljitLir.h b/sljit_src/sljitLir.h
index 0bc637d..955d1d3 100644
--- a/sljit_src/sljitLir.h
+++ b/sljit_src/sljitLir.h
@@ -1935,6 +1935,52 @@ SLJIT_API_FUNC_ATTRIBUTE void sljit_set_function_context(void** func_ptr, struct
 SLJIT_API_FUNC_ATTRIBUTE void sljit_free_unused_memory_exec(void);
 #endif
 
+#define SLJIT_ATOMIC_BASE 0x0
+/* Atomic load loads data from memory to a register atomically,
+   marking the memory location as locked if necessary. */
+
+/* Flags: - (does not modify flags) */
+#define SLJIT_ATOMIC_LOAD (SLJIT_ATOMIC_BASE + 1)
+
+/* Flags: - (does not modify flags) */
+#define SLJIT_ATOMIC_LOAD_U32 (SLJIT_ATOMIC_BASE + 2)
+/* Flags: - (does not modify flags) */
+#define SLJIT_ATOMIC_LOAD32 (SLJIT_ATOMIC_LOAD_U32 | SLJIT_32)
+
+/* Flags: - (does not modify flags) */
+#define SLJIT_ATOMIC_LOAD_U16 (SLJIT_ATOMIC_BASE + 3)
+/* Flags: - (does not modify flags) */
+#define SLJIT_ATOMIC_LOAD32_U16 (SLJIT_ATOMIC_LOAD_U16 | SLJIT_32)
+
+/* Flags: - (does not modify flags) */
+#define SLJIT_ATOMIC_LOAD_U8 (SLJIT_ATOMIC_BASE + 4)
+/* Flags: - (does not modify flags) */
+#define SLJIT_ATOMIC_LOAD32_U8 (SLJIT_ATOMIC_LOAD_U8 | SLJIT_32)
+
+/* Atomic store stores data from one register to memory atomically,
+   clearing a memory lock if necessary and successful.
+   Flags: ZF is cleared if not successful */
+
+/* Flags: Z */
+#define SLJIT_ATOMIC_STORE (SLJIT_ATOMIC_BASE + 5)
+
+/* Flags: Z */
+#define SLJIT_ATOMIC_STORE_U32 (SLJIT_ATOMIC_BASE + 6)
+/* Flags: Z */
+#define SLJIT_ATOMIC_STORE32 (SLJIT_ATOMIC_STORE_U32 | SLJIT_32)
+
+/* Flags: Z */
+#define SLJIT_ATOMIC_STORE_U16 (SLJIT_ATOMIC_BASE + 7)
+/* Flags: Z */
+#define SLJIT_ATOMIC_STORE32_U16 (SLJIT_ATOMIC_STORE_U16 | SLJIT_32)
+
+/* Flags: Z */
+#define SLJIT_ATOMIC_STORE_U8 (SLJIT_ATOMIC_BASE + 8)
+/* Flags: Z */
+#define SLJIT_ATOMIC_STORE32_U8 (SLJIT_ATOMIC_STORE_U8 | SLJIT_32)
+
+SLJIT_API_FUNC_ATTRIBUTE sljit_s32 sljit_emit_atomic_op1(struct sljit_compiler *compiler, sljit_s32 op, sljit_s32 base_reg, sljit_s32 data_reg, sljit_s32 temp_reg);
+
 #ifdef __cplusplus
 } /* extern "C" */
 #endif
diff --git a/sljit_src/sljitNativeARM_32.c b/sljit_src/sljitNativeARM_32.c
index fe41696..8b6c111 100644
--- a/sljit_src/sljitNativeARM_32.c
+++ b/sljit_src/sljitNativeARM_32.c
@@ -98,6 +98,7 @@ static const sljit_u8 freg_map[SLJIT_NUMBER_OF_FLOAT_REGISTERS + 3] = {
 #define CLZ		0xe16f0f10
 #define CMN		0xe1600000
 #define CMP		0xe1400000
+#define CMP_imm		0xE3500000
 #define BKPT		0xe1200070
 #define EOR		0xe0200000
 #define LDR		0xe5100000
@@ -147,6 +148,16 @@ static const sljit_u8 freg_map[SLJIT_NUMBER_OF_FLOAT_REGISTERS + 3] = {
 #define SXTH		0xe6bf0070
 #define UXTB		0xe6ef0070
 #define UXTH		0xe6ff0070
+
+#define LDREX 0x01900F9F
+#define LDREXB 0x01D00F9F
+#define LDREXD 0x01B00F9F
+#define LDREXH 0x01F00F9F
+#define STREX 0x01800F90
+#define STREXB 0x01C00F90
+#define STREXD 0x01A00F90
+#define STREXH 0x01E00F90
+#define CLREX 0xF57FF01F
 #endif
 
 #if (defined SLJIT_CONFIG_ARM_V5 && SLJIT_CONFIG_ARM_V5)
@@ -3848,3 +3859,89 @@ SLJIT_API_FUNC_ATTRIBUTE void sljit_set_const(sljit_uw addr, sljit_sw new_consta
 {
 	inline_set_const(addr, executable_offset, (sljit_uw)new_constant, 1);
 }
+
+SLJIT_API_FUNC_ATTRIBUTE sljit_s32 sljit_emit_atomic_load(struct sljit_compiler *compiler, sljit_s32 op, sljit_s32 base_reg, sljit_s32 data_reg) {
+	sljit_u32 inst = CONDITIONAL;
+
+	switch (GET_OPCODE(op)) {
+		case SLJIT_ATOMIC_LOAD:
+		case SLJIT_ATOMIC_LOAD32:
+		case SLJIT_ATOMIC_LOAD_U32:
+			inst |= LDREX;
+			break;
+		case SLJIT_ATOMIC_LOAD_U8:
+		case SLJIT_ATOMIC_LOAD32_U8:
+			inst |= LDREXB;
+			break;
+		case SLJIT_ATOMIC_LOAD_U16:
+		case SLJIT_ATOMIC_LOAD32_U16:
+			inst |= LDREXH;
+			break;
+		default:
+			SLJIT_UNREACHABLE();
+	}
+	push_inst(compiler, inst | RN(base_reg) | RD(data_reg));
+	return SLJIT_SUCCESS;
+}
+
+SLJIT_API_FUNC_ATTRIBUTE sljit_s32 sljit_emit_atomic_store(struct sljit_compiler *compiler, sljit_s32 op, sljit_s32 base_reg, sljit_s32 data_reg, sljit_s32 temp_reg) {
+	compiler->last_flags |= SLJIT_SET_Z;
+	sljit_u32 inst = CONDITIONAL;
+
+	switch (GET_OPCODE(op)) {
+		case SLJIT_ATOMIC_STORE:
+		case SLJIT_ATOMIC_STORE32:
+		case SLJIT_ATOMIC_STORE_U32:
+			inst |= STREX;
+			break;
+		case SLJIT_ATOMIC_STORE_U8:
+		case SLJIT_ATOMIC_STORE32_U8:
+			inst |= STREXB;
+			break;
+		case SLJIT_ATOMIC_STORE_U16:
+		case SLJIT_ATOMIC_STORE32_U16:
+			inst |= STREXH;
+			break;
+		default:
+			SLJIT_UNREACHABLE();
+	}
+
+	push_inst(compiler, inst | RN(base_reg) | RD(temp_reg) | RM(data_reg));
+	return SLJIT_SUCCESS;
+}
+
+SLJIT_API_FUNC_ATTRIBUTE sljit_s32 sljit_emit_atomic_op1(struct sljit_compiler *compiler, sljit_s32 op, sljit_s32 base_reg, sljit_s32 data_reg, sljit_s32 temp_reg) {
+#if (defined SLJIT_CONFIG_ARM_V7 && SLJIT_CONFIG_ARM_V7)
+	CHECK_ERROR();
+	CHECK(check_sljit_emit_atomic_op1(compiler, op, base_reg, data_reg, temp_reg));
+	SLJIT_ASSERT(data_reg != TMP_REG1 || HAS_FLAGS(op));
+
+	switch (GET_OPCODE(op)) {
+		case SLJIT_ATOMIC_LOAD:
+		case SLJIT_ATOMIC_LOAD32:
+		case SLJIT_ATOMIC_LOAD_U8:
+		case SLJIT_ATOMIC_LOAD32_U8:
+		case SLJIT_ATOMIC_LOAD_U16:
+		case SLJIT_ATOMIC_LOAD32_U16:
+		case SLJIT_ATOMIC_LOAD_U32: {
+			sljit_emit_mem_unaligned(compiler, SLJIT_MOV, data_reg, SLJIT_IMM, 0);
+			return sljit_emit_atomic_load(compiler, op, base_reg, data_reg);
+		}
+		case SLJIT_ATOMIC_STORE:
+		case SLJIT_ATOMIC_STORE32:
+		case SLJIT_ATOMIC_STORE_U16:
+		case SLJIT_ATOMIC_STORE32_U16:
+		case SLJIT_ATOMIC_STORE_U8:
+		case SLJIT_ATOMIC_STORE32_U8:
+		case SLJIT_ATOMIC_STORE_U32: {
+			int ret_val = sljit_emit_atomic_store(compiler, op, base_reg, data_reg, temp_reg);
+			push_inst(compiler, CMP_imm | RN(temp_reg) | 0);
+			return ret_val;
+		}
+		default:
+			SLJIT_UNREACHABLE();
+	}
+#else
+	return SLJIT_ERR_UNSUPPORTED;
+#endif
+}
diff --git a/sljit_src/sljitNativeARM_64.c b/sljit_src/sljitNativeARM_64.c
index af80f21..57d558b 100644
--- a/sljit_src/sljitNativeARM_64.c
+++ b/sljit_src/sljitNativeARM_64.c
@@ -51,7 +51,9 @@ static const sljit_u8 freg_map[SLJIT_NUMBER_OF_FLOAT_REGISTERS + 3] = {
 	0, 0, 1, 2, 3, 4, 5, 6, 7, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 15, 14, 13, 12, 11, 10, 9, 8, 30, 31
 };
 
+#define X_OP ((sljit_ins)1 << 30)
 #define W_OP ((sljit_ins)1 << 31)
+#define IMM12(imm12) ((sljit_ins)imm12 << 10)
 #define RD(rd) ((sljit_ins)reg_map[rd])
 #define RT(rt) ((sljit_ins)reg_map[rt])
 #define RN(rn) ((sljit_ins)reg_map[rn] << 5)
@@ -83,6 +85,7 @@ static const sljit_u8 freg_map[SLJIT_NUMBER_OF_FLOAT_REGISTERS + 3] = {
 #define CBZ 0xb4000000
 #define CCMPI 0xfa400800
 #define CLZ 0xdac01000
+#define CMP_imm 0x7100000F
 #define CSEL 0x9a800000
 #define CSINC 0x9a800400
 #define EOR 0xca000000
@@ -146,6 +149,16 @@ static const sljit_u8 freg_map[SLJIT_NUMBER_OF_FLOAT_REGISTERS + 3] = {
 #define UDIV 0x9ac00800
 #define UMULH 0x9bc03c00
 
+#define LDXR 0x885f7c00
+#define LDXRB 0x085F7C00
+#define LDXRH 0x485F7C00
+#define LDXP 0x887F0000
+#define STXR 0x88007C00
+#define STXRB 0x8007C00
+#define STXRH 0x48007C00
+#define STXP 0x88200000
+#define CLREX 0xD503305F
+
 static sljit_s32 push_inst(struct sljit_compiler *compiler, sljit_ins ins)
 {
 	sljit_ins *ptr = (sljit_ins*)ensure_buf(compiler, sizeof(sljit_ins));
@@ -2525,3 +2538,89 @@ SLJIT_API_FUNC_ATTRIBUTE void sljit_set_const(sljit_uw addr, sljit_sw new_consta
 {
 	sljit_set_jump_addr(addr, (sljit_uw)new_constant, executable_offset);
 }
+
+SLJIT_API_FUNC_ATTRIBUTE sljit_s32 sljit_emit_atomic_load(struct sljit_compiler *compiler, sljit_s32 op, sljit_s32 base_reg, sljit_s32 data_reg) {
+	sljit_u32 inst = 0;
+
+	switch (GET_OPCODE(op)) {
+		case SLJIT_ATOMIC_LOAD:
+			inst = LDXR | X_OP;
+			break;
+		case SLJIT_ATOMIC_LOAD32:
+		case SLJIT_ATOMIC_LOAD_U32:
+			inst = LDXR;
+			break;
+		case SLJIT_ATOMIC_LOAD_U8:
+		case SLJIT_ATOMIC_LOAD32_U8:
+			inst = LDXRB;
+			break;
+		case SLJIT_ATOMIC_LOAD_U16:
+		case SLJIT_ATOMIC_LOAD32_U16:
+			inst = LDXRH;
+			break;
+		default:
+			SLJIT_UNREACHABLE();
+	}
+	push_inst(compiler, inst | RN(base_reg) | RT(data_reg));
+	return SLJIT_SUCCESS;
+}
+
+SLJIT_API_FUNC_ATTRIBUTE sljit_s32 sljit_emit_atomic_store(struct sljit_compiler *compiler, sljit_s32 op, sljit_s32 base_reg, sljit_s32 data_reg, sljit_s32 temp_reg) {
+	sljit_u32 inst = 0;
+
+	switch (GET_OPCODE(op)) {
+		case SLJIT_ATOMIC_STORE:
+			inst = STXR | X_OP;
+			break;
+		case SLJIT_ATOMIC_STORE32:
+		case SLJIT_ATOMIC_STORE_U32:
+			inst = STXR;
+			break;
+
+		case SLJIT_ATOMIC_STORE_U8:
+		case SLJIT_ATOMIC_STORE32_U8:
+			inst = STXRB;
+			break;
+		case SLJIT_ATOMIC_STORE_U16:
+		case SLJIT_ATOMIC_STORE32_U16:
+			inst = STXRH;
+			break;
+		default:
+			SLJIT_UNREACHABLE();
+	}
+
+	push_inst(compiler, inst | RM(temp_reg) | RN(base_reg) | RT(data_reg));
+	return SLJIT_SUCCESS;
+}
+
+SLJIT_API_FUNC_ATTRIBUTE sljit_s32 sljit_emit_atomic_op1(struct sljit_compiler *compiler, sljit_s32 op, sljit_s32 base_reg, sljit_s32 data_reg, sljit_s32 temp_reg) {
+	CHECK_ERROR();
+	CHECK(check_sljit_emit_atomic_op1(compiler, op, base_reg, data_reg, temp_reg));
+	SLJIT_ASSERT(data_reg != TMP_REG1 || HAS_FLAGS(op));
+
+	switch (GET_OPCODE(op)) {
+		case SLJIT_ATOMIC_LOAD:
+		case SLJIT_ATOMIC_LOAD32:
+		case SLJIT_ATOMIC_LOAD_U8:
+		case SLJIT_ATOMIC_LOAD32_U8:
+		case SLJIT_ATOMIC_LOAD_U16:
+		case SLJIT_ATOMIC_LOAD32_U16:
+		case SLJIT_ATOMIC_LOAD_U32: {
+			sljit_emit_mem_unaligned(compiler, SLJIT_MOV, data_reg, SLJIT_IMM, 0);
+			return sljit_emit_atomic_load(compiler, op, base_reg, data_reg);
+		}
+		case SLJIT_ATOMIC_STORE:
+		case SLJIT_ATOMIC_STORE32:
+		case SLJIT_ATOMIC_STORE_U16:
+		case SLJIT_ATOMIC_STORE32_U16:
+		case SLJIT_ATOMIC_STORE_U8:
+		case SLJIT_ATOMIC_STORE32_U8:
+		case SLJIT_ATOMIC_STORE_U32: {
+			int ret_val = sljit_emit_atomic_store(compiler, op, base_reg, data_reg, temp_reg);
+			push_inst(compiler, CMP_imm | IMM12(0) | RN(temp_reg));
+			return ret_val;
+		}
+		default:
+			SLJIT_UNREACHABLE();
+	}
+}
diff --git a/sljit_src/sljitNativeARM_T2_32.c b/sljit_src/sljitNativeARM_T2_32.c
index f9bc2cb..ede317d 100644
--- a/sljit_src/sljitNativeARM_T2_32.c
+++ b/sljit_src/sljitNativeARM_T2_32.c
@@ -216,6 +216,16 @@ static const sljit_u8 freg_map[SLJIT_NUMBER_OF_FLOAT_REGISTERS + 3] = {
 #define VSTR_F32	0xed000a00
 #define VSUB_F32	0xee300a40
 
+#define LDAEX	0xE8D00FEF
+#define LDAEXB	0xE8D00FCF
+#define LDAEXD	0xE8D000FF
+#define LDAEXH	0xE8D00FDF
+#define STLEX	0xE8C00FE0
+#define STLEXB	0xE8C00FC0
+#define STLEXD	0xE8C000F0
+#define STLEXH	0xE8C00FD0
+#define CLREX 0xF3BF8F2F
+
 static sljit_s32 push_inst16(struct sljit_compiler *compiler, sljit_ins inst)
 {
 	sljit_u16 *ptr;
@@ -3282,3 +3292,85 @@ SLJIT_API_FUNC_ATTRIBUTE void sljit_set_const(sljit_uw addr, sljit_sw new_consta
 {
 	sljit_set_jump_addr(addr, (sljit_uw)new_constant, executable_offset);
 }
+
+SLJIT_API_FUNC_ATTRIBUTE sljit_s32 sljit_emit_atomic_load(struct sljit_compiler *compiler, sljit_s32 op, sljit_s32 base_reg, sljit_s32 data_reg) {
+	sljit_u32 inst = 0;
+
+	switch (GET_OPCODE(op)) {
+		case SLJIT_ATOMIC_LOAD:
+		case SLJIT_ATOMIC_LOAD32:
+		case SLJIT_ATOMIC_LOAD_U32:
+			inst = LDAEX;
+			break;
+		case SLJIT_ATOMIC_LOAD_U8:
+		case SLJIT_ATOMIC_LOAD32_U8:
+			inst = LDAEXB;
+			break;
+		case SLJIT_ATOMIC_LOAD_U16:
+		case SLJIT_ATOMIC_LOAD32_U16:
+			inst = LDAEXH;
+			break;
+		default:
+			SLJIT_UNREACHABLE();
+	}
+	push_inst32(compiler, inst | RN4(base_reg) | RT4(data_reg));
+	return SLJIT_SUCCESS;
+}
+
+SLJIT_API_FUNC_ATTRIBUTE sljit_s32 sljit_emit_atomic_store(struct sljit_compiler *compiler, sljit_s32 op, sljit_s32 base_reg, sljit_s32 data_reg, sljit_s32 temp_reg) {
+	compiler->last_flags |= SLJIT_SET_Z;
+	sljit_u32 inst = 0;
+
+	switch (GET_OPCODE(op)) {
+		case SLJIT_ATOMIC_STORE:
+		case SLJIT_ATOMIC_STORE32:
+		case SLJIT_ATOMIC_STORE_U32:
+			inst = STLEX;
+			break;
+		case SLJIT_ATOMIC_STORE_U8:
+		case SLJIT_ATOMIC_STORE32_U8:
+			inst = STLEXB;
+			break;
+		case SLJIT_ATOMIC_STORE_U16:
+		case SLJIT_ATOMIC_STORE32_U16:
+			inst = STLEXH;
+			break;
+		default:
+			SLJIT_UNREACHABLE();
+	}
+
+	push_inst32(compiler, inst | RN4(base_reg) | RT4(data_reg)| RM4(temp_reg) );
+	return SLJIT_SUCCESS;
+}
+
+SLJIT_API_FUNC_ATTRIBUTE sljit_s32 sljit_emit_atomic_op1(struct sljit_compiler *compiler, sljit_s32 op, sljit_s32 base_reg, sljit_s32 data_reg, sljit_s32 temp_reg) {
+	CHECK_ERROR();
+	CHECK(check_sljit_emit_atomic_op1(compiler, op, base_reg, data_reg, temp_reg));
+	SLJIT_ASSERT(data_reg != TMP_REG1 || HAS_FLAGS(op));
+
+	switch (GET_OPCODE(op)) {
+		case SLJIT_ATOMIC_LOAD:
+		case SLJIT_ATOMIC_LOAD32:
+		case SLJIT_ATOMIC_LOAD_U8:
+		case SLJIT_ATOMIC_LOAD32_U8:
+		case SLJIT_ATOMIC_LOAD_U16:
+		case SLJIT_ATOMIC_LOAD32_U16:
+		case SLJIT_ATOMIC_LOAD_U32: {
+			sljit_emit_mem_unaligned(compiler, SLJIT_MOV, data_reg, SLJIT_IMM, 0);
+			return sljit_emit_atomic_load(compiler, op, base_reg, data_reg);
+		}
+		case SLJIT_ATOMIC_STORE:
+		case SLJIT_ATOMIC_STORE32:
+		case SLJIT_ATOMIC_STORE_U16:
+		case SLJIT_ATOMIC_STORE32_U16:
+		case SLJIT_ATOMIC_STORE_U8:
+		case SLJIT_ATOMIC_STORE32_U8:
+		case SLJIT_ATOMIC_STORE_U32: {
+			int ret_val = sljit_emit_atomic_store(compiler, op, base_reg, data_reg, temp_reg);
+			push_inst16(compiler, CMP | IMM8(0) | RDN3(temp_reg));
+			return ret_val;
+		}
+		default:
+			SLJIT_UNREACHABLE();
+	}
+}
diff --git a/sljit_src/sljitNativeX86_common.c b/sljit_src/sljitNativeX86_common.c
index 1c41d7c..439839b 100644
--- a/sljit_src/sljitNativeX86_common.c
+++ b/sljit_src/sljitNativeX86_common.c
@@ -276,6 +276,12 @@ static const sljit_u8 freg_lmap[SLJIT_NUMBER_OF_FLOAT_REGISTERS + 1] = {
 #define XOR_rm_r	0x31
 #define XORPD_x_xm	0x57
 
+#define LOCK	0xf0
+#define DATA16	0x66
+#define CMPXCHG_1B	0xb0
+#define CMPXCHG	0xb1
+#define CMPXCHG8B	0xc7
+
 #define GROUP_0F	0x0f
 #define GROUP_66	0x66
 #define GROUP_F3	0xf3
@@ -3650,3 +3656,189 @@ SLJIT_API_FUNC_ATTRIBUTE void sljit_set_const(sljit_uw addr, sljit_sw new_consta
 	sljit_unaligned_store_sw((void*)addr, new_constant);
 	SLJIT_UPDATE_WX_FLAGS((void*)addr, (void*)(addr + sizeof(sljit_sw)), 1);
 }
+
+#define emit_single_byte_inst(instr)                 \
+	inst = (sljit_u8 *) ensure_buf(compiler, 1 + 1); \
+	FAIL_IF(!inst);                                  \
+	INC_SIZE(1);                                     \
+	*inst = instr;
+
+#define emit_double_inst(group, instr, source, source_w, dest, dest_w)        \
+	inst = emit_x86_instruction(compiler, 2, source, source_w, dest, dest_w); \
+	FAIL_IF(!inst);                                                           \
+	*inst++ = group;                                                          \
+	*inst = instr;
+
+SLJIT_API_FUNC_ATTRIBUTE sljit_s32 sljit_emit_atomic_load(struct sljit_compiler *compiler, sljit_s32 op, sljit_s32 base_reg, sljit_s32 data_reg) {
+	EMIT_MOV(compiler, data_reg, 0, SLJIT_IMM, 0);
+#if (defined SLJIT_CONFIG_X86_32 && SLJIT_CONFIG_X86_32)
+	switch (GET_OPCODE(op)) {
+		case SLJIT_ATOMIC_LOAD:
+		case SLJIT_ATOMIC_LOAD32:
+		case SLJIT_ATOMIC_LOAD_U32:
+			sljit_emit_mem_unaligned(compiler, SLJIT_MOV, data_reg, SLJIT_MEM1(base_reg), 0);
+			break;
+		case SLJIT_ATOMIC_LOAD_U8:
+		case SLJIT_ATOMIC_LOAD32_U8:
+			sljit_emit_mem_unaligned(compiler, SLJIT_MOV_U8, data_reg, SLJIT_MEM1(base_reg), 0);
+			break;
+		case SLJIT_ATOMIC_LOAD_U16:
+		case SLJIT_ATOMIC_LOAD32_U16:
+			sljit_emit_mem_unaligned(compiler, SLJIT_MOV_U16, data_reg, SLJIT_MEM1(base_reg), 0);
+			break;
+		default:
+			SLJIT_UNREACHABLE();
+	}
+#else
+	switch (GET_OPCODE(op)) {
+		case SLJIT_ATOMIC_LOAD:
+			sljit_emit_mem_unaligned(compiler, SLJIT_MOV, data_reg, SLJIT_MEM1(base_reg), 0);
+			break;
+		case SLJIT_ATOMIC_LOAD32:
+		case SLJIT_ATOMIC_LOAD_U32:
+			sljit_emit_mem_unaligned(compiler, SLJIT_MOV_U32, data_reg, SLJIT_MEM1(base_reg), 0);
+			break;
+
+		case SLJIT_ATOMIC_LOAD_U8:
+		case SLJIT_ATOMIC_LOAD32_U8:
+			sljit_emit_mem_unaligned(compiler, SLJIT_MOV_U8, data_reg, SLJIT_MEM1(base_reg), 0);
+			break;
+
+		case SLJIT_ATOMIC_LOAD_U16:
+		case SLJIT_ATOMIC_LOAD32_U16:
+			sljit_emit_mem_unaligned(compiler, SLJIT_MOV_U16, data_reg, SLJIT_MEM1(base_reg), 0);
+			break;
+		default:
+			SLJIT_UNREACHABLE();
+	}
+#endif
+	return SLJIT_SUCCESS;
+}
+
+SLJIT_API_FUNC_ATTRIBUTE sljit_s32 sljit_emit_atomic_store(struct sljit_compiler *compiler, sljit_s32 op, sljit_s32 base_reg, sljit_s32 data_reg, sljit_s32 temp_reg) {
+	sljit_u8 *inst;
+	sljit_s32 reg4 = -1;
+
+#if (defined SLJIT_CONFIG_X86_64 && SLJIT_CONFIG_X86_64)
+	compiler->mode32 = 0;
+#endif
+	EMIT_MOV(compiler, SLJIT_MEM1(SLJIT_SP), 0 * sizeof(sljit_sw), SLJIT_R0, 0);
+	if (base_reg == SLJIT_R0 || data_reg == SLJIT_R0 || temp_reg == SLJIT_R0) {
+		reg4 = (base_reg == SLJIT_R1 || data_reg == SLJIT_R1 || temp_reg == SLJIT_R1 ? (base_reg == SLJIT_R2 || data_reg == SLJIT_R2 || temp_reg == SLJIT_R2 ? SLJIT_R3 : SLJIT_R2) : SLJIT_R1);
+		EMIT_MOV(compiler, SLJIT_MEM1(SLJIT_SP), 1 * sizeof(sljit_sw), reg4, 0);
+		EMIT_MOV(compiler, reg4, 0, SLJIT_R0, 0);
+		if (base_reg == SLJIT_R0) {
+			base_reg = reg4;
+		} else if (data_reg == SLJIT_R0) {
+			data_reg = reg4;
+		} else if (temp_reg == SLJIT_R0) {
+			temp_reg = reg4;
+		}
+	} else {
+		EMIT_MOV(compiler, SLJIT_MEM1(SLJIT_SP), 1 * sizeof(sljit_sw), SLJIT_R0, 0);
+	}
+	EMIT_MOV(compiler, SLJIT_R0, 0, SLJIT_MEM1(base_reg), 0);
+#if (defined SLJIT_CONFIG_X86_64 && SLJIT_CONFIG_X86_64)
+	compiler->mode32 = op & SLJIT_32;
+#endif
+
+#if (defined SLJIT_CONFIG_X86_32 && SLJIT_CONFIG_X86_32)
+	switch (GET_OPCODE(op)) {
+		case SLJIT_ATOMIC_STORE:
+		case SLJIT_ATOMIC_STORE32:
+		case SLJIT_ATOMIC_STORE_U32:
+			break;
+
+		case SLJIT_ATOMIC_STORE_U8:
+		case SLJIT_ATOMIC_STORE32_U8:
+			emit_single_byte_inst(LOCK);
+			emit_double_inst(GROUP_0F, CMPXCHG_1B, data_reg, 0, SLJIT_MEM1(base_reg), 0);
+			return SLJIT_SUCCESS;
+
+		case SLJIT_ATOMIC_STORE_U16:
+		case SLJIT_ATOMIC_STORE32_U16:
+			emit_single_byte_inst(DATA16) break;
+
+		default:
+			SLJIT_UNREACHABLE();
+	}
+	emit_single_byte_inst(LOCK);
+	emit_double_inst(GROUP_0F, CMPXCHG, data_reg, 0, SLJIT_MEM1(base_reg), 0);
+#else
+	switch (GET_OPCODE(op)) {
+		case SLJIT_ATOMIC_STORE:
+		case SLJIT_ATOMIC_STORE32:
+			break;
+
+		case SLJIT_ATOMIC_STORE_U8:
+		case SLJIT_ATOMIC_STORE32_U8:
+			compiler->mode32 = 0;
+			emit_single_byte_inst(LOCK);
+			emit_double_inst(GROUP_0F, CMPXCHG_1B, data_reg, 0, SLJIT_MEM1(base_reg), 0);
+			return SLJIT_SUCCESS;
+
+		case SLJIT_ATOMIC_STORE_U16:
+		case SLJIT_ATOMIC_STORE32_U16:
+			compiler->mode32 = SLJIT_32;
+			emit_single_byte_inst(DATA16) break;
+
+		case SLJIT_ATOMIC_STORE_U32:
+			compiler->mode32 = SLJIT_32;
+			break;
+
+		default:
+			SLJIT_UNREACHABLE();
+	}
+	emit_single_byte_inst(LOCK);
+	emit_double_inst(GROUP_0F, CMPXCHG, data_reg, 0, SLJIT_MEM1(base_reg), 0);
+#endif
+#if (defined SLJIT_CONFIG_X86_64 && SLJIT_CONFIG_X86_64)
+	compiler->mode32 = 0;
+#endif
+	if (reg4 != -1) {
+		EMIT_MOV(compiler, SLJIT_R0, 0, reg4, 0);
+		EMIT_MOV(compiler, reg4, 0, SLJIT_MEM1(SLJIT_SP), 1 * sizeof(sljit_sw));
+	} else {
+		EMIT_MOV(compiler, SLJIT_R0, 0, SLJIT_MEM1(SLJIT_SP), 1 * sizeof(sljit_sw));
+	}
+#if (defined SLJIT_CONFIG_X86_64 && SLJIT_CONFIG_X86_64)
+	compiler->mode32 = op & SLJIT_32;
+#endif
+
+	return SLJIT_SUCCESS;
+}
+
+SLJIT_API_FUNC_ATTRIBUTE sljit_s32 sljit_emit_atomic_op1(struct sljit_compiler *compiler, sljit_s32 op, sljit_s32 base_reg, sljit_s32 data_reg, sljit_s32 temp_reg) {
+	CHECK_ERROR();
+	CHECK(check_sljit_emit_atomic_op1(compiler, op, base_reg, data_reg, temp_reg));
+
+#if (defined SLJIT_CONFIG_X86_64 && SLJIT_CONFIG_X86_64)
+	compiler->mode32 = op & SLJIT_32;
+#endif
+	SLJIT_ASSERT(data_reg != TMP_REG1 || HAS_FLAGS(op));
+
+	switch (GET_OPCODE(op)) {
+		case SLJIT_ATOMIC_LOAD:
+		case SLJIT_ATOMIC_LOAD32:
+		case SLJIT_ATOMIC_LOAD_U8:
+		case SLJIT_ATOMIC_LOAD32_U8:
+		case SLJIT_ATOMIC_LOAD_U16:
+		case SLJIT_ATOMIC_LOAD32_U16:
+		case SLJIT_ATOMIC_LOAD_U32: {
+			return sljit_emit_atomic_load(compiler, op, base_reg, data_reg);
+		}
+		case SLJIT_ATOMIC_STORE_U32:
+		case SLJIT_ATOMIC_STORE:
+		case SLJIT_ATOMIC_STORE32:
+		case SLJIT_ATOMIC_STORE_U16:
+		case SLJIT_ATOMIC_STORE32_U16:
+		case SLJIT_ATOMIC_STORE_U8:
+		case SLJIT_ATOMIC_STORE32_U8: {
+			return sljit_emit_atomic_store(compiler, op, base_reg, data_reg, temp_reg);
+		}
+		default:
+			SLJIT_UNREACHABLE();
+	}
+#undef emit_single_byte_inst
+#undef emit_double_inst
+}
diff --git a/test_src/sljitTest.c b/test_src/sljitTest.c
index 7bb2a5e..ff0565e 100644
--- a/test_src/sljitTest.c
+++ b/test_src/sljitTest.c
@@ -11289,7 +11289,7 @@ static void test90(void)
 	sljit_s32 i;
 
 	if (verbose)
-		printf("Run test89\n");
+		printf("Run test90\n");
 
 	FAILED(!compiler, "cannot create compiler\n");
 
@@ -11391,7 +11391,7 @@ static void test91(void)
 	sljit_s32 i;
 
 	if (verbose)
-		printf("Run test13\n");
+		printf("Run test91\n");
 
 	if (!sljit_has_cpu_feature(SLJIT_HAS_FPU)) {
 		if (verbose)
@@ -11516,6 +11516,96 @@ static void test91(void)
 	successful_tests++;
 }
 
+static void test92(void)
+{
+	/* Test atomic load and store. */
+	executable_code code;
+	struct sljit_compiler *compiler = sljit_create_compiler(NULL, NULL);
+#define datatype u_int64_t
+	datatype buf[7];
+
+	if (verbose)
+		printf("Run test92\n");
+
+	FAILED(!compiler, "cannot create compiler\n");
+
+	buf[0] = 0xffffffffffffffff;
+	buf[1] = 0xffffffffffffffff;
+	buf[2] = 0xffffffffffffffff;
+	buf[3] = 0xffffffffffffffff;
+	buf[4] = 0xffffffffffffffff;
+	buf[5] = 0xffffffffffffffff;
+	buf[6] = 0xffffffffffffffff;
+
+	sljit_emit_enter(compiler, 0, SLJIT_ARGS1(W, P), 7, 5, 4, 0, 2 * sizeof(sljit_sw));
+
+	/* buf[0] */
+	sljit_emit_op2(compiler, SLJIT_ADD, SLJIT_R0, 0, SLJIT_S0, 0, SLJIT_IMM, 0 * sizeof(u_int64_t));
+	sljit_emit_atomic_op1(compiler, SLJIT_ATOMIC_LOAD, SLJIT_R0, SLJIT_R2, SLJIT_R3);
+	sljit_emit_op2(compiler, SLJIT_ADD, SLJIT_R2, 0, SLJIT_R2, 0, SLJIT_IMM, (sljit_sw)0x1122334455667789);
+	sljit_emit_atomic_op1(compiler, SLJIT_ATOMIC_STORE | SLJIT_SET_Z, SLJIT_R0, SLJIT_R2, SLJIT_R3);
+
+	/* buf[1] */
+	sljit_emit_op2(compiler, SLJIT_ADD, SLJIT_R0, 0, SLJIT_S0, 0, SLJIT_IMM, 1 * sizeof(u_int64_t));
+	sljit_emit_atomic_op1(compiler, SLJIT_ATOMIC_LOAD32, SLJIT_R0, SLJIT_R2, SLJIT_R3);
+	sljit_emit_op2(compiler, SLJIT_ADD, SLJIT_R2, 0, SLJIT_R2, 0, SLJIT_IMM, (sljit_sw)0x1122334455667789);
+	sljit_emit_atomic_op1(compiler, SLJIT_ATOMIC_STORE32 | SLJIT_SET_Z, SLJIT_R0, SLJIT_R2, SLJIT_R3);
+
+	/* buf[2] */
+	sljit_emit_op2(compiler, SLJIT_ADD, SLJIT_R0, 0, SLJIT_S0, 0, SLJIT_IMM, 2 * sizeof(u_int64_t));
+	sljit_emit_atomic_op1(compiler, SLJIT_ATOMIC_LOAD_U8, SLJIT_R0, SLJIT_R2, SLJIT_R3);
+	sljit_emit_op2(compiler, SLJIT_ADD, SLJIT_R2, 0, SLJIT_R2, 0, SLJIT_IMM, (sljit_sw)0x1122334455667789);
+	sljit_emit_atomic_op1(compiler, SLJIT_ATOMIC_STORE_U8 | SLJIT_SET_Z, SLJIT_R0, SLJIT_R2, SLJIT_R3);
+
+	/* buf[3] */
+	sljit_emit_op2(compiler, SLJIT_ADD, SLJIT_R0, 0, SLJIT_S0, 0, SLJIT_IMM, 3 * sizeof(u_int64_t));
+	sljit_emit_atomic_op1(compiler, SLJIT_ATOMIC_LOAD32_U8, SLJIT_R0, SLJIT_R2, SLJIT_R3);
+	sljit_emit_op2(compiler, SLJIT_ADD, SLJIT_R2, 0, SLJIT_R2, 0, SLJIT_IMM, (sljit_sw)0x1122334455667789);
+	sljit_emit_atomic_op1(compiler, SLJIT_ATOMIC_STORE32_U8 | SLJIT_SET_Z, SLJIT_R0, SLJIT_R2, SLJIT_R3);
+
+	/* buf[4] */
+	sljit_emit_op2(compiler, SLJIT_ADD, SLJIT_R0, 0, SLJIT_S0, 0, SLJIT_IMM, 4 * sizeof(u_int64_t));
+	sljit_emit_atomic_op1(compiler, SLJIT_ATOMIC_LOAD_U16, SLJIT_R0, SLJIT_R2, SLJIT_R3);
+	sljit_emit_op2(compiler, SLJIT_ADD, SLJIT_R2, 0, SLJIT_R2, 0, SLJIT_IMM, (sljit_sw)0x1122334455667789);
+	sljit_emit_atomic_op1(compiler, SLJIT_ATOMIC_STORE_U16 | SLJIT_SET_Z, SLJIT_R0, SLJIT_R2, SLJIT_R3);
+
+	/* buf[5] */
+	sljit_emit_op2(compiler, SLJIT_ADD, SLJIT_R0, 0, SLJIT_S0, 0, SLJIT_IMM, 5 * sizeof(u_int64_t));
+	sljit_emit_atomic_op1(compiler, SLJIT_ATOMIC_LOAD32_U16, SLJIT_R0, SLJIT_R2, SLJIT_R3);
+	sljit_emit_op2(compiler, SLJIT_ADD, SLJIT_R2, 0, SLJIT_R2, 0, SLJIT_IMM, (sljit_sw)0x1122334455667789);
+	sljit_emit_atomic_op1(compiler, SLJIT_ATOMIC_STORE32_U16 | SLJIT_SET_Z, SLJIT_R0, SLJIT_R2, SLJIT_R3);
+
+	/* buf[6] */
+	sljit_emit_op2(compiler, SLJIT_ADD, SLJIT_R0, 0, SLJIT_S0, 0, SLJIT_IMM, 6 * sizeof(u_int64_t));
+	sljit_emit_atomic_op1(compiler, SLJIT_ATOMIC_LOAD_U32 , SLJIT_R0, SLJIT_R2, SLJIT_R3);
+	sljit_emit_op2(compiler, SLJIT_ADD, SLJIT_R2, 0, SLJIT_R2, 0, SLJIT_IMM, (sljit_sw)0x1122334455667789);
+	sljit_emit_atomic_op1(compiler, SLJIT_ATOMIC_STORE_U32 | SLJIT_SET_Z, SLJIT_R0, SLJIT_R2, SLJIT_R3);
+
+	sljit_emit_op1(compiler, SLJIT_MOV, SLJIT_R0, 0, SLJIT_IMM, 0);
+	sljit_emit_return(compiler, SLJIT_MOV, SLJIT_RETURN_REG, 0);
+
+	code.code = sljit_generate_code(compiler);
+	CHECK(compiler);
+	sljit_free_compiler(compiler);
+
+	code.func1((sljit_sw) &buf);
+#if (defined SLJIT_64BIT_ARCHITECTURE && SLJIT_64BIT_ARCHITECTURE)
+	FAILED(buf[0] != 0x1122334455667788, "test92 case 1 failed\n");
+#else
+	FAILED(buf[0] != 0xffffffff55667788, "test92 case 1 failed\n");
+#endif
+	FAILED(buf[1] != 0xffffffff55667788, "test92 case 2 failed\n");
+	FAILED(buf[2] != 0xffffffffffffff88, "test92 case 3 failed\n");
+	FAILED(buf[3] != 0xffffffffffffff88, "test92 case 4 failed\n");
+	FAILED(buf[4] != 0xffffffffffff7788, "test92 case 5 failed\n");
+	FAILED(buf[5] != 0xffffffffffff7788, "test92 case 6 failed\n");
+	FAILED(buf[6] != 0xffffffff55667788, "test92 case 7 failed\n");
+
+	sljit_free_code(code.code, NULL);
+	successful_tests++;
+#undef datatype
+}
+
 int sljit_test(int argc, char* argv[])
 {
 	sljit_s32 has_arg = (argc >= 2 && argv[1][0] == '-' && argv[1][2] == '\0');
@@ -11620,12 +11710,13 @@ int sljit_test(int argc, char* argv[])
 	test89();
 	test90();
 	test91();
+	test92();
 
 #if (defined SLJIT_EXECUTABLE_ALLOCATOR && SLJIT_EXECUTABLE_ALLOCATOR)
 	sljit_free_unused_memory_exec();
 #endif
 
-#	define TEST_COUNT 91
+#	define TEST_COUNT 92
 
 	printf("SLJIT tests: ");
 	if (successful_tests == TEST_COUNT)
-- 
2.40.0

