(module
  (memory 1 1 shared)
  (func (export "atomic_test1")
    i32.const 0 i32.atomic.load align=4 drop
    i32.const 0 i64.atomic.load align=8 drop
;;    i32.const 0 i32.atomic.load8_u align=1 drop
;;    i32.const 0 i32.atomic.load16_u align=2 drop
;;    i32.const 0 i64.atomic.load8_u align=1 drop
;;    i32.const 0 i64.atomic.load16_u align=2 drop
;;    i32.const 0 i64.atomic.load32_u align=4 drop

    i32.const 0 i32.const 0 i32.atomic.store align=4
    i32.const 0 i64.const 0 i64.atomic.store align=8
;;    i32.const 0 i32.const 0 i32.atomic.store8 align=1
;;    i32.const 0 i32.const 0 i32.atomic.store16 align=2
;;    i32.const 0 i64.const 0 i64.atomic.store8 align=1
;;    i32.const 0 i64.const 0 i64.atomic.store16 align=2
;;    i32.const 0 i64.const 0 i64.atomic.store32 align=4

    i32.const 0 i32.const 0 i32.atomic.rmw.add align=4 drop
    i32.const 0 i64.const 0 i64.atomic.rmw.add align=8 drop
;;    i32.const 0 i32.const 0 i32.atomic.rmw8.add_u align=1 drop
;;    i32.const 0 i32.const 0 i32.atomic.rmw16.add_u align=2 drop
;;    i32.const 0 i64.const 0 i64.atomic.rmw8.add_u align=1 drop
;;    i32.const 0 i64.const 0 i64.atomic.rmw16.add_u align=2 drop
;;    i32.const 0 i64.const 0 i64.atomic.rmw32.add_u align=4 drop

    i32.const 0 i32.const 0 i32.atomic.rmw.sub align=4 drop
    i32.const 0 i64.const 0 i64.atomic.rmw.sub align=8 drop
;;    i32.const 0 i32.const 0 i32.atomic.rmw8.sub_u align=1 drop
;;    i32.const 0 i32.const 0 i32.atomic.rmw16.sub_u align=2 drop
;;    i32.const 0 i64.const 0 i64.atomic.rmw8.sub_u align=1 drop
;;    i32.const 0 i64.const 0 i64.atomic.rmw16.sub_u align=2 drop
;;    i32.const 0 i64.const 0 i64.atomic.rmw32.sub_u align=4 drop

    i32.const 0 i32.const 0 i32.atomic.rmw.and align=4 drop
    i32.const 0 i64.const 0 i64.atomic.rmw.and align=8 drop
;;    i32.const 0 i32.const 0 i32.atomic.rmw8.and_u align=1 drop
;;    i32.const 0 i32.const 0 i32.atomic.rmw16.and_u align=2 drop
;;    i32.const 0 i64.const 0 i64.atomic.rmw8.and_u align=1 drop
;;    i32.const 0 i64.const 0 i64.atomic.rmw16.and_u align=2 drop
;;    i32.const 0 i64.const 0 i64.atomic.rmw32.and_u align=4 drop

    i32.const 0 i32.const 0 i32.atomic.rmw.or align=4 drop
    i32.const 0 i64.const 0 i64.atomic.rmw.or align=8 drop
;;    i32.const 0 i32.const 0 i32.atomic.rmw8.or_u align=1 drop
;;    i32.const 0 i32.const 0 i32.atomic.rmw16.or_u align=2 drop
;;    i32.const 0 i64.const 0 i64.atomic.rmw8.or_u align=1 drop
;;    i32.const 0 i64.const 0 i64.atomic.rmw16.or_u align=2 drop
;;    i32.const 0 i64.const 0 i64.atomic.rmw32.or_u align=4 drop

    i32.const 0 i32.const 0 i32.atomic.rmw.xor align=4 drop
    i32.const 0 i64.const 0 i64.atomic.rmw.xor align=8 drop
;;    i32.const 0 i32.const 0 i32.atomic.rmw8.xor_u align=1 drop
;;    i32.const 0 i32.const 0 i32.atomic.rmw16.xor_u align=2 drop
;;    i32.const 0 i64.const 0 i64.atomic.rmw8.xor_u align=1 drop
;;    i32.const 0 i64.const 0 i64.atomic.rmw16.xor_u align=2 drop
;;    i32.const 0 i64.const 0 i64.atomic.rmw32.xor_u align=4 drop

    i32.const 0 i32.const 0 i32.atomic.rmw.xchg align=4 drop
    i32.const 0 i64.const 0 i64.atomic.rmw.xchg align=8 drop
;;    i32.const 0 i32.const 0 i32.atomic.rmw8.xchg_u align=1 drop
;;    i32.const 0 i32.const 0 i32.atomic.rmw16.xchg_u align=2 drop
;;    i32.const 0 i64.const 0 i64.atomic.rmw8.xchg_u align=1 drop
;;    i32.const 0 i64.const 0 i64.atomic.rmw16.xchg_u align=2 drop
;;    i32.const 0 i64.const 0 i64.atomic.rmw32.xchg_u align=4 drop

    i32.const 0 i32.const 0 i32.const 0 i32.atomic.rmw.cmpxchg align=4 drop
    i32.const 0 i64.const 0 i64.const 0 i64.atomic.rmw.cmpxchg align=8 drop
;;    i32.const 0 i32.const 0 i32.const 0 i32.atomic.rmw8.cmpxchg_u align=1 drop
;;    i32.const 0 i32.const 0 i32.const 0 i32.atomic.rmw16.cmpxchg_u align=2 drop
;;    i32.const 0 i64.const 0 i64.const 0 i64.atomic.rmw8.cmpxchg_u align=1 drop
;;    i32.const 0 i64.const 0 i64.const 0 i64.atomic.rmw16.cmpxchg_u align=2 drop
;;    i32.const 0 i64.const 0 i64.const 0 i64.atomic.rmw32.cmpxchg_u align=4 drop
  )

;;  (func (export "i32_wrap_i64") (result i32 i32 i32 i32 i32 i32 i32 i32 i32 i32)
;;    i64.const 2147483647
;;    i32.wrap_i64
;;    i64.const 2147483648
;;    i32.wrap_i64
;;    i64.const 2147483649
;;    i32.wrap_i64
;;    i64.const 4294967296
;;    i32.wrap_i64
;;    i64.const 4294967297
;;    i32.wrap_i64
;;    i64.const 4294967290
;;    i32.wrap_i64
;;    i64.const 8589934592
;;    i32.wrap_i64
;;    i64.const 8589934593
;;    i32.wrap_i64
;;    i64.const 12
;;    i32.wrap_i64
;;    i64.const 120
;;    i32.wrap_i64
;;  ) ;; expected: 2147483647,-2147483648,-2147483647,0,1,-6,0,1,12,120 // in js
    ;; or this: i32:2147483647, i32:2147483648, i32:2147483649, i32:0, i32:1, i32:4294967290, i32:0, i32:1, i32:12, i32:120 // wasm-interp non-jit

;;  (func (export "i64_extend_i32_s") (result i64 i64 i64 i64 i64 i64 i64 i64 i64 i64 i64 i64)
;;    i32.const 0
;;    i64.extend_i32_s
;;    i32.const 1
;;    i64.extend_i32_s
;;    i32.const 12
;;    i64.extend_i32_s
;;    i32.const 120
;;    i64.extend_i32_s
;;    i32.const 2147483647
;;    i64.extend_i32_s
;;    i32.const 2147483648
;;    i64.extend_i32_s
;;    i32.const 2147483649
;;    i64.extend_i32_s
;;
;;    i32.const -1
;;    i64.extend_i32_s
;;    i32.const -12
;;    i64.extend_i32_s
;;    i32.const -120
;;    i64.extend_i32_s
;;    i32.const -2147483647
;;    i64.extend_i32_s
;;    i32.const -2147483648
;;    i64.extend_i32_s
;;  ) ;; expected: 0,1,12,120,2147483647,-2147483648,-2147483647,-1,-12,-120,-2147483647,-2147483648 // from js
    ;; expected: i64:0, i64:1, i64:12, i64:120, i64:2147483647, i64:18446744071562067968, i64:18446744071562067969, i64:18446744073709551615, i64:18446744073709551604, i64:18446744073709551496, i64:18446744071562067969, i64:18446744071562067968 // from wabt-interp

;;  (func (export "i64_extend_i32_u") (result i64 i64 i64 i64 i64 i64 i64 i64 i64 i64 i64 i64)
;;    i32.const 0
;;    i64.extend_i32_u
;;    i32.const 1
;;    i64.extend_i32_u
;;    i32.const 12
;;    i64.extend_i32_u
;;    i32.const 120
;;    i64.extend_i32_u
;;    i32.const 2147483647
;;    i64.extend_i32_u
;;    i32.const 2147483648
;;    i64.extend_i32_u
;;    i32.const 2147483649
;;    i64.extend_i32_u
;;
;;    i32.const -1
;;    i64.extend_i32_u
;;    i32.const -12
;;    i64.extend_i32_u
;;    i32.const -120
;;    i64.extend_i32_u
;;    i32.const -2147483647
;;    i64.extend_i32_u
;;    i32.const -2147483648
;;    i64.extend_i32_u
;;  ) ;; expected: 0,1,12,120,2147483647,2147483648,2147483649,4294967295,4294967284,4294967176,2147483649,2147483648 // from js
;;    ;; expected: i64:0, i64:1, i64:12, i64:120, i64:2147483647, i64:2147483648, i64:2147483649, i64:4294967295, i64:4294967284, i64:4294967176, i64:2147483649, i64:2147483648 // from wabt-interp


;;  (func (export "packed_i32") (result i32 i32 i32  i32 )
;;     i32.const 32
;;     i32.const 3232
;;     i32.const 320032
;;     i32.const 32000032
;;  )
;;
;;  (func (export "packed_i64") (result i64 i64 i64 i64 )
;;     i64.const 16448
;;     i64.const 16448
;;     i64.const 16448
;;     i64.const 16448
;;  )
;;
;;  (func (export "mixed_i32_i64") (result i64 i32 i64 i32 i64 )
;;     i64.const 16448
;;     i32.const 32
;;     i64.const 16448
;;     i32.const 32
;;     i64.const 16448
;;  )
;;
;;  (func (export "sparse_i32_i64") (result i32 i32 i64 i32 i32 i32 i64 i64 i64 i32 i64 i32 i32 i32 i32 i64)
;;     i32.const 32
;;     i32.const 32
;;     i64.const 16448
;;     i32.const 32
;;     i32.const 32
;;     i32.const 32
;;     i64.const 16448
;;     i64.const 16448
;;     i64.const 16448
;;     i32.const 32
;;     i64.const 16448
;;     i32.const 32
;;     i32.const 32
;;     i32.const 32
;;     i32.const 32
;;     i64.const 16448
;;
;;  )
;;
;;  (func (export "sparse_i32_i64_f32_f64") (result i32 i32 i64 f32 f32 f32 i64 i64 f64 i32 i64 i32 f32 i32 f32 i64)
;;     i32.const 32
;;     i32.const 32
;;     i64.const 16448
;;     f32.const 32.010101010
;;     f32.const 32.010101010
;;     f32.const 32.010101010
;;     i64.const 16448
;;     i64.const 16448
;;     f64.const 16448.2121212121212121
;;     i32.const 32
;;     i64.const 16448
;;     i32.const 32
;;     f32.const 32.010101010
;;     i32.const 32
;;     f32.const 32.010101010
;;     i64.const 16448
;;  )
;;  (func (export "mixed_i32_f32") (result i32 f32 i64 f64 )
;;     i32.const 32
;;     f32.const 3.2
;;     i64.const 64
;;     f64.const 6.4
;;  )
;;  (func (export "mixed_i32_f32") (result i32 f32 i64 f64 )
;;     i32.const 32
;;     f32.const 3.2
;;     i64.const 64
;;     f64.const 6.4
;; )
)
