Processzusok kommunikációja, vészhelyzetek, kölcsönös kizárás

A processzusoknak gyakran szükségük van az egymással való kommunikációra (IPC, InterProcess Communication).
Pl: parancsértelmező adatcső

Három fő terület:
- Hogyan tud egy processzus információt küldeni egy másiknak?
- Hogyan biztosítsuk, hogy kettő vagy több process ne keresztezze egymás útját, amikor kritikus a tevékenység?
- Függőség esetén hogyan állítsuk sorrendbe a processeket?

Vannak operációs rendszerek, ahol az együtt dolgozó processzusok közös tárolóterületen osztozhatnak.
Ez lehet főmemória vagy megosztott file.

Versenyhelyzet: Amikor kettő vagy több process ír vagy olvas megosztott adatokat, és a végeredmény attól függ, hogy ki és pontosan mikor fut.

A versenyhelyzeteket tartalmazó programokat igen nehéz nyomon követni, tesztelés során néha előfordulnak furcsa és látszólag megmagyarázhatatlan dolgok.

Kölcsönös kizárás: mód a versenyhelyzetek elkerülésére. A baj megelőzésének kulcs, hogy olyan módot találjunk, hogy egy időben egynél több process ne tudja írni és/vagy olvasni a megosztott adatokat.

Bármely operációs rendszer egyik fő tervezési szempontja, hogy megválasszuk az alkalmas primitív műveleteket a kölcsönös kizárás eléréséhez. A konkurens processzusok küzdenek a CPU-ért egymással.

Konkurens process:
- Küzdenek a CPU-ért

Kooperatív process:
- lemondanak a CPU használatáról bizonyos időnként
- semmi sem kötelezi, tehát tetszőleges ideig lefoglalhatja az erőforrást, a többi folyamatot várakozásra kényszerítve.


Kritikus szekciók és megvalósítási módszereik:

A programnak azt a részét, amelyben a megosztott memóriát használja, kritikus területnek vagy kritikus szekciónak nevezzük.
Ha úgy tudnánk rendezni a dolgokat, hogy soha ne legyen azonos időben két processzus a kritikus szekciójában, akkor elkerülhetnénk a versenyhelyzeteket.

A jó megoldáshoz négy feltétel kell:	
- Ne legyen két processzus egyszerre a saját kritikus szekciójában.
- Semmilyen előfeltétel ne legyen a sebességekről vagy a CPU-k számáról.
- Egyetlen, a kritikus szekcióján kívül futó processzus sem blokkolhat más processzusokat.
- Egyetlen processzusnak se kelljen örökké arra várni, hogy belépjen a kritikus szekciójába.

Azt, amikor folyamatosan tesztelünk egy változót egy bizonyos érték megjelenéséig, tevékeny várakozásnak nevezzük. Általában tartózkodni kellene ettől, mert pazarolja a CPU időt. Csak akkor használjuk a tevékeny várakozást, ha ésszerűen elvárható, hogy a várakozás rövid lesz. A tevékeny várakozást használó zárolásokat aktív várakozásnak hívjuk.
___________________________________________________________________________________________________________________________________

Altatás és ébresztés:

Termelő-fogyasztó probléma:
	-> Két process osztozik egy közös, rögzített méretű tárolón
	-> egyik gyárt (adatot helyez el), másik fogyaszt (kiveszi)
	-> Két nehézség:
		_ Új elem, de a tároló tele
		_ Üres tárolóból elemet kivenni
	-> Megoldások:
		_ Ha megtelik a tároló, elalszik a gyártó, és akkor ébresztik fel, amikor van helyú
		_ Ha a fogyasztó látja, hogy üres a tároló, akkor elalszik addig, amíg nem kerül bele elem


Szemafor: egy változótípus, amiben számolhatjuk az ébresztéseket későbbi felhasználáscéljából. A szemafor értéke lehet 0, jelezve, hogy nincs elmentett ébresztés, vagy valamilyen pozitív érték, ha egy vagy több ébresztés van függőben. Két alapvető művelet van, a down és az up (rendre a sleep és a wakeup általánosításai).

down művelet:
	-> megvizsgálja, hogy a szemafor > 0
		_ igen: csökkenti az értékét (felhasznál egy tárolt ébresztést) és folytatja
		_ nem: elaltatja, mielőtt a down befejeződne
	-> minden művelet osztatlan, elemi, ez garantálja, hogy nem lehet elérni a szemafort addig, amíg nem fejeződik be a művelet

up művelet:
	-> Szemafor értékét növeli
	-> Ha egy vagy több process aludne ezen, akkor közülük az egyiket kiválasztja a rendszer és megengedi, hogy befejezze a down műveletet
	-> Így olyanszemaforon végrehajtva az up műveletet, amelyen processzusok aludtak, a szemafor még mindig 0 lesz, de eggyel kevesebb processzus fog rajta aludni.
	-> Úgyszintén egy műveletnek számít, így nem blokkolható


Mutex: a szemafor egyszerűsített változatai, csak bizonyos erőforrások vagy kódrészek kölcsönös kizárásának kezelésére alkalmasak. Ez egy olyan változó, amely kétféle állapotban lehet: nem zárolt vagy zárolt. Ennek következtében egyetlen bit is elegendő a reprezentálásához, de a gyakorlatban gyakran egy egész értéket használnak, ahol a 0 jelenti a nem zárolt, és bármilyen más érték a zárolt állapotot.

mutex_lock:
	-> process (vagy szál), hozzá akar férni egy kritikus szekcióhoz
		_ ha nem zárolt: sikeres hívás, szabad belépés
		_ ha zárolt: hívó blokkolódik, amíg nem végez lock-oló process, majd meghívja a mutex_unlock eljárást

Holtpont: minden lehetséges process blokkolt

Monitor: eljárások, változók és adatszerkezetek együttese, mindezek egy speciális fajta modulba vagy csomagba összegyűjtve
	-> processek bármikor hívhatják az ott megtalálható eljárásokat
	-> nem érik el közvetlenül a első adatszerkezeteit
	-> minden időpillanatban, csak egy process lehet aktív a monitorban
	-> mutexek vagy binráis szemafor-t használnak a monitorok belépési pontjainál
___________________________________________________________________________________________________________________________________

Kölcsönös kizárás tevékeny várakozással

A kölcsönös kizárás tevékeny várakozással olyan módszerek és technikák összessége, amelyek célja, hogy kizárják a versenyhelyzeteket és az erőforrásokhoz való hozzáférés konfliktusait több szálú vagy több processzes környezetben. Az ilyen rendszerekben az erőforrásokat (pl. változók, fájlok, adatstruktúrák stb.) úgy kell kezelni, hogy egyszerre csak egy szál vagy processzus férhessen hozzájuk, és másoknak várakozniuk kell, amíg az erőforráshoz való hozzáférés elérhető lesz számukra.

A kölcsönös kizárás eléréséhez különböző technikák használhatók. Néhány ilyen technika a következő:

    Megszakítások tiltása: A megszakítások letiltása útján biztosítható a kizárólagos hozzáférés az erőforrásokhoz. Ez azt jelenti, hogy a kritikus szakaszokban nem engedélyezett megszakítások kiváltása, így a szál vagy processzus folyamat zavartalanul hozzáférhet az erőforráshoz.

    Változók zárolása: Zárolási mechanizmusokat lehet alkalmazni a változókhoz, amelyek garantálják, hogy csak egy szál vagy processzus férjen hozzá az adott változóhoz egyszerre. Például, ha egy szál már zárolta a változót, akkor más szálaknak várakozniuk kell, amíg a zárolás feloldódik.

    Szigorú váltogatás: Egy szigorú váltogatási protokoll során a szálak vagy processzusok szigorúan egymás után váltakoznak a kritikus szakaszokban. Például, ha két szál versenyezik egy erőforráshoz való hozzáférésért, akkor egy előre meghatározott sorrendben kell váltogatni közöttük, hogy biztosítsák a kizárólagos hozzáférést.

    Peterson megoldása: Az egyik híres kölcsönös kizárási algoritmus, amelyet William Wesley Peterson fejlesztett ki. Ez az algoritmus szoftveres megoldást nyújt a kölcsönös kizárás problémájára két szál között. Az algoritmus zárolási változókat és forgatókönyveket használ a kritikus szakaszok szinkronizálására.

    TSL utasítás (Test-and-Set Lock): A TSL utasítás egy alacsony szintű hardveres utasítás, amely atomikusan ellenőrzi és beállítja egy memóriaterület értékét. Ezt az utasítást használhatjuk zárolásra, hogy biztosítsuk a kizárólagos hozzáférést az erőforrásokhoz.

Ezek csak néhány példa a kölcsönös kizárás tevékeny várakozással kapcsolatos technikákból és megoldásokból. Az ilyen módszerek segítségével a több szál vagy több processzus által megosztott erőforrásokhoz való hozzáférés konfliktusai minimalizálhatók vagy megszüntethetők, biztosítva ezzel a programok helyes és kiszámítható végrehajtását.
___________________________________________________________________________________________________________________________________

Üzenetek, adás, vétel

Írók és olvasók problémája:
	-> Adatbázis elérést modellez
	-> Elfogadható, hogy több process egyidejúleg olvasson
	-> Ha egy process aktualizálja, akkor nem szabad a többieknek elérniük (olvasóknak sem)
	-> Kérdés: hogyan programozzuk az olvasókat és írókat
	-> Ha folyamatosan jönnek az olvasók, akkor az író nem fog olyan pontot találni, amikor nincs olvasó
	-> Megoldás: Írónak adni prioritást

Sorompó:
	-> primitív könyvtári eljárás
	-> Fázisokra osztja az alkalmazást
	-> Egy process se juthat a következő fázisra, amíg az összes másik process készen nem áll
	-> Mindegyik fázis végére teszünk egy sorompót, ha odaér egy process, akkor blokkolódik
	-> Az utolsó process beérkezése után engedi el a sorompó az összes többit
	-> Hasznos lehet nagy mátrixokon végzett párhuzamos műveleteknél


Sorompók:
