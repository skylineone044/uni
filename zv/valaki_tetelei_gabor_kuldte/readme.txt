Ezek a dokumentumok az előadók és gyakorlatvezetők által közzétett jegyzetekből és egyéb anyagokból való válogatás útján készültek.
Sajnos nem minden esetben teljesen egyértelmű, hogy pontosan mi tartozik egy adott tételhez. Hiányosságokért és/vagy pontatlanságokért felelősséget nem vállalok! :)

Copyright:

Dr. Farkas Richárd http://www.inf.u-szeged.hu/~rfarkas/Alga19/algaI.html
Dr. Iván Szabolcs http://www.inf.u-szeged.hu/~szabivan/
Dr. Fülöp Zoltán http://www.inf.u-szeged.hu/~fulop/okt_en.html
Dr. Csendes Tibor https://www.inf.u-szeged.hu/~csendes/oktatas_hu.html
Dr. Jelasity Márk http://www.inf.u-szeged.hu/~jelasity/mi1/2019/
Dr. London András http://www.inf.u-szeged.hu/~london/opkutEA.html
Dr. Nagy Antal Operációs rendszerek előadásanyag a pub/ könyvtárban (2014)
Gelle Kitti http://www.inf.u-szeged.hu/~kgelle/?q=node/2
Mester Abigél http://www.inf.u-szeged.hu/~mester/opkut.html

Dr. Balázs Péter és Dr. Németh Gábor Adatbázisok jegyzet a pub/ könyvtárban (2019)
Dr. Palágyi Kálmán Digitális képfeldolgozás előadásanyag a pub/ könyvtárban (2020)
Dr. Gergely Tamás és Dr. Jász Judit Programozás alapjai előadásanyag a pub/ könyvtárban (2019)
Dr. Ferenc Rudolf és Dr. Jász Judit Programozás I előadásanyag a pub/ könyvtárban (2019)
Dr. Alexin Zoltán Programozás II jegyzet a pub/ könyvtárban (2019)
Dr. Kertész Attila Programozási nyelvek jegyzet a pub/ könyvtárban (2018)
Dr. Gyimóthy Tibor / Dr. Beszédes Árpád Rendszerfejlesztés I előadásanyag a pub/ könyvtárban (2017)
Dr. Bilicki Vilmos http://www.inf.u-szeged.hu/~bilickiv/szh/index.html
Dr. Nagy Antal és Dr. Tanács Attila Számítógép architektúrák előadásanyag a Coospace-ről (2017)

2020. június

-----------------------------------

Hibajegyzék:

I/14. tétel Operációkutatás I
- 2. oldal komplementáris lazaság: A^T * y <= c helyett >= c kell (mint az előző oldalon látható)

